# Modello: a home made deep learning library

Modello is a library born by the desire of the author to learn
the Mathematics behind the functioning of the various different flavors of Neural Networks
(feed forward, deep, recurrent, ...). While nor the author\'s learning path has been completed,
neither his desire of understanding more about the functioning of these class of ML models
has been satisfied, Modello has now reached the state wherein it can do some actual work.
In particular, Modello is build upon a from scratch implemented FORTRAN 2003 backend using openMP,
that exposes a series of API to R. It can be used to build fairly complex computational graphs,
which ca be automatically differentiated and optimized with methods such as Stochastic Gradient
Descent and Adam. Thus the decision to share the library with the community, hoping for collaborations, 
suggestions and ideas, that could improve and develop further library. 
The hope is that in the future Modello can become and alternative to the main 
frameworks for approaching natural network modeling.

## Current features

Currently Modello focuses on those aspects that were of interests for the personal research and learning 
path of the author. 

The library implements the main in operators relative to arrays up to two dimensions, 
like those listed in the following.
* Different flavors of matrix-matrix and matrix vector multiplication.
* Main activation functions like sigmoid, tanh, relu, elu, swish.
* Most common reduction operators, such as sum and product.
* Popular loss functions: mse, mae, binary-entropy, cross-entropy.
* Commonly used probability density distributions, like Normal, Exponential, Gamma, Beta, Laplace.
These should give already significant flexibility in building different kind of models.

For what concern the modeling, classes for building Feed-Forward Networks of arbitrary
depth and width, Recurrent Networks and LSTM Networks have been implemented.

For the training of the models are provided the Stochastic Gradient Descent Algorithm, 
Stochastic Gradient Descent with Momentum, and Adam.

Basic capability to sequentially load data and feed them to the computational graph are also provided.

## Installation

The library is not currently on CRAN (it soon will be), thus to install it
clone the repository, enter the `Rpackage` directory, and user the standard R procedure.

Optionally, run the tests to assure that everything is correct:

`$ R check Rpackage`

Build and install the library:

`$ R CMD build Rpacakge && R CMD INSTALL modello_0.1.tar.gz`

## Basic Usage
Time allowing, some vignettes and tutorials will be provided. 
However the standard workflow follows.

Import the library and initialize the session.

```
> library(modello)
> modello.init()
```

Currently Modello uses fix size arrays to store the different steps of a calculations.
In the case that is needed to change the default sizes the desired values can be provided as
arguments to the `modello.init` function (see the relative documentation for more details).

Arrays are defined with the function `number`. For example:
```
> x1 = number(matrix(rnomr(9), 3, 3))
> x2 = number(runif(3))
> x4 = number(1, scalar=TRUE)
> x5 = number(c(1, 2, 3), dx=FALSE)
```
* `x1` will be a 3x3 array with value initialized according to the standard normal distribution.
* `x2` will be a 3x1 array with values initialized according to a Uniform(0, 1) distribution.
* `x4` will be a scalar equal to 1.
* `x5` will be a 3x1 array which does not have an associated gradient.

The main R operators have been overloaded and few complementary ones have been implemented. For instance:
```
> y1 = x1 + x4
> y2 = y1 %.% x1
> y3 = x2 %T.% y2
> y4 = y3 * x5
```
where `%.%` is the matrix product and `%T.%` is the matrix product between `x2^T` and `y2`. 
As it is possible to see from the example, a basic broadcasting mechanism has been provided as well.
Broadcasting id done along the dominant dimension, that is column-wise. Thus `y4` will be the result of 
the element wise product of the columns of `y3` with `x5`. 

If it is necessary to store the calculation for later use, especially differentiation,
it is necessary first to open a graph:
```
> G = graph.open()
> ...
> some calculations
> ...
> graph.close()
```
Now the following methods are available for the created graph.
* With `G$op()` all the calculation performed between the `graph.open` and `graph.close` statement 
will be undertaken and the values of the `number` object involved updated. These values can be retrieved 
though `x$v` or `x$get.v()`.
* With `G$bw()` backward differentiation is undertaken. The gradient of each `number` can be
obtained calling `x$dv` or `x$get.dv`
Data can be fed into a `number` with `x$v = data` or `x$set.v(data)`. Similarly it is possible to feed
the gradient as follows: `x$dv = data` or `x$set.dv(data)`.

## Example of LSTM training

Here in the following, the work flow that can be followed to train a LSTM Network.
With minimal modification the same flow can be used to train custom models defined as sub-classes of the
`module` class. Look at the implantation of `module.LSTM` as example.

```
> library(modello)

> modello.init()

> X = as.matrix(sin(seq(0, 100, length.out=100000)))

> batch_size = 100 
> sequence_length = 50
> hidden_layer_size= 4
> learning_rate = 0.001
> epochs = 50

> DS = DataSet.Seq$new(X, sequence)
> DL = DataLoader.Seq$new(DS.train, batch_size)


> mdl = module.LSTM$new(0, HIDDEN_LAYERS, ncol(X), ACTIVATION, NOUT, NULL, FALSE, FALSE)
> opt = adam.opt(mdl$pars())

> SEQ = DL$feed(NULL, "random")
> G = graph.open()
> ANS = lapply(SEQ$X, function(dd)mdl$op(dd))
> Yh = bind(ANS, 2)
> J = mse(SEQ$Y, Yh)
> graph.close()

> for (e in 1:epochs) {
	LOSS = 0
	DL$reset()
	while (DL$has.next()) {
		n = n + 1
		DL$feed(SEQ, "random")
		G$op()
		opt$step(G, J, lr, b1, b2, 1)
		LOSS = LOSS + J$v
	}
	print(paste("epoch =", e, "training-loss =", LOSS))
}
```
