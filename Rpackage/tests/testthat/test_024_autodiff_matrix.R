test_autodiff_matrix <- function () {

    test_that("'gmmmult__4' dx operator is consistent", {
        test <- function (ta, tb, x1, x2) {
            fn = function (A, B) {
                if (ta > 0) A = t(A)
                if (tb > 0) B = t(B)
                A %*% B
            }
            g = graph_open()
            y = gmmmult(ta=ta, tb=tb, alpha=NULL, A=x1$x, B=x2$x, beta=NULL, C=NULL)
            graph_close()
            y$dv = 1
            y$bw()
            dA = dfun(x1$a, function(x)fn(x, x2$a))
            dB = dfun(x2$a, function(x)fn(x1$a, x))
            expect_lt(test_err(c(x1$x$dv, x2$x$dv), c(dA, dB), toldx_()), toldx_())
        }
        test(0, 0, matrix_(3, 3, TRUE), matrix_(3, 5, TRUE))
        test(1, 0, matrix_(5, 3, TRUE), matrix_(5, 2, TRUE))
        test(0, 1, matrix_(6, 3, TRUE), matrix_(2, 3, TRUE))
        test(1, 1, matrix_(4, 2, TRUE), matrix_(6, 4, TRUE))
    })

    test_that("'gmmmult__3' dx operator is consistent", {
        test <- function (ta, tb, x1, x2, x3) {
            fn = function (A, B, C) {
                if (ta > 0) A = t(A)
                if (tb > 0) B = t(B)
                A %*% B + C
            }
            g = graph_open()
            y = gmmmult(ta=ta, tb=tb, alpha=NULL, A=x1$x, B=x2$x, beta=NULL, C=x3$x)
            graph_close()
            y$dv = 1
            y$bw()
            dA = dfun(x1$a, function(x)fn(x, x2$a, x3$a))
            dB = dfun(x2$a, function(x)fn(x1$a, x, x3$a))
            dC = dfun(x3$a, function(x)fn(x1$a, x2$a, x))
            expect_lt(test_err(c(x1$x$dv, x2$x$dv, x3$x$dv), c(dA, dB, dC), toldx_()), toldx_())
        }
        test(0, 0, matrix_(3, 3, TRUE), matrix_(3, 3, TRUE), matrix_(3, 3, TRUE))
        test(1, 0, matrix_(3, 3, TRUE), matrix_(3, 3, TRUE), matrix_(3, 3, TRUE))
        test(0, 1, matrix_(3, 3, TRUE), matrix_(3, 3, TRUE), matrix_(3, 3, TRUE))
        test(1, 1, matrix_(3, 3, TRUE), matrix_(3, 3, TRUE), matrix_(3, 3, TRUE))
    })

    test_that("'gmmmult__2' dx operator is consistent", {
        test <- function (ta, tb, a, x1, x2) {
            fn = function (alpha, A, B) {
                if (ta > 0) A = t(A)
                if (tb > 0) B = t(B)
                alpha * A %*% B
            }
            g = graph_open()
            y = gmmmult(ta=ta, tb=tb, alpha=a$x, A=x1$x, B=x2$x, beta=NULL, C=NULL)
            graph_close()
            y$dv = 1
            y$bw()
            dalpha = dfun(a$a, function(x)fn(x, x1$a, x2$a))
            dA = dfun(x1$a, function(x)fn(a$a, x, x2$a))
            dB = dfun(x2$a, function(x)fn(a$a, x1$a, x))
            expect_lt(test_err(c(a$x$dv, x1$x$dv, x2$x$dv), c(dalpha, dA, dB), toldx_()), toldx_())
        }
        test(0, 0, scalar_(TRUE), matrix_(3, 3, TRUE), matrix_(3, 3, TRUE))
        test(1, 0, scalar_(TRUE), matrix_(3, 3, TRUE), matrix_(3, 3, TRUE))
        test(0, 1, scalar_(TRUE), matrix_(3, 3, TRUE), matrix_(3, 3, TRUE))
        test(1, 1, scalar_(TRUE), matrix_(3, 3, TRUE), matrix_(3, 3, TRUE))
    })

    test_that("'gmmmult__1' dx operator is consistent", {
        test <- function (ta, tb, a, x1, x2, b, x3) {
            fn = function (alpha, A, B, beta, C) {
                if (ta > 0) A = t(A)
                if (tb > 0) B = t(B)
                alpha * A %*% B + beta * C
            }
            g = graph_open()
            y = gmmmult(ta=ta, tb=tb, alpha=a$x, A=x1$x, B=x2$x, beta=b$x, C=x3$x)
            graph_close()
            y$dv = 1
            y$bw()
            dalpha = dfun(a$a, function(x)fn(x, x1$a, x2$a, b$a, x3$a))
            dA = dfun(x1$a, function(x)fn(a$a, x, x2$a, b$a, x3$a))
            dB = dfun(x2$a, function(x)fn(a$a, x1$a, x, b$a, x3$a))
            dbeta = dfun(b$a, function(x)fn(a$a, x1$a, x2$a, x, x3$a))
            dC = dfun(x3$a, function(x)fn(a$a, x1$a, x2$a, b$a, x))
            expect_lt(test_err(c(a$x$dv, x1$x$dv, x2$x$dv, b$x$dv, x3$x$dv), c(dalpha, dA, dB, dbeta, dC), toldx_()), toldx_())
        }
        test(0, 0, scalar_(TRUE), matrix_(3, 3, TRUE), matrix_(3, 3, TRUE), scalar_(TRUE), matrix_(3, 3, TRUE))
        test(1, 0, scalar_(TRUE), matrix_(3, 3, TRUE), matrix_(3, 3, TRUE), scalar_(TRUE), matrix_(3, 3, TRUE))
        test(0, 1, scalar_(TRUE), matrix_(3, 3, TRUE), matrix_(3, 3, TRUE), scalar_(TRUE), matrix_(3, 3, TRUE))
        test(1, 1, scalar_(TRUE), matrix_(3, 3, TRUE), matrix_(3, 3, TRUE), scalar_(TRUE), matrix_(3, 3, TRUE))
    })

    test_that("'gmvmult__1' dx operator is consistent", {
        test <- function (ta, a, x1, x2, b, x3) {
            fn = function (alpha, A, B, beta, C) {
                if (ta > 0) A = t(A)
                alpha * A %*% B + beta * C
            }

            g = graph_open()
            y = gmvmult(ta=ta, alpha=a$x, A=x1$x, B=x2$x, beta=b$x, C=x3$x)
            graph_close()
            y$dv = 1
            y$bw()
            dalpha = dfun(a$a, function(x)fn(x, x1$a, x2$a, b$a, c(x3$a)))
            dA = dfun(x1$a, function(x)fn(a$a, x, x2$a, b$a, c(x3$a)))
            dB = dfun(x2$a, function(x)fn(a$a, x1$a, x, b$a, c(x3$a)))
            dbeta = dfun(b$a, function(x)fn(a$a, x1$a, x2$a, x, c(x3$a)))
            dC = dfun(x3$a, function(x)fn(a$a, x1$a, x2$a, b$a, c(x)))
            expect_lt(test_err(c(a$x$dv, x1$x$dv, x2$x$dv, b$x$dv, x3$x$dv), c(dalpha, dA, dB, dbeta, dC), toldx_()), toldx_())
        }
        test(0, scalar_(TRUE), matrix_(3, 3, TRUE), vector_(3, TRUE), scalar_(TRUE), vector_(3, TRUE))
        test(1, scalar_(TRUE), matrix_(3, 3, TRUE), vector_(3, TRUE), scalar_(TRUE), vector_(3, TRUE))
    })

    test_that("'gmvmult__2' dx operator is consistent", {
        test <- function (ta, a, x1, x2) {
            fn = function (alpha, A, B) {
                if (ta > 0) A = t(A)
                alpha * A %*% B
            }
            g = graph_open()
            y = gmvmult(ta=ta, alpha=a$x, A=x1$x, B=x2$x)
            graph_close()
            y$dv = 1
            y$bw()
            dalpha = dfun(a$a, function(x)fn(x, x1$a, x2$a))
            dA = dfun(x1$a, function(x)fn(a$a, x, x2$a))
            dB = dfun(x2$a, function(x)fn(a$a, x1$a, x))
            expect_lt(test_err(c(a$x$dv, x1$x$dv, x2$x$dv), c(dalpha, dA, dB), toldx_()), toldx_())
        }
        test(0, scalar_(TRUE),  matrix_(3, 3, TRUE), vector_(3, TRUE))
        test(1, scalar_(TRUE),  matrix_(3, 3, TRUE), vector_(3, TRUE))
    })

    test_that("'gmvmult__3' dx operator is consistent", {
        test <- function (ta, x1, x2, x3) {
            fn = function (A, B, C) {
                if (ta > 0) A = t(A)
                A %*% B + C
            }
            g = graph_open()
            y = gmvmult(ta=ta, A=x1$x, B=x2$x, C=x3$x)
            graph_close()
            y$dv = 1
            y$bw()
            dA = dfun(x1$a, function(x)fn(x, x2$a, c(x3$a)))
            dB = dfun(x2$a, function(x)fn(x1$a, x, c(x3$a)))
            dC = dfun(x3$a, function(x)fn(x1$a, x2$a, c(x)))
            expect_lt(test_err(c(x1$x$dv, x2$x$dv, x3$x$dv), c(dA, dB, dC), toldx_()), toldx_())
        }
        test(0, matrix_(3, 3, TRUE), vector_(3, TRUE), vector_(3, TRUE))
        test(1, matrix_(3, 3, TRUE), vector_(3, TRUE), vector_(3, TRUE))
    })

    test_that("'gmvmult__4' dx operator is consistent", {
        test <- function (ta, x1, x2) {
            fn = function (A, B) {
                if (ta > 0) A = t(A)
                A %*% B
            }
            g = graph_open()
            y = gmvmult(ta=ta, A=x1$x, B=x2$x)
            graph_close()
            y$dv = 1
            y$bw()
            dA = dfun(x1$a, function(x)fn(x, x2$a))
            dB = dfun(x2$a, function(x)fn(x1$a, x))
            expect_lt(test_err(c(x1$x$dv, x2$x$dv), c(dA, dB), toldx_()), toldx_())
        }
        test(0, matrix_(3, 3, TRUE), vector_(3, TRUE))
        test(1, matrix_(3, 3, TRUE), vector_(3, TRUE))
    })

    test_that("'vvouter__1' dx operator is consistent", {
        test <- function (a, x1, x2, x3) {
            fn = function (alpha, A, B, C) {
                alpha * A %*% t(B) + C
            }
            g = graph_open()
            y = ger(alpha=a$x, A=x1$x, B=x2$x, C=x3$x)
            graph_close()
            y$dv = 1
            y$bw()
            dalpha = dfun(a$a, function(x)fn(x, x1$a, x2$a, x3$a))
            dA = dfun(x1$a, function(x)fn(a$a, x, x2$a, x3$a))
            dB = dfun(x2$a, function(x)fn(a$a, x1$a, x, x3$a))
            dC = dfun(x3$a, function(x)fn(a$a, x1$a, x2$a, x))
            expect_lt(test_err(c(a$x$dv, x1$x$dv, x2$x$dv, x3$x$dv), c(dalpha, dA, dB, dC), toldx_()), toldx_())
        }
        test(scalar_(TRUE), vector_(3, TRUE), vector_(3, TRUE), matrix_(3, 3, TRUE))
    })

    test_that("'vvouter__2' dx operator is consistent", {
        test <- function (x1, x2, x3) {
            fn = function (A, B, C) {
                A %*% t(B) + C
            }

            g = graph_open()
            y = ger(A=x1$x, B=x2$x, C=x3$x)
            graph_close()
            y$dv = 1
            y$bw()
            dA = dfun(x1$a, function(x)fn(x, x2$a, x3$a))
            dB = dfun(x2$a, function(x)fn(x1$a, x, x3$a))
            dC = dfun(x3$a, function(x)fn(x1$a, x2$a, x))
            expect_lt(test_err(c(x1$x$dv, x2$x$dv, x3$x$dv), c(dA, dB, dC), toldx_()), toldx_())
        }
        test(vector_(3, TRUE), vector_(3, TRUE), matrix_(3, 3, TRUE))
    })

    test_that("'vvouter__3' dx operator is consistent", {
        test <- function (x1, x2) {
            fn = function (A, B) {
                A %*% t(B)
            }
            g = graph_open()
            y = ger(A=x1$x, B=x2$x)
            graph_close()
            y$dv = 1
            y$bw()
            dA = dfun(x1$a, function(x)fn(x, x2$a))
            dB = dfun(x2$a, function(x)fn(x1$a, x))
            expect_lt(test_err(c(x1$x$dv, x2$x$dv), c(dA, dB), toldx_()), toldx_())
        }
        test(vector_(3, TRUE), vector_(3, TRUE))
    })

    test_that("'vvinner' dx operator is consistent", {
        test <- function (x1, x2) {
            fn = function (A, B) {
                t(A) %*% B
            }

            g = graph_open()
            y = x1$x %T.% x2$x
            graph_close()
            y$dv = 1
            y$bw()
            dA = dfun(x1$a, function(x)fn(x, x2$a))
            dB = dfun(x2$a, function(x)fn(x1$a, x))
            expect_lt(test_err(c(x1$x$dv, x2$x$dv), c(dA, dB), toldx_()), toldx_())
        }
        test(vector_(3, TRUE), vector_(3, TRUE))
    })

    test_that("'invMat' dx operator is consistent", {
        test <- function (x1) {
            g = graph_open()
            y = invMat(x1$x)
            graph_close()
            y$dv = 1
            y$bw()
            dA = dfun(x1$a, invMat)
            expect_lt(test_err(c(x1$x$dv), c(dA), toldx_()), toldx_())
        }
        test(matrix_(3, 3, TRUE))
    })

}

context("autodifferentiation: matrix")

modello_reset()

test_autodiff_matrix()
