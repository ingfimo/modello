.intrf.number__ldexp <- function (y, lam) {
    .Call(intrf_c__number__ldexp, as.integer(y), as.integer(lam),
          PACKAGE="modello")
}

.intrf.number__ldlaplace <- function (y, mu, lam) {
    .Call(intrf_c__number__ldlaplace, as.integer(y), as.integer(mu),
          as.integer(lam), PACKAGE="modello")
}

.intrf.number__ldbeta <- function (y, a1, a2) {
    .Call(intrf_c__number__ldbeta, as.integer(y), as.integer(a1),
          as.integer(a2), PACKAGE="modello")
}

.intrf.number__ldgamma <- function (y, a, b) {
    .Call(intrf_c__number__ldgamma, as.integer(y), as.integer(a),
          as.integer(b), PACKAGE="modello")
}

.intrf.number__ldnorm <- function (y, mu, s) {
    .Call(intrf_c__number__ldnorm, as.integer(y), as.integer(mu),
          as.integer(s), PACKAGE="modello")
}

.intrf.number__ldmvnorm__1 <- function (y, mu, E) {
    .Call(intrf_c__number__ldmvnorm__1, as.integer(y), as.integer(mu),
          as.integer(E), PACKAGE="modello")
}

.intrf.number__mvnorm_posterior <- function (a11, e21, e22, y11, mu11, inv) {
    ans = .Call(intrf_c__number__mvnorm_posterior, as.integer(a11), as.integer(e21),
                as.integer(e22), as.integer(y11), as.integer(mu11), as.integer(inv), PACKAGE="modello")
    names(ans) = c("mu", "E")
    ans
}
