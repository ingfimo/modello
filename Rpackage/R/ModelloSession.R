##' Allocates all the arrays necessary to initialise
##' the modello session.
##' 
##' @title Initialise Modello Session
##' @param n.numbers number of \code{Number}s
##' @param n.nodes  number of operations
##' @param n.graphs number of \code{Graph}s
##' @param n.opts number of \code{Optimisers}
##' @return Return invisible NULL
##' @author Filippo Monari
##' @export
modello_init <- function (n.numbers=1000000, n.nodes=1000000,
                          n.graphs=1000,  n.opts=100, n.ht=10000, dtyp=.DP()) {
    .modello$init(n.numbers, n.nodes, n.graphs, n.opts, n.ht, dtyp)
}

##' Checks if the modello sesssion is initialised
##'
##' @title Modello Is Initialised
##' @return Returns TRUE/FALSE 
##' @author Filippo Monari
##' @examples
##' modello_init(10, 10, 10, 10) # Initilaises the modello session with arrays of the given size
##' modello_is_init() # TRUE
##' modello_close()   # Closes modello
##' modello_is_init() # FALSE
##' @export
modello_is_init <- function () {
    .modello$.init
}

##' Rest the modello session, that is
##' deallocates and reallocates all the arrays.
##' All the data stored in them is lost.
##'
##' @title Reset Modello Session 
##' @return Return invisible NULL
##' @author Filippo Monari
##' @examples
##' modello_init(10, 10, 10, 10) # Initilaises the modello session with arrays of the given size
##' modello_is_init() # TRUE
##' modello_reset()   # restart the modello session with initial array sizes 
##' modello_is_init() # TRUE
##' modello_close()
##' @export
modello_reset <- function () {
    .modello$reset()
}

##' Deallocates all the arrays.
##'
##' @title Close Modello Session 
##' @return Returns invsible NULL
##' @author Filippo Monari
##' @examples
##' modello_init(10, 10, 10, 10) # Initilaises the modello session with arrays of the given size
##' modello_is_init() # TRUE
##' modello_reset()   # restart the modello session with initial array sizes 
##' modello_is_init() # TRUE
##' modello_close()   # Closes modello
##' modello_is_init() # FALSE
##' @export
modello_close <- function () {
    .modello$close()
}

##' Sets globally the calculation mode
##'
##' @title Set Modello Global Mode 
##' @param m 
##' @return 
##' @author Filippo
##' @export
modello_mode <- function(m) {
    .modello$set_mode(m)
}
##' Gets the integer kind indicating single precision
##'
##' @title Single Precision  
##' @return integer, single precision kind value
##' @author Filippo
##' @export
.SP <- function () {
    .intrf.get_sp()
}
##' Gets the integer kind indicating double precision
##'
##' @title Double Precision  
##' @return integer, double precision kind value
##' @author Filippo
##' @export
.DP <- function () {
    .intrf.get_dp()
}
##' Retrieves the id of the training mode
##'
##' @title Get Evalaution Mode
##' @return integer, training mode id
##' @author Filippo
##' @export
.training_mode <- function () {
    .intrf.get_training_mode()
}
##' Retrieves the id of the evalaution mode
##'
##' @title Get Evalaution Mode
##' @return integer, evalaution mode id
##' @author Filippo
##' @export
.evaluation_mode <- function () {
    .intrf.get_evaluation_mode()
}
##' Gets the current precision as integer kind
##'
##' @title Get Current Precision
##' @return integer kind of the current precision
##' @author Filippo
##' @export
modello_dtyp <- function () {
    .modello$dtyp
}
##' Garbage collector for the current modello session.
##'
##' @title Modello Session Garbage Collector 
##' @param env environmet wherein collect the garbage
##' @author Filippo MOnari
##' @export
modello_gc <- function (env=.GlobalEnv) {
    .modello$number_gc()
    .modello$graph_gc()
    nms = ls(envir=env)
    for (nm in nms) {
        x = get(nm, envir=env)
        if (is.Number(x) || is.Graph(x)) {
            if (!x$is_linked()) {
                rm(list=nm, envir=env)
            }
        }
    }
}

##' Given its name (charcter id) retrieves
##' a \code{Number} (if it exists). 
##'
##' @title Get Number 
##' @param name \code{number} name
##' @return Returns the \code{number} associated
##' with the provided name 
##' @author Filippo Monari
.nn <- function (name) {
    .modello$get_number(name)
}

##' Given its name (character id) retrieves
##' a \code{graph} (if it exists).
##'
##' @title Get Graph 
##' @param name \code{Graph} name
##' @return Returns the \code{Graph} associated
##' with the provided name
##' @author Filippo Monari
.gg <- function (name) {
    .modello$get_graph(name)
}

##' R6 class representing the current modello session
##'
##' An object of this class called '.modello' is created when
##' the package is loaded and will do the booking keeping of
##' what happens during the session.
##' @author Filippo Monari
ModelloSession = R6Class(
    'ModelloSession',
    public = list(
        ##' @field .dtyp integer kind indicating the numeric precision
        dtyp = NULL,
        ##' @field .init stores the init status of the session (TRUE/FALSE)
        .init = NULL,
        ##' @description
        ##' modello session object initialisation method.
        ##' It does nothgin, it is just use on package load
        ##' to create the .modello object containing the session.
        initialize = function () {
            self$.init = FALSE
            self$.numbers = new.env()
            self$.nodes = new.env()
            self$.graphs = new.env()
            self$.opts = new.env()
        },
        ##' @description
        ##' Allocates all the arrays necessary to the session
        ##'
        ##' @param n.numbers number of \code{Number}s
        ##' @param n.nodes number of operations
        ##' @param n.graphs number of \code{Graph}s
        ##' @param n.opts number of \code{Optimisers}
        ##' @return Returns invisible NULL.
        init  = function (n.numbers, n.nodes, n.graphs, n.opts, n.ht, dtyp) {
            .intrf.set_dtyp(dtyp)
            .intrf.allocate_numbers(n.numbers)
            .intrf.allocate_nodes(n.nodes)
            .intrf.allocate_graphs(n.graphs)
            .intrf.allocate_gopts(n.opts)
            .intrf.allocate_hash_tables(n.ht)
            self$dtyp = dtyp
            self$.n_numbers = n.numbers
            self$.n_nodes = n.nodes
            self$.n_graphs = n.graphs
            self$.n_opts = n.opts
            self$.init = TRUE
            invisible()
        },
        ##' @description
        ##' Deallocates all the session arrays
        ##'
        ##' @return Returns invisible NULL.
        close = function () {
            .intrf.deallocate_numbers()
            .intrf.deallocate_nodes()
            .intrf.deallocate_graphs()
            .intrf.deallocate_gopts()
            self$.init = FALSE
            invisible()
        },
        ##' @description
        ##' Reset the session by deallocating and reallocating all the arrays.
        ##' The data in the arrays is lost.
        ##'
        ##' @return Returns invisible NULL.
        reset = function () {
            self$close()
            self$init(self$.n_numbers, self$.n_nodes,
                      self$.n_graphs, self$.n_opts, self$dtyp)
        },
        ##' @description
        ##' Closes the session if its object is destroyed.
        finalize = function () {
            if (self$.init) self$close()
        },
        set_mode = function (m) {
            .intrf.set_mode(m)
        },
        ##' @description
        ##' build unique random charcter identifier
        ##'
        ##' @param typ charcter indicating the type of object.
        next_name = function (typ) {
            tempfile('x', as.character(as.integer(Sys.time())), paste0('.', typ))
        },
        
        ##=======#
        ##NUMBERS#
        ##=======#

        ##' @field .n_numbers number of allocate slot for storing \code{Number}
        .n_numbers = NULL,
        ##' @field .numbers environment keeping track of the created \code{Number}s
        .numbers = NULL,
        ##' @description
        ##' Creates a new \code{Number}.
        ##'
        ##' @param id \code{Number} id 
        ##' @param name \code{Number} name
        number_new = function (id, name=NULL) {
            if (is.null(name)) {
                name = self$next_name('number')
            } else {
                stopifnot(!exists(name, envir=self$.numbers))
            }
            self$.numbers[[name]] = id
            .Number$new(name=name)
        },
        ##' @description
        ##' Append a \code{Number} of the given shape to the
        ##' \code{NUMBER_} array. A link to the \code{number}
        ##' is create the \code{Numbers} environment within the
        ##' session object. This method is called each time
        ##' a new \code{Number} is created.
        ##'
        ##' @param shp \code{Number} shape
        ##' @param dx if TRUE allocate a derivative array for the \code{Number}
        ##' @param name character, \code{Number} name. Only for loading saved \code{Number}s
        ##' @return Returns an object of class 'number' referring
        ##' to the appended \code{Number}
        number_append = function (shp, dx, name=NULL) {
            id = .intrf.number__append(shp, dx)
            self$number_new(id, name)
        },
        ##' @description
        ##' Given a name (character identifier) for a \code{number}
        ##' creates and returns a reference objects of class 'number'.
        ##'
        ##' @param name \code{number} name
        ##' @return Returns a object of class 'number'
        number_get = function (name) {
            stopifnot(exists(name, envir=self$.numbers))
            self$number_new(self$.numbers[[name]], name)
        },
        ##' @description
        ##' Pops (removes) a \code{Number} from the \code{NUMBERS_} array
        ##' according to the provided reference object.
        ##'
        ##' @param x reference object of class \code{Number}
        ##' @return Returns invisible x.
        number_pop = function (x) {
            stopifnot(is.Number(x) && x$is_linked())
            .intrf.number__pop(x$id())
            rm(list=x$name(), envir=self$.numbers)
            invisible(x)
        },
        ##' @description
        ##' Given a \code{Number}, retrives and
        ##' returs the id of the associated Fortran object
        ##' (i.e. its position in the \code{NUMBERS_} array.
        ##'
        ##' @param x a \code{Number}
        ##' @return Returns the \code{Number} id
        number_id = function (x) {
            stopifnot(is.Number(x))
            self$.numbers[[x$name()]]
        },
        ##' @description
        ##' Checks that the \code{Number} exists in the Fortran register \code{NUMBERS_}.
        ##' 
        ##' @param x reference object of class 'number'
        ##' @return Returns TRUE if the \code{Number} exists, FALSE otherwise.
        number_exists = function (x) {
            stopifnot(is.Number(x))
            exists(x$name(), envir=self$.numbers)
        },
        ##' @description
        ##' Calls the garbage collector for \code{Number}s.
        ##'
        ##' @return Returns invisible NULL
        number_gc = function () {
            .intrf.number__gc()
            nms = names(self$.numbers)
            for (nm in nms) {
                if (!.intrf.number__is_allocated(self$.numbers[[nm]])) {
                    rm(list=nm, envir=self$.numbers)
                }
            }
            invisible()
        },
        ##' @description
        ##' Applies a mathematical operator to its arguments.
        ##'
        ##' @param op operator name
        ##' @param ... operator parameters
        ##' @return Returns a \code{Number}
        apply_math_op = function (op, ...) {
            op.args = lapply(list(...), function (x) {
                stopifnot(!is.null(x))
                if (is.Number(x)) {
                    stopifnot(x$is_linked())
                    x$id()
                } else {
                    x
                }
            })
            id = do.call(op, op.args)
            if (is.null(id)) {
                invisible()
            } else if (length(id) > 1) {
                lapply(id, function(x)self$number_new(x))
            } else {
                self$number_new(id)
            }   
        },

        ##======##
        ##GRAPHS##
        ##======##

        ##' @field .n_graphs number of slots allocated for storing \code{Graph}s
        .n_graphs = NULL,
        ##' @field .graphs environement keeping track of the created \code{Graph}s
        .graphs = NULL,
        ##' @field .G current graph
        .G = NULL,
        G = function () {
            stopifnot(!is.null(self$.G))
            self$.G
        },
        ##' @description
        ##' Creates a new \code{Graph}
        ##'
        ##' @param id \code{Graph} id 
        ##' @return Returns a \code{Graph}
        graph_new = function (id) {
            name = self$next_name('graph')
            stopifnot(!exists(name, envir=self$.graphs))
            self$.graphs[[name]] = id
            .Graph$new(name)
        },
        ##' @description
        ##' Opens a \code{graph}. 
        ##'
        ##' if \code{g} is NULL a new graph is open and appended to the
        ##' \code{Graph} array. If a \code{Graph} is provided it is re-opened.
        ##'
        ##' @param g reference object of class 'graph'
        ##' @param wdx if TRUE the \code{Number}s in the graph will have gradients
        ##' @param dry if TRUE the graph will be allocated and initialised only
        ##' @return Returns an reference object of class 'graph'
        graph_open = function (g, wdx) {
            if (is.Graph(g)) {
                self$.G = g
                .intrf.graph__open(g$id(), wdx)
                invisible(g)
            } else {
                .intrf.graph__open(0, wdx)
                self$.G = self$graph_new(.intrf.graphi__get())
                self$.G
            }
        },
        graph_close = function () {
            ans = .intrf.graph__close()
            self$.G = NULL
            invisible()
        },
        ##' @description
        ##' Given a \code{Graph} name (character identifier) creates an
        ##' returns the corresponding \code{Graph}.
        ##'
        ##' @param name \code{graph} name
        ##' @return Returns a reference object of class 'graph'.
        graph_get = function (name) {
            stopifnot(exixsts(name, envir=self$.graphs))
            self$new_graph(self$.graphs[[name]])
        },
        ##' @description
        ##' Pops (removes) a \code{Graph} from the \code{GRAPHS_} array.
        ##'
        ##' @param x  a \code{Graph}
        ##' @return Returns invisible x.
        graph_pop = function (x) {
            stopifnot(is.Graph(x) && x$is_linked())
            .intrf.graph__pop(x$id())
            rm(list=x$name(), envir=self$.graphs)
            invisible(x)
        },
        ##' @description
        ##' Given \code{Graph}, retunrs
        ##' the id (i.e. position in the \code{GRAPHS_} array)
        ##' of the associated \code{graph}.
        ##'
        ##' @param x reference object of class 'graph'
        ##' @return Returns the \code{graph} id
        graph_id = function (x) {
            stopifnot(is.Graph(x))
            self$.graphs[[x$name()]]
        },
        ##' @description
        ##' Checks that the \code{Graph} associated to a referece
        ##' object of class 'graph' exists.
        ##' 
        ##' @param x reference object of class 'graph'
        ##' @return Returns TRUE if the \code{graph} exists, FALSE otherwise.
        graph_exists = function (x) {
            stopifnot(is.Graph(x))
            exists(x$name(), envir=self$.graphs)
        },
        ##' @description
        ##' Calls the \code{Graph} garbage collector.
        ##'
        ##' @return Returns invisible NULL
        graph_gc = function () {
            .intrf.graph__gc()
            nms = names(self$.graphs)
            for (nm in nms) {
                if (!.intrf.graph__is_allocated(self$.graphs[[nm]])) {
                    rm(list=nm, envir=self$.graphs)
                }
            }
        },

        ##=====##
        ##NODES##
        ##=====##

        ##' @field .n_nodes number of slot allocated for storing operations
        .n_nodes = NULL,
        .nodes = NULL,
        node_new = function (id, node) {
            name = self$next_name("node")
            stopifnot(!exists(name, envir=self$.nodes))
            self$.nodes[[name]] = id
            node$new(name)
        },
        node_append = function (append, node, ...) {
            id = append(...)
            self$node_new(id, node)
        },
        node_exists = function (x) {
            stopifnot(is.Node(x))
            exists(x$name(), envir=self$.nodes)
        },
        node_id = function (x) {
            stopifnot(self$node_exists(x))
            self$.nodes[[x$name()]]
        },
        apply_node_op = function (op, node, ...) {
            args = lapply(list(...), function (x) {
                stopifnot(!is.null(x))
                if (is.Number(x)) {
                    stopifnot(x$is_linked())
                    x$id()
                } else {
                    x
                }
            })
            do.call(self$node_append, c(op, node, args))   
        },
        ##====##
        ##OPTS##
        ##====##

        ##' @field .n_opts number of slots allocated for storing \code{Optimisers}
        .n_opts = NULL,
        ##' @field .opts environment keeping track of the created \code{Optimisers}
        .opts = NULL,
        ##' @description
        ##' Creates a new \code{Optimiser} and return the corresponding
        ##' reference object of class 'opt'
        ##'
        ##' @param id \code{Optimiser} id
        ##' @param opt R6 class indentifying the kind of optimiser
        ##' @return Returns a reference object of class 'opt'
        opt_new = function (id, opt) {
            name = self$next_name('opt')
            stopifnot(!exists(name, envir=self$.opts))
            self$.opts[[name]] = id
            opt$new(name)
        },
        ##' @description
        ##' Append an \code{Optimiser} with the given parameters to
        ##' the \code{OptS_} array. A link to the \code{Optimiser}
        ##' is created in \code{.opt} environment within the
        ##' session object. This method is called each time
        ##' a new \code{Optimiser} is created.
        ##'
        ##' @param append interface function for appending the optimiser to \code{GOPTS_} 
        ##' @param opt character dincating the kind of optimiser
        ##' @param ... optmiser parameters
        ##' @return Returns an object of class 'opt' referring
        ##' to the appended \code{Optmiser}
        opt_append = function (append, opt, ...) {
            id = append(...)
            self$opt_new(id, opt)
        },
        ##' @description
        ##' Given a name (character identifier) for an \code{Optimiser}
        ##' creates and returns a reference objects of class 'opt'.
        ##' [TO_CHECK]
        ##'
        ##' @param name \code{Optimiser} name
        ##' @return Returns a object of class 'opt'
        opt_get = function (name) {
            stopifnot(exists(name, envir=self$.opts))
            self$opt.number(self$.opts[[name]], name)
        },
        ##' @description
        ##' Pops (removes) an \code{Optmiser} from the \code{OPTS_} array
        ##' according to the provided reference object.
        ##'
        ##' @param x reference object of class 'opt'
        ##' @return Returns invisible x.
        opt_pop = function (x) {
            stopifnot(is.Optimiser(x))
            stopifnot(x$is_linked())
            .intrf.gopt__pop(x$id())
            rm(list=x$name(), envir=self$.opts)
            invisible(x)
        },
        ##' @description
        ##' Given a reference object of class 'opt', retrives and
        ##' returs the id of the associated \code{Optmiser}
        ##' (i.e. its position in the \code{OptS_} array.
        ##'
        ##' @param x reference object of class 'opt'
        ##' @return Returns the \code{Optmiser} id
        opt_id = function (x) {
            stopifnot(is.Optimiser(x))
            self$.opts[[x$name()]]
        },
        ##' @description
        ##' Checks that the \code{Optmiser} associated to a referece
        ##' object of class 'opt' exists.
        ##' 
        ##' @param x reference object of class 'opt'
        ##' @return Returns TRUE if the \code{Optmiser} exists, FALSE otherwise.
        opt_exists = function (x) {
            stopifnot(is.Optimiser(x))
            exists(x$name(), envir=self$.opts)
        }
    )
)

##' Object containing the corrent session.
.modello = ModelloSession$new()
