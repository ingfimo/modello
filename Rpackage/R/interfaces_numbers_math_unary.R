.intrf.number__abs <- function (id) {
    .Call(intrf_c__number__abs, as.integer(id),  PACKAGE="modello")
}

.intrf.number__exp <- function (id) {
    .Call(intrf_c__number__exp, as.integer(id),  PACKAGE="modello")
}

.intrf.number__log <- function (id) {
    .Call(intrf_c__number__log, as.integer(id),  PACKAGE="modello")
}

.intrf.number__sin <- function (id) {
    .Call(intrf_c__number__sin, as.integer(id),  PACKAGE="modello")
}

.intrf.number__cos <- function (id) {
    .Call(intrf_c__number__cos, as.integer(id),  PACKAGE="modello")
}

.intrf.number__tan <- function (id) {
    .Call(intrf_c__number__tan, as.integer(id),  PACKAGE="modello")
}

.intrf.number__sinh <- function (id) {
    .Call(intrf_c__number__sinh, as.integer(id),  PACKAGE="modello")
}

.intrf.number__cosh <- function (id) {
    .Call(intrf_c__number__cosh, as.integer(id),  PACKAGE="modello")
}

.intrf.number__tanh <- function (id) {
    .Call(intrf_c__number__tanh, as.integer(id),  PACKAGE="modello")
}
