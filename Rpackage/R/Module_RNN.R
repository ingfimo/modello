##' R6 class representing the recurrent layer of a Elmam or Jordan
##' recurrent neural network
##'
##' @author Filippo Monari
##' @export
ModuleRecUnit = R6Class(
    'ModuleRecUnit',
    inherit = Module,
    public = list(
        .tx = NULL,
        .Wh = NULL,
        .Wx = NULL,
        .B = NULL,
        .act = NULL,
        ##' @description
        ##' Initialisation method
        ##'
        ##' @param tx transposition flag. If > 0 op(x) = t(x) 
        ##' @param nh number of input from the previous time steps
        ##' @param nx number of input form the current time step
        ##' @param act activation function
        initialize = function (tx, nh, nx, act) {
            private$.tx = tx
            lh = sqrt(nh**-1)
            self$.Wh = private$par(number(matrix(runif(nh*nh, -lh, lh), nh, nh)))
            self$.Wx = private$par(number(matrix(runif(nh*nx, -lh, lh), nh, nx)))
            self$.B = private$par(number(runif(nh, -lh, lh)))
            self$.act = act
        },
        ##' @description
        ##' Performs:
        ##' act(Wx . op(x) + Wh . h + B)
        ##'
        ##' @param h \code{number} input from previous time steps
        ##' @param x \code{number} input from the current timestep
        op = function (h, x) {
            fc = gmmmult(ta=0, tb=self$.tx, A=self$.Wx, B=x)
            rec = gmmmult(ta=0, tb=0, A=self$.Wh, B=h)
            .op(self$.act)(fc + rec + self$.B)
        }
    )
)

##' R6 class representing an RNN.
##'
##' @author Filippo Monari
##' @export
ModuleRNN = R6Class(
    "ModuleRNN",
    inherit = Module,
    public = list(
        .H = NULL,
        .rus = NULL,
        .fc = NULL,
        ##' @description
        ##' Initialisation method
        ##'
        ##' @param tx transposition flag. If > 0 op(x) = t(x) 
        ##' @param nh integer vector of length equal to the hidden layes indicating the number of
        ##' hidden units for each layer
        ##' @param nx integer, number if inputs
        ##' @param acth activation function for the recurrent units
        ##' @param ny integer, number of outputs of the last fully connected layer
        ##' @param acty activation function for the last fully connected layer
        ##' @param par.h logical, true => the initial hidden state is treated as a paramter of the optimisation
        initialize = function (tx, nh, nx, acth, ny, acty, par_h=FALSE) {
            nx = c(nx, nh[-length(nh)])
            tx = c(tx, rep(0, length(nh)-1))
            self$.rus = lapply(1:length(nh), function (i) {
                private$par(ModuleRecUnit$new(tx[i], nh[i], nx[i], acth))
            })
            self$.fc = private$par(ModuleFC$new(0, nh[length(nh)], ny, acty))
            if (par_h) {
                self$.H = lapply(nh, function(n){
                    l = sqrt(n**-1)
                    private$par(number(matrix(runif(n, -l, l), n, 1), dx=TRUE))
                })
            } else {
                self$.H = lapply(nh, function(n){
                    private$val(number(matrix(0, n, 1), dx=FALSE))
                })
            }
        },
        ##' @description
        ##' Runs the calculation stored in the module
        ##'
        ##' @param X \code{Number}, module inputs
        ##' @param last if TRUE only the last element of the output sequence is returned
        op = function (X, last=TRUE) {
            X = lapply(1:dim(X)[X$rank()], function(i)X[i])
            h = self$.recur(list(self$.H), X)
            if (last) {
                h = h[[length(h)]]
                self$.fc$op(h[[length(h)]])
            } else {
                lapply(h, function(hh)self$.fc$op(hh[[length(hh)]]))
            }
        },
        ##' @description
        ##' Helper method for recursion
        ##'
        ##' @param h list of \code{Number}s, hidden states
        ##' @param X list of \code{Number}s, imputs
        .recur = function (h, X) {
            for (i in 1:length(X)) {
                l = length(h)
                h[[l + 1]] = private$.recur_inner(h[[l]], X[[i]], self$.rus)
            }
            h[-1]
        }
    ),
    private = list(
        .recur_inner = function (h, x, rus) { #hnew) {
            hnew = list()
            for (i in 1:length(rus)) {
                x = rus[[i]]$op(h[[i]], x)
                hnew[[i]] = x
            }
            hnew
        }
    )
)

##' R6 class representing the recurrent layer of a
##' LSTM network
##'
##' @author Filippo Monari
##' @export
ModuleLstmUnit = R6Class(
    "ModuleLstmUnit",
    inherit = Module,
    public = list(
        .tx = NULL,
        .nh = NULL,
        .act = NULL,
        .Wii = NULL,
        .Wif = NULL,
        .Wig = NULL,
        .Wio = NULL,
        .Whi = NULL,
        .Whf = NULL,
        .Whg = NULL,
        .Who = NULL,
        .Bii = NULL,
        .Bif = NULL,
        .Big = NULL,
        .Bio = NULL,
        .Bhi = NULL,
        .Bhf = NULL,
        .Bhg = NULL,
        .Bho = NULL,
        ##' @description
        ##' Initialisation method
        ##'
        ##' @param tx transposition flag. If > 0 op(x) = t(x) 
        ##' @param nh number of input from the previous time steps
        ##' @param nx number of input form the current time step
        ##' @param act activation function
        initialize = function (tx, nh, nx, act) {
            self$.tx = tx
            self$.nh = nh
            lh = sqrt(nh**-1)
            self$.Wii = private$par(number(matrix(runif(nx*nh, -lh, lh), nh, nx)))
            self$.Whi = private$par(number(matrix(runif(nh*nh, -lh, lh), nh, nh)))
            self$.Wif = private$par(number(matrix(runif(nx*nh, -lh, lh), nh, nx)))
            self$.Whf = private$par(number(matrix(runif(nh*nh, -lh, lh), nh, nh)))
            self$.Wig = private$par(number(matrix(runif(nx*nh, -lh, lh), nh, nx)))
            self$.Whg = private$par(number(matrix(runif(nh*nh, -lh, lh), nh, nh)))
            self$.Wio = private$par(number(matrix(runif(nx*nh, -lh, lh), nh, nx)))
            self$.Who = private$par(number(matrix(runif(nh*nh, -lh, lh), nh, nh)))
            self$.Bii = private$par(number(runif(nh, -lh, lh)))
            self$.Bhi = private$par(number(runif(nh, -lh, lh)))
            self$.Bif = private$par(number(runif(nh, -lh, lh)))
            self$.Bhf = private$par(number(runif(nh, -lh, lh)))
            self$.Big = private$par(number(runif(nh, -lh, lh)))
            self$.Bhg = private$par(number(runif(nh, -lh, lh)))
            self$.Bio = private$par(number(runif(nh, -lh, lh)))
            self$.Bho = private$par(number(runif(nh, -lh, lh)))
            self$.act = act
        },
        ##' @description
        ##' Performs:
        ##' input_gate = Wxi . op(x) + Whi . h + Bi
        ##' forget_gate = Wxf . op(x) + Whf . h + Bf
        ##' output_gate = Wxo . op(x) + Who . h + Bo
        ##' current_cell = act(Wxg . op(x) + Whg . h + Bg)
        ##' cell_sate = forget_gate * cell_previous + input_gate * cell_current
        ##' hidden_state = output_gate * act(cell_state)
        ##' Returns a list composed by hidde_state and cell_state
        ##'
        ##' @param h past hidden state 
        ##' @param g past cell state
        ##' @param x current input
        op = function (h_t0, c_t0, x_t) {
            i_t = sigmoid(gmmmult(ta=0, tb=self$.tx, A=self$.Wii, B=x_t) + self$.Bii +
                          gmmmult(ta=0, 0, A=self$.Whi, B=h_t0)$drop_dim() + self$.Bhi)
            f_t = sigmoid(gmmmult(ta=0, tb=self$.tx, A=self$.Wif, B=x_t) + self$.Bif +
                          gmmmult(ta=0, 0, A=self$.Whf, B=h_t0)$drop_dim() + self$.Bhf)
            g_t = self$.act(gmmmult(ta=0, tb=self$.tx, A=self$.Wig, B=x_t) + self$.Big +
                            gmmmult(ta=0, 0, A=self$.Whg, B=h_t0)$drop_dim() + self$.Bhg)
            o_t = sigmoid(gmmmult(ta=0, tb=self$.tx, A=self$.Wio, B=x_t) + self$.Bio +
                          gmmmult(ta=0, 0, A=self$.Who, B=h_t0)$drop_dim() + self$.Bho)
            c_t = c_t0 * f_t + i_t * g_t
            h_t = o_t * .op(self$.act)(c_t)
            list(h_t, c_t)
        }
    )
)

##' R6 class representing an LSTM.
##'
##' @author Filippo Monari
##' @export
ModuleLSTM = R6Class(
    "ModuleLSTM",
    inherit = ModuleRNN,
    public = list(
        .H = NULL,
        .rus = NULL,
        .fc = NULL,
        .par.h = NULL,
        .par.g = NULL,
        .stateful = NULL,
        ##' @description
        ##' Initialisation method
        ##'
        ##' @param tx transposition flag. If > 0 op(x) = t(x) 
        ##' @param nh integer vector of length equal to the hidden layes indicating the number of
        ##' hidden units for each layer
        ##' @param nx integer, number if inputs
        ##' @param acth activation function for the recurrent units
        ##' @param ny integer, number of outputs of the last fully connected layer
        ##' @param acty activation function for the last fully connected layer
        ##' @param par.h logical, true => the initial hidden state is treated as a paramter of the optimisation
        ##' @param par.g logical, true => the initial cell state is treated as a paramter of the optimisation
        initialize = function (tx, nh, nx, acth, ny, acty, stateful=FALSE) {
            nx = c(nx, nh[-length(nh)])
            tx = c(tx, rep(0, length(nh) - 1))
            self$.rus = lapply(1:length(nh), function(i) {
                private$par(ModuleLstmUnit$new(tx[i], nh[i], nx[i], acth))
            })
            self$.fc = private$par(ModuleFC$new(0, nh[length(nh)], ny, acty))
            H = lapply(nh, function(n){
                private$val(number(matrix(0, n, 1), dx=FALSE))
            })
            G = lapply(nh, function(n){
                private$val(number(matrix(0, n, 1), dx=FALSE))
            })
            self$.H = lapply(1:length(nh), function(i)list(H[[i]], G[[i]]))
            self$.stateful = stateful
        },
        ##' @description
        ##' Runs the calculation stored in the module
        ##'
        ##' @param X \code{Number}, module inputs
        ##' @param last if TRUE only the last element of the output sequence is returned
        op = function (X, last=TRUE) {
            ## n x l x s
            X = lapply(1:dim(X)[X$rank()], function(i)X[i])
            h = self$.recur(list(self$.H), X)
            if (self$.stateful) self$.update_state(h)
            if (last) {
                h = h[[length(h)]]
                self$.fc$op(h[[length(h)]][[1]])
            } else {
                lapply(h, function(hh)self$.fc$op(hh[[length(hh)]][[1]]))
            }
        },
        reset_state = function () {
            for (s in self$.H) {
                s[[1]]$v = 0
                s[[2]]$v = 0
            }
        },
        .update_state = function (states) {
            update <- function () {
                newstate = states[[1]]
                for (i in 1:length(self$.H)) {
                    self$.H[[i]][[1]] %<=% newstate[[i]][[1]]
                    self$.H[[i]][[2]] %<=% newstate[[i]][[2]]
                }
            }
            .modello$G()$.posts(update)
        }
    ),
    private = list(
        .recur_inner = function (hg, x, rus) {
            hgnew = list()
            for (i in 1:length(rus)) {
                x = rus[[i]]$op(hg[[i]][[1]], hg[[i]][[2]], x)
                hgnew[[i]] = x
                x = x[[1]]
            }
            hgnew
        }
    )
)

##' R6 class representing an GP recurrent unit.
##'
##' @author Filippo Monari
##' @export
ModuleGPUnit = R6Class(
    "ModuleGPUnit",
    inherit = Module,
    public = list(
        .kernel = NULL,
        .rid = NULL,
        ##' @description
        ##' Initialisation method
        ##'
        ##' @param kernel kernel function 
        ##' @param rid constant matrix to add to the kernel matrix, \code{Number}
        initialize = function (kernel, rid) {
            self$.kernel = kernel
            self$.rid = rid
        },
        ##' @description
        ##' Operator method - It calculates the normal posteriro ditribution for the kernel
        ##'
        ##' @param hp list of \code{Number}s, kernel hyper parameters
        ##' @param E11 \code{Number}, prior covariance matrix
        ##' @param y11 \code{Number}, vector with prior observations
        ##' @param mu11 scalar \code{Number}, prior mean
        ##' @param x1 prior feature matrix, \code{Number}
        ##' @param x2 new feaure matrix, \code{Number}
        op = function (hp, E11, y11, mu11, x1, x2) {
            E21 = self$.kernel(x1, x2, hp)
            E22 = self$.kernel(x2, x2, hp) + self$.rid
            ans = mvnorm_posterior(E11, E21, E22, y11, mu11, TRUE)
            if (!is.null(a)) ans = list(mu = ans$mu * a, E = ans$E * a)
            ans#@todo aplitude whre??
        }
    )
)
    



##' R6 class representing a Recurrent Gaussian Process Network.
##'
##' @author Filippo Monari
##' @export
ModuleRGPN = R6Class(
    "ModuleRGPN",
    inherit = Module,
    public = list(
        ##' @description
        ##' Initialisation method
        ##'
        ##' @param rec \code{Module}, recurrent module calculating the kernel hyper paramters
        ##' @param kernel function implementing the kernel
        ##' @param x0 \code{Number}, initial state for the feature matrix
        ##' @param y0 \code{Number}, initial state for the observation vector
        ##' @param mu0 \code{Number}, initial state for the mean vector
        ##' @param rid \code{Number}, constant matrix to add to the kernel matrix
        initialize = function (rec, kernel, x0, y0, mu0, rid) {
            private$.rec = rec
            private$.H$x0 = x0
            private$.H$y0 = y0
            private$.H$mu0 = mu0
            private$.$H$E0 = number(diag(1, length(y0)), dx=FALSE)
            private$.gp =module.GPUnit$new(kernel, rid)
        },
        ##' @description
        ##' Runs the calculation stored in the module
        ##'
        ##' @param X list of \code{Number}s, hyper paramters' model inputs
        ##' @param Y list of \code{Number}s, past obeservations
        ##' @param Z list of \code{Number}s, kernel features
        op = function (X, Y, Z) {
            g = self$.recur(X, Y, Z)
            g[[length(g)]]
        },
        ##' @description
        ##' Helper method for recursion
        ##'
        ##' @param X list of \code{Number}s, hyper paramters' model inputs
        ##' @param Y list of \code{Number}s, past obeservations
        ##' @param Z list of \code{Number}s, kernel features
        .recur = function (X, Y, Z) {
            hp = private$.rec$op(X, last=FALSE)
            private$.recur_gp(hp, private$.H$E0, c(private$.H$y0, Y), private$.H$mu0,
                              private$.H$x0, Z, list())
        }
    ),
    private = list(
        .rec = NULL,
        .H = list(),
        .gp = NULL,
        .recur_gp = function (hp, E11, y11, mu11, x1, x2, G) {
            if (length(hp) == 0) {
                G
            } else {
                g = private$.gp$op(hp[[1]], E11, y11[[1]], mu11, x1, x2[[1]])
                G[[length(G) + 1]] = g
                private$.recur_gp(hp[-1], g$E, y11[-1], g$mu, x2[[1]], x2[-1], G)
            }
        }
    )
)
