import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import numpy as np
import time

from torchvision import datasets, transforms
from torch.optim.lr_scheduler import StepLR


def pars_to_csv(mdl):
    n = 1
    for p in mdl.parameters():
        np.savetxt(f"p{n}.csv", p.detach().numpy(), delimiter=",")
        n += 1
    return True

torch.set_default_dtype(torch.float64)

# 1. Build a computation graph
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(784, 256)
        self.fc2 = nn.Linear(256, 128)
        self.fc3 = nn.Linear(128, 10)

    def forward(self, x):
        x = self.fc1(x)
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return F.softmax(x)


transform = transforms.Compose(
    [transforms.ToTensor(), transforms.Normalize((0.1307,), (0.3081,))]
)

DS_train = datasets.MNIST("./data", train=True, download=True, transform=transform)
DS_test = datasets.MNIST("./data", train=False, download=True, transform=transform)

DL_train = torch.utils.data.DataLoader(DS_train, batch_size=128)
DL_test = torch.utils.data.DataLoader(DS_test, batch_size=128)

net = Net()

lossfn = nn.CrossEntropyLoss()

opt = optim.Adam(net.parameters())

for i in range(30):
    LOSS = 0
    ACC = 0
    n = 0
    t0 = time.time()
    for inputs, target in DL_train:
        n += 1
        net.zero_grad()
        output = net(inputs.reshape((len(inputs), 28 * 28)))
        loss = lossfn(output, target)
        LOSS += loss.item()
        ACC += torch.sum(torch.argmax(output, dim=1) == target) / 128
        loss.backward()
        opt.step()
    print(f"[{n}] loss: {LOSS / n}; accuracy {ACC / n}; et {time.time()-t0}")

