library(modello)
library(keras)
library(data.table)

modello_init(dtyp=.DP())
set.seed(1001)
print(modello_dtyp())

mnist = dataset_mnist()
x_train = mnist$train$x
dim(x_train) = c(dim(x_train)[1], prod(dim(x_train)[-1]))
x_train = t(x_train) / 255
y_train = mnist$train$y
y_train = sapply(y_train, function(x){y = rep(0, 10); y[x+1] = 1; y})
x_test = mnist$test$x
dim(x_test) = c(dim(x_test)[1], prod(dim(x_test)[-1]))
x_test = t(x_test) / 255
y_test = mnist$test$y
y_test = sapply(y_test, function(x){y = rep(0, 10); y[x+1] = 1; y})

traini = sample(1:ncol(x_train), ncol(x_train))
DL_x_train = number(x_train, dx=FALSE) 
DL_y_train = number(y_train, dx=FALSE) 

Xtrain = feeding_number(DL_x_train, 1:ncol(x_train), c(28, 28, 1), 128)
Ytrain = feeding_number(DL_y_train, 1:ncol(x_train), 10, 128)

DL_x_test = number(x_test, dx=FALSE) 
DL_y_test = number(y_test, dx=FALSE) 

Xtest = feeding_number(DL_x_test, 1:ncol(x_test), c(28, 28, 1), 100)
Ytest = feeding_number(DL_y_test, 1:ncol(x_test), 10, 100)

MnistModelCnn = R6Class(
    "MnistModel",
    inherit = Module,
    public = list(
        .seq = NULL,
        initialize = function () {
            self$.seq = private$par(c(
                ConvUnit$new(c(3, 3), 1, 32),
                MaxPoolUnit$new(c(2, 2)),
                function(x)x$reshape(c(25*25*32, x$dim()[x$rank()])),
                ModuleFC$new(0, 25*25*32, 100, relu),
                ModuleFC$new(0, 100, 10, NULL)
            ))
        },
        op = function (X) {
            softmax(self$.seq$op(X), 2)
        }
    )
)

mdl = MnistModelCnn$new()

opt = adam_optimiser(mdl$pars())

Gtrain = graph_open()
feeder_train = feed(Xtrain, Ytrain)
Yhtrain = mdl$op(Xtrain)
Jtrain = crossentropy(Ytrain, Yhtrain) 
graph_close()

Gtest = graph_open(wdx=FALSE)
feeder_test = feed(Xtest, Ytest)
Yhtest = mdl$op(Xtest)
Jtest = crossentropy(Ytest, Yhtest) 
graph_close()

print("training")
T0 = Sys.time()
for (i in 1:20) {
    t0 = Sys.time()
    feeder_train$shuffle()
    feeder_test$shuffle()
    loss_train = opt$step(Gtrain, Jtrain, 0.001, 0.9, 0.999, feeder_train$length())
    loss_test = 0
    ACCtest = 0
    for (j in 1:feeder_test$length()) {
        Gtest$op()
        loss_test = loss_test + Jtest$v 
        ACCtest = ACCtest + mean(apply(Ytest$v, 2, which.max) == apply(Yhtest$v, 2, which.max))
    }
    ACCtest = ACCtest / j
    dt = difftime(Sys.time(), t0, units="secs")
    cat(paste0("epoch: ", i,
               "; train loss: ", loss_train,
               "; test loss: ", loss_test,
               "; test acc: ", ACCtest,
               "; et (s): ", dt), sep="\n")
}
mdl$save("mdl.rds")
ELAPSED = Sys.time() - T0

## mdl = module_from_RDS("mdl.rds")
