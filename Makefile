SHELL := /bin/bash

SRC := ./src
BLD := ./bld
RPACKAGE := ./Rpackage

DIRS := $(shell find ./src -mindepth 1 -maxdepth 10 -type d)

RMAKEVAR := ./src/R_Makevars

RSRC := $(shell find ./src | grep R_ | grep -v R_Makevar)

XP := $(shell find ./src | grep xp)

OTHERS := $(shell find ./src -type f | grep -v xp | grep -v R_)

rpackage_install: rpackage
	R CMD INSTALL modello_0.2.2.tar.gz

rpackage: _bld _xp _others _rsrc _rmakevar
	rm -f modello_0.2.2.tar.gz
	mv ./bld/src ${RPACKAGE}
	R CMD build Rpackage
	rm -rf ./bld

rpackage_clean:
	rm -rf ${RPACKAGE}/src
	rm -rf ./bld

document: _xp _rsrc _others
	doxygen Doxyfile
	rm -rf ./bld

_rmakevar:
	cp ${RMAKEVAR} ${BLD}/src/Makevars

_rsrc: _bld
	for ff in ${RSRC}; do \
		cp $$ff ${BLD}/$$ff; \
	done


_others: _bld
	for ff in ${OTHERS}; do \
		cp $$ff ${BLD}/$$ff; \
	done

_xp: _bld
	for ff in ${XP}; do \
		echo "##> populating $$ff"; \
		sed "s/xp_/sp_/g" $$ff > ${BLD}/$${ff/xp/sp}; \
		sed "s/xp_/dp_/g" $$ff > ${BLD}/$${ff/xp/dp}; \
	done

_bld:
	mkdir -p ./bld
	for dd in ${DIRS}; do \
		echo "##> Making ${BLD}/$$dd"; \
		mkdir -p ${BLD}/$$dd; \
	done
