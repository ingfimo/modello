    ! subroutine ginv (A, lnD, info)	!R
  !   !DOC
  !   !Calculates the general inverse and an approximation of the log determinant of a mtrix
  !   !approximates the inverse of an singular (or computationally singular)
  !   !matrix through eigen-decomposition (Moore - Penrose pseudoinverse)
  !   !VARIABLES
  !   implicit none
  !   !in/out	
  !   double precision, intent (inout) :: A(:,:), lnD			!(m,m)
  !   integer, intent(out) :: info
  !   !auxiliary
  !   double precision :: U(size(A, 1),size(A, 2))	!eigen vector matrix
  !   double precision :: VT(size(A,2),size(A,1))
  !   double precision :: D(size(A,1)), DD(size(A,1),size(A,1)) !eigen values
  !   double precision :: tol
  !   !counters
  !   integer :: i
  !   !EXECUTION
  !   ! if (.not. isSquare(A)) then
  !   !    call raiseError('ginvMat,matrix is not square,', 1)
  !   !    return
  !   ! else if (size(A,1) /= size(B,1) .or. size(A,2) /= size(B,2)) then
  !   !    call raiseError('ginvMat,matrices are not compatible', 2)
  !   !    return
  !   ! end if
  !   call svd(U, D, VT, A, info)
  !   lnD = 0; tol = eps_dbl_ * D(1)
  !   do i = 1, size(D)
  !      if (D(i) < tol) then
  !         D(i) = 0
  !      else
  !         D(i) = 1/D(i)
  !      end if
  !   end do
  !   call diag(DD, D)
  !   A = matmul(transpose(VT), matmul(DD, transpose(U)))	!@toDo: this can be done without creating diagonal matrix
  !   lnD = sum(log(D))
  ! end subroutine ginv

  ! subroutine svd (U, D, VT, A, info)
  !   implicit none
  !   double precision, intent (out) :: U(:,:), D(:), VT(:,:)
  !   integer, intent(out) :: info
  !   double precision, intent (in) :: A(:,:)

  !   double precision, allocatable :: work(:)
  !   double precision :: D_dm(1,size(D))
  !   integer, parameter :: lwmax = 10**3
  !   integer :: lwork

  !   allocate(work(lwmax))
  !   lwork = -1
  !   !DGESVD(JOBU, JOBVT, M, N, A, LDA, S, U, LDU, VT, LDVT, WORK, LWORK, INFO)	 
  !   call DGESVD('A', 'A', size(A,1), size(A,2), A, size(A,1), D, U, size(A,1), VT, size(A,1), &
  !        work, lwork, info)
  !   lwork = int(work(1))
  !   if (lwork > lwmax) then
  !      deallocate(work)
  !      allocate(work(lwork))
  !   end if
  !   call DGESVD('A', 'A', size(A,1), size(A,2), A, size(A,1), D, U, size(A,1), VT, size(A,1), & 
  !        work, lwork, info)
  !   D_dm(1,:) = D
  ! end subroutine svd

  ! subroutine wrp_dposv (uplo, A, B, lnD, info)
  !   !_DOC_
  !   !Wrap to LAPCK subrotuine dposv.
  !   !_VARIABLES_
  !   implicit none
  !   !in/out
  !   character(len = 1), intent(in) :: uplo !L for lower triangular, U for upper triangular 
  !   double precision, intent(inout) :: B(:,:) !(in) right side of the sysntem of equations / (out) solution
  !   double precision, intent(in) :: A(:,:) !left side of the systemof equations
  !   double precision, intent(out) :: lnD   !log det of A
  !   integer, intent(out) :: info           !error flag
  !   !local
  !   double precision :: A1(size(A, 1),size(A, 2)) !to copy A
  !   !INTERFACES
  !   interface
  !      subroutine DPOSV (UPLO, N, NRHS, A, LDA, B, LDB, INFO)
  !        character(len = 1), intent(in) :: UPLO
  !        integer, intent(in) :: N, NRHS, LDA, LDB
  !        integer, intent(out) :: INFO
  !        double precision, intent(inout) :: A(LDA,N), B(LDB,NRHS)
  !      end subroutine DPOSV
  !   end interface
  !   !MAIN
  !   A1 = A
  !   call DPOSV(uplo, size(A, 1), size(B, 2), A1, size(A, 1), B, size(A, 1), info)
  !   lnD = 2 * lntr(A1)
  ! end subroutine wrp_dposv

  ! subroutine wrp_dsymm (side, uplo, alpha, A, B, beta, C)
  !   !_DOC_
  !   !Wrap to BLAS DSYMM subroutine.
  !   !DSYMM performs one of the matrix-matrix operations:
  !   !C := alpha*A*B + beta*C,
  !   !or
  !   !C := alpha*B*A + beta*C,
  !   !where alpha and beta are scalars,  A is a symmetric matrix and  B and
  !   !C are  m by n matrices.
  !   implicit none
  !   integer, intent(in) :: side	!0 => C := alpha*A*B + beta*C || 1 => C := alpha*B*A + beta*C
  !   integer, intent(in) :: uplo	!0 => Only the upper triangular part of A is to be referenced || 1 => Only the lower triangular part of A is to be referenced.
  !   double precision, intent(in) :: alpha, A(:,:), B(:,:), beta
  !   double precision, intent(inout) :: C(:,:)	
  !   integer :: M, N, LDA, LDB, LDC
  !   character(len=1) :: sd, ul
  !   external DSYMM
  !   !MAIN					
  !   M = size(C, 1)
  !   N = size(C, 2)
  !   LDA = size(A, 1)
  !   LDB = size(B, 1)
  !   LDC = size(C, 1)
  !   if (M /= LDB) print *, 'error1'
  !   if (N /= size(B, 2)) print *, 'error2'
  !   if (side > 0) then
  !      if (N /= size(A, 1) .or. N /= size(A,2)) print *, 'error3'
  !      sd = 'R'
  !   else
  !      if (M /= size(A, 1) .or. M /= size(A,2)) print *, 'error4'
  !      sd = 'L'
  !   end if
  !   if (uplo > 0) then
  !      ul = 'L'
  !   else
  !      ul = 'U'
  !   end if
  !   call DSYMM(sd, ul, M, N, alpha, A, LDA, B, LDB, beta, C, LDC)
  ! end subroutine wrp_dsymm
  
  ! subroutine wrp_dsyrk (uplo, trans, alpha, A, beta, C)
  !   !_DOC_
  !   !DSYRK  performs one of the symmetric rank k operations
  !   !C := alpha*A*A**T + beta*C,
  !   !or
  !   !C := alpha*A**T*A + beta*C,
  !   !where  alpha and beta  are scalars, C is an  n by n  symmetric matrix
  !   !and  A  is an  n by k  matrix in the first case and a  k by n  matrix
  !   !in the second case.
  !   !_VARIABLES_
  !   implicit none
  !   !in/out
  !   integer, intent(in) :: uplo !0 => 'U', 1 => 'L'
  !   integer, intent(in) :: trans	!0 => C := alpha*A*A**T + beta*C || 1 =>  C := alpha*A**T*A + beta*C 
  !   double precision, intent(in) :: alpha, A(:,:), beta
  !   double precision, intent(inout) :: C(:,:)
  !   !local
  !   integer :: N, K, LDA, LDC
  !   character(len=1) :: ul, trn
  !   !MAIN
  !   N  = size(C, 1)
  !   if (trans == 0) then
  !      K = size(A, 2)
  !      LDA = size(A, 1)
  !      trn = 'N'
  !   else
  !      K = size(A, 1)
  !      LDA = size(A, 2)
  !      trn = 'T'
  !   end if
  !   if (uplo > 0) then
  !      ul = 'L'
  !   else
  !      ul = 'U'
  !   end if
  !   LDC = size(C, 1)
  !   call DSYRK(ul, trn, N, K, alpha, A, LDA, beta, C, LDC)
  ! end subroutine wrp_dsyrk

  ! !* CALCULATION OF COMMON MATRIX ENTITIES
  
  ! !* MATRIX DECOMPOSITION
  
  ! subroutine wrp_dpotrf (uplo, A, info)
  !   !_DOC_
  !   !Wrao to the LAPACK subroutine DPOTRF (Cholesky decomposition)
  !   !_VARIABLES_
  !   implicit none
  !   !in/out
  !   character(len = 1), intent(in) :: uplo		!L for lower triangular, U for upper triangular
  !   double precision, intent(inout) :: A(:,:)	!Matrix to decompose
  !   integer, intent(out) :: info 				!error flag
  !   integer :: i, j
  !   !INTERFACES
  !   interface
  !      subroutine DPOTRF (UPLO, N, A, LDA, INFO)
  !        character(len=1), intent(in) :: UPLO
  !        integer, intent(in) :: N, LDA
  !        integer, intent(out) :: INFO
  !        double precision, intent(inout) :: A(LDA,N)
  !      end subroutine DPOTRF
  !   end interface
  !   !MAIN
  !   call DPOTRF(uplo, size(A, 1), A, size(A, 1), info)
  !   if (uplo == 'L') then
  !      do i = 1, size(A, 1)
  !         do j = i, size(A, 2)
  !            if (i /= j) A(i,j) = 0
  !         end do
  !      end do
  !   else if (uplo == 'U') then
  !      do i = 1, size(A, 1)
  !         do j = 1, i
  !            if (i/=j) A(i,j) = 0
  !         end do
  !      end do
  !   end if
  ! end subroutine wrp_dpotrf
  

  
  ! !* STATISTICAL FUNCTIONS
  
  ! double precision function mean (A)
  !   !_DOC_
  !   !Mean of a vector
  !   double precision, intent (in) :: A(:)
  !   mean = sum(A) / size(A)
  ! end function mean
  
  ! double precision function var (A)
  !   !_DOC_
  !   !Variance of a vector
  !   !_VARIABLES_
  !   implicit none
  !   !in/out
  !   double precision, intent (in) :: A(:)
  !   !MAIN
  !   var = sum((A - mean(A))**2) / (size(A) - 1)		
  ! end function var
  
  ! double precision function sd (A)
  !   !_DOC_
  !   !Standard deviation of a vector
  !   !_VARIABLES_
  !   implicit none
  !   !in/out
  !   double precision, intent (in) :: A(:)
  !   !MAIN
  !   sd = sqrt(var(A))
  ! end function sd
  
  ! ! double precision function quantile (x, p, srt)
  ! !   !_DOC_
  ! !   !Calculates the q quantile of the vector x
  ! !   !srt is logical: if .true. the vector x is sorted.
  ! !   !This is done so that in case of recursive calls of the function the vector x can be sorted only once.
  ! !   !_VARIABLES_
  ! !   implicit none
  ! !   !in/out
  ! !   double precision, intent(inout) :: x(:)
  ! !   double precision, intent(in) :: p
  ! !   logical, intent(in) :: srt
  ! !   !local
  ! !   double precision :: i
  ! !   integer :: l, h
  ! !   !MAIN
  ! !   if (srt) call sort(x)
  ! !   i = 1 + (size(x) - 1) * p
  ! !   l = floor(i)
  ! !   h = ceiling(i)
  ! !   quantile = x(l) * (h - i) + x(h) * (i - l) 
  ! ! end function quantile
  
  ! !** PROBABILITY DENSITY FUCNTIONS
  ! !Due infitnity handling in the calculations the probability density function from R have been wraped so to return
  ! !a predefined constant intesad of infinity.
  ! !At the moment this seems to be the most reasonalble thing to do in the future I'll think for a better solution.
  
  ! !* LIKELIHOOD FUNCTIONS
  
  ! double precision function fdiff1 (fn, z, i)
  !   implicit none
  !   integer, intent(in) :: i
  !   double precision, intent(in) :: z(:)
  !   procedure(dpfun_1dp) :: fn
  !   double precision :: h(size(z))
  !   h = 0
  !   h(i) = tiny_
  !   fdiff1 = (fn(z + h) - fn(z)) / h(i)
  ! end function fdiff1
  
  ! double precision function bdiff1 (fn, z, i)
  !   implicit none
  !   integer, intent(in) :: i
  !   double precision, intent(in) :: z(:)
  !   procedure(dpfun_1dp) :: fn
  !   double precision :: h(size(z))
  !   h = 0
  !   h(i) = tiny_
  !   bdiff1 = (fn(z) - fn(z - h)) / h(i)
  ! end function bdiff1
  
  ! double precision function rdiff1 (fn, z, i)
  !   implicit none
  !   integer, intent(in) :: i
  !   double precision, intent(in) :: z(:)
  !   procedure(dpfun_1dp) :: fn
  !   if (c_runif(dble(0), dble(1)) < 0.5) then
  !      rdiff1 = fdiff1(fn, z, i)
  !   else
  !      rdiff1 = bdiff1(fn, z, i)
  !   end if
  ! end function rdiff1
  

  
  ! double precision function cdiff2 (fn, z, i, j)
  !   implicit none
  !   integer, intent (in) :: i, j
  !   procedure(dpfun_1dp) :: fn
  !   double precision, intent (in) :: z(:)
  !   double precision :: h(size(z))
  !   h = 0
  !   h(j) = tiny_
  !   cdiff2 = (cdiff1(fn, z + h, i) - cdiff1(fn, z - h, i)) / (2 * h(j)) 
  ! end function cdiff2
  
  ! subroutine grad (d, fn, z, dfn1)
  !   !DOC
  !   !Gradient approximation by central difference
  !   implicit none
  !   double precision, intent(out) :: d(:)
  !   double precision, intent(in) :: z(:) 
  !   procedure(dpfun_1dp) :: fn
  !   integer :: i
  
  !   interface
  !      double precision function dfn1 (fn, z, i)
  !        procedure(dpfun_1dp) :: fn
  !        double precision, intent(in) :: z(:)
  !        integer, intent(in) :: i
  !      end function dfn1
  !   end interface
  
  !   !_MAIN_
  !   d = 0
  !   do i = 1, size(z)
  !      d(i) = dfn1(fn, z, i) 
  !   end do
  ! end subroutine grad
  
  ! subroutine gradu (d, fn, z, dfn1)
  !    implicit none
  !   double precision, intent(out) :: d(:,:)
  !   double precision, intent(in) :: z(:)
  !   procedure(dpfun_1dp) :: fn
  !   integer :: i
  !   interface
  !      double precision function dfn1 (fn, z, i)
  !        procedure(dpfun_1dp) :: fn
  !        double precision, intent(in) :: z(:)
  !        integer, intent(in) :: i
  !      end function dfn1
  !   end interface
  !   call grad(d(:,1), fn, z, dfn1)
  !   d = d / L2norm(d)
  ! end subroutine gradu
  
  ! subroutine hessian (H, fn, z, dfn2)
  !   implicit none
  !   double precision, intent(in) :: z(:)
  !   procedure(dpfun_1dp) :: fn
  !   double precision, intent(out) :: H(size(z),size(z))
  !   integer :: i, j
  
  !   interface
  !      double precision function dfn2 (fn, z, i, j)
  !        procedure(dpfun_1dp) :: fn
  !        double precision, intent(in) :: z(:)
  !        integer, intent(in) :: i, j
  !      end function dfn2
  !   end interface
  
  
  !   do i = 1, size(z)
  !      do j = i, size(z)
  !         H(i,j) = dfn2(fn, z, i, j)
  !         if (i /= j) H(j,i) = H(i,j)
  !      end do
  !   end do
  
  ! end subroutine hessian
  
  

  ! !* VARIABLE TRANSFORMATION


  
  ! !** BETWEEN 0 AND 1
  ! !_DOC_

  ! double precision function Rto01_1 (x)
  !   implicit none
  !   double precision, intent(in) :: x

  !   Rto01_1 = (tanh(x) + 1) / 2

  ! end function Rto01_1

  ! double precision function Rfrom01_1 (x)
  !   implicit none
  !   double precision, intent(in) :: x

  !   Rfrom01_1 = atanh(2 * x - 1)

  ! end function Rfrom01_1

  ! double precision function Rto01_2 (x)
  !   implicit none
  !   double precision, intent(in) :: x

  !   Rto01_2 = 1 / (1 + exp(-x))

  ! end function Rto01_2

  ! double precision function Rfrom01_2 (x)
  !   implicit none
  !   double precision, intent(in) :: x

  !   Rfrom01_2 = log(x) - log(1 - x)

  ! end function Rfrom01_2
 
