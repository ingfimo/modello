subroutine intrf_f__template(spfn, dpfn)
  implicit none
  procedure(sbr0_) :: spfn
  procedure(sbr0_) :: dpfn
  select case(DTYP_)
  case (sp_)
     call spfn
  case (dp_)
     call dpfn
  case default
     call raise_error("DTYP_", err_unknwnVal_)
  end select
end subroutine intrf_f__template

