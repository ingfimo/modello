!> @addtogroup api__numbers_math_reductions_
!! @{

!> Interface for Sum
!! @param[in] idx c_int, id of the input 'number'
!! @param[out] idout c_int, id of the reduced 'number'
subroutine intrf_f__number__sum (idx, k, idout) bind(C, name='intrf_f__number__sum_')
  implicit none
  integer(kind=c_int), intent(in) :: idx, k
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__sum', mod_interfaces_name_, private_sum)
contains
  subroutine private_sum
    if (k > 0) then
       call intrf_f__template(private_sp1, private_dp1)
    else
       call intrf_f__template(private_sp2, private_dp2)
    end if
  end subroutine private_sum
  subroutine private_sp1
    call intrf_f__ans_id(number__sum(nnn(idx, real(0)), k), idout)
  end subroutine private_sp1
  subroutine private_dp1
    call intrf_f__ans_id(number__sum(nnn(idx, dble(0)), k), idout)
  end subroutine private_dp1
  subroutine private_sp2
    call intrf_f__ans_id(number__sum(nnn(idx, real(0))), idout)
  end subroutine private_sp2
  subroutine private_dp2
    call intrf_f__ans_id(number__sum(nnn(idx, dble(0))), idout)
  end subroutine private_dp2
end subroutine intrf_f__number__sum

!> Product operator
!!
!! @param[in] idx inout number identifier
!! @param[in] k axis along which perform the reduction. If < 0 all axis are reduced.
!! @param[out] idout output number
subroutine intrf_f__number__product (idx, k, idout) bind(C, name='intrf_f__number__product_')
  implicit none
  integer(kind=c_int), intent(in) :: idx, k
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__product', mod_interfaces_name_, private_product)
contains
  subroutine private_product
    if (k > 0) then
       call intrf_f__template(private_sp1, private_dp1)
    else
       call intrf_f__template(private_sp2, private_dp2)
    end if
  end subroutine private_product
  subroutine private_sp1
    call intrf_f__ans_id(number__product(nnn(idx, real(0)), k), idout)
  end subroutine private_sp1
  subroutine private_dp1
    call intrf_f__ans_id(number__product(nnn(idx, dble(0)), k), idout)
  end subroutine private_dp1
  subroutine private_sp2
    call intrf_f__ans_id(number__product(nnn(idx, real(0))), idout)
  end subroutine private_sp2
  subroutine private_dp2
    call intrf_f__ans_id(number__product(nnn(idx, dble(0))), idout)
  end subroutine private_dp2
end subroutine intrf_f__number__product

!> Interface for Sum Of Squares
!! @param[in] idx c_int, id of the input 'number'
!! @param[out] idout c_int, id of the reduced 'number'
subroutine intrf_f__number__ssq (idx, k, idout) bind(C, name='intrf_f__number__ssq_')
  implicit none
  integer(kind=c_int), intent(in) :: idx, k
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__ssq', mod_interfaces_name_, private_ssq)
contains
  subroutine private_ssq
    if (k > 0) then
       call intrf_f__template(private_sp1, private_dp1)
    else
       call intrf_f__template(private_sp2, private_dp2)
    end if
  end subroutine private_ssq
  subroutine private_sp1
    call intrf_f__ans_id(number__ssq(nnn(idx, real(0)), k), idout)
  end subroutine private_sp1
  subroutine private_dp1
    call intrf_f__ans_id(number__ssq(nnn(idx, dble(0)), k), idout)
  end subroutine private_dp1
  subroutine private_sp2
    call intrf_f__ans_id(number__ssq(nnn(idx, real(0))), idout)
  end subroutine private_sp2
  subroutine private_dp2
    call intrf_f__ans_id(number__ssq(nnn(idx, dble(0))), idout)
  end subroutine private_dp2
end subroutine intrf_f__number__ssq

!> @}
