subroutine intrf_f__number__conv (id1, id2, idout, stride, n1, dilation, n2, pad, n3) &
     bind(C, name="intrf_f__number__conv_")
  implicit none
  integer(kind=c_int), intent(in) :: n1, n2, n3
  integer(kind=c_int), intent(in) :: id1, id2, stride(n1), dilation(n2), pad(n3)
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__conv1d", mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    real(kind=sp_) :: dtyp
    integer :: d
    d = get_rank(nnn(id1, dtyp)) - 2
    select case (d)
    case (1)
       call intrf_f__ans_id(number__conv1d(nnn(id1, dtyp), nnn(id2, dtyp), &
            stride, dilation, pad), idout)
    case (2)
       call intrf_f__ans_id(number__conv2d(nnn(id1, dtyp), nnn(id2, dtyp), &
            stride, dilation, pad), idout)
    case (3)
       call intrf_f__ans_id(number__conv3d(nnn(id1, dtyp), nnn(id2, dtyp), &
            stride, dilation, pad), idout)
    case default
       call raise_error("d", err_unknwnVal_)
    end select
  end subroutine private_sp
  subroutine private_dp
    real(kind=dp_) :: dtyp
    integer :: d
    d = get_rank(nnn(id1, dtyp)) - 2
    select case (d)
    case (1)
       call intrf_f__ans_id(number__conv1d(nnn(id1, dtyp), nnn(id2, dtyp), &
            stride, dilation, pad), idout)
    case (2)
       call intrf_f__ans_id(number__conv2d(nnn(id1, dtyp), nnn(id2, dtyp), &
            stride, dilation, pad), idout)
    case (3)
       call intrf_f__ans_id(number__conv3d(nnn(id1, dtyp), nnn(id2, dtyp), &
            stride, dilation, pad), idout)
    case default
       call raise_error("d", err_unknwnVal_)
    end select
  end subroutine private_dp
end subroutine intrf_f__number__conv


subroutine intrf_f__number__maxpool (id1, idout, kshp, n1, stride, n2, dilation, n3, pad, n4) &
     bind(C, name="intrf_f__number__maxpool_")
  implicit none
  integer(kind=c_int), intent(in) :: n1, n2, n3, n4
  integer(kind=c_int), intent(in) :: id1, kshp(n1), stride(n2), dilation(n3), pad(n4)
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__maxpool", mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    real(kind=sp_) :: dtyp
    integer :: d
    d = get_rank(nnn(id1, dtyp)) - 2
    select case (d)
    case (1)
       call intrf_f__ans_id(number__maxpool1d(nnn(id1, dtyp), kshp, &
            stride, dilation, pad), idout)
    case (2)
       call intrf_f__ans_id(number__maxpool2d(nnn(id1, dtyp), kshp, &
            stride, dilation, pad), idout)
    case (3)
       call intrf_f__ans_id(number__maxpool3d(nnn(id1, dtyp), kshp, &
            stride, dilation, pad), idout)
    case default
       call raise_error("d", err_unknwnVal_)
    end select
  end subroutine private_sp
  subroutine private_dp
    real(kind=dp_) :: dtyp
    integer :: d
    d = get_rank(nnn(id1, dtyp)) - 2
    select case (d)
    case (1)
       call intrf_f__ans_id(number__maxpool1d(nnn(id1, dtyp), kshp, &
            stride, dilation, pad), idout)
    case (2)
       call intrf_f__ans_id(number__maxpool2d(nnn(id1, dtyp), kshp, &
            stride, dilation, pad), idout)
    case (3)
       call intrf_f__ans_id(number__maxpool3d(nnn(id1, dtyp), kshp, &
            stride, dilation, pad), idout)
    case default
       call raise_error("d", err_unknwnVal_)
    end select
  end subroutine private_dp
end subroutine intrf_f__number__maxpool
