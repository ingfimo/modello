!> @addtogroup api__numbers_math_kernels_
!! @{

!> Interface for ksqexp
!! @param[in] idx1 c_int, id of the first feature vector
!! @param[in] idx2 c_int, id of the second feature vector
!! @param[in] ids c_int, id of the amplitude parameter 'number'
!! @param[in] idb c_int, id of the rate parameter 'number'
!! @param[out] idout c_int, id of the output 'number'
subroutine intrf_f__number__ksqexp (idx1, idx2, ida, idb, idout) &
     bind(C, name='intrf_f__number__ksqexp_')
  implicit none
  integer(kind=c_int), intent(in) :: idx1, idx2, ida, idb
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__ksqexp', mod_interfaces_name_, private_ksqexp)
contains
  subroutine private_ksqexp
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_ksqexp
  subroutine private_sp
    call intrf_f__ans_id(number__ksqexp(nnn(idx1, real(0)), nnn(idx2, real(0)), &
         nnn(ida, real(0)), nnn(idb, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__ksqexp(nnn(idx1, dble(0)), nnn(idx2, dble(0)), &
         nnn(ida, dble(0)), nnn(idb, dble(0))), idout)
  end subroutine private_dp     
end subroutine intrf_f__number__ksqexp

!> @}
