!> @addtogrpup api__numbers_math_stats_
!! @{

!> Interface for ldexp
!! @param[in] idy c_int, id of the observation 'number'
!! @param[in] idlam c_int, id of the rate parameter 'number'
!! @param[out] idout c_int, id of the log-density 'number'
subroutine intrf_f__number__ldexp (idy, idlam, idout) bind(C, name='intrf_f__number__ldexp_')
  implicit none
  integer(kind=c_int), intent(in) :: idy, idlam
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__ldexp', mod_interfaces_name_, private_ldexp)
contains
  subroutine private_ldexp
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_ldexp
  subroutine private_sp
    call intrf_f__ans_id(number__ldexp(nnn(idy, real(0)), nnn(idlam, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__ldexp(nnn(idy, dble(0)), nnn(idlam, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__ldexp

!> Interface for ldlaplace
!! @param[in] idy c_int, id of the observation 'number'
!! @param[in] idmu c_int, mean 'number'
!! @param[in] idlam c_int, id of the rate parameter 'number'
!! @param[out] idout c_int, id of the log-density 'number'
subroutine intrf_f__number__ldlaplace (idy, idmu, idlam, idout) &
     bind(C, name='intrf_f__number__ldlaplace_')
  implicit none
  integer(kind=c_int), intent(in) :: idy, idmu, idlam
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__ldlaplace', mod_interfaces_name_, private_ldlaplace)
contains
  subroutine private_ldlaplace
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_ldlaplace
  subroutine private_sp
    call intrf_f__ans_id(number__ldlaplace(nnn(idy, real(0)), nnn(idmu, real(0)), nnn(idlam, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__ldlaplace(nnn(idy, dble(0)), nnn(idmu, dble(0)), nnn(idlam, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__ldlaplace

!> Interface for ldexp
!! @param[in] idy c_int, id of the observation 'number'
!! @param[in] ida1 c_int, id of the shape 1 'number'
!! @param[in] ida2 c_int, id of the shape 2 'number'
!! @param[out] idout c_int, id of the log-density 'number'
subroutine intrf_f__number__ldbeta (idy, ida1, ida2, idout) &
     bind(C, name='intrf_f__number__ldbeta_')
  implicit none
  integer(kind=c_int), intent(in) :: idy, ida1, ida2
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__ldbeta', mod_interfaces_name_, private_ldbeta)
contains
  subroutine private_ldbeta
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_ldbeta
  subroutine private_sp
    call intrf_f__ans_id(number__ldbeta(nnn(idy, real(0)), nnn(ida1, real(0)), nnn(ida2, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__ldbeta(nnn(idy, dble(0)), nnn(ida1, dble(0)), nnn(ida2, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__ldbeta

!> Interface for ldgamma
!! @param[in] idy c_int, id of the observation 'number'
!! @param[in] ida c_int, id of the shape parameter 'number'
!! @param[in] idb c_int, id of the rate parameter 'number'
!! @param[out] idout c_int, id of the log-density 'number'
subroutine intrf_f__number__ldgamma (idy, ida, idb, idout) &
     bind(C, name='intrf_f__number__ldgamma_')
  implicit none
  integer(kind=c_int), intent(in) :: idy, ida, idb
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__ldgamma', mod_interfaces_name_, private_ldgamma)
contains
  subroutine private_ldgamma
    real(kind=sp_) :: sp
    real(kind=dp_) :: dp
    select case (DTYP_)
    case (sp_)
       call intrf_f__ans_id(number__ldgamma(nnn(idy, sp), nnn(ida, sp), nnn(idb, sp)), idout)
    case (dp_)
       call intrf_f__ans_id(number__ldgamma(nnn(idy, dp), nnn(ida, dp), nnn(idb, dp)), idout)
    case default
       call raise_error("DTYP_", err_unknwnVal_)
    end select
  end subroutine private_ldgamma
  subroutine private_sp
    call intrf_f__ans_id(number__ldgamma(nnn(idy, real(0)), nnn(ida, real(0)), nnn(idb, real(0))), idout)
  end subroutine private_sp
    subroutine private_dp
    call intrf_f__ans_id(number__ldgamma(nnn(idy, dble(0)), nnn(ida, dble(0)), nnn(idb, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__ldgamma

!> Interface for ldnorm
!! @param[in] idy c_int, id of the observation 'number'
!! @param[in] idmu c_int, id of the mean 'number'
!! @param[in] ids c_int, id of the standard deviation 'number'
!! @param[out] idout c_int, id of the log-density 'number'
subroutine intrf_f__number__ldnorm (idy, idmu, ids, idout) &
     bind(C, name='intrf_f__number__ldnorm_')
  implicit none
  integer(kind=c_int), intent(in) :: idy, idmu, ids
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__ldnorm', mod_interfaces_name_, private_ldnorm)
contains
  subroutine private_ldnorm
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_ldnorm
  subroutine private_sp
    call intrf_f__ans_id(number__ldnorm(nnn(idy, real(0)), nnn(idmu, real(0)), nnn(ids, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__ldnorm(nnn(idy, dble(0)), nnn(idmu, dble(0)), nnn(ids, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__ldnorm

!> Interface for ldmvnorm
!! @param[in] idy c_int, id of the observation 'number'
!! @param[in] idmu c_int, id of the mean 'number'
!! @param[in] idE c_int, id of the covariance matrix 'number'
!! @param[out] idout c_int, id of the log-density 'number'
subroutine intrf_f__number__ldmvnorm__1 (idy, idmu, idE, idout) &
     bind(C, name='intrf_f__number__ldmvnorm__1_')
  implicit none
  integer(kind=c_int), intent(in) :: idy, idmu, idE
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__ldmvnorm__1', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(number__ldmvnorm__1(nnn(idy, real(0)), nnn(idmu, real(0)), nnn(idE, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__ldmvnorm__1(nnn(idy, dble(0)), nnn(idmu, dble(0)), nnn(idE, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__ldmvnorm__1

!> Interface for mvnorm_posterior
!! @param[in] inv c_int, if > 0 A11 is a precision matrix, else it is a covariance matrix
!! @param[in] ida11 c_int, id of A11
!! @param[in] ide21 c_int, id of Cov21
!! @param[in] ide22 c_int, id of Cov22
!! @param[in] idy11 c_int, id of y1
!! @param[in] idmu11 c_int, id of mu1
!! @param[out] idout c_int(2), ids of posterior mean and convariance
subroutine intrf_f__number__mvnorm_posterior (inv, ida11, ide21, ide22, idy11, idmu11, idout) &
     bind(C, name='intrf_f__number__mvnorm_posterior_')
  implicit none
  integer(kind=c_int), intent(in) :: inv, ida11, ide21, ide22, idy11, idmu11
  integer(kind=c_int), intent(out) :: idout(2)
  call do_within('intrf_f__number__mvnorm_posterior', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    type(sp_number), pointer :: mu22, e22
    call number__mvnorm_posterior(inv > 0, nnn(ida11, real(0)), nnn(ide21, real(0)), nnn(ide22, real(0)), nnn(idy11, real(0)), &
         nnn(idmu11, real(0)), mu22, e22)
    call intrf_f__ans_id(mu22, idout(1))
    call intrf_f__ans_id(e22, idout(2))
  end subroutine private_sp
  subroutine private_dp
    type(dp_number), pointer :: mu22, e22
    call number__mvnorm_posterior(inv > 0, nnn(ida11, dble(0)), nnn(ide21, dble(0)), nnn(ide22, dble(0)), nnn(idy11, dble(0)), &
         nnn(idmu11, dble(0)), mu22, e22)
    call intrf_f__ans_id(mu22, idout(1))
    call intrf_f__ans_id(e22, idout(2))
  end subroutine private_dp
end subroutine intrf_f__number__mvnorm_posterior

!> @}
