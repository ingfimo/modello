!> @addtogroup api__numbers_math_objectives_
!! @{

!> Interface for number__binentropy
!! @relates number__binentropy
!! @param[in] id1 c_int, id of the target 'number'
!! @param[in] id2 c_int, id of the prediction 'number'
!! @param[out] idout c_int, id of objective 'number'
subroutine intrf_f__number__binentropy (id1, id2, idout) bind(C, name='intrf_f__number__binentropy_')
  implicit none
  integer(kind=c_int), intent(in) :: id1, id2
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__binentropy', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(number__binentropy(nnn(id1, real(0)), nnn(id2, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__binentropy(nnn(id1, dble(0)), nnn(id2, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__binentropy

!> Interface for number__logit_binentropy
!! @relates number__logit_binentropy
!! @param[in] id1 c_int, id of the target 'number'
!! @param[in] id2 c_int, id of the prediction 'number'
!! @param[out] idout c_int, id of objective 'number'
subroutine intrf_f__number__logit_binentropy (id1, id2, idout) bind(C, name='intrf_f__number__logit_binentropy_')
  implicit none
  integer(kind=c_int), intent(in) :: id1, id2
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__logit_binentropy', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(number__logit_binentropy(nnn(id1, real(0)), nnn(id2, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__logit_binentropy(nnn(id1, dble(0)), nnn(id2, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__logit_binentropy

!> Interface for number__crossentropy
!! @relates number__crossentropy
!! @param[in] id1 c_int, id of the target 'number'
!! @param[in] id2 c_int, id of the prediction 'number'
!! @param[out] idout c_int, id of objective 'number
subroutine intrf_f__number__crossentropy (id1, id2, idout) bind(C, name='intrf_f__number__crossentropy_')
  implicit none
  integer(kind=c_int), intent(in) :: id1, id2
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__crossentropy', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(number__crossentropy(nnn(id1, real(0)), nnn(id2, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__crossentropy(nnn(id1, dble(0)), nnn(id2, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__crossentropy

!> Interface for number__logit_crossentropy
!! @relates number__logit_crossentropy
!! @param[in] id1 c_int, id of the target number
!! @param[in] id2 c_int, id of the prediction number
!! @param[out] idout c_int, id of the objective number
subroutine intrf_f__number__logit_crossentropy (id1, id2, k, idout) bind(C, name="intrf_f__number__logit_crossentropy_")
  implicit none
  integer(kind=c_int), intent(in) :: id1, id2, k
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__logit_crossentropy", mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    if (k > 0) then
       call intrf_f__ans_id(number__logit_crossentropy(nnn(id1, real(0)), nnn(id2, real(0)), k), idout)
    else
       call intrf_f__ans_id(number__logit_crossentropy(nnn(id1, real(0)), nnn(id2, real(0))), idout)
    end if
  end subroutine private_sp
  subroutine private_dp
    if (k > 0) then
       call intrf_f__ans_id(number__logit_crossentropy(nnn(id1, dble(0)), nnn(id2, dble(0)), k), idout)
    else
       call intrf_f__ans_id(number__logit_crossentropy(nnn(id1, dble(0)), nnn(id2, dble(0))), idout)
    end if
  end subroutine private_dp
end subroutine intrf_f__number__logit_crossentropy

!> Interface for number__mse
!! @relates number__mse
!! @param[in] id1 c_int, id of the target 'number'
!! @param[in] id2 c_int, id of the prediction 'number'
!! @param[out] idout c_int, id of objective 'number
subroutine intrf_f__number__mse (idy, idyh, idout) bind(C, name='intrf_f__number__mse_')
  integer(kind=c_int), intent(in) :: idy, idyh
  integer(kind=c_int), intent(out) :: idout
  idout=1
  call do_within('intrf_f__number__mse', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(number__mse(nnn(idy, real(0)), nnn(idyh, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__mse(nnn(idy, dble(0)), nnn(idyh, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__mse

!> Interface for number__mae
!! @relates number__mae
!! @param[in] id1 c_int, id of the target 'number'
!! @param[in] id2 c_int, id of the prediction 'number'
!! @param[out] idout c_int, id of objective 'number
subroutine intrf_f__number__mae (idy, idyh, idout) bind(C, name='intrf_f__number__mae_')
  integer(kind=c_int), intent(in) :: idy, idyh
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__mae', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(number__mae(nnn(idy, real(0)), nnn(idyh, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__mae(nnn(idy, dble(0)), nnn(idyh, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__mae

!> Interfate for number__lkhnorm
!! @relates number__lkhnorm
!! @param[in] idy c_int, id of the observation 'number'
!! @param[in] idmu c_int, id of the mea 'number'
!! @param[in] ids c_int, id of the standard deviation 'number'
!! @param[in] idw c_int, id of the weight 'number' (if < 1 no weighting)
!! @param[out] idout c_int, id of the log-likelihood
subroutine intrf_f__number__lkhnorm (idy, idmu, ids, idw, idout) &
     bind(C, name='intrf_f__number__lkhnorm_')
  implicit none
  integer(kind=c_int), intent(in) :: idy, idmu, ids, idw
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__lkhnorm', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    if (idw > 0) then
       call intrf_f__template(private_sp1, private_dp1)
    else
       call intrf_f__template(private_sp2, private_dp2)
    end if
  end subroutine private_do
  subroutine private_sp1
    call intrf_f__ans_id(number__lkhnorm(nnn(idy, real(0)), nnn(idmu, real(0)), &
         nnn(ids, real(0)), nnn(idw, real(0))), idout)
  end subroutine private_sp1
  subroutine private_dp1
    call intrf_f__ans_id(number__lkhnorm(nnn(idy, dble(0)), nnn(idmu, dble(0)), &
         nnn(ids, dble(0)), nnn(idw, dble(0))), idout)
  end subroutine private_dp1
  subroutine private_sp2
    call intrf_f__ans_id(number__lkhnorm(nnn(idy, real(0)), nnn(idmu, real(0)), &
         nnn(ids, real(0))), idout)
  end subroutine private_sp2
  subroutine private_dp2
    call intrf_f__ans_id(number__lkhnorm(nnn(idy, dble(0)), nnn(idmu, dble(0)), &
         nnn(ids, dble(0))), idout)
  end subroutine private_dp2
end subroutine intrf_f__number__lkhnorm

!> @}
