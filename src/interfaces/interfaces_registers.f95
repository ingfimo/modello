!> @addtogroup api__registers_
!! @{

!> Interface to set_mode
!! @param m c_int, mode identifier
subroutine intrf_f__set_mode (m) bind(C, name="intrf_f__set_mode_")
  implicit none
  integer(kind=c_int), intent(in) :: m
  call do_within("intrf_f__set_mode", mod_interfaces_name_, private_set)
contains
  subroutine private_set
    call set_mode(m)
  end subroutine private_set
end subroutine intrf_f__set_mode

!> Interface to set_dtyp
!! @param dtyp c_int, default data type identifier
subroutine intrf_f__set_dtyp (dtyp) bind(C, name="intrf_f__set_dtyp_")
  implicit none
  integer(kind=c_int), intent(in) :: dtyp
  call do_within("intrf_f__set_dtyp", mod_interfaces_name_, private_set)
contains
  subroutine private_set
    call set_dtyp(dtyp)
  end subroutine private_set
end subroutine intrf_f__set_dtyp

subroutine intrf_f__set_inposts (x) bind(C, name="intrf_f__set_inposts_")
  implicit none
  integer(kind=c_int), intent(in) :: x
  call do_within("intrf_f__set_inposts", mod_interfaces_name_, private_set)
contains
  subroutine private_set
    call set_inposts(x > 0)
  end subroutine private_set
end subroutine intrf_f__set_inposts

!> Gets the identifier of the single precision data type
!! @param[out] x c_int
subroutine intrf_f__get_sp (x) bind(C, name="intrf_f__get_sp_")
  implicit none
  integer(kind=c_int), intent(out) :: x
  x = sp_
end subroutine intrf_f__get_sp

!> Gets the identifier of the double precision data type
!! @param[out] x c_int
subroutine intrf_f__get_dp (x) bind(C, name="intrf_f__get_dp_")
  implicit none
  integer(kind=c_int), intent(out) :: x
  x = dp_
end subroutine intrf_f__get_dp

!> Gets the identifier of the training mode
!! @param[out] x c_int
subroutine intrf_f__get_training_mode (x) bind(C, name="intrf_f__get_training_mode_")
  implicit none
  integer(kind=c_int), intent(out) :: x
  x = training_mode_
end subroutine intrf_f__get_training_mode

!> Gets the identifier of the evaluation mode
!! @param[out] x c_int
subroutine intrf_f__get_evaluation_mode (x) bind(C, name="intrf_f__get_evaluation_mode_")
  implicit none
  integer(kind=c_int), intent(out) :: x
  x = evaluation_mode_
end subroutine intrf_f__get_evaluation_mode

subroutine intrf_f__allocate_hash_tables (n) bind(C, name="intrf_f__allocate_hash_tables_")
  implicit none
  integer(kind=c_int), intent(in) :: n
  call do_within("intrf_f__allocate_hash_tables", mod_interfaces_name_, private_allocate)
contains
  subroutine private_allocate
    call allocate_hash_tables(n)
  end subroutine private_allocate
end subroutine intrf_f__allocate_hash_tables

!> @}
