!> @addtogroup api__numbers_math_unary_
!! @{

!> Interface for Abs
!! @param[in] id c_int, id of the input 'number'
!! @param[out] idout c_int, id of output 'number
subroutine intrf_f__number__abs (id, idout) bind(C, name='intrf_f__number__abs_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__abs', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(abs(nnn(id, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(abs(nnn(id, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__abs

!> Interface for Exp
!! @param[in] id c_int, id of the input 'number'
!! @param[out] idout c_int, id of output 'number
subroutine intrf_f__number__exp (id, idout) bind(C, name='intrf_f__number__exp_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__exp', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(exp(nnn(id, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(exp(nnn(id, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__exp

!> Interface for Log
!! @param[in] id c_int, id of the input 'number'
!! @param[out] idout c_int, id of output 'number
subroutine intrf_f__number__log (id, idout) bind(C, name='intrf_f__number__log_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__log', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(log(nnn(id, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(log(nnn(id, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__log

!> Interface for Sin
!! @param[in] id c_int, id of the input 'number'
!! @param[out] idout c_int, id of output 'number
subroutine intrf_f__number__sin (id, idout) bind(C, name='intrf_f__number__sin_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__sin', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(sin(nnn(id, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(sin(nnn(id, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__sin

!> Interface for Cos
!! @param[in] id c_int, id of the input 'number'
!! @param[out] idout c_int, id of output 'number
subroutine intrf_f__number__cos (id, idout) bind(C, name='intrf_f__number__cos_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__cos', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(cos(nnn(id, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(cos(nnn(id, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__cos

!> Interface for Tan
!! @param[in] id c_int, id of the input 'number'
!! @param[out] idout c_int, id of output 'number
subroutine intrf_f__number__tan (id, idout) bind(C, name='intrf_f__number__tan_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__tan', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(tan(nnn(id, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(tan(nnn(id, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__tan

!> Interface for Sinh
!! @param[in] id c_int, id of the input 'number'
!! @param[out] idout c_int, id of output 'number
subroutine intrf_f__number__sinh (id, idout) bind(C, name='intrf_f__number__sinh_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__sinh', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(sinh(nnn(id, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(sinh(nnn(id, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__sinh

!> Interface for Cosh
!! @param[in] id c_int, id of the input 'number'
!! @param[out] idout c_int, id of output 'number
subroutine intrf_f__number__cosh (id, idout) bind(C, name='intrf_f__number__cosh_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__cosh', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(cosh(nnn(id, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(cosh(nnn(id, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__cosh

!> Interface for Tanh
!! @param[in] id c_int, id of the input 'number'
!! @param[out] idout c_int, id of output 'number
subroutine intrf_f__number__tanh (id, idout) bind(C, name='intrf_f__number__tanh_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__tanh', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(tanh(nnn(id, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(tanh(nnn(id, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__tanh

!> @}
