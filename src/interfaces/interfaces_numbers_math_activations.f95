!> @addtogroup api__numbers_math_activations_
!! @{

!> Interface for Sigmoid
!! @param[in] id c_int, id of the input 'number'
!! @param[out] idout c_int, id of output 'number
subroutine intrf_f__number__sigmoid (id, idout) bind(C, name='intrf_f__number__sigmoid_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__sigmoid', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(number__sigmoid(nnn(id, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__sigmoid(nnn(id, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__sigmoid

!> Softplus operator
!!
!! @param[in] id input number identifier
!! @param[out] idout output number identifier
subroutine intrf_f__number__softplus (id, idout) bind(C, name='intrf_f__number__softplus_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__sigmoid', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(number__softplus(nnn(id, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__softplus(nnn(id, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__softplus

!> Interface for ReLU
!! @param[in] id c_int, id of the input 'number'
!! @param[out] idout c_int, id of output 'number
subroutine intrf_f__number__relu (id, idout) bind(C, name="intrf_f__number__relu_")
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__relu", mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(number__relu(nnn(id, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__relu(nnn(id, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__relu

!> Leaky-relu Operator
!!
!! @param[in] idx input number identifier
!! @param[in] ida leaky coefficient number identifier
!! @param[out] idout output number identifier
subroutine intrf_f__number__leaky_relu (idx, ida, idout) bind(C, name="intrf_f__number__leaky_relu_")
  implicit none
  integer(kind=c_int), intent(in) :: idx, ida
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__leaky_relu", mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(number__leaky_relu(nnn(idx, real(0)), nnn(ida, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__leaky_relu(nnn(idx, dble(0)), nnn(ida, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__leaky_relu

!> Interface for ELU
!! @param[in] id c_int, id of the input 'number'
!! @param[out] idout c_int, id of output 'number
subroutine intrf_f__number__elu (id, ida, idout) bind(C, name="intrf_f__number__elu_")
  implicit none
  integer(kind=c_int), intent(in) :: id, ida
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__elu", mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(number__elu(nnn(id, real(0)), nnn(ida, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__elu(nnn(id, dble(0)), nnn(ida, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__elu

!> SiLU operator
!!
!! @param[in] id input number identifer
!! @param[out] idout output number identifier
subroutine intrf_f__number__silu (id, idout) bind(C, name="intrf_f__number__silu_")
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__silu", mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(number__silu(nnn(id, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__silu(nnn(id, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__silu

!> Interface for Swish
!! @param[in] id c_int, id of the input 'number'
!! @param[out] idout c_int, id of output 'number
subroutine intrf_f__number__swish (idx, ida, idout) bind(C, name="intrf_f__number__swish_")
  implicit none
  integer(kind=c_int), intent(in) :: idx, ida
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__swish", mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(number__swish(nnn(idx, real(0)), nnn(ida, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__swish(nnn(idx, dble(0)), nnn(ida, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__swish

!> Interface for Softmax
!! @param[in] id c_int, id of the input 'number'
!! @param[out] idout c_int, id of output 'number
subroutine intrf_f__number__softmax (id, k, idout) bind(C, name='intrf_f__number__softmax_')
  implicit none
  integer(kind=c_int), intent(in) :: id, k
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__softmax', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    if (k > 0) then
       call intrf_f__template(private_sp1, private_dp1)
    else
       call intrf_f__template(private_sp2, private_dp2)
    end if
  end subroutine private_do
  subroutine private_sp1
    call intrf_f__ans_id(number__softmax(nnn(id, real(0)), k), idout)
  end subroutine private_sp1
  subroutine private_dp1
    call intrf_f__ans_id(number__softmax(nnn(id, dble(0)), k), idout)
  end subroutine private_dp1
  subroutine private_sp2
    call intrf_f__ans_id(number__softmax(nnn(id, real(0))), idout)
  end subroutine private_sp2
  subroutine private_dp2
    call intrf_f__ans_id(number__softmax(nnn(id, dble(0))), idout)
  end subroutine private_dp2
end subroutine intrf_f__number__softmax

!> @}
