!> @addtogroup api__optim_methods_
!! @{

!> Allocates the GOPTS_ register
!! @param[in] n integer, array size
subroutine intrf_f__allocate_gopts (n) bind(C, name='intrf_f__allocate_gopts_')
  implicit none
  integer(c_int), intent(in) :: n
  call do_within('intrf_f__allocate_gopts', mod_interfaces_name_, private_allocate)
contains
  subroutine private_allocate
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_allocate
  subroutine private_sp
    call allocate_opts(n, real(0))
  end subroutine private_sp
  subroutine private_dp
    call allocate_opts(n, dble(0))
  end subroutine private_dp
end subroutine intrf_f__allocate_gopts

!> Deallocates the GOPTS_ register
subroutine intrf_f__deallocate_gopts () bind(C, name='intrf_f__deallocate_gopts_')
  implicit none
  call do_within('intrf_f__deallocate_gopts', mod_interfaces_name_, private_deallocate)
contains
  subroutine private_deallocate
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_deallocate
  subroutine private_sp
    call deallocate_opts(real(0))
  end subroutine private_sp
  subroutine private_dp
    call deallocate_opts(dble(0))
  end subroutine private_dp
end subroutine intrf_f__deallocate_gopts

!> Append a sgd optimiser to the GOPTS_ register
!! @param[out] i integer, 'opt' index
!! @param[in] nin integer, size of the parameter vector
!! @param[in] xin intger(:), parameter vector containing the indexes of the parameter 'numbers'
subroutine intrf_f__opt__append_sgd (i, nin, xin) bind(C, name='intrf_f__opt__append_sgd_')
  implicit none
  integer(kind=c_int), intent(out) :: i
  integer(kind=c_int), intent(in) :: nin, xin(nin)
  call do_within('intrf_f__opt__append_sgd', mod_interfaces_name_, private_append)
contains
  subroutine private_append
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_append
  subroutine private_sp
    type(sp_opt), pointer :: opt
    call opt__append_sgd(opt, xin)
    if (err_free()) i = opt%id
  end subroutine private_sp
  subroutine private_dp
    type(dp_opt), pointer :: opt
    call opt__append_sgd(opt, xin)
    if (err_free()) i = opt%id
  end subroutine private_dp
end subroutine intrf_f__opt__append_sgd

!> Append a sgdwm optimiser to the GOPTS_ register
!! @param[out] i integer, 'opt' index
!! @param[in] nin integer, size of the parameter vector
!! @param[in] xin intger(:), parameter vector containing the indexes of the parameter 'numbers' 
subroutine intrf_f__opt__append_sgdwm (i, nin, xin) bind(C, name='intrf_f__opt__append_sgdwm_')
  implicit none
  integer(kind=c_int), intent(out) :: i
  integer(kind=c_int), intent(in) :: nin, xin(nin)
  call do_within('intrf_f__opt__append_sgdwm', mod_interfaces_name_, private_append)
contains
  subroutine private_append
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_append
  subroutine private_sp
    type(sp_opt), pointer :: opt
    call opt__append_sgdwm(opt, xin)
    if (err_free()) i = opt%id
  end subroutine private_sp
  subroutine private_dp
    type(dp_opt), pointer :: opt
    call opt__append_sgdwm(opt, xin)
    if (err_free()) i = opt%id
  end subroutine private_dp
end subroutine intrf_f__opt__append_sgdwm

!> Append an Adam optimiser to the GOPTS_ register
!! @param[out] i integer, 'opt' index
!! @param[in] nin integer, size of the parameter vector
!! @param[in] xin intger(:), parameter vector containing the indexes of the parameter 'numbers' 
subroutine intrf_f__opt__append_adam (i, nin, xin) bind(C, name='intrf_f__opt__append_adam_')
  implicit none
  integer(kind=c_int), intent(out) :: i
  integer(kind=c_int), intent(in) :: nin, xin(nin)
  call do_within('intrf_f__opt__append_adam', mod_interfaces_name_, private_append)
contains
  subroutine private_append
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_append
  subroutine private_sp
    type(sp_opt), pointer :: opt
    call opt__append_adam(opt, xin)
    if (err_free()) i = opt%id
  end subroutine private_sp
  subroutine private_dp
    type(dp_opt), pointer :: opt
    call opt__append_adam(opt, xin)
    if (err_free()) i = opt%id
  end subroutine private_dp
end subroutine intrf_f__opt__append_adam

!> Pops (removes) an optimiser fromt he GOPTS_ register
!! @param[in] i integer, 'opt' index
subroutine intrf_f__opt__pop (i) bind(C, name='intrf_f__opt__pop_')
  implicit none
  integer(kind=c_int), intent(in) :: i
  call do_within('intrf_f__opt__pop', mod_interfaces_name_, private_pop)
contains
  subroutine private_pop
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_pop
  subroutine private_sp
    type(sp_opt), pointer :: opt
    call get_opt(i, opt)
    call opt__pop(opt)
  end subroutine private_sp
  subroutine private_dp
    type(dp_opt), pointer :: opt
    call get_opt(i, opt)
    call opt__pop(opt)
  end subroutine private_dp
end subroutine intrf_f__opt__pop

!> Performs niter iterations of a sgd optimiser step
!! @param[in] xoi integer, 'opt' index
!! @param[in] gi integer, 'graph' index
!! @param[in] xout integer, output 'number' index
!! @param[in] lr dpuble precision, lerning rate
!! @param[in] niter integer, number of iterations to perform
subroutine intrf_f__opt__sgd_step (xoi, gi, xout, lr, niter, ftrain) bind(C, name='intrf_f__opt__sgd_step_')
  implicit none
  integer(kind=c_int), intent(in) :: xoi, gi, xout, niter
  real(kind=c_double), intent(in) :: lr
  real(kind=c_double), intent(out) :: ftrain
  call do_within('intrf_f__opt__sgd_step', mod_interfaces_name_, private_step)
contains
  subroutine private_step
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_step
  subroutine private_sp
    real(kind=sp_) :: ff
    call intrf_f__opt__sgd_step__0(xoi, gi, xout, real(lr), niter, ff)
    ftrain = dble(ff)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__opt__sgd_step__0(xoi, gi, xout, lr, niter, ftrain)
  end subroutine private_dp
end subroutine intrf_f__opt__sgd_step

!> Performs niter iterations of a sgdwm optimiser step
!! @param[in] xoi integer, 'opt' index
!! @param[in] gi integer, 'graph' index
!! @param[in] xout integer, output 'number' index
!! @param[in] lr dpouble precision, lerning rate
!! @param[in] alpha double precision,  momentum parameter
!! @param[in] niter integer, number of iterations to perform
subroutine intrf_f__opt__sgdwm_step (xoi, gi, xout, lr, alpha, niter, ftrain) bind(C, name='intrf_f__opt__sgdwm_step_')
  implicit none
  integer(kind=c_int), intent(in) :: xoi, gi, xout, niter
  real(kind=c_double), intent(in) :: lr, alpha
  real(kind=c_double), intent(out) :: ftrain
  call do_within('intrf_f__opt__sgd_step', mod_interfaces_name_, private_step)
contains
  subroutine private_step
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_step
  subroutine private_sp
    real(kind=sp_) :: ff
    call intrf_f__opt__sgdwm_step__0(xoi, gi, xout, real(lr), real(alpha), niter, ff)
    ftrain = dble(ff)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__opt__sgdwm_step__0(xoi, gi, xout, lr, alpha, niter, ftrain)
  end subroutine private_dp
end subroutine intrf_f__opt__sgdwm_step

!> Performs niter iterations of an Adam optimiser step
!! @param[in] xoi integer, 'opt' index
!! @param[in] gi integer, 'graph' index
!! @param[in] xout integer, output 'number' index
!! @param[in] lr double precision, lerning rate
!! @param[in] beta1 double precision, first order momentum parameter
!! @param[in] beta2 double precision, second order momentum parameter
!! @param[in] niter integer, number of iterations to perform
subroutine intrf_f__opt__adam_step (xoi, gi, xout, lr, beta1, beta2, niter, ftrain) &
     bind(C, name='intrf_f__opt__adam_step_')
  implicit none
  integer(kind=c_int), intent(in) :: xoi, gi, xout,  niter
  real(kind=c_double), intent(in) :: lr, beta1, beta2
  real(kind=c_double), intent(out) :: ftrain
  call do_within('intrf_f__opt__adam_step', mod_interfaces_name_, private_step)
contains
  subroutine private_step
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_step
  subroutine private_sp
    real(kind=sp_) :: ff
    call intrf_f__opt__adam_step__0(xoi, gi, xout, real(lr), &
         real(beta1), real(beta2), niter, ff)
    ftrain = dble(ff)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__opt__adam_step__0(xoi, gi, xout, lr, &
         beta1, beta2, niter, ftrain)
  end subroutine private_dp
end subroutine intrf_f__opt__adam_step

subroutine intrf_f__opt__get_state (id, i, n, state, sz) bind(C, name="intrf_f__opt__get_state_")
  implicit none
  integer(kind=c_int), intent(in) :: id, i, sz
  integer(kind=c_int), intent(inout) :: n
  real(kind=c_double), intent(out) :: state(n)
  call do_within("intrf_f__opt__get_state", mod_interfaces_name_, private_get)
contains
  subroutine private_get
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_get
  subroutine private_sp
    real(kind=sp_), allocatable :: s(:)
    call opt__get_state(ooo(id, real(0)), i, s)
    if (sz > 0) then
       n = size(s)
    else
       state = dble(s)
    end if
  end subroutine private_sp
  subroutine private_dp
    real(kind=dp_), allocatable :: s(:)
    call opt__get_state(ooo(id, dble(0)), i, s)
    if (sz > 0) then
       n = size(s)
    else
       state = s
    end if
  end subroutine private_dp
end subroutine intrf_f__opt__get_state

subroutine intrf_f__opt__set_state (id, i, n, state) bind(C, name="intrf_f__opt__set_state_")
  implicit none
  integer(kind=c_int), intent(in) :: id, i, n
  real(kind=c_double), intent(in) :: state(n)
  call do_within("intrf_f__opt__set_state", mod_interfaces_name_, private_set)
  contains
  subroutine private_set
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_set
  subroutine private_sp
    type(sp_opt), pointer :: opt
    call get_opt(id, opt)
    call opt__set_state(opt, i, real(state))
  end subroutine private_sp
  subroutine private_dp
    type(dp_opt), pointer :: opt
    call get_opt(id, opt)
    call opt__set_state(opt, i, state)
  end subroutine private_dp
end subroutine intrf_f__opt__set_state

subroutine intrf_f__opt__set_clipping (id, clip, clipval) bind(C, name="intrf_f__opt__set_clipping_")
  implicit none
  integer(kind=c_int), intent(in) :: id, clip
  real(kind=c_double), intent(in) :: clipval
  call do_within("intrf_f__opt__set_state", mod_interfaces_name_, private_set)
contains
  subroutine private_set
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_set
  subroutine private_sp
    type(sp_opt), pointer :: opt
    call get_opt(id, opt)
    call opt__set_clipping(opt, clip > 0, real(clipval))
  end subroutine private_sp
  subroutine private_dp
    type(dp_opt), pointer :: opt
    call get_opt(id, opt)
    call opt__set_clipping(opt, clip > 0, clipval)
  end subroutine private_dp
end subroutine intrf_f__opt__set_clipping

subroutine intrf_f__opt__get_clipping (id, clipping) bind(C, name="intrf_f__opt__get_clipping_")
  implicit none
  integer(kind=c_int), intent(in) :: id
  real(kind=c_double), intent(out) :: clipping(2)
  call do_within("intrf_f__opt__get_clipping", mod_interfaces_name_, private_get)
contains
  subroutine private_get
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_get
  subroutine private_sp
    real(kind=sp_) :: x(2)
    call opt__get_clipping(ooo(id, 0._sp_), x)
    clipping = dble(x)
  end subroutine private_sp
  subroutine private_dp
    real(kind=dp_) :: x(2)
    call opt__get_clipping(ooo(id, 0._dp_), x)
    clipping = x
  end subroutine private_dp
end subroutine intrf_f__opt__get_clipping
 


!> @}
