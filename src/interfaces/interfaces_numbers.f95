!> @addtogroup api__numbers_methods_
!! @{

!> Allocates the NUMBERS_ array and it dependency registers
!! @param[in] n c_int, size of the array
subroutine intrf_f__allocate_numbers (n) bind(C, name='intrf_f__allocate_numbers_')
  implicit none
  integer(kind=c_int), intent(in) :: n
  call do_within('intrf_f__allocate_numbers', mod_interfaces_name_, private_allocate)
contains
  subroutine private_allocate
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_allocate
  subroutine private_sp
    call allocate_numbers(n, real(0))
  end subroutine private_sp
  subroutine private_dp
    call allocate_numbers(n, dble(0))
  end subroutine private_dp
end subroutine intrf_f__allocate_numbers

!> Deallocates the NUMBERS_ array and its dependency registers
subroutine intrf_f__deallocate_numbers () bind(C, name='intrf_f__deallocate_numbers_')
  implicit none
  call do_within('intrf_f__deallocate_numbers_', mod_interfaces_name_, private_deallocate)
contains
  subroutine private_deallocate
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_deallocate
  subroutine private_sp
    call deallocate_numbers(real(0))
  end subroutine private_sp
  subroutine private_dp
    call deallocate_numbers(dble(0))
  end subroutine private_dp
end subroutine intrf_f__deallocate_numbers

!> Checks if a 'number' is allocated
!! @param[in] id c_int, id of the 'number'
!! @param[out] ans c_int, is equal to 1 if the 'number' is allocated, 0 otherwise
subroutine intrf_f__number__is_allocated (id, ans) bind(C, name='intrf_f__number__is_allocated_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: ans
  call do_within('intrf_f__number_is_allocated', mod_interfaces_name_, private_is_allocated)
contains
  subroutine private_is_allocated
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_is_allocated
  subroutine private_sp
     if (allocated(sp_NUMBERS_)) then
       call assert(id > 0 .and. id <= size(sp_NUMBERS_), err_oorng_, 'id')
       if (err_free()) ans = merge(1, 0, is_allocated(sp_NUMBERS_(id)))
    else
       ans = 0
    end if
  end subroutine private_sp
  subroutine private_dp
    if (allocated(dp_NUMBERS_)) then
       call assert(id > 0 .and. id <= size(dp_NUMBERS_), err_oorng_, 'id')
       if (err_free()) ans = merge(1, 0, is_allocated(dp_NUMBERS_(id)))
    else
       ans = 0
    end if
  end subroutine private_dp
end subroutine intrf_f__number__is_allocated

!> Checks if a 'number' has dv allocated
!! @param[in] id c_int, 'number' id
!! @param[out] ans c_int, 1 if dv is allocated else 0
subroutine intrf_f__number__has_dx (id, ans) bind(C, name='intrf_f__number__has_dx_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: ans
  call do_within('intrf_f__number__is_allocated', mod_interfaces_name_, private_has_dx)
contains
  subroutine private_has_dx
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_has_dx
  subroutine private_sp
    ans = merge(1, 0, has_dx(nnn(id, real(0))))
  end subroutine private_sp
   subroutine private_dp
    ans = merge(1, 0, has_dx(nnn(id, dble(0))))
  end subroutine private_dp
end subroutine intrf_f__number__has_dx

!> Gets the rank of a 'number'
!! @param[in] id c_int, 'number' id
!! @param[out] r c_int, rank of the number
subroutine intrf_f__number__rank (id, r) bind(C, name='intrf_f__number__rank_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: r
  call do_within('intrf_f__number__rank', mod_interfaces_name_, private_rank)
contains
  subroutine private_rank
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_rank
  subroutine private_sp
    r = get_rank(nnn(id, real(0)))
  end subroutine private_sp
  subroutine private_dp
    r = get_rank(nnn(id, dble(0)))
  end subroutine private_dp
end subroutine intrf_f__number__rank

!> Append a 'number' of the given shape to the NUMBER_ array
!! @param[out] id c_int, appended 'number' id
!! @param[in] r c_in, rank of the 'number'
!! @param[in] shp c_int, shape of the number
!! @param[in] dx c_int, if > 0 the 'number' has a dx element
subroutine intrf_f__number__append (id, r, shp, dx) bind(C, name='intrf_f__number__append_')
  implicit none
  integer(kind=c_int), intent(out) :: id
  integer(kind=c_int), intent(in) :: r, shp(r), dx
  type(number), pointer :: x
  call do_within('intrf_f__number__append', mod_interfaces_name_, private_append)
contains
  subroutine private_append
    if (r > 0) then
       call intrf_f__template(private_sp1, private_dp1)
    else
       call intrf_f__template(private_sp2, private_dp2)
    end if
  end subroutine private_append
  subroutine private_sp1
    type(sp_number), pointer :: x
    call number__append(x, shp, dx > 0)
    if (err_free()) id = x%id
  end subroutine private_sp1
  subroutine private_dp1
    type(dp_number), pointer :: x
    call number__append(x, shp, dx > 0)
    if (err_free()) id = x%id
  end subroutine private_dp1
  subroutine private_sp2
    type(sp_number), pointer :: x
    call number__append(x, [integer::], dx > 0)
    if (err_free()) id = x%id
  end subroutine private_sp2
  subroutine private_dp2
    type(dp_number), pointer :: x
    call number__append(x, [integer::], dx > 0)
    if (err_free()) id = x%id
  end subroutine private_dp2
end subroutine intrf_f__number__append

!> Gets the size of a 'number'
!! @param[in] id c_int, 'number' id
!! @param[out] sz c_int, size of the 'number'
subroutine intrf_f__number__size (id, sz) bind(C, name='intrf_f__number__size_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: sz
  call do_within('intrf_f__number__size', mod_interfaces_name_, private_size)
contains
  subroutine private_size
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_size
  subroutine private_sp
    sz = get_size(nnn(id, real(0)))
  end subroutine private_sp
  subroutine private_dp
    sz = get_size(nnn(id, dble(0)))
  end subroutine private_dp
end subroutine intrf_f__number__size

!> Gets the shape of a 'number'
!! @param[in] id c_int, 'number' id
!! @param[in] r c_int, rank of the 'number'
!! @param[out] shp c_int, shape of the 'number'
subroutine intrf_f__number__shape (id, r, shp) bind(C, name='intrf_f__number__shape_')
  implicit none
  integer(kind=c_int), intent(in) :: id, r
  integer(kind=c_int), intent(out) :: shp(r)
  call do_within('intrf_f__number__shape', mod_interfaces_name_, private_shape)
contains
  subroutine private_shape
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_shape
  subroutine private_sp
    type(sp_number), pointer :: x
    call get_number(id, x)
    shp = x%shp
  end subroutine private_sp
  subroutine private_dp
    type(dp_number), pointer :: x
    call get_number(id, x)
    shp = x%shp
  end subroutine private_dp
end subroutine intrf_f__number__shape

!> Sets the value of a 'number'
!! @param[in] id c_int, 'number' id
!! @param[in] n c_int, size of the 'number'
!! @param[inout] x c_double, new value of the number
subroutine intrf_f__number__set_v (id, n, x) bind(C, name='intrf_f__number__set_v_')
  implicit none
  integer(kind=c_int), intent(in) :: id, n
  real(kind=c_double), intent(inout) :: x(n)
  call do_within('intrf_f__number__set_v', mod_interfaces_name_, private_set_v)
contains
  subroutine private_set_v
    if (n > 1) then
       call intrf_f__template(private_sp1, private_dp1)
    else
       call intrf_f__template(private_sp2, private_dp2)
    end if
  end subroutine private_set_v
  subroutine private_sp1
    type(sp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_v(xx, real(x))
  end subroutine private_sp1
  subroutine private_dp1
    type(dp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_v(xx, x)
  end subroutine private_dp1
   subroutine private_sp2
    type(sp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_v(xx, real(x(1)))
  end subroutine private_sp2
  subroutine private_dp2
    type(dp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_v(xx, x(1))
  end subroutine private_dp2
end subroutine intrf_f__number__set_v

!> [DEPRECATED]
subroutine intrf_f__number__set_v_sequence (id, m, n, x) bind(C, name='intrf_f__number__set_v_sequence_')
  implicit none
  integer(kind=c_int), intent(in) :: id(m), n, m
  real(kind=c_double), intent(inout) :: x(n,m)
  integer :: i
  call do_within('intrf_f__number__set_v_sequence', mod_interfaces_name_, private_set_v)
contains
  subroutine private_set_v
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_set_v
  subroutine private_sp
    type(sp_number) :: xx(size(id))
    xx = [(nnn(id(i), real(0)), i=1, size(id))]
    call number__set_v(xx, real(x))
  end subroutine private_sp
  subroutine private_dp
    type(dp_number) :: xx(size(id))
    xx = [(nnn(id(i), dble(0)), i=1, size(id))]
    call number__set_v(xx, x)
  end subroutine private_dp
end subroutine intrf_f__number__set_v_sequence

!> Stes the derivative of 'number'
!! @param[in] id c_int, 'number' id
!! @param[in] n c_int, size of the 'number'
!! @param[inout] x c_double, new derivative value
subroutine intrf_f__number__set_dv (id, n, x) bind(C, name='intrf_f__number__set_dv_')
  implicit none
  integer(kind=c_int), intent(in) :: id, n
  real(kind=c_double), intent(inout) :: x(n)
  call do_within('intrf_f__number__set_dv', mod_interfaces_name_, private_set_dv)
contains
  subroutine private_set_dv
    if (n > 1) then
       call intrf_f__template(private_sp1, private_dp1)
    else
       call intrf_f__template(private_sp2, private_dp2)
    end if
  end subroutine private_set_dv
  subroutine private_sp1
    type(sp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_dv(xx, real(x))
  end subroutine private_sp1
  subroutine private_dp1
    type(dp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_dv(xx, x)
  end subroutine private_dp1
   subroutine private_sp2
    type(sp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_dv(xx, real(x(1)))
  end subroutine private_sp2
  subroutine private_dp2
    type(dp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_dv(xx, x(1))
  end subroutine private_dp2
end subroutine intrf_f__number__set_dv

!> Sets the values of a number slice to the given array
!!
!! @param[in] id c_int, number id
!! @param[in] v c_double(:), array with the slice vlaues
!! @param[in] s c_int(:,:), array with the slice indexes
!! @param[in] l c_int, size of v
!! @param[in] m c_int, number of rows of s
!! @param[in] n c_int, number of columns of s
subroutine intrf_f__number__set_slice_v (id, v, s, m, n, l) bind(C, name='intrf_f__number__set_slice_v_')
  implicit none
  integer(kind=c_int), intent(in) :: id, l, m, n, s(m,n)
  real(kind=c_double), intent(in) :: v(l)
  call do_within('intrf_f__number__set_slice_v', mod_interfaces_name_, private_set_slice_v)
contains
  subroutine private_set_slice_v
    if (l > 1) then
       call intrf_f__template(private_sp1, private_dp1)
    else
       call intrf_f__template(private_sp2, private_dp2)
    end if
  end subroutine private_set_slice_v
  subroutine private_sp1
    type(sp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_slice_v(xx, s, real(v))
  end subroutine private_sp1
  subroutine private_dp1
    type(dp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_slice_v(xx, s, v)
  end subroutine private_dp1
   subroutine private_sp2
    type(sp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_slice_v(xx, s, real(v(1)))
  end subroutine private_sp2
  subroutine private_dp2
    type(dp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_slice_v(xx, s, v(1))
  end subroutine private_dp2
end subroutine intrf_f__number__set_slice_v

!> Sets the derivative values of a number slice to the given array
!!
!! @param[in] id c_int, number id
!! @param[in] v c_double(:), array with the slice vlaues
!! @param[in] s c_int(:,:), array with the slice indexes
!! @param[in] l c_int, size of v
!! @param[in] m c_int, number of rows of s
!! @param[in] n c_int, number of columns of s
subroutine intrf_f__number__set_slice_dv (id, dv, s, m, n, l) &
     bind(C, name='intrf_f__number__set_slice_dv_')
  implicit none
  integer(kind=c_int), intent(in) :: id, l, m, n, s(m,n)
  real(kind=c_double), intent(in) :: dv(l)
  call do_within('intrf_f__number__set_slice_dv', mod_interfaces_name_, private_set_slice_dv)
contains
  subroutine private_set_slice_dv
    if (l > 1) then
       call intrf_f__template(private_sp1, private_dp1)
    else
       call intrf_f__template(private_sp2, private_dp2)
    end if
  end subroutine private_set_slice_dv
  subroutine private_sp1
    type(sp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_slice_dv(xx, s, real(dv))
  end subroutine private_sp1
  subroutine private_dp1
    type(dp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_slice_dv(xx, s, dv)
  end subroutine private_dp1
   subroutine private_sp2
    type(sp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_slice_dv(xx, s, real(dv(1)))
  end subroutine private_sp2
  subroutine private_dp2
    type(dp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_slice_dv(xx, s, dv(1))
  end subroutine private_dp2
end subroutine intrf_f__number__set_slice_dv

!> Sets the values of a number flat slice to the given array
!!
!! @param[in] id c_int, number id
!! @param[in] v c_double(:), array with the slice vlaues
!! @param[in] s c_int(:,:), array with the slice indexes
!! @param[in] l c_int, size of v
!! @param[in] m c_int, size of s
subroutine intrf_f__number__set_flat_slice_v (id, v, s, m, l) bind(C, name='intrf_f__number__set_flat_slice_v_')
  implicit none
  integer(kind=c_int), intent(in) :: id, l, m, s(m)
  real(kind=c_double), intent(in) :: v(l)
  call do_within('intrf_f__number__set_flat_slice_v', mod_interfaces_name_, private_set_flat_slice_v)
contains
  subroutine private_set_flat_slice_v
    if (l > 1) then
       call intrf_f__template(private_sp1, private_dp1)
    else
       call intrf_f__template(private_sp2, private_dp2)
    end if
  end subroutine private_set_flat_slice_v
  subroutine private_sp1
    type(sp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_flat_slice_v(xx, s, real(v))
  end subroutine private_sp1
  subroutine private_dp1
    type(dp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_flat_slice_v(xx, s, v)
  end subroutine private_dp1
   subroutine private_sp2
    type(sp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_flat_slice_v(xx, s, real(v(1)))
  end subroutine private_sp2
  subroutine private_dp2
    type(dp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_flat_slice_v(xx, s, v(1))
  end subroutine private_dp2
end subroutine intrf_f__number__set_flat_slice_v

!> Sets the derivative values of a number flat slice to the given array
!!
!! @param[in] id c_int, number id
!! @param[in] v c_double(:), array with the slice vlaues
!! @param[in] s c_int(:,:), array with the slice indexes
!! @param[in] l c_int, size of v
!! @param[in] m c_int, size of s
subroutine intrf_f__number__set_flat_slice_dv (id, dv, s, m, l) &
     bind(C, name='intrf_f__number__set_flat_slice_dv_')
  implicit none
  integer(kind=c_int), intent(in) :: id, l, m, s(m)
  real(kind=c_double), intent(in) :: dv(l)
  call do_within('intrf_f__number__set_flat_slice_dv', mod_interfaces_name_, private_set_flat_slice_dv)
contains
  subroutine private_set_flat_slice_dv
    if (l > 1) then
       call intrf_f__template(private_sp1, private_dp1)
    else
       call intrf_f__template(private_sp2, private_dp2)
    end if
  end subroutine private_set_flat_slice_dv
  subroutine private_sp1
    type(sp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_flat_slice_dv(xx, s, real(dv))
  end subroutine private_sp1
  subroutine private_dp1
    type(dp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_flat_slice_dv(xx, s, dv)
  end subroutine private_dp1
   subroutine private_sp2
    type(sp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_flat_slice_dv(xx, s, real(dv(1)))
  end subroutine private_sp2
  subroutine private_dp2
    type(dp_number), pointer :: xx
    call get_number(id, xx)
    call number__set_flat_slice_dv(xx, s, dv(1))
  end subroutine private_dp2
end subroutine intrf_f__number__set_flat_slice_dv

!> Gets the value of a 'number'
!! @param[in] id c_int, 'number' id
!! @param[in] n c_int, 'number' size
!! @param[out] x c_double, where to store the 'number' value
subroutine intrf_f__number__get_v (id, n, x) bind(C, name='intrf_f__number__get_v_')
  implicit none
  integer(kind=c_int), intent(in) :: id, n
  real(kind=c_double), intent(out) :: x(n)
  call do_within('intrf_f__number__get_v', mod_interfaces_name_, private_get_x)
contains
  subroutine private_get_x
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_get_x
  subroutine private_sp
    real(kind=sp_), allocatable :: v(:)
    call number__get_v(nnn(id, real(0)), v)
    if (err_free()) x = dble(v)
  end subroutine private_sp
  subroutine private_dp
    real(kind=dp_), allocatable :: v(:)
    call number__get_v(nnn(id, dble(0)), v)
    if (err_free()) x = v
  end subroutine private_dp
end subroutine intrf_f__number__get_v

!> Gets the derivative value of a 'number'
!! @param[in] id c_int, 'number' id
!! @param[in] n c_int, 'number' size
!! @param[out] x c_double, where to store the 'number' derivative value
subroutine intrf_f__number__get_dv (id, n, x) bind(C, name='intrf_f__number__get_dv_')
  implicit none
  integer(kind=c_int), intent(in) :: id, n
  real(kind=c_double), intent(out) :: x(n)
  real(dp_), allocatable :: xx(:)
  call do_within('intrf_f__number__get_dv', mod_interfaces_name_, private_get_dx)
contains
  subroutine private_get_dx
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_get_dx
  subroutine private_sp
    real(kind=sp_), allocatable :: v(:)
    call number__get_dv(nnn(id, real(0)), v)
    if (err_free()) x = dble(v)
  end subroutine private_sp
  subroutine private_dp
    real(kind=dp_), allocatable :: v(:)
    call number__get_dv(nnn(id, dble(0)), v)
    if (err_free()) x = v
  end subroutine private_dp
end subroutine intrf_f__number__get_dv

!> Gets the 'node' id that has as output a certain 'number'
!! @param[in] id c_int, 'number' id
!! @param[out] ans c_int, 'node' id
subroutine intrf_f__number__get_nd (id, ans) bind(C, name='intrf__number__get_nd_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: ans
  call do_within('intrf_f__number__get_nd', mod_interfaces_name_, private_get_nd)
contains
  subroutine private_get_nd
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_get_nd
  subroutine private_sp
    ans = number__get_nd(nnn(id, real(0)))
  end subroutine private_sp
  subroutine private_dp
    ans = number__get_nd(nnn(id, dble(0)))
  end subroutine private_dp
end subroutine intrf_f__number__get_nd

!> Checks if a number as input depencies
!! @param[in] id c_int, 'number' id
!! @param[out] ans c_int, 1 if the 'number' is free else 0
subroutine intrf_f__number__inlock_free (id, ans) bind(C, name='intrf_f__number__inlock_free_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: ans
  call do_within('intrf_f__number__inlock_fee', mod_interfaces_name_, private_inlock_free)
contains
  subroutine private_inlock_free
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_inlock_free
  subroutine private_sp
    call assert(allocated(sp_NUMBERS_), err_notAlloc_, "sp_NUMBERS_")
    if (err_free()) then
       ans = merge(1, 0, number__inlock_free(nnn(id, real(0))))
    else
       ans = 0
    end if
  end subroutine private_sp
  subroutine private_dp
    call assert(allocated(dp_NUMBERS_), err_notAlloc_, "dp_NUMBERS_")
    if (err_free()) then
       ans = merge(1, 0, number__inlock_free(nnn(id, dble(0))))
    else
       ans = 0
    end if
  end subroutine private_dp
end subroutine intrf_f__number__inlock_free

!> Pop (removes) a 'number' from the NUMBERS_ array.
!! @param[in] id c_int, 'number' id
subroutine intrf_f__number__pop (id) bind(C, name='intrf_f__number__pop_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  call do_within('intrf_f__number__pop', mod_interfaces_name_, private_pop)
contains
  subroutine private_pop
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_pop
  subroutine private_sp
    type(sp_number), pointer :: x
    call get_number(id, x)
    call number__pop(x)
  end subroutine private_sp
  subroutine private_dp
    type(dp_number), pointer :: x
    call get_number(id, x)
    call number__pop(x)
  end subroutine private_dp
end subroutine intrf_f__number__pop

!> Calls the garbage collectors for 'numbers'
subroutine intrf_f__number__gc () bind(C, name='intrf_f__number__gc_')
  implicit none
  call do_within('intrf_f__number__gc', mod_interfaces_name_, private_gc)
contains
  subroutine private_gc
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_gc
  subroutine private_sp
    call numbers_gc(real(0))
  end subroutine private_sp
  subroutine private_dp
    call numbers_gc(dble(0))
  end subroutine private_dp
end subroutine intrf_f__number__gc

!> Performs the 'node' operator that generated a certain 'number'
!! @param[in] id c_int, 'number' id
subroutine intrf_f__number__op (id) bind(C, name='intrf_f__number__op_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  call do_within("intrf_f__number__op", mod_interfaces_name_, private_op)
contains
  subroutine private_op
    call intrf_f__template(private_dp, private_sp)
  end subroutine private_op
  subroutine private_sp
    type(sp_number), pointer :: x
    call get_number(id, x)
    call number__op(x)
  end subroutine private_sp
  subroutine private_dp
    type(dp_number), pointer :: x
    call get_number(id, x)
    call number__op(x)
  end subroutine private_dp
end subroutine intrf_f__number__op

!> Perfroms the forward differentiation relative to the node
!! that generated a certain 'number'
!! @param[in] id c_int, 'number' id
subroutine intrf_f__number__fw (id) bind(C, name='intrf_f__number__fw_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  call do_within("intrf_f__number__fw", mod_interfaces_name_, private_fw)
contains
  subroutine private_fw
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_fw
  subroutine private_sp
    type(sp_number), pointer :: x
    call get_number(id, x)
    call number__fw(x)
  end subroutine private_sp
  subroutine private_dp
    type(dp_number), pointer :: x
    call get_number(id, x)
    call number__fw(x)
  end subroutine private_dp
end subroutine intrf_f__number__fw

!> Reset the dx of the 'node' gnerating a certain 'number' according to the
!! backward differentiation schema.
!! @param[in] id c_int, 'number' id
subroutine intrf_f__number__bw_zero (id) bind(C, name='intrf_f__number__bw_zero_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  call do_within("intrf_f__number__bw_zero", mod_interfaces_name_, private_bw_zero)
contains
  subroutine private_bw_zero
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_bw_zero
  subroutine private_sp
    type(sp_number), pointer :: x
    call get_number(id, x)
    call number__bw_zero(x)
  end subroutine private_sp
  subroutine private_dp
    type(dp_number), pointer :: x
    call get_number(id, x)
    call number__bw_zero(x)
  end subroutine private_dp
end subroutine intrf_f__number__bw_zero

!> Perfroms the backward differentiation relative to the node
!! that generated a certain 'number'
!! @param[in] id c_int, 'number' id
subroutine intrf_f__number__bw (id) bind(C, name='intrf_f__number__bw_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  call do_within("intrf_f__number__op", mod_interfaces_name_, private_bw)
contains
  subroutine private_bw
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_bw
  subroutine private_sp
    type(sp_number), pointer :: x
    call get_number(id, x)
    call number__bw(x)
  end subroutine private_sp
  subroutine private_dp
    type(dp_number), pointer :: x
    call get_number(id, x)
    call number__bw(x)
  end subroutine private_dp
end subroutine intrf_f__number__bw

!> @}
