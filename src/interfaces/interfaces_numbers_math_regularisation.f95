!> @addtogroup api__numbers_math_regularisation_
!! @{

!> Dropput opertor
!!
!! @param[in] id1 inout number identifier
!! @param[in] r dropout rate [0, 1]
!! @param[out] idout output number identifier
subroutine intrf_f__number__dropout (id1, r, idout) bind(C, name='intrf_f__number__dropout_')
  implicit none
  integer(kind=c_int), intent(in) :: id1
  real(kind=c_double), intent(in) :: r
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__dropout', mod_interfaces_name_, private_dropout)
contains
  subroutine private_dropout
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_dropout
  subroutine private_sp
    call intrf_f__ans_id(number__dropout(nnn(id1, real(0)), real(r)), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__dropout(nnn(id1, dble(0)), dble(r)), idout)
  end subroutine private_dp
end subroutine intrf_f__number__dropout

!> @}
