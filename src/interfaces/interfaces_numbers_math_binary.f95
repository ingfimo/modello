!> @addtogroup api__numbers_math_binary_
!! @{

!> Interface for Addition
!! @param[in] id1 c_int, id of the first input 'number'
!! @param[in] id2 c_int, id of the second input 'number'
!! @param[out] idout c_int, id of the output 'number'
subroutine intrf_f__number__add (id1, id2, idout) bind(C, name='intrf_f__number__add_')
  implicit none
  integer(kind=c_int), intent(in) :: id1, id2
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__add', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(nnn(id1, real(0)) + nnn(id2, real(0)), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(nnn(id1, dble(0)) + nnn(id2, dble(0)), idout)
  end subroutine private_dp
end subroutine intrf_f__number__add

!> Interface for Subtraction
!! @param[in] id1 c_int, id of the first input 'number'
!! @param[in] id2 c_int, id of the second input 'number'
!! @param[out] idout c_int, id of the output 'number'
subroutine intrf_f__number__sub (id1, id2, idout) bind(C, name='intrf_f__number__sub_')
  implicit none
  integer(kind=c_int), intent(in) :: id1, id2
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__sub', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    if (id2 > 0) then
       call intrf_f__template(private_sp1, private_dp1)
    else
       call intrf_f__template(private_sp2, private_dp2)
    end if
  end subroutine private_do
  subroutine private_sp1
    call intrf_f__ans_id(nnn(id1, real(0)) - nnn(id2, real(0)), idout)
  end subroutine private_sp1
  subroutine private_dp1
    call intrf_f__ans_id(nnn(id1, dble(0)) - nnn(id2, dble(0)), idout)
  end subroutine private_dp1
  subroutine private_sp2
    call intrf_f__ans_id(-nnn(id1, real(0)), idout)
  end subroutine private_sp2
  subroutine private_dp2
    call intrf_f__ans_id(-nnn(id1, dble(0)), idout)
  end subroutine private_dp2
end subroutine intrf_f__number__sub

!> Interface for Multiplication
!! @param[in] id1 c_int, id of the first input 'number'
!! @param[in] id2 c_int, id of the second input 'number'
!! @param[out] idout c_int, id of the output 'number'
subroutine intrf_f__number__mult (id1, id2, idout) bind(C, name='intrf_f__number__mult_')
  implicit none
  integer(kind=c_int), intent(in) :: id1, id2
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__mult', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(nnn(id1, real(0)) * nnn(id2, real(0)), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(nnn(id1, dble(0)) * nnn(id2, dble(0)), idout)
  end subroutine private_dp
end subroutine intrf_f__number__mult

!> Interface for Power
!! @param[in] id1 c_int, id of the first input 'number'
!! @param[in] id2 c_int, id of the second input 'number'
!! @param[out] idout c_int, id of the output 'number'
subroutine intrf_f__number__pow (id1, id2, idout) bind(C, name='intrf_f__number__pow_')
  implicit none
  integer(kind=c_int), intent(in) :: id1, id2
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__pow', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(nnn(id1, real(0)) ** nnn(id2, real(0)), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(nnn(id1, dble(0)) ** nnn(id2, dble(0)), idout)
  end subroutine private_dp
end subroutine intrf_f__number__pow

!> Interface for Division
!! @param[in] id1 c_int, id of the first input 'number'
!! @param[in] id2 c_int, id of the second input 'number'
!! @param[out] idout c_int, id of the output 'number'
subroutine intrf_f__number__div (id1, id2, idout) bind(C, name='intrf_f__number__div_')
  implicit none
  integer(kind=c_int), intent(in) :: id1, id2
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__div', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(nnn(id1, real(0)) / nnn(id2, real(0)), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(nnn(id1, dble(0)) / nnn(id2, dble(0)), idout)
  end subroutine private_dp
end subroutine intrf_f__number__div

!> @}
