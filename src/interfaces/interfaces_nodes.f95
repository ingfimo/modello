!> @addtogroup api__nodes_methods_
!! @{

!> Allocates the NODES_ array
!! @param[in] n c_int, size of the array
subroutine intrf_f__allocate_nodes (n) bind(C, name='intrf_f__allocate_nodes_')
  implicit none
  integer(kind=c_int), intent(in) :: n
  call do_within('intrf_f__allocate_nodes', mod_interfaces_name_, private_allocate)
contains
  subroutine private_allocate
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_allocate
  subroutine private_sp
    call allocate_nodes(n, real(0))
  end subroutine private_sp
  subroutine private_dp
    call allocate_nodes(n, dble(0))
  end subroutine private_dp
end subroutine intrf_f__allocate_nodes

!> Deallocates the NODES_ array
subroutine intrf_f__deallocate_nodes () bind(C, name='intrf_f__deallocate_nodes_')
  implicit none
  call do_within('intrf_f_deallocate_nodes', mod_interfaces_name_, private_deallocate)
contains
  subroutine private_deallocate
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_deallocate
  subroutine private_sp
    call deallocate_nodes(real(0))
  end subroutine private_sp
  subroutine private_dp
    call deallocate_nodes(dble(0))
  end subroutine private_dp
end subroutine intrf_f__deallocate_nodes

subroutine intrf_f__node__get_ipar (id, sz, n, p) bind(C, name="intrf_f__node__get_ipar_")
  implicit none
  integer(kind=c_int), intent(in) :: id, sz, n
  integer(kind=c_int), intent(out) :: p(n)
  integer, allocatable :: pp(:)
  call do_within("intrf_f__node__get_ipar", mod_interfaces_name_, private_get)
contains
  subroutine private_get
    call intrf_f__template(private_sp, private_dp)
    if (sz > 0) then
       p(1) = size(pp)
    else
       p = pp
    end if
  end subroutine private_get
  subroutine private_sp
    call node__get_ipar(nnd(id, 1._sp_), pp)
  end subroutine private_sp
  subroutine private_dp
    call node__get_ipar(nnd(id, 1._dp_), pp)
  end subroutine private_dp
end subroutine intrf_f__node__get_ipar

subroutine intrf_f__node__get_rpar (id, sz, n, p) bind(C, name="intrf_f__node__get_rpar_")
  implicit none
  integer(kind=c_int), intent(in) :: id, sz
  integer(kind=c_int), intent(inout) :: n
  real(kind=c_double), intent(out) :: p(n)
  call do_within("intrf_f__node__get_ipar", mod_interfaces_name_, private_get)
contains
  subroutine private_get
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_get
  subroutine private_sp
    real(kind=sp_), allocatable :: pp(:)
    call node__get_rpar(nnd(id, 1._sp_), pp)
    if (sz > 0) then
       n = size(pp)
    else
       p = dble(pp)
    end if
  end subroutine private_sp
  subroutine private_dp
    real(kind=dp_), allocatable :: pp(:)
    call node__get_rpar(nnd(id, 1._dp_), pp)
    if (sz > 0) then
       p(1) = size(pp)
    else
       p = pp
    end if
  end subroutine private_dp
end subroutine intrf_f__node__get_rpar

subroutine intrf_f__node__get_state (id, i, sz, n, s) bind(C, name="intrf_f__node__get_state_")
  implicit none
  integer(kind=c_int), intent(in) :: id, i, sz, n
  integer(kind=c_int), intent(out) :: s(n)
  integer, allocatable :: state(:)
  call do_within("intrf_f__node__get_state", mod_interfaces_name_, private_get)
contains
  subroutine private_get
    call intrf_f__template(private_sp, private_dp)
    if (sz > 0) then
       s(1) = size(state)
    else
       s = state
    end if
  end subroutine private_get
  subroutine private_sp
    call node__get_state(nnd(id, 1._sp_), i, state)  
  end subroutine private_sp
  subroutine private_dp
    call node__get_state(nnd(id, 1._dp_), i, state)
  end subroutine private_dp
end subroutine intrf_f__node__get_state

subroutine intrf_f__node__set_state (id, i, n, s) bind(C, name="intrf_f__node__set_state_")
  implicit none
  integer(kind=c_int), intent(in) :: id, i, n
  integer(kind=c_int), intent(in) :: s(n)
  call do_within("intrf_f__node__set_state", mod_interfaces_name_, private_set)
contains
  subroutine private_set
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_set
  subroutine private_sp
    call node__set_state(nnd(id, 1._sp_), i, s)  
  end subroutine private_sp
  subroutine private_dp
    call node__set_state(nnd(id, 1._dp_), i, s)
  end subroutine private_dp
end subroutine intrf_f__node__set_state

!> Allocates the GRAPHS_ array
!! @param[in] n c_int, array size
subroutine intrf_f__allocate_graphs (n) bind(C, name='intrf_f__allocate_graphs_')
  implicit none
  integer(kind=c_int), intent(in) :: n
  call do_within('intrf_f__allocate_graphs', mod_interfaces_name_, private_allocate)
contains
  subroutine private_allocate
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_allocate
  subroutine private_sp
    call allocate_graphs(n, real(0))
  end subroutine private_sp
  subroutine private_dp
    call allocate_graphs(n, dble(0))
  end subroutine private_dp
end subroutine intrf_f__allocate_graphs

!> Checks if a 'graph' is allocated
!! @param[in] id c_int, 'graph' id
!! @param[out] ans c_int, if is allocated equal to 1 otherwise to 0
subroutine intrf_f__graph__is_allocated (id, ans) bind(C, name='intrf_f__graph__is_allocated_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: ans
  call do_within('intrf_f__graph_is_allocated', mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    if (allocated(sp_GRAPHS_)) then
       call assert(id > 0 .and. id <= size(sp_GRAPHS_), err_oorng_, 'id')
       if (err_free()) ans = merge(1, 0, is_allocated(sp_GRAPHS_(id)))
    else
       ans = 0
    end if
  end subroutine private_sp
  subroutine private_dp
    if (allocated(dp_GRAPHS_)) then
       call assert(id > 0 .and. id <= size(dp_GRAPHS_), err_oorng_, 'id')
       if (err_free()) ans = merge(1, 0, is_allocated(dp_GRAPHS_(id)))
    else
       ans = 0
    end if
  end subroutine private_dp
end subroutine intrf_f__graph__is_allocated

!> Deallocates the GRAPHS_ array
subroutine intrf_f__deallocate_graphs () bind(C, name='intrf_f__deallocate_graphs_')
  implicit none
  call do_within('intrf_f_deallocate_ndoes', mod_interfaces_name_, private_deallocate)
contains
  subroutine private_deallocate
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_deallocate
  subroutine private_sp
    call deallocate_graphs(real(0))
  end subroutine private_sp
  subroutine private_dp
    call deallocate_graphs(dble(0))
  end subroutine private_dp
end subroutine intrf_f__deallocate_graphs

!> Open an existing or new 'graph', and puts it on the stack
!! @param[in] i c_inf, 'graph' id, if < 1 it opens a new 'graph'
subroutine intrf_f__graph__open (i, wdx) bind(C, name='intrf_f__graph__open_')
  implicit none
  integer(kind=c_int), intent(in) :: i, wdx
  call do_within('intrf_f__graph__open', mod_interfaces_name_, private_open)
contains
  subroutine private_open
    if (i > 0) then
       call intrf_f__template(private_sp1, private_dp1)
    else
       call intrf_f__template(private_sp2, private_dp2)
    end if
  end subroutine private_open
  subroutine private_sp1
    call graph__open(ggg(i, real(0)))
  end subroutine private_sp1
  subroutine private_dp1
    call graph__open(ggg(i, dble(0)))
  end subroutine private_dp1
  subroutine private_sp2
    call graph__open(real(0), wdx > 0)
  end subroutine private_sp2
  subroutine private_dp2
    call graph__open(dble(0), wdx > 0)
  end subroutine private_dp2
end subroutine intrf_f__graph__open

!> Closes the 'graph' on the stack
subroutine intrf_f__graph__close () bind(C, name='intrf_f__graph__close_')
  implicit none
  call do_within('intrf_f__graph__close', mod_interfaces_name_, private_close)
contains
  subroutine private_close
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_close
  subroutine private_sp
    call graph__close(real(0))
  end subroutine private_sp
  subroutine private_dp
    call graph__close(dble(0))
  end subroutine private_dp
end subroutine intrf_f__graph__close

!> Pops (removes) a 'graph' from the GRAPHS_ array
!! @param[in] gi c_int, 'graph' id
subroutine intrf_f__graph__pop (gi) bind(C, name='intrf_f__graph__pop_')
  implicit none
  integer(kind=c_int), intent(in) :: gi
  call do_within('intrf_f__graph__pop', mod_interfaces_name_, private_pop)
contains
  subroutine private_pop
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_pop
  subroutine private_sp
    type(sp_graph), pointer :: g
    call get_graph(gi, g)
    call graph__pop(g)
  end subroutine private_sp
  subroutine private_dp
    type(dp_graph), pointer :: g
    call get_graph(gi, g)
    call graph__pop(g)
  end subroutine private_dp
end subroutine intrf_f__graph__pop

!> Calls the garbage collector for 'graphs'
subroutine intrf_f__graph__gc () bind(C, name='intrf_f__graph__gc_')
  implicit none
  call do_within('intrf_f__graph__gc', mod_interfaces_name_, private_gc)
contains
  subroutine private_gc
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_gc
  subroutine private_sp
    call graphs_gc(real(0))
  end subroutine private_sp
  subroutine private_dp
    call graphs_gc(dble(0))
  end subroutine private_dp
end subroutine intrf_f__graph__gc

!> Gets the indes of the 'graph' on the stack
!! @param[out] i c_int, index of the 'graph' on the stack
subroutine intrf_f__get_graphi (i) bind(C, name='intrf_f__get_graphi_')
  implicit none
  integer(kind=c_int), intent(out) :: i
  call do_within('intrf_f__get_graphi', mod_interfaces_name_, private_get)
contains
  subroutine private_get
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_get
  subroutine private_sp
    i = get_graphi(real(0))
  end subroutine private_sp
  subroutine private_dp
    i = get_graphi(dble(0))
  end subroutine private_dp
end subroutine intrf_f__get_graphi

!> Performs all the 'node' operators within a certain graph
!! @param[in] gi c_int, 'graph' id
subroutine intrf_f__graph__op (gi) bind(C, name='intrf_f__graph__op_')
  implicit none
  integer(kind=c_int), intent(in) :: gi
  call do_within('intrf_f__graph__op', mod_interfaces_name_, private_op)
contains
  subroutine private_op
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_op
  subroutine private_sp
    call graph__op(ggg(gi, real(0)))
  end subroutine private_sp
  subroutine private_dp
    call graph__op(ggg(gi, dble(0)))
  end subroutine private_dp
end subroutine intrf_f__graph__op

!> Performs all the 'node' forward differentiation operators
!! within a certain graph
!! @param[in] gi c_int, 'graph' id
subroutine intrf_f__graph__fw (gi) bind(C, name='intrf_f__graph__fw_')
  implicit none
  integer(kind=c_int), intent(in) :: gi
  call do_within('intrf_f__graph__fw', mod_interfaces_name_, private_fw)
contains
  subroutine private_fw
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_fw
  subroutine private_sp
    call graph__fw(ggg(gi, real(0)))
  end subroutine private_sp
  subroutine private_dp
    call graph__fw(ggg(gi, dble(0)))
  end subroutine private_dp
end subroutine intrf_f__graph__fw

!> Reset all the dx within a certain 'graph' according to the
!! backward differentiation schema
!! @param[in] gi c_int, 'grph' id
subroutine intrf_f__graph__bw_zero (gi) bind(C, name='intrf_f__graph__bw_zero_')
  implicit none
  integer(kind=c_int), intent(in) :: gi
  call do_within('intrf_f__graph__bw_zero', mod_interfaces_name_, private_bw_zero)
contains
  subroutine private_bw_zero
    type(graph) :: x
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_bw_zero
  subroutine private_sp
    call graph__bw_zero(ggg(gi, real(0)))
  end subroutine private_sp
  subroutine private_dp
    call graph__bw_zero(ggg(gi, dble(0)))
  end subroutine private_dp
end subroutine intrf_f__graph__bw_zero

!> Performs all the 'node' backward differentiation operators
!! within a certain graph
!! @param[in] gi c_int, 'graph' id
subroutine intrf_f__graph__bw (gi) bind(C, name='intrf_f__graph__bw_')
  implicit none
  integer(kind=c_int), intent(in) :: gi
  call do_within('intrf_f__graph__bw', mod_interfaces_name_, private_bw)
contains
  subroutine private_bw
    type(graph) :: x
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_bw
  subroutine private_sp
    call graph__bw(ggg(gi, real(0)))
  end subroutine private_sp
  subroutine private_dp
    call graph__bw(ggg(gi, dble(0)))
  end subroutine private_dp
end subroutine intrf_f__graph__bw

!> @}
