!> @addtogroup api__numbers_math_modifiers_
!! @{

subroutine intrf_f__number__assign (id1, id2) bind(C, name="intrf_f__number__assign_")
  implicit none
  integer(kind=c_int), intent(in) :: id1, id2
  call do_within("intrf_f__number__assign", mod_interfaces_name_, private_assign)
contains
  subroutine private_assign
    call intrf_f__template(private_sp, private_dp) 
  end subroutine private_assign
  subroutine private_sp
    type(sp_number), pointer :: x2
    call get_number(id2, x2)
    call number__assign(nnn(id1, real(0)), x2)
  end subroutine private_sp
  subroutine private_dp
    type(dp_number), pointer :: x2
    call get_number(id2, x2)
    call number__assign(nnn(id1, dble(0)), x2)
  end subroutine private_dp
end subroutine intrf_f__number__assign

!> Interface for Slice
!! Slices a 'number' according to the provided set od indexes.
!! @param[in] id c_int, id of the 'number' to slice
!! @param[in] n c_int, size of the slice
!! @param[in] s c_int, indexes of the slice
!! @param[out] idoout c_int, id of the slice 'number'
subroutine intrf_f__number__slice (id, n, s, idout) bind(C, name='intrf_f__number__slice_')
  implicit none
  integer(kind=c_int), intent(in) :: id, n
  integer(kind=c_int), intent(in), target :: s(n)
  integer(kind=c_int), intent(out) :: idout
  integer, pointer :: ss(:,:)
  call do_within("intrf_f__number__slice", mod_interfaces_name_, private_slice)
contains
  subroutine private_slice
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_slice
  subroutine private_sp
    call assert(modulo(n, get_rank(nnn(id, real(0)))) == 0, err_wrngArg_, 's')
    if (err_free()) then
       ss(1:get_rank(nnn(id, real(0))),1:(n/get_rank(nnn(id, real(0))))) => s
       call intrf_f__ans_id(number__slice(nnn(id, real(0)), ss), idout)
    end if
  end subroutine private_sp
  subroutine private_dp
    call assert(modulo(n, get_rank(nnn(id, dble(0)))) == 0, err_wrngArg_, 's')
    if (err_free()) then
       ss(1:get_rank(nnn(id, dble(0))),1:(n/get_rank(nnn(id, dble(0))))) => s
       call intrf_f__ans_id(number__slice(nnn(id, dble(0)), ss), idout)
    end if
  end subroutine private_dp
end subroutine intrf_f__number__slice

!> Interface for Flat Slice
!! Gets the flat slice of 'number' given the slice indexes
!! @param[in] id c_int, if of the number to slice
!! @param[in] n c_int, size of the slice
!! @param[in] s c_int, indexes of the flat slice
!! @param[out] idout c_int, id of the flat slice 'number'
subroutine intrf_f__number__flat_slice (id, n, s, idout) bind(C, name='intrf_f__number__flat_slice_')
  implicit none
  integer(kind=c_int), intent(in) :: id, n, s(n)
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__flat_slice", mod_interfaces_name_, private_slice)
contains
  subroutine private_slice
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_slice
  subroutine private_sp
    call intrf_f__ans_id(number__flat_slice(nnn(id, real(0)), s), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__flat_slice(nnn(id, dble(0)), s), idout)
  end subroutine private_dp
end subroutine intrf_f__number__flat_slice

!> Interface for Contiguous Slice
!! Takes a contiguos slice from  a 'number'
!! @param[in] id c_int, id of the 'number' to slice
!! @param[in] s c_int, vector of two element: start indexes and final index of the slice
!! @param[out] idout c_int, id of the 'number' containing conguos slice
subroutine intrf_f__number__contiguous_slice (id, s1, s2, idout) bind(C, name='intrf_f__number__contiguous_slice_')
  implicit none
  integer(kind=c_int), intent(in) :: id, s1, s2
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__contiguous_slice", mod_interfaces_name_, private_slice)
contains
  subroutine private_slice
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_slice
  subroutine private_sp
    call intrf_f__ans_id(number__contiguous_slice(nnn(id, real(0)), s1, s2), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__contiguous_slice(nnn(id, dble(0)), s1, s2), idout)
  end subroutine private_dp
end subroutine intrf_f__number__contiguous_slice

!> Interface for Reshape
!! Reshapes a number according to the given shape
!! @param[in] id c_int, id of the 'number' to reshape
!! @param[in] n c_int, size of the shape
!! @param[in] shp c_int, new shape vector
!! @param[out] idput c_int, id of the reshaped 'number'
subroutine intrf_f__number__reshape (id, n, shp, idout) bind(C, name='intrf_f__number__reshape_')
  implicit none
  integer(kind=c_int), intent(in) :: id, n, shp(n)
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__reshape', mod_interfaces_name_, private_reshape)
contains
  subroutine private_reshape
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_reshape
  subroutine private_sp
    call intrf_f__ans_id(number__reshape(nnn(id, real(0)), shp), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__reshape(nnn(id, dble(0)), shp), idout)
  end subroutine private_dp
end subroutine intrf_f__number__reshape

!> Interface for Drop
!! Reshape a 'number' by dropping the collapsed dimensions of a 'number'
!! @param[in] id c_int, id of the 'number' to reshape
!! @param[out] idout c_int, id of the reshaped 'number'
subroutine intrf_f__number__drop_shape (id, idout) bind(C, name='intrf_f__number__drop_shape_')
  implicit none
  integer(kind=c_int), intent(in) :: id
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__drop_shape', mod_interfaces_name_, private_drop_shape)
contains
  subroutine private_drop_shape
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_drop_shape
  subroutine private_sp
    call intrf_f__ans_id(number__drop_shape(nnn(id, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__drop_shape(nnn(id, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__drop_shape

!> Interface for Bind
!! Binds two 'numbers' along the given dimension
!! @param[in] id1 c_int, id of the first 'number'
!! @param[in] id2 c_int, id of the second 'number'
!! @param[in] k c_int, dimension index
!! @param[out] idout c_int, id of the binded 'number'
subroutine intrf_f__number__bind (id1, id2, k, idout) bind(C, name='intrf_f__number__bind_')
  implicit none
  integer(kind=c_int), intent(in) :: id1, id2, k
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__bind', mod_interfaces_name_, private_bind)
contains
  subroutine private_bind
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_bind
  subroutine private_sp
    call intrf_f__ans_id(number__bind(nnn(id1, real(0)), nnn(id2, real(0)), k), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__bind(nnn(id1, dble(0)), nnn(id2, dble(0)), k), idout)
  end subroutine private_dp
end subroutine intrf_f__number__bind

!> Interface for Embeddings
!! Returns the embedding matrix corresponding the the provided number of fectors
!! @param[in] idf c_int, id of the factor 'number'
!! @param[in] idx c_int, id of the 'number' containing the embeddings
!! @param[in] n c_int, size (number of embeddings in) of the output embedding 'number'
!! @param[out] idout c_int, id of the output embedding 'number'
subroutine intrf_f__number__embeddings (idf, idx, shp, n, idout) bind(C, name='intrf_f__number__embeddings_')
  implicit none
  integer(kind=c_int), intent(in) :: idf, idx, n, shp(n)
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__embeddings', mod_interfaces_name_, private_emb)
contains
  subroutine private_emb
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_emb
  subroutine private_sp
    call intrf_f__ans_id(number__embeddings(nnn(idf, real(0)), nnn(idx, real(0)), shp), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__embeddings(nnn(idf, dble(0)), nnn(idx, dble(0)), shp), idout)
  end subroutine private_dp
end subroutine intrf_f__number__embeddings

!> Transposes a number of rank 2 (matrix)
!!
!! @param[in] id identifier of the number to transpose
!! @param[out] idout identifier of the output number
subroutine intrf_f__number__transpose (idx, idout) bind(C, name='intrf_f__number__transpose_')
  implicit none
  integer(kind=c_int), intent(in) :: idx
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__transpose', mod_interfaces_name_, private_transpose)
contains
  subroutine private_transpose
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_transpose
  subroutine private_sp
    call intrf_f__ans_id(number__transpose(nnn(idx, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__transpose(nnn(idx, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__transpose

!> Cretes a feeding-fed link btween two numbers
!!
!! @param[in] id identifier of the feeding number
!! @param[out] idout identifier of the fed number
!! @param[in] indexes indexes of the records to use for feeding
!! @param[in] shp shape of each record
!! @param[in] bsz batch size
!! @param[in] m size of shp
!! @param[in] n size of indexes
subroutine intrf_f__number__feeding_number (id, idout, indexes, shp, bsz, m, n) &
     bind(C, name="intrf_f__number__feeding_number_")
  implicit none
  integer(kind=c_int), intent(in) :: id, indexes(n), shp(m), bsz, m, n
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__feeding_number", mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(number__feeding_number(nnn(id, real(0)), indexes, shp, bsz), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__feeding_number(nnn(id, dble(0)), indexes, shp, bsz), idout)
  end subroutine private_dp
end subroutine intrf_f__number__feeding_number

!> Cretes a feeding-fed link btween two numbers,
!! in order to feed temporal sequences into the fed number
!!
!! @param[in] id identifier of the feeding number
!! @param[out] idout identifier of the fed number
!! @param[in] indexes indexes of the records to use for feeding
!! @param[in] shp shape of each record
!! @param[in] l sequence length
!! @param[in] bsz batch size
!! @param[in] m size of shp
!! @param[in] n size of indexes
subroutine intrf_f__number__feeding_sequence (id, idout, indexes, shp, l, bsz, b, m, n) &
     bind(C, name="intrf_f__number__feeding_sequence_")
  implicit none
  integer(kind=c_int), intent(in) :: id, indexes(n), shp(m), l, bsz, b, m, n
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__feeding_number", mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    call intrf_f__ans_id(number__feeding_sequence(nnn(id, real(0)), indexes, shp, l, bsz, b), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__feeding_sequence(nnn(id, dble(0)), indexes, shp, l, bsz, b), idout)
  end subroutine private_dp
end subroutine intrf_f__number__feeding_sequence

!> Triggers the feeding mechanism for fed numbers (output of a feeding method)
!!
!! @param[in] id identifiers of the fed numbers
!! @param[in] n size of id
!! @param[in] shuffle if > 0 the data will be shuffled
subroutine intrf_f__number__feed (id, n, idout) bind(C, name="intrf_f__number__feed_")
  implicit none
  integer(kind=c_int), intent(in) :: id(n), n
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__feed", mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    type(sp_node) :: x
    integer :: i
    x = number__feed([(nnn(id(i), real(0)), i=1, n)])
    if (err_free()) idout = x%attrs%id
  end subroutine private_sp
  subroutine private_dp
    type(dp_node) :: x
    integer :: i
    x = number__feed([(nnn(id(i), dble(0)), i=1, n)])
    if (err_free()) idout = x%attrs%id
  end subroutine private_dp
end subroutine intrf_f__number__feed

!> @}
