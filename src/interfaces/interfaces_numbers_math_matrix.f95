!> @addtogroup api__numbers_math_matrix_
!! @{

!> Interface for dp_gmmmult__0
!! @param[in] transA c_int, if > 0 op(A) = A**T else op(A) = A
!! @param[in] transB c_int, if > 0 op(B) = B**T else op(B) = B
!! @param[in] idAlpha c_int, id of the 'number' representing alpha
!! @param[in] idA c_int, if of 'number' representing A
!! @param[in] idB c_int, if of 'number' representing B
!! @param[in] idBeta c_int, if of 'number' representing beta
!! @param[in] idC c_int, if of 'number' representing C
!! @param[out] idout c_int, id of the outpput 'number'
subroutine intrf_f__number__gmmmult__1 (transA, transB, idAlpha, idA, idB, idBeta, &
     idC, idout) bind(C, name='intrf_f__number__gmmmult__1_')
  implicit none
  integer(kind=c_int), intent(in) :: transA, transB
  integer(kind=c_int), intent(in) :: idAlpha, idA, idB, idBeta, idC
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__gmmmult__1', mod_interfaces_name_, private_gmmmult)
contains
  subroutine private_gmmmult
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_gmmmult
  subroutine private_sp
    call intrf_f__ans_id(number__gmmmult( &
            transA, transB, &
            nnn(idAlpha, real(0)), &
            nnn(idA, real(0)), &
            nnn(idB, real(0)), &
            nnn(idBeta, real(0)), &
            nnn(idC, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__gmmmult( &
            transA, transB, &
            nnn(idAlpha, dble(0)), &
            nnn(idA, dble(0)), &
            nnn(idB, dble(0)), &
            nnn(idBeta, dble(0)), &
            nnn(idC, dble(0))), idout)
  end subroutine private_dp
    
end subroutine intrf_f__number__gmmmult__1

!> Interface for dp_gmmmult__1
!! @param[in] transA c_int, if > 0 op(A) = A**T else op(A) = A
!! @param[in] transB c_int, if > 0 op(B) = B**T else op(B) = B
!! @param[in] idAlpha c_int, id of the 'number' representing alpha
!! @param[in] idA c_int, if of 'number' representing A
!! @param[in] idB c_int, if of 'number' representing B
!! @param[out] idout c_int, id of the outpput 'number'
subroutine intrf_f__number__gmmmult__2 (transA, transB, idAlpha, idA, idB, idout) &
     bind(C, name='intrf_f__number__gmmmult__2_')
  implicit none
  integer(kind=c_int), intent(in) :: transA, transB
  integer(kind=c_int), intent(in) :: idAlpha, idA, idB
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__gmmmult__2', mod_interfaces_name_, private_gmmmult)
contains
  subroutine private_gmmmult
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_gmmmult
  subroutine private_sp
    call intrf_f__ans_id(number__gmmmult(nnn(idAlpha, real(0)), transA, transB, &
         nnn(idA, real(0)), nnn(idB, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__gmmmult(nnn(idAlpha, dble(0)), transA, transB, &
         nnn(idA, dble(0)), nnn(idB, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__gmmmult__2

!> Interface for dp_gmmmult__0
!! @param[in] transA c_int, if > 0 op(A) = A**T else op(A) = A
!! @param[in] transB c_int, if > 0 op(B) = B**T else op(B) = B
!! @param[in] idA c_int, if of 'number' representing A
!! @param[in] idB c_int, if of 'number' representing B
!! @param[in] idC c_int, if of 'number' representing C
!! @param[out] idout c_int, id of the outpput 'number'
subroutine intrf_f__number__gmmmult__3 (transA, transB, idA, idB, idC, idout) &
     bind(c, name='intrf_f__number__gmmmult__3_')
  implicit none
  integer(kind=c_int), intent(in) :: transA, transB
  integer(kind=c_int), intent(in) :: idA, idB, idC
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__gmmmult__3', mod_interfaces_name_, private_gmmmult)
contains
  subroutine private_gmmmult
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_gmmmult
  subroutine private_sp
    call intrf_f__ans_id( &
         number__gmmmult(transA, transB, &
         nnn(idA, real(0)), &
         nnn(idB, real(0)), &
         nnn(idC, real(0))), &
         idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id( &
         number__gmmmult(transA, transB, &
         nnn(idA, dble(0)), &
         nnn(idB, dble(0)), &
         nnn(idC, dble(0))), &
         idout)
  end subroutine private_dp
end subroutine intrf_f__number__gmmmult__3

!> Interface for dp_gmmmult__0
!! @param[in] transA c_int, if > 0 op(A) = A**T else op(A) = A
!! @param[in] transB c_int, if > 0 op(B) = B**T else op(B) = B
!! @param[in] idA c_int, if of 'number' representing A
!! @param[in] idB c_int, if of 'number' representing B
!! @param[out] idout c_int, id of the outpput 'number'
subroutine intrf_f__number__gmmmult__4 (transA, transB, idA, idB, idout) &
     bind(C, name='intrf_f__number__gmmmult__4_')
  implicit none
  integer(kind=c_int), intent(in) :: transA, transB
  integer(kind=c_int), intent(in) :: idA, idB
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__gmmmult__4', mod_interfaces_name_, private_gmmmult)
contains
  subroutine private_gmmmult
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_gmmmult
  subroutine private_sp
    call intrf_f__ans_id(number__gmmmult(transA, transB, &
         nnn(idA, real(0)), nnn(idB, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__gmmmult(transA, transB, &
         nnn(idA, dble(0)), nnn(idB, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__gmmmult__4

!> Interface for gmvmult__1
!! @param[in] trans c_int, if > 0 op(A) = A**T else op(A) = A
!! @param[in] idAlpha c_int, id of the 'number' representing alpha
!! @param[in] idA c_int, if of 'number' representing A
!! @param[in] idx c_int, if of 'number' representing x
!! @param[in] idBeta c_int, if of 'number' representing beta
!! @param[in] idy c_int, if of 'number' representing y
!! @param[out] idout c_int, id of the outpput 'number'
subroutine intrf_f__number__gmvmult__1 (trans, idAlpha, idA, idx, idBeta, idy, idout) &
     bind(C, name="intrf_f__number__gmvmult__1_")
  implicit none
  integer(kind=c_int), intent(in) :: trans, idAlpha, idA, idx, idBeta, idy
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__gmvmult__1", mod_interfaces_name_, private_gmvmult)
contains
  subroutine private_gmvmult
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_gmvmult
  subroutine private_sp
    call intrf_f__ans_id(number__gmvmult(trans, nnn(idAlpha, real(0)), nnn(idA, real(0)), &
         nnn(idx, real(0)), nnn(idBeta, real(0)), nnn(idy, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__gmvmult(trans, nnn(idAlpha, dble(0)), nnn(idA, dble(0)), &
         nnn(idx, dble(0)), nnn(idBeta, dble(0)), nnn(idy, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__gmvmult__1

!> Interface for gmvmult__2
!! @param[in] trans c_int, if > 0 op(A) = A**T else op(A) = A
!! @param[in] idAlpha c_int, id of the 'number' representing alpha
!! @param[in] idA c_int, if of 'number' representing A
!! @param[in] idx c_int, if of 'number' representing x
!! @param[out] idout c_int, id of the outpput 'number'
subroutine intrf_f__number__gmvmult__2 (trans, idAlpha, idA, idx, idout) &
     bind(C, name="intrf_f__number__gmvmult__2_")
  implicit none
  integer(kind=c_int), intent(in) :: trans, idAlpha, idA, idx
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__gmvmult__2", mod_interfaces_name_, private_gmvmult)
contains
  subroutine private_gmvmult
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_gmvmult
  subroutine private_sp
    call intrf_f__ans_id(number__gmvmult(nnn(idAlpha, real(0)), trans, &
         nnn(idA, real(0)), nnn(idx, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__gmvmult(nnn(idAlpha, dble(0)), trans, &
         nnn(idA, dble(0)), nnn(idx, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__gmvmult__2

!> Interface for gmvmult__3
!! @param[in] trans c_int, if > 0 op(A) = A**T else op(A) = A
!! @param[in] idA c_int, if of 'number' representing A
!! @param[in] idx c_int, if of 'number' representing x
!! @param[in] idy c_int, if of 'number' representing y
!! @param[out] idout c_int, id of the outpput 'number'
subroutine intrf_f__number__gmvmult__3 (trans, idA, idx, idy, idout) &
     bind(C, name="intrf_f__number__gmvmult__3_")
  implicit none
  integer(kind=c_int), intent(in) :: trans, idA, idx, idy
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__gmvmult__3", mod_interfaces_name_, private_gmvmult)
contains
  subroutine private_gmvmult
    call intrf_f__template(private_sp, private_dp)
   end subroutine private_gmvmult
   subroutine private_sp
     call intrf_f__ans_id(number__gmvmult(trans, nnn(idA, real(0)), &
          nnn(idx, real(0)), nnn(idy, real(0))), idout)
   end subroutine private_sp
   subroutine private_dp
     call intrf_f__ans_id(number__gmvmult(trans, nnn(idA, dble(0)), &
          nnn(idx, dble(0)), nnn(idy, dble(0))), idout)
   end subroutine private_dp
end subroutine intrf_f__number__gmvmult__3

!> Interface for gmvmult__4
!! @param[in] trans c_int, if > 0 op(A) = A**T else op(A) = A
!! @param[in] idA c_int, if of 'number' representing A
!! @param[in] idx c_int, if of 'number' representing x
!! @param[out] idout c_int, id of the outpput 'number'
subroutine intrf_f__number__gmvmult__4 (trans, idA, idx, idout) &
     bind(C, name="intrf_f__number__gmvmult__4_")
  implicit none
  integer(kind=c_int), intent(in) :: trans, idA, idx
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__gmvmult__4", mod_interfaces_name_, private_gmvmult)
contains
  subroutine private_gmvmult
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_gmvmult
  subroutine private_sp
    call intrf_f__ans_id(number__gmvmult(trans, nnn(idA, real(0)), nnn(idx, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__gmvmult(trans, nnn(idA, dble(0)), nnn(idx, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__gmvmult__4

!> Interface for vvouter__1
!! @param[in] idAlpah c_int, id of the 'number' representing alpha
!! @param[in] idx c_int, id of the 'number' representing x
!! @param[in] idy c_int, id of the 'number' representing y
!! @param[in] idz c_int, id of the 'number' representing z
!! @param[out] idout c_int, id of the output 'number'
subroutine intrf_f__number__vvouter__1 (idAlpha, idx, idy, idz, idout) &
     bind(C, name="intrf_f__number__vvouter__1_")
  implicit none
  integer(kind=c_int), intent(in) :: idAlpha, idx, idy, idz
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__vvouter__1", mod_interfaces_name_, private_ger)
contains
  subroutine private_ger
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_ger
  subroutine private_sp
    call intrf_f__ans_id(number__vvouter(nnn(idAlpha, real(0)), nnn(idx, real(0)), &
         nnn(idy, real(0)), nnn(idz, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__vvouter(nnn(idAlpha, dble(0)), nnn(idx, dble(0)), &
         nnn(idy, dble(0)), nnn(idz, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__vvouter__1

!> Interface for vvouter__2
!! @param[in] idx c_int, id of the 'number' representing x
!! @param[in] idy c_int, id of the 'number' representing y
!! @param[in] idz c_int, id of the 'number' representing z
!! @param[out] idout c_int, id of the output 'number'
subroutine intrf_f__number__vvouter__2 (idx, idy, idz, idout) &
     bind(C, name="intrf_f__number__vvouter__2_")
  implicit none
  integer(kind=c_int), intent(in) :: idx, idy, idz
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__vvouter__2", mod_interfaces_name_, private_ger)
contains
  subroutine private_ger
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_ger
  subroutine private_sp
    call intrf_f__ans_id(number__vvouter(nnn(idx, real(0)), nnn(idy, real(0)), &
         nnn(idz, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__vvouter(nnn(idx, dble(0)), nnn(idy, dble(0)), &
         nnn(idz, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__vvouter__2

!> Interface for vvouter__3
!! @param[in] idx c_int, id of the 'number' representing x
!! @param[in] idy c_int, id of the 'number' representing y
!! @param[out] idout c_int, id of the output 'number'
subroutine intrf_f__number__vvouter__3 (idx, idy, idout) &
     bind(C, name="intrf_f__number__vvouter__3_")
  implicit none
  integer(kind=c_int), intent(in) :: idx, idy
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__vvouter__3", mod_interfaces_name_, private_ger)
contains
  subroutine private_ger
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_ger
  subroutine private_sp
    call intrf_f__ans_id(number__vvouter(nnn(idx, real(0)), nnn(idy, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__vvouter(nnn(idx, dble(0)), nnn(idy, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__vvouter__3

!> Interface for vvinner
!! @param[in] idx c_int, id of the 'number' representing x
!! @param[in] idy c_int, id of the 'number' representing y
!! @param[out] idout c_int, id of the output 'number'
subroutine intrf_f__number__vvinner (idx, idy, idout) bind(C, name="intrf_f__number__vvinner_")
  implicit none
  integer(kind=c_int), intent(in) :: idx, idy
  integer(kind=c_int), intent(out) :: idout
  call do_within("intrf_f__number__vvinner", mod_interfaces_name_, private_dot)
contains
  subroutine private_dot
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_dot
  subroutine private_sp
    call intrf_f__ans_id(number__vvinner(nnn(idx, real(0)), nnn(idy, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__vvinner(nnn(idx, dble(0)), nnn(idy, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__vvinner

!> Interface for invMat
!! @param[in] idx c_int, id of the 'number' containing the matrix to invert
!! @param[out] idout c_int, id of the 'number' containing the inverted matrix
subroutine intrf_f__number__invMat (idx, idout) bind(C, name='intrf_f__number__invMat_')
  implicit none
  integer(kind=c_int), intent(in) :: idx
  integer(kind=c_int), intent(out) :: idout
  call do_within('intrf_f__number__invMat', mod_interfaces_name_, private_inv)
contains
  subroutine private_inv
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_inv
  subroutine private_sp
    call intrf_f__ans_id(number__invMat(nnn(idx, real(0))), idout)
  end subroutine private_sp
  subroutine private_dp
    call intrf_f__ans_id(number__invMat(nnn(idx, dble(0))), idout)
  end subroutine private_dp
end subroutine intrf_f__number__invMat

subroutine intrf_f__number__slidemmm (transA, transB, hashB, ida, idb, idc, bm, bn, hm, l, b) &
     bind(C, name="intrf_f__number__slidemmm_")
  implicit none
  integer(kind=c_int), intent(in) :: transA, transB, hashB, ida, idb, bm, bn, hm, l, b
  integer(kind=c_int), intent(out) :: idc
  call do_within("intrf_f__number__slidemmm", mod_interfaces_name_, private_do)
contains
  subroutine private_do
    call intrf_f__template(private_sp, private_dp)
  end subroutine private_do
  subroutine private_sp
    real(kind=sp_) :: dtyp
    call intrf_f__ans_id(number__slidemmm(transA, transB, hashB, nnn(ida, dtyp), nnn(idb, dtyp), bm, bn, hm, l, b), idc) 
  end subroutine private_sp
  subroutine private_dp
    real(kind=dp_) :: dtyp
    call intrf_f__ans_id(number__slidemmm(transA, transB, hashB, nnn(ida, dtyp), nnn(idb, dtyp), bm, bn, hm, l, b), idc) 
  end subroutine private_dp
end subroutine intrf_f__number__slidemmm

!> @}
