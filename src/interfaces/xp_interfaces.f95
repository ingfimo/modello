
! subroutine xp_intrf_f__number__is_allocated__0 (dtyp, id, ans)
!   implicit none
!   real(kind=xp_), intent(in) :: dtyp
!   integer, intent(in) :: id
!   integer, intent(out) :: ans
!   call do_within("xp_intrf_f__number__is_allocated", mod_interfaces_name_, private_is_allocated)
! contains
!   subroutine private_is_allocated
!     if (allocated(xp_NUMBERS_)) then
!        call assert(id > 0 .and. id <= size(xp_NUMBERS_), err_oorng_, 'id')
!        if (err_free()) ans = merge(1, 0, is_allocated(xp_NUMBERS_(id)))
!     else
!        ans = 0
!     end if
!   end subroutine private_is_allocated
! end subroutine xp_intrf_f__number__is_allocated__0

! subroutine xp_intrf_f__number__append__0 (dtyp, id, r, shp, dx)
!   implicit none
!   real(kind=xp_), intent(in) :: dtyp
!   integer, intent(out) :: id
!   integer, intent(in) :: r, shp(r), dx
!   call do_within("xp_intrf_f__number__append", mod_interfaces_name_, private_append)
! contains
!   subroutine private_append
!     type(xp_number), pointer :: x
!     if (r == 0) then
!        call number__append(x, [integer::], dx > 0)
!     else
!        call number__append(x, shp, dx > 0)
!     end if
!     if (err_free()) id = x%id
!   end subroutine private_append
! end subroutine xp_intrf_f__number__append__0

! subroutine xp_intrf_f__number__shape__0 (dtyp, id, shp)
!   implicit none
!   real(kind=xp_), intent(in) :: dtyp
!   integer, intent(in) ::id
!   integer, intent(out) :: shp(:)
!   call do_within("xp_intrf_f__number__shape", mod_interfaces_name_, private_shape)
! contains
!   subroutine private_shape
!     type(xp_number), pointer :: x
!     call get_number(id, x)
!     shp = x%shp
!   end subroutine private_shape
! end subroutine xp_intrf_f__number__shape__0

! subroutine xp_intrf_f__number__set_v__0 (id, x)
!   implicit none
!   integer, intent(in) :: id
!   real(kind=xp_), intent(in) :: x(:)
!   call do_within("xp_intrf_f__number__set_v", mod_interfaces_name_, private_set)
! contains
!   subroutine private_set
!     type(xp_number), pointer :: xx
!     call get_number(id, xx)
!     if (size(x) > 1) then
!        call number__set_v(xx, x)
!     else
!        call number__set_v(xx, x(1))
!     end if
!   end subroutine private_set
! end subroutine xp_intrf_f__number__set_v__0

! subroutine xp_intrf_f__number__set_v_sequence__0 (id, x)
!   implicit none
!   real(kind=xp_), intent(in) :: x(:,:)
!   integer, intent(in) :: id(:)
!   call do_within("xp_intrf_f__number__set_v__0", mod_interfaces_name_, private_set)
! contains
!   subroutine private_set
!     type(xp_number) :: xx(size(id))
!     real(kind=xp_) :: dtyp
!     integer :: i
!     xx = [(nnn(id(i), dtyp), i=1, size(id))]
!     call number__set_v(xx, x)
!   end subroutine private_set
! end subroutine xp_intrf_f__number__set_v_sequence__0

! subroutine xp_intrf_f__number__set_dv__0 (id, x)
!   implicit none
!   integer, intent(in) :: id
!   real(kind=xp_), intent(in) :: x(:)
!   call do_within("xp_intrf_f__number__set_dv", mod_interfaces_name_, private_set)
! contains
!   subroutine private_set
!     type(xp_number), pointer :: xx
!     call get_number(id, xx)
!     if (size(x) > 1) then
!        call number__set_dv(xx, x)
!     else
!        call number__set_dv(xx, x(1))
!     end if
!   end subroutine private_set
! end subroutine xp_intrf_f__number__set_dv__0

! subroutine xp_intrf_f__number__set_slice_v__0 (id, s, v)
!   implicit none
!   integer, intent(in) :: id, s(:,:)
!   real(kind=xp_), intent(in) :: v(:)
!   call do_within("xp_intrf_f__number__set_slice_v__0", mod_interfaces_name_, private_set)
! contains
!   subroutine private_set
!     type(xp_number), pointer :: x
!     call get_number(id, x)
!     if (size(v) > 1) then
!        call number__set_slice_v(x, s, v)
!     else
!        call number__set_slice_v(x, s, v(1))
!     end if
!   end subroutine private_set
! end subroutine xp_intrf_f__number__set_slice_v__0

! subroutine xp_intrf_f__number__set_slice_dv__0 (id, s, dv)
!   implicit none
!   integer, intent(in) :: id, s(:,:)
!   real(kind=xp_), intent(in) :: dv(:)
!   call do_within("xp_intrf_f__number__set_slice_dv__0", mod_interfaces_name_, private_set)
! contains
!   subroutine private_set
!     type(xp_number), pointer :: x
!     call get_number(id, x)
!     if (size(dv) > 1) then
!        call number__set_slice_dv(x, s, dv)
!     else
!        call number__set_slice_dv(x, s, dv(1))
!     end if
!   end subroutine private_set
! end subroutine xp_intrf_f__number__set_slice_dv__0

! subroutine xp_intrf_f__number__set_flat_slice_v__0 (id, s, v)
!   implicit none
!   integer, intent(in) :: id, s(:)
!   real(kind=xp_), intent(in) :: v(:)
!   call do_within("xp_intrf_f__number__set_flat_slice_v__0", mod_interfaces_name_, private_set)
! contains
!   subroutine private_set
!     type(xp_number), pointer :: x
!     call get_number(id, x)
!     if (size(v) > 1) then
!        call number__set_flat_slice_v(x, s, v)
!     else
!        call number__set_flat_slice_v(x, s, v(1))
!     end if
!   end subroutine private_set
! end subroutine xp_intrf_f__number__set_flat_slice_v__0

! subroutine xp_intrf_f__number__set_flat_slice_dv__0 (id, s, dv)
!   implicit none
!   integer, intent(in) :: id, s(:)
!   real(kind=xp_), intent(in) :: dv(:)
!   call do_within("xp_intrf_f__number__set_flat_slice_dv__0", mod_interfaces_name_, private_set)
! contains
!   subroutine private_set
!     type(xp_number), pointer :: x
!     call get_number(id, x)
!     if (size(dv) > 1) then
!        call number__set_flat_slice_dv(x, s, dv)
!     else
!        call number__set_flat_slice_dv(x, s, dv(1))
!     end if
!   end subroutine private_set
! end subroutine xp_intrf_f__number__set_flat_slice_dv__0

! subroutine xp_intrf_f__number__inlock_free__0 (dtyp, id, ans)
!   implicit none
!   real(kind=xp_), intent(in) :: dtyp
!   integer, intent(in) :: id
!   integer, intent(out) :: ans
!   call do_within("xp_intrf_f__number__inlock_free__0", mod_interfaces_name_, private_inlock_free)
! contains
!   subroutine private_inlock_free
!     call assert(allocated(xp_NUMBERS_), err_notAlloc_, "xp_NUMBERS_")
!     if (err_free()) then
!        ans = merge(1, 0, number__inlock_free(nnn(id, dtyp)))
!     else
!        ans = 0
!     end if
!   end subroutine private_inlock_free
! end subroutine xp_intrf_f__number__inlock_free__0

! subroutine xp_intrf_f__number__pop__0 (dtyp, id)
!   implicit none
!   real(kind=xp_), intent(in) :: dtyp
!   integer, intent(in) :: id
!   call do_within("xp_intrf_f__number__pop__0", mod_interfaces_name_, private_pop)
! contains
!   subroutine private_pop
!     type(xp_number), pointer :: x
!     call get_number(id, x)
!     call number__pop(x)
!   end subroutine private_pop
! end subroutine xp_intrf_f__number__pop__0

! subroutine xp_intrf_f__graph__is_allocated__0 (g, id, ans)
!   implicit none
!   type(xp_graph), intent(in), allocatable :: g(:)
!   integer, intent(in) :: id
!   integer, intent(out) :: ans
!   call do_within("xp_intrf_f__graph__is_allocated", mod_interfaces_name_, private_is_allocated)
! contains
!   subroutine private_is_allocated
!     if (allocated(g)) then
!        call assert(id > 0 .and. id <= size(g), err_oorng_, 'id')
!        if (err_free()) ans = merge(1, 0, is_allocated(g(id)))
!     else
!        ans = 0
!     end if
!   end subroutine private_is_allocated
! end subroutine xp_intrf_f__graph__is_allocated__0

! subroutine xp_intrf_f__number__op__0 (dtyp, id)
!   implicit none
!   real(kind=xp_), intent(in) :: dtyp
!   integer, intent(in) :: id
!   call do_within("xp_intrf_f__number__op__0", mod_interfaces_name_, private_op)
! contains
!   subroutine private_op
!     type(xp_number), pointer :: x
!     call get_number(id, x)
!     call number__op(x)
!   end subroutine private_op
! end subroutine xp_intrf_f__number__op__0

! subroutine xp_intrf_f__number__fw__0 (dtyp, id)
!   implicit none
!   real(kind=xp_), intent(in) :: dtyp
!   integer, intent(in) :: id
!   call do_within("xp_intrf_f__number__fw__0", mod_interfaces_name_, private_fw)
! contains
!   subroutine private_fw
!     type(xp_number), pointer :: x
!     call get_number(id, x)
!     call number__fw(x)
!   end subroutine private_fw
! end subroutine xp_intrf_f__number__fw__0

! subroutine xp_intrf_f__number__bw_zero__0 (dtyp, id)
!   implicit none
!   real(kind=xp_), intent(in) :: dtyp
!   integer, intent(in) :: id
!   call do_within("xp_intrf_f__number__bw_zero__0", mod_interfaces_name_, private_bw_zero)
! contains
!   subroutine private_bw_zero
!     type(xp_number), pointer :: x
!     call get_number(id, x)
!     call number__bw_zero(x)
!   end subroutine private_bw_zero
! end subroutine xp_intrf_f__number__bw_zero__0

! subroutine xp_intrf_f__number__bw__0 (dtyp, id)
!   implicit none
!   real(kind=xp_), intent(in) :: dtyp
!   integer, intent(in) :: id
!   call do_within("xp_intrf_f__number__bw__0", mod_interfaces_name_, private_bw)
! contains
!   subroutine private_bw
!     type(xp_number), pointer :: x
!     call get_number(id, x)
!     call number__bw(x)
!   end subroutine private_bw
! end subroutine xp_intrf_f__number__bw__0








! subroutine xp_intrf_f__number__softmax (dtyp, id, k, idout)
!   implicit none
!   real(kind=xp_), intent(in) :: dtyp
!   integer, intent(in) :: id, k
!   integer, intent(out) :: idout
!   call do_within("xp_intrf_f__number__softmax", mod_interfaces_name_, private_softmax)
! contains
!   subroutine private_softmax
!     if (k > 0) then
!        call intrf_f__ans_id(number__softmax(nnn(id, dtyp), k), idout)
!     else
!        call intrf_f__ans_id(number__softmax(nnn(id, dtyp)), idout)
!     end if
!   end subroutine private_softmax
! end subroutine xp_intrf_f__number__softmax

subroutine xp_intrf_f__opt__sgd_step__0 (xoi, gi, xout, lr, niter, ftrain)
  implicit none
  integer, intent(in) :: xoi, gi, xout, niter
  real(kind=xp_), intent(in) :: lr
  real(kind=xp_), intent(out) :: ftrain
  call do_within("xp_intrf_f__opt__sgd_step__0", mod_interfaces_name_, private_step)
contains
  subroutine private_step
    type(xp_opt), pointer :: xxo
    type(xp_number), pointer :: xxout
    call get_number(xout, xxout)
    call assert(get_rank(xxout) == 0, err_generic_, 'NUMBERS_(xout) rank /= 0')
    call assert(has_dx(xxout), err_generic_, 'xout has no dx')
    if (err_free()) then
       call get_opt(xoi, xxo)
       call opt__sgd_step(xxo,  ggg(gi, lr), xxout, lr, niter, ftrain)
    end if
  end subroutine private_step
end subroutine xp_intrf_f__opt__sgd_step__0

subroutine xp_intrf_f__opt__sgdwm_step__0 (xoi, gi, xout, lr, alpha, niter, ftrain)
  implicit none
  integer, intent(in) :: xoi, gi, xout, niter
  real(kind=xp_), intent(in) :: lr, alpha
  real(kind=xp_), intent(out) :: ftrain
  call do_within("xp_intrf_f__opt__sgdwm_step__0", mod_interfaces_name_, private_step)
contains
  subroutine private_step
    type(xp_opt), pointer :: xxo
    type(xp_number), pointer :: xxout
    call get_number(xout, xxout)
    call assert(get_rank(xxout) == 0, err_generic_, 'NUMBERS_(xout) rank /= 0')
    call assert(has_dx(xxout), err_generic_, 'xout has no dx')
    if (err_free()) then
       call get_opt(xoi, xxo)
       call opt__sgdwm_step(xxo, ggg(gi, lr), xxout, lr, alpha, niter, ftrain)
    end if
  end subroutine private_step
end subroutine xp_intrf_f__opt__sgdwm_step__0

subroutine xp_intrf_f__opt__adam_step__0 (xoi, gi, xout, lr, beta1, beta2, niter, ftrain)
  implicit none
  integer, intent(in) :: xoi, gi, xout, niter
  real(kind=xp_), intent(in) :: lr, beta1, beta2
  real(kind=xp_), intent(out) :: ftrain
  call do_within("xp_intrf_f__opt__adam_step__0", mod_interfaces_name_, private_step)
contains
  subroutine private_step
    type(xp_opt), pointer :: xxo
    type(xp_number), pointer :: xxout
    call get_number(xout, xxout)
    call assert(get_rank(xxout) == 0, err_generic_, 'NUMBERS_(xout) rank /= 0')
    call assert(has_dx(xxout), err_generic_, 'xout has no dx')
    if (err_free()) then
       call get_opt(xoi, xxo)
       call opt__adam_step(xxo, ggg(gi, lr), xxout, lr, beta1, beta2, niter, ftrain)
    end if
  end subroutine private_step
end subroutine xp_intrf_f__opt__adam_step__0
