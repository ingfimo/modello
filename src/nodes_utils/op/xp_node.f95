!> @brief Runs all the operators within the given @graph
!! @param[in] g @xp_graph 
subroutine xp_graph__op (g)
  implicit none
  type(xp_graph), intent(in) :: g
  integer :: i, ii
  type(xp_node), pointer :: nd
  do i = 1, size(g%nodes)
     ii = g%nodes(i)
     nd => xp_NODES_(ii)
     call nd%op(nd%attrs, nd%states)
  end do
end subroutine xp_graph__op

subroutine xp_graph__post (g)
  implicit none
  type(xp_graph), intent(in) :: g
  integer :: i, ii
  type(xp_node), pointer :: nd
  do i = 1, size(g%posts)
     ii = g%posts(i)
     nd => xp_NODES_(ii)
     call nd%op(nd%attrs, nd%states)
  end do
end subroutine xp_graph__post

