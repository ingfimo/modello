subroutine nodestates__allocate (x, s1, s2)
  implicit none
  type(nodestates), intent(inout) :: x
  integer, intent(in) :: s1(:), s2(:)
  call do_safe_within("nodestate__allocate", mod_nodes_utils_name_, private_do)
contains
  subroutine private_do
    call alloc(x%s1, s1, "node state s1")
    call alloc(x%s2, s2, "node state s2")
  end subroutine private_do
end subroutine nodestates__allocate
