!> Allocate a 'node'
!! @param[inout] x 'node'
!! @param[in] op integer, operator id
!! @param[in] flg integer(:), flags or indexes necessary for the operator
!! @param[in] in number(:), array of input 'numbers'
!! @param[in] out number(:), array of output 'number'
subroutine xp_nodeattrs__allocate (x, op, in, out, ipar, rpar)
  implicit none
  type(xp_nodeattrs), intent(inout) :: x
  integer, intent(in) :: op
  type(xp_number), intent(in) :: in(:), out(:)
  integer, intent(in) :: ipar(:)
  real(kind=xp_), intent(in) :: rpar(:)
  call do_safe_within('xp_nodeattrs__allocate', mod_nodes_utils_name_, private_allocate)
contains
  subroutine private_allocate
    implicit none
    integer :: i
    call alloc(x%in, [(in(i)%id, i = 1, size(in))], 'x%in')
    call alloc(x%ipar, ipar, "x%ipar")
    call alloc(x%rpar, rpar, "x%rpar")
    if (size(out) > 0) then
       call alloc(x%out, [(out(i)%id, i = 1, size(out))], 'x%out')
       call err_safe(private_set_number_nd)
    else
       call alloc(x%out, [integer::], 'x%out')
    end if
    if (err_free()) then
       x%opid = op
       x%id = get_nodei()
       call set_graphi(1._xp_, op)
    end if
  end subroutine private_allocate
  subroutine private_set_number_nd
    implicit none
    integer :: i
    do i = 1, size(out)
       call number__set_nd(out(i), get_nodei())
    end do
  end subroutine private_set_number_nd
end subroutine xp_nodeattrs__allocate

subroutine xp_node__allocate__1 (x, op, in, out, ipar, rpar, s1, s2)
  implicit none
  type(xp_node), intent(inout) :: x
  type(xp_number), intent(in) :: in(:), out(:)
  integer, intent(in) :: op, ipar(:), s1(:), s2(:)
  real(kind=xp_), intent(in) :: rpar(:)
  call do_safe_within("node__allocate__1", mod_nodes_utils_name_, private_allocate)
contains
  subroutine private_allocate
    call assert(.not. is_allocated(x), err_alreadyAlloc_, "x")
    call err_safe(private_allocate_attrs)
    call err_safe(private_allocate_states)
    call node__op(x)
    call node__fw(x)
    call node__bw(x)
    call node__update_inlock(x, '+')
  end subroutine private_allocate
  subroutine private_allocate_attrs
    integer :: info
    info = 0
    allocate(x%attrs, stat=info)
    call assert(info == 0, err_alloc_, "node attrs")
    call nodeattrs__allocate(x%attrs, op, in, out, ipar, rpar)
  end subroutine private_allocate_attrs
  subroutine private_allocate_states
    integer :: info
    info = 0
    allocate(x%states, stat=info)
    call assert(info == 0, err_alloc_, "node states")
    call nodestates__allocate(x%states, s1, s2)
  end subroutine private_allocate_states
end subroutine xp_node__allocate__1

!> Allocates and array of 'nodes' of dimension (:), given the required size
!! @param[inout] x node(:), allocatable
!! @param[in] n integer, array size
!! @param[in] xname character, name of the variable (for error reporting)
subroutine xp_node__allocate__2 (x, n)
  implicit none
  type(xp_node), intent(inout), allocatable :: x(:)
  integer :: n
  call do_safe_within("node__allocate__2", mod_nodes_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: info
    call assert(.not. allocated(x), err_alreadyAlloc_, "x")
    if (err_free()) then
       allocate(x(n), stat=info)
       call assert(info == 0, err_alloc_, "x")
    end if
  end subroutine private_allocate
end subroutine xp_node__allocate__2

!> Allocates and array of 'graphs' of dimension (:), given the required size
!! @param[inout] x graph(:), allocatable
!! @param[in] n integer, array size
!! @param[in] xname character, name of the variable (for error reporting)
subroutine xp_graph__allocate (x, n)
  implicit none
  type(xp_graph), intent(inout), allocatable :: x(:)
  integer :: n
  call do_safe_within("xp_graph__allocate", mod_nodes_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: info
    call assert(.not. allocated(x), err_alreadyAlloc_, "x")
    if (err_free()) then
       allocate(x(n), stat=info)
       call assert(info == 0, err_alloc_, "x")
    end if
  end subroutine private_allocate
end subroutine xp_graph__allocate
