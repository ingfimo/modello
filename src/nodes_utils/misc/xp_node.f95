!> Converts a 'node' to a vector of integers
!! @author Filippo Monari
!! @param[in] x 'node' to be converted
function xp_node__to_vec (x) result(v)
  implicit none
  type(xp_node), intent(in) :: x
  integer, allocatable :: v(:)
  integer :: insz, flgsz
  call do_safe_within('xp_node__to_vec', mod_nodes_utils_name_, private_to_vec)
contains
  subroutine private_to_vec
    insz = size(x%attrs%in)
    flgsz = size(x%attrs%ipar)
    call alloc(v, [insz, flgsz, x%attrs%id, x%attrs%opid, x%attrs%out, x%attrs%in, x%attrs%ipar], 'v')
  end subroutine private_to_vec
end function xp_node__to_vec

subroutine xp_node__get_ipar (x, p)
  implicit none
  type(xp_node), intent(in) :: x
  integer, intent(out), allocatable :: p(:)
  call do_safe_within("xp_node__get_ipar", mod_nodes_utils_name_, private_get)
contains
  subroutine private_get
    call alloc(p, x%attrs%ipar, "p")
  end subroutine private_get
end subroutine xp_node__get_ipar

subroutine xp_node__get_rpar (x, p)
  implicit none
  type(xp_node), intent(in) :: x
  real(kind=xp_), intent(out), allocatable :: p(:)
  call do_safe_within("xp_node__get_rpar", mod_nodes_utils_name_, private_get)
contains
  subroutine private_get
    call alloc(p, x%attrs%rpar, "p")
  end subroutine private_get
end subroutine xp_node__get_rpar
  
subroutine xp_node__get_state (x, i, s)
  implicit none
  type(xp_node), intent(in) :: x
  integer, intent(in) :: i
  integer, intent(out), allocatable :: s(:)
  call do_safe_within("xp_node__get_state", mod_nodes_utils_name_, private_get)
contains
  subroutine private_get
    select case (i)
    case (1)
       call alloc(s, x%states%s1, "s1")
    case (2)
       call alloc(s, x%states%s2, "s2")
    case default
       call raise_error("i", err_unknwnVal_)
    end select
  end subroutine private_get
end subroutine xp_node__get_state

subroutine xp_node__set_state (x, i, s)
  implicit none
  type(xp_node), intent(in) :: x
  integer, intent(in) :: i
  integer, intent(in) :: s(:)
  call do_safe_within("xp_node__get_state", mod_nodes_utils_name_, private_set)
contains
  subroutine private_set
    select case (i)
    case (1)
       call dealloc(x%states%s1, "s1")
       call alloc(x%states%s1, s, "s1")
    case (2)
       call dealloc(x%states%s2, "s2")
       call alloc(x%states%s2, s, "s2")
    case default
       call raise_error("i", err_unknwnVal_)
    end select
  end subroutine private_set
end subroutine xp_node__set_state


!> Updates the inlock count for all the inputs to a 'node'.
!! @author Filippo Monari
!! @param[in] x 'node'
!! @param[in] m character, update mode (i.e. '+' or '-')
subroutine xp_node__update_inlock (x, m)
  type(xp_node), intent(in) :: x
  character(len=*), intent(in) :: m
  integer :: i, ii
  call do_safe_within('xp_node__update_inlock', mod_nodes_utils_name_, private_update)
contains
  subroutine private_update
    call assert(is_allocated(x), err_notAlloc_, 'x')
    if (err_free()) then
       do i = 1, size(x%attrs%in)
          ii = x%attrs%in(i)
          call number__inlock(xp_NUMBERS_(ii), m)
       end do
    end if
  end subroutine private_update
end subroutine xp_node__update_inlock

!> Pack a 'graph' by removing the index of the node from its node vector.
!! @author Filippo Monari
!! @param[in] x 'node' to be removed
subroutine xp_node__pack_graph (x)
  type(xp_node), intent(inout) :: x
  integer, allocatable :: tmp(:)
  integer :: i
  call do_safe_within('xp_node__pack_graph', mod_nodes_utils_name_, private_pack)
contains
  subroutine private_pack
    call assert(is_allocated(x), err_notInit_, 'x')
    i = -1
    if (err_free()) i = NODEGRPHS_(x%attrs%id)
    if (i > 0) then
       call alloc(tmp, pack(xp_GRAPHS_(i)%nodes, xp_GRAPHS_(i)%nodes /= x%attrs%id), 'tmp')
       call dealloc(xp_GRAPHS_(i)%nodes, 'GRAPH_(i)%nodes')
       call alloc(xp_GRAPHS_(i)%nodes, tmp, 'GRAPHS_(i)%nodes')
    end if
  end subroutine private_pack
end subroutine xp_node__pack_graph

!> Reset (set to -1) the 'NUMNDS_' position corresponding to the 'number'
!! output of the 'node'.
!! This means that the 'number' was output of a node that does not exists
!! anymore.
!! @author Filippo Monari
!! @param[in] x 'node'
subroutine xp_node__reset_number (x)
  type(xp_node), intent(in) :: x
  call do_safe_within('xp_node__rest_number', mod_nodes_utils_name_, private_reset)
contains
  subroutine private_reset
    NUMNDS_(x%attrs%out) = -1
  end subroutine private_reset
end subroutine xp_node__reset_number

!> Resets all the registers relaitve to the opening and closing of 'graph'
!! Resets GRAPHi_, GRAPH_ and WITHDX_ to thier default values
subroutine xp_graph__reset_graphi (dtyp)
  implicit none
  real(kind=xp_), intent(in) :: dtyp
  GRAPHi_ = 0
  if (associated(xp_GRAPH_)) nullify(xp_GRAPH_)
  WITHDX_ = .true.
  INPOSTS_ = .false.
end subroutine xp_graph__reset_graphi

!> Checks if there is a 'graph' on the stack
!! Returns .true. if ther is a graph already on the stack
!! .false. otherwise.
function xp_graph__graphi_is_set (dtyp) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: dtyp
  logical :: ans
  ans = GRAPHi_ > 0 .and. associated(xp_GRAPH_)
end function xp_graph__graphi_is_set

!> Returns the index of current graph (the one on the stack).
!! @author Filippo Monari
function xp_graph__get_graphi (dtyp) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: dtyp
  integer :: ans
  call do_safe_within('get_graphi', mod_nodes_utils_name_, private_graphi)
contains
  subroutine private_graphi
    ans = 0
    call assert(graphi_is_set(dtyp), err_generic_, 'GRAPHi_ is not set.')
    if (err_free()) ans = GRAPHi_
  end subroutine private_graphi
end function xp_graph__get_graphi

!> Sets the current graph index for the node currently on the stack.
!! @author Filippo Monari
subroutine xp_graph__set_graphi (dtyp, op)
  implicit none
  real(kind=xp_), intent(in) :: dtyp
  integer, intent(in) :: op
  call do_safe_within('set_graphi', mod_nodes_utils_name_, private_set_nodegrphs)
contains
  subroutine private_set_nodegrphs
    select case (op)
    case (op_feed_start_:op_feed_end_)
       NODEGRPHS_(get_nodei()) = -1
    case default
       NODEGRPHS_(get_nodei()) = get_graphi(dtyp)
    end select
  end subroutine private_set_nodegrphs
end subroutine xp_graph__set_graphi



