! subroutine graph__fw_zero0 (nds)
!   implicit none
!   integer, intent(in) :: nds(:)
! end subroutine graph__fw_zero0

!> @brief Apply forward differentiation operators
!!within the given @graph
!! @param[in] g @xp_graph
subroutine xp_graph__fw (g)
  implicit none
  type(xp_graph), intent(in) :: g
  integer :: i, ii
  type(xp_node), pointer :: nd
  do i = 1, size(g%nodes)
     ii = g%nodes(i)
     nd => xp_NODES_(ii)
     call nd%fw(nd%attrs, nd%states)
  end do
end subroutine xp_graph__fw
