!> @brief Deallocates a @nodeattrs
!! @param[inout] x @xp_nodeattrs
!! @todo check for deallocating states
subroutine xp_nodeattrs__deallocate (x)
  implicit none
  type(xp_nodeattrs), intent(inout) :: x
  call do_safe_within('xp_nodeattrs__deallocate', mod_nodes_utils_name_, private_do)
contains
  subroutine private_do
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call dealloc(x%in, 'x%in')
    call dealloc(x%out, 'x%out')
    call dealloc(x%ipar, 'x%ipar')
    call dealloc(x%rpar, 'x%rpar')
    call err_safe(private_deinit)
  end subroutine private_do
  subroutine private_deinit
    x%id = 0
    x%opid = 0
  end subroutine private_deinit
end subroutine xp_nodeattrs__deallocate

!> @brief Deallocate a @node
!! @param[inout] x @xp_node
subroutine xp_node__deallocate__1 (x)
  implicit none
  type(xp_node), intent(inout) :: x
  call do_safe_within("xp_node__deallocate__1", mod_nodes_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    call assert(is_allocated(x), err_notAlloc_, "x")
    call err_safe(private_deallocate_attrs)
    call err_safe(private_deallocate_states)
    if (err_free()) then
       nullify(x%op)
       nullify(x%fw)
       nullify(x%bw)
    end if
  end subroutine private_deallocate
  subroutine private_deallocate_attrs
    integer :: info
    call nodeattrs__deallocate(x%attrs)
    if (err_free()) then
       deallocate(x%attrs, stat=info)
       call assert(info == 0, err_dealloc_, "node attrs")
    end if
  end subroutine private_deallocate_attrs
  subroutine private_deallocate_states
    integer :: info
    call nodestates__deallocate(x%states)
    if (err_free()) then
       deallocate(x%states, stat=info)
       call assert(info == 0, err_dealloc_, "node states")
    end if
  end subroutine private_deallocate_states
end subroutine xp_node__deallocate__1

!> @brief Deallocates a @node vector
!! @param[inout] x @xp_node @vector @allocatable
subroutine xp_node__deallocate__2 (x)
  implicit none
  type(xp_node), intent(inout), allocatable :: x(:)
  call do_safe_within("node__deallocate__2", mod_nodes_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    integer :: info
    call assert(allocated(x), err_notAlloc_, "x")
    if (err_free()) then
       deallocate(x, stat=info)
       call assert(info == 0, err_dealloc_, "x")
    end if
  end subroutine private_deallocate
end subroutine xp_node__deallocate__2

!> @brief Deallocates a @graph vector
!! @param[inout] x @graph @vector @allocatable
subroutine xp_graph__deallocate (x)
  implicit none
  type(xp_graph), intent(inout), allocatable :: x(:)
  call do_safe_within("graph__deallocate", mod_nodes_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    integer :: info
    call assert(allocated(x), err_notAlloc_, "x")
    if (err_free()) then
       deallocate(x, stat=info)
       call assert(info == 0, err_dealloc_, "x")
    end if
  end subroutine private_deallocate
end subroutine xp_graph__deallocate
