subroutine nodestates__deallocate (x)
  implicit none
  type(nodestates), intent(inout) :: x
  call do_safe_within("nodestates__deallocate", mod_nodes_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    call dealloc(x%s1, "node state s1")
    call dealloc(x%s2, "node state s2")
  end subroutine private_deallocate
end subroutine nodestates__deallocate
