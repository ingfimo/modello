!> @brief Reset all the bakward gradients within a @graph
!! @param[in] g @xp_graph 
subroutine xp_graph__bw_zero (g)
  implicit none
  type(xp_graph), intent(in) :: g
  integer :: i, ii
  do i = 1, size(g%nodes)
     ii = g%nodes(i)
     call node__bw_zero(xp_NODES_(ii))
  end do
end subroutine xp_graph__bw_zero

!> @brief Runs backward differentiation for all opertors within a @graph
!! @param[in] g @xp_graph
subroutine xp_graph__bw (g)
  implicit none
  type(xp_graph), intent(in) :: g
  integer :: i, ii
  type(xp_node), pointer :: nd
  do i = size(g%nodes), 1, -1
     ii = g%nodes(i)
     nd => xp_NODES_(ii)
     call nd%bw(nd%attrs, nd%states)
  end do
end subroutine xp_graph__bw
