module optim_utils

  use env
  use types
  use errwarn
  use registers
  use math
  use nodes_utils
  use utils
  use numbers_utils

  character(len=*), parameter :: mod_optim_utils_name_ = 'optim_utils'

  interface opt__get_state
     module procedure sp_opt__get_state
     module procedure dp_opt__get_state
  end interface opt__get_state

  interface opt__set_state
     module procedure sp_opt__set_state
     module procedure dp_opt__set_state
  end interface opt__set_state

  interface opt__set_clipping
     module procedure sp_opt__set_clipping
     module procedure dp_opt__set_clipping
  end interface opt__set_clipping

  interface opt__get_clipping
     module procedure sp_opt__get_clipping
     module procedure dp_opt__get_clipping
  end interface opt__get_clipping

  interface opt__collect_xab
     module procedure sp_opt__collect_xab
     module procedure dp_opt__collect_xab
  end interface opt__collect_xab
  
  interface collect_xin
     module procedure sp__collect_xin
     module procedure dp__collect_xin
  end interface collect_xin

  interface collect_dxin
     module procedure sp__collect_dxin
     module procedure dp__collect_dxin
  end interface collect_dxin

  ! interface init_bw
  !    module procedure sp__init_bw
  !    module procedure dp__init_bw
  ! end interface init_bw

  interface assert_xin_has_dx
     module procedure sp_opt__assert_xin_has_dx
     module procedure dp_opt__assert_xin_has_dx
  end interface assert_xin_has_dx

  interface simple_update
     module procedure sp__simple_update
     module procedure dp__simple_update
  end interface simple_update

  interface get_obj
     module procedure sp__get_obj
     module procedure dp__get_obj
  end interface get_obj

  interface opt__deallocate
     module procedure sp_opt__deallocate
     module procedure dp_opt__deallocate
  end interface opt__deallocate

  interface opt__pop
     module procedure sp_opt__pop
     module procedure dp_opt__pop
  end interface opt__pop
  
contains

  include "./optim_utils/sp.f95"
  include "./optim_utils/dp.f95"
    
end module optim_utils
