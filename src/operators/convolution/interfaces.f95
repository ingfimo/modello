  interface op_conv
     module procedure sp_number__op_conv
     module procedure dp_number__op_conv
  end interface op_conv

  interface fw_conv
     module procedure sp_number__fw_conv
     module procedure dp_number__fw_conv
  end interface fw_conv

  interface bw_conv
     module procedure sp_number__bw_conv
     module procedure dp_number__bw_conv
  end interface bw_conv

  interface op_maxpool
     module procedure sp_number__op_maxpool
     module procedure dp_number__op_maxpool
  end interface op_maxpool

  interface fw_maxpool
     module procedure sp_number__fw_maxpool
     module procedure dp_number__fw_maxpool
  end interface fw_maxpool

  interface bw_maxpool
     module procedure sp_number__bw_maxpool
     module procedure dp_number__bw_maxpool
  end interface bw_maxpool
