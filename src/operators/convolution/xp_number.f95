!x1 -> (s, chi, b)
!k -> (s, chi, cho)
!x2 -> (l, cho, b) 
subroutine xp_number__op_conv (x1, k, x2, slices)
  implicit none
  type(xp_number), intent(in), target :: x1, k
  type(xp_number), intent(inout), target :: x2
  integer, intent(in), target :: slices(:)
  integer, parameter :: ci = 1, co = 2, b = 3, li = 4, lk = 5, lo = 6, s = 7 
  real(kind=xp_), pointer :: xx1(:,:), xk(:,:), xx2(:,:,:)
  integer, pointer :: xslices(:,:)
  integer :: xslice(slices(lk) * slices(ci)), i
  xx1(1:(slices(li) * slices(ci)), 1:slices(b)) => x1%v
  xk(1:(slices(lk) * slices(ci)), 1:slices(co)) => k%v
  xslices(1:slices(lk), 1:(size(slices(s:)) / slices(lk))) => slices(s:)
  xx2(1:slices(lo), 1:slices(co), 1:slices(b)) => x2%v
  !$omp parallel do private(xslice)
  do i = 1, size(xslices, 2)
     xslice = offset_slice(xslices(:,i), slices(li), slices(ci))
     if (all(xslice <= 0)) then
        xx2(i,:,:) = 0
     else
        call gmmmult(1, 0, 1._xp_,  xk(which(xslice > 0),:), &
             xx1(pack(xslice, xslice > 0),:), 0._xp_, xx2(i,:,:))
     end if
  end do
  !$omp end parallel do
end subroutine xp_number__op_conv

subroutine xp_number__fw_conv (x1, k, x2, slices)
  implicit none
  type(xp_number), intent(in), target :: x1, k
  type(xp_number), intent(inout), target :: x2
  integer, intent(in), target :: slices(:)
  integer, parameter :: ci = 1, co = 2, b = 3, li = 4, lk = 5, lo = 6, s = 7 
  real(kind=xp_), pointer :: xx1(:,:), xk(:,:), dx1(:,:), dxk(:,:), dx2(:,:,:)
  integer, pointer :: xslices(:,:)
  integer :: xslice(slices(lk) * slices(ci)), i, j
  if (has_dx(x2)) then
     xx1(1:(slices(li) * slices(ci)), 1:slices(b)) => x1%v
     if (has_dx(x1)) dx1(1:(slices(li) * slices(ci)), 1:slices(b)) => x1%dv
     xk(1:(slices(lk) * slices(ci)), 1:slices(co)) => k%v
     if (has_dx(k)) dxk(1:(slices(lk) * slices(ci)), 1:slices(co)) => k%dv
     xslices(1:slices(lk), 1:(size(slices(s:)) / slices(lk))) => slices(s:)
     dx2(1:slices(lo), 1:slices(co), 1:slices(b)) => x2%dv
     do i = 1, size(xslices, 2)
        xslice = offset_slice(xslices(:,i), slices(li), slices(ci))
        if (all(xslice <= 0)) then
           dx2(i,:,:) = 0
        else
           do j = 1, slices(co)
              if (has_dx(x1)) call gmvmult(1, 1._xp_, dx1(pack(xslice, xslice > 0),:), &
                   pack(xk(:,j), xslice > 0), 1._xp_, dx2(i,j,:))
              if (has_dx(k)) call gmvmult(1, 1._xp_, xx1(pack(xslice, xslice > 0),:), &
                   pack(dxk(:,j), xslice > 0), 1._xp_, dx2(i,j,:))
           end do
        end if
     end do
  end if
end subroutine xp_number__fw_conv

subroutine xp_number__bw_conv (x1, k, x2, slices)
  implicit none
  type(xp_number), intent(inout), target :: x1, k
  type(xp_number), intent(in) :: x2
  integer, intent(in), target :: slices(:)
  integer, parameter :: ci = 1, co = 2, b = 3, li = 4, lk = 5, lo = 6, s = 7 
  real(kind=xp_), pointer :: xx1(:,:), xk(:,:)
  real(kind=xp_), pointer :: dx1(:,:), dxk(:,:), dx2(:,:,:)
  integer, pointer :: xslices(:,:)
  integer, allocatable :: s1(:)
  real(kind=xp_), allocatable :: ss1(:,:)
  integer :: xslice(slices(lk) * slices(ci)), i, j
  if (has_dx(x2)) then
     xx1(1:(slices(li) * slices(ci)), 1:slices(b)) => x1%v
     if (has_dx(x1)) dx1(1:(slices(li) * slices(ci)), 1:slices(b)) => x1%dv
     xk(1:(slices(lk) * slices(ci)), 1:slices(co)) => k%v
     if (has_dx(k)) dxk(1:(slices(lk) * slices(ci)), 1:slices(co)) => k%dv
     xslices(1:slices(lk), 1:(size(slices(s:)) / slices(lk))) => slices(s:)
     dx2(1:slices(lo), 1:slices(co), 1:slices(b)) => x2%dv
     !$omp parallel do private(s1, ss1, xslice)
     do i = 1, size(xslices, 2)
        xslice = offset_slice(xslices(:,i), slices(li), slices(ci))
        if (any(xslice > 0)) then 
           if (has_dx(x1)) then
              allocate(s1, source=pack(xslice, xslice > 0))
              allocate(ss1(size(s1), size(dx1, 2)), source=0._xp_)
              call bdB_gmmmult(1, 0, 1._xp_, xk(which(xslice > 0),:), &
                   dx2(i,:,:), ss1)
              !$omp critical
              dx1(s1,:) = dx1(s1,:) + ss1
              !$omp end critical
              deallocate(s1)
              deallocate(ss1)
           end if
           if (has_dx(k)) then
              allocate(s1, source=which(xslice > 0))
              allocate(ss1(size(s1), size(dxk, 2)), source=0._xp_)
              call bdA_gmmmult(1, 0, 1._xp_, xx1(pack(xslice, xslice > 0),:), &
                   dx2(i,:,:), ss1)
              !$omp critical
              dxk(s1,:) = dxk(s1,:) + ss1
              !$omp end critical
              deallocate(s1)
              deallocate(ss1)
           end if
        end if
     end do
     !$omp end parallel do
  end if 
end subroutine xp_number__bw_conv

subroutine xp_number__op_maxpool (x1, x2, slices)
  implicit none
  type(xp_number), intent(in), target :: x1
  type(xp_number), intent(inout), target :: x2
  integer, intent(in), target :: slices(:)
  integer, intent(inout) :: imaxloc
  integer, parameter :: ci = 1, b = 2, li = 3, lk = 4, lo = 5, s = 6 
  real(kind=xp_), pointer :: xx1(:,:,:), xx2(:,:,:)
  integer, pointer :: xslices(:,:), xslice(:)
  integer :: i, j
  xx1(1:slices(li), 1:slices(ci), 1:slices(b)) => x1%v
  xslices(1:slices(lk), 1:(size(slices(s:)) / slices(lk))) => slices(s:)
  xx2(1:slices(lo), 1:slices(ci), 1:slices(b)) => x2%v
  !$omp parallel do private(xslice)
  do i = 1, size(xslices, 2)
     xslice => xslices(:,i)
     if (all(xslice <= 0)) then
        xx2(i,:,:) = 0
     else
        xx2(i,:,:) = maxval(xx1(pack(xslice, xslice > 0), :, :), 1)
     end if
  end do
  !$omp end parallel do
end subroutine xp_number__op_maxpool

subroutine xp_number__fw_maxpool(x1, x2, slices)
  implicit none
  type(xp_number), intent(in), target :: x1
  type(xp_number), intent(inout), target :: x2
  integer, intent(in) :: slices(:)
  call raise_error("not implemented, it's only a place holder", err_generic_)
end subroutine xp_number__fw_maxpool

subroutine xp_number__bw_maxpool (x1, x2, slices)
  implicit none
  type(xp_number), intent(inout), target :: x1
  type(xp_number), intent(in), target :: x2
  integer, intent(in), target :: slices(:)
  integer, parameter :: ci = 1, b = 2, li = 3, lk = 4, lo = 5, s = 6 
  real(kind=xp_), pointer :: xx1(:,:,:), dx1(:,:,:), dx2(:,:,:)
  integer, pointer :: xslices(:,:), ximaxloc1(:)
  integer, target :: ximaxloc2(slices(ci),slices(b))
  integer, allocatable :: xslice(:)
  integer :: i, j, l
  if (has_dx(x2)) then
     xx1(1:slices(li), 1:slices(ci), 1:slices(b)) => x1%v
     dx1(1:slices(li), 1:slices(ci), 1:slices(b)) => x1%dv
     dx2(1:slices(lo), 1:slices(ci), 1:slices(b)) => x2%dv
     xslices(1:slices(lk), 1:(size(slices(s:)) / slices(lk))) => slices(s:)
     ximaxloc1(1:(slices(ci) * slices(b))) => ximaxloc2
     do i = 1, size(xslices, 2)
        if (any(xslices(:,i) > 0)) then
           allocate(xslice, source=pack(xslices(:,i), xslices(:,i) > 0))
           !!$omp parallel workshare
           ximaxloc2 = maxloc(xx1(xslice,:,:), 1)
           !!$omp end parallel workshare
           ximaxloc1 = xslice(ximaxloc1)
           !!$omp parallel do private(l)
           do j = 1, slices(b)
              do l = 1, slices(ci)
                 dx1(ximaxloc2(l, j), l, j) = dx1(ximaxloc2(l, j), l, j) + dx2(i, l, j)
              end do
           end do
           !!$omp end parallel do
           deallocate(xslice)
        end if
     end do
  end if
end subroutine xp_number__bw_maxpool
     
