  !> @addtogroup operators__objectives_interfaces_
  !! @{
  
  !> Bin Entropy - operator
  !! param[in] y 'numbner', target variable
  !! param[in] yh 'number', predictions
  !! param[inout] j 'number', objective value
  interface op_binentropy
     module procedure sp_number__op_binentropy
     module procedure dp_number__op_binentropy
  end interface op_binentropy

  !> Bin Entropy - forward differentiation
  !! param[in] y 'numbner', target variable
  !! param[in] yh 'number', predictions
  !! param[inout] j 'number', objective value
  interface fw_binentropy
     module procedure sp_number__fw_binentropy
     module procedure dp_number__fw_binentropy
  end interface fw_binentropy

  !> Bin Entropy - backward differentiation
  !! param[in] y 'numbner', target variable
  !! param[inout] yh 'number', predictions
  !! param[in] j 'number', objective value
  interface bw_binentropy
     module procedure sp_number__bw_binentropy
     module procedure dp_number__bw_binentropy
  end interface bw_binentropy

  !> Logit Binary Entropy - operator
  !! @param[in] y @b number, target
  !! @param[in] yh @b number, predictions
  !! @param[inout] j @b number, objective value
  interface op_logit_binentropy
     module procedure sp_number__op_logit_binentropy
     module procedure dp_number__op_logit_binentropy
  end interface op_logit_binentropy

  !> Logit Binary Entropy - forward differentiation
  !! @param[in] y @b number, target
  !! @param[in] yh @b number, predictions
  !! @param[inout] j @b number, objective value
  interface fw_logit_binentropy
     module procedure sp_number__fw_logit_binentropy
     module procedure dp_number__fw_logit_binentropy
  end interface fw_logit_binentropy

  !> Logit Binary Entropy - backward differentiation
  !! @param[in] y @b number, target
  !! @param[inout] yh @b number, predictions
  !! @param[in] j @b number, objective value
  interface bw_logit_binentropy
     module procedure sp_number__bw_logit_binentropy
     module procedure dp_number__bw_logit_binentropy
  end interface bw_logit_binentropy

  !> Cross-entropy - operator
  !! param[in] y 'numbner', target variable
  !! param[in] yh 'number', predictions
  !! param[inout] j 'number', objective value
  interface op_crossentropy
     module procedure sp_number__op_crossentropy
     module procedure dp_number__op_crossentropy
  end interface op_crossentropy

  !> Cross-entropy - forward differentiation
  !! param[in] y 'numbner', target variable
  !! param[in] yh 'number', predictions
  !! param[inout] j 'number', objective value
  interface fw_crossentropy
     module procedure sp_number__fw_crossentropy
     module procedure dp_number__fw_crossentropy
  end interface fw_crossentropy

  !> Cross-entropy - backward differentiation
  !! param[in] y 'numbner', target variable
  !! param[inout] yh 'number', predictions
  interface bw_crossentropy
     module procedure sp_number__bw_crossentropy
     module procedure dp_number__bw_crossentropy
  end interface bw_crossentropy

  !> Logit Cross-entropy - operator
  !! @param[in] y @b number, target
  !! @param[in] yh @b number, predictions
  !! @param[inout] j @b number, objective value
  interface op_logit_crossentropy
     module procedure sp_number__op_logit_crossentropy__1
     module procedure sp_number__op_logit_crossentropy__2
     module procedure dp_number__op_logit_crossentropy__1
     module procedure dp_number__op_logit_crossentropy__2
  end interface op_logit_crossentropy

  !> Logit Cross-entropy - forward differentiation
  !! @param[in] y @b number, target
  !! @param[in] yh @b number, predictions
  !! @param[inout] j @b number, objective value
  interface fw_logit_crossentropy
     module procedure sp_number__fw_logit_crossentropy__1
     module procedure dp_number__fw_logit_crossentropy__1
     module procedure sp_number__fw_logit_crossentropy__2
     module procedure dp_number__fw_logit_crossentropy__2
  end interface fw_logit_crossentropy

  !> Logit Cross-entropy - backward differentiation
  !! @param[in] y @b number, target
  !! @param[inout] yh @b number, predictions
  !! @param[in] j @b number, objective value
  interface bw_logit_crossentropy
     module procedure sp_number__bw_logit_crossentropy__1
     module procedure dp_number__bw_logit_crossentropy__1
     module procedure sp_number__bw_logit_crossentropy__2
     module procedure dp_number__bw_logit_crossentropy__2
  end interface bw_logit_crossentropy

  !> MSE - operator
  !! param[in] y 'numbner', target variable
  !! param[in] yh 'number', predictions
  !! param[inout] j 'number', objective value
  interface op_mse
     module procedure sp_number__op_mse
     module procedure dp_number__op_mse
  end interface op_mse

  !> MSE - forward differentiation
  !! param[in] y 'numbner', target variable
  !! param[in] yh 'number', predictions
  !! param[inout] j 'number', objective value
  interface fw_mse
     module procedure sp_number__fw_mse
     module procedure dp_number__fw_mse
  end interface fw_mse

  !> MSE - backward differentiation
  !! param[in] y 'numbner', target variable
  !! param[inout] yh 'number', predictions
  !! param[in] j 'number', objective value
  interface bw_mse
     module procedure sp_number__bw_mse
     module procedure dp_number__bw_mse
  end interface bw_mse

  !> MAE - operator
  !! param[in] y 'numbner', target variable
  !! param[in] yh 'number', predictions
  !! param[inout] j 'number', objective value
  interface op_mae
     module procedure sp_number__op_mae
     module procedure dp_number__op_mae
  end interface op_mae

  !> MAE - forward differentiation
  !! param[in] y 'numbner', target variable
  !! param[in] yh 'number', predictions
  !! param[inout] j 'number', objective value
  interface fw_mae
     module procedure sp_number__fw_mae
     module procedure dp_number__fw_mae
  end interface fw_mae

  !> MAE - backward differentiation
  !! param[in] y 'numbner', target variable
  !! param[inout] yh 'number', predictions
  !! param[in] j 'number', objective value
  interface bw_mae
     module procedure sp_number__bw_mae
     module procedure dp_number__bw_mae
  end interface bw_mae

  !> lkhnorm - operator
  !! @param[in] y 'number', observations
  !! @param[in] mu 'number', mean
  !! @param[in] s 'number', standard deviation
  !! @param[inout] L 'number' of rank 0, log-likelihood
  interface op_lkhnorm
     module procedure sp_number__op_lkhnorm__1
     module procedure sp_number__op_lkhnorm__2
     module procedure dp_number__op_lkhnorm__1
     module procedure dp_number__op_lkhnorm__2
  end interface op_lkhnorm

  !> lkhnorm__1 - forward differentiation
  !! @param[in] y 'number', observations
  !! @param[in] mu 'number', mean
  !! @param[in] s 'number', standard deviation
  !! @param[inout] L 'number' of rank 0, log-likelihood
  interface fw_lkhnorm
     module procedure sp_number__fw_lkhnorm__1
     module procedure sp_number__fw_lkhnorm__2
     module procedure dp_number__fw_lkhnorm__1
     module procedure dp_number__fw_lkhnorm__2
  end interface fw_lkhnorm

  !> lkhnorm__1 - backward differentiation
  !! @param[in] y 'number', observations
  !! @param[inout] mu 'number', mean
  !! @param[inout] s 'number', standard deviation
  !! @param[in] L 'number' of rank 0, log-likelihood
  interface bw_lkhnorm
     module procedure sp_number__bw_lkhnorm__1
     module procedure sp_number__bw_lkhnorm__2
     module procedure dp_number__bw_lkhnorm__1
     module procedure dp_number__bw_lkhnorm__2
  end interface bw_lkhnorm

  !> @}

