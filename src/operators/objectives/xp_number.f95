!> @relates op_binentropy
subroutine xp_number__op_binentropy (y, yh, j)
  implicit none
  type(xp_number), intent(in) :: y, yh
  type(xp_number), intent(inout) :: j
  !$omp parallel workshare
  j%v = sum(binentropy(y%v, yh%v))
  !$omp end parallel workshare
end subroutine xp_number__op_binentropy

!> @relates fw_binentropy
subroutine xp_number__fw_binentropy (y, yh, j)
  implicit none
  type(xp_number), intent(in) :: y, yh
  type(xp_number), intent(inout) :: j
  if (has_dx(j)) then
     !$omp parallel workshare
     j%dv = sum(yh%dv * dx_binentropy(y%v, yh%v))
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_binentropy

!> @relates bw_binentropy
subroutine xp_number__bw_binentropy (y, yh, j)
  implicit none
  type(xp_number), intent(in) :: y, j
  type(xp_number), intent(inout) :: yh
  if (has_dx(j)) then
     !$omp parallel workshare
     yh%dv = yh%dv + dx_binentropy(y%v, yh%v) * j%dv(1)
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_binentropy

!> @relates op_logit_binentropy
subroutine xp_number__op_logit_binentropy (y, z, j)
  implicit none
  type(xp_number), intent(in) :: y, z
  type(xp_number), intent(inout) :: j
  !$omp parallel workshare
  j%v = sum(logit_binentropy(y%v, z%v))
  !$omp end parallel workshare
end subroutine xp_number__op_logit_binentropy

!> @relates fw_logit_binentropy
subroutine xp_number__fw_logit_binentropy (y, z, j)
  implicit none
  type(xp_number), intent(in) :: y, z
  type(xp_number), intent(inout) :: j
  if (has_dx(j)) then
     !$omp parallel workshare
     j%dv = j%dv(1) + sum(z%dv * dx_logit_binentropy(y%v, z%v))
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_logit_binentropy

!> @relates bw_logit_binentropy
subroutine xp_number__bw_logit_binentropy (y, z, j)
  implicit none
  type(xp_number), intent(in) :: y, z
  type(xp_number), intent(inout) :: j
  if (has_dx(z)) then
     !$omp parallel workshare
     z%dv = z%dv + j%dv(1) * dx_logit_binentropy(y%v, z%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_logit_binentropy

!> @relates op_crossentropy
subroutine xp_number__op_crossentropy (y, yh, j)
  implicit none
  type(xp_number), intent(in) :: y, yh
  type(xp_number), intent(inout) :: j
  !$omp parallel workshare
  j%v = sum(crossentropy(y%v, yh%v))
  !$omp end parallel workshare
end subroutine xp_number__op_crossentropy

!> @relates fw_crossentropy
subroutine xp_number__fw_crossentropy (y, yh, j)
  implicit none
  type(xp_number), intent(in) :: y, yh
  type(xp_number), intent(inout) :: j
  if (has_dx(j)) then
     !$omp parallel workshare
     j%dv = yh%dv * sum(dx_crossentropy(y%v, yh%v))
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_crossentropy

!> @relates bw_crossentropy
subroutine xp_number__bw_crossentropy (y, yh, j)
  implicit none
  type(xp_number), intent(in) :: y, j
  type(xp_number), intent(inout) :: yh
  if (has_dx(j)) then
     !$omp parallel workshare
     yh%dv = yh%dv + dx_crossentropy(y%v, yh%v) * j%dv(1)
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_crossentropy

!> @relates op_logit_crossentropy
subroutine xp_number__op_logit_crossentropy__1 (y, z, j)
  implicit none
  type(xp_number), intent(in) :: y, z
  type(xp_number), intent(inout) :: j
  j%v = sum(LOGIT_CROSSENTROPY(y%v, z%v))
end subroutine xp_number__op_logit_crossentropy__1

!> @relates fw_logit_crossentropy
subroutine xp_number__fw_logit_crossentropy__1 (y, z, j)
  implicit none
  type(xp_number), intent(in) :: y, z
  type(xp_number), intent(inout) :: j
  j%dv = j%dv(1) + sum(z%dv * DX_LOGIT_CROSSENTROPY(y%v, z%v))
end subroutine xp_number__fw_logit_crossentropy__1

!> @relates bw_logit_crossentropy
subroutine xp_number__bw_logit_crossentropy__1 (y, z, j)
  implicit none
  type(xp_number), intent(in) :: y, j
  type(xp_number), intent(inout) :: z
  if (has_dx(z)) then
     z%dv = z%dv + j%dv(1) * DX_LOGIT_CROSSENTROPY(y%v, z%v)
  end if
end subroutine xp_number__bw_logit_crossentropy__1

subroutine xp_number__op_logit_crossentropy__2 (y, z, j, indexes)
  implicit none
  type(xp_number), intent(in) :: y, z
  type(xp_number), intent(inout) :: j
  integer, intent(in), target :: indexes(:)
  integer, pointer :: ii(:,:)
  integer :: i
  ii(1:(get_size(y) / indexes(1)), 1:indexes(1)) => indexes(2:)
  j%v = 0
  do i = 1, indexes(1)
     j%v = j%v(1) + sum(LOGIT_CROSSENTROPY(y%v(ii(:,i)), z%v(ii(:,i))))
  end do
end subroutine xp_number__op_logit_crossentropy__2

subroutine xp_number__fw_logit_crossentropy__2 (y, z, j, indexes)
  implicit none
  type(xp_number), intent(in) :: y, z
  type(xp_number), intent(inout) :: j
  integer, intent(in), target :: indexes(:)
  integer, pointer :: ii(:,:)
  integer :: i
  ii(1:(get_size(y) / indexes(1)), 1:indexes(1)) => indexes(2:)
  do i = 1, indexes(1)
     j%dv = j%dv(1) + sum(z%dv(ii(:,i)) * DX_LOGIT_CROSSENTROPY(y%v(ii(:,i)), z%v(ii(:,i))))
  end do
end subroutine xp_number__fw_logit_crossentropy__2

subroutine xp_number__bw_logit_crossentropy__2 (y, z, j, indexes)
  implicit none
  type(xp_number), intent(in) :: y, j
  type(xp_number), intent(inout) :: z
  integer, intent(in), target :: indexes(:)
  integer, pointer :: ii(:,:)
  integer :: i
  ii(1:(get_size(y) / indexes(1)), 1:indexes(1)) => indexes(2:)
  do i = 1, indexes(1)
     z%dv(ii(:,i)) = z%dv(ii(:,i)) + j%dv(1) * DX_LOGIT_CROSSENTROPY(y%v(ii(:,i)), z%v(ii(:,i)))
  end do
end subroutine xp_number__bw_logit_crossentropy__2

!> @relates op_mse
subroutine xp_number__op_mse (y, yh, j)
  implicit none
  type(xp_number), intent(in) :: y, yh
  type(xp_number), intent(inout) :: j
  integer :: n
  n = get_size(y)
  !$omp parallel workshare
  j%v = sum((y%v - yh%v)**2) / n
  !$omp end parallel workshare
end subroutine xp_number__op_mse

!> @relates fw_mse
subroutine xp_number__fw_mse (y, yh, j)
  implicit none
  type(xp_number), intent(in) :: y, yh
  type(xp_number), intent(inout) :: j
  if (has_dx(j)) then
     !$omp parallel workshare
     j%dv = sum(dx_ssq(y%v - yh%v) * (-yh%dv)) / size(y%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_mse

!> @relates be_mse
subroutine xp_number__bw_mse (y, yh, j)
  implicit none
  type(xp_number), intent(in) :: y, j
  type(xp_number), intent(inout) :: yh
  integer :: n
  n = get_size(y)
  if (has_dx(j)) then
     !$omp parallel workshare
     yh%dv = yh%dv - (dx_ssq(y%v - yh%v) * j%dv(1)) / n
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_mse

!> @relates op_mae
subroutine xp_number__op_mae (y, yh, j)
  implicit none
  type(xp_number), intent(in) :: y, yh
  type(xp_number), intent(inout) :: j
  integer :: n
  n = get_size(y)
  !$omp parallel workshare
  j%v = sum(abs(y%v - yh%v)) / n
  !$omp end parallel workshare
end subroutine xp_number__op_mae

!> @relates fw_mae
subroutine xp_number__fw_mae (y, yh, j)
  implicit none
  type(xp_number), intent(in) :: y, yh
  type(xp_number), intent(inout) :: j
  if (has_dx(j)) then
     !$omp parallel workshare
     j%dv = sum(dx_abs(y%v - yh%v) * (-yh%dv)) / size(y%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_mae

!> @relates bw_mae
subroutine xp_number__bw_mae (y, yh, j)
  implicit none
  type(xp_number), intent(in) :: y, j
  type(xp_number), intent(inout) :: yh
  integer :: n
  n = get_size(y)
  if (has_dx(j)) then
     !$omp parallel workshare
     yh%dv = yh%dv - (dx_abs(y%v - yh%v) * j%dv(1)) / n
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_mae

!> @relates op_lkhnorm
subroutine xp_number__op_lkhnorm__1 (y, mu, s, L)
  implicit none
  type(xp_number), intent(in) :: y, mu, s
  type(xp_number), intent(inout) :: L
  !$omp parallel workshare
  L%v = sum(ldnorm(y%v, mu%v, s%v))
  !$omp end parallel workshare
end subroutine xp_number__op_lkhnorm__1

!> @relates fw_lkhnorm
subroutine xp_number__fw_lkhnorm__1 (y, mu, s, L)
  implicit none
  type(xp_number), intent(in) :: y, mu, s
  type(xp_number), intent(inout) :: L
  if (has_dx(L)) then
     if (has_dx(mu)) L%dv = L%dv + sum(dmu_ldnorm(y%v, mu%v, s%v) * mu%dv)
     if (has_dx(s)) L%dv = L%dv + sum(ds_ldnorm(y%v, mu%v, s%v) * s%dv)
  end if
end subroutine xp_number__fw_lkhnorm__1

!> @relates bw_lkhnorm
subroutine xp_number__bw_lkhnorm__1 (y, mu, s, L)
  implicit none
  type(xp_number), intent(in) :: y, L
  type(xp_number), intent(inout) :: mu, s
  if (has_dx(L)) then
     if (has_dx(mu)) then
        !$omp parallel workshare
        mu%dv = mu%dv + dmu_ldnorm(y%v, mu%v, s%v) * L%dv(1)
        !$omp end parallel workshare
     end if
     if (has_dx(s)) then
        !$omp parallel workshare
        s%dv = s%dv + ds_ldnorm(y%v, mu%v, s%v) * L%dv(1)
        !$omp end parallel workshare
     end if
  end if
end subroutine xp_number__bw_lkhnorm__1
!> @}

!> @defgroup operators_objectives_lkhNorm__2_ Weighted Normal Likelohood for Independent Variables
!! @{

!> lkhnorm__2 - operator
!! @param[in] y 'number', observations
!! @param[in] mu 'number', mean
!! @param[in] s 'number', standard deviation
!! @param[in] w 'number', weights
!! @param[inout] L 'number' of rank 0, log-likelihood
subroutine xp_number__op_lkhnorm__2 (y, mu, s, w, L)
  implicit none
  type(xp_number), intent(in) :: y, mu, s, w
  type(xp_number), intent(inout) :: L
  !$omp parallel workshare
  L%v = sum(w%v * ldnorm(y%v, mu%v, s%v))
  !$omp end parallel workshare
end subroutine xp_number__op_lkhnorm__2

!> lkhnorm__2 - forward differentiation
!! @param[in] y 'number', observations
!! @param[in] mu 'number', mean
!! @param[in] s 'number', standard deviation
!! @param[in] w 'number', weights
!! @param[inout] L 'number' of rank 0, log-likelihood
subroutine xp_number__fw_lkhnorm__2 (y, mu, s, w, L)
  implicit none
  type(xp_number), intent(in) :: y, mu, s, w
  type(xp_number), intent(inout) :: L
  if (has_dx(L)) then
     if (has_dx(mu)) then
        !$omp parallel workshare
        L%dv = L%dv + sum(w%v * dmu_ldnorm(y%v, mu%v, s%v) * mu%dv)
        !$omp end parallel workshare
     end if
     if (has_dx(s)) then
        !$omp parallel workshare
        L%dv = L%dv + sum(w%v * ds_ldnorm(y%v, mu%v, s%v) * s%dv)
        !$omp end parallel workshare
     end if
  end if
end subroutine xp_number__fw_lkhnorm__2

!> lkhnorm__2 - backward differentiation
!! @param[in] y 'number', observations
!! @param[inout] mu 'number', mean
!! @param[inout] s 'number', standard deviation
!! @param[inout] w 'number', weights
!! @param[in] L 'number' of rank 0, log-likelihood
subroutine xp_number__bw_lkhnorm__2 (y, mu, s, w, L)
  implicit none
  type(xp_number), intent(in) :: y, w, L
  type(xp_number), intent(inout) :: mu, s
  if (has_dx(L)) then
     if (has_dx(mu)) then
        !$omp parallel workshare
        mu%dv = mu%dv + w%v * dmu_ldnorm(y%v, mu%v, s%v) * L%dv(1)
        !$omp end parallel workshare
     end if
     if (has_dx(s)) then
        !$omp parallel workshare
        s%dv = s%dv + w%v * ds_ldnorm(y%v, mu%v, s%v) * L%dv(1)
        !$omp end parallel workshare
     end if
  end if
end subroutine xp_number__bw_lkhnorm__2


!> @}
  
