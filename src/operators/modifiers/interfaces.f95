  !> @addtogroup operators__modifiers_interfaces_
  !! @{

  interface op_assign
     module procedure sp_number__op_assign
     module procedure dp_number__op_assign
  end interface op_assign

  interface fw_assign
     module procedure sp_number__fw_assign
     module procedure dp_number__fw_assign
  end interface fw_assign

  interface bw_assign
     module procedure sp_number__bw_assign
     module procedure dp_number__bw_assign
  end interface bw_assign
  
  !> General slice - operator
  !! @param[in   ] x1 'number' input
  !! @param[inout] x2 'number' output
  !! @param[in   ] s integer(:), slice containing the indexes w.r.t. to x1%v
  interface op_slice
     module procedure sp_number__op_slice
     module procedure dp_number__op_slice
  end interface op_slice

  !> General slice - forward differentiation
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  !! @param[in] s integer(:), slice containing the indexes w.r.t. to x1%v
  interface fw_slice
     module procedure sp_number__fw_slice
     module procedure dp_number__fw_slice
  end interface fw_slice

  !> General slice - backward differentiation
  !! @param[inout] x1 'number' input
  !! @param[in] x2 'number' output
  !! @param[in] s integer(:), slice containing the indexes w.r.t. to x1%v
  interface bw_slice
     module procedure sp_number__bw_slice
     module procedure dp_number__bw_slice
  end interface bw_slice

  !> Flat slice - operator
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  !! @param[in] s integer(:), slice containing the indexes w.r.t. to x1%v
  interface op_flatslice
     module procedure sp_number__op_flatslice
     module procedure dp_number__op_flatslice
  end interface op_flatslice

  !> Flat slice - forward differentiation
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  !! @param[in] s integer(:), slice containing the indexes w.r.t. to x1%v
  interface fw_flatslice
     module procedure sp_number__fw_flatslice
     module procedure dp_number__fw_flatslice
  end interface fw_flatslice

  !> Flat slice - backward differentiation
  !! @param[inout] x1 'number' input
  !! @param[in] x2 'number' output
  !! @param[in] s integer(:), slice containing the indexes w.r.t. to x1%v
  interface bw_flatslice
     module procedure sp_number__bw_flatslice
     module procedure dp_number__bw_flatslice
  end interface bw_flatslice

  !> Bind - operator
  !! @param[in] x1 'number', input
  !! @param[in] x2 'number', input
  !! @param[inout] x3 'number' output
  !! @param[in] k integer, dimension along hich to bind
  interface op_bind
     module procedure sp_number__op_bind
     module procedure dp_number__op_bind
  end interface op_bind

  !> Bind - forward differentiation
  !! @param[in] x1 'number', input
  !! @param[in] x2 'number', input
  !! @param[inout] x3 'number' output
  !! @param[in] k integer, dimension along hich to bind
  interface fw_bind
     module procedure sp_number__fw_bind
     module procedure dp_number__fw_bind
  end interface fw_bind

  !> Bind - backward differentiation
  !! @param[inout] x1 'number', input
  !! @param[inout] x2 'number', input
  !! @param[in] x3 'number' output
  !! @param[in] k integer, dimension along hich to bind
  interface bw_bind
     module procedure sp_number__bw_bind
     module procedure dp_number__bw_bind
  end interface bw_bind

  !> Embeddings - operator
  !! @param[in] f 'number', factors
  !! @param[in] x 'number', embeddings
  !! @param[in] e 'number', embeddings array
  interface op_embeddings
     module procedure sp_number__op_embeddings
     module procedure dp_number__op_embeddings
  end interface op_embeddings

  !> Embeddings - forward differentiation
  !! @param[in] f 'number', factors
  !! @param[in] x 'number', embeddings
  !! @param[in] e 'number', embeddings array
  interface fw_embeddings
     module procedure sp_number__fw_embeddings
     module procedure dp_number__fw_embeddings
  end interface fw_embeddings

  !> Embeddings - backward differentiation
  !! @param[in] f 'number', factors
  !! @param[in] x 'number', embeddings
  !! @param[in] e 'number', embeddings array
  interface bw_embeddings
     module procedure sp_number__bw_embeddings
     module procedure dp_number__bw_embeddings
  end interface bw_embeddings

  !> Transpose Matrix - operator
  !! @param[in] x1 @b number of rank 2, matrix to transpose
  !! @param[inout] x2 @b number of rank 2, transposed matrix
  interface op_transpose
     module procedure sp_number__op_transpose
     module procedure dp_number__op_transpose
  end interface op_transpose

  !> Transpose Matrix - forward differentiation
  !! @param[in] x1 @b number of rank 2, matrix to transpose
  !! @param[inout] x2 @b number of rank 2, transposed matrix
  interface fw_transpose
     module procedure sp_number__fw_transpose
     module procedure dp_number__fw_transpose
  end interface fw_transpose

  !> Transpose Matrix - bakward differentiation
  !! @param[inout] x1 @b number of rank 2, matrix to transpose
  !! @param[in] x2 @b number of rank 2, transposed matrix
  interface bw_transpose
     module procedure sp_number__bw_transpose
     module procedure dp_number__bw_transpose
  end interface bw_transpose

  !> Feeding Number - operator
  !! @param[in] x1 @b number of rank 2, matrix with data records as columns
  !! @param[inout] x2 @b number with shape [data record shape, batch size], data batch to feed
  !! @param[in] s1 @b integer(:) of size batch size, indeces of the records to be fed to the data batch 
  interface op_feeding_number
     module procedure sp_number__op_feeding_number
     module procedure dp_number__op_feeding_number
  end interface op_feeding_number

  !> Feeding Number Sequence - operator
  !! @param[in] x1 @b number of rank 2, matrix with data records as columns
  !! @param[inout] x2 @b number with shape [data record shape, sequnce length, batch size], data batch to feed
  !! @param[in] s1 @b integer(:) of size batch size, indeces of the starting records of each sequence to be fed to the data batch
  !! @param[in] l @b integer, length of the sequence
  interface op_feeding_sequence
     module procedure sp_number__op_feeding_sequence
     module procedure dp_number__op_feeding_sequence
  end interface op_feeding_sequence

  !> @}
