subroutine xp_number__op_assign (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  x2%v = x1%v
end subroutine xp_number__op_assign

subroutine xp_number__fw_assign (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  if (has_dx(x2)) x1%dv = x2%dv
end subroutine xp_number__fw_assign

subroutine xp_number__bw_assign (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  if (has_dx(x2)) x2%dv = x1%dv
end subroutine xp_number__bw_assign

!> @relates op_slice
subroutine xp_number__op_slice (x1, x2, s)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  integer, intent(in) :: s(:)
  x2%v = x1%v(s)
end subroutine xp_number__op_slice

!> @relates fw_slice
subroutine xp_number__fw_slice (x1, x2, s)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  integer, intent(in) :: s(:)
  x2%dv = x1%dv(s)
end subroutine xp_number__fw_slice

!> @relates bw_slice
subroutine xp_number__bw_slice (x1, x2, s)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  integer, intent(in) :: s(:)
  x1%dv(s) = x2%dv 
end subroutine xp_number__bw_slice

!> @reltes op_flatslice
subroutine xp_number__op_flatslice (x1, x2, s)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  integer, intent(in) :: s(:)
  x2%v = x1%v(s)
end subroutine xp_number__op_flatslice

!> @relates fw_flatslice
subroutine xp_number__fw_flatslice (x1, x2, s)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  integer, intent(in) :: s(:)
  x2%dv = x1%dv(s)
end subroutine xp_number__fw_flatslice

!> @relates bw_flatslice
subroutine xp_number__bw_flatslice (x1, x2, s)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  integer, intent(in) :: s(:)
  x1%dv(s) = x2%dv 
end subroutine xp_number__bw_flatslice

!> @relates op_bind
subroutine xp_number__op_bind (x1, x2, x3, k)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  integer, intent(in) :: k
  call bind_fill(x3%v, x1%v, x2%v, x1%shp, x2%shp, k, .false.)
end subroutine xp_number__op_bind

!> @relates fw_bind
subroutine xp_number__fw_bind (x1, x2, x3, k)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  integer, intent(in) :: k
  call bind_fill(x3%dv, x1%dv, x2%dv, x1%shp, x2%shp, k, .false.)
end subroutine xp_number__fw_bind

!> @relates bw_bind
subroutine xp_number__bw_bind (x1, x2, x3, k)
  implicit none
  type(xp_number), intent(inout) :: x1, x2
  type(xp_number), intent(in) :: x3
  integer, intent(in) :: k
  call bind_fill(x3%dv, x1%dv, x2%dv, x1%shp, x2%shp, k, .true.)
end subroutine xp_number__bw_bind

!> @relates op_embeddings
subroutine xp_number__op_embeddings (f, x, e)
  implicit none
  type(xp_number), intent(in) :: f, x
  type(xp_number), intent(inout) :: e
  real(kind=xp_), pointer :: xx(:,:)
  integer :: i, j, a, b
  call with_shape(x, xx, .false.)
  if (get_rank(f) == 0) then
     i = nint(f%v(1))
     !$omp parallel workshare
     e%v = xx(:,i) 
     !$omp end parallel workshare
  else
     !$omp parallel do private(j, a, b)
     do i = 1, get_size(f)
        j = nint(f%v(i))
        a = 1 + (i - 1) * size(xx, 1)
        b = a + size(xx, 1)
        e%v(a:b) = xx(:,j)
     end do
     !$omp end parallel do
  end if
end subroutine xp_number__op_embeddings

!> @relates fw_embeddings
subroutine xp_number__fw_embeddings (f, x, e)
  implicit none
  type(xp_number), intent(in) :: f, e
  type(xp_number), intent(inout) :: x
  real(kind=xp_), pointer :: dx(:,:)
  integer :: i, j, a, b
  call with_shape(x, dx, .true.)
  if (get_rank(f) == 0) then
     i = nint(f%v(1))
     !$omp parallel workshare
     e%dv = dx(:,i)
     !$omp end parallel workshare
  else
     !$omp parallel do private(j, a, b)
      do i = 1, get_size(f)
        j = nint(f%v(i))
        a = 1 + (i - 1) * size(dx, 1)
        b = a + size(dx, 1)
        e%dv(a:b) = dx(:,j)
     end do
     !$omp end parallel do
  end if
end subroutine xp_number__fw_embeddings

!> @relates bw_embeddings
subroutine xp_number__bw_embeddings (f, x, e)
  implicit none
  type(xp_number), intent(in) :: f, e
  type(xp_number), intent(inout) :: x
  real(kind=xp_), pointer :: dx(:,:)
  integer :: i, j, a, b
  call with_shape(x, dx, .true.)
  if (get_rank(f) == 0) then
     i = nint(f%v(1))
     !$omp parallel workshare
     dx(:,i) = dx(:,i) + e%dv
     !$omp end parallel workshare
  else
     !$omp parallel do private(j, a, b)
      do i = 1, get_size(f)
        j = nint(f%v(i))
        a = 1 + (i - 1) * size(dx, 1)
        b = a + size(dx, 1)
        dx(:,j) = dx(:,j) + e%dv(a:b)
     end do
     !$omp end parallel do
  end if
end subroutine xp_number__bw_embeddings

!> @relates op_transpose
subroutine xp_number__op_transpose (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  real(kind=xp_), pointer :: xx1(:,:), xx2(:,:)
  call with_shape(x1, xx1, .false.)
  call with_shape(x2, xx2, .false.)
  xx2 = transpose(xx1)
end subroutine xp_number__op_transpose

!> @relates fw_transpose
subroutine xp_number__fw_transpose (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  real(kind=xp_), pointer :: xx1(:,:), xx2(:,:)
  call with_shape(x1, xx1, .true.)
  call with_shape(x2, xx2, .true.)
  xx2 = transpose(xx1)
end subroutine xp_number__fw_transpose

!> @relates bw_transpose
subroutine xp_number__bw_transpose (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  real(kind=xp_), pointer :: xx1(:,:), xx2(:,:)
  call with_shape(x1, xx1, .true.)
  call with_shape(x2, xx2, .true.)
  xx1 = transpose(xx2)
end subroutine xp_number__bw_transpose

!> @relates op_feeding_number
subroutine xp_number__op_feeding_number (x1, x2, s1)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  integer, intent(in) :: s1(:)
  real(kind=xp_), pointer :: xx1(:,:)
  integer :: i, j, a, b, bsz
  call with_shape(x1, xx1, .false.)
  bsz = size(s1)
  !$omp parallel do private(a, b, j)
  do i = 1, bsz
     a = (i - 1) * size(xx1, 1) + 1
     b = i * size(xx1, 1)
     j = s1(i)
     x2%v(a:b) = xx1(:,j)
  end do
  !$omp end parallel do
end subroutine xp_number__op_feeding_number

!> @relates op_feeding_sequence
subroutine xp_number__op_feeding_sequence (x1, x2, s1, l, bybsz)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  integer, intent(in) :: s1(:), l, bybsz
  integer :: i, a, b, s2(size(s1) * l)
  if (bybsz > 0) then
     call private_by_batchsize
  else
     call private_by_seqlength
  end if
  call op_feeding_number(x1, x2, s2)
contains
  subroutine private_by_batchsize
    integer :: bz
    bz = size(s1)
    !$omp parallel do private(a, b)
    do i = 0, l - 1
       a = i * bz + 1
       b = (i + 1) * bz
       s2(a:b) = s1 + i
    end do
    !$omp end parallel do
  end subroutine private_by_batchsize
  subroutine private_by_seqlength
    integer :: ll(l)
    ll = [(i, i=0, l-1)]
    !$omp parallel do private(a, b)
    do i = 1, size(s1)
       a = (i - 1) * l + 1
       b = (i + 1) * l
       s2(a:b) = s1(i) + ll
    end do
    !$omp end parallel do
  end subroutine private_by_seqlength
end subroutine xp_number__op_feeding_sequence
