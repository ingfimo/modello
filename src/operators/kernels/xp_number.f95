!> @relates op_ksqexp
subroutine xp_number__op_ksqexp (x1, x2, a, b, k)
  implicit none
  type(xp_number), intent(in) :: x1, x2, a, b
  type(xp_number), intent(inout) :: k
  if (get_rank(b) == 0) then
     call private_ksqexp0_2_2
  else if (get_rank(b) == 1) then
     call private_ksqexp1_2_2
  end if
contains
  subroutine private_ksqexp0_2_2
    real(kind=xp_), pointer :: xx1(:,:), xx2(:,:), xk(:,:)
    integer :: i, j
    call with_shape(x1, xx1, .false.)
    call with_shape(x2, xx2, .false.)
    call with_shape(k, xk, .false.)
    !$omp parallel do
    do i = 1, size(xx1, 2)
       do j = 1, size(xx2, 2)
          xk(j,i) = ksqexp(xx1(:,i), xx2(:,j), a%v(1), b%v(1))
       end do
    end do
    !$omp end parallel do
  end subroutine private_ksqexp0_2_2
  subroutine private_ksqexp1_2_2
    real(kind=xp_), pointer :: xx1(:,:), xx2(:,:), xk(:,:)
    integer :: i, j
    call with_shape(x1, xx1, .false.)
    call with_shape(x2, xx2, .false.)
    call with_shape(k, xk, .false.)
    !$omp parallel do
    do i = 1, size(xx1, 2)
       do j = 1, size(xx2, 2)
          xk(j,i) = ksqexp(xx1(:,i), xx2(:,j), a%v(1), b%v)
       end do
    end do
    !$omp end parallel do
  end subroutine private_ksqexp1_2_2
end subroutine xp_number__op_ksqexp

!> @relates fw_ksqexp
subroutine xp_number__fw_ksqexp(x1, x2, a, b, k)
  implicit none
  type(xp_number), intent(in) :: x1, x2, a, b
  type(xp_number), intent(inout) :: k
  call raise_error("fw_ksqexp is only a placeholder", err_generic_)
end subroutine xp_number__fw_ksqexp

!> @relates bw_ksqexp
subroutine xp_number__bw_ksqexp (x1, x2, a, b, k)
  implicit none
  type(xp_number), intent(in) :: k
  type(xp_number), intent(inout) :: x1, x2, a, b
  if (get_rank(b) == 0) then
     call private_ksqexp0_2_2
  else if (get_rank(b) == 1) then
     call private_ksqexp1_2_2
  end if
contains
  subroutine private_ksqexp0_2_2
    real(kind=xp_), pointer :: xx1(:,:), xx2(:,:)
    real(kind=xp_), pointer :: dxx1(:,:), dxx2(:,:), dxk(:,:)
    integer :: i, j
    call with_shape(x1, xx1, .false.)
    call with_shape(x2, xx2, .false.)
    call with_shape(x1, dxx1, .true.)
    call with_shape(x2, dxx2, .true.)
    call with_shape(k, dxk, .true.)
    do i = 1, size(xx1, 2)
       do j = 1, size(xx2, 2)
          if (has_dx(x1)) dxx1(:,i) = dxx1(:,i) + DX1_KSQEXP(xx1(:,i), xx2(:,j), a%v(1), b%v(1)) * dxk(j,i)
          if (has_dx(x2)) dxx2(:,j) = dxx2(:,j) + DX2_KSQEXP(xx1(:,i), xx2(:,j), a%v(1), b%v(1)) * dxk(j,i)
          if (has_dx(a)) a%dv(1) = a%dv(1) + DA_KSQEXP(xx1(:,i), xx2(:,j), a%v(1), b%v(1)) * dxk(j,i)
          if (has_dx(b)) b%dv(1) = b%dv(1) + DB_KSQEXP(xx1(:,i), xx2(:,j), a%v(1), b%v(1)) * dxk(j,i)
       end do
    end do
  end subroutine private_ksqexp0_2_2
  subroutine private_ksqexp1_2_2
    real(kind=xp_), pointer :: xx1(:,:), xx2(:,:)
    real(kind=xp_), pointer :: dxx1(:,:), dxx2(:,:), dxk(:,:)
    integer :: i, j
    call with_shape(x1, xx1, .false.)
    call with_shape(x2, xx2, .false.)
    call with_shape(x1, dxx1, .true.)
    call with_shape(x2, dxx2, .true.)
    call with_shape(k, dxk, .true.) 
    do i = 1, size(xx1, 2)
       do j = 1, size(xx2, 2)
          if (has_dx(x1)) dxx1(:,i) = dxx1(:,i) + DX1_KSQEXP(xx1(:,i), xx2(:,i), a%v(1), b%v) * dxk(j,i)
          if (has_dx(x2)) dxx2(:,j) = dxx2(:,j) + DX2_KSQEXP(xx1(:,i), xx2(:,j), a%v(1), b%v) * dxk(j,i)
          if (has_dx(a)) a%dv(1) = a%dv(1) + DA_KSQEXP(xx1(:,i), xx2(:,i), a%v(1), b%v) * dxk(j,i)
          if (has_dx(b)) b%dv = b%dv + DB_KSQEXP(xx1(:,i), xx2(:,j), a%v(1), b%v) * dxk(j,i)
       end do
    end do
  end subroutine private_ksqexp1_2_2
end subroutine xp_number__bw_ksqexp
!> @}

!> @}
