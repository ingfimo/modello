  !> @addtogroup operators__kernels_interfaces_ 
  !! @{

  !> ksqexp - operator
  !! @param[in] x1 'number', feature vector
  !! @param[in] x2 'number', feature vector
  !! @param[in] a 'number', amplitude parameter
  !! @param[in] b 'number', rate parameter
  !! @param[inout] k 'number', kernel value
  interface op_ksqexp
     module procedure sp_number__op_ksqexp
     module procedure dp_number__op_ksqexp
  end interface op_ksqexp

  !> ksqexp - forward differentiation
  !! @todo PLACE HOLDER
  interface fw_ksqexp
     module procedure sp_number__fw_ksqexp
     module procedure dp_number__fw_ksqexp
  end interface fw_ksqexp

  !> ksqexp - backward differentiation
  !! @param[in] x1 'number', feature vector
  !! @param[in] x2 'number', feature vector
  !! @param[in] a 'number', amplitude parameter
  !! @param[in] b 'number', rate parameter
  !! @param[inout] k 'number', kernel value
  interface bw_ksqexp
     module procedure sp_number__bw_ksqexp
     module procedure dp_number__bw_ksqexp
  end interface bw_ksqexp

  !> @}
