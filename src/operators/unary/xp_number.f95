!> @relates op_neg
subroutine xp_number__op_neg (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = -x1%v
  !$omp end parallel workshare
end subroutine xp_number__op_neg

!> @relates fw_neg
subroutine xp_number__fw_neg (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%dv = -x1%dv
  !$omp end parallel workshare
end subroutine xp_number__fw_neg

!> @relates bw_neg
subroutine xp_number__bw_neg (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  !$omp parallel workshare
  x1%dv = x1%dv - x2%dv
  !$omp end parallel workshare
end subroutine xp_number__bw_neg

!> @relates op_abs
subroutine xp_number__op_abs (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = abs(x1%v)
  !$omp end parallel workshare
end subroutine xp_number__op_abs

!> @relates fw_abs
subroutine xp_number__fw_abs (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x2%dv = dx_abs(x1%v) * x1%dv
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_abs

!> @relates bw_abs
subroutine xp_number__bw_abs (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x1%dv = x1%dv + dx_abs(x1%v) * x2%dv
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_abs

!> @relates op_exp
subroutine xp_number__op_exp (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = exp(x1%v)
  !$omp end parallel workshare
end subroutine xp_number__op_exp

!> @relates fw_exp
subroutine xp_number__fw_exp (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(in) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x2%dv = x1%dv * dx_exp(x1%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_exp

!> @relates bw_exp
subroutine xp_number__bw_exp (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x1%dv = x1%dv + dx_exp(x1%v) * x2%dv
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_exp

!> @relates op_log
subroutine xp_number__op_log (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = log(x1%v)
  !$omp end parallel workshare
end subroutine xp_number__op_log

!> @relates fw_log
subroutine xp_number__fw_log (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x2%dv = x1%dv * dx_log(x1%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_log

!> @relates bw_log
subroutine xp_number__bw_log (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x1%dv = x1%dv + dx_log(x1%v) * x2%dv
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_log

!> @relates op_sin
subroutine xp_number__op_sin (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = sin(x1%v)
  !$omp end parallel workshare
end subroutine xp_number__op_sin

!> @relates fw_sin
subroutine xp_number__fw_sin (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x2%dv = x1%dv * dx_sin(x1%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_sin

!> @relates bw_sin
subroutine xp_number__bw_sin (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x1%dv = x1%dv + dx_sin(x1%v) * x2%dv
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_sin

!> @relates op_cos
subroutine xp_number__op_cos (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = cos(x1%v)
  !$omp end parallel workshare
end subroutine xp_number__op_cos

!> @relates fw_cos
subroutine xp_number__fw_cos (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x2%dv = x1%dv * dx_cos(x1%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_cos

!> @bw_cos
subroutine xp_number__bw_cos (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x1%dv = x1%dv + dx_cos(x1%v) * x2%dv
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_cos

!> @relates op_tan
subroutine xp_number__op_tan (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = tan(x1%v)
  !$omp end parallel workshare
end subroutine xp_number__op_tan

!> @relates fw_tan
subroutine xp_number__fw_tan (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x2%dv = x1%dv * dx_tan(x1%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_tan

!> @relates bw_tan
subroutine xp_number__bw_tan (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x1%dv = x1%dv + dx_tan(x1%v) * x2%dv
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_tan

!> @relates op_sinh
subroutine xp_number__op_sinh (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = sinh(x1%v)
  !$omp end parallel workshare
end subroutine xp_number__op_sinh

!> @relates fw_sinh
subroutine xp_number__fw_sinh (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x2%dv = x1%dv * dx_sinh(x1%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_sinh

!> @relates bw_sinh
subroutine xp_number__bw_sinh (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x1%dv = x1%dv + dx_sinh(x1%v) * x2%dv
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_sinh

!> @relates op_cosh
subroutine xp_number__op_cosh (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = cosh(x1%v)
  !$omp end parallel workshare
end subroutine xp_number__op_cosh

!> @relates fw_cosh
subroutine xp_number__fw_cosh (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x2%dv = x1%dv * dx_cosh(x1%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_cosh

!> @relates bw_cosh
subroutine xp_number__bw_cosh (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x1%dv = x1%dv + dx_cosh(x1%v) * x2%dv
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_cosh

!> @relates op_tanh
subroutine xp_number__op_tanh (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = tanh(x1%v)
  !$omp end parallel workshare
end subroutine xp_number__op_tanh

!> @relates fw_tanh
subroutine xp_number__fw_tanh (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x2%dv = x1%dv * dx_tanh(x1%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_tanh

!> @relates bw_tanh
subroutine xp_number__bw_tanh (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x1%dv = x1%dv + dx_tanh(x1%v) * x2%dv
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_tanh
!> @}

!> @}
