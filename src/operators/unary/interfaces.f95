  !> @addtogroup operators__unary_interfaces_
  !! @{

  !> Negation - operator
  !! @param[in] x1 @b number
  !! @param[inout] @b number, negation of x1 (-x1)
  interface op_neg
     module procedure sp_number__op_neg
     module procedure dp_number__op_neg
  end interface op_neg

  !> Negation - forward differentiation
  !! @param[in] x1 @b number
  !! @param[inout] x2 @ number, negation of x1 (-x1)
  interface fw_neg
     module procedure sp_number__fw_neg
     module procedure dp_number__fw_neg
  end interface fw_neg

  !> Negation - backward differentiation
  !! @param[inout] x1 @b number
  !! @param[in] x1 @b number, negation of x1 (-x1)
  interface bw_neg
     module procedure sp_number__bw_neg
     module procedure dp_number__bw_neg
  end interface bw_neg

  !> Absolute value (abs) - operator
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface op_abs
     module procedure sp_number__op_abs
     module procedure dp_number__op_abs
  end interface op_abs

  !> Absolute value (abs) - forward differentiation
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface fw_abs
     module procedure sp_number__fw_abs
     module procedure dp_number__fw_abs
  end interface fw_abs

  !> Absolute (value) - backward differentiation
  !! @param[inout] x1 'number' input
  !! @param[in] x2 'number' output
  interface bw_abs
     module procedure sp_number__bw_abs
     module procedure dp_number__bw_abs
  end interface bw_abs

  !> Exponential (exp) - operator
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface op_exp
     module procedure sp_number__op_exp
     module procedure dp_number__op_exp
  end interface op_exp

  !> Exponential (exp) - forward differentiation
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface fw_exp
     module procedure sp_number__fw_exp
     module procedure dp_number__fw_exp
  end interface fw_exp

  !> Exponential (exp) - backward differentiation
  !! @param[inout] x1 'number' input
  !! @param[in] x2 'number' output
  interface bw_exp
     module procedure sp_number__bw_exp
     module procedure dp_number__bw_exp
  end interface bw_exp

  !> Logarithm - operator
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface op_log
     module procedure sp_number__op_log
     module procedure dp_number__op_log
  end interface op_log

  !> Logarithm - forward differentiation
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface fw_log
     module procedure sp_number__fw_log
     module procedure dp_number__fw_log
  end interface fw_log

  !> Logarithm - backward differentiation
  !! @param[inout] x1 'number' input
  !! @param[in] x2 'number' output
  interface bw_log
     module procedure sp_number__bw_log
     module procedure dp_number__bw_log
  end interface bw_log

  !> Sine - operator
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface op_sin
     module procedure sp_number__op_sin
     module procedure dp_number__op_sin
  end interface op_sin

  !> Sine - forward differentiation
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface fw_sin
     module procedure sp_number__fw_sin
     module procedure dp_number__fw_sin
  end interface fw_sin

  !> sine - backward differenatiation
  !! @param[inout] x1 'number' input
  !! @param[in] x2 'number' output
  interface bw_sin
     module procedure sp_number__bw_sin
     module procedure dp_number__bw_sin
  end interface bw_sin

  !> Cosine - operator
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface op_cos
     module procedure sp_number__op_cos
     module procedure dp_number__op_cos
  end interface op_cos

  !> Cosine - forward differenation
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface fw_cos
     module procedure sp_number__fw_cos
     module procedure dp_number__fw_cos
  end interface fw_cos

  !> Cosine - backward differenation
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface bw_cos
     module procedure sp_number__bw_cos
     module procedure dp_number__bw_cos
  end interface bw_cos

  !> Tangent - operator
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface op_tan
     module procedure sp_number__op_tan
     module procedure dp_number__op_tan
  end interface op_tan

  !> Tangent - forward differentiation
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface fw_tan
     module procedure sp_number__fw_tan
     module procedure dp_number__fw_tan
  end interface fw_tan

  !> Tangent - backward differentiation
  !! @param[inout] x1 'number' input
  !! @param[in] x2 'number' output
  interface bw_tan
     module procedure sp_number__bw_tan
     module procedure dp_number__bw_tan
  end interface bw_tan

  !> Hyperbolic Sine - operator
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface op_sinh
     module procedure sp_number__op_sinh
     module procedure dp_number__op_sinh
  end interface op_sinh

  !> Hyperbolic Sine - forward differentiation
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface fw_sinh
     module procedure sp_number__fw_sinh
     module procedure dp_number__fw_sinh
  end interface fw_sinh

  !> Hyperbolic Sine - backward differentiation
  !! @param[inout] x1 'number' input
  !! @param[in] x2 'number' output
  interface bw_sinh
     module procedure sp_number__bw_sinh
     module procedure dp_number__bw_sinh
  end interface bw_sinh

  !> Hyperbolic Cosine - operator
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface op_cosh
     module procedure sp_number__op_cosh
     module procedure dp_number__op_cosh
  end interface op_cosh

  !> Hyperbolic Cosine - forward differentiation
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface fw_cosh
     module procedure sp_number__fw_cosh
     module procedure dp_number__fw_cosh
  end interface fw_cosh

  !> Hyperbolic Cosine - backward differentiation
  !! @param[inout] x1 'number' input
  !! @param[in] x2 'number' output
  interface bw_cosh
     module procedure sp_number__bw_cosh
     module procedure dp_number__bw_cosh
  end interface bw_cosh

  !> Hyperbolic Tangent - operator
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface op_tanh
     module procedure sp_number__op_tanh
     module procedure dp_number__op_tanh
  end interface op_tanh

  !> Hyerbolic Tangent - forward difference
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface fw_tanh
     module procedure sp_number__fw_tanh
     module procedure dp_number__fw_tanh
  end interface fw_tanh

  !> Hyperbolic Tangent - backward difference
  !! @param[inout] x1 'number' input
  !! @param[in] x2 'number' output
  interface bw_tanh
     module procedure sp_number__bw_tanh
     module procedure dp_number__bw_tanh
  end interface bw_tanh
  
  !> @}
