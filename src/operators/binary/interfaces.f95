  !> @addtogroup operators__binary_interfaces_ 
  !! @{

  !> Addition - operator
  !! @param[in] x1 'number', input
  !! @param[in] x2 'number', input
  !! @param[inout] x3 'number' output
  interface op_add
     module procedure sp_number__op_add__1
     module procedure sp_number__op_add__2
     module procedure dp_number__op_add__1
     module procedure dp_number__op_add__2
  end interface op_add

  !> Addition - forward differentiation
  !! @param[in] x1 'number', input
  !! @param[in] x2 'number', input
  !! @param[inout] x3 'number' output
  interface fw_add
     module procedure sp_number__fw_add__1
     module procedure sp_number__fw_add__2
     module procedure dp_number__fw_add__1
     module procedure dp_number__fw_add__2
  end interface fw_add

  !> Addition -backward differentiation
  !! @param[inout] x1 'number', input
  !! @param[inout] x2 'number', input
  !! @param[in] x3 'number' output
  interface bw_add
     module procedure sp_number__bw_add__1
     module procedure sp_number__bw_add__2
     module procedure dp_number__bw_add__1
     module procedure dp_number__bw_add__2
  end interface bw_add

  !> Subtraction - operator
  !! @param[in] x1 'number', input
  !! @param[in] x2 'number', input
  !! @param[inout] x3 'number' output
  interface op_sub
     module procedure sp_number__op_sub__1
     module procedure sp_number__op_sub__2
     module procedure dp_number__op_sub__1
     module procedure dp_number__op_sub__2
  end interface op_sub

  !> Subtraction - forward differentiation
  !! @param[in] x1 'number', input
  !! @param[in] x2 'number', input
  !! @param[inout] x3 'number' output
  interface fw_sub
     module procedure sp_number__fw_sub__1
     module procedure sp_number__fw_sub__2
     module procedure dp_number__fw_sub__1
     module procedure dp_number__fw_sub__2
  end interface fw_sub

  !> Subtraction - backeard differentiation
  !! @param[inout] x1 'number', input
  !! @param[inout] x2 'number', input
  !! @param[in] x3 'number' output
  interface bw_sub
     module procedure sp_number__bw_sub__1
     module procedure sp_number__bw_sub__2
     module procedure dp_number__bw_sub__1
     module procedure dp_number__bw_sub__2
  end interface bw_sub

  !> Multiplication - operator
  !! @param[in] x1 'number', input
  !! @param[in] x2 'number', input
  !! @param[inout] x3 'number' output
  interface op_mult
     module procedure sp_number__op_mult__1
     module procedure sp_number__op_mult__2
     module procedure dp_number__op_mult__1
     module procedure dp_number__op_mult__2
  end interface op_mult

  !> Multiplication - forward differentiation
  !! @param[in] x1 'number', input
  !! @param[in] x2 'number', input
  !! @param[inout] x3 'number' output
  interface fw_mult
     module procedure sp_number__fw_mult__1
     module procedure sp_number__fw_mult__2
     module procedure dp_number__fw_mult__1
     module procedure dp_number__fw_mult__2
  end interface fw_mult

  !> Multiplication - backward differentiation
  !! @param[inout] x1 'number', input
  !! @param[inout] x2 'number', input
  !! @param[in] x3 'number' output
  interface bw_mult
     module procedure sp_number__bw_mult__1
     module procedure sp_number__bw_mult__2
     module procedure dp_number__bw_mult__1
     module procedure dp_number__bw_mult__2
  end interface bw_mult

  !> Power - operator
  !! @param[in] x1 'number', input
  !! @param[in] x2 'number', input
  !! @param[inout] x3 'number' output
  interface op_pow
     module procedure sp_number__op_pow__1
     module procedure sp_number__op_pow__2
     module procedure dp_number__op_pow__1
     module procedure dp_number__op_pow__2
  end interface op_pow

  !> Power - forward differentiation
  !! @param[in] x1 'number', input
  !! @param[in] x2 'number', input
  !! @param[inout] x3 'number' output
  interface fw_pow
     module procedure sp_number__fw_pow__1
     module procedure sp_number__fw_pow__2
     module procedure dp_number__fw_pow__1
     module procedure dp_number__fw_pow__2
  end interface fw_pow

  !> Power - backward differentiation
  !! @param[inout] x1 'number', input
  !! @param[inout] x2 'number', input
  !! @param[in] x3 'number' output
  interface bw_pow
     module procedure sp_number__bw_pow__1
     module procedure sp_number__bw_pow__2
     module procedure dp_number__bw_pow__1
     module procedure dp_number__bw_pow__2
  end interface bw_pow

  !> Division - operator
  !! @param[in] x1 'number', input
  !! @param[in] x2 'number', input
  !! @param[inout] x3 'number' output
  interface op_div
     module procedure sp_number__op_div__1
     module procedure sp_number__op_div__2
     module procedure dp_number__op_div__1
     module procedure dp_number__op_div__2
  end interface op_div

  !> Division - forward differentiation
  !! @param[in] x1 'number', input
  !! @param[in] x2 'number', input
  !! @param[inout] x3 'number' output
  interface fw_div
     module procedure sp_number__fw_div__1
     module procedure sp_number__fw_div__2
     module procedure dp_number__fw_div__1
     module procedure dp_number__fw_div__2
  end interface fw_div

  !> Division - backward differentiation
  !! @param[inout] x1 'number', input
  !! @param[inout] x2 'number', input
  !! @param[in] x3 'number' output
  interface bw_div
     module procedure sp_number__bw_div__1
     module procedure sp_number__bw_div__2
     module procedure dp_number__bw_div__1
     module procedure dp_number__bw_div__2
  end interface bw_div

  !> @}
