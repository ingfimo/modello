!> @relates op_add
subroutine xp_number__op_add__1 (x1, x2, x3)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  integer :: i
  if (get_rank(x1) == 0) then
     !$omp parallel workshare
     x3%v = x1%v(1) + x2%v
     !$omp end parallel workshare
  else if (get_rank(x2) == 0) then
     !$omp parallel workshare
     x3%v = x1%v + x2%v(1)
     !$omp end parallel workshare
  else if (get_size(x1) == get_size(x2)) then
     !$omp parallel workshare     
     x3%v = x1%v + x2%v
     !$omp end parallel workshare
  else
     call private_bcast
  end if
contains
  subroutine private_bcast
    real(kind=xp_), pointer :: xh(:,:), xo(:,:), xl(:)
    integer :: i, n
    if (get_size(x1) < get_size(x2)) then
       call reshape_bcast(x2%v, get_size(x1), xh)
       call reshape_bcast(x3%v, get_size(x1), xo)
       xl => x1%v
    else
       call reshape_bcast(x1%v, get_size(x2), xh)
       call reshape_bcast(x3%v, get_size(x2), xo)
       xl => x2%v
    end if
    !$omp parallel do
    do i = 1, size(xo, 2)
       xo(:,i) = xh(:,i) + xl
    end do
    !$omp end parallel do
  end subroutine private_bcast
end subroutine xp_number__op_add__1

!> @relates fw_add
subroutine xp_number__fw_add__1 (x1, x2, x3)
  type(xp_number) :: x1, x2
  type(xp_number), intent(inout) :: x3
  if (has_dx(x3)) then
     call private_fw_x(x1)
     call private_fw_x(x2)
  end if
contains
  subroutine private_fw_x (x)
    type(xp_number), intent(in) :: x
    if (has_dx(x)) then
       if (get_rank(x) == 0) then
          x3%dv = x3%dv + x%dv(1)
       else if (get_size(x) == get_size(x3)) then
          x3%dv = x3%dv + x%dv
       else
          call private_bcast(x)
       end if
    end if
  end subroutine private_fw_x
  subroutine private_bcast (x)
    type(xp_number), intent(in) :: x
    real(kind=xp_), pointer :: xo(:,:)
    integer :: i
    call reshape_bcast(x3%dv, get_size(x), xo)
    do i = 1, size(xo, 2)
       xo(:,i) = xo(:,i) + x%dv
    end do
  end subroutine private_bcast
end subroutine xp_number__fw_add__1

!> @relates bw_add
subroutine xp_number__bw_add__1 (x1, x2, x3)
  implicit none
  type(xp_number), intent(inout) :: x1, x2
  type(xp_number), intent(in) :: x3
  if (has_dx(x3)) then
     call private_bw_x(x1)
     call private_bw_x(x2)
  end if
contains
  subroutine private_bw_x (x)
    type(xp_number), intent(inout) :: x
    if (has_dx(x)) then
       if (get_rank(x) == 0) then
          !omp parallel workshare
          x%dv = x%dv + sum(x3%dv)
          !omp end parallel workshare
       else if (get_size(x) == get_size(x3)) then
          !omp parallel workshare
          x%dv = x%dv + x3%dv
          !omp end parallel workshare
       else
          call private_bcast(x)
       end if
    end if
  end subroutine private_bw_x
  subroutine private_bcast (x)
    type(xp_number), intent(in) :: x
    real(kind=xp_), pointer :: xo(:,:)
    integer :: i, j
    call reshape_bcast(x3%dv, get_size(x), xo)
    !$omp parallel workshare
    x%dv = x%dv + sum(xo, 2)
    !$omp end parallel workshare
  end subroutine private_bcast
end subroutine xp_number__bw_add__1

!> @relates op_add
subroutine xp_number__op_add__2 (x1, x2, x3, indexes)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  integer, intent(in), target :: indexes(:)
  if (get_size(x1) > get_size(x2)) then
     call private_add(x1, x2)
  else
     call private_add(x2, x1)
  end if
contains
  subroutine private_add(xx1, xx2)
    type(xp_number), intent(in) :: xx1, xx2
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(xx2), 1:indexes(1)) => indexes(2:)
    !$omp parallel do
    do i = 1, indexes(1)
       x3%v(j(:,i)) = xx1%v(j(:,i)) + xx2%v
    end do
    !$omp end parallel do
  end subroutine private_add
end subroutine xp_number__op_add__2

!> @relates fw_add
subroutine xp_number__fw_add__2 (x1, x2, x3, indexes)
  type(xp_number) :: x1, x2
  type(xp_number), intent(inout) :: x3
  integer, intent(in), target :: indexes(:)
  if (has_dx(x3)) then
     if (get_size(x1) > get_size(x2)) then
        call private_fw(x1, x2)
     else
        call private_fw(x2, x1)
     end if
  end if
contains
  subroutine private_fw (xx1, xx2)
    type(xp_number), intent(in) :: xx1, xx2
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(xx2), 1:indexes(1)) => indexes(2:)
    if (has_dx(xx1)) x3%dv = x3%dv + xx1%dv
    if (has_dx(xx2)) then
       do i = 1, indexes(1)
          x3%dv(j(:,i)) = x3%dv(j(:,i)) + xx2%dv
       end do
    end if
  end subroutine private_fw
end subroutine xp_number__fw_add__2

!> @relates bw_add
subroutine xp_number__bw_add__2 (x1, x2, x3, indexes)
  implicit none
  type(xp_number), intent(inout) :: x1, x2
  type(xp_number), intent(in) :: x3
  integer, intent(in), target :: indexes(:)
  if (has_dx(x3)) then
     if (get_size(x1) > get_size(x2)) then
        call private_bw(x1, x2)
     else
        call private_bw(x2, x1)
     end if
  end if
contains
  subroutine private_bw (xx1, xx2)
    !integer, pointer :: j(:,:)
    integer :: i
    type(xp_number), intent(inout) :: xx1, xx2
    if (has_dx(xx1)) then
       !$omp parallel workshare
       xx1%dv = xx1%dv + x3%dv
       !$omp end parallel workshare
    end if
    if (has_dx(xx2)) then
       call private_bw_xx2(xx2%dv)
       ! j(1:get_size(xx2), 1:indexes(1)) => indexes(2:)
       ! do i = 1, indexes(1)
       !    !$omp parallel workshare
       !    xx2%dv = xx2%dv + x3%dv(j(:,i))
       !    !$omp end parallel workshare
       ! end do
    end if
  end subroutine private_bw
  subroutine private_bw_xx2 (x)
    real(kind=xp_), intent(inout) :: x(:)
    integer :: i
    integer, pointer :: j(:,:)
    j(1:size(x), 1:indexes(1)) => indexes(2:)
    !$omp parallel do reduction(+:x)
    do i = 1, indexes(1)
       x = x + x3%dv(j(:,i))
    end do
    !$omp end parallel do
  end subroutine private_bw_xx2
end subroutine xp_number__bw_add__2

!> @relates op_sub
subroutine xp_number__op_sub__1 (x1, x2, x3)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  if (get_rank(x1) == 0) then
     !$omp parallel workshare
     x3%v = x1%v(1) - x2%v
     !$omp end parallel workshare
  else if (get_rank(x2) == 0) then
     !$omp parallel workshare
     x3%v = x1%v - x2%v(1)
     !$omp end parallel workshare
  else if (get_size(x1) == get_size(x2)) then
     !$omp parallel workshare
     x3%v = x1%v - x2%v
     !$omp end parallel workshare
  else
     call private_bcast 
  end if
contains
  subroutine private_bcast
    real(kind=xp_), pointer :: xh(:,:), xo(:,:)
    integer :: i
    if (get_size(x1) < get_size(x2)) then
       call reshape_bcast(x2%v, get_size(x1), xh)
       call reshape_bcast(x3%v, get_size(x1), xo)
       !$omp parallel do
       do i = 1, size(xo, 2)
          xo(:,i) = x1%v - xh(:,i)
       end do
       !$omp end parallel do
    else
       call reshape_bcast(x1%v, get_size(x2), xh)
       call reshape_bcast(x3%v, get_size(x2), xo)
       !$omp parallel do
       do i = 1, size(xo, 2)
          xo(:,i) = xh(:,i) - x2%v
       end do
       !$omp end parallel do
    end if
  end subroutine private_bcast
end subroutine xp_number__op_sub__1

!> @relates fw_sub
subroutine xp_number__fw_sub__1 (x1, x2, x3)
  type(xp_number) :: x1, x2
  type(xp_number), intent(inout) :: x3
  if (has_dx(x3)) then
     call private_fw_x(x1, 1._xp_)
     call private_fw_x(x2, -1._xp_)
  end if
contains
  subroutine private_fw_x (x, sgn)
    type(xp_number), intent(in) :: x
    real(kind=xp_), intent(in) :: sgn
    if (has_dx(x)) then
       if (get_rank(x) == 0) then
          x3%dv = x3%dv + x%dv(1) * sgn
       else if (get_size(x) == get_size(x3)) then
          x3%dv = x3%dv + x%dv * sgn
       else
          call private_bcast(x, sgn)
       end if
    end if
  end subroutine private_fw_x
  subroutine private_bcast (x, sgn)
    type(xp_number), intent(in) :: x
    real(kind=xp_), intent(in) :: sgn
    real(kind=xp_), pointer :: xo(:,:)
    integer :: i
    call reshape_bcast(x3%dv, get_size(x), xo)
    do i = 1, size(xo, 2)
       xo(:,i) = xo(:,i) + x%dv * sgn
    end do
  end subroutine private_bcast
end subroutine xp_number__fw_sub__1

!> @relates bw_sub
subroutine xp_number__bw_sub__1 (x1, x2, x3)
  implicit none
  type(xp_number), intent(inout) :: x1, x2
  type(xp_number), intent(in) :: x3
  if (has_dx(x3)) then
     call private_bw_x(x1, 1._xp_)
     call private_bw_x(x2, -1._xp_)
  end if
contains
  subroutine private_bw_x (x, sgn)
    type(xp_number), intent(inout) :: x
    real(kind=xp_), intent(in) :: sgn
    if (has_dx(x)) then
       if (get_rank(x) == 0) then
          !$omp parallel workshare
          x%dv = x%dv(1) + sum(x3%dv) * sgn
          !$omp end parallel workshare
       else if (get_size(x) == get_size(x3)) then
          !$omp parallel workshare
          x%dv = x%dv + x3%dv * sgn
          !$omp end parallel workshare
       else
          call private_bcast(x, sgn)
       end if
    end if
  end subroutine private_bw_x
  subroutine private_bcast (x, sgn)
    type(xp_number), intent(in) :: x
    real(kind=xp_), intent(in) :: sgn
    real(kind=xp_), pointer :: xo(:,:)
    call reshape_bcast(x3%dv, get_size(x), xo)
    !$omp parallel workshare
    x%dv = x%dv + sum(xo, 2) * sgn
    !$omp end parallel workshare
  end subroutine private_bcast
end subroutine xp_number__bw_sub__1

!> @relates op_sub
subroutine xp_number__op_sub__2 (x1, x2, x3, indexes)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  integer, intent(in), target :: indexes(:)
  if (get_size(x1) > get_size(x2)) then
     call private_sub(x1, x2, 1._xp_)
  else
     call private_sub(x2, x1, -1._xp_)
  end if
contains
  subroutine private_sub(xx1, xx2, sgn)
    type(xp_number), intent(in) :: xx1, xx2
    real(kind=xp_), intent(in) :: sgn
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(xx2), 1:indexes(1)) => indexes(2:)
    !$omp parallel do 
    do i = 1, indexes(1)
       x3%v(j(:,i)) = (xx1%v(j(:,i)) - xx2%v) * sgn
    end do
    !$omp end parallel do
  end subroutine private_sub
end subroutine xp_number__op_sub__2

!> @relates fw_sub
subroutine xp_number__fw_sub__2 (x1, x2, x3, indexes)
  type(xp_number) :: x1, x2
  type(xp_number), intent(inout) :: x3
  integer, intent(in), target :: indexes(:)
  if (has_dx(x3)) then
     if (get_size(x1) > get_size(x2)) then
        call private_fw(x1, x2, 1._xp_)
     else
        call private_fw(x2, x1, -1._xp_)
     end if
  end if
contains
  subroutine private_fw (xx1, xx2, sgn)
    type(xp_number), intent(in) :: xx1, xx2
    real(kind=xp_), intent(in) :: sgn
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(xx2), 1:indexes(1)) => indexes(2:)
    if (has_dx(xx1)) x3%dv = x3%dv + xx1%dv * sgn
    if (has_dx(xx2)) then
       !$omp parallel do
       do i = 1, indexes(1)
          x3%dv(j(:,i)) = x3%dv(j(:,i)) - xx2%dv * sgn
       end do
       !$omp end parallel do
    end if
  end subroutine private_fw
end subroutine xp_number__fw_sub__2

!> @relates bw_sub
subroutine xp_number__bw_sub__2 (x1, x2, x3, indexes)
  implicit none
  type(xp_number), intent(inout) :: x1, x2
  type(xp_number), intent(in) :: x3
  integer, intent(in), target :: indexes(:)
  if (has_dx(x3)) then
     if (get_size(x1) > get_size(x2)) then
        call private_bw(x1, x2, 1._xp_)
     else
        call private_bw(x2, x1, -1._xp_)
     end if
  end if
contains
  subroutine private_bw (xx1, xx2, sgn)
    type(xp_number), intent(inout) :: xx1, xx2
    real(kind=xp_), intent(in) :: sgn
    integer :: i
    integer, pointer :: j(:,:)
    if (has_dx(xx1)) then
       !$omp parallel workshare
       xx1%dv = xx1%dv + x3%dv * sgn
       !$omp end parallel workshare
    end if
    if (has_dx(xx2)) then
       j(1:get_size(xx2), 1:indexes(1)) => indexes(2:)
       do i = 1, indexes(1)
          !$omp parallel workshare
          xx2%dv = xx2%dv - x3%dv(j(:,i)) * sgn
          !$omp end parallel workshare
       end do
    end if
  end subroutine private_bw
end subroutine xp_number__bw_sub__2

!> @relates op_mult
subroutine xp_number__op_mult__1 (x1, x2, x3)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  if (get_rank(x1) == 0) then
     !$omp parallel workshare
     x3%v = x1%v(1) * x2%v
     !$omp end parallel workshare
  else if (get_rank(x2) == 0) then
     !omp parallel wprkshare
     x3%v = x1%v * x2%v(1)
     !omp end parallel workshare
  else if (get_size(x1) == get_size(x2)) then
     !omp parallel workshare
     x3%v = x1%v * x2%v
     !omp end parallel workshare
  else
     call private_bcast
  end if
contains
  subroutine private_bcast
    real(kind=xp_), pointer :: xh(:,:), xl(:), xo(:,:)
    integer :: i
    if (get_size(x1) < get_size(x2)) then
       call reshape_bcast(x2%v, get_size(x1), xh)
       call reshape_bcast(x3%v, get_size(x1), xo)
       xl => x1%v
    else
       call reshape_bcast(x1%v, get_size(x2), xh)
       call reshape_bcast(x3%v, get_size(x2), xo)
       xl => x2%v
    end if
    !$omp parallel do
    do i = 1, size(xo, 2)
       xo(:,i) = xh(:,i) * xl
    end do
    !$omp end parallel do
  end subroutine private_bcast
end subroutine xp_number__op_mult__1

!> @relates fw_mult
subroutine xp_number__fw_mult__1 (x1, x2, x3)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  if (has_dx(x3)) then
     call private_fw_x(x1, x2)
     call private_fw_x(x2, x1)
  end if
contains
  subroutine private_fw_x (a, b)
    type(xp_number), intent(in) :: a, b
    if (has_dx(a)) then
       if (get_rank(a) == 0) then
          !$omp parallel workshare
          x3%dv = x3%dv + a%dv(1) * b%v
          !$omp end parallel workshare
       else if (get_rank(b) == 0) then
          !$omp parallel workshare
          x3%dv = x3%dv + a%dv * b%v(1)
          !$omp end parallel workshare
       else if (get_size(x1) == get_size(x2)) then
          !$omp parallel workshare
          x3%dv = x3%dv + a%dv * b%v
          !$omp end parallel workshare
       else
          call private_bcast(a, b)
       end if
    end if
  end subroutine private_fw_x
  subroutine private_bcast (a, b)
    type(xp_number), intent(in) :: a, b
    real(kind=xp_), pointer :: xh(:,:), xl(:), xo(:,:)
    integer :: i
    if (get_size(a) < get_size(b)) then
       call reshape_bcast(b%v, get_size(a), xh)
       call reshape_bcast(x3%dv, get_size(a), xo)
       xl => a%dv
    else
       call reshape_bcast(a%dv, get_size(b), xh)
       call reshape_bcast(x3%dv, get_size(b), xo)
       xl => b%v
    end if
    !$omp parallel do
    do i = 1, size(xo, 2)
       xo(:,i) = xo(:,i) + xh(:,i) * xl
    end do
    !$omp end parallel do
  end subroutine private_bcast
end subroutine xp_number__fw_mult__1

!> @relates bw_mult
subroutine xp_number__bw_mult__1 (x1, x2, x3)
  implicit none
  type(xp_number), intent(inout) :: x1, x2
  type(xp_number), intent(in) :: x3
  if (has_dx(x3)) then
     call private_bw_x(x1, x2)
     call private_bw_x(x2, x1)
  end if
contains
  subroutine private_bw_x (a, b)
    type(xp_number), intent(inout) :: a
    type(xp_number), intent(in) :: b
    if (has_dx(a)) then
       if (get_rank(a) == 0) then
          !$omp parallel workshare
          a%dv = a%dv(1) + sum(b%v * x3%dv)
          !$omp end parallel workshare
       else if (get_rank(b) == 0) then
          !$omp parallel workshare
          a%dv = a%dv + b%v(1) * x3%dv
          !$omp end parallel workshare
       else if (get_size(a) == get_size(b)) then
          !$omp parallel workshare
          a%dv = a%dv + b%v * x3%dv
          !$omp end parallel workshare
       else
          call private_bcast(a, b)
       end if
    end if
  end subroutine private_bw_x
  subroutine private_bcast (a, b)
    type(xp_number), intent(in) :: a, b
    real(kind=xp_), pointer :: xh(:,:), xo(:,:)
    integer :: i
    if (get_size(a) < get_size(b)) then
       call reshape_bcast(b%v, get_size(a), xh)
       call reshape_bcast(x3%dv, get_size(a), xo)
       !$omp parallel workshare
       a%dv = a%dv + sum(xh * xo, 2)
       !$omp end parallel workshare
    else
       call reshape_bcast(a%dv, get_size(b), xh)
       call reshape_bcast(x3%dv, get_size(b), xo)
       do i = 1, size(xh, 2)
          !$omp parallel workshare
          xh(:,i) = xh(:,i) + xo(:,i) * b%v
          !$omp end parallel workshare
       end do
    end if
  end subroutine private_bcast
end subroutine xp_number__bw_mult__1

!> @relates op_mult
subroutine xp_number__op_mult__2 (x1, x2, x3, indexes)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  integer, intent(in), target :: indexes(:)
  if (get_size(x1) > get_size(x2)) then
     call private_mult(x1, x2)
  else
     call private_mult(x2, x1)
  end if
contains
  subroutine private_mult (xx1, xx2)
    type(xp_number), intent(in) :: xx1, xx2
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(xx2), 1:indexes(1)) => indexes(2:)
    !$omp parallel do 
    do i = 1, indexes(1)
       x3%v(j(:,i)) = xx1%v(j(:,i)) * xx2%v
    end do
    !$omp end parallel do
  end subroutine private_mult
end subroutine xp_number__op_mult__2

!> @relates fw_mult
subroutine xp_number__fw_mult__2 (x1, x2, x3, indexes)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  integer, intent(in), target :: indexes(:)
  if (has_dx(x3)) then
     if (get_size(x1) > get_size(x2)) then
        call private_fw(x1, x2)
     else
        call private_fw(x2, x1)
     end if
  end if
contains
  subroutine private_fw (xx1, xx2)
    type(xp_number), intent(in) :: xx1, xx2
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(xx2), 1:indexes(1)) => indexes(2:)
    if (has_dx(xx1)) then
       !$omp parallel do
       do i = 1, indexes(1)
          x3%dv(j(:,i)) = x3%dv(j(:,i)) + xx1%dv(j(:,i)) * xx2%v
       end do
       !$omp end parallel do
    end if
    if (has_dx(xx2)) then
       !$omp parallel do
       do i = 1, indexes(1)
          x3%dv(j(:,i)) = x3%dv(j(:,i)) + xx2%dv * xx1%v(j(:,i))
       end do
       !$omp end parallel do
    end if
  end subroutine private_fw
end subroutine xp_number__fw_mult__2

!> @relates bw_mult
subroutine xp_number__bw_mult__2 (x1, x2, x3, indexes)
  implicit none
  type(xp_number), intent(inout) :: x1, x2
  type(xp_number), intent(in) :: x3
  integer, intent(in), target :: indexes(:)
  if (has_dx(x3)) then
     if (get_size(x1) > get_size(x2)) then
        call private_bw(x1, x2)
     else
        call private_bw(x2, x1)
     end if
  end if
contains
  subroutine private_bw (xx1, xx2)
    type(xp_number), intent(inout) :: xx1, xx2
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(xx2), 1:indexes(1)) => indexes(2:)
    if (has_dx(xx1)) then
       do i = 1, indexes(1)
          !$omp parallel workshare
          xx1%dv(j(:,i)) = xx1%dv(j(:,i)) + xx2%v * x3%dv(j(:,i))
          !$omp end parallel workshare
       end do
    end if
    if (has_dx(xx2)) then
       do i = 1, indexes(1)
          !$omp parallel workshare
          xx2%dv = xx2%dv + xx1%v(j(:,i)) * x3%dv(j(:,i))
          !$omp end parallel workshare
       end do
    end if
  end subroutine private_bw
end subroutine xp_number__bw_mult__2

!> @relates op_pow
subroutine xp_number__op_pow__1 (x1, x2, x3)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  if (get_rank(x1) == 0) then
     !$omp parallel workshare
     x3%v = x1%v(1) ** x2%v
     !$omp end parallel workshare
  else if (get_rank(x2) == 0) then
     !$omp parallel workshare
     x3%v = x1%v ** x2%v(1)
     !$omp end parallel workshare
  else if (get_size(x1) == get_size(x2)) then
     !$omp parallel workshare
     x3%v = x1%v ** x2%v
     !$omp end parallel workshare
  else
     call private_bcast
  end if
contains
  subroutine private_bcast
    real(kind=xp_), pointer :: xh(:,:), xo(:,:)
    integer :: i
    if (get_size(x1) < get_size(x2)) then
       call reshape_bcast(x2%v, get_size(x1), xh)
       call reshape_bcast(x3%v, get_size(x1), xo)
       !$omp parallel do
       do i = 1, size(xo, 2)
          xo(:,i) = x1%v ** xh(:,i)
       end do
       !$omp end parallel do
    else
       call reshape_bcast(x1%v, get_size(x2), xh)
       call reshape_bcast(x3%v, get_size(x2), xo)
       !$omp parallel do
       do i = 1, size(xo, 2)
          xo(:,i) = xh(:,i) ** x2%v
       end do
       !$omp end parallel do
    end if
  end subroutine private_bcast
end subroutine xp_number__op_pow__1

!> @relates fw_pow
subroutine xp_number__fw_pow__1 (x1, x2, x3)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  if (has_dx(x3)) then
     call private_fw_x1
     call private_fw_x2
  end if
contains
  subroutine private_fw_x1
    if (has_dx(x1)) then
       if (get_rank(x1) == 0) then
          x3%dv = x3%dv + x1%dv(1) * dx_xpow(x1%v(1), x2%v)
       else if (get_rank(x2) == 0) then
          x3%dv = x3%dv + x1%dv * dx_xpow(x1%v, x2%v(1))
       else if (get_size(x1) == get_size(x2)) then
          x3%dv = x3%dv + x1%dv * dx_xpow(x1%v, x2%v)
       else
          call private_bcast_x1
       end if
    end if
  end subroutine private_fw_x1
  subroutine private_bcast_x1
    real(kind=xp_), pointer :: xh1(:,:), xh2(:,:), xo(:,:)
    integer :: i
    if (get_size(x1) < get_size(x2)) then
       call reshape_bcast(x2%v, get_size(x1), xh1)
       call reshape_bcast(x3%dv, get_size(x1), xo)
       do i = 1, size(xo, 2)
          xo(:,i) = xo(:,i) + x1%dv * dx_xpow(x1%v, xh1(:,i))
       end do
    else
       call reshape_bcast(x1%v, get_size(x2), xh1)
       call reshape_bcast(x1%dv, get_size(x2), xh2)
       call reshape_bcast(x3%dv, get_size(x2), xo)
       do i = 1, size(xo, 2)
          xo(:,i) = xo(:,i) + xh2(:,i) * dx_xpow(xh1(:,i), x2%dv)
       end do
    end if
  end subroutine private_bcast_x1
  subroutine private_fw_x2
    if (has_dx(x2)) then
       if (get_rank(x1) == 0) then
          x3%dv = x3%dv + x2%dv * dx_powx(x1%v(1), x2%v)
       else if (get_rank(x2) == 0) then
          x3%dv = x3%dv + x2%dv(1) * dx_powx(x1%v, x2%v(1))
       else if (get_size(x1) == get_size(x2)) then
          x3%dv = x3%dv + x2%dv * dx_powx(x1%v, x2%v)
       else
          call private_bcast_x2
       end if
    end if
  end subroutine private_fw_x2
  subroutine private_bcast_x2
    real(kind=xp_), pointer :: xh1(:,:), xh2(:,:), xo(:,:)
    integer :: i
    if (get_size(x1) < get_size(x2)) then
       call reshape_bcast(x2%v, get_size(x1), xh1)
       call reshape_bcast(x2%dv, get_size(x1), xh2)
       call reshape_bcast(x3%dv, get_size(x1), xo)
       do i = 1, size(xo, 2)
          xo(:,i) = xo(:,i) + xh2(:,i) * dx_xpow(x1%v, xh1(:,i))
       end do
    else
       call reshape_bcast(x1%v, get_size(x2), xh1)
       call reshape_bcast(x3%dv, get_size(x2), xo)
       do i = 1, size(xo, 2)
          xo(:,i) = xo(:,i) + x2%dv * dx_xpow(xh1(:,i), x2%dv)
       end do
    end if
  end subroutine private_bcast_x2
end subroutine xp_number__fw_pow__1

!> @relates bw_pow
subroutine xp_number__bw_pow__1 (x1, x2, x3)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  if (has_dx(x3)) then
     call private_bw_x1
     call private_bw_x2
  end if
contains
  subroutine private_bw_x1
    if (has_dx(x1)) then
       if (get_rank(x1) == 0) then
          !$omp parallel workshare
          x1%dv = x1%dv(1) + sum(x3%dv * dx_xpow(x1%v(1), x2%v))
          !$omp end parallel workshare
       else if (get_rank(x2) == 0) then
          !$omp parallel workshare
          x1%dv = x1%dv + x3%dv * dx_xpow(x1%v, x2%v(1))
          !$omp end parallel workshare
       else if (get_size(x1) == get_size(x2)) then
          !$omp parallel workshare
          x1%dv = x1%dv + x3%dv * dx_xpow(x1%v, x2%v)
          !$omp end parallel workshare
       else
          call private_bcast_x1
       end if
    end if
  end subroutine private_bw_x1
  subroutine private_bcast_x1
    real(kind=xp_), pointer :: xh1(:,:), xh2(:,:), xo(:,:)
    integer :: i
    if (get_size(x1) < get_size(x2)) then
       call reshape_bcast(x2%v, get_size(x1), xh1)
       call reshape_bcast(x3%dv, get_size(x1), xo)
       do i = 1, size(xo, 2)
          !$omp parallel workshare
          x1%dv = x1%dv + xo(:,i) * dx_xpow(x1%v, xh1(:,i))
          !$omp end parallel workshare
       end do
    else
       call reshape_bcast(x1%v, get_size(x2), xh1)
       call reshape_bcast(x1%dv, get_size(x2), xh2)
       call reshape_bcast(x3%dv, get_size(x2), xo)
       do i = 1, size(xo, 2)
          !$omp parallel workshare
          xh2(:,i) = xh2(:,i) + xo(:,i) * dx_xpow(xh1(:,i), x2%v)
          !$omp end parallel workshare
       end do
    end if
  end subroutine private_bcast_x1
  subroutine private_bw_x2
    if (has_dx(x2)) then
       if (get_rank(x1) == 0) then
          !$omp parallel workshare
          x2%dv = x2%dv + x3%dv * dx_powx(x1%v(1), x2%v)
          !$omp end parallel workshare
       else if (get_rank(x2) == 0) then
          !$omp parallel workshare
          x2%dv = x2%dv(1) + sum(x3%dv * dx_powx(x1%v, x2%v(1)))
          !$omp end parallel workshare
       else if (get_size(x1) == get_size(x2)) then
          !$omp parallel workshare
          x2%dv = x2%dv + x3%dv * dx_powx(x1%v, x2%v)
          !$omp end parallel workshare
       else
          call private_bcast_x2
       end if
    end if
  end subroutine private_bw_x2
  subroutine private_bcast_x2
    real(kind=xp_), pointer :: xh1(:,:), xh2(:,:), xo(:,:)
    integer :: i
    if (get_size(x1) < get_size(x2)) then
       call reshape_bcast(x2%v, get_size(x1), xh1)
       call reshape_bcast(x2%dv, get_size(x1), xh2)
       call reshape_bcast(x3%dv, get_size(x1), xo)
       do i = 1, size(xo, 2)
          !$omp parallel workshare
          xh2(:,i) = xh2(:,i) + xo(:,i) * dx_powx(x1%v, xh1(:,i))
          !$omp end parallel workshare
       end do
    else
       call reshape_bcast(x1%v, get_size(x2), xh1)
       call reshape_bcast(x3%dv, get_size(x2), xo)
       do i = 1, size(xo, 2)
          !$omp parallel workshare
          x2%dv = x2%dv + xo(:,i) * dx_powx(xh1(:,i), x2%v)
          !$omp end parallel workshare
       end do
    end if
  end subroutine private_bcast_x2
end subroutine xp_number__bw_pow__1

!> @relates op_pow
subroutine xp_number__op_pow__2 (x1, x2, x3, indexes)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  integer, intent(in), target :: indexes(:)
  if (get_size(x1) > get_size(x2)) then
     call private_pow_left
  else
     call private_pow_right
  end if
contains
  subroutine private_pow_left
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(x2), 1:indexes(1)) => indexes(2:)
    !$omp parallel do
    do i = 1, indexes(1)
       x3%v(j(:,i)) = x1%v(j(:,i)) ** x2%v
    end do
    !$omp end parallel do
  end subroutine private_pow_left
  subroutine private_pow_right
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(x1), 1:indexes(1)) => indexes(2:)
    !$omp parallel do
    do i = 1, indexes(1)
       x3%v(j(:,i)) = x1%v ** x2%v(j(:,i))
    end do
    !$omp end parallel do 
  end subroutine private_pow_right
end subroutine xp_number__op_pow__2

!> @relates fw_pow
subroutine xp_number__fw_pow__2 (x1, x2, x3, indexes)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  integer, intent(in), target :: indexes(:)
  if (has_dx(x3)) then
     if (get_size(x1) > get_size(x2)) then
        call private_fw_left
     else
        call private_fw_right
     end if
  end if
contains
  subroutine private_fw_left
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(x2), 1:indexes(1)) => indexes(2:)
    if (has_dx(x1)) then
       !$omp parallel do
       do i = 1, indexes(1)
          x3%v(j(:,i)) = x3%dv(j(:,i)) + x1%dv(j(:,i)) * dx_xpow(x1%v(j(:,i)), x2%v)
       end do
       !$omp end parallel do
    end if
    if (has_dx(x2)) then
       !$omp parallel do
       do i = 1, indexes(1)
          x3%v(j(:,i)) = x3%dv(j(:,i)) + x2%dv * dx_powx(x1%v(j(:,i)), x2%v)
       end do
       !$omp end parallel do
    end if
  end subroutine private_fw_left
  subroutine private_fw_right
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(x1), 1:indexes(1)) => indexes(2:)
    if (has_dx(x1)) then
       !$omp parallel do
       do i = 1, indexes(1)
          x3%dv(j(:,i)) = x3%dv(j(:,i)) + x1%dv * dx_xpow(x1%v, x2%v(j(:,i)))
       end do
       !$omp end parallel do
    end if
    if (has_dx(x2)) then
       !$omp parallel do
       do i = 1, indexes(1)
          x3%dv(j(:,i)) = x3%dv + x2%dv * dx_powx(x1%v, x2%v(j(:,i)))
       end do
       !$omp end parallel do
    end if
  end subroutine private_fw_right
end subroutine xp_number__fw_pow__2

!> @relates bw_pow
subroutine xp_number__bw_pow__2 (x1, x2, x3, indexes)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  integer, intent(in), target :: indexes(:)
  if (has_dx(x3)) then
     if (get_size(x1) > get_size(x2)) then
        call private_bw_left
     else
        call private_bw_right
     end if
  end if
contains
  subroutine private_bw_left
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(x2), 1:indexes(1)) => indexes(2:)
    if (has_dx(x1)) then
       do i = 1, indexes(1)
          !$omp parallel workshare
          x1%dv(j(:,i)) = x1%dv(j(:,i)) + x3%dv(j(:,i)) * dx_xpow(x1%v(j(:,i)), x2%v)
          !$omp end parallel workshare
       end do
    end if
    if (has_dx(x2)) then
       do i = 1, indexes(1)
          !$omp parallel workshare
          x2%dv = x2%dv + x3%dv(j(:,i)) * dx_powx(x1%v(j(:,i)), x2%v)
          !$omp end parallel workshare
       end do 
    end if
  end subroutine private_bw_left
    subroutine private_bw_right
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(x1), 1:indexes(1)) => indexes(2:)
    if (has_dx(x1)) then
       do i = 1, indexes(1)
          !$omp parallel workshare
          x1%dv = x1%dv + x3%dv(j(:,i)) * dx_xpow(x1%v, x2%v(j(:,i)))
          !$omp end parallel workshare
       end do
    end if
    if (has_dx(x2)) then
       do i = 1, indexes(1)
          !$omp parallel workshare
          x2%dv(j(:,i)) = x2%dv(j(:,i)) + x3%dv(j(:,i)) * dx_powx(x1%v, x2%v(j(:,i)))
          !$omp end parallel workshare
       end do
    end if
  end subroutine private_bw_right
end subroutine xp_number__bw_pow__2

!> @relates op_div
subroutine xp_number__op_div__1 (x1, x2, x3)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  if (get_rank(x1) == 0) then
     !$omp parallel workshare
     x3%v = x1%v(1) / x2%v
     !$omp end parallel workshare
  else if (get_rank(x2) == 0) then
     !$omp parallel workshare
     x3%v = x1%v / x2%v(1)
     !$omp end parallel workshare
  else if (get_size(x1) == get_size(x2)) then
     !$omp parallel workshare
     x3%v = x1%v / x2%v
     !$omp end parallel workshare
  else
     call private_bcast
  end if
contains
  subroutine private_bcast
    real(kind=xp_), pointer :: xh(:,:), xo(:,:)
    integer :: i
    if (get_size(x1) < get_size(x2)) then
       call reshape_bcast(x2%v, get_size(x1), xh)
       call reshape_bcast(x3%v, get_size(x1), xo)
       !$omp parallel do
       do i = 1, size(xo, 2)
          xo(:,i) = x1%v / xh(:,i) 
       end do
       !$omp end parallel do
    else
       call reshape_bcast(x1%v, get_size(x2), xh)
       call reshape_bcast(x3%v, get_size(x2), xo)
       !$omp parallel do
       do i = 1, size(xo, 2)
          xo(:,i) = xh(:,i) / x2%v 
       end do
       !$omp end parallel do
    end if
  end subroutine private_bcast
end subroutine xp_number__op_div__1

!> @relates fw_div
subroutine xp_number__fw_div__1 (x1, x2, x3)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  if (has_dx(x3)) then
     call private_fw_x1
     call private_fw_x2
  end if
contains
  subroutine private_fw_x1
    if (has_dx(x1)) then
       if (get_rank(x1) == 0) then
          x3%dv = x3%dv + x1%dv(1) * x2%v ** (-1._xp_)
       else if (get_rank(x2) == 0) then
          x3%dv = x3%dv + x1%dv * x2%v(1) ** (-1._xp_)
       else if (get_size(x1) == get_size(x2)) then
          x3%dv = x3%dv + x1%dv * x2%v ** (-1._xp_)
       else
          call private_bcast_x1
       end if
    end if
  end subroutine private_fw_x1
  subroutine private_bcast_x1
    real(kind=xp_), pointer :: xh(:,:), xo(:,:)
    integer :: i
    if (get_size(x1) < get_size(x2)) then
       call reshape_bcast(x2%v, get_size(x1), xh)
       call reshape_bcast(x3%dv, get_size(x1), xo)
       do i = 1, size(xo, 2)
          xo(:,i) = xo(:,i) + x1%dv * xh(:,i) ** (-1._xp_)
       end do
    else
       call reshape_bcast(x1%dv, get_size(x2), xh)
       call reshape_bcast(x3%dv, get_size(x2), xo)
       do i = 1, size(xo, 2)
          xo(:,i) = xo(:,i) + xh(:,i) * x2%v ** (-1._xp_)
       end do
    end if
  end subroutine private_bcast_x1
  subroutine private_fw_x2
    if (has_dx(x2)) then
       if (get_rank(x1) == 0) then
          x3%dv = x3%dv + x2%dv * x1%v(1) * dx_xpow(x2%v, -1._xp_)
       else if (get_rank(x2) == 0) then
          x3%dv = x3%dv + x2%dv(1) * x1%v * dx_xpow(x2%v(1), -1._xp_)
       else if (get_size(x1) == get_size(x2)) then
          x3%dv = x3%dv + x2%dv * x1%v * dx_xpow(x2%v, -1._xp_)
       else
          call private_bcast_x2
       end if
    end if
  end subroutine private_fw_x2
  subroutine private_bcast_x2
    real(kind=xp_), pointer :: xh1(:,:), xh2(:,:), xo(:,:)
    integer :: i
    if (get_size(x1) < get_size(x2)) then
       call reshape_bcast(x2%v, get_size(x1), xh1)
       call reshape_bcast(x2%dv, get_size(x1), xh2)
       call reshape_bcast(x3%dv, get_size(x1), xo)
       do i = 1, size(xo, 2)
          xo(:,i) = xo(:,i) + xh2(:,i) + x1%v * dx_xpow(xh1(:,i), -1._xp_)
       end do
    else
       call reshape_bcast(x1%dv, get_size(x2), xh1)
       call reshape_bcast(x3%dv, get_size(x2), xo)
       do i = 1, size(xo, 2)
          xo(:,i) = xo(:,i) + x2%dv * xh1(:,i) * dx_xpow(x2%v, -1._xp_)
       end do
    end if
  end subroutine private_bcast_x2
end subroutine xp_number__fw_div__1

!> @relates bw_div
subroutine xp_number__bw_div__1 (x1, x2, x3)
  implicit none
  type(xp_number), intent(inout) :: x1, x2
  type(xp_number), intent(in) :: x3
  if (has_dx(x3)) then
     call private_bw_x1
     call private_bw_x2
  end if
contains
  subroutine private_bw_x1
    if (has_dx(x1)) then
       if (get_rank(x1) == 0) then
          !$omp parallel workshare
          x1%dv = x1%dv(1) + sum(x2%v ** (-1._xp_) * x3%dv)
          !$omp end parallel workshare
       else if (get_rank(x2) == 0) then
          !$omp parallel workshare
          x1%dv = x1%dv + x2%v(1) ** (-1._xp_) * x3%dv
          !$omp end parallel workshare
       else if (get_size(x1) == get_size(x2)) then
          !$omp parallel workshare
          x1%dv = x1%dv + x2%v ** (-1._xp_) * x3%dv
          !$omp end parallel workshare
       else
          call private_bcast_x1
       end if
    end if
  end subroutine private_bw_x1
  subroutine private_bcast_x1
    real(kind=xp_), pointer :: xh(:,:), xo(:,:)
    integer :: i
    if (get_size(x1) < get_size(x2)) then
       call reshape_bcast(x2%v, get_size(x1), xh)
       call reshape_bcast(x3%dv, get_size(x1), xo)
       !$omp parallel workshare
       x1%dv = x1%dv + sum(xh ** (-1._xp_) * xo, 2)
       !$omp end parallel workshare
    else
       call reshape_bcast(x1%dv, get_size(x2), xh)
       call reshape_bcast(x3%dv, get_size(x2), xo)
       !$omp parallel do
       do i = 1, size(xo, 2)
          xh(:,i) = xh(:,i) + x2%v ** (-1._xp_) * xo(:,i)
       end do
       !$omp end parallel do
    end if
  end subroutine private_bcast_x1
  subroutine private_bw_x2
    if (has_dx(x2)) then
       if (get_rank(x1) == 0) then
          !$omp parallel workshare
          x2%dv = x2%dv + x1%v(1) * dx_xpow(x2%v, -1._xp_) * x3%dv
          !$omp end parallel workshare
       else if (get_rank(x2) == 0) then
          !$omp parallel workshare
          x2%dv(1) = x2%dv(1) + sum(x1%v * dx_xpow(x2%v(1), -1._xp_) * x3%dv)
          !$omp end parallel workshare
       else if (get_size(x1) == get_size(x2)) then
          !$omp parallel workshare
          x2%dv = x2%dv + x1%v * dx_xpow(x2%v, -1._xp_) * x3%dv
          !$omp end parallel workshare
       else
          call private_bcast_x2
       end if
    end if
  end subroutine private_bw_x2
  subroutine private_bcast_x2
    real(kind=xp_), pointer :: xh1(:,:), xh2(:,:), xo(:,:)
    integer :: i
    if (get_size(x1) < get_size(x2)) then
       call reshape_bcast(x2%v, get_size(x1), xh1)
       call reshape_bcast(x2%dv, get_size(x1), xh2)
       call reshape_bcast(x3%dv, get_size(x1), xo)
       !$omp parallel do
       do i = 1, size(xo, 2)
          xh2(:,i) = xh2(:,i) + x1%v * dx_xpow(xh1(:,i), -1._xp_) * xo(:,i)
       end do
       !$omp end parallel do
    else
       call reshape_bcast(x1%v, get_size(x2), xh1)
       call reshape_bcast(x3%dv, get_size(x2), xo)
       !$omp parallel workshare
       x2%dv = x2%dv + dx_xpow(x2%v, -1._xp_) * sum(xh1  * xo, 2)
       !$omp end parallel workshare
    end if
  end subroutine private_bcast_x2
end subroutine xp_number__bw_div__1

!> @relates op_div
subroutine xp_number__op_div__2 (x1, x2, x3, indexes)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  integer, intent(in), target :: indexes(:)
  if (get_size(x1) > get_size(x2)) then
     call private_div_left
  else
     call private_div_right
  end if
contains
  subroutine private_div_left
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(x2), 1:indexes(1)) => indexes(2:)
    !$omp parallel do
    do i = 1, indexes(1)
       x3%v(j(:,i)) = x1%v(j(:,i)) / x2%v
    end do
    !omp end parallel do
  end subroutine private_div_left
  subroutine private_div_right
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(x1), 1:indexes(1)) => indexes(2:)
    !$omp parallel do
    do i = 1, indexes(1)
       x3%v(j(:,i)) = x1%v / x2%v(j(:,i))
    end do
    !$omp end parallel do
  end subroutine private_div_right
end subroutine xp_number__op_div__2

!> @relates fw_div
subroutine xp_number__fw_div__2 (x1, x2, x3, indexes)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  integer, intent(in), target :: indexes(:)
  if (has_dx(x3)) then
     if (get_size(x1) > get_size(x2)) then
        call private_fw_left
     else
        call private_fw_right
     end if
  end if
contains
  subroutine private_fw_left
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(x2), 1:indexes(1)) => indexes(2:)
    if (has_dx(x1)) then
       do i = 1, indexes(1)
          x3%dv(j(:,i)) = x3%dv(j(:,i)) + x1%dv(j(:,i)) * x2%v ** (-1._xp_)
       end do
    end if
    if (has_dx(x2)) then
       do i = 1, indexes(1)
          x3%dv(j(:,i)) = x3%dv(j(:,i)) + x2%dv * x1%v(j(:,i)) ** (-1._xp_)
       end do
    end if
  end subroutine private_fw_left
  subroutine private_fw_right
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(x1), 1:indexes(1)) => indexes(2:)
    if (has_dx(x1)) then
       do i = 1, indexes(1)
          x3%dv(j(:,i)) = x3%dv(j(:,i)) + x1%dv * x2%v(j(:,i)) ** (-1._xp_)
       end do
    end if
    if (has_dx(x2)) then
       do i = 1, indexes(1)
          x3%dv(j(:,i)) = x3%dv(j(:,i)) + x2%dv(j(:,i)) * x1%v ** (-1._xp_)
       end do
    end if
  end subroutine private_fw_right
end subroutine xp_number__fw_div__2

!> @relates bw_div
subroutine xp_number__bw_div__2 (x1, x2, x3, indexes)
  implicit none
  type(xp_number), intent(inout) :: x1, x2
  type(xp_number), intent(in) :: x3
  integer, intent(in), target :: indexes(:)
  if (has_dx(x3)) then
     if (get_size(x1) > get_size(x2)) then
        call private_bw_left
     else
        call private_bw_right
     end if
  end if
contains
  subroutine private_bw_left
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(x2), 1:indexes(1)) => indexes(2:)
    if (has_dx(x1)) then
       do i = 1, indexes(1)
          !$omp parallel workshare
          x1%dv(j(:,i)) = x1%dv(j(:,i)) + x2%v ** (-1._xp_) * x3%dv(j(:,i))
          !$omp end parallel workshare
       end do
    end if
    if (has_dx(x2)) then
       do i = 1, indexes(1)
          !$omp parallel workshare
          x2%dv = x2%dv + x1%v(j(:,i)) * dx_xpow(x2%v, -1._xp_) * x3%dv(j(:,i))
          !$omp end parallel workshare
       end do
    end if
  end subroutine private_bw_left
  subroutine private_bw_right
    integer :: i
    integer, pointer :: j(:,:)
    j(1:get_size(x1), 1:indexes(1)) => indexes(2:)
    if (has_dx(x1)) then
       do i = 1, indexes(1)
          x1%dv= x1%dv + x2%v(j(:,i)) ** (-1._xp_) * x3%dv(j(:,i))
       end do
    end if
    if (has_dx(x2)) then
       do i = 1, indexes(1)
          x2%dv(j(:,i)) = x2%dv(j(:,i)) + x1%v * dx_xpow(x2%v(j(:,i)), -1._xp_) * x3%dv(j(:,i))
       end do
    end if
  end subroutine private_bw_right
end subroutine xp_number__bw_div__2
