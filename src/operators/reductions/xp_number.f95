!> @relates op_sum
subroutine xp_number__op_sum__1 (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = sum(x1%v)
  !$omp end parallel workshare
end subroutine xp_number__op_sum__1

!> @relates fw_sum
subroutine xp_number__fw_sum__1 (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x2%dv = sum(x1%dv)
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_sum__1

!> @relates bw_sum
subroutine xp_number__bw_sum__1 (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x1%dv = x1%dv + x2%dv(1)
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_sum__1

!> @relates op_sum
subroutine xp_number__op_sum__2 (x1, x2, indexes)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  integer, intent(in), target :: indexes(:)
  integer, pointer :: j(:,:)
  integer :: i
  j(1:(get_size(x1) / indexes(1)), 1:indexes(1)) => indexes(2:)
  x2%v = 0
  do i = 1, indexes(1)
     !$omp parallel workshare
     x2%v = x2%v + x1%v(j(:,i))
     !$omp end parallel workshare
  end do
end subroutine xp_number__op_sum__2

!> @relates fw_sum
subroutine xp_number__fw_sum__2 (x1, x2, indexes)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  integer, intent(in), target :: indexes(:)
  integer, pointer :: j(:,:)
  integer :: i
  j(1:(get_size(x1) / indexes(1)), 1:indexes(1)) => indexes(2:)
  do i = 1, indexes(1)
     !!$omp parallel workshare
     x2%dv = x2%dv + x1%dv(j(:,i))
     !!$omp end parallel workshare
  end do
end subroutine xp_number__fw_sum__2

!> @relates bw_sum
subroutine xp_number__bw_sum__2 (x1, x2, indexes)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(in) :: x2
  integer, intent(in), target :: indexes(:)
  integer, pointer :: j(:,:)
  integer :: i
  j(1:(get_size(x1) / indexes(1)), 1:indexes(1)) => indexes(2:)
  do i = 1, indexes(1)
     !!$omp parallel workshare
     x1%dv(j(:,i)) = x1%dv(j(:,i)) + x2%dv
     !!$omp end parallel workshare
  end do
end subroutine xp_number__bw_sum__2

!> @relates op_product
subroutine xp_number__op_product__1 (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = product(x1%v)
  !$omp end parallel workshare
end subroutine xp_number__op_product__1

!> @relates fw_product
subroutine xp_number__fw_product__1 (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  if (has_dx(x2)) x2%dv = sum(dx_product(x1%v) * x1%dv)
end subroutine xp_number__fw_product__1

!> @relates bw_product
subroutine xp_number__bw_product__1 (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x1%dv = x1%dv + x2%v(1) / x1%v * x2%dv(1)
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_product__1

!> @relates op_product
subroutine xp_number__op_product__2 (x1, x2, indexes)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  integer, intent(in), target :: indexes(:)
  integer, pointer :: j(:,:)
  integer :: i
  j(1:(get_size(x1) / indexes(1)), 1:indexes(1)) => indexes(2:)
  x2%v = 1
  do i = 1, indexes(1)
     !!$omp parallel workshare
     x2%v = x2%v * x1%v(j(:,i))
     !!$omp end parallel workshare
  end do
end subroutine xp_number__op_product__2

!> @relates fw_product
subroutine xp_number__fw_product__2 (x1, x2, k)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  integer, intent(in) :: k(:)
  call raise_error("fw_product__2 is only a placehlder", err_generic_)
end subroutine xp_number__fw_product__2

!> @relates bw_product
subroutine xp_number__bw_product__2 (x1, x2, indexes)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(in) :: x2
  integer, intent(in), target :: indexes(:)
  integer, pointer :: j(:,:)
  integer :: i
  j(1:(get_size(x1) / indexes(1)), 1:indexes(1)) => indexes(2:)
  do i = 1, indexes(1)
     !!$omp parallel workshare
     x1%dv(j(:,i)) = x1%dv(j(:,i)) + x2%v / x1%v(j(:,i)) * x2%dv
     !!$omp end parallel workshare
  end do
end subroutine xp_number__bw_product__2

!> @relates op_ssq
subroutine xp_number__op_ssq__1 (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = sum(x1%v**2)
  !$omp end parallel workshare
end subroutine xp_number__op_ssq__1

!> @relates fw_ssq
subroutine xp_number__fw_ssq__1 (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  if (has_dx(x2)) x2%dv = sum(x1%dv * dx_ssq(x1%v))
end subroutine xp_number__fw_ssq__1

!> @relates bw_ssq
subroutine xp_number__bw_ssq__1 (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x1%dv = x1%dv + dx_ssq(x1%v) * x2%dv(1)
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_ssq__1

!> @relates op_ssq
subroutine xp_number__op_ssq__2 (x1, x2, indexes)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  integer, intent(in), target :: indexes(:)
  integer, pointer :: j(:,:)
  integer :: i
  j(1:(get_size(x1) / indexes(1)), 1:indexes(1)) => indexes(2:)
  x2%v = 0
  do i = 1, indexes(1)
     !!$omp parallel workshare
     x2%v = x2%v + x1%v(j(:,i))**2
     !!$omp end parallel workshare
  end do
end subroutine xp_number__op_ssq__2

!> @relates fw_ssq
subroutine xp_number__fw_ssq__2 (x1, x2, k)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  integer :: i, k(:)
  call raise_error("fw_ssq__2 is only a placehlder", err_generic_)
end subroutine xp_number__fw_ssq__2

!> @relates bw_ssq
subroutine xp_number__bw_ssq__2 (x1, x2, indexes)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(in) :: x2
  integer, intent(in), target :: indexes(:)
  integer, pointer :: j(:,:)
  integer :: i
  j(1:(get_size(x1) / indexes(1)), 1:indexes(1)) => indexes(2:)
  do i = 1, indexes(1)
     !!$omp parallel workshare
     x1%dv(j(:,i)) = x1%dv(j(:,i)) + dx_ssq(x1%v(j(:,i))) * x2%dv
     !!$omp end parallel workshare
  end do
end subroutine xp_number__bw_ssq__2

