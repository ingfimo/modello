  !> @addtogroup operators__reductions_interfaces_
  !! @{

  !> Sum - operator
  !! @param[in] x1 'number' of rank > 0
  !! @param[inout] x2 'number' of rank 0
  interface op_sum
     module procedure sp_number__op_sum__1
     module procedure sp_number__op_sum__2
     module procedure dp_number__op_sum__1
     module procedure dp_number__op_sum__2
  end interface op_sum

  !> Sum - forward differentiation
  !! @param[in] x1 'number' of rank > 0
  !! @param[inout] x2 'number' of rank 0
  interface fw_sum
     module procedure sp_number__fw_sum__1
     module procedure sp_number__fw_sum__2
     module procedure dp_number__fw_sum__1
     module procedure dp_number__fw_sum__2
  end interface fw_sum

  !> Sum - backward differentiation
  !! @param[inout] x1 'number' of rank > 0
  !! @param[in] x2 'number' of rank 0
  interface bw_sum
     module procedure sp_number__bw_sum__1
     module procedure sp_number__bw_sum__2
     module procedure dp_number__bw_sum__1
     module procedure dp_number__bw_sum__2
  end interface bw_sum

  !> Product - Operator
  !! @param[in] x1 'number' of rank > 0
  !! @param[inout] x2 'number' of rank 0
  interface op_product
     module procedure sp_number__op_product__1
     module procedure sp_number__op_product__2
     module procedure dp_number__op_product__1
     module procedure dp_number__op_product__2
  end interface op_product

  !> Product - forward differentiation
  !! @param[in] x1 'number' of rank > 0
  !! @param[inout] x2 'number' of rank 0
  interface fw_product
     module procedure sp_number__fw_product__1
     module procedure sp_number__fw_product__2
     module procedure dp_number__fw_product__1
     module procedure dp_number__fw_product__2
  end interface fw_product

  !> Product - backward differentiation
  !! @param[inout] x1 'number' of rank > 0
  !! @param[in] x2 'number' of rank 0
  interface bw_product
     module procedure sp_number__bw_product__1
     module procedure sp_number__bw_product__2
     module procedure dp_number__bw_product__1
     module procedure dp_number__bw_product__2
  end interface bw_product

  !> Sum of Squares - operator
  !! @param[in] x1 'number' of rank > 0
  !! @param[inout] x2 'number' of rank 0
  interface op_ssq
     module procedure sp_number__op_ssq__1
     module procedure sp_number__op_ssq__2
     module procedure dp_number__op_ssq__1
     module procedure dp_number__op_ssq__2
  end interface op_ssq

  !> Sum of Squares - forward differentiation
  !! @param[in] x1 'number' of rank > 0
  !! @param[inout] x2 'number' of rank 0
  interface fw_ssq
     module procedure sp_number__fw_ssq__1
     module procedure sp_number__fw_ssq__2
     module procedure dp_number__fw_ssq__1
     module procedure dp_number__fw_ssq__2
  end interface fw_ssq

  !> Sum of Squares - backward differentiation
  !! @param[inout] x1 'number' of rank > 0
  !! @param[in] x2 'number' of rank 0
  interface bw_ssq
     module procedure sp_number__bw_ssq__1
     module procedure sp_number__bw_ssq__2
     module procedure dp_number__bw_ssq__1
     module procedure dp_number__bw_ssq__2
  end interface bw_ssq

  !> @}
