  !> @addtogroup operators__stats_interfaces_
  !! @{

  !> ldexp - operator
  !! @param[in] y 'number', obeservations
  !! @param[in] lam 'number', rate parameter
  !! @param[inout] d 'number', log-density 
  interface op_ldexp
     module procedure sp_number__op_ldexp
     module procedure dp_number__op_ldexp
  end interface op_ldexp

  !> ldexp - forward differentiation
  !! @param[in] y 'number', obeservations
  !! @param[in] lam 'number', rate parameter
  !! @param[inout] d 'number', log-density
  interface fw_ldexp
     module procedure sp_number__fw_ldexp
     module procedure dp_number__fw_ldexp
  end interface fw_ldexp

  !> ldexp - backward differentiation
  !! @param[in] y 'number', obeservations
  !! @param[inout] lam 'number', rate parameter
  !! @param[in] d 'number', log-density
  interface bw_ldexp
     module procedure sp_number__bw_ldexp
     module procedure dp_number__bw_ldexp
  end interface bw_ldexp

  !> ldlaplace - operator
  !! @param[in] y 'number', observations
  !! @param[in] mu 'number', location parameter
  !! @param[in] lam 'number', rate paramter
  !! @param[inout] ld 'number', log-density
  interface op_ldlaplace
     module procedure sp_number__op_ldlaplace
     module procedure dp_number__op_ldlaplace
  end interface op_ldlaplace

  !> ldlaplace - forward differentiation
  !! @param[in] y 'number', observations
  !! @param[in] mu 'number', location parameter
  !! @param[in] lam 'number', rate paramter
  !! @param[inout] ld 'number', log-density
  interface fw_ldlaplace
     module procedure sp_number__fw_ldlaplace
     module procedure dp_number__fw_ldlaplace
  end interface fw_ldlaplace

  !> ldlaplace - backward differentiation
  !! @param[in] y 'number', observations
  !! @param[inout] mu 'number', location parameter
  !! @param[inout] lam 'number', rate paramter
  !! @param[in] ld 'number', log-density
  interface bw_ldlaplace
     module procedure sp_number__bw_ldlaplace
     module procedure dp_number__bw_ldlaplace
  end interface bw_ldlaplace

  !> ldbeta - operator
  !! @param[in] y 'number', observations
  !! @param[in] a1,a2 'number', shape parameters
  !! @param[inout] ld 'number', log-density
  interface op_ldbeta
     module procedure sp_number__op_ldbeta
     module procedure dp_number__op_ldbeta
  end interface op_ldbeta

  !> ldbeta - forward differentiation
  !! @param[in] y 'number', observations
  !! @param[in] a1,a2 'number', shape parameters
  !! @param[inout] ld 'number', log-density
  interface fw_ldbeta
     module procedure sp_number__fw_ldbeta
     module procedure dp_number__fw_ldbeta
  end interface fw_ldbeta

  !> ldbeta - backward differentiation
  !! @param[in] y 'number', observations
  !! @param[inout] a1,a2 'number', shape parameters
  !! @param[in] ld 'number', log-density
  interface bw_ldbeta
     module procedure sp_number__bw_ldbeta
     module procedure dp_number__bw_ldbeta
  end interface bw_ldbeta

  !> ldgamma - operator
  !! @param[in] y 'number', observations
  !! @param[in] a 'number', shape parameter
  !! @param[in] b 'number', rate parameter
  !! @param[inout] ld 'number', log-density
  interface op_ldgamma
     module procedure sp_number__op_ldgamma
     module procedure dp_number__op_ldgamma
  end interface op_ldgamma

  !> ldgamma - forward difference
  !! @param[in] y 'number', observations
  !! @param[in] a 'number', shape parameter
  !! @param[in] b 'number', rate parameter
  !! @param[inout] ld 'number', log-density
  interface fw_ldgamma
     module procedure sp_number__fw_ldgamma
     module procedure dp_number__fw_ldgamma
  end interface fw_ldgamma

  !> ldgamma - backward difference
  !! @param[in] y 'number', observations
  !! @param[inout] a 'number', shape parameter
  !! @param[inout] b 'number', rate parameter
  !! @param[in] ld 'number', log-density
  interface bw_ldgamma
     module procedure sp_number__bw_ldgamma
     module procedure dp_number__bw_ldgamma
  end interface bw_ldgamma

  !> ldnorm - operator
  !! @param[in] y 'number', observations
  !! @param[in] mu 'number' mean
  !! @param[in] s 'number', standard deviation
  !! @param[inout] ld 'number', log-density
  interface op_ldnorm
     module procedure sp_number__op_ldnorm
     module procedure dp_number__op_ldnorm
  end interface op_ldnorm

  !> ldnorm - forward differenatiation
  !! @param[in] y 'number', observations
  !! @param[in] mu 'number' mean
  !! @param[in] s 'number', standard deviation
  !! @param[inout] ld 'number', log-density
  interface fw_ldnorm
     module procedure sp_number__fw_ldnorm
     module procedure dp_number__fw_ldnorm
  end interface fw_ldnorm

  !> ldnorm - backward differentiation
  !! @param[in] y 'number', observations
  !! @param[inout] mu 'number' mean
  !! @param[inout] s 'number', standard deviation
  !! @param[in] ld 'number', log-density
  interface bw_ldnorm
     module procedure sp_number__bw_ldnorm
     module procedure dp_number__bw_ldnorm
  end interface bw_ldnorm

  !> ldmvnorm__1 - operator
  !! @param[in] y 'number', observations
  !! @param[in] mu 'number', mean
  !! @param[in] E 'number', covariance matrix
  !! @param[inout] ld 'number' of rank 0, log-density
  interface op_ldmvnorm__1
     module procedure sp_number__op_ldmvnorm__1
     module procedure dp_number__op_ldmvnorm__1
  end interface op_ldmvnorm__1

  !> ldmvnorm__1 - forwanrd differentiation
  !! PLACE HOLDER
  !! @param[in] y 'number', observations
  !! @param[in] mu 'number', mean
  !! @param[in] E 'number', covariance matrix
  !! @param[inout] ld 'number' of rank 0, log-density
  interface fw_ldmvnorm__1
     module procedure sp_number__fw_ldmvnorm__1
     module procedure dp_number__fw_ldmvnorm__1
  end interface fw_ldmvnorm__1

  !> ldmvnorm__1 - backward differentiation
  !! @param[in] y 'number', observations
  !! @param[inout] mu 'number', mean
  !! @param[inout] E 'number', covariance matrix
  !! @param[in] ld 'number' of rank 0, log-density
  interface bw_ldmvnorm__1
     module procedure sp_number__bw_ldmvnorm__1
     module procedure dp_number__bw_ldmvnorm__1
  end interface bw_ldmvnorm__1

  !> Multivariate posterior distribution
  !! Calculates the posterior distribution of a mutlivariate Gaussian
  !! according to its partitions. Patition 1,1 needs to be specified
  !! w.r.t. the relative precision matrix.
  !!
  !! @param[in] a11 `number`, precision matrix for partition 1,1
  !! @param[in] e21 `number`, covariance matrix for partition 2,1
  !! @param[in] e22 `number`, covariance matrix of partition 2,2
  !! @param[in] y11 `number`, observations of partition 1,1
  !! @param[in] mu11 `number`, mean of partition 1,1
  !! @param[inout] pmu `number`, posterior mean
  !! @param[inout] pe `number`, posterior covariance
  interface op_mvnorm_posterior__1
     module procedure sp_number__op_mvnorm_posterior__1
     module procedure dp_number__op_mvnorm_posterior__1
  end interface op_mvnorm_posterior__1


    !> Multivariate posterior distribution
  !! Calculates the posterior distribution of a mutlivariate Gaussian
  !! according to its partitions. Patition 1,1 needs to be specified
  !! w.r.t. the relative covarinace matrix.
  !!
  !! @param[in] a11 `number`, precision matrix for partition 1,1
  !! @param[in] e21 `number`, covariance matrix for partition 2,1
  !! @param[in] e22 `number`, covariance matrix of partition 2,2
  !! @param[in] y11 `number`, observations of partition 1,1
  !! @param[in] mu11 `number`, mean of partition 1,1
  !! @param[inout] pmu `number`, posterior mean
  !! @param[inout] pe `number`, posterior covariance
  interface op_mvnorm_posterior__2
     module procedure sp_number__op_mvnorm_posterior__2
     module procedure dp_number__op_mvnorm_posterior__2
  end interface op_mvnorm_posterior__2

  !> Multivariate posterior distribution
  !! Forward differentiation considering partition 1,1 specified w.r.t.
  !! its precision matrix.
  !!
  !! @param[in] a11 `number`, precision matrix for partition 1,1
  !! @param[in] e21 `number`, covariance matrix for partition 2,1
  !! @param[in] e22 `number`, covariance matrix of partition 2,2
  !! @param[in] y11 `number`, observations of partition 1,1
  !! @param[in] mu11 `number`, mean of partition 1,1
  !! @param[inout] pmu `number`, posterior mean
  !! @param[inout] pe `number`, posterior covariance
  interface fw_mvnorm_posterior__1
     module procedure sp_number__fw_mvnorm_posterior__1
     module procedure dp_number__fw_mvnorm_posterior__1
  end interface fw_mvnorm_posterior__1

  !> Multivariate posterior distribution
  !! Forward differentiation considering partition 1,1 specified w.r.t.
  !! its covariance matrix.
  !!
  !! @param[in] a11 `number`, precision matrix for partition 1,1
  !! @param[in] e21 `number`, covariance matrix for partition 2,1
  !! @param[in] e22 `number`, covariance matrix of partition 2,2
  !! @param[in] y11 `number`, observations of partition 1,1
  !! @param[in] mu11 `number`, mean of partition 1,1
  !! @param[inout] pmu `number`, posterior mean
  !! @param[inout] pe `number`, posterior covariance
  interface fw_mvnorm_posterior__2
     module procedure sp_number__fw_mvnorm_posterior__2
     module procedure dp_number__fw_mvnorm_posterior__2
  end interface fw_mvnorm_posterior__2

  !> Multivariate posterior distribution
  !! Backward differentiation considering partition 1,1 specified w.r.t.
  !! its precision matrix.
  !!
  !! @param[in] a11 `number`, precision matrix for partition 1,1
  !! @param[in] e21 `number`, covariance matrix for partition 2,1
  !! @param[in] e22 `number`, covariance matrix of partition 2,2
  !! @param[in] y11 `number`, observations of partition 1,1
  !! @param[in] mu11 `number`, mean of partition 1,1
  !! @param[inout] pmu `number`, posterior mean
  !! @param[inout] pe `number`, posterior covariance
  interface bw_mvnorm_posterior__1
     module procedure sp_number__bw_mvnorm_posterior__1
     module procedure dp_number__bw_mvnorm_posterior__1
  end interface bw_mvnorm_posterior__1

  !> Multivariate posterior distribution
  !! Backward differentiation considering partition 1,1 specified w.r.t.
  !! its covariance matrix.
  !!
  !! @param[in] a11 `number`, precision matrix for partition 1,1
  !! @param[in] e21 `number`, covariance matrix for partition 2,1
  !! @param[in] e22 `number`, covariance matrix of partition 2,2
  !! @param[in] y11 `number`, observations of partition 1,1
  !! @param[in] mu11 `number`, mean of partition 1,1
  !! @param[inout] pmu `number`, posterior mean
  !! @param[inout] pe `number`, posterior covariance
  interface bw_mvnorm_posterior__2
     module procedure sp_number__bw_mvnorm_posterior__2
     module procedure dp_number__bw_mvnorm_posterior__2
  end interface bw_mvnorm_posterior__2

  !> @}
