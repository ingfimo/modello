!> @relates op_ldexp
subroutine xp_number__op_ldexp (y, lam, ld)
  implicit none
  type(xp_number), intent(in) :: y, lam
  type(xp_number), intent(inout) :: ld
  if (get_rank(lam) == 0) then
     !$omp parallel workshare
     ld%v = ldexp(y%v, lam%v(1))
     !$omp end parallel workshare
  else
     !$omp parallel workshare
     ld%v = ldexp(y%v, lam%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__op_ldexp

!> @relates fw_ldexp 
subroutine xp_number__fw_ldexp (y, lam, ld)
  implicit none
  type(xp_number), intent(in) :: y, lam
  type(xp_number), intent(inout) :: ld
  if (has_dx(ld)) then
     if (get_rank(lam) == 0) then
        ld%dv = dlam_ldexp(y%v, lam%v(1)) * lam%dv(1)
     else
        ld%dv = dlam_ldexp(y%v, lam%v) * lam%dv
     end if
  end if
end subroutine xp_number__fw_ldexp

!> @relates bw_ldexp 
subroutine xp_number__bw_ldexp (y, lam, ld)
  implicit none
  type(xp_number), intent(in) :: y, ld
  type(xp_number), intent(inout) :: lam
  if (has_dx(ld)) then
     if (get_rank(lam) == 0) then
        !$omp parallel workshare
        lam%dv = lam%dv + sum(dlam_ldexp(y%v, lam%v(1)) * ld%dv)
        !$omp end parallel workshare
     else
        !$omp parallel workshare
        lam%dv = lam%dv + dlam_ldexp(y%v, lam%v) * ld%dv
        !$omp end parallel workshare
     end if
  end if
end subroutine xp_number__bw_ldexp

!> @relates op_ldlaplace
subroutine xp_number__op_ldlaplace (y, mu, lam, ld)
  implicit none
  type(xp_number), intent(in) :: y, mu, lam
  type(xp_number), intent(inout) :: ld
  if (get_rank(mu) == 0 .and. get_rank(lam) == 0) then
     !$omp parallel workshare
     ld%v = ldlaplace(y%v, mu%v(1), lam%v(1))
     !$omp end parallel workshare
  else if (get_rank(mu) == 0) then
     !$omp parallel workshare
     ld%v = ldlaplace(y%v, mu%v(1), lam%v)
     !$omp end parallel workshare
  else if (get_rank(lam) == 0) then
     !$omp parallel workshare
     ld%v = ldlaplace(y%v, mu%v, lam%v(1))
     !$omp end parallel workshare
  else
     !$omp parallel workshare
     ld%v = ldlaplace(y%v, mu%v, lam%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__op_ldlaplace

!> @relates fw_ldlaplace
subroutine xp_number__fw_ldlaplace (y, mu, lam, ld)
  implicit none
  type(xp_number), intent(in) :: y, mu, lam
  type(xp_number), intent(inout) :: ld
  if (has_dx(ld)) then
     if (has_dx(mu)) then
        if (get_rank(mu) == 0 .and. get_rank(lam) == 0) then
           ld%dv = ld%dv + dmu_ldlaplace(y%v, mu%v(1), lam%v(1)) * mu%dv(1)
        else if (get_rank(mu) == 0) then
           ld%dv = ld%dv + dmu_ldlaplace(y%v, mu%v(1), lam%v) * mu%dv(1)
        else if (get_rank(lam) == 0) then
           ld%dv = ld%dv + dmu_ldlaplace(y%v, mu%v(1), lam%v(1)) * mu%dv(1)
        else
           ld%dv = ld%dv + dmu_ldlaplace(y%v, mu%v, lam%v) * mu%dv
        end if
     end if
     if (has_dx(lam)) then
        if (get_rank(mu) == 0 .and. get_rank(lam) == 0) then
           ld%dv = ld%dv + dlam_ldlaplace(y%v, mu%v(1), lam%v(1)) * lam%dv
        else if (get_rank(mu) == 0) then
           ld%dv = ld%dv + dlam_ldlaplace(y%v, mu%v(1), lam%v) * lam%dv
        else if (get_rank(lam) == 0) then
           ld%dv = ld%dv + dlam_ldlaplace(y%v, mu%v, lam%v(1)) * lam%dv(1)
        else
           ld%dv = ld%dv + dlam_ldlaplace(y%v, mu%v, lam%v) * lam%dv
        end if
     end if
  end if
end subroutine xp_number__fw_ldlaplace

!> @relates bw_ldlaplace
subroutine xp_number__bw_ldlaplace (y, mu, lam, ld)
  implicit none
  type(xp_number), intent(in) :: y, ld
  type(xp_number), intent(inout) :: mu, lam
  if (has_dx(lam)) then
     if (has_dx(mu)) then
        if (get_rank(mu) == 0 .and. get_rank(lam) == 0) then
           !$omp parallel workshare
           mu%dv = mu%dv + sum(dmu_ldlaplace(y%v, mu%v(1), lam%v(1)) * ld%dv)
           !$omp end parallel workshare
        else if (get_rank(mu) == 0) then
           !$omp parallel workshare
           mu%dv = mu%dv + sum(dmu_ldlaplace(y%v, mu%v(1), lam%v) * ld%dv)
           !$omp end parallel workshare
        else if (get_rank(lam) == 0) then
           !$omp parallel workshare
           mu%dv = mu%dv + dmu_ldlaplace(y%v, mu%v, lam%v(1)) * ld%dv
           !$omp end parallel workshare
        else
           !$omp parallel workshare
           mu%dv = mu%dv + dmu_ldlaplace(y%v, mu%v, lam%v) * ld%dv
           !$omp end parallel workshare
        end if
     end if
     if (has_dx(lam)) then
        if (get_rank(mu) == 0 .and. get_rank(lam) == 0) then
           !$omp parallel workshare
           lam%dv = lam%dv + sum(dlam_ldlaplace(y%v, mu%v(1), lam%v(1)) * ld%dv)
           !$omp end parallel workshare
        else if (get_rank(mu) == 0) then
           !$omp parallel workshare
           lam%dv = lam%dv + dlam_ldlaplace(y%v, mu%v(1), lam%v) * ld%dv
           !$omp end parallel workshare
        else if (get_rank(lam) == 0) then
           !$omp parallel workshare
           lam%dv = lam%dv + sum(dlam_ldlaplace(y%v, mu%v, lam%v(1)) * ld%dv)
           !$omp end parallel workshare
        else
           !$omp parallel workshare
           lam%dv = lam%dv + dlam_ldlaplace(y%v, mu%v, lam%v) * ld%dv
           !$omp end parallel workshare
        end if
     end if
  end if
end subroutine xp_number__bw_ldlaplace

!> @relates op_ldbeta
subroutine xp_number__op_ldbeta (y, a1, a2, ld)
  implicit none
  type(xp_number), intent(in) :: y, a1, a2
  type(xp_number), intent(inout) :: ld
  if (get_rank(a1) == 0 .and. get_rank(a2) == 0) then
     !$omp parallel workshare
     ld%v = ldbeta(y%v, a1%v(1), a2%v(1))
     !$omp end parallel workshare
  else if (get_rank(a1) == 0) then
     !$omp parallel workshare
     ld%v = ldbeta(y%v, a1%v(1), a2%v)
     !$omp end parallel workshare
  else if (get_rank(a2) == 0) then
     !$omp parallel workshare
     ld%v = ldbeta(y%v, a1%v, a2%v(1))
     !$omp end parallel workshare
  else
     !$omp parallel workshare
     ld%v = ldbeta(y%v, a1%v, a2%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__op_ldbeta

!> @relates fw_ldbeta
subroutine xp_number__fw_ldbeta (y, a1, a2, ld)
  type(xp_number), intent(in) :: y, a1, a2
  type(xp_number), intent(inout) :: ld
  if (has_dx(ld)) then
     if (has_dx(a1)) then
        if (get_rank(a1) == 0 .and. get_rank(a2) == 0) then
           ld%dv = ld%dv + da1_ldbeta(y%v, a1%v(1), a2%v(1)) * a1%dv(1)
        else if (get_rank(a1) == 0) then
           ld%dv = ld%dv + da1_ldbeta(y%v, a1%v(1), a2%v) * a1%dv(1)
        else if (get_rank(a2) == 0) then
           ld%dv = ld%dv + da1_ldbeta(y%v, a1%v, a2%v(1)) * a1%dv
        else
           ld%dv = ld%dv + da1_ldbeta(y%v, a1%v, a2%v) * a1%dv
        end if
     end if
     if (has_dx(a2)) then
        if (get_rank(a1) == 0 .and. get_rank(a2) == 0) then
           ld%dv = ld%dv + da2_ldbeta(y%v, a1%v(1), a2%v(1)) * a2%dv(1)
        else if (get_rank(a1) == 0) then
           ld%dv = ld%dv + da2_ldbeta(y%v, a1%v(1), a2%v) * a2%dv
        else if (get_rank(a2) == 0) then
           ld%dv = ld%dv + da2_ldbeta(y%v, a1%v, a2%v(1)) * a2%dv(1)
        else
           ld%dv = ld%dv + da2_ldbeta(y%v, a1%v, a2%v) * a2%dv
        end if
     end if
  end if
end subroutine xp_number__fw_ldbeta

!> @relates bw_ldbeta
subroutine xp_number__bw_ldbeta (y, a1, a2, ld)
  implicit none
  type(xp_number), intent(in) :: y, ld
  type(xp_number), intent(inout) :: a1, a2
  if (has_dx(ld)) then
     if (has_dx(a1)) then
        if (get_rank(a1) == 0 .and. get_rank(a2) == 0) then
           !$omp parallel workshare
           a1%dv = a1%dv + sum(da1_ldbeta(y%v, a1%v(1), a2%v(1)) * ld%dv)
           !$omp end parallel workshare
        else if (get_rank(a1) == 0) then
           !$omp parallel workshare
           a1%dv = a1%dv + sum(da1_ldbeta(y%v, a1%v(1), a2%v) * ld%dv)
           !$omp end parallel workshare
        else if (get_rank(a2) == 0) then
           !$omp parallel workshare
           a1%dv = a1%dv + da1_ldbeta(y%v, a1%v, a2%v(1)) * ld%dv
           !$omp end parallel workshare
        else
           !$omp parallel workshare
           a1%dv = a1%dv + da1_ldbeta(y%v, a1%v, a2%v) * ld%dv
           !$omp end parallel workshare
        end if
     end if
     if (has_dx(a2)) then
        if (get_rank(a1) == 0 .and. get_rank(a2) == 0) then
           !$omp parallel workshare
           a2%dv = a2%dv + sum(da2_ldbeta(y%v, a1%v(1), a2%v(1)) * ld%dv)
           !$omp end parallel workshare
        else if (get_rank(a1) == 0) then
           !$omp parallel workshare
           a2%dv = a2%dv + da2_ldbeta(y%v, a1%v(1), a2%v) * ld%dv
           !$omp end parallel workshare
        else if (get_rank(a2) == 0) then
           !$omp parallel workshare
           a2%dv = a2%dv + sum(da2_ldbeta(y%v, a1%v, a2%v(1)) * ld%dv)
           !$omp end parallel workshare
        else
           !$omp parallel workshare
           a2%dv = a2%dv + da2_ldbeta(y%v, a1%v, a2%v) * ld%dv
           !$omp end parallel workshare
        end if
     end if
  end if
end subroutine xp_number__bw_ldbeta

!> @relates op_ldgamma
subroutine xp_number__op_ldgamma (y, a, b, ld)
  implicit none
  type(xp_number), intent(in) :: y, a, b
  type(xp_number), intent(inout) :: ld
  if (get_rank(a) == 0 .and. get_rank(b) == 0) then
     !$omp parallel workshare
     ld%v = ldgamma(y%v, a%v(1), b%v(1))
     !$omp end parallel workshare
  else if (get_rank(a) == 0) then
     !$omp parallel workshare
     ld%v = ldgamma(y%v, a%v(1), b%v)
     !$omp end parallel workshare
  else if (get_rank(b) == 0) then
     !$omp parallel workshare
     ld%v = ldgamma(y%v, a%v, b%v(1))
     !$omp end parallel workshare
  else
     !$omp parallel workshare
     ld%v = ldgamma(y%v, a%v, b%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__op_ldgamma

!> @relates fw_ldgamma
subroutine xp_number__fw_ldgamma (y, a, b, ld)
  implicit none
  type(xp_number), intent(in) :: y, a, b
  type(xp_number), intent(inout) :: ld
  if (has_dx(ld)) then
     if (has_dx(a)) then
        if (get_rank(a) == 0 .and. get_rank(b) == 0) then
           ld%dv = ld%dv + da_ldgamma(y%v, a%v(1), b%v(1)) * a%dv(1)
        else if (get_rank(a) == 0) then
           ld%dv = ld%dv + da_ldgamma(y%v, a%v(1), b%v) * a%dv(1)
        else if (get_rank(b) == 0) then
           ld%dv = ld%dv + da_ldgamma(y%v, a%v, b%v(1)) * a%dv
        else
           ld%dv = ld%dv + da_ldgamma(y%v, a%v, b%v) * a%dv
        end if
     end if
     if (has_dx(b)) then
        if (get_rank(a) == 0 .and. get_rank(b) == 0) then
           ld%dv = ld%dv + db_ldgamma(y%v, a%v(1), b%v(1)) * b%dv(1)
        else if (get_rank(a) == 0) then
           ld%dv = ld%dv + db_ldgamma(y%v, a%v(1), b%v) * b%dv
        else if (get_rank(b) == 0) then
           ld%dv = ld%dv + db_ldgamma(y%v, a%v, b%v(1)) * b%dv(1)
        else
           ld%dv = ld%dv + db_ldgamma(y%v, a%v, b%v) * b%dv
        end if
     end if
  end if
end subroutine xp_number__fw_ldgamma

!> @relates bw_ldgamma
subroutine xp_number__bw_ldgamma (y, a, b, ld)
  implicit none
  type(xp_number), intent(in) :: y, ld
  type(xp_number), intent(inout) :: a, b
  if (has_dx(ld)) then
     if (has_dx(a)) then
        if (get_rank(a) == 0 .and. get_rank(b) == 0) then
           !$omp parallel workshare
           a%dv = a%dv + sum(da_ldgamma(y%v, a%v(1), b%v(1)) * ld%dv)
           !$omp end parallel workshare
        else if (get_rank(a) == 0) then
           !$omp parallel workshare
           a%dv = a%dv + sum(da_ldgamma(y%v, a%v(1), b%v) * ld%dv)
           !$omp end parallel workshare
        else if (get_rank(b) == 0) then
           !$omp parallel workshare
           a%dv = a%dv + da_ldgamma(y%v, a%v, b%v(1)) * ld%dv
           !$omp end parallel workshare
        else
           !$omp parallel workshare
           a%dv = a%dv + da_ldgamma(y%v, a%v, b%v) * ld%dv
           !$omp end parallel workshare
        end if
     end if
     if (has_dx(b)) then
        if (get_rank(a) == 0 .and. get_rank(b) == 0) then
           !$omp parallel workshare
           b%dv = b%dv + sum(db_ldgamma(y%v, a%v(1), b%v(1)) * ld%dv)
           !$omp end parallel workshare
        else if (get_rank(a) == 0) then
           !$omp parallel workshare
           b%dv = b%dv + db_ldgamma(y%v, a%v(1), b%v) * ld%dv
           !$omp end parallel workshare
        else if (get_rank(b) == 0) then
           !$omp parallel workshare
           b%dv = b%dv + sum(db_ldgamma(y%v, a%v, b%v(1)) * ld%dv)
           !$omp end parallel workshare
        else
           !$omp parallel workshare
           b%dv = db_ldgamma(y%v, a%v, b%v) * ld%dv
           !$omp end parallel workshare
        end if
     end if
  end if
end subroutine xp_number__bw_ldgamma

!> @relates op_ldnorm
subroutine xp_number__op_ldnorm (y, mu, s, ld)
  implicit none
  type(xp_number), intent(in) :: y, mu, s
  type(xp_number), intent(inout) :: ld
  if (get_rank(mu) == 0 .and. get_rank(s) == 0) then
     !$omp parallel workshare
     ld%v = ldnorm(y%v, mu%v(1), s%v(1))
     !$omp end parallel workshare
  else if (get_rank(mu) == 0) then
     !$omp parallel workshare
     ld%v = ldnorm(y%v, mu%v(1), s%v)
     !$omp end parallel workshare
  else if (get_rank(s) == 0) then
     !$omp parallel workshare
     ld%v = ldnorm(y%v, mu%v, s%v(1))
     !$omp end parallel workshare
  else
     !$omp parallel workshare
     ld%v = ldnorm(y%v, mu%v, s%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__op_ldnorm

!> @relates fw_ldnorm
subroutine xp_number__fw_ldnorm (y, mu, s, ld)
  implicit none
  type(xp_number), intent(in) :: y, mu, s
  type(xp_number), intent(inout) :: ld
  if (has_dx(ld)) then
     if (has_dx(mu)) then
        if (get_rank(mu) == 0 .and. get_rank(s) == 0) then
           ld%dv = ld%dv + dmu_ldnorm(y%v, mu%v(1), s%v(1)) * mu%dv(1)
        else if (get_rank(mu) == 0) then
           ld%dv = ld%dv + dmu_ldnorm(y%v, mu%v(1), s%v) * mu%dv(1)
        else if (get_rank(s) == 0) then
           ld%dv = ld%dv + dmu_ldnorm(y%v, mu%v, s%v(1)) * mu%dv
        end if
     end if
     if (has_dx(s)) then
        if (get_rank(mu) == 0 .and. get_rank(s) == 0) then
           ld%dv = ld%dv + ds_ldnorm(y%v, mu%v(1), s%v(1)) * s%dv
        else if (get_rank(mu) == 0) then
           ld%dv = ld%dv + ds_ldnorm(y%v, mu%v(1), s%v) * s%dv
        else if (get_rank(s) == 0) then
           ld%dv = ld%dv + ds_ldnorm(y%v, mu%v, s%v(1)) * s%dv(1)
        else
           ld%dv = ld%dv + ds_ldnorm(y%v, mu%v, s%v) * s%dv
        end if
     end if
  end if
end subroutine xp_number__fw_ldnorm

!> @relates bw_ldnorm
subroutine xp_number__bw_ldnorm (y, mu, s, ld)
  implicit none
  type(xp_number), intent(in) :: y, ld
  type(xp_number), intent(inout) :: mu, s
  if (has_dx(ld)) then
     if (has_dx(mu)) then
        if (get_rank(mu) == 0 .and. get_rank(s) == 0) then
           !$omp parallel workshare
           mu%dv = mu%dv + sum(dmu_ldnorm(y%v, mu%v(1), s%v(1)) * ld%dv)
           !$omp end parallel workshare
        else if (get_rank(mu) == 0) then
           !$omp parallel workshare
           mu%dv = mu%dv + sum(dmu_ldnorm(y%v, mu%v(1), s%v) * ld%dv)
           !$omp end parallel workshare
        else if (get_rank(s) == 0) then
           !$omp parallel workshare
           mu%dv = mu%dv + dmu_ldnorm(y%v, mu%v, s%v(1)) * ld%dv
           !$omp end parallel workshare
        else
           !$omp parallel workshare
           mu%dv = mu%dv + dmu_ldnorm(y%v, mu%v, s%v) * ld%dv
           !$omp end parallel workshare
        end if
     end if
     if (has_dx(s)) then
        if (get_rank(mu) == 0 .and. get_rank(s) == 0) then
           !$omp parallel workshare
           s%dv = s%dv + sum(ds_ldnorm(y%v, mu%v(1), s%v(1)) * ld%dv)
           !$omp end parallel workshare
        else if (get_rank(mu) == 0) then
           !$omp parallel workshare
           s%dv = s%dv + ds_ldnorm(y%v, mu%v(1), s%v) * ld%dv
           !$omp end parallel workshare
        else if (get_rank(s) == 0) then
           !$omp parallel workshare
           s%dv = s%dv + sum(ds_ldnorm(y%v, mu%v, s%v(1)) * ld%dv)
           !$omp end parallel workshare
        else
           !$omp parallel workshare
           s%dv = s%dv + ds_ldnorm(y%v, mu%v, s%v) * ld%dv
           !$omp end parallel workshare
        end if
     end if
  end if
end subroutine xp_number__bw_ldnorm

!> @relates op_ldmvnorm__1
subroutine xp_number__op_ldmvnorm__1 (y, mu, E, ld)
  implicit none
  type(xp_number), intent(in) :: y, mu, E
  type(xp_number), intent(inout) :: ld
  call do_within('op_ldmvnorm__1', mod_operators_name_, private_do)
contains
  subroutine private_do
    real(kind=xp_), pointer :: xE(:,:)
    real(kind=xp_) :: Q(1,1), x(get_size(y),1), lnD
    integer :: info
    info = 0
    Q = 0
    lnD = 0
    call with_shape(E, xE, .false.)
    !$omp parallel workshare
    x(:,1) = y%v - mu%v
    !$omp end parallel workshare
    call quadraticsym2(xE, x, Q, info, lnD)
    call assert(info==0, info, 'quadraticsym2')
    if (info == 0) ld%v = -0.5_xp_ * (get_size(y) * log(2 * pi_xp_) + lnD + Q(1,1))
  end subroutine private_do
end subroutine xp_number__op_ldmvnorm__1

!> @relates fw_ldmvnorm__1
subroutine xp_number__fw_ldmvnorm__1 (y, mu, E, ld)
  implicit none
  type(xp_number), intent(in) :: y, mu, E
  type(xp_number), intent(inout) :: ld
  call raise_error("fw_ldmvnorm__1 is only a placeholder", err_generic_)
end subroutine xp_number__fw_ldmvnorm__1

!> @relates bw_ldmvnorm__1
subroutine xp_number__bw_ldmvnorm__1 (y, mu, E, ld)
  implicit none
  type(xp_number), intent(in) :: y, mu, E
  type(xp_number), intent(inout) :: ld
  call do_within('bw_ldmvnorm__1', mod_operators_name_, private_bw)
contains
  subroutine private_bw
    real(kind=xp_), dimension(:,:), pointer :: xE, dxE
    real(kind=xp_), dimension(E%shp(1),E%shp(2)) :: IE, dIE
    real(kind=xp_) :: x(get_size(y),1), dx(get_size(y),1)
    real(kind=xp_) :: dQ(1,1), lnD, D
    integer :: info
    call with_shape(E, xE, .false.)
    call with_shape(E, dxE, .true.)
    info = 0
    dx = 0
    IE = xE
    dIE = 0
    dQ = -0.5_xp_
    lnD = 0
    D = 0
    x(:,1) = y%v - mu%v
    call INVSYMMAT(IE, info, lnD)
    call bd_quadraticsym1(IE, x, dQ, dIE, dx)
    call assert(info==0, info, 'DX_BW_quadraticsym2')
    if (info == 0) then
       ! w.r.t. mu
       if (has_dx(mu)) then
          mu%dv = mu%dv - dx(:,1) * ld%dv(1)
       end if
       ! w.r.t. E
       if (has_dx(E)) then
          dIE = dIE * ld%dv(1)
          call bdx_invmat(dIE, IE, dxE)
          D = exp(lnD)
          IE = BDX_DET(IE, D, dx_log(D), .true.)
          dxE = dxE - 0.5_xp_ * IE * ld%dv(1)
       end if
       ! w.r.t. y
       if (has_dx(y)) then
          y%dv = y%dv + dx(:,1) * ld%dv(1)
       end if
    end if
  end subroutine private_bw
end subroutine xp_number__bw_ldmvnorm__1

!> @relates op_mvnorm_posterior__1
subroutine xp_number__op_mvnorm_posterior__1 (a11, e21, e22, y11, mu11, pmu, pe)
  implicit none
  type(xp_number), intent(in) :: a11, e21, e22, y11, mu11
  type(xp_number), intent(inout) :: pmu, pe
  real(kind=xp_), dimension(:,:), pointer :: xa11, xe21, xpe
  real(kind=xp_) :: X(a11%shp(2),e21%shp(1))
  call with_shape(a11, xa11, .false.)
  call with_shape(e21, xe21, .false.) 
  call with_shape(pe, xpe, .false.)
  call smmmult(0, 1, 1._xp_, xa11, xe21, 0._xp_, X)
  !$omp parallel sections
  !$omp section
  pmu%v = mu11%v(1)
  call gmvmult(1, 1._xp_, X, y11%v - mu11%v(1), 1._xp_, pmu%v)
  !$omp section
  pe%v = e22%v
  call gmmmult(0, 0, -1._xp_, xe21, X, 1._xp_, xpe)
  !$omp end parallel sections 
end subroutine xp_number__op_mvnorm_posterior__1

!> @relates fw_mvnorm_posterior__1
subroutine xp_number__fw_mvnorm_posterior__1 (a11, e21, e22, y11, mu11, pmu, pe)
  type(xp_number), intent(in) :: a11, e21, e22, y11, mu11
  type(xp_number), intent(inout) :: pmu, pe
  call raise_error("fw_mvnorm_posterior is only a placeholder", err_generic_)
end subroutine xp_number__fw_mvnorm_posterior__1

!> @relates bw_mvnorm_posterior__1
subroutine xp_number__bw_mvnorm_posterior__1 (a11, e21, e22, y11, mu11, pmu, pe)
  implicit none
  type(xp_number), intent(inout) :: a11, e21, e22, y11, mu11
  type(xp_number), intent(in) :: pmu, pe
  real(kind=xp_), dimension(:,:), pointer :: xa11, xe21
  real(kind=xp_), dimension(:,:), pointer :: dxa11, dxe21, dxpe
  real(kind=xp_), dimension(a11%shp(2),e21%shp(1)) :: X, dX
  real(kind=xp_), dimension(e22%shp(1)) :: dd
  call with_shape(a11, xa11, .false.)
  call with_shape(a11, dxa11, .true.)
  call with_shape(e21, xe21, .false.)
  call with_shape(e21, dxe21, .true.)
  call with_shape(pe, dxpe, .true.)
  X = 0
  dX = 0
  dd = 0
  call smmmult(0, 1, 1._xp_, xa11, xe21, 0._xp_, X)
  !$omp parallel sections
  !$omp section
  if (has_dx(y11) .or. has_dx(mu11)) call bdx_gmvmult(1, 1._xp_, X, pmu%dv, dd)
  !$omp section
  call bdA_gmvmult(1, 1._xp_, y11%v - mu11%v(1), pmu%dv, dX)
  call bdB_gmmmult(0, 0, -1._xp_, xe21, dxpe, dX)
  if (has_dx(a11)) call bdA_gmmmult(0, 1, 1._xp_, xe21, dX, dxa11)
  if (has_dx(e21)) then
     call bdB_gmmmult(0, 1, 1._xp_, xa11, dX, dxe21)
     call bdA_gmmmult(0, 0, -1._xp_, X, dxpe, dxe21)
  end if
  !$omp end parallel sections
  if (has_dx(y11)) then
     !$omp parallel workshare
     y11%dv = y11%dv + dd
     !$omp end parallel workshare
  end if
  if (has_dx(mu11)) then
     !$omp parallel workshare
     mu11%dv = mu11%dv - sum(dd) + sum(pmu%dv)
     !$omp end parallel workshare
  end if
  if (has_dx(e22)) then
     !$omp parallel workshare
     e22%dv = e22%dv + pe%dv
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_mvnorm_posterior__1

!> @relates op_mvnorm_posterior__2
subroutine xp_number__op_mvnorm_posterior__2 (e11, e21, e22, y11, mu11, pmu, pe)
  implicit none
  type(xp_number), intent(in) :: e11, e21, e22, y11, mu11
  type(xp_number), intent(inout) :: pmu, pe
  real(kind=xp_), dimension(:,:), pointer :: xe11, xe21, xpe
  real(kind=xp_) :: A(e11%shp(1),e11%shp(2)), B(e21%shp(1),e21%shp(2))
  integer :: info
  call with_shape(e11, xe11, .false.)
  call with_shape(e21, xe21, .false.) 
  call with_shape(pe, xpe, .false.)
  A = xe11
  B = transpose(xe21)
  call SOLVESYM(A, B, info)
  !$omp parallel sections
  !$omp section
  pmu%v = mu11%v(1)
  call gmvmult(1, 1._xp_, B, y11%v - mu11%v(1), 1._xp_, pmu%v)
  !$omp section
  pe%v = e22%v
  call gmmmult(0, 0, -1._xp_, xe21, B, 1._xp_, xpe)
  !$omp end parallel sections 
end subroutine xp_number__op_mvnorm_posterior__2

!> @relates fw_mvnorm_posterior__2
subroutine xp_number__fw_mvnorm_posterior__2 (e11, e21, e22, y11, mu11, pmu, pe)
  implicit none
  type(xp_number), intent(inout) :: e11, e21, e22, y11, mu11
  type(xp_number), intent(in) :: pmu, pe
  call raise_error("fw_mvnorm_posterior__2 is only a placeholder", err_generic_)
end subroutine xp_number__fw_mvnorm_posterior__2

!> @relates bw_mvnorm_posterior__2
subroutine xp_number__bw_mvnorm_posterior__2 (e11, e21, e22, y11, mu11, pmu, pe)
  implicit none
  type(xp_number), intent(inout) :: e11, e21, e22, y11, mu11
  type(xp_number), intent(in) :: pmu, pe
  call do_within('bw_mvnorm_posterior__2', mod_operators_name_, private_bw)
contains
  subroutine private_bw
    real(kind=xp_), dimension(:,:), pointer :: xe11, xe21
    real(kind=xp_), dimension(:,:), pointer :: dxe11, dxe21, dxpe
    real(kind=xp_), dimension(e21%shp(2),e21%shp(1)) :: X, dB
    real(kind=xp_), dimension(e11%shp(2),e21%shp(1)) :: dX
    real(kind=xp_), dimension(e22%shp(1)) :: dd
    integer :: info
    call with_shape(e11, xe11, .false.)
    call with_shape(e11, dxe11, .true.)
    call with_shape(e21, xe21, .false.)
    call with_shape(e21, dxe21, .true.)
    call with_shape(pe, dxpe, .true.)
    X = transpose(xe21)
    dB = 0
    dX = 0
    dd = 0
    call SOLVESYM(xe11, X, info)
    call assert(info == 0, info, "SOLVESYM")
    if (info == 0) then
       !$omp parallel sections
       !$omp section
       if (has_dx(y11) .or. has_dx(mu11)) then
          call bdx_gmvmult(1, 1._xp_, X, pmu%dv, dd)
       end if
       !$omp section
       call bdA_gmvmult(1, 1._xp_, y11%v - mu11%v(1), pmu%dv, dX)
       call bdB_gmmmult(0, 0, -1._xp_, xe21, dxpe, dX)
       if (has_dx(e11) .or. has_dx(e21)) then
          call bdB_solvesym(xe11, dX, dB, info)
          call assert(info == 0, info, "bdB_solvesym")
          if (info == 0) then
             if (has_dx(e11)) call bdA_solve(dB, X, dxe11)
             if (has_dx(e21)) then
                dxe21 = transpose(dB)
                call bdA_gmmmult(0, 0, -1._xp_, X, dxpe, dxe21)
             end if
          end if
       end if
       !$omp end parallel sections
       if (has_dx(e22)) then
          !$omp parallel workshare
          e22%dv = e22%dv + pe%dv
          !$omp end parallel workshare
       end if
       if (has_dx(y11)) then
          !$omp parallel workshare
          y11%dv = y11%dv + dd
          !$omp end parallel workshare
       end if
       if (has_dx(mu11)) then
          !$omp parallel workshare
          mu11%dv = mu11%dv - sum(dd) + sum(pmu%dv)
          !$omp end parallel workshare
       end if
    end if
  end subroutine private_bw
end subroutine xp_number__bw_mvnorm_posterior__2
