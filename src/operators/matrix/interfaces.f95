  !> @addtogroup operators__matrix_interfaces_
  !! @{

  !> General Matrix-Matrix Multiplication - operator
  !! @param[in] transA integer, if > 0 op(A) = A**T else op(A) = A
  !! @param[in] transB integer, if > 0 op(B) = B**T else op(B) = B
  !! @param[in] alpha 'number of rank 0
  !! @param[in] A 'number' of rank 2
  !! @param[in] B 'number' of rank 2
  !! @param[in] beta 'number' of rank 1
  !! @param[in] C 'number' of rank 2
  !! @param[inout] CC 'number' of rank 2
  interface op_gmmmult
     !> alpha * op(A) . op(B) + beta * C
     module procedure sp_number__op_gmmmult__1
     !> alpha * op(A) . op(B)
     module procedure sp_number__op_gmmmult__2
     !> op(A) . op(B) + C
     module procedure sp_number__op_gmmmult__3
     !> op(A) . op(B)
     module procedure sp_number__op_gmmmult__4
     module procedure dp_number__op_gmmmult__1
     module procedure dp_number__op_gmmmult__2
     module procedure dp_number__op_gmmmult__3
     module procedure dp_number__op_gmmmult__4
  end interface op_gmmmult

  !> General Matrix-Matrix Multiplication - forward differentiation
  !! @param[in] transA integer, if > 0 op(A) = A**T else op(A) = A
  !! @param[in] transB integer, if > 0 op(B) = B**T else op(B) = B
  !! @param[in] alpha 'number of rank 0
  !! @param[in] A 'number' of rank 2
  !! @param[in] B 'number' of rank 2
  !! @param[in] beta 'number' of rank 1
  !! @param[in] C 'number' of rank 2
  !! @param[inout] CC 'number' of rank 2
  interface fw_gmmmult
     module procedure sp_number__fw_gmmmult__1
     module procedure sp_number__fw_gmmmult__2
     module procedure sp_number__fw_gmmmult__3
     module procedure sp_number__fw_gmmmult__4
     module procedure dp_number__fw_gmmmult__1
     module procedure dp_number__fw_gmmmult__2
     module procedure dp_number__fw_gmmmult__3
     module procedure dp_number__fw_gmmmult__4
  end interface fw_gmmmult

  !> Gemm 1 - backward differentiation
  !! @param[in] transA integer, if > 0 op(A) = A**T else op(A) = A
  !! @param[in] transB integer, if > 0 op(B) = B**T else op(B) = B
  !! @param[inout] alpha 'number of rank 0
  !! @param[inout] A 'number' of rank 2
  !! @param[inout] B 'number' of rank 2
  !! @param[inout] beta 'number' of rank 1
  !! @param[inout] C 'number' of rank 2
  !! @param[in] CC 'number' of rank 2
  interface bw_gmmmult
     module procedure sp_number__bw_gmmmult__1
     module procedure sp_number__bw_gmmmult__2
     module procedure sp_number__bw_gmmmult__3
     module procedure sp_number__bw_gmmmult__4
     module procedure dp_number__bw_gmmmult__1
     module procedure dp_number__bw_gmmmult__2
     module procedure dp_number__bw_gmmmult__3
     module procedure dp_number__bw_gmmmult__4
  end interface bw_gmmmult

  !> General Matrix-Vector Multiplication - operator
  !! @param[in] trans integer, if > 0 op(A) = A**T else op(A) = A
  !! @param[in] alpha 'number' of rank 0
  !! @param[in] x 'number' of rank 1
  !! @param[in] A 'number' of rank 2
  !! @param[in] beta 'number' of rank 0
  !! @param[in] y 'number' of rank 1
  !! @param[inout] yy 'number' of rank 1
  interface op_gmvmult
     !> alpha * op(A) . x + beta * y
     module procedure sp_number__op_gmvmult__1
     !> alpha * op(A) . x
     module procedure sp_number__op_gmvmult__2
     !> op(A) . x + y
     module procedure sp_number__op_gmvmult__3
     !> op(A) . x
     module procedure sp_number__op_gmvmult__4
     module procedure dp_number__op_gmvmult__1
     module procedure dp_number__op_gmvmult__2
     module procedure dp_number__op_gmvmult__3
     module procedure dp_number__op_gmvmult__4
  end interface op_gmvmult

  !> General Matrix-Vector Multiplication - forward differentiation
  !! @param[in] trans integer, if > 0 op(A) = A**T else op(A) = A
  !! @param[in] alpha 'number' of rank 0
  !! @param[in] x 'number' of rank 1
  !! @param[in] A 'number' of rank 2
  !! @param[in] beta 'number' of rank 0
  !! @param[in] y 'number' of rank 1
  !! @param[inout] yy 'number' of rank 1
  interface fw_gmvmult
     module procedure sp_number__fw_gmvmult__1
     module procedure sp_number__fw_gmvmult__2
     module procedure sp_number__fw_gmvmult__3
     module procedure sp_number__fw_gmvmult__4
     module procedure dp_number__fw_gmvmult__1
     module procedure dp_number__fw_gmvmult__2
     module procedure dp_number__fw_gmvmult__3
     module procedure dp_number__fw_gmvmult__4
  end interface fw_gmvmult

  !> General Matrix-Vector Multiplication - backward differentiation
  !! @param[in] trans integer, if > 0 op(A) = A**T else op(A) = A
  !! @param[inout] alpha 'number' of rank 0
  !! @param[inout] x 'number' of rank 1
  !! @param[inout] A 'number' of rank 2
  !! @param[inout] beta 'number' of rank 0
  !! @param[inout] y 'number' of rank 1
  !! @param[in] yy 'number' of rank 1
  interface bw_gmvmult
     module procedure sp_number__bw_gmvmult__1
     module procedure sp_number__bw_gmvmult__2
     module procedure sp_number__bw_gmvmult__3
     module procedure sp_number__bw_gmvmult__4
     module procedure dp_number__bw_gmvmult__1
     module procedure dp_number__bw_gmvmult__2
     module procedure dp_number__bw_gmvmult__3
     module procedure dp_number__bw_gmvmult__4
  end interface bw_gmvmult

  !> Outer Vector Product - operator
  !! @param[in] alpha 'number' of rank 0
  !! @param[in] x 'number' of rank 1
  !! @param[in] y 'number' of rank 1
  !! @param[in] z 'number' of rank 2
  !! @param[inout] A 'number' of rank 2
  interface op_vvouter
     !> alpha * x . y**T + A
     module procedure sp_number__op_vvouter__1
     !> x . y**T + A
     module procedure sp_number__op_vvouter__2
     !> x . y**T
     module procedure sp_number__op_vvouter__3
     module procedure dp_number__op_vvouter__1
     module procedure dp_number__op_vvouter__2
     module procedure dp_number__op_vvouter__3
  end interface op_vvouter

  !> Outer Vector Product - forward differentiation
  !! PLACE HOLDER
  !! @todo implement
  interface fw_vvouter
     module procedure sp_number__fw_vvouter__1
     module procedure sp_number__fw_vvouter__2
     module procedure sp_number__fw_vvouter__3
     module procedure dp_number__fw_vvouter__1
     module procedure dp_number__fw_vvouter__2
     module procedure dp_number__fw_vvouter__3
  end interface fw_vvouter

  !> Outer Vector Product - backward differentiation
  !! @param[in] alpha 'number' of rank 0
  !! @param[inout] 'number' of rank 1
  !! @param[inout] 'number' of rank 1
  !! @param[inout] 'number' of rank 2
  !! @param[in] A 'number' of rank 2
  interface bw_vvouter
     module procedure sp_number__bw_vvouter__1
     module procedure sp_number__bw_vvouter__2
     module procedure sp_number__bw_vvouter__3
     module procedure dp_number__bw_vvouter__1
     module procedure dp_number__bw_vvouter__2
     module procedure dp_number__bw_vvouter__3
  end interface bw_vvouter

  !> Inner Vector Product - operator
  !! x**T . y
  !! @param[in] x 'number' of rank 1
  !! @param[in] y 'number' of rank 1
  !! @param[inout] z 'number' of rank 0
  interface op_vvinner
     module procedure sp_number__op_vvinner
     module procedure dp_number__op_vvinner
  end interface op_vvinner

  !> Inner Vector Product - forward differentiation
  !! PLACE HOLDER
  !! @todo implement
  !! @param[in] x 'number' of rank 1
  !! @param[in] y 'number' of rank 1
  !! @param[inout] z 'number' of rank 0
  interface fw_vvinner
     module procedure sp_number__fw_vvinner
     module procedure dp_number__fw_vvinner
  end interface fw_vvinner

  !> Inner Vector Product - backward differentiation
  !! @param[inout 'number' of rank 1
  !! @param[inout 'number' of rank 1
  !! @param[inout]  z 'number' of rank 0
  interface bw_vvinner
     module procedure sp_number__bw_vvinner
     module procedure dp_number__bw_vvinner
  end interface bw_vvinner

  !> General Matrix Inversion - operator
  !! @param[in] x 'number' of rank 2
  !! @param[inout] xi 'number' of rank 2
  interface op_invMat
     module procedure sp_number__op_invMat
     module procedure dp_number__op_invMat
  end interface op_invMat

  !> General Matrix Inversion - forward differentiation
  !! @param[in] x 'number' of rank 2
  !! @param[inout] xi 'number' of rank 2
  interface fw_invMat
     module procedure sp_number__fw_invMat
     module procedure dp_number__fw_invMat
  end interface fw_invMat

  !> General Matrix Inversion - backward differentiation
  !! @param[inout 'number' of rank 2
  !! @param[in] xi 'number' of rank 2
  interface bw_invMat
     module procedure sp_number__bw_invMat
     module procedure dp_number__bw_invMat
  end interface bw_invMat


  interface op_slidemmm
     module procedure sp_number__op_slidemmm
     module procedure dp_number__op_slidemmm
  end interface op_slidemmm

  interface fw_slidemmm
     module procedure sp_number__fw_slidemmm
     module procedure dp_number__fw_slidemmm
  end interface fw_slidemmm

  interface bw_slidemmm
     module procedure sp_number__bw_slidemmm
     module procedure dp_number__bw_slidemmm
  end interface bw_slidemmm
  
  !> @}
