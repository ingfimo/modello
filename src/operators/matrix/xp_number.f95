!> @relates op_gmmmult
subroutine xp_number__op_gmmmult__1 (transA, transB, alpha, A, B, beta, C, CC)
  implicit none
  integer, intent(in) :: transA, transB
  type(xp_number), intent(in), target :: alpha, beta
  type(xp_number), intent(in), target :: A, B
  type(xp_number), intent(in) :: C
  type(xp_number), intent(inout), target :: CC
  real(kind=xp_), pointer :: xa(:,:), xb(:,:), xcc(:,:)
  CC%v = C%v
  call with_shape(A, xa, .false.)
  call with_shape(B, xb, .false.)
  call with_shape(CC, xcc, .false.)
  call gmmmult(transA, transB, alpha%v(1), xa, xb, beta%v(1), xcc)
end subroutine xp_number__op_gmmmult__1

!> @relates fw_gmmmult
subroutine xp_number__fw_gmmmult__1 (transA, transB, alpha, A, B, beta, C, CC)
  implicit none
  integer, intent(in) :: transA, transB
  type(xp_number), intent(in), target :: A, B, alpha
  type(xp_number), intent(inout), target :: CC
  type(xp_number), intent(in) :: beta, C
  real(kind=xp_), pointer :: xa(:,:), xb(:,:), xcc(:,:)
  real(kind=xp_), pointer :: dxa(:,:), dxb(:,:), dxcc(:,:) 
  real(kind=xp_) :: bb
  if (has_dx(CC)) then
     call with_shape(A, xa, .false.)
     call with_shape(B, xb, .false.)
     call with_shape(CC, xcc, .false.)
     call with_shape(CC, dxcc, .true.) 
     call with_shape(CC, dxcc, .true.)
     bb = 0
     if (has_dx(A)) then
        call with_shape(A, dxa, .true.)
        call gmmmult(transA, transB, alpha%v(1), dxa, xb, bb, xcc)
        bb = 1._xp_
     end if
     if (has_dx(B)) then
        call with_shape(B, dxb, .true.)
        call gmmmult(transA, transB, alpha%v(1), xa, dxb, bb, xcc)
        bb = 1._xp_
     end if
     if (has_dx(alpha)) then
        call gmmmult(transA, transB, alpha%dv(1), xa, xb, bb, xcc)
     end if
     if (has_dx(C)) then
        CC%dv = CC%dv + beta%v(1) * C%dv
     end if
     if (has_dx(beta)) then
        CC%dv = CC%dv + beta%dv(1) * C%v
     end if
  end if
end subroutine xp_number__fw_gmmmult__1

!> @relates bw_gmmmult
subroutine xp_number__bw_gmmmult__1 (transA, transB, alpha, A, B, beta, C, CC)
  implicit none
  integer, intent(in) :: transA, transB
  type(xp_number), intent(inout) :: A, B, C
  type(xp_number), intent(inout) :: alpha, beta
  type(xp_number), intent(in) :: CC
  real(kind=xp_), pointer :: xa(:,:), xb(:,:), xcc(:,:)
  real(kind=xp_), pointer :: dxa(:,:), dxb(:,:), dxcc(:,:) 
  if (has_dx(CC)) then
     call with_shape(A, xa, .false.)
     call with_shape(B, xb, .false.)
     call with_shape(CC, xcc, .false.)
     call with_shape(CC, dxcc, .true.)
     !$omp parallel sections
     !$omp section
     if (has_dx(A)) then
        call with_shape(A, dxa, .true.)
        call bdA_gmmmult(transA, transB, alpha%v(1), xb, dxcc, dxa)
     end if
     !$omp section
     if (has_dx(B)) then
        call with_shape(B, dxb, .true.)
        call bdB_gmmmult(transA, transB, alpha%v(1), xa, dxcc, dxb)
     end if
     !$omp section
     if (has_dx(alpha)) then
        call bdALPHA_gmmmult(transA, transB, xa, xb, dxcc, alpha%dv(1))
     end if
     !$omp end parallel sections
     if (has_dx(C)) then
        !$omp parallel workshare
        C%dv = C%dv + beta%v(1) * CC%dv
        !$omp end parallel workshare
     end if
     if (has_dx(beta)) then
        !$omp parallel workshare
        beta%dv(1) = beta%dv(1) + sum(C%v * CC%dv)
        !$omp end parallel workshare
     end if
  end if
end subroutine xp_number__bw_gmmmult__1

!> @relates op_gmmmult
subroutine xp_number__op_gmmmult__2 (alpha, transA, transB, A, B, C)
  integer, intent(in) :: transA, transB
  type(xp_number), intent(in) :: alpha
  type(xp_number), intent(in) :: A, B
  type(xp_number), intent(inout) :: C
  real(kind=xp_), pointer :: xa(:,:), xb(:,:), xc(:,:)
  call with_shape(A, xa, .false.)
  call with_shape(B, xb, .false.)
  call with_shape(C, xc, .false.)
  call gmmmult(transA, transB, alpha%v(1), xa, xb, 0._xp_, xc)
end subroutine xp_number__op_gmmmult__2

!> @relates fw_gmmmult
subroutine xp_number__fw_gmmmult__2 (alpha, transA, transB, A, B, C)
  implicit none
  integer, intent(in) :: transA, transB
  type(xp_number), intent(in) :: A, B
  type(xp_number), intent(inout) :: C
  type(xp_number), intent(in) :: alpha
  real(kind=xp_) :: beta
  real(kind=xp_), pointer :: xa(:,:), xb(:,:), xc(:,:)
  real(kind=xp_), pointer :: dxa(:,:), dxb(:,:), dxc(:,:)
  if (has_dx(C)) then
     call with_shape(A, xa, .false.)
     call with_shape(B, xb, .false.)
     call with_shape(C, xc, .false.)
     call with_shape(C, dxc, .true.)
     beta = 0
     if (has_dx(A)) then
        call with_shape(A, dxa, .true.)
        call gmmmult(transA, transB, alpha%v(1), dxa, xb, beta, dxc)
        beta = 1
     end if
     if (has_dx(B)) then
        call with_shape(B, dxb, .true.)
        call gmmmult(transA, transB, alpha%v(1), xa, dxb, beta, dxc)
        beta = 1
     end if
     if (has_dx(alpha)) then
        call gmmmult(transA, transB, alpha%dv(1), xa, xb, beta, dxc)
     end if
  end if
end subroutine xp_number__fw_gmmmult__2

!> @relates bw_gmmmult
subroutine xp_number__bw_gmmmult__2 (alpha, transA, transB, A, B, C)
  implicit none
  integer, intent(in) :: transA, transB
  type(xp_number), intent(inout) :: A, B
  type(xp_number), intent(inout) :: alpha
  type(xp_number), intent(in) :: C
  real(kind=xp_), pointer :: xa(:,:), xb(:,:), xc(:,:)
  real(kind=xp_), pointer :: dxa(:,:), dxb(:,:), dxc(:,:)
  if (has_dx(C)) then
     call with_shape(A, xa, .false.)
     call with_shape(B, xb, .false.)
     call with_shape(C, xc, .false.)
     call with_shape(C, dxc, .true.)
     !$omp parallel sections
     !$omp section
     if (has_dx(A)) then
        call with_shape(A, dxa, .true.)
        call bdA_gmmmult(transA, transB, alpha%v(1), xb, dxc, dxa)
     end if
     !$omp section
     if (has_dx(B)) then
        call with_shape(B, dxb, .true.)
        call bdB_gmmmult(transA, transB, alpha%v(1), xa, dxc, dxb)
     end if
     !$omp section
     if (has_dx(alpha)) then
        call bdALPHA_gmmmult(transA, transB, xa, xb, dxc, alpha%dv(1))
     end if
     !$omp end parallel sections
  end if
end subroutine xp_number__bw_gmmmult__2

!> @relates op_gmmmult
subroutine xp_number__op_gmmmult__3 (transA, transB, A, B, C, CC)
  implicit none
  integer, intent(in) :: transA, transB
  type(xp_number), intent(in) :: A, B, C
  type(xp_number), intent(inout) :: CC
  real(kind=xp_), pointer :: xa(:,:), xb(:,:), xcc(:,:)
  CC%v = C%v
  call with_shape(A, xa, .false.)
  call with_shape(B, xb, .false.)
  call with_shape(CC, xcc, .false.)
  call gmmmult(transA, transB, 1._xp_, xa, xb, 1._xp_, xcc)
end subroutine xp_number__op_gmmmult__3

!> @relates fw_gmmmult
subroutine xp_number__fw_gmmmult__3 (transA, transB, A, B, C, CC)
  implicit none
  integer, intent(in) :: transA, transB
  type(xp_number), intent(in) :: A, B, C
  type(xp_number), intent(inout) :: CC
  real(kind=xp_), pointer :: xa(:,:), xb(:,:), xcc(:,:)
  real(kind=xp_), pointer :: dxa(:,:), dxb(:,:), dxcc(:,:)
  real(kind=xp_) :: bb
  if (has_dx(CC)) then
     call with_shape(A, xa, .false.)
     call with_shape(B, xb, .false.)
     call with_shape(CC, xcc, .false.)
     call with_shape(CC, dxcc, .true.)
     bb = 0
     if (has_dx(A)) then
        call with_shape(A, dxa, .true.)
        call gmmmult(transA, transB, 1._xp_, dxa, xb, bb, dxcc)
        bb = 1
     end if
     if (has_dx(B)) then
        call with_shape(B, dxb, .true.)
        call gmmmult(transA, transB, 1._xp_, xa, dxb, bb, dxcc)
     end if
     if (has_dx(C)) then
        CC%dv = CC%dv + C%dv
     end if
  end if
end subroutine xp_number__fw_gmmmult__3

!> @relates bw_gmmmult
subroutine xp_number__bw_gmmmult__3 (transA, transB, A, B, C, CC)
  implicit none
  integer, intent(in) :: transA, transB
  type(xp_number), intent(inout) :: A, B, C
  type(xp_number), intent(in) :: CC
  real(kind=xp_), pointer :: xa(:,:), xb(:,:), xcc(:,:)
  real(kind=xp_), pointer :: dxa(:,:), dxb(:,:), dxcc(:,:)
  if (has_dx(CC)) then
     call with_shape(A, xa, .false.)
     call with_shape(B, xb, .false.)
     call with_shape(CC, xcc, .false.)
     call with_shape(CC, dxcc, .true.)
     !$omp parallel sections
     !$omp section
     if (has_dx(A)) then
        call with_shape(A, dxa, .true.)
        call bdA_gmmmult(transA, transB, 1._xp_, xb, dxcc, dxa)
     end if
     !$omp section
     if (has_dx(B)) then
        call with_shape(B, dxb, .true.)
        call bdB_gmmmult(transA, transB, 1._xp_, xa, dxcc, dxb)
     end if
     !$omp end parallel sections
     if (has_dx(C)) then
        !$omp parallel workshare
        C%dv = C%dv + CC%dv
        !$omp end parallel workshare
     end if
  end if
end subroutine xp_number__bw_gmmmult__3

!> @relates op_gmmmult
subroutine xp_number__op_gmmmult__4 (transA, transB, A, B, C)
  integer, intent(in) :: transA, transB
  type(xp_number), intent(in) :: A, B
  type(xp_number), intent(inout) :: C
  real(kind=xp_), pointer :: xa(:,:), xb(:,:), xc(:,:)
  call with_shape(A, xa, .false.)
  call with_shape(B, xb, .false.)
  call with_shape(C, xc, .false.)
  call gmmmult(transA, transB, 1._xp_, xa, xb, 0._xp_, xc)
end subroutine xp_number__op_gmmmult__4

!> @relates fw_gmmmult
subroutine xp_number__fw_gmmmult__4 (transA, transB, A, B, C)
  implicit none
  integer, intent(in) :: transA, transB
  type(xp_number), intent(in) :: A, B
  type(xp_number), intent(inout) :: C
  real(kind=xp_), pointer :: xa(:,:), xb(:,:)
  real(kind=xp_), pointer :: dxa(:,:), dxb(:,:), dxc(:,:)
  real(kind=xp_) :: beta
  if (has_dx(C)) then
     call with_shape(A, xa, .false.)
     call with_shape(B, xb, .false.)
     call with_shape(C, dxc, .true.)
     beta = 0
     if (has_dx(A)) then
        call with_shape(A, dxa, .true.)
        call gmmmult(transA, transB, 1._xp_, dxa, xb, beta, dxc)
        beta = 1
     end if
     if (has_dx(B)) then
        call with_shape(B, dxb, .true.)
        call gmmmult(transA, transB, 1._xp_, xa, dxb, beta, dxc)
     end if
  end if
end subroutine xp_number__fw_gmmmult__4

!> @relates bw_gmmmult
subroutine xp_number__bw_gmmmult__4 (transA, transB, A, B, C)
  implicit none
  integer, intent(in) :: transA, transB
  type(xp_number), intent(inout) :: A, B
  type(xp_number), intent(in) :: C
  real(kind=xp_), pointer :: xa(:,:), xb(:,:)
  real(kind=xp_), pointer :: dxa(:,:), dxb(:,:), dxc(:,:)
  if (has_dx(C)) then
     call with_shape(A, xa, .false.)
     call with_shape(B, xb, .false.)
     call with_shape(C, dxc, .true.)
     !!$omp parallel sections
     !!$omp section
     if (has_dx(A)) then
        call with_shape(A, dxa, .true.)
        call bdA_gmmmult(transA, transB, 1._xp_, xb, dxc, dxa)
     end if
     !!$omp section
     if (has_dx(B)) then
        call with_shape(B, dxb, .true.)
        call bdB_gmmmult(transA, transB, 1._xp_, xa, dxc, dxb)
     end if
     !!$omp end parallel sections
  end if
end subroutine xp_number__bw_gmmmult__4

!> @relates op_gmvmult
subroutine xp_number__op_gmvmult__1 (trans, alpha, x, A, beta, y, yy)
  implicit none
  integer, intent(in) :: trans
  type(xp_number), intent(in) :: alpha, x, A, beta, y
  type(xp_number), intent(inout) :: yy
  real(kind=xp_), pointer :: xa(:,:)
  yy%v = y%v
  call with_shape(A, xa, .false.)
  call gmvmult(trans, alpha%v(1), xa, x%v, beta%v(1), yy%v)
end subroutine xp_number__op_gmvmult__1

!> @relates fw_gmvmult
subroutine xp_number__fw_gmvmult__1 (trans, alpha, x, A, beta, y, yy)
  implicit none
  integer, intent(in) :: trans
  type(xp_number), intent(in) :: alpha, x, A, y, beta
  type(xp_number), intent(inout) :: yy
  real(kind=xp_), pointer :: xa(:,:)
  real(kind=xp_), pointer :: dxa(:,:)
  real(kind=xp_) :: bb
  if (has_dx(yy)) then
     call with_shape(A, xa, .false.)
     bb = 0._xp_
     if (has_dx(A)) then
        call with_shape(A, dxa, .true.)
        call gmvmult(trans, alpha%v(1), dxa, x%v, bb, yy%dv)
        bb = 1._xp_
     end if
     if (has_dx(x)) then
        call gmvmult(trans, alpha%v(1), xa, x%dv, bb, yy%dv)
        bb = 1._xp_
     end if
     if (has_dx(alpha)) then
        call gmvmult(trans, alpha%dv(1), xa, x%v, bb, yy%dv)
     end if
     if (has_dx(y)) then
        yy%dv = yy%dv + beta%v(1) * y%dv
     end if
     if (has_dx(beta)) then
        yy%dv = yy%dv + beta%dv(1) * y%v
     end if
  end if
end subroutine xp_number__fw_gmvmult__1

!> @relates gw_gmvmult
subroutine xp_number__bw_gmvmult__1 (trans, alpha, x, A, beta, y, yy)
  implicit none
  integer, intent(in) :: trans
  type(xp_number), intent(inout) :: x, A, y
  type(xp_number), intent(inout) :: alpha, beta
  type(xp_number), intent(in) :: yy
  real(kind=xp_), pointer :: xa(:,:), dxa(:,:) 
  if (has_dx(yy)) then
     call with_shape(A, xa, .false.)
     call with_shape(A, dxa, .true.)
     !$omp parallel sections
     !$omp section
     if (has_dx(A)) then
        call bdA_gmvmult(trans, alpha%v(1), x%v, yy%dv, dxa)
     end if
     !$omp section
     if (has_dx(x)) then
        call bdx_gmvmult(trans, alpha%v(1), xa, yy%dv, x%dv)
     end if
     !$omp section
     if (has_dx(alpha)) then
        call bdalpha_gmvmult(trans, xa, x%v, yy%dv, alpha%dv(1))
     end if
     !$omp end parallel sections
     if (has_dx(y)) then
        !$omp parallel workshare
        y%dv = y%dv + beta%v(1) * yy%dv
        !$omp end parallel workshare
     end if
     if (has_dx(beta)) then
        !$omp parallel workshare
        beta%dv(1) = beta%dv(1) + sum(y%v * yy%dv)
        !$omp end parallel workshare
     end if
  end if
end subroutine xp_number__bw_gmvmult__1

!> @relates op_gmvmult
subroutine xp_number__op_gmvmult__2 (alpha, trans, x, A, y)
  implicit none
  integer, intent(in) :: trans
  type(xp_number), intent(in) :: alpha, x, A
  type(xp_number), intent(inout) :: y
  real(kind=xp_), pointer :: xa(:,:)
  call with_shape(A, xa, .false.)
  call gmvmult(trans, alpha%v(1), xa, x%v, 0._xp_, y%v)
end subroutine xp_number__op_gmvmult__2

!> @relates fw_gmvmult
subroutine xp_number__fw_gmvmult__2 (alpha, trans, x, A, y)
  implicit none
  integer, intent(in) :: trans
  type(xp_number), intent(in) :: alpha, x, A
  type(xp_number), intent(inout) :: y
  real(kind=xp_), pointer :: xa(:,:)
  real(kind=xp_), pointer :: dxa(:,:)
  real(kind=xp_) :: bb
  if (has_dx(y)) then
     call with_shape(A, xa, .false.)
     bb = 0._xp_
     if (has_dx(A)) then
        call with_shape(A, dxa, .true.)
        call gmvmult(trans, alpha%v(1), dxa, x%v, bb, y%dv)
        bb = 1._xp_
     end if
     if (has_dx(x)) then
        call gmvmult(trans, alpha%v(1), xa, x%dv, bb, y%dv)
        bb = 1._xp_
     end if
     if (has_dx(alpha)) then
        call gmvmult(trans, alpha%dv(1), xa, x%v, bb, y%dv)
     end if
  end if
end subroutine xp_number__fw_gmvmult__2

!> @relates bw_gmvmult
subroutine xp_number__bw_gmvmult__2 (alpha, trans, x, A, y)
  implicit none
  integer, intent(in) :: trans
  type(xp_number), intent(inout) :: x, A
  type(xp_number), intent(inout) :: alpha
  type(xp_number), intent(in) :: y
  real(kind=xp_), pointer :: xa(:,:)
  real(kind=xp_), pointer :: dxa(:,:)
  if (has_dx(y)) then
     call with_shape(A, xa, .false.)
     !$omp parallel sections
     !$omp section
     if (has_dx(A)) then
        call with_shape(A, dxa, .true.)
        call bdA_gmvmult(trans, alpha%v(1), x%v, y%dv, dxa)
     end if
     !$omp section
     if (has_dx(x)) then
        call bdx_gmvmult(trans, alpha%v(1), xa, y%dv, x%dv)
     end if
     !$omp section
     if (has_dx(alpha)) then
        call bdalpha_gmvmult(trans, xa, x%v, y%dv, alpha%dv(1))
     end if
     !$omp end parallel sections
  end if
end subroutine xp_number__bw_gmvmult__2

!> @relates op_gmvmult
subroutine xp_number__op_gmvmult__3 (trans, x, A, y, yy)
  implicit none
  integer, intent(in) :: trans
  type(xp_number), intent(in) :: x, A, y
  type(xp_number), intent(inout) :: yy
  real(kind=xp_), pointer :: xa(:,:)
  yy%v = y%v
  call with_shape(A, xa, .false.)
  call gmvmult(trans, 1._xp_, xa, x%v, 1._xp_, yy%v)
end subroutine xp_number__op_gmvmult__3

!> @relates fw_gmvmult
subroutine xp_number__fw_gmvmult__3 (trans, x, A, y, yy)
  implicit none
  integer, intent(in) :: trans
  type(xp_number), intent(in) :: x, A, y
  type(xp_number), intent(inout) :: yy
  real(kind=xp_), pointer :: xa(:,:)
  real(kind=xp_), pointer :: dxa(:,:)
  real(kind=xp_) :: bb
  if (has_dx(yy)) then
     call with_shape(A, xa, .false.)
     bb = 0._xp_
     if (has_dx(A)) then
        call with_shape(A, dxa, .true.)
        call gmvmult(trans, 1._xp_, dxa, x%v, bb, yy%dv)
        bb = 1._xp_
     end if
     if (has_dx(x)) then
        call gmvmult(trans, 1._xp_, xa, x%dv, bb, yy%dv)
        bb = 1._xp_
     end if
     if (has_dx(y)) then
        yy%dv = yy%dv + y%dv
     end if
  end if
end subroutine xp_number__fw_gmvmult__3

!> @relates bw_gmvmult
subroutine xp_number__bw_gmvmult__3 (trans, x, A, y, yy)
  implicit none
  integer, intent(in) :: trans
  type(xp_number), intent(inout) :: x, A, y
  type(xp_number), intent(in) :: yy
  real(kind=xp_), pointer :: xa(:,:)
  real(kind=xp_), pointer :: dxa(:,:) 
  if (has_dx(yy)) then 
     call with_shape(A, xa, .false.)
     !$omp parallel sections
     !$omp section
     if (has_dx(A)) then
        call with_shape(A, dxa, .true.)
        call bdA_gmvmult(trans, 1._xp_, x%v, yy%dv, dxa)
     end if
     !$omp section
     if (has_dx(x)) then
        call bdx_gmvmult(trans, 1._xp_, xa, yy%dv, x%dv)
     end if
     !$omp end parallel sections
     if (has_dx(y)) then
        !$omp parallel workshare
        y%dv = y%dv + yy%dv
        !$omp end parallel workshare
     end if
  end if
end subroutine xp_number__bw_gmvmult__3

!> @relates op_gmvmult
subroutine xp_number__op_gmvmult__4 (trans, x, A, y)
  implicit none
  integer, intent(in) :: trans
  type(xp_number), intent(in) :: x, A
  type(xp_number), intent(inout) :: y
  real(kind=xp_), pointer :: xa(:,:)
  call with_shape(A, xa, .false.)
  call gmvmult(trans, 1._xp_, xa, x%v, 0._xp_, y%v)
end subroutine xp_number__op_gmvmult__4

!> @relates fw_gmvmult
subroutine xp_number__fw_gmvmult__4 (trans, x, A, y)
  implicit none
  integer, intent(in) :: trans
  type(xp_number), intent(in) :: x, A
  type(xp_number), intent(inout) :: y
  real(kind=xp_), pointer :: xa(:,:)
  real(kind=xp_), pointer :: dxa(:,:)
  real(kind=xp_) :: bb
  if (has_dx(y)) then
     call with_shape(A, xa, .false.)
     bb = 0._xp_
     if (has_dx(A)) then
        call with_shape(A, dxa, .true.)
        call gmvmult(trans, 1._xp_, dxa, x%v, bb, y%dv)
        bb = 1._xp_
     end if
     if (has_dx(x)) then
        call gmvmult(trans, 1._xp_, xa, x%dv, bb, y%dv)
        bb = 1._xp_
     end if
  end if
end subroutine xp_number__fw_gmvmult__4

!> @relates bw_gmvmult
subroutine xp_number__bw_gmvmult__4 (trans, x, A, y)
  implicit none
  integer, intent(in) :: trans
  type(xp_number), intent(inout) :: x, A
  type(xp_number), intent(in) :: y
  real(kind=xp_), pointer :: xa(:,:)
  real(kind=xp_), pointer :: dxa(:,:)
  if (has_dx(y)) then 
     call with_shape(A, xa, .false.)
     !!$omp parallel sections
     !!$omp section
     if (has_dx(A)) then
        call with_shape(A, dxa, .true.)
        call bdA_gmvmult(trans, 1._xp_, x%v, y%dv, dxa)
     end if
     !!$omp section
     if (has_dx(x)) then
        call bdx_gmvmult(trans, 1._xp_, xa, y%dv, x%dv)
     end if
     !!$omp end parallel sections
  end if
end subroutine xp_number__bw_gmvmult__4

!> @relates op_vvouter
subroutine xp_number__op_vvouter__1 (alpha, x, y, z, A)
  implicit none
  type(xp_number), intent(in) :: alpha, x, y, z
  type(xp_number), intent(inout) :: A
  real(kind=xp_), pointer :: xA(:,:)
  A%v = z%v
  call with_shape(A, xA, .false.)
  call vvouter(alpha%v(1), x%v, y%v, xA)
end subroutine xp_number__op_vvouter__1

!> @relates fw_vvouter
subroutine xp_number__fw_vvouter__1(alpha, x, y, z, A)
  implicit none
  type(xp_number), intent(in) :: alpha, x, y, z
  type(xp_number), intent(inout) :: A
  call raise_error("fw_vvouter__1 is only a placeholder", err_generic_)
end subroutine xp_number__fw_vvouter__1

!> @relates bw_vvouter
subroutine xp_number__bw_vvouter__1 (alpha, x, y, z, A)
  implicit none
  type(xp_number), intent(inout) :: alpha, x, y, z
  type(xp_number), intent(in) :: A
  real(kind=xp_), pointer :: dxA(:,:)
  reAL(kind=xp_) :: s
  if (has_dx(A)) then
     call with_shape(A, dxA, .true.)
     !$omp parallel sections
     !$omp section
     if (has_dx(x)) then
        call gmvmult(0, alpha%v(1), dxA, y%v, 1._xp_, x%dv)
     end if
     !$omp section
     if (has_dx(y)) then
        call gmvmult(0, alpha%v(1), dxA, x%v, 1._xp_, y%dv)
     end if
     !$omp end parallel sections
     if (has_dx(alpha)) then
        !$omp parallel workshare
        !alpha%dv = alpha%dv + sum(outerprod(xx, xy) * dxA)
        s = sum((A%v - z%v) / alpha%v(1) * A%dv)
        alpha%dv = alpha%dv + s
        !$omp end parallel workshare
     end if
     if (has_dx(z)) then
        !$omp parallel workshare
        z%dv = z%dv + A%dv
        !$omp end parallel workshare
     end if
  end if
end subroutine xp_number__bw_vvouter__1

!> @relates op_vvouter
subroutine xp_number__op_vvouter__2 (x, y, z, A)
  implicit none
  type(xp_number), intent(in) :: x, y, z
  type(xp_number), intent(inout) :: A
  real(kind=xp_), pointer :: xA(:,:)
  A%v = z%v
  call with_shape(A, xA, .false.)
  call vvouter(1._xp_, x%v, y%v, xA)
end subroutine xp_number__op_vvouter__2

!> @relates fw_vvouter
subroutine xp_number__fw_vvouter__2 (x, y, z, A)
  implicit none
  type(xp_number), intent(in) :: x, y, z
  type(xp_number), intent(inout) :: A
  call raise_error("fw_vvouter__2 is only a placeholder", err_generic_)
end subroutine xp_number__fw_vvouter__2

!> @relates bw_vvouter
subroutine xp_number__bw_vvouter__2 (x, y, z, A)
  implicit none
  type(xp_number), intent(inout) :: x, y, z
  type(xp_number), intent(in) :: A
  real(kind=xp_), pointer :: dxA(:,:)
  if (has_dx(A)) then
     call with_shape(A, dxA, .true.)
     !!omp parallel sections
     !!omp section
     if (has_dx(x)) then
        call gmvmult(0, 1._xp_, dxA, y%v, 1._xp_, x%dv)
     end if
     !!omp section
     if (has_dx(y)) then
        call gmvmult(0, 1._xp_, dxA, x%v, 1._xp_, y%dv)
     end if
     !!omp end parallel sections
     if (has_dx(z)) then
        !$omp parallel workshare
        z%dv = A%dv
        !$omp end parallel workshare
     end if
  end if
end subroutine xp_number__bw_vvouter__2

!> @relates op_vvouter
subroutine xp_number__op_vvouter__3 (x, y, A)
  implicit none
  type(xp_number), intent(in) :: x, y
  type(xp_number), intent(inout) :: A
  real(kind=xp_), pointer :: xA(:,:)
  call with_shape(A, xA, .false.)
  call vvouter(1._xp_, x%v, y%v, xA)
end subroutine xp_number__op_vvouter__3

!> @relates fw_vvouter
subroutine xp_number__fw_vvouter__3 (x, y, A)
  implicit none
  type(xp_number), intent(in) :: x, y
  type(xp_number), intent(inout) :: A
  call raise_error("fw_vvouter__3 is only a placeholder", err_generic_)
end subroutine xp_number__fw_vvouter__3

!> @relates bw_vvouter
subroutine xp_number__bw_vvouter__3 (x, y, A)
  implicit none
  type(xp_number), intent(inout) :: x, y
  type(xp_number), intent(in) :: A
  real(kind=xp_), pointer :: dxA(:,:)
  if (has_dx(A)) then
     call with_shape(A, dxA, .true.)
     !$omp parallel sections
     !$omp section
     if (has_dx(x)) then
        call gmvmult(0, 1._xp_, dxA, y%v, 1._xp_, x%dv)
     end if
     !$omp section
     if (has_dx(y)) then
        call gmvmult(0, 1._xp_, dxA, x%v, 1._xp_, y%dv)
     end if
     !$omp end parallel sections
  end if
end subroutine xp_number__bw_vvouter__3

!> @relates op_vvinner
subroutine xp_number__op_vvinner (x, y, z)
  implicit none
  type(xp_number), intent(in) :: x, y
  type(xp_number), intent(inout) :: z
  z%v(1) = vvinner(x%v, y%v)
end subroutine xp_number__op_vvinner

!> @relates fw_vvinner
subroutine xp_number__fw_vvinner (x, y, z)
  implicit none
  type(xp_number), intent(in) :: x, y
  type(xp_number), intent(inout) :: z
  call raise_error("fw_vvinner is only a placeholder", err_generic_)
end subroutine xp_number__fw_vvinner

!> @relates bw_vvinner
subroutine xp_number__bw_vvinner (x, y, z)
  implicit none
  type(xp_number), intent(inout) :: x, y
  type(xp_number), intent(in) :: z
  if (has_dx(z)) then
     if (has_dx(x)) then
        !$omp parallel workshare
        x%dv = y%v * z%dv(1)
        !$omp end parallel workshare
     end if
     if (has_dx(y)) then
        !$omp parallel workshare
        y%dv = x%v * z%dv(1)
        !$omp end parallel workshare
     end if
  end if
end subroutine xp_number__bw_vvinner

!> @relates op_invmat
subroutine xp_number__op_invMat (x, xi)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), intent(inout) :: xi
  call do_within('op_invMat', mod_operators_name_, private_op)
contains
  subroutine private_op
    real(kind=xp_), pointer :: xx(:,:), xxi(:,:)
    integer :: info
    info = 0
    call with_shape(x, xx, .false.)
    call with_shape(xi, xxi, .false.)
    xxi = xx
    call invMat(xxi, info)
    call assert(info == 0, info, 'invMat')
  end subroutine private_op
end subroutine xp_number__op_invMat

!> @relates fw_invmat
subroutine xp_number__fw_invMat (x, xi)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), intent(inout) :: xi
  real(kind=xp_), pointer :: dxx(:,:), xxi(:,:), dxxi(:,:)
  if (has_dx(xi)) then
     call with_shape(x, dxx, .true.)
     call with_shape(xi, xxi, .false.)
     call with_shape(xi, dxxi, .true.)
     call fdx_invmat(dxx, xxi, dxxi)
  end if
end subroutine xp_number__fw_invMat

!> @relates bw_invmat
subroutine xp_number__bw_invMat (x, xi)
  implicit none
  type(xp_number), intent(inout) :: x
  type(xp_number), intent(in) :: xi
  real(kind=xp_), pointer :: dxx(:,:), xxi(:,:), dxxi(:,:)
  if (has_dx(xi)) then
     call with_shape(x, dxx, .true.)
     call with_shape(xi, xxi, .false.)
     call with_shape(xi, dxxi, .true.)
     call bdx_invmat(dxxi, xxi, dxx)
  end if
end subroutine xp_number__bw_invMat

subroutine xp_number__op_slidemmm (x1, x2, x3, p)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  integer, intent(in), target :: p(:) ![b, l, b1, b2, ht]
  select case (MODE_)
  case (training_mode_)
     call private_op
  case default
     call op_gmmmult(0, 0, x1, x2, x3)
  end select
contains
  subroutine private_op
    real(kind=xp_), pointer :: xxa(:,:), xxb(:,:), xxc(:,:), z1(:), z2(:)
    integer, pointer :: transA, transB, hashB, l, b1(:), b2(:), ht(:)
    type(hashtab), pointer :: htab
    logical :: ta, tb
    integer, allocatable :: s(:)
    integer :: i, j, b, m, n
    transA => p(1)
    transB => p(2)
    hashB => p(3)
    htab => HTS_(p(4))
    call hashtab__clear(htab)
    b = size(htab%t, 1)
    l => p(5)
    b1 => p(6:(5 + l))
    b2 => p((6 + l):(5 + 2*l))
    ht => p((6 + 2*l):(5 + 3*l))
    if (hashB > 0) then
       call with_shape(x1, xxb, .false.)
       call with_shape(x2, xxa, .false.)
       tb = transA > 0
       ta = transB > 0
       n = merge(size(xxb, 1), size(xxb, 2), transA > 0)
       m = merge(size(xxa, 2), size(xxa, 1), transB > 0)
    else
       call with_shape(x1, xxa, .false.)
       call with_shape(x2, xxb, .false.)
       ta = transA > 0
       tb = transB > 0
       n = merge(size(xxb, 1), size(xxb, 2), transB > 0)
       m = merge(size(xxa, 2), size(xxa, 1), transA > 0)
    end if
    call with_shape(x3, xxc, .false.)
    xxc = 0
    do j = 1, n
       if (tb) then
          z1 => xxb(j,:)
       else
          z1 => xxb(:,j)
       end if
       call binary_simhash__sample(z1, HTS_(b1), HTS_(b2), HTS_(ht), b, m, s)
       do i = 1, size(s)
          if (ta) then
             z2 => xxa(:,s(i))
          else
             z2 => xxa(s(i),:)
          end if
          xxc(s(i), j) = vvinner(z2, z1)
          call hashtab__put(htab, j - 1, s(i))
       end do
    end do
    do i = 1, size(htab%t, 2)
    end do
  end subroutine private_op
end subroutine xp_number__op_slidemmm

subroutine xp_number__fw_slidemmm(x1, x2, x3, p)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(out) :: x3
  integer, intent(in) :: p(:)
  call raise_error("not implemented", err_generic_)
end subroutine xp_number__fw_slidemmm

subroutine xp_number__bw_slidemmm (x1, x2, x3, p)
  implicit none
  type(xp_number), intent(inout) :: x1, x2
  type(xp_number), intent(in) :: x3
  integer, intent(in), target :: p(:)
  real(kind=xp_), pointer :: xxa(:,:), xxb(:,:), z(:), dz(:)
  real(kind=xp_), pointer :: dxa(:,:), dxb(:,:), dxc(:,:)
  type(hashtab), pointer :: htab
  integer, pointer :: s(:), transA, transB, hashB
  logical :: ta, tb
  integer :: i, j, n
  if (has_dx(x3)) then
     transA => p(1)
     transB => p(2)
     hashB => p(3)
     htab => HTS_(p(4))
     call with_shape(x3, dxc, .true.)
     if (hashB > 0) then
        call with_shape(x1, xxb, .false.)
        if (has_dx(x1)) call with_shape(x1, dxb, .true.)
        tb = transA > 0   
        call with_shape(x2, xxa, .false.)
        if (has_dx(x2)) call with_shape(x2, dxa, .true.)
        ta = transB > 0
        n = merge(size(xxb, 1), size(xxb, 2), transA > 0)
     else
        call with_shape(x1, xxa, .false.)
        if (has_dx(x1)) call with_shape(x1, dxa, .true.)
        ta = transA > 0   
        call with_shape(x2, xxb, .false.)
        if (has_dx(x2)) call with_shape(x2, dxb, .true.)
        tb = transB > 0
        n = merge(size(xxb, 1), size(xxb, 2), transB > 0)
     end if
     do j = 1, n
        if (hashtab__contains(htab, j - 1)) then
           s => hashtab__get(htab, j - 1)
           do i = 1, size(s)
              if (associated(dxa)) then
                 if (ta) then
                    dz => dxa(:,s(i))
                 else
                    dz => dxa(s(i),:)
                 end if
                 if (tb) then
                    z => xxb(j,:)
                 else
                    z => xxb(:,j)
                 end if
                 dz = z * dxc(s(i),j)
              end if
              if (associated(dxb)) then
                 if (tb) then
                    dz => dxb(j,:)
                 else
                    dz => dxb(:,j)
                 end if
                 if (ta) then
                    z => xxa(:,s(i))
                 else
                    z => xxa(s(i),:)
                 end if
                 dz = z * dxc(s(i),j)
              end if
           end do
        end if
     end do
  end if
end subroutine xp_number__bw_slidemmm
