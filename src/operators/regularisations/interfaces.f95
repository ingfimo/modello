  !> @addtogroup operators__regularisations_interfaces_
  !! @{

  !> Dropout - operator
  !! @param[in] x1 @b number, array to regularize
  !! @param[inout] x2 @b number, to store the dropput coefficients
  !! @param[inout] x3 @b number, regularized array
  interface op_dropout
     module procedure sp_number__op_dropout
     module procedure dp_number__op_dropout
  end interface op_dropout

  !> Dropout - operator
  !! @param[in] x1 @b number, array to regularize
  !! @param[in] x2 @b number, to store the dropput coefficients
  !! @param[inout] x3 @b number, regularized array
  interface fw_dropout
     module procedure sp_number__fw_dropout
     module procedure dp_number__fw_dropout
  end interface fw_dropout

  !> Dropout - backward differenatiation
  !! @param[inout] x1 @b number, array to regularize
  !! @param[in] x2 @b number, to store the dropput coefficients
  !! @param[in] x3 @b number, regularized array
  interface bw_dropout
     module procedure sp_number__bw_dropout
     module procedure dp_number__bw_dropout
  end interface bw_dropout

  !> @}
