!> @relates op_dropout
subroutine xp_number__op_dropout (x1, x2, x3, r)
  implicit none
  type(xp_number), intent(inout) :: x2, x3
  type(xp_number), intent(in) :: x1
  real(kind=xp_), intent(in) :: r
  select case(MODE_)
  case (training_mode_)
     call unif_rand(x2%v)
     !$omp parallel workshare
     where (x2%v > r)
        x2%v = 1 / (1 - r)
     else where (x2%v <= r)
        x2%v = 0
     end where
     x3%v = x1%v * x2%v
     !$omp end parallel workshare
  case default
     !$omp parallel workshare
     x3%v = x1%v
     !$omp end parallel workshare
  end select
end subroutine xp_number__op_dropout

!> @relates fw_dropout
subroutine xp_number__fw_dropout (x1, x2, x3)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(inout) :: x3
  select case(MODE_)
  case (training_mode_)
     if (has_dx(x3)) then
        !$omp parallel workshare
        x3%dv = x3%dv + x1%dv * x2%v
        !$omp end parallel workshare
     end if
  case default
     if (has_dx(x3)) then
        !$omp parallel workshare
        x3%dv = x1%dv
        !$omp end parallel workshare
     end if
  end select
end subroutine xp_number__fw_dropout

!> @relates bw_dropout
subroutine xp_number__bw_dropout (x1, x2, x3)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2, x3
  select case(MODE_)
  case (training_mode_)
     if(has_dx(x3)) then
        !$omp parallel workshare
        x1%dv = x1%dv + x3%dv * x2%v
        !$omp end parallel workshare
     end if
  case default
     if (has_dx(x3)) then
        !$omp parallel workshare
        x1%dv = x3%dv
        !$omp end parallel workshare
     end if
  end select
end subroutine xp_number__bw_dropout
  
