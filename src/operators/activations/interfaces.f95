  !> @addtogroup operators__activations_interfaces_ 
  !! @{

  !> Sigmoid - operator
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface op_sigmoid
     module procedure sp_number__op_sigmoid
     module procedure dp_number__op_sigmoid
  end interface op_sigmoid

  !> Sigmoid - forward differentiation
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface fw_sigmoid
     module procedure sp_number__fw_sigmoid
     module procedure dp_number__fw_sigmoid
  end interface fw_sigmoid

  !> Sigmoid - backward differentiation
  !! @param[inout] x1 'number' input
  !! @param[in] x2 'number' output
  interface bw_sigmoid
     module procedure sp_number__bw_sigmoid
     module procedure dp_number__bw_sigmoid
  end interface bw_sigmoid

  interface op_softplus
     module procedure sp_number__op_softplus
     module procedure dp_number__op_softplus
  end interface op_softplus

  interface fw_softplus
     module procedure sp_number__fw_softplus
     module procedure dp_number__fw_softplus
  end interface fw_softplus

  interface bw_softplus
     module procedure sp_number__bw_softplus
     module procedure dp_number__bw_softplus
  end interface bw_softplus

  !> ReLU - operator
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface op_relu
     module procedure sp_number__op_relu
     module procedure dp_number__op_relu
  end interface op_relu

  !> ReLU - forward differentiation
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface fw_relu
     module procedure sp_number__fw_relu
     module procedure dp_number__fw_relu
  end interface fw_relu

  !> ReLU - backward differentiation
  !! @param[inout] x1 'number' input
  !! @param[in] x2 'number' output
  interface bw_relu
     module procedure sp_number__bw_relu
     module procedure dp_number__bw_relu
  end interface bw_relu

  !> Leaky ReLU - operator
  !! @param[in] x1 'number' input
  !! @param[in] a 'number'
  !! @param[inout] x2 'number' output
  interface op_leakyrelu
     module procedure sp_number__op_leakyrelu
     module procedure dp_number__op_leakyrelu
  end interface op_leakyrelu

  !> ReLU - forward differentiation
  !! @param[in] x1 'number' input
  !! @param[in] a 'number'
  !! @param[inout] x2 'number' output
  interface fw_leakyrelu
     module procedure sp_number__fw_leakyrelu
     module procedure dp_number__fw_leakyrelu
  end interface fw_leakyrelu

  !> ReLU - backward differentiation
  !! @param[inout] x1 'number' input
  !! @param[in] a 'number'
  !! @param[in] x2 'number' output
  interface bw_leakyrelu
     module procedure sp_number__bw_leakyrelu
     module procedure dp_number__bw_leakyrelu
  end interface bw_leakyrelu

  !> ELU - operator
  !! @param[in] x1 'number', input
  !! @param[inout] x2 'number', output
  !! @param[in] a 'number', parameter
  interface op_elu
     module procedure sp_number__op_elu
     module procedure dp_number__op_elu
  end interface op_elu

  !> ELU - forward differentiation
  !! @param[in] x1 'number', input
  !! @param[inout] x2 'number', output
  !! @param[in] a 'number', parameter
  interface fw_elu
     module procedure sp_number__fw_elu
     module procedure dp_number__fw_elu
  end interface fw_elu

  !> ELU - backward differentiation
  !! @param[inout] x1 'number', input
  !! @param[in] x2 'number', output
  !! @param[in] a 'number', parameter
  interface bw_elu
     module procedure sp_number__bw_elu
     module procedure dp_number__bw_elu
  end interface bw_elu

  interface op_silu
     module procedure sp_number__op_silu
     module procedure dp_number__op_silu
  end interface op_silu

  interface fw_silu
     module procedure sp_number__fw_silu
     module procedure dp_number__fw_silu
  end interface fw_silu

  interface bw_silu
     module procedure sp_number__bw_silu
     module procedure dp_number__bw_silu
  end interface bw_silu

  !> Swish - operator
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface op_swish
     module procedure sp_number__op_swish
     module procedure dp_number__op_swish
  end interface op_swish

  !> Swish - forward differentiation
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  interface fw_swish
     module procedure sp_number__fw_swish
     module procedure dp_number__fw_swish
  end interface fw_swish

  !> Swish - bacward differentiation
  !! @param[inout] x1 'number' input
  !! @param[in] x2 'number' output
  interface bw_swish
     module procedure sp_number__bw_swish
     module procedure dp_number__bw_swish
  end interface bw_swish

  !> Standard Softmax - operator
  !! @param[inout] x1 'number' input
  !! @param[in] x2 'number' output
  !! @param[in] k integer, dimensi
  interface op_softmax
     module procedure sp_number__op_softmax__1
     module procedure sp_number__op_softmax__2
     module procedure dp_number__op_softmax__1
     module procedure dp_number__op_softmax__2
  end interface op_softmax

  !> Standard Softmax - forward differentiation
  !! @param[inout] x1 'number' input
  !! @param[in] x2 'number' output
  !! @param[in] k integer, dimension index
  interface fw_softmax
     module procedure sp_number__fw_softmax__1
     module procedure sp_number__fw_softmax__2
     module procedure dp_number__fw_softmax__1
     module procedure dp_number__fw_softmax__2
  end interface fw_softmax

  !> Standard Softmax - backward differentiation
  !! @param[in] x1 'number' input
  !! @param[inout] x2 'number' output
  !! @param[in] k integer, dimension index
  interface bw_softmax
     module procedure sp_number__bw_softmax__1
     module procedure sp_number__bw_softmax__2
     module procedure dp_number__bw_softmax__1
     module procedure dp_number__bw_softmax__2
  end interface bw_softmax

  !> @}
