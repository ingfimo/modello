!> @relates op_sigmoid
subroutine xp_number__op_sigmoid (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = sigmoid(x1%v)
  !$omp end parallel workshare
end subroutine xp_number__op_sigmoid

!> @relates fw_sigmoid
subroutine xp_number__fw_sigmoid (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x2%dv = x1%dv * dx_sigmoid(x1%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_sigmoid

!> @relates bw_sogmoid
subroutine xp_number__bw_sigmoid (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x1%dv = x1%dv + dx_sigmoid(x1%v) * x2%dv
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_sigmoid

!> @relates op_softplus
subroutine xp_number__op_softplus (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = softplus(x1%v)
  !$omp end parallel workshare
end subroutine xp_number__op_softplus

!> @relates fw_softplus
subroutine xp_number__fw_softplus (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x2%dv = x2%dv + x1%dv * dx_softplus(x1%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_softplus

!> @relates bw_softplus
subroutine xp_number__bw_softplus (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x1%dv = x1%dv + dx_softplus(x1%v) * x2%dv
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_softplus

!> @relates op_relu
subroutine xp_number__op_relu (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = relu(x1%v)
  !$omp end parallel workshare
end subroutine xp_number__op_relu

!> @relates fw_relu
subroutine xp_number__fw_relu (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x2%dv = x1%dv * dx_relu(x1%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_relu

!> @relates bw_relu
subroutine xp_number__bw_relu (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  if (has_dx(x2)) then
     !$omp parallel workshare
     x1%dv = x1%dv + dx_relu(x1%v) * x2%dv
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_relu

!> @relates op_leakyrelu
subroutine xp_number__op_leakyrelu (x1, a, x2)
  implicit none
  type(xp_number), intent(in) :: x1, a
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = leakyrelu(x1%v, a%v(1))
  !$omp end parallel workshare
end subroutine xp_number__op_leakyrelu

!> @relates fw_leakyrelu
subroutine xp_number__fw_leakyrelu (x1, a, x2)
  implicit none
  type(xp_number), intent(in) :: x1, a
  type(xp_number), intent(inout) :: x2
  if (has_dx(x2)) then
     if (has_dx(x1)) then
        !$omp parallel workshare
        x2%dv = x2%dv + x1%dv * dx_leakyrelu(x1%v, a%v(1))
        !$omp end parallel workshare
     end if
     if (has_dx(a)) then
        !$omp parallel workshare
        x2%dv = x2%dv + a%dv * da_leakyrelu(x1%v)
        !$omp end parallel workshare
     end if
  end if
end subroutine xp_number__fw_leakyrelu

!> @relates bw_leakyrelu
subroutine xp_number__bw_leakyrelu (x1, a, x2)
  implicit none
  type(xp_number), intent(inout) :: x1, a
  type(xp_number), intent(in) :: x2
  if (has_dx(x2)) then
     if (has_dx(x1)) then
        !$omp parallel workshare
        x1%dv = x1%dv + dx_leakyrelu(x1%v, a%dv(1)) * x2%dv
        !$omp end parallel workshare
     end if
     if (has_dx(x2)) then
        !$omp parallel workshare
        a%dv = a%dv(1) + sum(da_leakyrelu(x1%v) * x2%dv)
        !$omp end parallel workshare
     end if
  end if
end subroutine xp_number__bw_leakyrelu

!> @relates op_relu
subroutine xp_number__op_elu (x1, a, x2)
  implicit none
  type(xp_number), intent(in) :: x1, a
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = elu(x1%v, a%v(1))
  !$omp end parallel workshare
end subroutine xp_number__op_elu

!> @relates fw_elu
subroutine xp_number__fw_elu (x1, a, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  type(xp_number), intent(in) :: a
  if (has_dx(x1)) then
     !$omp parallel workshare
     x2%dv = x2%dv + x1%dv * dx_elu(x1%v, a%v(1))
     !$omp end parallel workshare
  end if
  if (has_dx(a)) then
     !$omp parallel workshare
     x2%dv = x2%dv + a%dv(1) * da_elu(x1%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_elu

!> @relates bw_elu
subroutine xp_number__bw_elu (x1, a, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  type(xp_number), intent(in) :: a
  if (has_dx(x1)) then
     !$omp parallel workshare
     x1%dv = x1%dv + dx_elu(x1%v, a%v(1)) * x2%dv
     !$omp end parallel workshare
  end if
  if (has_dx(a)) then
     !$omp parallel workshare
     a%dv = a%dv(1) + sum(da_elu(x1%v) * x2%dv)
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_elu

!> @relates op_silu
subroutine xp_number__op_silu (x1, ans)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: ans
  !$omp parallel workshare
  ans%v = x1%v * sigmoid(x1%v)
  !$omp end parallel workshare
end subroutine xp_number__op_silu

!> @relates fw_silu
subroutine xp_number__fw_silu (x1, ans)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: ans
  if (has_dx(x1)) then
     !$omp parallel workshare
     ans%dv = ans%dv + x1%dv * dx_silu(x1%v)
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_silu

!> @relates bw_silu
subroutine xp_number__bw_silu (x1, ans)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: ans
  if (has_dx(x1)) then
     !$omp parallel workshare
     x1%dv = x1%dv + dx_silu(x1%v) * ans%dv
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_silu

!> @relates op_swish
subroutine xp_number__op_swish (x1, a, x2)
  implicit none
  type(xp_number), intent(in) :: x1, a
  type(xp_number), intent(inout) :: x2
  !$omp parallel workshare
  x2%v = swish(x1%v, a%v(1))
  !$omp end parallel workshare
end subroutine xp_number__op_swish

!> @relates fw_swish
subroutine xp_number__fw_swish (x1, a, x2)
  implicit none
  type(xp_number), intent(in) :: x1, a
  type(xp_number), intent(inout) :: x2
  if (has_dx(x1)) then
     !$omp parallel workshare
     x2%dv = x2%dv + x1%dv * dx_swish(x1%v, a%v(1))
     !$omp end parallel workshare
  end if
  if (has_dx(x2)) then
     !$omp parallel workshare
     x2%dv = x2%dv + a%dv(1) * da_swish(x1%v, a%v(1))
     !$omp end parallel workshare
  end if
end subroutine xp_number__fw_swish

!> @relates bw_swish
subroutine xp_number__bw_swish (x1, a, x2)
  implicit none
  type(xp_number), intent(inout) :: x1, a
  type(xp_number), intent(in) :: x2
  if (has_dx(x1)) then
     !$omp parallel workshare
     x1%dv = x1%dv + dx_swish(x1%v, a%v(1)) * x2%dv
     !$omp end parallel workshare
  end if
  if (has_dx(a)) then
     !$omp parallel workshare
     a%dv = a%dv + sum(da_swish(x1%v, a%v(1)) * x2%dv)
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_swish

!> @op_softmax
subroutine xp_number__op_softmax__1 (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  x2%v = SOFTMAX(x1%v)
end subroutine xp_number__op_softmax__1

!> @relates fw_softmax
subroutine xp_number__fw_softmax__1 (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  if (has_dx(x2)) then
     x2%dv = x1%dv * sum(DX_SOFTMAX(x1%v, x2%v), 1)
  end if
end subroutine xp_number__fw_softmax__1

!> @relates bw_softmax
subroutine xp_number__bw_softmax__1 (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  real(kind=xp_) :: dx(get_size(x1))
  if (has_dx(x2)) then
     call gmvmult(1, 1._xp_, DX_SOFTMAX(x1%v, x2%v), x2%dv, 0._xp_, dx)
     !$omp parallel workshare
     x1%dv = x1%dv + dx
     !$omp end parallel workshare
  end if
end subroutine xp_number__bw_softmax__1

!> @relates op_softmax
subroutine xp_number__op_softmax__2 (x1, x2, indexes)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  integer, intent(in), target :: indexes(:)
  integer, pointer :: j(:,:)
  integer :: i
  real(kind=xp_) :: m
  j(1:(get_size(x1) / indexes(1)), 1:indexes(1)) => indexes(2:)
  m = maxval(x1%v)
  do i = 1, indexes(1)
     x2%v(j(:,i)) = SOFTMAX(x1%v(j(:,i)))
  end do
end subroutine xp_number__op_softmax__2

!> Softmax by Dimension - forward differentiation
!! @param[in] x1 'number' input
!! @param[inout] x2 'number' output
!! @param[in] k integer, dimension index
!! @todo fix!
subroutine xp_number__fw_softmax__2 (x1, x2, indexes)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  integer, intent(in), target :: indexes(:)
  integer, pointer :: j(:,:)
  integer :: i
  if (has_dx(x2)) then
     j(1:(get_size(x1) / indexes(1)), 1:indexes(1)) => indexes(2:)
     do i = 1, indexes(1)
        x2%dv(j(:,i)) = x2%dv(j(:,i)) + &
             x1%dv(j(:,i)) * sum(DX_SOFTMAX(x1%v(j(:,i)), x2%v(j(:,i))), 1)
     end do
  end if
end subroutine xp_number__fw_softmax__2

!> Softmax by Dimension - backward differentiation
!! @param[inout] x1 'number' input
!! @param[in] x2 'number' output
!! @param[in] k integer, dimension index
subroutine xp_number__bw_softmax__2 (x1, x2, indexes)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  integer, intent(in), target :: indexes(:)
  integer, pointer :: j(:,:)
  integer :: i
  if (has_dx(x2)) then
     j(1:(get_size(x1) / indexes(1)), 1:indexes(1)) => indexes(2:)
     do i = 1, indexes(1)
        x1%dv(j(:,i)) = x1%dv(j(:,i)) + &
             private_bw(x1%v(j(:,i)), x2%v(j(:,i)), x2%dv(j(:,i)))
     end do
  end if
contains
  function private_bw (xx1, xx2, dxx2) result(ans)
    implicit none
    real(kind=xp_), intent(in), dimension(:) :: xx1, xx2, dxx2
    real(kind=xp_) :: ans(size(xx1))
    call gmvmult(1, 1._xp_, DX_SOFTMAX(xx1, xx2), dxx2, 0._xp_, ans)
  end function private_bw
end subroutine xp_number__bw_softmax__2
!> @}

!> @}
