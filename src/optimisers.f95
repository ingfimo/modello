module optimisers

  use env
  use types
  use registers, only: sp_OPTS_, dp_OPTS_
  use registers_utils
  use errwarn
  use math
  use nodes_utils, only: graph__post
  use nodes
  use optim_utils

  private

  public &
       opt__append_sgd, &
       opt__sgd_step, &
       opt__append_sgdwm, &
       opt__sgdwm_step, &
       opt__append_adam, &
       opt__adam_step

  character(len=*), parameter :: mod_optimisers_name_ = 'optimisers'

  interface opt__allocate_sgd
     module procedure sp_opt__allocate_sgd
     module procedure dp_opt__allocate_sgd
  end interface opt__allocate_sgd
  
  interface opt__append_sgd
     module procedure sp_opt__append_sgd
     module procedure dp_opt__append_sgd
  end interface opt__append_sgd

  interface opt__sgd_step
     module procedure sp_opt__sgd_step
     module procedure dp_opt__sgd_step
  end interface opt__sgd_step

  interface opt__allocate_sgdwm
     module procedure sp_opt__allocate_sgdwm
     module procedure dp_opt__allocate_sgdwm
  end interface opt__allocate_sgdwm
  
  interface opt__append_sgdwm
     module procedure sp_opt__append_sgdwm
     module procedure dp_opt__append_sgdwm
  end interface opt__append_sgdwm

  interface opt__sgdwm_step
     module procedure sp_opt__sgdwm_step
     module procedure dp_opt__sgdwm_step
  end interface opt__sgdwm_step

  interface opt__allocate_adam
     module procedure sp_opt__allocate_adam
     module procedure dp_opt__allocate_adam
  end interface opt__allocate_adam

  interface opt__append_adam
     module procedure sp_opt__append_adam
     module procedure dp_opt__append_adam
  end interface opt__append_adam

  interface opt__adam_step
     module procedure sp_opt__adam_step
     module procedure dp_opt__adam_step
  end interface opt__adam_step

  interface sgd_update
     module procedure sp__sgd_update
     module procedure dp__sgd_update
  end interface sgd_update

  interface sgdwm_update
     module procedure sp__sgdwm_update
     module procedure dp__sgdwm_update
  end interface sgdwm_update

  interface adam_update
     module procedure sp__adam_update
     module procedure dp__adam_update
  end interface adam_update

  
  
contains

  include "./optimisers/sgd/sp_opt.f95"
  include "./optimisers/sgd/dp_opt.f95"

  include "./optimisers/sgdwm/sp_opt.f95"
  include "./optimisers/sgdwm/dp_opt.f95"

  include "./optimisers/adam/sp_opt.f95"
  include "./optimisers/adam/dp_opt.f95"
  
end module optimisers


