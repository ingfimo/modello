module numbers_utils

  use omp_lib
  use env
  use types
  use registers, only: &
       sp_NUMBERS_, &
       dp_NUMBERS_, &
       INLOCKS_, &
       NUMNDS_, &
       WITHDX_
  use errwarn
  use utils
  
  private
  public &
       number__allocate, &
       number__associate, &
       number__deallocate, &
       number__get_init, &
       number__set_v, &
       number__set_dv, &
       number__set_slice_v, &
       number__set_slice_dv, &
       number__set_flat_slice_v, &
       number__set_flat_slice_dv, &
       number__get_v, &
       number__get_dv, &
       number__set_nd, &
       number__get_nd, &
       number__reset_nd, &
       number__inlock, &
       number__inlock_free, &
       ! number__take_slice, &
       ! number__take_flat_slice, &
       number__take_contiguous_slice, &
       number__make_reshape, &
       number__make_drop_shape, &
       number__fill_bind, &
       number__make_bind, &
       higher_shape, &
       lower_shape

  character(len=*), parameter :: mod_numbers_utils_name_ = 'numbers_utils'

  !> Allocates a 'number'.
  !! @author Filippo Monariq
  !! @param[inout] x 'number' to allocate
  !! @param[in] v optional, source array (rank 0 or 1)
  !! @param[in] shp integer vector, shape
  !! @param[in] dx logical, if .true. the derivative of the is allocated as well
  interface number__allocate
     module procedure sp_number__allocate__1
     module procedure sp_number__allocate__2
     module procedure sp_number__allocate__3
     module procedure sp_number__allocate__4
     module procedure dp_number__allocate__1
     module procedure dp_number__allocate__2
     module procedure dp_number__allocate__3
     module procedure dp_number__allocate__4
  end interface number__allocate

  interface number__deallocate
     module procedure sp_number__deallocate__1
     module procedure sp_number__deallocate__2
     module procedure dp_number__deallocate__1
     module procedure dp_number__deallocate__2
  end interface number__deallocate

  interface number__get_v
     module procedure sp_number__get_v
     module procedure dp_number__get_v
  end interface number__get_v

  interface number__get_dv
     module procedure sp_number__get_dv
     module procedure dp_number__get_dv
  end interface number__get_dv
  
  !> Sets the value ('x%v') of a 'number'.
  !! @author Filippo Monari
  !! @param[inout] x 'number'
  !! @param[in] v real or real(:), new value 
  interface number__set_v
     module procedure sp_number__set_v__1
     module procedure sp_number__set_v__2
     module procedure sp_number__set_v__3
     module procedure dp_number__set_v__1
     module procedure dp_number__set_v__2
     module procedure dp_number__set_v__3
  end interface number__set_v

  !> Sets the value of the gradient ('x%dx') of a 'number'.
  !! @author Filippo Monari
  !! @param[inout] x class(number)
  !! @param[in] dv real or real(:), new value
  interface number__set_dv
     module procedure sp_number__set_dv__1
     module procedure sp_number__set_dv__2
     module procedure dp_number__set_dv__1
     module procedure dp_number__set_dv__2
  end interface number__set_dv

  ! interface number__take_slice
  !    module procedure sp_number__take_slice
  !    module procedure dp_number__take_slice
  ! end interface number__take_slice

  !> Sets the value of a 'number' slice.
  !! @author Filippo Monari
  !! @param[inout] x 'number'
  !! @param[in] v real or real(:), new value 
  interface number__set_slice_v
     module procedure sp_number__set_slice_v__1
     module procedure sp_number__set_slice_v__2
     module procedure dp_number__set_slice_v__1
     module procedure dp_number__set_slice_v__2
  end interface number__set_slice_v

  !> Sets the value of the gradient of a 'number' slice.
  !! @author Filippo Monari
  !! @param[inout] x class(number)
  !! @param[in] dv real or real(:), new value
  interface number__set_slice_dv
     module procedure sp_number__set_slice_dv__1
     module procedure sp_number__set_slice_dv__2
     module procedure dp_number__set_slice_dv__1
     module procedure dp_number__set_slice_dv__2
  end interface number__set_slice_dv

  ! interface number__take_flat_slice
  !    module procedure sp_number__take_flat_slice
  !    module procedure dp_number__take_flat_slice
  ! end interface number__take_flat_slice

  !> Sets the value of a 'number' flat slice.
  !! @author Filippo Monari
  !! @param[inout] x 'number'
  !! @param[in] v real or real(:), new value 
  interface number__set_flat_slice_v
     module procedure sp_number__set_flat_slice_v__1
     module procedure sp_number__set_flat_slice_v__2
     module procedure dp_number__set_flat_slice_v__1
     module procedure dp_number__set_flat_slice_v__2
  end interface number__set_flat_slice_v

  !> Sets the value of the gradient of a 'number' flat slice.
  !! @author Filippo Monari
  !! @param[inout] x class(number)
  !! @param[in] dv real or real(:), new value
  interface number__set_flat_slice_dv
     module procedure sp_number__set_flat_slice_dv__1
     module procedure sp_number__set_flat_slice_dv__2
     module procedure dp_number__set_flat_slice_dv__1
     module procedure dp_number__set_flat_slice_dv__2
  end interface number__set_flat_slice_dv

  interface number__take_contiguous_slice
     module procedure sp_number__take_contiguous_slice
     module procedure dp_number__take_contiguous_slice
  end interface number__take_contiguous_slice

  !> Returns the shape of the number with higher rank.
  !! @author Filippo Monari
  !! @param[in] x1,x2 'numbers'
  !! @param[in] shp1 integer vector, shape of x1 (instead of x1 itself).
  interface higher_shape
     module procedure higher_shape__1
     module procedure higher_shape__2
  end interface higher_shape

  !> Returns the shape of the number with lower rank.
  !! @author Filippo Monari
  !! @param[in] x1,x2 'numbers'
  !! @param[in] shp1 integer vector, shape of x1 (instead of x1 itself).
  interface lower_shape
     module procedure lower_shape__1
     module procedure lower_shape__2
  end interface lower_shape

  interface number__inlock
     module procedure sp_number__inlock
     module procedure dp_number__inlock
  end interface number__inlock

  interface number__inlock_free
     module procedure sp_number__inlock_free
     module procedure dp_number__inlock_free
  end interface number__inlock_free

  interface number__set_nd
     module procedure sp_number__set_nd
     module procedure dp_number__set_nd
  end interface number__set_nd

  interface number__get_nd
     module procedure sp_number__get_nd
     module procedure dp_number__get_nd
  end interface number__get_nd

  interface number__reset_nd
     module procedure sp_number__reset_nd
     module procedure dp_number__reset_nd
  end interface number__reset_nd

  interface number__make_reshape
     module procedure sp_number__make_reshape
     module procedure dp_number__make_reshape
  end interface number__make_reshape

  interface number__make_drop_shape
     module procedure sp_number__make_drop_shape
     module procedure dp_number__make_drop_shape
  end interface number__make_drop_shape

  interface number__make_bind
     module procedure sp_number__make_bind
     module procedure dp_number__make_bind
  end interface number__make_bind
  
contains

  !> @defgroup number_utils_ Utility Procedures for Numbers
  !! @author Filippo Monari
  
  include "./numbers_utils/allocate/number.f95"
  include "./numbers_utils/allocate/sp_number.f95"
  include "./numbers_utils/allocate/dp_number.f95"
  include "./numbers_utils/bind/sp_number.f95"
  include "./numbers_utils/bind/dp_number.f95"
  include "./numbers_utils/compare_shape/number.f95"
  include "./numbers_utils/deallocate/sp_number.f95"
  include "./numbers_utils/deallocate/dp_number.f95"
  include "./numbers_utils/get/sp_number.f95"
  include "./numbers_utils/get/dp_number.f95"
  include "./numbers_utils/node/sp_number.f95"
  include "./numbers_utils/node/dp_number.f95"
  include "./numbers_utils/reshape/sp_number.f95"
  include "./numbers_utils/reshape/dp_number.f95"
  include "./numbers_utils/set/sp_number.f95"
  include "./numbers_utils/set/dp_number.f95"
  include "./numbers_utils/slice/sp_number.f95"
  include "./numbers_utils/slice/dp_number.f95"

 !> @}
 
end module numbers_utils
