!> Appends a new 'node' to the NODES_ register
!! @param[in] op integer, operator id
!! @param[in] in number(:), array of input 'numbers'
!! @param[in] flg integer(:), flags and indexed needed by the operator
!! @param[in] out number(:), array of output 'numbers'
subroutine xp_node__append (op, in, out, ipar, rpar, s1, s2)
  implicit none
  integer, intent(in) :: op
  type(xp_number), intent(in) :: in(:), out(:)
  integer, intent(in) :: ipar(:), s1(:), s2(:)
  real(kind=xp_), intent(in) :: rpar(:) 
  call do_safe_within("xp_node__append", mod_nodes_name_, private_append)
contains
  subroutine private_append
    integer :: i
    if (graphi_is_set(1._xp_) .or. is_feed(op)) then
       call assert(allocated(xp_NODES_), err_notAlloc_, 'NODES_')
       call next_node(1._xp_)
       call node__allocate(xp_NODES_(get_nodei()), op, in, out, ipar, rpar, s1, s2)
    else
       if (size(out) > 0) then
          do i = 1, size(out)
             call number__set_nd(out(i), -1)
          end do
       end if
    end if
  end subroutine private_append
end subroutine xp_node__append

!> Sets the current graph index GRAPHi_.
!! @author Filippo Monari
!! @param[in] i integer, optional, if present GRAPHi_ = i else GRAPHi_ = next free slot
subroutine xp_graph__open__1 (dtyp, withdx)
  implicit none
  logical, intent(in), optional :: withdx
  real(kind=xp_), intent(in) :: dtyp
  call do_safe_within('xp_graph__open__1', mod_nodes_name_, private_open)
contains
  subroutine private_open
    call assert(.not. graphi_is_set(dtyp), err_alreadyAlloc_, "GRAPHi_")
    if (err_free()) then
       call next_graph(dtyp)
       if (err_free()) then
          xp_GRAPH_ => xp_GRAPHS_(GRAPHi_)
          call alloc(xp_GRAPH_%nodes, [integer::], "xp_GRAPH_%nodes")
          call alloc(xp_GRAPH_%posts, [integer::], "xp_GRAPH_%posts")
          xp_GRAPH_%id = GRAPHi_
       end if
    end if
    if (present(withdx)) then
       WITHDX_ = withdx
       xp_GRAPH_%withdx = withdx
    end if
  end subroutine private_open
end subroutine xp_graph__open__1

subroutine xp_graph__open__2 (g)
  implicit none
  type(xp_graph), intent(in) :: g
  call do_safe_within('xp_graph__open__2', mod_nodes_name_, private_open)
contains
  subroutine private_open
    call assert(is_allocated(g), err_notAlloc_, "g")
    if (err_free()) then
       WITHDX_ = g%withdx
       xp_GRAPH_ => xp_GRAPHS_(g%id)
       GRAPHi_ = g%id
    end if
  end subroutine private_open
end subroutine xp_graph__open__2
  
!> Reset 'GRAPHi_' and 'GRPAH_', so that no 'graph' is on the stack anymore
!! @author Filippo Monari
subroutine xp_graph__close (dtyp)
  implicit none
  real(kind=xp_), intent(in) :: dtyp
  call do_safe_within("graph__close", mod_nodes_name_, private_close)
contains
  subroutine private_close
    call assert(graphi_is_set(dtyp), err_generic_, "no graph is open.")
    if (err_free()) call reset_graphi(dtyp)
  end subroutine private_close
end subroutine xp_graph__close

!> Appends the node on the stack to the current graph and flushes it.
!! @author Filippo Monari
subroutine xp_graph__append (dtyp)
  implicit none
  real(kind=xp_), intent(in) :: dtyp
  call do_safe_within('graph__append', mod_nodes_name_, private_append)
contains
  subroutine private_append
    integer :: i
    if (graphi_is_set(dtyp)) then
       call assert(associated(xp_GRAPH_), err_notAssoc_, 'GRAPH_')
       if (within_posts()) then
          call append_to_array(xp_GRAPH_%posts, flush_nodei(), 'GRAPH_%posts')
       else
          call append_to_array(xp_GRAPH_%nodes, flush_nodei(), 'GRAPH_%ndes')
       end if
    end if
  end subroutine private_append
end subroutine xp_graph__append

