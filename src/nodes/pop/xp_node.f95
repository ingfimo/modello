!> Pops (removes) a 'node' from the 'NODES_' register.
!! Updates all the other registers accordingly.
!! @details Nodes are poped through their id. This
!! has been deemed simple, since this function has not to be called,
!! directly, but by the corresponding routined popping 'numbers'
!! and graphs.
!! @author Filippo Monari
!! @param[in] i integer, 'node' index (position in 'NODES_')
subroutine xp_node__pop (i, dtyp)
  implicit none
  integer, intent(in) :: i
  real(kind=xp_), intent(in) :: dtyp
  call do_safe_within("node__pop", mod_nodes_name_, private_do)
contains
  subroutine private_do
    call assert(allocated(xp_NODES_), err_notAlloc_, 'xp_NODES_')
    if (err_free()) call assert(0 < i .and. i <= size(xp_NODES_), err_oorng_, 'i')
    if (err_free()) call assert(is_allocated(xp_NODES_(i)), err_notAlloc_, 'x')
    call err_safe(private_pop)
  end subroutine private_do
  subroutine private_pop
    call node__update_inlock(xp_NODES_(i), '-') !< updates the INLOCK_ register
    call node__reset_number(xp_NODES_(i))       !< updates the NUMNDS_ register
    call node__pack_graph(xp_NODES_(i))         !< remove the node index from the 'graph'
    call node__deallocate(xp_NODES_(i))
    if (err_free()) NODEINDEX_ = [NODEINDEX_, i]
  end subroutine private_pop
end subroutine xp_node__pop

!> Pops (removes) a 'graph' from 'GRAPHS_'. All its 'nodes' are removed as well
!! from 'NODES'. The registers are update accordingly.
!! @author Filippo Monari
!! @param[in] gi integer, 'graph' index
subroutine xp_graph__pop (g)
  implicit none
  type(xp_graph), intent(inout), pointer :: g
      real(kind=xp_) :: dtyp
  call do_safe_within('xp_graph__pop', mod_nodes_name_, private_do)
contains
  subroutine private_do
    if (graphi_is_set(dtyp)) then
       call assert(g%id /= get_graphi(dtyp), err_generic_, 'g is open.')
    end if
    call err_safe(private_pop)
  end subroutine private_do
  recursive subroutine private_pop
    integer :: ndi, gi
    gi = g%id
    nullify(g)
    do while(get_size(xp_GRAPHS_(gi)) > 0)
       ndi = xp_GRAPHS_(gi)%nodes(1)
       call node__pop(ndi, dtyp)
       if (.not. err_free()) exit
    end do
    call dealloc(xp_GRAPHS_(gi)%nodes, 'GRAPHS_(gi)%nodes')
    if (err_free()) GRAPHINDEX_ = [GRAPHINDEX_, gi]
  end subroutine private_pop
end subroutine xp_graph__pop


