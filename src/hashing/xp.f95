function xp_binary_simhash__hash (x, B1, B2) result(h)
  implicit none
  real(kind=xp_), intent(in) :: x(:)
  type(hashtab), intent(in) :: B1, B2
  integer :: h, i
  real(kind=xp_) :: k
  h = 0
  do i = 0, size(B1%t, 2) - 1
     k = 0
     if (hashtab__contains(B1, i)) k = k + sum(x(hashtab__get(B1, i)))
     if (hashtab__contains(B2, i)) k = k - sum(x(hashtab__get(B2, i)))
     if (k > 0) h = h + 2**(i)
  end do
end function xp_binary_simhash__hash

subroutine xp_binary_simhash__fill_ht (X, B1, B2, byrows, HT)
  implicit none
  real(kind=xp_), intent(in) :: X(:,:)
  type(hashtab), intent(in) :: B1, B2
  logical, intent(in) :: byrows
  type(hashtab), intent(inout) :: HT
  integer :: i, j, h
  if (byrows) then
     do i = 1, size(X, 1)
        h = binary_simhash__hash(X(i,:), B1, B2)
        call hashtab__put(HT, h, i)
     end do
  else 
     do i = 1, size(X, 2)
        h = binary_simhash__hash(X(:,i), B1, B2)
        call hashtab__put(HT, h, i)
     end do
  end if
end subroutine xp_binary_simhash__fill_ht

subroutine xp_binary_simhash__sample (x, B1, B2, HT, m, n, s)
  implicit none
  real(kind=xp_), intent(in) :: x(:)
  type(hashtab), intent(in) :: B1(:), B2(:), HT(:)
  integer, intent(in) :: m, n
  integer, intent(out), allocatable :: s(:)
  integer :: i(1), h, z(n), k
  z = 0
  k = 0
  do while (sum(z) < m .and. k < size(HT)) 
     call rand_int(i, 1, size(HT), 1._xp_)
     h = xp_binary_simhash__hash(x, B1(i(1)), B2(i(1)))
     if (hashtab__contains(HT(i(1)), h)) then
        z(hashtab__get(HT(i(1)), h)) = 1
     end if
     k = k + 1
  end do
  allocate(s, source=which(z > 0))
end subroutine xp_binary_simhash__sample
  
  
