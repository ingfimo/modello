subroutine binary_simhash__basis (HT1, HT2, n)
  implicit none
  type(hashtab), intent(inout) :: HT1, HT2
  integer, intent(in) :: n
  real(kind=sp_), parameter :: a = 1._sp_ / 3._sp_
  real(kind=sp_), parameter :: b = 2 * a
  real(kind=sp_) :: p(1)
  integer :: i, j
  call hashtab__clear(HT1)
  call hashtab__clear(HT2)
  do j = 1, size(HT1%t, 2)
     do i = 1, n
        call unif_rand(p)
        if (p(1) < a) then
           call hashtab__put(HT1, j, i) 
        else if (p(1) >= b) then
           call hashtab__put(HT1, j, i)
        end if
     end do
  end do
end subroutine binary_simhash__basis

