  interface binary_simhash__hash
     module procedure sp_binary_simhash__hash
     module procedure dp_binary_simhash__hash
  end interface binary_simhash__hash

  interface binary_simhash__fill_ht
     module procedure sp_binary_simhash__fill_ht
     module procedure dp_binary_simhash__fill_ht
  end interface binary_simhash__fill_ht

  interface binary_simhash__sample
     module procedure sp_binary_simhash__sample
     module procedure dp_binary_simhash__sample
  end interface binary_simhash__sample
