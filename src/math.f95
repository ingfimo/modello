!> @defgroup math_ Math
!! @{
!! @defgroup math_functions_ Functions
!! @defgroup math_functions_private_ Private Functions
!! @}


module math

  use omp_lib
  use env
  use errwarn

  implicit none

  character(len=*), parameter :: mod_math_name_ = 'math'

  include "./math/externals/interfaces.f95"
  include "./math/functions/interfaces.f95"
  include "./math/activations/interfaces.f95"
  include "./math/matrix/interfaces.f95"
  include "./math/objectives/interfaces.f95"
  include "./math/reductions/interfaces.f95"
  include "./math/kernels/interfaces.f95"
  include "./math/stats/interfaces.f95"
  include "./math/random/interfaces.f95"

contains

  include "./math/functions/sp.f95"
  include "./math/functions/dp.f95"

  include "./math/activations/sp.f95"
  include "./math/activations/dp.f95"

  include "./math/reductions/sp.f95"
  include "./math/reductions/dp.f95"

  include "./math/matrix/sp.f95"
  include "./math/matrix/dp.f95"

  include "./math/kernels/sp.f95"
  include "./math/kernels/dp.f95"

  include "./math/stats/sp.f95"
  include "./math/stats/dp.f95"

  include "./math/objectives/sp.f95"
  include "./math/objectives/dp.f95"

  include "./math/random/sp.f95"
  include "./math/random/dp.f95"

end module math
!> @}

