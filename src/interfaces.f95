!> @defgroup api_ Modello API
!! @{

module interfaces

  use env
  use registers
  use registers_utils
  use errwarn
  use numbers_utils
  use numbers
  use numbers_math
  use nodes_utils
  use nodes
  use optimisers
  use optim_utils
  use iso_c_binding

  character(len=*), parameter :: mod_interfaces_name_ = 'interfaces'  

  interface intrf_f__opt__sgd_step__0
     module procedure sp_intrf_f__opt__sgd_step__0
     module procedure dp_intrf_f__opt__sgd_step__0
  end interface intrf_f__opt__sgd_step__0

  interface intrf_f__opt__sgdwm_step__0
     module procedure sp_intrf_f__opt__sgdwm_step__0
     module procedure dp_intrf_f__opt__sgdwm_step__0
  end interface intrf_f__opt__sgdwm_step__0

  interface intrf_f__opt__adam_step__0
     module procedure sp_intrf_f__opt__adam_step__0
     module procedure dp_intrf_f__opt__adam_step__0
  end interface intrf_f__opt__adam_step__0
  
contains

  include "./interfaces/interfaces_templates.f95"
  
  include "./interfaces/sp_interfaces.f95"
  include "./interfaces/dp_interfaces.f95"

  !> @defgroup api__numbers_methods_ API for Numbers' Methods
  !! @{
  include "./interfaces/interfaces_numbers.f95"
  !> @}

  !> @defgroup api__nodes_methods_ API for Nodes' Methods
  !> @{
  include "./interfaces/interfaces_nodes.f95"
  !> @}

  !> @defgroup api__optim_methods_ API for Optimisers' Methods
  !! @{
  include "./interfaces/interfaces_optim.f95"
  !> @}
  
  !> Stores the id of the output (ans) 'number' if no error was found.
  !! @param[in] ans output 'number'
  !! @param[out] idout c_int, id of the output 'number'
  subroutine intrf_f__ans_id (ans, idout)
    implicit none
    class(number), intent(in) :: ans
    integer(kind=c_int), intent(out) :: idout
    if (err_free()) idout = ans%id
  end subroutine intrf_f__ans_id

  !> @defgroup api__numbers_math_modifiers_ API for Modifiers
  !! @{
  include "./interfaces/interfaces_numbers_math_modifiers.f95"
  !> @}

  !> @defgroup api__numbers_math_unary_ API for Unary Operators
  !! @{
  include "./interfaces/interfaces_numbers_math_unary.f95"
  !> @}

  !> @defgroup api__numbers_math_binary_ API for Binary Operators
  !! @{
  include "./interfaces/interfaces_numbers_math_binary.f95"
  !> @}

  !> @defgroup api__numbers_math_activations_ API for Activation Functions
  !! @{
  include "./interfaces/interfaces_numbers_math_activations.f95"
  !> @}

  !> @defgroup api__numbers_math_matrix_ API for Matrix Operators
  !! @{
  include "./interfaces/interfaces_numbers_math_matrix.f95"
  !> @}

  !> @defgroup api__numbers_math_reductons_ API for Reduction Operators
  !! @{
  include "./interfaces/interfaces_numbers_math_reductions.f95"
  !> @}

  !> @defgroup api__numbers_math_kernels_ API for Kernels Operators
  !! @{
  include "./interfaces/interfaces_numbers_math_kernels.f95"
  !> @}

  !> @defgroup api__number_math_stats_ API for Stats Operators
  !! @{
  include "./interfaces/interfaces_numbers_math_stats.f95"
  !> @}
  
  !> @defgroup api__numbers_math_objectives_ API for Objective Functions
  !! @{
  include "./interfaces/interfaces_numbers_math_objectives.f95"
  !> @}

  !> @defgroup api__numbers_math_regularisation_ API for Regularisation Operators
  !! @{
  include "./interfaces/interfaces_numbers_math_regularisation.f95"
  !> @}

  !> @defgroup api__registers_ API for Register Procedures
  !! @{
  include "./interfaces/interfaces_registers.f95"
  !> @}

  include "./interfaces/interfaces_numbers_math_convolution.f95"
  
end module interfaces

!> @}
