!> Utility procedures to manipulate elementar entities.
!! @author Filippo Monari
module utils

  use env
  use errwarn
  use types
  use math, only: unif_rand
  
  private
  public &
       alloc, &
       dealloc, &
       dealloc_element, &
       append_to_array, &
       iminloc, &
       imaxloc, &
       swap, &
       bind_lag, &
       bind_indexes, &
       bind_fill, &
       shape_to_flat, &
       flat_to_shape, &
       get_slice_flat_indexes, &
       get_slice_shape_indexes, &
       get_slices_along_dim, &
       slice_shape, &
       unique, &
       reshape_bcast, &
       dpEq, &
       drop, &
       which, &
       which_eq, &
       which_neq, &
       sort_index, &
       find_slices, &
       dilated_size, &
       dilated_grid_1d, &
       dilated_grid_2d, &
       dilated_grid_3d, &
       conv1d_lout, &
       offset_slice, &
       kernel_slices_1d, &
       kernel_slices_2d, &
       kernel_slices_3d
  
  character(len=*), parameter :: mod_utils_name_ = 'utils'

  include "./utils/alloc/interfaces.f95"
  
  include "./utils/dealloc/interfaces.f95"

  include "./utils/append/interfaces.f95"
     
  
  !> Find index of the min in an array
  !! @author Filippo Monari
  !! @param arr array
  interface iminloc
     module procedure int__iminloc__1
     module procedure int__iminloc__2
  end interface iminloc

  !> Find the index of the max in an array
  !! @author Filippo Monari
  !! @param arr array
  interface imaxloc
     module procedure int__imaxloc__1
     module procedure int__imaxloc__2
  end interface imaxloc

  !> Swap real/integer scalar/array, meaning
  !! a goes into b and b goes into a.
  !! @author Filippo Monari
  !! @param{a, b} arrays
  interface swap
     module procedure int__swap__1
     module procedure sp__swap__1
     module procedure sp__swap__2
     module procedure dp__swap__1
     module procedure dp__swap__2
  end interface swap

  interface swap_index_order
     module procedure int__swap_index_order
  end interface swap_index_order

  interface sort_index
     module procedure int__sort_index
  end interface sort_index

  interface bind_lag
     module procedure int__bind_lag
  end interface bind_lag

  interface bind_indexes
     module procedure int__bind_indexes
  end interface bind_indexes

  interface bind_fill
     module procedure sp__bind_fill
     module procedure dp__bind_fill
  end interface bind_fill

  interface shape_to_flat
     module procedure int__shape_to_flat__1
     module procedure int__shape_to_flat__2
     module procedure int__shape_to_flat__3
  end interface shape_to_flat

  interface flat_to_shape
     module procedure int__flat_to_shape
  end interface flat_to_shape

  interface get_slice_flat_indexes
     module procedure int__get_slice_flat_indexes
  end interface get_slice_flat_indexes

  interface get_slice_shape_indexes
     module procedure int__get_slice_shape_indexes
  end interface get_slice_shape_indexes

  interface get_slices_along_dim
     module procedure int__get_slices_along_dim
  end interface get_slices_along_dim

  ! interface slice_index
  !    module procedure int__slice_index
  ! end interface slice_index

  ! interface slice_indexes
  !    module procedure int__slice_indexes
  ! end interface slice_indexes

  ! interface slice_dim_indexes
  !    module procedure int__slice_dim_indexes
  ! end interface slice_dim_indexes

  ! interface slice_ndim_indexes
  !    module procedure int__slice_ndim_indexes
  ! end interface slice_ndim_indexes

  interface slice_shape
     module procedure int__slice_shape
  end interface slice_shape

  interface reshape_bcast
     module procedure sp__reshape_bcast
     module procedure dp__reshape_bcast
     module procedure logical__reshape_bcast
  end interface reshape_bcast

  interface dilated_size
     module procedure int__dilated_size
  end interface dilated_size

  interface dilated_grid_1d
     module procedure int__dilated_grid_1d
  end interface dilated_grid_1d

  interface dilated_grid_2d
     module procedure int__dilated_grid_2d
  end interface dilated_grid_2d

  interface dilated_grid_3d
     module procedure int__dilated_grid_3d
  end interface dilated_grid_3d
  
contains

  !> @defgroup utils_ Genreal Utility Procedures
  !! @author Filippo Monari
  !! @{
  
  include "./utils/alloc/int.f95"
  include "./utils/alloc/sp.f95"
  include "./utils/alloc/dp.f95"
  include "./utils/alloc/char.f95"

  include "./utils/append/int.f95"
  include "./utils/append/sp.f95"
  include "./utils/append/dp.f95"

  include "./utils/bind/int.f95"
  include "./utils/bind/sp.f95"
  include "./utils/bind/dp.f95"

  include "./utils/dealloc/int.f95"
  include "./utils/dealloc/sp.f95"
  include "./utils/dealloc/dp.f95"
  include "./utils/dealloc/char.f95"

  include "./utils/dealloc_element/int.f95"
  include "./utils/dealloc_element/sp.f95"
  include "./utils/dealloc_element/dp.f95"

  include "./utils/iloc/int.f95"

  include "./utils/slice/int.f95"

  include "./utils/sort/int.f95"
  
  include "./utils/swap/int.f95"
  include "./utils/swap/sp.f95"
  include "./utils/swap/dp.f95"

  include "./utils/reshape/sp.f95"
  include "./utils/reshape/dp.f95"
  include "./utils/reshape/logical.f95"

  !> @defgroup utils__misc Miscellaneous Utility Procedures
  !! @author Filippo Monari

  !> Returns the unique elements in a integer array of rank 1.
  !! @author Filippo Monari
  !! @param[in] x integer array of rank 1
  recursive function unique (x) result(ans)
    implicit none
    integer, intent(in) :: x(:)
    integer, allocatable :: ans(:)
    if (size(x) == 0) then
       ans = [integer::]
    else
       ans = [x(1), unique(pack(x, x /= x(1)))]
    end if
  end function unique

  !> Compares to double precision scalar for equality
  !! @author Filippo Monari
  !! @param[in] a,b double precsion scalars
  pure function dpEq (a, b) result(ans)
    implicit none
    real(kind=dp_), intent(in) :: a, b
    logical :: ans
    ans = a - b < eps_dp_
  end function dpEq

  !> Drops the ith element of an array
  !! @param[in] x integer(:)
  !! @param[in] i integer, index of the element to drop
  function drop (x, i) result(ans)
    implicit none
    integer, intent(in) :: x(:), i
    integer :: ans(size(x) - 1), j
    call do_safe_within("drop", mod_utils_name_, private_drop)
  contains
    subroutine private_drop
      call assert(i > 0 .and. i <= size(x), err_oorng_, "i")
      if (err_free()) then
         if (size(x) == 1) then
            ans = [integer::]
            return
         end if
         if (i == 1) then
            ans = x(2:)
         else if (i == size(x)) then
            ans = x(:(i-1))
         else
            ans = [x(:(i-1)), x(i+1:)]
         end if
      end if
    end subroutine private_drop
  end function drop

  pure function which (x) result(ans)
    implicit none
    logical, intent(in) :: x(:)
    integer, allocatable :: ans(:)
    integer :: i
    ans = [(pack([(i, i=1, size(x))], x))]
  end function which
  
  !>  Returns the indexes of the element of x that are equal to i
  !! @params[in] x integer(:)
  !! @params[in] i integer
  pure function which_eq (x, i) result(ans)
    implicit none
    integer, intent(in) :: x(:), i
    integer, allocatable :: ans(:)
    integer :: j
    ans = [pack([(j, j=1, size(x))], x == i)]
  end function which_eq

  !>  Returns the indexes of the element of x that are not equal to i
  !! @params[in] x integer(:)
  !! @params[in] i integer
  pure function which_neq (x, i) result(ans)
    implicit none
    integer, intent(in) :: x(:), i
    integer, allocatable :: ans(:)
    integer :: j
    ans = [pack([(j, j=1, size(x))], x /= i)]
  end function which_neq

  !> @}
  !> @}

end module utils
    
    
