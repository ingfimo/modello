module registers

  use env
  use types

  implicit none

  character(len=*), parameter :: mod_registers_name_ = "registers" 

  !> @defgroup registers_ Registers
  !! @author Filippo Monari

  !> @defgroup registers__errors_ Error Registers
  !! The following registers are used to track and handle errors.
  !! @{
  type(prc_call), target :: PRC_CALL_HEAD_ !< head of the call stack
  type(prc_call), pointer :: PRC_CALL_NEXT_ => PRC_CALL_HEAD_ !< next stack position
  integer :: INFO_ = 0             !< error flag (error => /= 0)
  logical :: ISCRITICAL_ = .false. !< critical error flag
  integer :: WINFO_ = 0            !< warning flag (warning => /=0)
  !> @}
  
  !> @defgroup registers_numbersRegisters Numbers' Registers
  !! The following registers are used to store and manage the 'numbers' in the calculations.
  !! All these three arrays mus be allocated woth the same length as a 'number' id identify the
  !! a slot in each of three registers.
  !! @{
  type(sp_number), allocatable, target :: sp_NUMBERS_(:)
  type(dp_number), allocatable, target :: dp_NUMBERS_(:)
  integer, allocatable :: NUMINDEX_(:)             !< array of indexes indicating the slots available in the NUMBERS_ register
  integer, allocatable :: INLOCKS_(:) !< array coounting for each number how many times is used as calculation input
  integer, allocatable :: NUMNDS_(:) !< array storing the index of the node of which a certain number is output (if any)
  !> @}
  
  !> @defgroup registers_nodesRegisters_ Nodes' Registers
  !! The following registers are used to keep track and manage nodes representing calculation steps.
  !! 'NODES_' and 'NODEGRPHS_' must be allocated with the same length since a slot in the former must a
  !! a corresponding slot in the latter.
  !! @{
  type(sp_node), allocatable, target :: sp_NODES_(:)
  type(dp_node), allocatable, target :: dp_NODES_(:)
  integer, allocatable :: NODEINDEX_(:)        !< array in indexes indicating the slota available in the NODES_ register
  integer, allocatable :: NODEGRPHS_(:) !< array storgint he index of the graph including a certain node
  integer :: NODEi_ = 0 !< stores the index (position in 'NODES_') of the node corrently on the stack
  !> @}
  
  !> @defgroup registers_graphs_ Graphs' Registers.
  !! The following registers keep track and manage 'graphs' representing calculation flows.
  !! @{
  !type(graph), allocatable, target :: GRAPHS_(:) !< array stroring all the 'graphs' [DEPRECATED]
  type(sp_graph), allocatable, target :: sp_GRAPHS_(:)
  type(dp_graph), allocatable, target :: dp_GRAPHS_(:)
  integer :: GRAPHi_ = 0 !< stores the index (position in 'GRAPHS_') of the 'graph' currently on the stack
  integer, allocatable :: GRAPHINDEX_(:) !< array of indexes indicating the slota available in the GRAPHS_ register
  type(sp_graph), pointer :: sp_GRAPH_ !< pointer to the 'graph' currently on the stack (=> GRAPHS_(GRAPHi_))
  type(dp_graph), pointer :: dp_GRAPH_
  logical :: WITHDX_ = .true.    !< flag indicating if the graph allows gradients (if .false. all the 'numbers' within the grpah will have no gradient)
  logical :: INPOSTS_ = .false.
  !> @}

  !> @defgroup registers_optimisation_ Optimisation Registers
  !! @{
  !> sp_opt
  type(sp_opt), allocatable, target :: sp_OPTS_(:)
  !> dp_opt
  type(dp_opt), allocatable, target :: dp_OPTS_(:)
  integer, allocatable :: OPTIMINDEX_(:)       !< array of indexes indicating the available slots in the OPTIMS_ register
  !> @}

  type(hashtab), allocatable, target :: HTS_(:)
  integer, allocatable :: HTSINDEX_(:)

  integer :: MODE_ = training_mode_
  integer :: DTYP_ = dp_

  !> @}

end module registers
