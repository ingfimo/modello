module nodes
  use env
  use types
  use registers, only: &
       sp_NUMBERS_, &       
       dp_NUMBERS_, &
       sp_NODES_, &
       dp_NODES_, &
       NODEINDEX_, &
       NODEGRPHS_, &
       NODEi_, &
       sp_GRAPHS_, &
       dp_GRAPHS_, &
       GRAPHINDEX_, &
       GRAPHi_, &
       sp_GRAPH_, &
       dp_GRAPH_, &
       WITHDX_
  use registers_utils
  use errwarn
  use utils
  use nodes_utils
  use numbers_utils
  use node_operators

  implicit none

  character(len=*), parameter :: mod_nodes_name_ = 'nodes'

  interface node__append
     module procedure sp_node__append
     module procedure dp_node__append
  end interface node__append

  interface graph__open
     module procedure sp_graph__open__1
     module procedure dp_graph__open__1
     module procedure sp_graph__open__2
     module procedure dp_graph__open__2
  end interface graph__open

  interface graph__close
     module procedure sp_graph__close
     module procedure dp_graph__close
  end interface graph__close

  interface graph__append
     module procedure sp_graph__append
     module procedure dp_graph__append
  end interface graph__append

  interface node__pop
     module procedure sp_node__pop
     module procedure dp_node__pop
  end interface node__pop

  interface graph__pop
     module procedure sp_graph__pop
     module procedure dp_graph__pop
  end interface graph__pop

contains

  include "./nodes/append/sp_node.f95"
  include "./nodes/append/dp_node.f95"

  include "./nodes/pop/sp_node.f95"
  include "./nodes/pop/dp_node.f95"
               
end module nodes
