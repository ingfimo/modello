!> Deallocates a 'number'
!! @author Filippo Monari
!! @param[inout] x 'number' to deallocate.
subroutine xp_number__deallocate__1 (x)
  implicit none
  type(xp_number), intent(inout) :: x
  call do_safe_within('number__deallocate__1', mod_numbers_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call dealloc_element(x%v, number__get_init(x, 'v'), 'v')
    call dealloc_element(x%dv, number__get_init(x, 'dv'), 'dv')
    call dealloc(x%shp, "x%shp")
    x%id = 0
    x%init = init_null_
    call assert(is_deallocated(x), err_dealloc_, "x")
  end subroutine private_deallocate
end subroutine xp_number__deallocate__1

!> Deallocates a 'number' array of dimension (:)
!! @param[inout] x number(:), allocatable
!! @param[in] xname character, variable name (for error reporting)
subroutine xp_number__deallocate__2 (x)
  implicit none
  type(xp_number), intent(inout), allocatable :: x(:)
  call do_safe_within("number__deallocate__2", mod_numbers_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    integer :: info
    call assert(allocated(x), err_notAlloc_, "x")
    if (err_free()) then
       deallocate(x, stat=info)
       call assert(info == 0, err_dealloc_, "x")
    end if
  end subroutine private_deallocate
end subroutine xp_number__deallocate__2
