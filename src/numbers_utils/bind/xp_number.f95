!> Binds two numbers togheter along the given dimension.
!! @author Filippo Monari
!! @param[inout] x1 'number' storing the bind
!! @param[in] x2,x3 'numbers' to bind together
!! @param[in] k integer, dimension along which realise the bind 
subroutine xp_number__make_bind (x1, x2, x3, k)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2, x3
  integer, intent(in) :: k
  call do_safe_within('number__make_bind', mod_numbers_utils_name_, private_make_bind)
contains
  subroutine private_make_bind
    call assert(is_deallocated(x1), err_alreadyInit_, 'x1')
    call assert(is_allocated(x2), err_notAlloc_, 'x2')
    call assert(is_allocated(x3), err_notAlloc_, 'x3')
    call assert(get_rank(x2) == get_rank(x3), err_wrngSz_, 'x2 or x3')
    call assert(has_dx(x2) .eqv. has_dx(x3), err_generic_, 'has_dx(x2) /= has_dx(x3)')
    call assert(all(drop(x2%shp, k) == drop(x3%shp, k)), err_wrngSz_, 'x2 or x3')
    call err_safe(private_allocate)
    call err_safe(private_bind)
  end subroutine private_make_bind
  subroutine private_bind
    call bind_fill(x1%v, x2%v, x3%v, x2%shp, x3%shp, k, .false.)
  end subroutine private_bind
  subroutine private_allocate
    integer :: shp(get_rank(x2))
    shp = 0
    shp(k) = x3%shp(k)
    call number__allocate(x1, x2%shp + shp, has_dx(x2))
  end subroutine private_allocate
end subroutine xp_number__make_bind
