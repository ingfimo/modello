!> Retrieves the value of a 'number'.
!! @author Filippo Monari
!! @param[in] x 'number'
!! @param[out] v real(:), allocatable, variable wherein to store the value
subroutine xp_number__get_v (x, v)
  implicit none
  type(xp_number), intent(in) :: x
  real(kind=xp_), intent(out), allocatable :: v(:)
  call do_safe_within('number__get_v', mod_numbers_utils_name_, private_get)
contains
  subroutine private_get
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call alloc(v, x%v, 'v')
  end subroutine private_get
end subroutine xp_number__get_v

!> Retrieves the value of the derivative of 'number'.
!! @author Filippo Monari
!! @param[in] x 'number'
!! @param[out] dv real(:) allocatable, variable wherein to store the value of dx
subroutine xp_number__get_dv (x, dv)
  implicit none
  type(xp_number), intent(in) :: x
  real(kind=xp_), intent(out), allocatable :: dv(:)
  call do_safe_within('number__get_dv', mod_numbers_utils_name_, private_get)
contains
  subroutine private_get
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(has_dx(x), err_generic_, 'x has no dx.')
    call alloc(dv, x%dv, 'dv')
  end subroutine private_get
end subroutine xp_number__get_dv


