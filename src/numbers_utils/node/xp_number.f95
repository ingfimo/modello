!> Increases or decreases the inlock count of a 'number'.
!! @author Filippo Monari
!! @param[in] x 'number'
!! @param[in] m character indicating if to increase or to decrease the inlock count
!! @details
!! If 'm' is '+' then the inlock count is increased.
!! If 'm' is '-' then the inlock count is decreased.
subroutine xp_number__inlock (x, m)
  implicit none
  type(xp_number), intent(in) :: x
  character(len=*), intent(in) :: m
  call do_safe_within('xp_number__inlock', mod_numbers_utils_name_, private_do)
contains
  subroutine private_do
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call err_safe(private_inlock)
  end subroutine private_do
  subroutine private_inlock
    select case (m)
    case ('+')
       INLOCKS_(x%id) = INLOCKS_(x%id) + 1
    case ('-')
       INLOCKS_(x%id) = INLOCKS_(x%id) - 1
    case default
       call raise_error('m', err_unknwnVal_)
    end select
  end subroutine private_inlock
end subroutine xp_number__inlock

!> Checks if for a 'number' the inlock if free (i.e. count = 0).
!! @author Filippo Monari
!! @param[in] x 'number'
function xp_number__inlock_free (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  logical :: ans
  call do_safe_within('xp_number__inlock_free', mod_numbers_utils_name_, private_inlock_free)
contains
  subroutine private_inlock_free
    call assert(is_allocated(x), err_notAlloc_, 'x')
    if (err_free()) ans = INLOCKS_(x%id) == 0
  end subroutine private_inlock_free
end function xp_number__inlock_free

!> Sets the id corresponding to the 'node' generating the given 'number'.
!! @author Filippo Monri
!! @param[in] x 'number'
!! @param[in] nd integer, 'node' id
subroutine xp_number__set_nd (x, nd)
  implicit none
  type(xp_number), intent(in) :: x
  integer, intent(in) :: nd
  call do_safe_within('xp_number__set_nd', mod_numbers_utils_name_, private_set_nd)
contains
  subroutine private_set_nd
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(NUMNDS_(x%id) == 0, err_generic_, 'NUMBER_NODES(x%id) /= 0.')
    if (err_free()) NUMNDS_(x%id) = nd
  end subroutine private_set_nd
end subroutine xp_number__set_nd

!> Retrieves the id of the 'node' generating the given 'number'.
!! @author Filippo Monari
!! @param[in] x 'number'
function xp_number__get_nd (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  integer :: ans
  call do_safe_within('xp_number__get_nd', mod_numbers_utils_name_, private_get_nd)
contains
  subroutine private_get_nd
    call assert(is_allocated(x), err_notAlloc_, 'x')
    ans = NUMNDS_(x%id)
  end subroutine private_get_nd
end function xp_number__get_nd

!> Resets (sets to 0) the 'node' id relative to the given 'number'.
!! @author Filippo Monari
!! @param[in] x 'number'
subroutine xp_number__reset_nd (x)
  implicit none
  type(xp_number), intent(in) :: x
  call do_safe_within('xp_number__reset_nd', mod_numbers_utils_name_, private_reset_nd)
contains
  subroutine private_reset_nd
    call assert(is_allocated(x), err_notAlloc_, 'x')
    NUMNDS_(x%id) = 0
  end subroutine private_reset_nd
end subroutine xp_number__reset_nd
