!> Sets the values of a 'number' slice to the given scalar
!! @param[inout] x 'number'
!! @param[in] s integer(:,:), matrix containig the slice indexes
!! @param[in] v real, new value
subroutine xp_number__set_slice_v__1 (x, s, v)
  implicit none
  type(xp_number), intent(inout) :: x
  integer, intent(in) :: s(:,:)
  real(kind=xp_), intent(in) :: v
  call do_safe_within('xp_number__set_slice_v__1', mod_numbers_utils_name_, private_set)
contains
  subroutine private_set
    integer :: indx(size(s, 2))
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(size(s, 1) == get_rank(x), err_wrngArg_, 's')
    call assert(all(minval(s, 2) > 0), err_oorng_, 's')
    call assert(all(maxval(s, 2) <= x%shp), err_oorng_, 's')
    if (err_free()) then
       call get_slice_flat_indexes(s, x%shp, indx)
       x%v(indx) = v
    end if
  end subroutine private_set
end subroutine xp_number__set_slice_v__1

!> Sets the values of a 'number' slice to the given array of values
!! @param[inout] x 'number'
!! @param[in] s integer(:,:), matrix containing the slice indexes
!! @param[in] v real(:), mew values
subroutine xp_number__set_slice_v__2 (x, s, v)
  implicit none
  type(xp_number), intent(inout) :: x
  integer, intent(in) :: s(:,:)
  real(kind=xp_), intent(in) :: v(:)
  call do_safe_within('xp_number__set_slice_v__2', mod_numbers_utils_name_, private_set)
contains
  subroutine private_set
    integer :: indx(size(s, 2))
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(size(s, 1) == get_rank(x), err_wrngArg_, 's')
    call assert(size(s, 2) == size(v), err_wrngArg_, 'v')
    call assert(all(minval(s, 2) > 0), err_oorng_, 's')
    call assert(all(maxval(s, 2) <= x%shp), err_oorng_, 's')
    if (err_free()) then
       call get_slice_flat_indexes(s, x%shp, indx)
       x%v(indx) = v
    end if
  end subroutine private_set
end subroutine xp_number__set_slice_v__2

!> Sets the gradient values of a 'number' slice to the given scalar
!! @param[inout] x 'number'
!! @param[in] s integer(:,:), matrix containig the slice indexes
!! @param[in] v real, new value
subroutine xp_number__set_slice_dv__1 (x, s, dv)
  implicit none
  type(xp_number), intent(inout) :: x
  integer, intent(in) :: s(:,:)
  real(kind=xp_), intent(in) :: dv
  call do_safe_within('dual_number__set_dv__1', mod_numbers_utils_name_, private_set)
contains
  subroutine private_set
    integer :: indx(size(s, 2))
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(has_dx(x), err_generic_, "x has no dx")
    call assert(size(s, 1) == get_rank(x), err_wrngArg_, 's')
    call assert(all(minval(s, 2) > 0), err_oorng_, 's')
    call assert(all(maxval(s, 2) <= x%shp), err_oorng_, 's')
    if (err_free()) then
       call get_slice_flat_indexes(s, x%shp, indx)
       x%dv(indx) = dv
    end if
  end subroutine private_set
end subroutine xp_number__set_slice_dv__1

!> Sets the gradient values of a 'number' slice to the given array of values
!! @param[inout] x 'number'
!! @param[in] s integer(:,:), matrix containing the slice indexes
!! @param[in] v real(:), new values
subroutine xp_number__set_slice_dv__2 (x, s, dv)
  implicit none
  type(xp_number), intent(inout) :: x
  integer, intent(in) :: s(:,:)
  real(kind=xp_), intent(in) :: dv(:)
  call do_safe_within('dual_number__set_dv__1', mod_numbers_utils_name_, private_set)
contains
  subroutine private_set
    integer :: indx(size(s, 2))
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(has_dx(x), err_generic_, "x has no dx")
    call assert(size(s, 1) == get_rank(x), err_wrngArg_, 's')
    call assert(size(s, 2) == size(dv), err_wrngArg_, 'sv')
    call assert(all(minval(s, 2) > 0), err_oorng_, 's')
    call assert(all(maxval(s, 2) <= x%shp), err_oorng_, 's')
    if (err_free()) then
       call get_slice_flat_indexes(s, x%shp, indx)
       x%dv(indx) = dv
    end if
  end subroutine private_set
end subroutine xp_number__set_slice_dv__2

!> Sets the values of a 'number' flat slice to the given value
!! @param[inout] x 'number'
!! @param[in] s integer(:,:), matrix containing the slice indexes
!! @param[in] v real, new value
subroutine xp_number__set_flat_slice_v__1 (x, s, v)
  implicit none
  type(xp_number), intent(inout) :: x
  integer, intent(in) :: s(:)
  real(kind=xp_), intent(in) :: v
  call do_safe_within('dual_number__set_dv__1', mod_numbers_utils_name_, private_set)
contains
  subroutine private_set
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(minval(s) > 0, err_oorng_, 's')
    call assert(maxval(s) <= size(x%v), err_oorng_, 's')
    if (err_free()) x%v(s) = v
  end subroutine private_set
end subroutine xp_number__set_flat_slice_v__1

!> Sets the values of a 'number' flat slice to the given array of values
!! @param[inout] x 'number'
!! @param[in] s integer(:,:), matrix containing the slice indexes
!! @param[in] v real(:), new values
subroutine xp_number__set_flat_slice_v__2 (x, s, v)
  implicit none
  type(xp_number), intent(inout) :: x
  integer, intent(in) :: s(:)
  real(kind=xp_), intent(in) :: v(:)
  call do_safe_within('dual_number__set_dv__1', mod_numbers_utils_name_, private_set)
contains
  subroutine private_set
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(minval(s) > 0, err_oorng_, 's')
    call assert(maxval(s) <= size(x%v), err_oorng_, 's')
    if (err_free()) x%v(s) = v
  end subroutine private_set
end subroutine xp_number__set_flat_slice_v__2

!> Sets the gradient values of a 'number' flat slice to the given value
!! @param[inout] x 'number'
!! @param[in] s integer(:,:), matrix containing the slice indexes
!! @param[in] v real, new value
subroutine xp_number__set_flat_slice_dv__1 (x, s, dv)
  implicit none
  type(xp_number), intent(inout) :: x
  integer, intent(in) :: s(:)
  real(kind=xp_), intent(in) :: dv
  call do_safe_within('dual_number__set_dv__1', mod_numbers_utils_name_, private_set)
contains
  subroutine private_set
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(has_dx(x), err_generic_, 'x has no dx')
    call assert(minval(s) > 0, err_oorng_, 's')
    call assert(maxval(s) <= size(x%v), err_oorng_, 's')
    if (err_free()) x%dv(s) = dv
  end subroutine private_set
end subroutine xp_number__set_flat_slice_dv__1

!> Sets the gradient values of a 'number' flat slice to the given array of values
!! @param[inout] x 'number'
!! @param[in] s integer(:,:), matrix containing the slice indexes
!! @param[in] v real(:), new values
subroutine xp_number__set_flat_slice_dv__2 (x, s, dv)
  implicit none
  type(xp_number), intent(inout) :: x
  integer, intent(in) :: s(:)
  real(kind=xp_), intent(in) :: dv(:)
  call do_safe_within('dual_number__set_dv__1', mod_numbers_utils_name_, private_set)
contains
  subroutine private_set
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(size(s) == size(dv), err_wrngArg_, 'v')
    call assert(minval(s) > 0, err_oorng_, 's')
    call assert(maxval(s) <= size(x%v), err_oorng_, 's')
    if (err_free()) x%dv(s) = dv
  end subroutine private_set
end subroutine xp_number__set_flat_slice_dv__2

!> Take a contiguous slice from a number (along the highest rank dimension)
!! @author Filippo Monari
!! @param[inout] x1 'number' storing the slice
!! @param[in] x2 'number' to slice
!! @param[in] s1,s2 integer, initial and final index of the slice
!! @details
!! The slice is realized by means of pointers and no data is copied.
subroutine xp_number__take_contiguous_slice (x1, x2, s1, s2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  integer, intent(in) :: s1, s2
  integer :: init_dv
  call do_safe_within("number__take_contiguous_slice", mod_numbers_utils_name_, private_slice)
contains
  subroutine private_slice
    call assert(is_deallocated(x1), err_alreadyInit_, "x1")
    call assert(is_allocated(x2), err_notAlloc_, "x2")
    call assert(s1 > 0 .and. s2 >= s1 .and. s2 <= x2%shp(get_rank(x2)), err_oorng_, 's1 or s2')
    if (get_rank(x2) == 1) then
       call err_safe(private_slice_rank1)
    else if (get_rank(x2) > 1) then 
       call err_safe(private_slice_rankGt1)
    else
       call raise_error("slice implemented only for rank 1 and ran2", err_generic_)
    end if
    call err_safe(private_complete_allocation)
  end subroutine private_slice
  subroutine private_slice_rank1
    x1%v => x2%v(s1:s2)
    if (has_dx(x2)) then
       x1%dv => x2%dv(s1:s2)
       init_dv = init_assoc_
    else 
       init_dv = init_alloc_
    end if
    call alloc(x1%shp, [s2 - s1 + 1], 'x1%shp')
  end subroutine private_slice_rank1
  subroutine private_slice_rankGt1
    logical :: mask(get_rank(x2))
    integer :: l, a, b, sz
    mask = .true.
    mask(get_rank(x2)) = .false.
    l = product(pack(x2%shp, mask))
    a = (s1 - 1) * l + 1
    b = s2 * l
    sz = b - a + 1
    x1%v(1:sz) => x2%v(a:b)
    if (has_dx(x2)) then
       x1%dv(1:sz) => x2%dv(a:b)
       init_dv = init_assoc_
    else
       init_dv = init_alloc_
    end if
    if (s1 == s2) then
       call alloc(x1%shp, pack(x2%shp, mask), 'x1%shp')
    else
       call alloc(x1%shp, [pack(x2%shp, mask), s2 - s1 + 1], 'x1%shp')
    end if
  end subroutine private_slice_rankGt1
  subroutine private_complete_allocation
    if (init_dv == init_alloc_) call alloc(x1%dv, [real(kind=xp_)::], "x1%dv")
    call number__set_init(x1, "v", init_assoc_)
    call number__set_init(x1, "dv", init_dv)
  end subroutine private_complete_allocation
end subroutine xp_number__take_contiguous_slice
  
  
