!> Higher shape between two 'numbers'.
!! @param[in] x1 'number'
!! @param[in] x2 'number'
pure function higher_shape__1 (x1, x2) result(shp)
  implicit none
  class(number), intent(in) :: x1, x2
  integer :: shp(max(get_rank(x1), get_rank(x2)))
  if (get_rank(x1) > get_rank(x2)) then
     shp = x1%shp
  else if (get_rank(x2) > get_rank(x1)) then
     shp = x2%shp
  else if (product(x1%shp) > product(x2%shp)) then
     shp = x1%shp
  else
     shp = x2%shp
  end if
end function higher_shape__1

!> Higher shape between a shape vector and a 'number'.
!! @param[in] shp1 shape vector
!! @param[in] x2 'number'
pure function higher_shape__2 (shp1, x2) result(shp)
  implicit none
  integer, intent(in) :: shp1(:)
  class(number), intent(in) :: x2
  integer :: shp(max(size(shp1), get_rank(x2)))
  if (size(shp1) > get_rank(x2)) then
     shp = shp1
  else if (get_rank(x2) > size(shp1)) then
     shp = x2%shp
  else if (product(shp1) > product(x2%shp)) then
     shp = shp1
  else
     shp = x2%shp
  end if
end function higher_shape__2

!> Lower shape between two 'numbers'.
!! @param[in] x1 'number'
!! @param[in] x2 'number'
pure function lower_shape__1 (x1, x2) result(shp)
  implicit none
  class(number), intent(in) :: x1, x2
  integer :: shp(min(get_rank(x1), get_rank(x2)))
  if (get_rank(x1) < get_rank(x2)) then
     shp = x1%shp
  else if (get_rank(x2) < get_rank(x1)) then
     shp = x2%shp
  else if (product(x1%shp) < product(x2%shp)) then
     shp = x1%shp
  else
     shp = x2%shp
  end if
end function lower_shape__1

!> Lower shape between a shape vector and a 'number'.
!! @param[in] shp1 shape vector
!! @param[in] x2 'number'
pure function lower_shape__2 (shp1, x2) result(shp)
  implicit none
  integer, intent(in) :: shp1(:)
  class(number), intent(in) :: x2
  integer :: shp(min(size(shp1), get_rank(x2)))
  if (size(shp1) < get_rank(x2)) then
     shp = shp1
  else if (get_rank(x2) < size(shp1)) then
     shp = x2%shp
  else if (product(shp1) < product(x2%shp)) then
     shp = shp1
  else
     shp = x2%shp
  end if
end function lower_shape__2
