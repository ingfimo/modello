!> To set 'x%v' to the same scalar value 'v'
!! @param[inout] x 'number'
!! @param[in] v double precision, value
subroutine xp_number__set_v__1 (x, v)
  implicit none
  type(xp_number), intent(inout) :: x
  real(kind=xp_), intent(in) :: v
  call do_safe_within('number__set_v__1', mod_numbers_utils_name_, private_set)
contains
  subroutine private_set
    call assert(is_allocated(x), err_notAlloc_, 'x')
    if (err_free()) x%v = v
  end subroutine private_set
end subroutine xp_number__set_v__1

!> To set 'x%v' to the vector 'v'
!! @param[inout] x 'number'
!! @param[in] double precision(:), value
subroutine xp_number__set_v__2 (x, v)
  implicit none
  type(xp_number), intent(inout) :: x
  real(kind=xp_), intent(in) :: v(:)
  call do_safe_within('number__set_v__2', mod_numbers_utils_name_, private_set)
contains
  subroutine private_set
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(size(x%v) == size(v), err_wrngSz_, 'v')
    if (err_free()) x%v = v
  end subroutine private_set
end subroutine xp_number__set_v__2

!> Set the values for a sequence of 'numbers'.
!! A sequence of 'numbers' is a seres of 'numbers' having the same shape
!! @param[inout] x number(:), array containing the sequence of 'numbers'
!! @param[in] v real(:,:), matrix containing as columns the values of the sequence
subroutine xp_number__set_v__3 (x, v)
  implicit none
  type(xp_number), intent(inout) :: x(:)
  real(kind=xp_), intent(in) :: v(:,:)
  call do_safe_within('number__set_v__3', mod_numbers_utils_name_, private_do)
contains
  subroutine private_do
    integer :: i, info(size(x))
    call assert(size(x) == size(v, 2), err_wrngSz_, "x or v")
    if (err_free()) then
       !$omp parallel do
       do i = 1, size(x)
          call private_set(x(i), v(:,i), info(i))
       end do
       !end omp parallel do
       call assert(all(info == 0), err_generic_, "parallel set operation retuned erros")
    end if
  end subroutine private_do
  subroutine private_set(xi, vi, infoi)
    type(xp_number), intent(inout) :: xi
    real(kind=xp_), intent(in) :: vi(:)
    integer, intent(out) :: infoi
    infoi = 0
    if (.not. is_allocated(xi)) infoi = 1
    if (infoi == 0) then
       if (size(xi%v) /= size(vi)) infoi = 1
       if (infoi == 0) then
          xi%v = vi
       end if
    end if
  end subroutine private_set
end subroutine xp_number__set_v__3

!> To set 'x%dv' to the same scalar value 'v'
!! @param[inout] x 'number'
!! @param[in] dv double precision, derivative value
subroutine xp_number__set_dv__1 (x, dv)
  implicit none
  type(xp_number), intent(inout) :: x
  real(kind=xp_), intent(in) :: dv
  call do_safe_within('dual_number__set_dv__1', mod_numbers_utils_name_, private_set)
contains
  subroutine private_set
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(has_dx(x), err_generic_, 'x has no dx.')
    if (err_free()) x%dv = dv
  end subroutine private_set
end subroutine xp_number__set_dv__1

!> To set 'x%dv' to the vector 'v'
!! @param[inout] x 'number'
!! @param[in] dv double precision(:), derivative value
subroutine xp_number__set_dv__2 (x, dv)
  implicit none
  type(xp_number), intent(inout) :: x
  real(kind=xp_), intent(in) :: dv(:)
  call do_safe_within('dual_number__set_dv__2', mod_numbers_utils_name_, private_set)
contains
  subroutine private_set
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(has_dx(x), err_generic_, 'x has no dx.')
    call assert(size(x%v) == size(dv), err_wrngSz_, 'v')
    if (err_free()) x%dv = dv
  end subroutine private_set
end subroutine xp_number__set_dv__2
