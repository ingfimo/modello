!> Make the reshae of a 'number'
!! @author Filippo Monari
!! @param[inout] x1 'number' sotoring the reshaping
!! @param[in] x2 'number' to reshape
!! @param[in] shp integer(:), new shape
!! @details
!! The reshaping is done thorugh pointers and no data is copied.
subroutine xp_number__make_reshape (x1, x2, shp)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  integer, intent(in) :: shp(:)
  call do_safe_within('number__make_reshape', mod_numbers_utils_name_, private_reshape)
contains
  subroutine private_reshape
    call assert(is_deallocated(x1), err_alreadyInit_, 'x1')
    call assert(is_allocated(x2), err_notAlloc_, 'x2')
    call assert(product(shp) == size(x2%v), err_wrngArg_, 'shp')
    call alloc(x1%shp, shp, 'x1%shp')
    if(err_free()) then
       x1%v => x2%v
       call number__set_init(x1, 'v',  init_assoc_)
    end if
    if (err_free()) then
       if (has_dx(x2)) then
          x1%dv => x2%dv
          call number__set_init(x1, 'dv',  init_assoc_)
       else
          call alloc(x1%dv, [real(kind=xp_)::], 'x1%dv')
          call number__set_init(x1, 'dv', init_assoc_)
       end if
    end if
  end subroutine private_reshape
end subroutine xp_number__make_reshape

!> Drops the dimesions of a number that are equal to 1.
!! @author Filippo Monari
!! @param[inout] x1 'number' storing the reshaping
!! @param[in] x2 'number' to reshape
!! @details
!! The reshaping is done thorugh pointers and no data is copied.
subroutine xp_number__make_drop_shape (x1, x2)
  implicit none
  type(xp_number), intent(inout) :: x1
  type(xp_number), intent(in) :: x2
  call do_safe_within('number__make_drop_shape', mod_numbers_utils_name_, private_drop_shape)
contains
  subroutine private_drop_shape
    call number__make_reshape(x1, x2, pack(x2%shp, x2%shp /= 1))
  end subroutine private_drop_shape
end subroutine xp_number__make_drop_shape
