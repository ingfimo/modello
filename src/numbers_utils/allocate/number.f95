!> Given a 'number' and a character identifying an element, sets the
!! appropriate slot in the 'init' register of the 'number' to
!! an initialisation value.
!! @author Filippo Monari
!! @param[in] x a 'number'
!! @param[in] k character identifying the element
!! @param[in] w integer, initialisation value
subroutine number__set_init (x, k, w)
  implicit none
  class(number), intent(inout) :: x
  character(len=*), intent(in) :: k
  integer, intent(in) :: w
  call do_safe_within('number__set_init', mod_numbers_utils_name_, private_do)
contains
  subroutine private_do
    call assert(any(init_vals_ == w), err_unknwnVal_, 'w')
    call err_safe(private_set_init)
  end subroutine private_do
  subroutine private_set_init
    select case (k)
    case ('v')
       call assert(x%init(number_init_vi_) == init_null_, err_alreadyAlloc_, 'x%v')
       if (err_free()) x%init(number_init_vi_) = w
    case ('dv')
       call assert(x%init(number_init_dvi_) == init_null_, err_alreadyAlloc_, 'x%dv') 
       if (err_free()) x%init(number_init_dvi_) = w
    case default
       call raise_error('k', err_unknwnVal_)
    end select
  end subroutine private_set_init
end subroutine number__set_init

!> Retrieves the initialisation value stored in the 'init' register
!! of a number relaive to the element indentified by the provided character.
!! @author Filippo Monari
!! @param[in] x 'number'
!! @param[in] k character indicating the element 
function number__get_init (x, k) result(ans)
  implicit none
  class(number), intent(inout) :: x
  character(*), intent(in) :: k
  integer :: ans
  call do_safe_within('number__get_init', mod_numbers_utils_name_, private_get_init)
contains
  subroutine private_get_init
    implicit none
    select case (k)
    ! case ('shp')
    !    ans = x%init(number_init_shpi_)
    ! case('id')
    !    ans = x%init(number_init_idi_)
    case ('v')
       ans = x%init(number_init_vi_)
    case ('dv')
       ans = x%init(number_init_dvi_)
    case default
       call raise_error('k', err_unknwnVal_)
    end select
  end subroutine private_get_init
end function number__get_init

!> Allocates a 'number' with given shape 'shp'.
subroutine number__allocate__0 (x, shp)
  implicit none
  class(number), intent(inout) :: x
  integer, intent(in) :: shp(:)
  call do_safe_within('number__allocate__0', mod_numbers_utils_name_, private_allocate)
contains
  subroutine private_allocate
    call alloc(x%shp, shp, 'x%shp')
    call number__set_init(x, 'v',  init_alloc_)
    call number__set_init(x, 'dv', init_alloc_)
  end subroutine private_allocate
end subroutine number__allocate__0

