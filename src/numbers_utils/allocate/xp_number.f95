!> Allocates a 'number' with given shape 'shp'.
subroutine xp_number__allocate__1 (x, shp, dx)
  implicit none
  type(xp_number), intent(inout) :: x
  integer, intent(in) :: shp(:)
  logical, intent(in) :: dx
  call do_safe_within('xp_number__allocate__1', mod_numbers_utils_name_, private_allocate)
contains
  subroutine private_allocate
    call assert(is_deallocated(x), err_alreadyAlloc_, 'x')
    call alloc(x%v, product(shp), "x%v")
    if (dx .and. WITHDX_) then
       call alloc(x%dv, product(shp), 'x%dv')
    else
       call alloc(x%dv, [real(kind=xp_)::], 'x%dv')
    end if
    call number__allocate__0(x, shp)
  end subroutine private_allocate
end subroutine xp_number__allocate__1

!> Allocates a 'number' with given shape 'shp' and as source the scalar 'v'.
subroutine xp_number__allocate__2 (x, shp, dx, v)
  implicit none
  type(xp_number), intent(inout) :: x
  real(kind=xp_), intent(in) :: v
  integer, intent(in) :: shp(:)
  logical, intent(in) :: dx
  call do_safe_within('xp_number__allocate__2', 'mod_numbers_utils', private_allocate)
contains
  subroutine private_allocate
    call number__allocate(x, shp, dx)
    call number__set_v(x, v)
  end subroutine private_allocate
end subroutine xp_number__allocate__2

!> Allocate a 'number' with given shape 'shp' and as source the array 'v'.
subroutine xp_number__allocate__3 (x, shp, dx, v)
  implicit none
  type(xp_number), intent(inout) :: x
  real(kind=xp_), intent(in) :: v(:)
  integer, intent(in) :: shp(:)
  logical, intent(in) :: dx
  call do_safe_within('xp_number__allocate__3', mod_numbers_utils_name_, private_allocate)
contains
  subroutine private_allocate
    call number__allocate(x, shp, dx)
    call number__set_v(x, v)
  end subroutine private_allocate
end subroutine xp_number__allocate__3

!> Allocates an array of 'numbers' of dimesion (:), given the required size 
!! @param[inout] number(:), allocatable
!! @param[in] n integer, array size
!! @param[in] xname character, name of the variable (for error reporting)
subroutine xp_number__allocate__4 (x, n)
  implicit none
  type(xp_number), intent(inout), allocatable :: x(:)
  integer :: n
  call do_safe_within("xp_number__allocate__4", mod_numbers_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: info
    call assert(.not. allocated(x), err_alreadyAlloc_, "x")
    if (err_free()) then
       allocate(x(n), stat=info)
       call assert(info == 0, err_alloc_, "x")
    end if
  end subroutine private_allocate
end subroutine xp_number__allocate__4





