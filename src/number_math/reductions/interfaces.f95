  !> @addtogroup numbers_math__reductions_
  !! @{

  !> Checks that the inputs of a reduction operation are consistent.
  !! @author Filippo Monari
  !! @param[in] x1 'number' with rank > 0 
  interface number__red_arg_check
     module procedure sp_number__red_arg_check
     module procedure dp_number__red_arg_check
  end interface number__red_arg_check

  !> Summation
  !!
  !! Given a `number` with rank > 0, returns the
  !! summation along a certain dimension or along all dimensions.
  !!
  !! @param[in] x 'number' of rank > 0
  !! @param[in] k integer, dimension index
  interface number__sum
     module procedure sp_number__sum__1
     module procedure sp_number__sum__2
     module procedure dp_number__sum__1
     module procedure dp_number__sum__2
  end interface number__sum

  !> Product
  !!
  !! Given a `number` with rank > 0, returns the
  !! product along a certain dimension or along all dimensions.
  !!
  !! @param[in] x 'number' of rank > 0
  !! @param[in] k integer, dimension index
  interface number__product
     module procedure sp_number__product__1
     module procedure sp_number__product__2
     module procedure dp_number__product__1
     module procedure dp_number__product__2
  end interface number__product

  !> Sum of Squares
  !!
  !! Given a `number` with rank > 0, returns the
  !! sum of squares along a certain dimension or along all dimensions.
  !!
  !! @param[in] x 'number' of rank > 0
  !! @param[in] k integer, dimension index
  interface number__ssq
     module procedure sp_number__ssq__1
     module procedure sp_number__ssq__2
     module procedure dp_number__ssq__1
     module procedure dp_number__ssq__2
  end interface number__ssq

  !> @}
