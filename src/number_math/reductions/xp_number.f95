!> @relates number__red_arg_check
subroutine xp_number__red_arg_check (x1, k)
  implicit none
  type(xp_number), intent(in) :: x1
  integer, optional :: k
  call do_safe_within('number__red_arg_check', mod_numbers_math_name_, private_check)
contains
  subroutine private_check
    call assert(is_allocated(x1), err_notAlloc_, 'x1')
    call assert(get_rank(x1) > 0, err_generic_, 'x1 is scalar')
    if (present(k)) call assert(get_rank(x1) >= k .and. k > 0, err_wrngArg_, 'k')
  end subroutine private_check
end subroutine xp_number__red_arg_check

!> @relates number__sum
function xp_number__sum__1 (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within('number__sum', mod_numbers_math_name_, private_sum)
contains
  subroutine private_sum
    real(kind=xp_) :: dtyp
    call number__red_arg_check(x)
    call number__append(ans, [integer::], has_dx(x))
    call node__append(op_sum1_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_sum(x, ans)
  end subroutine private_sum
end function xp_number__sum__1

!> @relates number__sum
function xp_number__sum__2 (x, k) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  integer, intent(in) :: k
  type(xp_number), pointer :: ans
  call do_safe_within("number__sum__2", mod_numbers_math_name_, private_sum)
contains
  subroutine private_sum
    real(kind=xp_) :: dtyp
    integer :: i, indexes(get_size(x) + 1)
    call number__red_arg_check(x, k)
    call number__append(ans, pack(x%shp, [(i, i=1, get_rank(x))] /= k), has_dx(x))
    if (err_free()  .and. (.not. within_posts())) call get_slices_along_dim(x%shp, [k], indexes(2:), indexes(1))
    call node__append(op_sum2_id_, [x], [ans], indexes, [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_sum(x, ans, indexes)
  end subroutine private_sum
end function xp_number__sum__2

!> @relates number__product
function xp_number__product__1 (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within("number__product__1", mod_numbers_math_name_, private_product)
contains
  subroutine private_product
    real(kind=xp_) :: dtyp
    call number__red_arg_check(x)
    call number__append(ans, [integer::], has_dx(x))
    call node__append(op_product1_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_product(x, ans)
  end subroutine private_product
end function xp_number__product__1

!> @relates number__product
function xp_number__product__2 (x, k) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  integer, intent(in) :: k
  type(xp_number), pointer :: ans
  call do_safe_within("number__product__2", mod_numbers_math_name_, private_product)
contains
  subroutine private_product
    real(kind=xp_) :: dtyp
    integer :: i, indexes(get_size(x)+1)
    call number__red_arg_check(x, k)
    call number__append(ans, [pack(x%shp, [(i, i=1, get_rank(x))] /= k)], has_dx(x))
    if (err_free()  .and. (.not. within_posts())) call get_slices_along_dim(x%shp, [k], indexes(2:), indexes(1))
    call node__append(op_product2_id_, [x], [ans], indexes, [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_product(x, ans, indexes)
  end subroutine private_product
end function xp_number__product__2

!> @relates number_ssq
function xp_number__ssq__1 (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within('number__ssq', mod_numbers_math_name_, private_ssq)
contains
  subroutine private_ssq
    real(kind=xp_) :: dtyp
    call number__red_arg_check(x)
    call number__append(ans, [integer::], has_dx(x))
    call node__append(op_ssq1_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_ssq(x, ans)
  end subroutine private_ssq
end function xp_number__ssq__1

!> @relates number__ssq
function xp_number__ssq__2 (x, k) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  integer, intent(in) :: k
  type(xp_number), pointer :: ans
  call do_safe_within("number__ssq__2", mod_numbers_math_name_, private_ssq)
contains
  subroutine private_ssq
    real(kind=xp_) :: dtyp
    integer :: i, indexes(get_size(x)+1)
    call number__red_arg_check(x, k)
    call number__append(ans, [pack(x%shp, [(i, i=1, get_rank(x))] /= k)], has_dx(x))
    if (err_free()  .and. (.not. within_posts())) call get_slices_along_dim(x%shp, [k], indexes(2:), indexes(1))
    call node__append(op_ssq2_id_, [x], [ans], indexes, [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_ssq(x, ans, indexes)
  end subroutine private_ssq
end function xp_number__ssq__2
