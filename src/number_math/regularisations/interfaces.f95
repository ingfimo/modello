  !> @addtogroup numbers_math__regularisations_
  !! @{

  !> Dropout
  !! @param[in] x1 number, array to apply dropout onto
  !! @param[in] r sp_ or dp_, dropout rate
  interface number__dropout
     module procedure sp_number__dropout
     module procedure dp_number__dropout
  end interface number__dropout

  !> @}
