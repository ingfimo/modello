!> @relates number__dropout
function xp_number__dropout (x1, r) result(ans)
  implicit none
  type(xp_number), intent(in) :: x1
  real(kind=xp_), intent(in) :: r
  type(xp_number), pointer :: ans
  call do_safe_within('number__dropout', mod_numbers_math_name_, private_dropout)
contains
  subroutine private_dropout
    real(kind=xp_) :: dtyp
    type(xp_number), pointer :: x2
    call number__append(ans, x1%shp, has_dx(x1))
    call number__append(x2, x1%shp, .false.)
    call node__append(op_dropout_id_, [x1, x2], [ans], [integer::], [r], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_dropout(x1, x2, ans, r)
  end subroutine private_dropout
end function xp_number__dropout

