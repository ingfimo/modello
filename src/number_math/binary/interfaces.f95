  !> @addtogroup numbers_math__binary_
  !! @{

  !> Check Binary Operator Arguments
  !!
  !! Helper subroutine for checking the arguments of binary operators
  !!
  !! @param[in] x1,x2 @c number, arguments
  !! @param[out] opid @c integer, type of broadcasting operator
  interface number__binary_arg_check
     module procedure sp_number__binary_arg_check
     module procedure dp_number__binary_arg_check
  end interface number__binary_arg_check

  interface number__make_binary_op
     module procedure sp_number__make_binary_op
     module procedure dp_number__make_binary_op
  end interface number__make_binary_op
  
  !> Returns the addition between two 'numbers'
  !! @author Filippo Monari
  !! @param[in] x1,x2 'numbers'
  interface operator (+)
     module procedure sp_number__add
     module procedure dp_number__add
  end interface operator (+)

  !> Returns the subtraction between two 'numbers'
  !! @author Filippo Monari
  !! @param[in] x1,x2 'numbers'
  interface operator (-)
     module procedure sp_number__sub
     module procedure dp_number__sub
     module procedure sp_number__neg
     module procedure dp_number__neg
  end interface operator (-)

  !> Returns the multiplication between two 'numbers'
  !! @author Filippo Monari
  !! @param[in] x1,x2 'numbers'
  interface operator (*)
     module procedure sp_number__mult
     module procedure dp_number__mult
  end interface operator (*)

  !> Returns the power between two 'numbers'
  !! @author Filippo Monari
  !! @param[in] x1,x2 'numbers'
  interface operator (**)
     module procedure sp_number__pow
     module procedure dp_number__pow
  end interface operator (**)

  !> Returns the division between two 'numbers'
  !! @author Filippo Monari
  !! @param[in] x1,x2 'numbers'
  interface operator (/)
     module procedure sp_number__div
     module procedure dp_number__div
  end interface operator (/)

  !> @}
