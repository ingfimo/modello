!> @relates number__binary_arg_check
subroutine xp_number__binary_arg_check (x1, x2, opid)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  integer, intent(out) :: opid
  call do_safe_within('number__binary_arg_check', mod_numbers_math_name_, private_check)
contains
  subroutine private_check
    call assert(is_allocated(x1), err_notAlloc_, 'x1')
    call assert(is_allocated(x2), err_notAlloc_, 'x2')
    call assert(mod(product(higher_shape(x1, x2)), product(lower_shape(x1, x2))) == 0, &
         err_wrngSz_, 'x1 or x2')
    if (get_rank(x1) > 1 .and. get_rank(x1) == get_rank(x2) .and. get_size(x1) /= get_size(x2)) then
       call assert(all(pack(lower_shape(x1, x2), lower_shape(x1, x2) /= higher_shape(x1, x2)) == 1), &
            err_generic_, 'bradcasting error')
       opid = 2
    else
       opid = 1
    end if
  end subroutine private_check
end subroutine xp_number__binary_arg_check

subroutine xp_number__make_binary_op(x1, x2, ans, opid1, opid2, jjj)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(out), pointer :: ans
  integer, intent(out), allocatable :: jjj(:)
  integer, intent(in) :: opid1, opid2
  integer :: optyp
  real(kind=xp_) :: dtyp
  call do_safe_within("xp_number__binary", mod_numbers_math_name_, private_do)
contains
  subroutine private_do
    call number__binary_arg_check(x1, x2, optyp)
    call number__append(ans, higher_shape(x1, x2), has_dx(x1) .or. has_dx(x2))
    select case(optyp)
    case (1)
       call node__append(opid1, [x1, x2], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
       call alloc(jjj, [integer::], "jjj")
    case (2)
       call alloc(jjj, product(higher_shape(x1, x2)) + 1, "jjj")
       if (err_free()) call get_slices_along_dim(higher_shape(x1, x2), which_eq(lower_shape(x1, x2), 1), jjj(2:), jjj(1))
       call node__append(opid2, [x1, x2], [ans], jjj, [real(kind=xp_)::], [integer::], [integer::])
    case default
       call raise_error("optyp", err_unknwnVal_)
    end select
    call graph__append(dtyp)
  end subroutine private_do
end subroutine xp_number__make_binary_op

!> @relates 
function xp_number__add (x1, x2) result(ans)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), pointer :: ans
  call do_safe_within('xp_number__add', mod_numbers_math_name_, private_add)
contains
  subroutine private_add
    integer, allocatable :: jjj(:)
    call number__make_binary_op(x1, x2, ans, op_add1_id_, op_add2_id_, jjj)
    if (err_free()  .and. (.not. within_posts())) then
       if (size(jjj) > 0) then
          call op_add(x1, x2, ans, jjj)
       else
          call op_add(x1, x2, ans)
       end if
    end if
  end subroutine private_add
end function xp_number__add

!> @relates -
function xp_number__sub (x1, x2) result(ans)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), pointer :: ans
  call do_safe_within('xp_number__sub', mod_numbers_math_name_, private_sub)
contains
  subroutine private_sub
    integer, allocatable :: jjj(:)
    call number__make_binary_op(x1, x2, ans, op_sub1_id_, op_sub2_id_, jjj)
    if (err_free()  .and. (.not. within_posts())) then
       if (size(jjj) > 0) then
          call op_sub(x1, x2, ans, jjj)
       else
          call op_sub(x1, x2, ans)
       end if
    end if
  end subroutine private_sub
end function xp_number__sub

!> @relates *
function xp_number__mult (x1, x2) result(ans)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), pointer :: ans
  call do_safe_within('xp_number__mult', mod_numbers_math_name_, private_mult)
contains
  subroutine private_mult
    integer, allocatable :: jjj(:)
    call number__make_binary_op(x1, x2, ans, op_mult1_id_, op_mult2_id_, jjj)
    if (err_free()  .and. (.not. within_posts())) then
       if (size(jjj) > 0) then
          call op_mult(x1, x2, ans, jjj)
       else
          call op_mult(x1, x2, ans)
       end if
    end if
  end subroutine private_mult
end function xp_number__mult

!> @relates **
function xp_number__pow (x1, x2) result(ans)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), pointer :: ans
  call do_safe_within('xp_number__pow', mod_numbers_math_name_, private_pow)
contains
  subroutine private_pow
    integer, allocatable :: jjj(:)
    call number__make_binary_op(x1, x2, ans, op_pow1_id_, op_pow2_id_, jjj)
    if (err_free()  .and. (.not. within_posts())) then
       if (size(jjj) > 0) then
          call op_pow(x1, x2, ans, jjj)
       else
          call op_pow(x1, x2, ans)
       end if
    end if
  end subroutine private_pow
end function xp_number__pow

!> @relates operator(/)
function xp_number__div (x1, x2) result(ans)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), pointer :: ans
  real(kind=xp_) :: dtyp
  call do_safe_within('number__div', mod_numbers_math_name_, private_div)
contains
  subroutine private_div
    integer, allocatable :: jjj(:)
    call number__make_binary_op(x1, x2, ans, op_div1_id_, op_div2_id_, jjj)
    if (err_free()  .and. (.not. within_posts())) then
       if (size(jjj) > 0) then
          call op_div(x1, x2, ans, jjj)
       else
          call op_div(x1, x2, ans)
       end if
    end if
  end subroutine private_div
end function xp_number__div

