  !> @addtogroup numbers_math__activations_
  !! @{

  !> Sigmoid
  !! ans = 1 / (1 - e ** -x)
  !! @param[in] x 'number'
  interface number__sigmoid
     module procedure sp_number__sigmoid
     module procedure dp_number__sigmoid
  end interface number__sigmoid

  !> @todo document
  interface number__softplus
     module procedure sp_number__softplus
     module procedure dp_number__softplus
  end interface number__softplus

  !> ReLU
  !! ans = if (x > 0) x else 0
  !! @param[in] x 'number'
  interface number__relu
     module procedure sp_number__relu
     module procedure dp_number__relu
  end interface number__relu

  !> Leaky ReLU
  !! ans = if (x > 0) x else a*x
  !! @param[in] x 'number'
  !! @param[in] a 'number' scalar
  interface number__leaky_relu
     module procedure sp_number__leakyrelu
     module procedure dp_number__leakyrelu
  end interface number__leaky_relu

  !> ELU
  !! if (x > 0) ans = x else ans = a * (exp(x) - 1) 
  !! @param[in] x 'number'
  !! @param[in] a 'number' scalar
  interface number__elu
     module procedure sp_number__elu
     module procedure dp_number__elu
  end interface number__elu

  !> SiLU
  !! ans = x * sigmoid(x)
  !! @param[in] x 'number'
  interface number__silu
     module procedure sp_number__silu
     module procedure dp_number__silu
  end interface number__silu

  !> Swish
  !! ans = 1 / sigmoid(x * a)
  !! @param[in] x 'number'
  !! @param[in] a 'number' scalar
  interface number__swish
     module procedure sp_number__swish
     module procedure dp_number__swish
  end interface number__swish

  !> Returns the softmax of a 'number' of 0 > rank <= 0.
  !! @author Filippo Monari
  !! @param[in] x 'number'
  !! @param[in] k integer, dimension along which calculate the softmax (only rank ==2)
  interface number__softmax
     module procedure sp_number__softmax__1
     module procedure sp_number__softmax__2
     module procedure dp_number__softmax__1
     module procedure dp_number__softmax__2
  end interface number__softmax

  !> @}
