!> @relates umber__sigmoid
function xp_number__sigmoid (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within("xp_number__sigmoid", mod_numbers_math_name_, private_sigmoid)
contains
  subroutine private_sigmoid
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call number__append(ans, x%shp, has_dx(x))
    call node__append(op_sigmoid_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_sigmoid(x, ans)
  end subroutine private_sigmoid
end function xp_number__sigmoid

!> @relates number_softplus
function xp_number__softplus (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within("xp_number__softplus", mod_numbers_math_name_, private_softplus)
contains
  subroutine private_softplus
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, "x")
    call number__append(ans, x%shp, has_dx(x))
    call node__append(op_softplus_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_softplus(x, ans)
  end subroutine private_softplus
end function xp_number__softplus

!> @relates numnber__relu
function xp_number__relu (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within("number__relu", mod_numbers_math_name_, private_relu)
contains
  subroutine private_relu
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call number__append(ans, x%shp, has_dx(x))
    call node__append(op_relu_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_relu(x, ans)
  end subroutine private_relu
end function xp_number__relu

!> @relates number__leakyrelu
function xp_number__leakyrelu (x, a) result(ans)
  implicit none
  type(xp_number), intent(in) :: a, x
  type(xp_number), pointer :: ans
  call do_safe_within("number__leaky_relu", mod_numbers_math_name_, private_leakyrelu)
contains
  subroutine private_leakyrelu
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(is_allocated(a), err_notAlloc_, 'a')
    call assert(get_rank(a) == 0, err_generic_, 'rank(a) != 0')
    call number__append(ans, x%shp, has_dx(x) .or. has_dx(a))
    call node__append(op_leakyrelu_id_, [x, a], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_leakyrelu(x, a, ans)
  end subroutine private_leakyrelu
end function xp_number__leakyrelu

!> @relates number__elu
function xp_number__elu (x, a) result(ans)
  implicit none
  type(xp_number), intent(in) :: x, a
  type(xp_number), pointer :: ans
  call do_safe_within("number__elu", mod_numbers_math_name_, private_elu)
contains
  subroutine private_elu
    real(kind=xp_) :: dtyp
    call assert(is_allocated(a), err_notAlloc_, 'a')
    call assert(get_rank(a) == 0, err_wrngSz_, 'a')
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call number__append(ans, x%shp, has_dx(x) .or. has_dx(a))
    call node__append(op_elu_id_, [x, a], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_elu(x, a, ans)
  end subroutine private_elu
end function xp_number__elu

!> @relates number__silu
function xp_number__silu (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within("number__silu", mod_numbers_math_name_, private_silu)
contains
  subroutine private_silu
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call number__append(ans, x%shp, has_dx(x))
    call node__append(op_silu_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_silu(x, ans)
  end subroutine private_silu
end function xp_number__silu

!> @relates number__swish
function xp_number__swish (x, a) result(ans)
  implicit none
  type(xp_number), intent(in) :: x, a
  type(xp_number), pointer :: ans
  call do_safe_within("number__swish", mod_numbers_math_name_, private_swish)
contains
  subroutine private_swish
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(is_allocated(a), err_notAlloc_, 'a')
    call assert(get_rank(a) == 0, err_wrngSz_, 'a')
    call number__append(ans, x%shp, has_dx(x))
    call node__append(op_swish_id_, [x, a], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_swish(x, a, ans)
  end subroutine private_swish
end function xp_number__swish

!> @relates number__softmax
function xp_number__softmax__1 (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within("number__softmax__2", mod_numbers_math_name_, private_softmax)
contains
  subroutine private_softmax
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call number__append(ans, x%shp, has_dx(x))
    call node__append(op_softmax1_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_softmax(x, ans)
  end subroutine private_softmax
end function xp_number__softmax__1

!> @relates number__softmax
function xp_number__softmax__2 (x, k) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  integer, intent(in) :: k
  type(xp_number), pointer :: ans
  call do_safe_within("number__softmax__2", mod_numbers_math_name_, private_softmax)
contains
  subroutine private_softmax
    real(kind=xp_) :: dtyp
    integer :: iii(get_size(x) + 1)
    call assert(get_rank(x) > 1 .and. k <= get_rank(x), err_wrngSz_, 'x')
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call number__append(ans, x%shp, has_dx(x))
    if (err_free()) call get_slices_along_dim(x%shp, [k], iii(2:), iii(1))
    call node__append(op_softmax2_id_, [x], [ans], iii, [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp) 
    if (err_free()  .and. (.not. within_posts())) call op_softmax(x, ans, iii)
  end subroutine private_softmax
end function xp_number__softmax__2


