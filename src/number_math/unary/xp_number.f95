!> @relates operator(-)
function xp_number__neg (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within("number__neg", mod_numbers_math_name_, private_neg)
contains
  subroutine private_neg
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call number__append(ans, x%shp, has_dx(x))
    call node__append(op_neg_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) call op_neg(x, ans)
  end subroutine private_neg
end function xp_number__neg

!> @relates number__abs
function xp_number__abs (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within("number__abs", mod_numbers_math_name_, private_abs)
contains
  subroutine private_abs
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call number__append(ans, x%shp, has_dx(x))
    call node__append(op_abs_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) call op_abs(x, ans)
  end subroutine private_abs
end function xp_number__abs

!> @relates number__exp
function xp_number__exp (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within('number__exp', mod_numbers_math_name_, private_exp)
contains
  subroutine private_exp
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call number__append(ans, x%shp, has_dx(x))
    call node__append(op_expon_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) call op_exp(x, ans)
  end subroutine private_exp
end function xp_number__exp

!> @relates number__log
function xp_number__log (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within('number__log', mod_numbers_math_name_, private_log)
contains
  subroutine private_log
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call number__append(ans, x%shp, has_dx(x))
    call node__append(op_log_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) call op_log(x, ans)
  end subroutine private_log
end function xp_number__log

!> @relates number__sin
function xp_number__sin (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within('number__sin', mod_numbers_math_name_, private_sin)
contains
  subroutine private_sin
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call number__append(ans, x%shp, has_dx(x))
    call node__append(op_sin_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) call op_sin(x, ans)
  end subroutine private_sin
end function xp_number__sin

!> @relates number__cos
function xp_number__cos (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within('number__cos', mod_numbers_math_name_, private_cos)
contains
  subroutine private_cos
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call number__append(ans, x%shp, has_dx(x))
    call node__append(op_cos_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) call op_cos(x, ans)
  end subroutine private_cos
end function xp_number__cos

!> @relates number__tan
function xp_number__tan (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within('number__tan', mod_numbers_math_name_, private_tan)
contains
  subroutine private_tan
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call number__append(ans, x%shp, has_dx(x))
    call node__append(op_tan_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) call op_tan(x, ans)
  end subroutine private_tan
end function xp_number__tan

!> @relates number__sinh
function xp_number__sinh (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within('number__sinh_dpn0', mod_numbers_math_name_, private_sinh)
contains
  subroutine private_sinh
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call number__append(ans, x%shp, has_dx(x))
    call node__append(op_sinh_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) call op_sinh(x, ans)
  end subroutine private_sinh
end function xp_number__sinh

!> @realtes number__cosh
function xp_number__cosh (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within('number__cosh_dpn0', mod_numbers_math_name_, private_cosh)
contains
  subroutine private_cosh
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call number__append(ans, x%shp, has_dx(x))
    call node__append(op_cosh_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) call op_cosh(x, ans)
  end subroutine private_cosh
end function xp_number__cosh

!> @relates number__tanh
function xp_number__tanh (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within('number__tanh_dpn0', mod_numbers_math_name_, private_tanh)
contains
  subroutine private_tanh
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call number__append(ans, x%shp, has_dx(x))
    call node__append(op_tanh_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) call op_tanh(x, ans)
  end subroutine private_tanh
end function xp_number__tanh
