  !> @addtogroup numbers_math__unary_
  !! @{

  !> Returns the absolute value of a 'number'
  !! @param[in] x 'number'
  interface abs
     module procedure sp_number__abs
     module procedure dp_number__abs
  end interface abs
  
  !> Returns the exponential of a 'number'
  !! @author Filippo Monari
  !! @param[in] x1 'number'
  interface exp
     module procedure sp_number__exp
     module procedure dp_number__exp
  end interface exp

  !> Returns the log of a 'number'
  !! @author Filippo Monari
  !! @param[in] x1 'number'
  interface log
     module procedure sp_number__log
     module procedure dp_number__log
  end interface log

  !> Returns the sin of a 'number'
  !! @author Filippo Monari
  !! @param[in] x1 'number'
  interface sin
     module procedure sp_number__sin
     module procedure dp_number__sin
  end interface sin

  !> Returns the cosine of a 'number'
  !! @author Filippo Monari
  !! @param[in] x1 'number'
  interface cos
     module procedure sp_number__cos
     module procedure dp_number__cos
  end interface cos

  !> Returns the tangent of a 'number'
  !! @author Filippo Monari
  !! @param[in] x1 'number'
  interface tan
     module procedure sp_number__tan
     module procedure dp_number__tan
  end interface tan

  !> Returns the hyperbolic sine of a 'number'
  !! @author Filippo Monari
  !! @param[in] x1 'number'
  interface sinh
     module procedure sp_number__sinh
     module procedure dp_number__sinh
  end interface sinh

  !> Returns the hyperbolic cosine of a 'number'
  !! @author Filippo Monari
  !! @param[in] x1 'number'
  interface cosh
     module procedure sp_number__cosh
     module procedure dp_number__cosh
  end interface cosh

  !> Returns the hyperbolic tangent of a 'number'
  !! @author Filippo Monari
  !! @param[in] x1 'number'
  interface tanh
     module procedure sp_number__tanh
     module procedure dp_number__tanh
  end interface tanh

  !> @}
