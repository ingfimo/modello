!> @relates number__dprob_arg_check
!! @todo review
subroutine xp_number__dprob_arg_check (y, a, aname, b, bname, c, cname)
  implicit none
  type(xp_number), intent(in) :: y, a
  character(len=*), intent(in) :: aname
  type(xp_number), intent(in), optional :: b, c
  character(len=*), intent(in), optional :: bname, cname
  call do_safe_within('op_dprob_check', mod_numbers_math_name_, private_check)
contains
  subroutine private_check
    call assert(is_allocated(y), err_notAlloc_, 'y')
    call warn(has_dx(y), warn_hasdx_, 'y')
    call assert(is_allocated(a), err_notAlloc_, aname)
    if (get_rank(a) > 0) call assert(all(shape(y) == shape(a)), err_wrngSz_, aname)
    if (present(b)) then
       call assert(present(bname), err_generic_, 'bname, missing')
       call assert(is_allocated(b), err_notAlloc_, bname)
       if (get_rank(b) > 0) call assert(all(shape(y) == shape(b)), err_wrngSz_, bname)
    end if
    if (present(c)) then
       call assert(present(cname), err_generic_, 'cname, missing')
       call assert(is_allocated(c), err_notAlloc_, cname)
       if (get_rank(c) > 0) call assert(all(shape(y) == shape(c)), err_wrngSz_, cname)
    end if
  end subroutine private_check
end subroutine xp_number__dprob_arg_check

!> @relates number__ldexp
function xp_number__ldexp (y, lam) result(ans)
  implicit none
  type(xp_number), intent(in) :: y, lam
  type(xp_number), pointer :: ans
  call do_safe_within('number__ldexp', mod_numbers_math_name_, private_ldexp)
contains
  subroutine private_ldexp
    real(kind=xp_) :: dtyp
    call number__dprob_arg_check(y, lam, 'lam')
    call number__append(ans, y%shp, has_dx(lam))
    call node__append(op_ldexpon_id_, [y, lam], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_ldexp(y, lam, ans)
  end subroutine private_ldexp
end function xp_number__ldexp

!> @relates number__ldlaplace
function xp_number__ldlaplace (y, mu, lam) result(ans)
  implicit none
  type(xp_number), intent(in) :: y, mu, lam
  type(xp_number), pointer :: ans
  call do_safe_within('number__ldlaplace', mod_numbers_math_name_, private_ldlaplace)
contains
  subroutine private_ldlaplace
    real(kind=xp_) :: dtyp
    call number__dprob_arg_check(y, mu, 'mu', lam, 'lam')
    call number__append(ans, y%shp, has_dx(mu) .or. has_dx(lam))
    call node__append(op_ldlaplace_id_, [y, mu, lam], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_ldlaplace(y, mu, lam, ans)
  end subroutine private_ldlaplace
end function xp_number__ldlaplace

!> @relates number__ldbeta
function xp_number__ldbeta (y, a1, a2) result(ans)
  implicit none
  type(xp_number), intent(in) :: y, a1, a2
  type(xp_number), pointer :: ans
  call do_safe_within('number__ldbeta', mod_numbers_math_name_, private_ldbeta)
contains
  subroutine private_ldbeta
    real(kind=xp_) :: dtyp
    call number__dprob_arg_check(y, a1, 'a1', a2, 'a2')
    call number__append(ans, y%shp, has_dx(a1) .or. has_dx(a2))
    call node__append(op_ldbeta_id_, [y, a1, a2], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_ldbeta(y, a1, a2, ans)
  end subroutine private_ldbeta
end function xp_number__ldbeta

!> @relates number__ldgamma
function xp_number__ldgamma (y, a, b) result(ans)
  implicit none
  type(xp_number), intent(in) :: y, a, b
  type(xp_number), pointer :: ans
  call do_safe_within('number__ldgamma', mod_numbers_math_name_, private_ldgamma)
contains
  subroutine private_ldgamma
    real(kind=xp_) :: dtyp
    call number__dprob_arg_check(y, a, 'a', b, 'b')
    call number__append(ans, y%shp, has_dx(a) .or. has_dx(b))
    call node__append(op_ldgamma_id_, [y, a, b], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_ldgamma(y, a, b, ans)
  end subroutine private_ldgamma
end function xp_number__ldgamma

!> @relates number__ldnorm
function xp_number__ldnorm (y, mu, s) result(ans)
  implicit none
  type(xp_number), intent(in) :: y, mu, s
  type(xp_number), pointer :: ans
  call do_safe_within('number__ldnorm', mod_numbers_math_name_, private_ldnorm)
contains
  subroutine private_ldnorm
    real(kind=xp_) :: dtyp
    call number__dprob_arg_check(y, mu, 'mu', s, 's')
    call number__append(ans, y%shp, has_dx(mu) .or. has_dx(s))
    call node__append(op_ldnorm_id_, [y, mu, s], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_ldnorm(y, mu, s, ans)
  end subroutine private_ldnorm
end function xp_number__ldnorm

!> @relates number__ldmvnorm__1
function xp_number__ldmvnorm__1 (y, mu, E) result(ans)
  implicit none
  type(xp_number), intent(in) :: y, mu, E
  type(xp_number), pointer :: ans
  call do_safe_within('number__ldmvnorm__1', mod_numbers_math_name_, private_do)
contains
  subroutine private_do
    real(kind=xp_) :: dtyp
    call assert(is_allocated(y), err_notAlloc_, 'y')
    call assert(is_allocated(mu), err_notAlloc_, 'mu')
    call assert(is_allocated(E), err_notAlloc_, 'E')
    call assert(get_rank(y) == 1 .or. (get_rank(y) < 3 .and. any(y%shp == 1)), &
         err_generic_, 'y has rank != 1 or all dim > 1')
    call assert(get_rank(mu) == 1 .or. (get_rank(mu) < 3 .and. all(mu%shp == 1)), &
         err_generic_, 'mu has rank != 0 or not all dim != 1')
    call assert(get_size(y) == get_size(mu), err_wrngSz_, 'y or mu')
    call assert(get_rank(E) == 2, err_generic_, 'E has rank /= 2')
    call assert(E%shp(1) == E%shp(2), err_generic_, 'E is not square')
    call assert(get_size(y) == E%shp(1), err_wrngSz_, 'y or E')
    call number__append(ans, [integer::], has_dx(mu) .or. has_dx(E))
    call node__append(op_ldmvnorm1_id_, [y, mu, E], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_ldmvnorm__1(y, mu, E, ans)
  end subroutine private_do
end function xp_number__ldmvnorm__1

!> @relates number__mvnorm_posterior
!! @todo review
subroutine xp_number__mvnorm_posterior (inv, a11, e21, e22, y11, mu11, pmu, pe)
  implicit none
  logical, intent(in) :: inv
  type(xp_number), intent(in) :: a11, e21, e22, y11, mu11
  type(xp_number), intent(out), pointer :: pmu, pe
  call do_safe_within('number__mvnorm_posterior', mod_numbers_math_name_, private_do)
contains
  subroutine private_do
    real(kind=xp_) :: dtyp
    logical :: dx
    call assert(is_allocated(a11), err_notAlloc_, 'a11')
    call assert(get_rank(a11) == 2, err_generic_, 'a11 has rank != 2')
    call assert(is_allocated(e21), err_notAlloc_, 'e21')
    call assert(get_rank(e21) == 2, err_generic_, 'e21 has rank != 2')
    call assert(is_allocated(e22), err_notAlloc_, 'e22')
    call assert(get_rank(e22) == 2, err_generic_, 'e22 has rank != 2')
    call assert(is_allocated(y11), err_notAlloc_, 'y11')
    call assert(get_rank(y11) == 1 .or. (get_rank(y11) < 3 .and. any(y11%shp == 1)), &
         err_generic_, 'y11 has rank != 1 or all dim > 1')
    call assert(is_allocated(mu11), err_notAlloc_, 'mu11')
    call assert(get_rank(mu11) == 0 .or. (get_rank(mu11) < 3 .and. all(mu11%shp == 1)), &
         err_generic_, 'mu11 has rank != 0 or not all dim != 1')
    call assert(a11%shp(1) == e21%shp(1), err_wrngSz_, 'a11 or e21')
    call assert(a11%shp(1) == y11%shp(1), err_wrngSz_, 'y11 or a11')
    call assert(e21%shp(1) == e22%shp(1), err_wrngSz_, 'e21 or e22')
    dx = has_dx(a11) .or. has_dx(e21) .or. has_dx(y11) .or. has_dx(mu11)
    call number__append(pmu, [e21%shp(1)], dx)
    call number__append(pe, [e21%shp(1), e21%shp(1)], dx .or. has_dx(e22))
    call node__append(merge(op_mvnormpost2_id_, op_mvnormpost1_id_, inv), &
         [a11, e21, e22, y11, mu11], [pmu, pe], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) then
       if (inv) then
          call op_mvnorm_posterior__2(a11, e21, e22, y11, mu11, pmu, pe)
       else
          call op_mvnorm_posterior__1(a11, e21, e22, y11, mu11, pmu, pe)
       end if
    end if
  end subroutine private_do
end subroutine xp_number__mvnorm_posterior

