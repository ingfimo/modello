  !> @addtogroup numbers_math__stats_
  !! @{

  !> Checks that the arguments of a dprob operations are consistent
  !! @param[in] y 'number', observations
  !! @param[in] a 'number', proibability density paramter
  !! @param[in] aname character, name identifying the a paramter
  !! @param[in] b 'number', proibability density paramter
  !! @param[in] bname character, name identifying the b paramter
  !! @param[in] c 'number', proibability density paramter
  !! @param[in] cname character, name identifying the c paramter
  interface number__dprob_arg_check
     module procedure sp_number__dprob_arg_check
     module procedure dp_number__dprob_arg_check
  end interface number__dprob_arg_check

  !> Exponential distribution - log-density
  !! @param[in] y 'number', observations
  !! @param[in] lam 'number', rate parameter
  interface number__ldexp
     module procedure sp_number__ldexp
     module procedure dp_number__ldexp
  end interface number__ldexp

  !> Laplace distribution - log-density
  !! @param[in] y 'number', observations
  !! @param[in] mu 'number', location parameter
  !! @param[in] lam 'number', rate parameter
  interface number__ldlaplace
     module procedure sp_number__ldlaplace
     module procedure dp_number__ldlaplace
  end interface number__ldlaplace

  !> Beta distribution - log-density
  !! @param[in] y 'number', observations
  !! @param[in] a1 'number', shape 1 parameter
  !! @param[in] a2 'number', shape 2 parameter
  interface number__ldbeta
     module procedure sp_number__ldbeta
     module procedure dp_number__ldbeta
  end interface number__ldbeta

  !> Gamma distribution - log-density
  !! @param[in] y 'number', observations
  !! @param[in] a 'number', shape parameter
  !! @param[in] b 'number', rate parameter
  interface number__ldgamma
     module procedure sp_number__ldgamma
     module procedure dp_number__ldgamma
  end interface number__ldgamma

  !> Normal distribution - log-density
  !! @param[in] y 'number', observations
  !! @param[in] mu 'number', mean parameter
  !! @param[in] s 'number', standard deviation parameter
  interface number__ldnorm
     module procedure sp_number__ldnorm
     module procedure dp_number__ldnorm
  end interface number__ldnorm

  !> Multivariante Normal Distribution - log-density
  !! @param[in] y 'number, observations
  !! @param[in] mu 'number', mean
  !! @param[in] E 'number', covariance matrix
  interface number__ldmvnorm__1
     module procedure sp_number__ldmvnorm__1
     module procedure dp_number__ldmvnorm__1
  end interface number__ldmvnorm__1

  !> Posterior Multivariate Normal distribution
  !! @param[in] inv logical, if .true. partition 1,1 needs to be specified w.r.t. its precision matrix
  !! @param[in] a11 number, partition 1,1 precision matrix
  !! @param[in] e21 number, partition 2,1 covariance matrix
  !! @param[in] e22 number, partition 2,2 covariance matrix
  !! @param[in] y11 number, partition 1,1 observations
  !! @param[in] mu11 number, partition 1,1 mean
  !! @param[inout] pmu number, pointer, posterior mean
  !! @param[inout] pe number, pointer, posterior covariance
  interface number__mvnorm_posterior
     module procedure sp_number__mvnorm_posterior
     module procedure dp_number__mvnorm_posterior
  end interface number__mvnorm_posterior

  !> @}
