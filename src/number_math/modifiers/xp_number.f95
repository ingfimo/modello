subroutine xp_number__assign (x1, x2)
  implicit none
  type(xp_number), intent(in) :: x1
  type(xp_number), intent(inout) :: x2
  call do_safe_within("xp_number__assign", mod_numbers_math_name_, private_assign)
contains
  subroutine private_assign
    real(kind=xp_) :: dtyp
    call assert(all(x1%shp == x2%shp), err_generic_, "x1 and x2 have incopatible shapes")
    call node__append(op_assign_id_, [x1, x2], [xp_number::], [integer::], &
         [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) call op_assign(x1, x2)
  end subroutine private_assign
end subroutine xp_number__assign

!> @realates number__slice
function xp_number__slice (x, s) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  integer, intent(in), contiguous, target :: s(:,:)
  type(xp_number), pointer :: ans
  call do_safe_within("number__slice", mod_numbers_math_name_, private_slice)
contains
  subroutine private_slice
    real(kind=xp_) :: dtyp
    integer :: indx(size(s, 2))
    call assert(is_allocated(x), err_notAlloc_, "x")
    call assert(size(s, 1) == get_rank(x), err_wrngArg_, "s")
    call assert(all(minval(s, 2) > 0), err_oorng_, "s")
    call assert(all(maxval(s, 2) <= x%shp), err_oorng_, "s")
    call number__append(ans, slice_shape(s), has_dx(x))
    if (err_free()) call get_slice_flat_indexes(s, x%shp, indx)
    call node__append(op_slice_id_, [x], [ans], indx, [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) call op_slice(x, ans, indx)
  end subroutine private_slice
end function xp_number__slice

!> @relates number__flat_slice
function xp_number__flat_slice (x, s) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  integer, intent(in) :: s(:)
  type(xp_number), pointer :: ans
  call do_safe_within("number__flat_slice", mod_numbers_math_name_, private_slice)
contains
  subroutine private_slice
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, "x")
    call assert(minval(s) > 0 .and. maxval(s) <= get_size(x), err_oorng_, 's')
    call number__append(ans, [size(s)], has_dx(x))
    call node__append(op_flatslice_id_, [x], [ans], s, [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) call op_flatslice(x, ans, s)
  end subroutine private_slice
end function xp_number__flat_slice

!> @relates number__contiguous_slice
function xp_number__contiguous_slice (x, s1, s2) result(ans)
  type(xp_number), intent(in) :: x
  integer, intent(in) :: s1, s2
  type(xp_number), pointer :: ans
  call do_safe_within("number__slice", mod_numbers_math_name_, private_slice)
contains
  subroutine private_slice
    real(kind=xp_) :: dtyp
    call number__append_contiguous_slice(ans, x, s1, s2)
    call node__append(op_contiguous_slice_id_, [x], [ans], [s1, s2], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
  end subroutine private_slice
end function xp_number__contiguous_slice

!> @relates number__reshape
function xp_number__reshape (x, shp) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  integer, intent(in) :: shp(:)
  type(xp_number), pointer :: ans
  call do_safe_within("number__reshape", mod_numbers_math_name_, private_reshape)
contains
  subroutine private_reshape
    real(kind=xp_) :: dtyp
    call number__append_reshape(ans, x, shp)
    call node__append(op_reshape_id_, [x], [ans], shp, [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
  end subroutine private_reshape
end function xp_number__reshape

!> @relates number__drop_shape
function xp_number__drop_shape (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within("number__drop_shape", mod_numbers_math_name_, private_drop_shape)
contains
  subroutine private_drop_shape
    real(kind=xp_) :: dtyp
    call number__append_drop_shape(ans, x)
    call node__append(op_drop_shape_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
  end subroutine private_drop_shape
end function xp_number__drop_shape

!> @relates number__bind
function xp_number__bind (x1, x2, k) result(ans)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  integer, intent(in) :: k
  type(xp_number), pointer :: ans
  call do_safe_within ('number__bind', mod_numbers_math_name_, private_bind)
contains
  subroutine private_bind
    real(kind=xp_) :: dtyp
    call number__append_bind(ans, x1, x2, k)
    call node__append(op_bind_id_, [x1, x2], [ans], [k], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
  end subroutine private_bind
end function xp_number__bind

!> @relates number__embeddings
function xp_number__embeddings (f, x, shp) result(ans)
  implicit none
  type(xp_number), intent(in) :: f, x
  integer, intent(in) :: shp(:)
  type(xp_number), pointer :: ans
  call do_safe_within('number__embeddings', mod_numbers_math_name_, private_emb)
contains
  subroutine private_emb
    real(kind=xp_) :: dtyp
    integer :: n
    n = 0
    call assert(is_allocated(f), err_notAlloc_, 'f')
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(get_rank(f) < 2, err_generic_, 'f has rank > 1')
    call assert(get_rank(x) == 2, err_generic_, 'x has rank /= 2')
    if (get_rank(f) == 0) then
       call assert(product(shp) == x%shp(1), err_wrngSz_, 'x or shp')
    else
       call assert(product(shp)/shp(size(shp)) == x%shp(1), err_wrngSz_, 'x or shp')
       call assert(shp(size(shp)) == get_size(f), err_wrngSz_, 'x or shp')
    end if
    call assert(nint(maxval(f%v)) <= x%shp(get_rank(x)), err_oorng_, 'f')
    call number__append(ans, shp, has_dx(x))
    call node__append(op_embeddings_id_, [f, x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) call op_embeddings(f, x, ans)
  end subroutine private_emb
end function xp_number__embeddings

!> @relates number__transpose
function xp_number__transpose (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within('number__transpose', mod_numbers_math_name_, private_transpose)
contains
  subroutine private_transpose
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(get_rank(x) == 2, err_generic_, 'rank(x) /= 2')
    call number__append(ans, [x%shp(2),x%shp(1)], has_dx(x))
    call node__append(op_transpose_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) call op_transpose(x, ans)
  end subroutine private_transpose
end function xp_number__transpose

!> @relates number__feeding_number
function xp_number__feeding_number (x, indexes, shp, bsz) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  integer, intent(in) :: indexes(:), shp(:), bsz
  type(xp_number), pointer :: ans
  call do_safe_within("xp_number__feeding_number", mod_numbers_math_name_, private_do)
contains
  subroutine private_do
    integer :: i
    call assert(is_allocated(x), err_notAlloc_, "x")
    call assert(get_rank(x) == 2, err_generic_, "rank(x) /= 2")
    call assert(get_size(x, 1) == product(shp), err_wrngArg_, "shp")
    call assert(size(indexes) >= bsz, err_wrngArg_, "bsz")
    call assert(maxval(indexes) <= get_size(x, 2), err_wrngArg_, "indexes or x")
    call number__append(ans, [shp, bsz], .false.)
    call node__append(op_feeding_number_id_, [x], [ans], [bsz, size(indexes), 0, 0, indexes], &
         [real(kind=xp_)::], [integer::], [integer::])
    if (nodei_is_set()) i = flush_nodei()
    if (err_free() .and. (.not. within_posts())) call op_feeding_number(x, ans, indexes(:bsz))
  end subroutine private_do
end function xp_number__feeding_number

!> @relates number__feeding_sequence
function xp_number__feeding_sequence (x, indexes, shp, l, bsz, b) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  integer, intent(in) :: indexes(:), shp(:), l, bsz, b
  type(xp_number), pointer :: ans
  call do_safe_within("xp_number__feeding_sequence", mod_numbers_math_name_, private_do)
contains
  subroutine private_do
    integer :: i, len
    call assert(is_allocated(x), err_notAlloc_, "x")
    call assert(get_rank(x) == 2, err_generic_, "rank(x) /= 2")
    call assert(get_size(x, 1) == product(shp), err_wrngArg_, "shp")
    call assert(size(indexes) >= bsz, err_wrngArg_, "bsz")
    call assert(maxval(indexes) <= get_size(x, 2), err_wrngArg_, "indexes or x")
    if (b > 0) then
       call number__append(ans, [shp, bsz, l], .false.)
    else
       call number__append(ans, [shp, l, bsz], .false.)
    end if
    call node__append(op_feeding_sequence_id_, [x], [ans], [bsz, size(indexes), l, b, indexes], &
         [real(kind=xp_)::], [integer::], [integer::])
    if (nodei_is_set()) i = flush_nodei()
    if (err_free() .and. (.not. within_posts())) call op_feeding_sequence(x, ans, indexes(:bsz), l, b)
  end subroutine private_do
end function xp_number__feeding_sequence

!> @relates number__feed
function xp_number__feed (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x(:)
  type(xp_node), pointer :: ans
  integer :: bsz, dsz
  call do_safe_within("xp_number__feed", mod_numbers_math_name_, private_do)
contains
  subroutine private_do
    real(kind=xp_) :: dtyp
    integer :: i
    integer, allocatable :: s1(:), s2(:)
    call private_check_allocation
    call err_safe(private_check_bsz)
    call err_safe(private_check_dsz)
    call node__append(op_feed_id_, [xp_number::], [xp_number::], [bsz, [(number__get_nd(x(i)), i = 1, size(x))]], &
         [real(kind=xp_)::], [(i, i=1, dsz)], [0])
    call get_node(get_nodei(), ans)
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) call ans%op(ans%attrs, ans%states)
  end subroutine private_do
  subroutine private_check_allocation
    integer :: i
    do i = 1, size(x)
       call assert(is_allocated(x(i)), err_notAlloc_, "x(i)")
       if (.not. err_free()) exit
    end do
  end subroutine private_check_allocation
  subroutine private_check_bsz
    integer :: i, j
    type(xp_node), pointer :: nd
    call get_node(number__get_nd(x(1)), nd)
    if (err_free()) then
       bsz = nd%attrs%ipar(1)
       do i = 2, size(x)
          call get_node(number__get_nd(x(i)), nd)
          if (.not. err_free()) exit
          j = nd%attrs%ipar(1)
          call assert(bsz == j, err_wrngSz_, "bsz")
          if (.not. err_free()) exit
       end do
    end if
  end subroutine private_check_bsz
  subroutine private_check_dsz
    integer :: i, j
    dsz = xp_NODES_(number__get_nd(x(1)))%attrs%ipar(2)
    do i = 2, size(x)
       j = xp_NODES_(number__get_nd(x(i)))%attrs%ipar(2)
       call assert(dsz == j, err_wrngSz_, "dsz")
       if (.not. err_free()) exit
    end do  
  end subroutine private_check_dsz
end function xp_number__feed

