  !> @addtogroup numbers_math__modifiers_
  !! @{

  interface number__assign
     module procedure sp_number__assign
     module procedure dp_number__assign
  end interface number__assign
  
  !> General @b Number Slice
  !!
  !! Returns slice of a number as specified by the matrix s
  !!
  !! @param[in] x 'number' to slice
  !! @param[in] s integer(:,:), slice matrix
  interface number__slice
     module procedure sp_number__slice
     module procedure dp_number__slice
  end interface number__slice

  !> Flat @b Number Slice
  !!
  !! Returns the flat slice of a 'number' as
  !! specified by the vector s
  !!
  !! @param[in] x 'number' to slice
  !! @param[in] s integer(:), slice vector
  interface number__flat_slice
     module procedure sp_number__flat_slice
     module procedure dp_number__flat_slice
  end interface number__flat_slice

  !> Contiguous @b Number Slice
  !!
  !! Returns a contiguous slice of a 'number'.
  !! A contiguous slice is done along the dominant dimension (columns).
  !! Data are not copied be refernece through a pointer.
  !!
  !! @param[in] x 'number' to slice
  !! @param[in] s1 integer, initial index of the slice
  !! @param[in] s2 integer, final index of the slice
  interface number__contiguous_slice
     module procedure sp_number__contiguous_slice
     module procedure dp_number__contiguous_slice
  end interface number__contiguous_slice

  !> Reshape @b Number
  !!
  !! Returns a 'number' reshaping another one.
  !! Data are not copied but referred through a pointer.
  !!
  !! @param[in] x 'number' to slice
  !! @param[in] shp integer(:), new shape vector
  interface number__reshape
     module procedure sp_number__reshape
     module procedure dp_number__reshape
  end interface number__reshape

  !> Drop @b Number Shape
  !!
  !! Returns a 'number' dropping all the dimensions collapsed to 1.
  !! Data are not copied but referenced through a pointer.
  !!
  !! @param[in] x 'number'
  interface number__drop_shape
     module procedure sp_number__drop_shape
     module procedure dp_number__drop_shape
  end interface number__drop_shape

  !> Bind @b Number
  !!
  !! Returns the bind between two 'numbers' along the specifide dimension.
  !! 
  !! @param[in] x1,x2 'numbers' to bind
  !! @param[in] k integer, dimension index
  interface number__bind
     module procedure sp_number__bind
     module procedure dp_number__bind
  end interface number__bind

  !> Transpose @b Number
  !!
  !! Returns the transpose of a @b number of rank 2 (matrix)
  !!
  !! @param[in] x1 @b number of rank 2, matrix to transpose
  interface number__transpose
     module procedure sp_number__transpose
     module procedure dp_number__transpose
  end interface number__transpose

  !> Feeding @a Number
  !!
  !! Returns a @a number representing a data batch with data taken from the feeding @a number.
  !! The result @a number will have shape [data record shape, batch size]
  !!
  !! @param[in] x @b number of rank 2, feeding number containing the data records as columns
  !! @param[in] shp @b integer(:), shape to give to each data record
  !! @param[in] bsz @ integer, batch size
  interface number__feeding_number
     module procedure sp_number__feeding_number
     module procedure dp_number__feeding_number
  end interface number__feeding_number

  !> Feeding @b Number Sequence
  !!
  !! Returns a @a number represeting a data batch of sequences,
  !! with data taken from the feeding @a number.
  !! The result @a number will have shape [data record shape, sequence length, batch size]
  !!
  !! @param[in] x @c number of rank 2, feeding number having as columns data records
  !! @param[in] shp @c integer(:), shape to give to each data record
  !! @param[in] l @c integer, sequence length
  !! @param[in] bsz @c integer, batch size
  interface number__feeding_sequence
     module procedure sp_number__feeding_sequence
     module procedure dp_number__feeding_sequence
  end interface number__feeding_sequence

  !> Feed @a Numbers
  !!
  !! Given an array of @a numbers created with @b feeding_* fucntions, feed them with
  !! data contained in the feeding @a numbers
  !!
  !! @param[in] x @c number(:), array of @a numbers to feed
  !! @return Nothing
  interface number__feed
     module procedure sp_number__feed
     module procedure dp_number__feed
  end interface number__feed

  !> Embeddings
  !!
  !! Given a @a number containing embedding vectors and a @a number of indexes
  !! returns a @a number containing the corresponding embeddings
  !!
  !! @param[in] f @c number rank 1, embedding indexes
  !! @param[in] x @c number of rank 2, matrix having as columns the embedding vectors
  !! @param[in] shp @c integer(:), shape to give to each embedding vector
  !! @return A @a number of shape [shp, size(f)] containing the embeddings corresponding to the give indexes
  interface number__embeddings
     module procedure sp_number__embeddings
     module procedure dp_number__embeddings
  end interface number__embeddings
