  !> @addtogroup numbers_math__objectives_
  !! @{

  !> Checks that the inputs of an objective operation are consistent.
  !! @param[in] x1 'number'
  !! @param[in] x2 'number'
  interface number__obj_arg_check
     module procedure sp_number__obj_arg_check
     module procedure dp_number__obj_arg_check
  end interface number__obj_arg_check

  !> Binary Entropy - cross-entropy for a binary variable.
  !! @param[in] y 'number', target
  !! @param[in] yh 'number', prediction
  interface number__binentropy
     module procedure sp_number__binentropy
     module procedure dp_number__binentropy
  end interface number__binentropy

  interface number__logit_binentropy
     module procedure sp_number__logit_binentropy
     module procedure dp_number__logit_binentropy
  end interface number__logit_binentropy

  interface number__crossentropy
     module procedure sp_number__crossentropy
     module procedure dp_number__crossentropy
  end interface number__crossentropy

  interface number__logit_crossentropy
     module procedure sp_number__logit_crossentropy__1
     module procedure dp_number__logit_crossentropy__1
     module procedure sp_number__logit_crossentropy__2
     module procedure dp_number__logit_crossentropy__2
  end interface number__logit_crossentropy

  interface number__mse
     module procedure sp_number__mse
     module procedure dp_number__mse
  end interface number__mse

  interface number__mae
     module procedure sp_number__mae
     module procedure dp_number__mae
  end interface number__mae
  
  interface number__lkhnorm
     module procedure sp_number__lkhnorm__1
     module procedure sp_number__lkhnorm__2
     module procedure dp_number__lkhnorm__1
     module procedure dp_number__lkhnorm__2
  end interface number__lkhnorm
