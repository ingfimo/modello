!> @defgroup numbers_math_objectives_ Objective Operations Between Numbers
!! These procedures define the objective functions suitable for optimisation.
!! @details No broadcasting is allowed and the shape of the target and prediction
!! must be the same.

!> @relates number__obj_arg_check
subroutine xp_number__obj_arg_check (y, yh)
  implicit none
  type(xp_number), intent(in) :: y, yh
  call do_safe_within('obj_check', mod_numbers_math_name_, private_check)
contains
  subroutine private_check
    call assert(is_allocated(y), err_notAlloc_, 'x1')
    call assert(is_allocated(yh), err_notAlloc_, 'x2')
    call warn(has_dx(y), warn_generic_, "y has dx")
    call assert(get_size(y) == get_size(yh), err_wrngSz_, 'x1 or x2')
  end subroutine private_check
end subroutine xp_number__obj_arg_check

!> @relates number__binentropy
function xp_number__binentropy (y, yh) result(ans)
  implicit none
  type(xp_number), intent(in) :: y, yh
  type(xp_number), pointer :: ans
  call do_safe_within('xp_number__binentropy', mod_numbers_math_name_, private_binentropy)
contains
  subroutine private_binentropy
    real(kind=xp_) :: dtyp
    call number__obj_arg_check(y, yh)
    call number__append(ans, [integer::], has_dx(yh))
    call node__append(op_binentropy_id_, [y, yh], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_binentropy(y, yh, ans)
  end subroutine private_binentropy
end function xp_number__binentropy

!> @relates number__logit_binentropy
function xp_number__logit_binentropy (y, yh) result(ans)
  implicit none
  type(xp_number), intent(in) :: y, yh
  type(xp_number), pointer :: ans
  call do_safe_within("xp_number__logit_binentropy", mod_numbers_math_name_, private_logit_binentropy)
contains
  subroutine private_logit_binentropy
    real(kind=xp_) :: dtyp
    call number__obj_arg_check(y, yh)
    call number__append(ans, [integer::], has_dx(yh))
    call node__append(op_logit_binentropy_id_, [y, yh], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_logit_binentropy(y, yh, ans)
  end subroutine private_logit_binentropy
end function xp_number__logit_binentropy

!> @relates number__crossentropy
function xp_number__crossentropy (y, yh) result(ans)
  implicit none
  type(xp_number), intent(in) :: y, yh
  type(xp_number), pointer :: ans
  call do_safe_within('xp_number__corss_entropy__1', mod_numbers_math_name_, private_crossentropy)
contains
  subroutine private_crossentropy
    real(kind=xp_) :: dtyp
    call number__obj_arg_check(y, yh)
    call number__append(ans, [integer::], has_dx(yh))
    call node__append(op_crossentropy_id_, [y, yh], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_crossentropy(y, yh, ans)
  end subroutine private_crossentropy
end function xp_number__crossentropy

!> @relates logit_crossentropy
function xp_number__logit_crossentropy__1 (y, yh) result(ans)
  implicit none
  type(xp_number), intent(in) :: y, yh
  type(xp_number), pointer :: ans
  call do_safe_within("xp_number__logit_crossentropy", mod_numbers_math_name_, private_logit_crossentropy)
contains
  subroutine private_logit_crossentropy
    real(kind=xp_) :: dtyp
    call number__obj_arg_check(y, yh)
    call number__append(ans, [integer::], has_dx(yh))
    call node__append(op_logit_crossentropy1_id_, [y, yh], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_logit_crossentropy(y, yh, ans)
  end subroutine private_logit_crossentropy
end function xp_number__logit_crossentropy__1

function xp_number__logit_crossentropy__2 (y, yh, k) result(ans)
  implicit none
  type(xp_number), intent(in) :: y, yh
  integer, intent(in) :: k
  type(xp_number), pointer :: ans
  call do_safe_within("xp_number__logit_crossentropy__2", mod_numbers_math_name_, private_logit_crossentropy)
contains
  subroutine private_logit_crossentropy
    real(kind=xp_) :: dtyp
    integer :: i, indexes(get_size(y) + 1)
    call number__obj_arg_check(y, yh)
    call number__append(ans, [integer::], has_dx(yh))
    if (err_free()  .and. (.not. within_posts())) call get_slices_along_dim(y%shp, [k], indexes(2:), indexes(1))
    call node__append(op_logit_crossentropy2_id_, [y, yh], [ans], indexes, [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_logit_crossentropy(y, yh, ans, indexes)
  end subroutine private_logit_crossentropy
end function xp_number__logit_crossentropy__2

!> @relates number__mse
function xp_number__mse (y, yh) result(ans)
  type(xp_number), intent(in) :: y, yh
  type(xp_number), pointer :: ans
  call do_safe_within("number__mse", mod_numbers_math_name_, private_mse)
contains
  subroutine private_mse
    real(kind=xp_) :: dtyp
    call number__obj_arg_check(y, yh)
    call number__append(ans, [integer::], has_dx(yh))
    call node__append(op_mse_id_, [y, yh], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_mse(y, yh, ans)
  end subroutine private_mse
end function xp_number__mse

!> @relates number__mae
function xp_number__mae (y, yh) result(ans)
  type(xp_number), intent(in) :: y, yh
  type(xp_number), pointer :: ans
  call do_safe_within("number__mae", mod_numbers_math_name_, private_mae)
contains
  subroutine private_mae
    real(kind=xp_) :: dtyp
    call number__obj_arg_check(y, yh)
    call number__append(ans, [integer::], has_dx(yh))
    call node__append(op_mae_id_, [y, yh], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_mae(y, yh, ans)
  end subroutine private_mae
end function xp_number__mae

!> @relates number__lkhnorm
function xp_number__lkhnorm__1 (y, mu, s) result(ans)
  implicit none
  type(xp_number), intent(in) :: y, mu, s
  type(xp_number), pointer :: ans
  call do_safe_within('xp_number__lkhnorm__1', mod_numbers_math_name_, private_lkh)
contains
  subroutine private_lkh
    real(kind=xp_) :: dtyp
    call assert(is_allocated(y), err_notAlloc_, 'y')
    call assert(is_allocated(mu), err_notAlloc_, 'mu')
    call assert(is_allocated(s), err_notAlloc_, 's')
    call warn(has_dx(y), warn_hasdx_, 'y')
    call assert(all(y%shp == mu%shp) .and. all(y%shp == s%shp), &
         err_wrngSz_, 'y, or mu or s')
    call number__append(ans, [integer::], has_dx(mu) .or. has_dx(s))
    call node__append(op_lkhnorm1_id_, [y, mu, s], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_lkhnorm(y, mu, s, ans)
  end subroutine private_lkh
end function xp_number__lkhnorm__1

!> @relates number__lkhnorm
function xp_number__lkhnorm__2 (y, mu, s, w) result(ans)
  implicit none
  type(xp_number), intent(in) :: y, mu, s, w
  type(xp_number), pointer :: ans
  call do_safe_within('xp_number__lkhnorm__1', mod_numbers_math_name_, private_lkh)
contains
  subroutine private_lkh
    real(kind=xp_) :: dtyp
    call assert(is_allocated(y), err_notAlloc_, 'y')
    call assert(is_allocated(mu), err_notAlloc_, 'mu')
    call assert(is_allocated(s), err_notAlloc_, 's')
    call assert(is_allocated(w), err_notAlloc_, 'w')
    call warn(has_dx(y), warn_hasdx_, 'y')
    call warn(has_dx(w), warn_hasdx_, 'w')
    call assert(all(y%shp == mu%shp) .and. all(y%shp == s%shp) .and. &
         all(y%shp == w%shp), err_wrngSz_, 'y, or mu or s or w')
    call number__append(ans, [integer::], has_dx(mu) .or. has_dx(s))
    call node__append(op_lkhnorm2_id_, [y, mu, s, w], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_lkhnorm(y, mu, s, w, ans)
  end subroutine private_lkh
end function xp_number__lkhnorm__2
