  !> @addtogroup numbers_math__kernels_
  !! @{
  
  !> Checks the input arguments of a kernel
  !! @param[in] x1 number, input matrix
  !! @param[in] x2 number, input matrix
  !! @param[in] a number, hyper paramters
  !! @param[in] b number, hyper parameter
  interface number__krn_arg_check   
     module procedure sp_number__krn_arg_check
     module procedure dp_number__krn_arg_check
  end interface number__krn_arg_check

  !> Squared exponential kernel
  !! @param[in] x1 'number', feature vector
  !! @param[in] x2 'number', feature vector
  !! @param[in] a 'number', amplitude parameter
  !! @param[in] b 'number', rate parameter
  interface number__ksqexp
     module procedure sp_number__ksqexp
     module procedure dp_number__ksqexp
  end interface number__ksqexp
  
  !> @}
