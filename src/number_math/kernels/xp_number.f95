!> @defgroup number_math__kernels_ Kernel (or Covariance) Functions
!! @author Monari Filippo
!! @{

!> @relates number__krn_arg_check
subroutine xp_number__krn_arg_check (shpout, x1, x2, a, b)
  implicit none
  type(xp_number), intent(in) :: x1, x2
  type(xp_number), intent(in) :: a, b
  integer, intent(out), allocatable :: shpout(:)
  call do_safe_within('number__krn_arg_check', mod_numbers_math_name_, private_check)
contains
  subroutine private_check
    call assert(is_allocated(x1), err_notAlloc_, 'x1')
    call assert(is_allocated(x2), err_notAlloc_, 'x2')
    call assert(is_allocated(a), err_notAlloc_, 'a')
    call assert(is_allocated(b), err_notAlloc_, 'b')
    call err_safe(private_check_shape)
  end subroutine private_check
  subroutine private_check_shape
    call assert(get_rank(a) == 0, err_wrngSz_, 'a')
    call assert(get_rank(x1) == 2 .and. get_rank(x2) == 2, err_wrngSz_, "x1 or x2.")
    call assert(x1%shp(1) == x2%shp(1), err_wrngSz_, 'x1 or x2')
    call assert(get_rank(b) == 0 .or. get_size(b) == x1%shp(1), err_wrngSz_, 'b')
    if (err_free()) call alloc(shpout, [x2%shp(2), x1%shp(2)], 'shpout')
  end subroutine private_check_shape
end subroutine xp_number__krn_arg_check

!> @relates number__ksqexp
function xp_number__ksqexp (x1, x2, a, b) result(ans)
  implicit none
  type(xp_number), intent(in) :: x1, x2, a, b
  type(xp_number), pointer :: ans
  call do_safe_within('number__ksqexp', mod_numbers_math_name_, private_ksqexp)
contains
  subroutine private_ksqexp
    real(kind=xp_) :: dtyp
    integer, allocatable :: shpout(:)
    call number__krn_arg_check(shpout, x1, x2, a, b)
    call number__append(ans, shpout, has_dx(x1) .or. has_dx(x2) .or. &
         has_dx(a) .or. has_dx(b))
    call node__append(op_ksqexpon_id_, [x1, x2, a, b], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_ksqexp(x1, x2, a, b, ans)
  end subroutine private_ksqexp
end function xp_number__ksqexp
