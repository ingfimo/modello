subroutine xp_number__conv_check_args (x, k, stride, dilation, pad, d)
  implicit none
  type(xp_number), intent(in) :: x, k
  integer, intent(in) :: stride(:), dilation(:), pad(:), d
  call assert(is_allocated(x), err_notAlloc_, "x")
  call assert(is_allocated(k), err_notAlloc_, "k")
  call assert(get_rank(x) == d + 2, err_wrngSz_, "x")
  call assert(get_rank(k) == d + 2, err_wrngSz_, "k")
  call assert(d == size(dilation), err_wrngSz_, "dilaton")
  call assert(all(dilation >= 0), err_wrngArg_, "dilation")
  call assert(d == size(stride), err_wrngSz_, "stride")
  call assert(all(stride > 0), err_wrngArg_, "stride")
  call assert(d == size(pad), err_wrngSz_, "pad")
  call assert(all(pad >= 0), err_wrngArg_, "pad")
  call assert(all(dilated_size(k%shp(1:d), dilation) <= product(x%shp(1:d))), err_wrngSz_, "x or k")
  call assert(get_size(x, d+1) == get_size(k, d+1), err_wrngSz_, "x or k")
end subroutine xp_number__conv_check_args

subroutine xp_number__conv__0 (ans, x, k, stride, dilation, pad, d)
  implicit none
  type(xp_number), intent(in) :: x, k
  integer, intent(in) :: stride(:), dilation(:), pad(:), d
  type(xp_number), intent(out), pointer :: ans
  call do_safe_within("xp_number__conv__0", mod_numbers_math_name_, private_do)
contains
  subroutine private_do
    call xp_number__conv_check_args(x, k, stride, dilation, pad, d)
    call err_safe(private_conv)
  end subroutine private_do
  subroutine private_conv
    real(kind=xp_) :: dtyp
    integer :: ci, co, b, li, lk, lo(d)
    integer :: slices(6 + product(k%shp(1:d)) * product(conv1d_lout(x%shp(1:d), k%shp(1:d), stride, dilation, pad)))
    b = get_size(x, d + 2)
    li = product(x%shp(1:d))
    lk = product(k%shp(1:d))
    ci = get_size(x, d + 1)
    co = k%shp(d + 2)
    lo = conv1d_lout(x%shp(1:d), k%shp(1:d), stride, dilation, pad)
    select case (d)
    case (1)
       call kernel_slices_1d(get_size(x, 1), get_size(k, 1), stride(1), dilation(1), pad(1), slices(7:))
    case (2)   
       call kernel_slices_2d(x%shp(1:d), k%shp(1:d), stride, dilation, pad, slices(7:))
    case (3)
       call kernel_slices_3d(x%shp(1:d), k%shp(1:d), stride, dilation, pad, slices(7:))
    case default
       call raise_error("d", err_unknwnVal_)
    end select
    call number__append(ans, [lo, co, b], has_dx(x) .or. has_dx(k))
    slices(1:6) = [ci, co, b, li, lk, product(lo)]
    call node__append(op_conv_id_, [x, k], [ans], slices, [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) call op_conv(x, k, ans, slices)
  end subroutine private_conv
end subroutine xp_number__conv__0

function xp_number__conv1d (x, k, stride, dilation, pad) result(ans)
  implicit none
  type(xp_number), intent(in) :: x, k
  integer, intent(in) :: stride(:), dilation(:), pad(:)
  type(xp_number), pointer :: ans
  integer, parameter :: d = 1
  call do_safe_within("xp_number__conv1d", mod_numbers_math_name_, private_conv)
contains
  subroutine private_conv
    call xp_number__conv__0(ans, x, k, stride, dilation, pad, d)
  end subroutine private_conv
end function xp_number__conv1d

function xp_number__conv2d (x, k, stride, dilation, pad) result(ans)
  implicit none
  type(xp_number), intent(in) :: x, k
  integer, intent(in) :: stride(:), dilation(:), pad(:)
  type(xp_number), pointer :: ans
  integer, parameter :: d = 2
  call do_safe_within("xp_number__conv2d", mod_numbers_math_name_, private_conv)
contains
  subroutine private_conv
    call xp_number__conv__0(ans, x, k, stride, dilation, pad, d)
  end subroutine private_conv
end function xp_number__conv2d

function xp_number__conv3d (x, k, stride, dilation, pad) result(ans)
  implicit none
  type(xp_number), intent(in) :: x, k
  integer, intent(in) :: stride(:), dilation(:), pad(:)
  type(xp_number), pointer :: ans
  integer, parameter :: d = 3
  call do_safe_within("xp_number__conv3d", mod_numbers_math_name_, private_conv)
contains
  subroutine private_conv
    call xp_number__conv__0(ans, x, k, stride, dilation, pad, d)
  end subroutine private_conv
end function xp_number__conv3d

subroutine xp_number__pooling_check_args (x, kshp, stride, dilation, pad, d)
  implicit none
  type(xp_number), intent(in) :: x
  integer, intent(in) :: kshp(:), stride(:), dilation(:), pad(:), d
  call assert(is_allocated(x), err_notAlloc_, "x")
  call assert(get_rank(x) == d + 2, err_wrngSz_, "x")
  call assert(size(kshp) == d, err_wrngSz_, "k")
  call assert(d == size(dilation), err_wrngSz_, "dilaton")
  call assert(all(dilation >= 0), err_wrngArg_, "dilation")
  call assert(d == size(stride), err_wrngSz_, "stride")
  call assert(all(stride > 0), err_wrngArg_, "stride")
  call assert(d == size(pad), err_wrngSz_, "pad")
  call assert(all(pad >= 0), err_wrngArg_, "pad")
  call assert(all(dilated_size(kshp, dilation) <= product(x%shp(1:d))), err_wrngSz_, "x or kshp")
end subroutine xp_number__pooling_check_args

subroutine xp_number__pooling__0 (ans, x, kshp, stride, dilation, pad, d, op)
  implicit none
  type(xp_number), intent(in) :: x
  integer, intent(in) :: kshp(:), stride(:), dilation(:), pad(:), d, op
  type(xp_number), intent(out), pointer :: ans
  call do_safe_within("xp_number__pooling__0", mod_numbers_math_name_, private_do)
contains
  subroutine private_do
    call xp_number__pooling_check_args(x, kshp, stride, dilation, pad, d)
    call err_safe(private_pooling)
  end subroutine private_do
  subroutine private_pooling
    real(kind=xp_) :: dtyp
    integer :: ci, b, li, lk, lo(d)
    integer :: slices(5 + product(kshp) * product(conv1d_lout(x%shp(1:d), kshp, stride, dilation, pad)))
    b = get_size(x, d + 2)
    li = product(x%shp(1:d))
    lk = product(kshp)
    ci = get_size(x, d + 1)
    lo = conv1d_lout(x%shp(1:d), kshp, stride, dilation, pad)
    select case (d)
    case (1)
       call kernel_slices_1d(get_size(x, 1), kshp(1), stride(1), dilation(1), pad(1), slices(6:))
    case (2)   
       call kernel_slices_2d(x%shp(1:d), kshp, stride, dilation, pad, slices(6:))
    case (3)
       call kernel_slices_3d(x%shp(1:d), kshp, stride, dilation, pad, slices(6:))
    case default
       call raise_error("d", err_unknwnVal_)
    end select
    call number__append(ans, [lo, ci, b], has_dx(x))
    slices(1:5) = [ci, b, li, lk, product(lo)]
    call node__append(op, [x], [ans], slices, [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) then
       select case (op)
       case (op_maxpool_id_)
          call op_maxpool(x, ans, slices)
       case default
          call raise_error("d", err_unknwnVal_)
       end select
    end if
  end subroutine private_pooling
end subroutine xp_number__pooling__0

function xp_number__maxpool1d (x, kshp, stride, dilation, pad) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  integer, intent(in) :: kshp(:), stride(:), dilation(:), pad(:)
  type(xp_number), pointer :: ans
  integer, parameter ::d = 1
  call do_safe_within("xp_number__maxpool1d", mod_numbers_math_name_, private_pooling)
contains
  subroutine private_pooling
    call xp_number__pooling__0(ans, x, kshp, stride, dilation, pad, d, op_maxpool_id_)
  end subroutine private_pooling
end function xp_number__maxpool1d

function xp_number__maxpool2d (x, kshp, stride, dilation, pad) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  integer, intent(in) :: kshp(:), stride(:), dilation(:), pad(:)
  type(xp_number), pointer :: ans
  integer, parameter ::d = 2
  call do_safe_within("xp_number__maxpool1d", mod_numbers_math_name_, private_pooling)
contains
  subroutine private_pooling
    call xp_number__pooling__0(ans, x, kshp, stride, dilation, pad, d, op_maxpool_id_)
  end subroutine private_pooling
end function xp_number__maxpool2d

function xp_number__maxpool3d (x, kshp, stride, dilation, pad) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  integer, intent(in) :: kshp(:), stride(:), dilation(:), pad(:)
  type(xp_number), pointer :: ans
  integer, parameter ::d = 3
  call do_safe_within("xp_number__maxpool1d", mod_numbers_math_name_, private_pooling)
contains
  subroutine private_pooling
    call xp_number__pooling__0(ans, x, kshp, stride, dilation, pad, d, op_maxpool_id_)
  end subroutine private_pooling
end function xp_number__maxpool3d



   

  
  
