  interface number__conv1d
     module procedure sp_number__conv1d
     module procedure dp_number__conv1d
  end interface number__conv1d

  interface number__conv2d
     module procedure sp_number__conv2d
     module procedure dp_number__conv2d
  end interface number__conv2d

  interface number__conv3d
     module procedure sp_number__conv3d
     module procedure dp_number__conv3d
  end interface number__conv3d

  interface number__maxpool1d
     module procedure sp_number__maxpool1d
     module procedure dp_number__maxpool1d
  end interface number__maxpool1d

  interface number__maxpool2d
     module procedure sp_number__maxpool2d
     module procedure dp_number__maxpool2d
  end interface number__maxpool2d

   interface number__maxpool3d
     module procedure sp_number__maxpool3d
     module procedure dp_number__maxpool3d
  end interface number__maxpool3d
