!> @relates number__gmmmult_arg_check
subroutine xp_number__gmmmult_arg_check (shpout, ta, tb, A, B, C, alpha, beta)
  implicit none
  integer, intent(out) :: shpout(2)
  integer, intent(in) :: ta, tb
  type(xp_number), intent(in) :: A, B
  type(xp_number), intent(in), optional :: alpha, beta, C
  shpout = 1
  call do_safe_within('xp_number__gmmmult_arg_check', mod_numbers_math_name_, private_check)
contains
  subroutine private_check
    call assert(is_allocated(A), err_notAlloc_, 'A')
    call assert(get_rank(A) == 2, err_generic_, 'A rank /= 2')
    call assert(is_allocated(B), err_notAlloc_, 'B')
    call assert(get_rank(B) == 2, err_generic_, 'B rank /= 2')
    if (present(alpha)) then
       call assert(is_allocated(alpha), err_alloc_, 'alpha')
       call assert(get_rank(alpha) == 0, err_generic_, 'alpha rank /= 0')
    end if
    if (present(beta)) then
       call assert(is_allocated(beta), err_alloc_, 'alpha')
       call assert(get_rank(beta) == 0, err_generic_, 'beta rank /= 0')
    end if
    if (present(C)) then
       call assert(is_allocated(C), err_notAlloc_, 'C')
       call assert(get_rank(C) == 2, err_generic_, 'C rank /= 2')
       if (err_free()) call private_check_shape(C)
    else
       if (err_free()) call private_check_shape
    end if
  end subroutine private_check
  subroutine private_check_shape (C)
    type(xp_number), intent(in), optional :: C
    integer :: mA, kA, nB, kB
    mA = merge(A%shp(1), A%shp(2), ta < 1)
    kA = merge(A%shp(2), A%shp(1), ta <1)
    nB = merge(B%shp(2), B%shp(1), tB < 1)
    kB = merge(B%shp(1), B%shp(2), tB < 1)
    call assert(kA == kB, err_wrngSz_, 'A or B')
    if (present(C)) call assert(mA == C%shp(1) .and. nB == C%shp(2), err_wrngSz_, 'C')
    if (err_free()) shpout = [mA, nB]
  end subroutine private_check_shape
end subroutine xp_number__gmmmult_arg_check

!>  @relates number__gmmmult
function xp_number__gmmmult__1 (transA, transB, alpha, A, B, beta, C) result(CC)
  implicit none
  integer, intent(in) :: transA, transB
  type(xp_number), intent(in) :: alpha, beta
  type(xp_number), intent(in) :: A, B, C
  type(xp_number), pointer :: CC
  call do_safe_within('xp_number__gmmmult__1', mod_numbers_math_name_, private_gmmmult)
contains
  subroutine private_gmmmult
    real(kind=xp_) :: dtyp
    integer :: shpout(2)
    call number__gmmmult_arg_check(shpout, transA, transB, A, B, C, alpha, beta)
    call number__append(CC, shpout, has_dx(A) .or. has_dx(B) .or. has_dx(alpha) .or. has_dx(beta))
    call node__append(op_gmmmult1_id_, [alpha, A, B, beta, C], [CC], [transA, transB], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_gmmmult(transA, transB, alpha, A, B, beta, C, CC)
  end subroutine private_gmmmult
end function xp_number__gmmmult__1

!>  @relates number__gmmmult
function xp_number__gmmmult__2 (alpha, transA, transB, A, B) result(C)
  implicit none
  integer, intent(in) :: transA, transB
  type(xp_number), intent(in) :: alpha
  type(xp_number), intent(in) :: A, B
  type(xp_number), pointer :: C
  call do_safe_within('xp_number__gmmmult__2', mod_numbers_math_name_, private_gmmmult)
contains
  subroutine private_gmmmult
    real(kind=xp_) :: dtyp
    integer :: shpout(2)
    call number__gmmmult_arg_check(shpout, transA, transB, A, B, alpha=alpha)
    call number__append(C, shpout, has_dx(A) .or. has_dx(B) .or. has_dx(alpha))
    call node__append(op_gmmmult2_id_, [alpha, A, B], [C], [transA, transB], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_gmmmult(alpha, transA, transB, A, B, C)
  end subroutine private_gmmmult
end function xp_number__gmmmult__2

!> @relates number__gmmmult
function xp_number__gmmmult__3 (transA, transB, A, B, C) result(CC)
  implicit none
  integer, intent(in) :: transA, transB
  type(xp_number), intent(in) :: A, B, C
  type(xp_number), pointer :: CC
  call do_safe_within('xp_number__gmmmult__3', mod_numbers_math_name_, private_gmmmult)
contains
  subroutine private_gmmmult
    real(kind=xp_) :: dtyp
    integer :: shpout(2)
    call number__gmmmult_arg_check(shpout, transA, transB, A, B, C)
    call number__append(CC, shpout, has_dx(A) .or. has_dx(B) .or. has_dx(C))
    call node__append(op_gmmmult3_id_, [A, B, C], [CC], [transA, transB], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_gmmmult(transA, transB, A, B, C, CC)
  end subroutine private_gmmmult
end function xp_number__gmmmult__3

!> @relates number__gmmmult
function xp_number__gmmmult__4 (transA, transB, A, B) result(C)
  implicit none
  integer, intent(in) :: transA, transB
  type(xp_number), intent(in) :: A, B
  type(xp_number), pointer :: C
  call do_safe_within('xp_number__gmmmult__4', mod_numbers_math_name_, private_gmmmult)
contains
  subroutine private_gmmmult
    real(kind=xp_) :: dtyp
    integer :: shpout(2)
    call number__gmmmult_arg_check(shpout, transA, transB, A, B)
    call number__append(C, shpout, has_dx(A) .or. has_dx(B))
    call node__append(op_gmmmult4_id_, [A, B], [C], [transA, transB], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_gmmmult(transA, transB, A, B, C)
  end subroutine private_gmmmult
end function xp_number__gmmmult__4

!> @relates xp_number__gmvmult_arg_check
subroutine xp_number__gmvmult_arg_check (shpout, trans, A, x, y, alpha, beta)
  implicit none
  integer, intent(out) :: shpout(1)
  integer, intent(in) :: trans
  type(xp_number), intent(in) :: A, x
  type(xp_number), intent(in), optional :: y, alpha, beta
  shpout = 1
  call do_safe_within('xp_number__gmvmult_arg_check', mod_numbers_math_name_, private_check)
contains
  subroutine private_check
    call assert(is_allocated(A), err_notAlloc_, 'A')
    call assert(get_rank(A) == 2, err_generic_, 'A rank /= 2')
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(get_rank(x) == 1, err_generic_, 'x rank /= 1')
    if (present(alpha)) then
       call assert(is_allocated(alpha), err_notAlloc_, 'alpha')
       call assert(get_rank(alpha) == 0, err_generic_, 'alpha rank /= 0')
    end if
    if (present(beta)) then
       call assert(is_allocated(beta), err_notAlloc_, 'beta')
       call assert(get_rank(beta) == 0, err_generic_, 'beta rank /= 0')
    end if
    if (present(y)) then
       call assert(is_allocated(y), err_notAlloc_, 'y')
       call assert(get_rank(y) == 1, err_generic_, 'y rank /= 1')
       if(err_free()) call private_check_shape(y)
    else
       if (err_free()) call private_check_shape
    end if
  end subroutine private_check
  subroutine private_check_shape (y)
    type(xp_number), intent(in), optional :: y
    integer :: n, m
    m = merge(A%shp(1), A%shp(2), trans < 1)
    n = merge(A%shp(2), A%shp(1), trans < 1)
    call assert(n == get_size(x), err_wrngSz_, 'A or x')
    if (present(y)) call assert(m == get_size(y), err_wrngSz_, 'y')
    if (err_free()) shpout = m
  end subroutine private_check_shape
end subroutine xp_number__gmvmult_arg_check

!> @relates number__gvmmult
function xp_number__gmvmult__1 (trans, alpha, A, x, beta, y) result(yy)
  implicit none
  integer, intent(in) :: trans
  type(xp_number), intent(in) :: alpha, beta
  type(xp_number), intent(in) :: A, x, y
  type(xp_number), pointer :: yy
  call do_safe_within('xp_number__gmvmult__1', mod_numbers_math_name_, private_gmvmult)
contains
  subroutine private_gmvmult
    real(kind=xp_) :: dtyp
    integer :: shpout(1)
    call number__gmvmult_arg_check(shpout, trans, A, x, y, alpha, beta)
    call number__append(yy, shpout, has_dx(A) .or. has_dx(x) .or. has_dx(alpha) .or. has_dx(beta))
    call node__append(op_gmvmult1_id_, [alpha, x, A, beta, y], [yy], [trans], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_gmvmult(trans, alpha, x, A, beta, y, yy)
  end subroutine private_gmvmult
end function xp_number__gmvmult__1

!> @relates number__gvmmult
function xp_number__gmvmult__2 (alpha, trans, A, x) result(y)
  implicit none
  integer, intent(in) :: trans
  type(xp_number), intent(in) :: alpha
  type(xp_number), intent(in) :: A, x
  type(xp_number), pointer :: y
  call do_safe_within('xp_number__gmvmult__2', mod_numbers_math_name_, private_gmvmult)
contains
  subroutine private_gmvmult
    real(kind=xp_) :: dtyp
    integer :: shpout(1)
    call number__gmvmult_arg_check(shpout, trans, A, x, alpha=alpha)
    call number__append(y, shpout, has_dx(A) .or. has_dx(x) .or. has_dx(alpha))
    call node__append(op_gmvmult2_id_, [alpha, x, A], [y], [trans], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_gmvmult(alpha, trans, x, A, y)
  end subroutine private_gmvmult
end function xp_number__gmvmult__2

!> @relates number__gvmmult
function xp_number__gmvmult__3 (trans, A, x, y) result(yy)
  implicit none
  integer, intent(in) :: trans
  type(xp_number), intent(in) :: A, x, y
  type(xp_number), pointer :: yy
  call do_safe_within('xp_number__gmmmult__3', mod_numbers_math_name_, private_gmvmult)
contains
  subroutine private_gmvmult
    real(kind=xp_) :: dtyp
    integer :: shpout(1)
    call number__gmvmult_arg_check(shpout, trans, A, x, y)
    call number__append(yy, shpout, has_dx(A) .or. has_dx(x) .or. has_dx(y))
    call node__append(op_gmvmult3_id_, [x, A, y], [yy], [trans], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_gmvmult(trans, x, A, y, yy)
  end subroutine private_gmvmult
end function xp_number__gmvmult__3

!> @relates number__gvmmult
function xp_number__gmvmult__4 (trans, A, x) result(y)
  implicit none
  integer, intent(in) :: trans
  type(xp_number), intent(in) :: A, x
  type(xp_number), pointer :: y
  call do_safe_within('xp_number__gmvmult__4', mod_numbers_math_name_, private_gmvmult)
contains
  subroutine private_gmvmult
    real(kind=xp_) :: dtyp
    integer :: shpout(1)
    call number__gmvmult_arg_check(shpout, trans, A, x)
    call number__append(y, shpout, has_dx(A) .or. has_dx(x))
    call node__append(op_gmvmult4_id_, [x, A], [y], [trans], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_gmvmult(trans, x, A, y)
  end subroutine private_gmvmult
end function xp_number__gmvmult__4

!> @relates number__vvouter_arg_check
subroutine xp_number__vvouter_arg_check (shpout, x, y, z, alpha)
  implicit none
  integer, intent(out) :: shpout(2)
  type(xp_number), intent(in) :: x, y
  type(xp_number), intent(in), optional :: z, alpha
  shpout = 1
  call do_safe_within('xp_number__vvouter_arg_check', mod_numbers_math_name_, private_check)
contains
  subroutine private_check
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(get_rank(x) == 1, err_generic_, 'x rank /= 1')
    call assert(is_allocated(y), err_notAlloc_, 'y')
    call assert(get_rank(y) == 1, err_generic_, 'y rank /= 1')
    if (present(alpha))  then
       call assert(is_allocated(alpha), err_notAlloc_, 'alpha')
       call assert(get_rank(alpha) == 0, err_generic_, 'alpha rank /= 0')
    end if
    if (present(z)) then
       call assert(is_allocated(z), err_notAlloc_, 'z')
       call assert(get_rank(z) == 2, err_generic_, 'z rank /= 2')
       if (err_free()) call private_check_shape(z)
    else
       if (err_free()) call private_check_shape
    end if
  end subroutine private_check
  subroutine private_check_shape (z)
    type(xp_number), intent(in), optional :: z
    if (present(z)) call assert(z%shp(1) == get_size(x) .and. z%shp(2) == get_size(y), err_wrngSz_, 'z')
    if (err_free()) shpout = [get_size(x), get_size(y)]
  end subroutine private_check_shape
end subroutine xp_number__vvouter_arg_check

!> @relates number__vvouter
function xp_number__vvouter__1 (alpha, x, y, z) result(A)
  implicit none
  type(xp_number), intent(in) :: alpha, x, y, z
  type(xp_number), pointer :: A
  call do_safe_within('xp_number__vvouter__1', mod_numbers_math_name_, private_vvouter)
contains
  subroutine private_vvouter
    real(kind=xp_) :: dtyp
    integer :: shpout(2)
    call number__vvouter_arg_check(shpout, x, y, z, alpha)
    call number__append(A, shpout, has_dx(alpha) .or. has_dx(x) .or. has_dx(y) .or. has_dx(z))
    call node__append(op_vvouter1_id_, [alpha, x, y, z], [A], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_vvouter(alpha, x, y, z, A)
  end subroutine private_vvouter
end function xp_number__vvouter__1

!> @relates number__vvouter
function xp_number__vvouter__2 (x, y, z) result(A)
  implicit none
  type(xp_number), intent(in) :: x, y, z
  type(xp_number), pointer :: A
  call do_safe_within('xp_number__vvouter__2', mod_numbers_math_name_, private_vvouter)
contains
  subroutine private_vvouter
    real(kind=xp_) :: dtyp
    integer :: shpout(2)
    call number__vvouter_arg_check(shpout, x, y, z)
    call number__append(A, z%shp, has_dx(x) .or. has_dx(y) .or. has_dx(z))
    call node__append(op_vvouter2_id_, [x, y, z], [A], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_vvouter(x, y, z, A)
  end subroutine private_vvouter
end function xp_number__vvouter__2

!> @relates number__vvouter
function xp_number__vvouter__3 (x, y) result(A)
  implicit none
  type(xp_number), intent(in) :: x, y
  type(xp_number), pointer :: A
  call do_safe_within('xp_number__vvouter__3', mod_numbers_math_name_, private_vvouter)
contains
  subroutine private_vvouter
    real(kind=xp_) :: dtyp
    integer :: shpout(2)
    call number__vvouter_arg_check(shpout, x, y)
    call number__append(A, [get_size(x),get_size(y)], has_dx(x) .or. has_dx(y))
    call node__append(op_vvouter3_id_, [x, y], [A], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_vvouter(x, y, A)
  end subroutine private_vvouter 
end function xp_number__vvouter__3

!> @relates number__vvinner
function xp_number__vvinner (x, y) result(z)
  implicit none
  type(xp_number), intent(in) :: x, y
  type(xp_number), pointer :: z
  call do_safe_within("number__vvinner", mod_numbers_math_name_, private_dot)
contains
  subroutine private_dot
    real(kind=xp_) :: dtyp
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(get_rank(x) == 1, err_generic_, 'x rank /= 1')
    call assert(is_allocated(y), err_notAlloc_, 'y')
    call assert(get_rank(y) == 1, err_generic_, 'y rank /= 1')
    call assert(get_size(x) == get_size(y), err_wrngSz_, 'x or y')
    call number__append(z, [integer::], has_dx(x) .or. has_dx(y))
    call node__append(op_vvinner_id_, [x, y], [z], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_vvinner(x, y, z)
  end subroutine private_dot
end function xp_number__vvinner  

!> @relates number__invmat_arg_check
subroutine xp_number__invMat_arg_check (x)
  implicit none
  type(xp_number), intent(in) :: x
  call do_safe_within("number__invMat_arg_check", mod_numbers_math_name_, private_check)
contains
  subroutine private_check
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call assert(get_rank(x) == 2, err_generic_, 'x has not rank 2')
    call assert(x%shp(1) == x%shp(2), err_generic_, 'x is not square')
  end subroutine private_check
end subroutine xp_number__invMat_arg_check

!> @relates number__invmat
function xp_number__invMat (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  type(xp_number), pointer :: ans
  call do_safe_within('xp_number__invMat_dpn2', mod_numbers_math_name_, private_invMat)
contains
  subroutine private_invMat
    real(kind=xp_) :: dtyp
    call number__invMat_arg_check(x)
    call number__append(ans, [x%shp(1), x%shp(2)], has_dx(x))
    call node__append(op_invMat_id_, [x], [ans], [integer::], [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free()  .and. (.not. within_posts())) call op_invMat(x, ans)
  end subroutine private_invMat
end function xp_number__invMat

function xp_number__slidemmm (transA, transB, hashB, A, B, bm, bn, hm, l, beta) result(C)
  implicit none
  type(xp_number), intent(in) :: A, B
  integer, intent(in) :: transA, transB, hashB, bm, bn, hm, l, beta
  type(xp_number), pointer :: C
  call do_safe_within("xp_number__slidemmm", mod_numbers_math_name_, private_do)
contains
  subroutine private_do
    real(kind=xp_) :: dtyp
    real(kind=xp_), pointer :: xx(:,:)
    integer :: n, hn, i, shpout(2), b1(l), b2(l), ht(l)
    type(hashtab), pointer :: htab1, htab2, htab3, htab4
    logical :: byrows
    call number__gmmmult_arg_check(shpout, 0, 0, A, B)
    !rows of A to hash
    hn = 2**bn
    if (hashB > 0) then
       call with_shape(B, xx, .false.)
       byrows = transB > 0
       n = merge(get_size(B, 2), get_size(B, 1), transB > 0)
    else
       call with_shape(A, xx, .false.)
       byrows = transA < 1
       n = merge(get_size(A, 2), get_size(A, 1), transA > 0)
    end if
    do i = 1, l
       call hashtab__append(htab1, bm, bn)
       b1(i) = htab1%id
       call hashtab__append(htab2, bm, bn)
       b2(i) = htab2%id
       call binary_simhash__basis(htab1, htab2, n)
       call hashtab__append(htab3, hm, hn)
       ht(i) = htab3%id
       call binary_simhash__fill_ht(xx, htab1, htab2, byrows, htab3)
    end do
    call number__append(C, shpout, has_dx(A) .or. has_dx(B))
    call hashtab__append(htab4, beta, get_size(B, 2))
    call node__append(op_slidemmm_id_, [A, B], [C], [transA, transB, hashB, htab4%id, l, b1, b2, ht], &
         [real(kind=xp_)::], [integer::], [integer::])
    call graph__append(dtyp)
    if (err_free() .and. (.not. within_posts())) then
       call op_slidemmm(A, B, C, [transA, transB, hashB, htab4%id, l, b1, b2, ht])
       if (.not. graphi_is_set(1._xp_)) then
          do i = 1, l
             htab1 => HTS_(b1(i))
             call hashtab__pop(htab1)
             htab2 => HTS_(b2(i))
             call hashtab__pop(htab2)
             htab3 => HTS_(ht(i))
             call hashtab__pop(htab3)
          end do
          call hashtab__pop(htab4)
       end if
    end if
  end subroutine private_do
end function xp_number__slidemmm
