  !> @addtogroup numbers_math__matrix_
  !! @{
  
  !> Checks that the inputs of a gmmmult operation are consistent.
  !! @param[out] shpout integer(2), shape of the result
  !! @param[in] ta,tb integer, transposition flags
  !! @param[in] A,B 'numbers' of rank 2
  !! @param[in] alpha,beta 'numbers' of rank 0, optional
  !! @param[in] C 'number' of rank 2, optional
  interface number__gmmmult_arg_check
     module procedure sp_number__gmmmult_arg_check
     module procedure dp_number__gmmmult_arg_check
  end interface number__gmmmult_arg_check
  
  !> General Matrix-Matrix Multiplication
  !!
  !! @param[in] transA integer, if > 1 op(A) = A**T else op(A) = A
  !! @param[in] transB integer, if > 1 op(B) = B**T else op(B) = B
  !! @param[in] alpha 'number' wirh rank 0
  !! @param[in] A 'number' with rank 2
  !! @param[in] B 'number' with rank 2
  !! @param[in] beta 'number' with rank 0
  !! @param[in] C 'number' with rank 2
  interface number__gmmmult
     !> ans = alpha * op(A).op(B) + beta * C
     module procedure sp_number__gmmmult__1
     !> ans = alpha * op(A).op(B)
     module procedure sp_number__gmmmult__2
     !> ans =  op(A).op(B) + C
     module procedure sp_number__gmmmult__3
     !> ans = op(A).op(B)
     module procedure sp_number__gmmmult__4
     module procedure dp_number__gmmmult__1
     module procedure dp_number__gmmmult__2
     module procedure dp_number__gmmmult__3
     module procedure dp_number__gmmmult__4
  end interface number__gmmmult

  !> Checks that the input of a gmvmult operation are consistent.
  !! @param[out] shpout integer(1), shape of the output 'number'
  !! @param[in] trans integer, if > 0 op(A) = A**T else op(A) = A
  !! @param[in] A 'number', with rank 2
  !! @param[in] x 'number', with rank 1
  !! @param[in] y 'number', with y
  !! @param[in] alpha 'number', with rank 0
  !! @param[in] beta 'number', with rank 0
  interface number__gmvmult_arg_check
     module procedure sp_number__gmvmult_arg_check
     module procedure dp_number__gmvmult_arg_check
  end interface number__gmvmult_arg_check

  !> Genral matrix vector multiplication
  !!
  !! @param[in] trans integer, if > 0 op(A) = A**T else op(A) = A
  !! @param[in] alpha 'number' with rank 0
  !! @param[in] A 'number' with rank 2
  !! @param[in] x 'number' with rank 1
  !! @param[in] beta 'number' with rank 0
  !! @param[in] y 'number' with rank 1
  interface number__gmvmult
     !> ans = alpha * op(A).x + beta * y
     module procedure sp_number__gmvmult__1
     !> ans = alpha * op(A).x
     module procedure sp_number__gmvmult__2
     !> ans = op(A).x + y
     module procedure sp_number__gmvmult__3
     !> ans = op(A).x
     module procedure sp_number__gmvmult__4
     module procedure dp_number__gmvmult__1
     module procedure dp_number__gmvmult__2
     module procedure dp_number__gmvmult__3
     module procedure dp_number__gmvmult__4
  end interface number__gmvmult

  !> Checks that the input to a ger operatio are consistent.
  !! @param[in] shpout integer(2), shape of the output 'number'
  !! @param[in] x 'number' with rank 1
  !! @param[in] y 'number' with rank 1
  !! @param[in] z 'number' with rank 2
  !! @param[in] alpha 'number' with rank 0
  interface number__vvouter_arg_check
     module procedure sp_number__vvouter_arg_check
     module procedure dp_number__vvouter_arg_check
  end interface number__vvouter_arg_check

  !> General outer product between vectors.
  !!
  !! @param[in] alpha double precision scalar
  !! @param[in] x,y 'number' of rank 1
  !! @param[in] z 'number' of rank 2
  !! @return ans number of rank 2
  interface number__vvouter
     !> ans = alpha * x.y**T + z
     module procedure sp_number__vvouter__1
     !> ans = x.y**T + z
     module procedure sp_number__vvouter__2
     !> ans = x.y**T
     module procedure sp_number__vvouter__3
     module procedure dp_number__vvouter__1
     module procedure dp_number__vvouter__2
     module procedure dp_number__vvouter__3
  end interface number__vvouter

  !> Inner product betwee vectors.
  !! ans = x**T . y
  !! @param[in] x, 'number' of rank 1
  !! @param[in] y 'number' of rank 1
  !! @return ans number of rank 0
  interface number__vvinner
     module procedure sp_number__vvinner
     module procedure dp_number__vvinner
  end interface number__vvinner

  !> Checks that the input of an invMat operations are consistent
  !! @param[in] x 'number' with rank 2 and square shape
  interface number__invMat_arg_check
     module procedure sp_number__invMat_arg_check
     module procedure dp_number__invMat_arg_check
  end interface number__invMat_arg_check

  !> Genreal Matrix inversion.
  !! @param[in] x 'number' with rank 2 and square shape
  interface number__invMat
     module procedure sp_number__invMat
     module procedure dp_number__invMat
  end interface number__invMat

  interface number__slidemmm
     module procedure sp_number__slidemmm
     module procedure dp_number__slidemmm
  end interface number__slidemmm

  !> @}
