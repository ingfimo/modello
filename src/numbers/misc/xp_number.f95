!> @brief Runs the calculation @node of which the given @number is output
!! @param[in] x @xp_number output @number
subroutine xp_number__op (x)
  implicit none
  type(xp_number), intent(in) :: x
  call do_safe_within("xp_number__op", mod_numbers_name_, private_op)
contains
  subroutine private_op
    type(xp_node), pointer :: nd
    integer :: ndi
    ndi = number__get_nd(x)
    call assert(ndi > 0, err_generic_, "x is not node output.")
    if (err_free()) then
       nd => xp_NODES_(ndi)
       call nd%op(nd%attrs, nd%states)
    end if
  end subroutine private_op
end subroutine xp_number__op

!> @brief Runs the forward differentiation @node of which the given @number is output
!! @param[in] x @xp_number output @number
subroutine xp_number__fw (x)
  implicit none
  type(xp_number), intent(inout) :: x
  call do_safe_within("xp_number__fw", mod_numbers_name_, private_op)
contains
  subroutine private_op
    integer :: ndi
    type(xp_node), pointer :: nd
    ndi = number__get_nd(x)
    call assert(ndi > 0, err_generic_, "x is not node output.")
    if (err_free()) then
       nd => xp_NODES_(ndi)
       call nd%fw(nd%attrs, nd%states)
    end if
  end subroutine private_op
end subroutine xp_number__fw

!> @brief Resets (sets to 0) the backward gradients of the @node that
!! has as output the given @number
!! @param[in] x @xp_number output @number
subroutine xp_number__bw_zero (x)
  implicit none
  type(xp_number), intent(inout) :: x
  call do_safe_within("xp_number__fw", mod_numbers_name_, private_op)
contains
  subroutine private_op
    integer :: ndi
    type(xp_node), pointer :: nd
    ndi = number__get_nd(x)
    call assert(ndi > 0, err_generic_, "x is not node output.")
    if (err_free()) then
       nd => xp_NODES_(ndi)
       call node__bw_zero(nd)
    end if
  end subroutine private_op
end subroutine xp_number__bw_zero

!> @brief Runs the backward differentiation @node of which the given @number is output
!! @param[in] x @xp_number output number
subroutine xp_number__bw (x)
  implicit none
  type(xp_number), intent(inout) :: x
  call do_safe_within("xp_number__fw", mod_numbers_name_, private_op)
contains
  subroutine private_op
    integer :: ndi
    type(xp_node), pointer :: nd
    ndi = number__get_nd(x)
    call assert(ndi > 0, err_generic_, "x is not node output.")
    if (err_free()) then
       nd => xp_NODES_(ndi)
       call nd%bw(nd%attrs, nd%states)
    end if
  end subroutine private_op
end subroutine xp_number__bw

!> @brief Pops (removes) a @number from the relative register
!! @parame[inout] x @xp_number @pointer refernce to the number to pop
subroutine xp_number__pop (x)
  implicit none
  type(xp_number), intent(inout), pointer :: x
  call do_safe_within('xp_number__pop', mod_numbers_name_, private_do)
contains
  subroutine private_do
    call assert(number__inlock_free(x), err_generic_, 'inlock is not free.')
    call err_safe(private_pop)
  end subroutine private_do
  subroutine private_pop
    integer :: i, nd
    real(kind=xp_) :: dtyp
    i = x%id
    nd = number__get_nd(x)
    if (nd > 0) call node__pop(nd, dtyp)
    call number__reset_nd(x)
    nullify(x)
    call number__deallocate(xp_NUMBERS_(i))
    if (err_free()) NUMINDEX_ = [NUMINDEX_, i]
  end subroutine private_pop
end subroutine xp_number__pop
