!> @brief Appends a @number to the reltive register given its shape
!! @param[inout] x @xp_number @pointer reference to the appended @number 
!! @param[in] shp @int @vector shape
!! @param[in] dx @bool if .true. a gradient is attached to the @number
subroutine xp_number__append__1 (x, shp, dx)
  implicit none
  type(xp_number), intent(out), pointer :: x
  integer, intent(in) :: shp(:)
  logical, intent(in) :: dx
  integer :: i
  call do_safe_within('xp_number__append__1', mod_numbers_name_, private_append)
contains
  subroutine private_append
    i = next_number(x)
    call number__allocate(xp_NUMBERS_(i), shp, dx)
    x => xp_NUMBERS_(i)
    if (err_free()) x%id = i
  end subroutine private_append
end subroutine xp_number__append__1

!> @brief Appends a @number to the relative register given its, and a source the scalar
!! @param[inout] x @xp_number @pointer reference to the appended @number
!! @param[in] shp @int @vector shape
!! @param[in] dx @bool if .true. a gradient is attached to the @number
!! @param[in] v @xp_ source scalar
subroutine xp_number__append__2 (x, shp, dx, v)
  implicit none
  type(xp_number), intent(out), pointer :: x
  integer, intent(in) :: shp(:)
  logical, intent(in) :: dx
  real(kind=xp_), intent(in) :: v
  integer :: i
  call do_safe_within('xp_number__append__2', mod_numbers_name_, private_append)
contains
  subroutine private_append
    i = next_number(x)
    call number__allocate(xp_NUMBERS_(i), shp, dx, v)
    x => xp_NUMBERS_(i)
    if (err_free()) x%id = i
  end subroutine private_append
end subroutine xp_number__append__2

!> @brief Appends a @number to the relative scalar given its shape and a source vector
!! @param[inout] x @xp_number @pointer reference to the appended @number
!! @param[in] shp @int @vector shape
!! @param[in] dx @bool if .true. a gradient is attached to the @number
!! @param[in] v double precision(:)
subroutine xp_number__append__3 (x, shp, dx, v)
  implicit none
  type(xp_number), intent(out), pointer :: x
  integer, intent(in) :: shp(:)
  logical, intent(in) :: dx
  real(kind=xp_), intent(in) :: v(:)
  integer :: i
  call do_safe_within('xp_number__append__3', mod_numbers_name_, private_append)
contains
  subroutine private_append
    i = next_number(x)
    call number__allocate(xp_NUMBERS_(i), shp, dx, v)
    x => xp_NUMBERS_(i)
    if (err_free()) x%id = i
  end subroutine private_append
end subroutine xp_number__append__3

!> @brief Apppends a @number contiguous slice to the relative register
!! @param[inout] x1 @xp_number @pointer reference to the slice
!! @param[in] x2 @xp_number @number to slice
!! @param[in] s1 @int starting index of the slice
!! @param[in] s2 @int ending index of the slice
subroutine xp_number__append_contiguous_slice (x1, x2, s1, s2)
  implicit none
  type(xp_number), intent(inout), pointer :: x1
  type(xp_number), intent(in) :: x2
  integer, intent(in) :: s1, s2
  call do_safe_within("xp_number__append_slice", mod_numbers_name_, private_append_slice)
contains
  subroutine private_append_slice
    integer :: i
    i = next_number(x2)
    call number__take_contiguous_slice(xp_NUMBERS_(i), x2, s1, s2)
    x1 => xp_NUMBERS_(i)
    if (err_free()) x1%id = i
  end subroutine private_append_slice
end subroutine xp_number__append_contiguous_slice

!> @brief Appends a reshaped @number to the reltive register
!! @param[inout] x1 @xp_number @pointer reference to the reshaped number
!! @param[in] x2 @xp_number @number to reshape
!! @param[in] shp @int @vector new shape
subroutine xp_number__append_reshape (x1, x2, shp)
  implicit none
  type(xp_number), intent(inout), pointer :: x1
  type(xp_number), intent(in) :: x2
  integer, intent(in) :: shp(:)
  call do_safe_within('xp_number__append_reshape', mod_numbers_name_, private_append_reshape)
contains
  subroutine private_append_reshape
    integer :: i
    i = next_number(x2)
    call number__make_reshape(xp_NUMBERS_(i), x2, shp)
    x1 => xp_NUMBERS_(i)
    if (err_free()) x1%id = i
  end subroutine private_append_reshape
end subroutine xp_number__append_reshape

!> @brief Appends a @number reshaped by dropping degenerate dimensions to the relative register
!! @param[inout] x1 @xp_number @pointer reference to the reshaped number
!! @param[in] x2 @xp_number number to reshape
subroutine xp_number__append_drop_shape (x1, x2)
  implicit none
  type(xp_number), intent(inout), pointer :: x1
  type(xp_number), intent(in) :: x2
  call do_safe_within('number__append_drop_shape', mod_numbers_name_, private_append_drop)
contains
  subroutine private_append_drop
    integer :: i
    i = next_number(x2)
    call number__make_drop_shape(xp_NUMBERS_(i), x2)
    x1 => xp_NUMBERS_(i)
    if (err_free()) x1%id = i
  end subroutine private_append_drop
end subroutine xp_number__append_drop_shape

!> @brief Appends a @number resulting from binding two @numbers 
!! @param[inout] x1 @xp_number @pointer reference to the appended @number
!! @param[in] x2,x3 @xp_numbers @numbers to bind
!! @param[in] k @xp_ dimension along which to perform the binding
subroutine xp_number__append_bind (x1, x2, x3, k)
  implicit none
  type(xp_number), intent(inout), pointer :: x1
  type(xp_number), intent(in) :: x2, x3
  integer, intent(in) :: k
  call do_safe_within('xp_number__append_bind', mod_numbers_name_, private_append_bind)
contains
  subroutine private_append_bind
    integer :: i
    i = next_number(x2)
    call number__make_bind(xp_NUMBERS_(i), x2, x3, k)
    x1 => xp_NUMBERS_(i)
    if (err_free()) x1%id = i
  end subroutine private_append_bind
end subroutine xp_number__append_bind
