subroutine allocate_hash_tables (n)
  implicit none
  integer, intent(in) :: n
  call do_safe_within("allocate_hash_tables", mod_registers_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: info, i
    call assert(.not. allocated(HTS_), err_alreadyAlloc_, "HTS_")
    if (err_free()) then
       allocate(HTS_(n), stat=info)
       call assert(info == 0, err_alloc_, "HTS_")
       allocate(HTSINDEX_, source=[(i, i=1, n)], stat=info)
       call assert(info == 0, err_alloc_, "HTSINDEX_")
    end if
  end subroutine private_allocate
end subroutine allocate_hash_tables

function next_hashtab () result(i)
  implicit none
  integer :: i
  call do_safe_within("hashtab__next_hashtab", mod_registers_utils_name_, private_next)
contains
  subroutine private_next
    call assert(allocated(HTS_), err_notAlloc_, "HTS_")
    call next_index__0(i, HTSINDEX_, "HTSINDEX_")
  end subroutine private_next
end function next_hashtab




