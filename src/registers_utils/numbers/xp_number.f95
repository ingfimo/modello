!> @addtogroup registers_utils_private_
!! @{

!> @brief Allocates numbers' register arrays
!! @param[in] n @int array sizes
!! @param[in] dtyp @xp_ @precision
subroutine xp_number__allocate_numbers (n, dtyp)
  implicit none
  integer, intent(in) :: n
  real(kind=xp_), intent(in) :: dtyp
  call do_safe_within('xp_number__allocate_numbers', mod_registers_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: i
    call number__allocate(xp_NUMBERS_, n)
    call alloc(NUMINDEX_, [(i, i=1, n)], "NUMINDEX_")
    call alloc(INLOCKS_, n, 'INLOCK_')
    call alloc(NUMNDS_, n, 'NUMBER_NDS_')
  end subroutine private_allocate
end subroutine xp_number__allocate_numbers

!> @brief Deallocates numbers' register arrays
!! @param[in] dtyp @xp_ @precision
subroutine xp_number__deallocate_numbers (dtyp)
  implicit none
  real(kind=xp_), intent(in) :: dtyp
  call do_safe_within('deallocate_numbers', mod_registers_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    call assert(allocated(xp_NUMBERS_), err_notAlloc_, "NUMBERS_")
    call err_safe(private_deallocate_numbers)
    call number__deallocate(xp_NUMBERS_)
    call dealloc(NUMINDEX_, "NUMINDEX_")
    call dealloc(INLOCKS_, 'INLOCKS_')
    call dealloc(NUMNDS_, 'NUMNDS_')
  end subroutine private_deallocate
  subroutine private_deallocate_numbers
    integer :: i
    do i = 1, size(xp_NUMBERS_)
       if (is_allocated(xp_NUMBERS_(i))) then
          call number__deallocate(xp_NUMBERS_(i))
       end if
       if (.not. err_free()) exit
    end do
  end subroutine private_deallocate_numbers
end subroutine xp_number__deallocate_numbers

!> @brief Returns the next available number id
!! @param[in] dtyp @xp_ @precision
function xp_number__next_number (dtyp) result(i)
  type(xp_number), intent(in) :: dtyp
  integer :: i
  call do_safe_within('xp_number__next_number', mod_registers_utils_name_, private_do)
contains
  subroutine private_do
    call assert(allocated(xp_NUMBERS_), err_notAlloc_, 'xp_NUMBERS_')
    call number__next_number__0(i)
  end subroutine private_do
end function xp_number__next_number

!> @brief Garbage collector for numbers
!! @param[in] dtyp @xp_ @precision
subroutine xp_number__numbers_gc (dtyp)
  implicit none
  real(kind=xp_), intent(in) :: dtyp
  call do_safe_within('number__gc', mod_registers_utils_name_, private_do)
contains
  subroutine private_do
    call assert(allocated(xp_NUMBERS_), err_notAlloc_, 'xp_NUMBERS_')
    call err_safe(private_gc)
  end subroutine private_do
  subroutine private_gc
    integer :: i
    do i = 1, size(xp_NUMBERS_)
       if (is_allocated(xp_NUMBERS_(i))) then
          if (number__get_nd(nnn(i, dtyp)) < 0 .and. number__inlock_free(nnn(i, dtyp))) then
             call number__reset_nd(xp_NUMBERS_(i))
             call number__deallocate(xp_NUMBERS_(i))
          end if
       end if
    end do
  end subroutine private_gc
end subroutine xp_number__numbers_gc

!> @brief Links a pointer to the number with the given id
!! @param[in] i @int number id
!! @param[out] x @xp_number @pointer
subroutine xp_number__get_number (i, x)
  implicit none
  integer, intent(in) :: i
  type(xp_number), intent(out), pointer :: x
  call do_safe_within('xp_number__get_number', mod_registers_utils_name_, private_get)
contains
  subroutine private_get
    call assert(allocated(xp_NUMBERS_), err_notAlloc_, 'xp_NUMBERS_')
    if (err_free()) call assert(i > 0 .and. i <= size(xp_NUMBERS_), err_oorng_, 'i')
    if (err_free()) call assert(is_allocated(xp_NUMBERS_(i)), err_notAlloc_, 'xp_NUMBERS_(i)')
    if (err_free()) x => xp_NUMBERS_(i)
  end subroutine private_get
end subroutine xp_number__get_number

!> @brief Returns a pointer linked to the number with the given id
!! @param[in] i @int number id
!! @param[in] dtyp @xp_ @precision
function xp_number__nnn (i, dtyp) result(ans)
  implicit none
  integer, intent(in) :: i
  real(kind=xp_), intent(in) :: dtyp
  type(xp_number), pointer :: ans
  call do_safe_within('xp_number__nnn', mod_registers_utils_name_, private_nnn)
contains
  subroutine private_nnn
    call get_number(i, ans)
  end subroutine private_nnn
end function xp_number__nnn

!> @}
