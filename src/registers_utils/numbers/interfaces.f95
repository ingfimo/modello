  !> @addtogroup registers_utils_public_
  !! @{

  !> @brief Allocates numbers' arrays
  interface allocate_numbers
     !> @prcdoc{registers_utils::sp_number__allocate_numbers}
     module procedure sp_number__allocate_numbers
     !> @prcdoc{registers_utils::dp_number__allocate_numbers} 
     module procedure dp_number__allocate_numbers
  end interface allocate_numbers

  !> @brief Deallocates numbers' arrays
  interface deallocate_numbers
     !> @prcdoc{registers_utils::sp_number__deallocate_numbers}
     module procedure sp_number__deallocate_numbers
     !> @prcdoc{registers_utils::dp_number__deallocate_numbers}
     module procedure dp_number__deallocate_numbers
  end interface deallocate_numbers

  !> @brief Gets the next available number id
  interface next_number
     !> @prcdoc{registers_utils::sp_number__next_number}
     module procedure sp_number__next_number
     !> @prcdoc{registers_utils::dp_number__next_number}
     module procedure dp_number__next_number
  end interface next_number

  !> @brief Garbage collector for numbers
  !! calculation output, and it is not input of any other operation.
  !! These numbers are deallocated.
  interface numbers_gc
     !> @prcdoc{registers_utils::sp_number__numbers_gc}
     module procedure sp_number__numbers_gc
     !> @prcdoc{registers_utils::dp_number__numbers_gc}
     module procedure dp_number__numbers_gc
  end interface numbers_gc

  !> @brief Links a pointer to the number with the given id
  interface get_number
     !> @prcdoc{registers_utils::sp_number__get_number}
     module procedure sp_number__get_number
     !> @prcdoc{registers_utils::dp_number__get_number}
     module procedure dp_number__get_number
  end interface get_number

  !> @brief returns a pointer linked to the number with the
  !! given id
  interface nnn
     !> @prcdoc{registers_utils::sp_number__nnn}
     module procedure sp_number__nnn
     !> @prcdoc{registers_utils::dp_number__nnn}
     module procedure dp_number__nnn
  end interface nnn

  !> @}
