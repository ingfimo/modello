!> @ingroup registers_utils_private_
!! @brief base procedure to find next available number id
!! @param[out] i @int next available id 
subroutine number__next_number__0 (i) 
  implicit none
  integer, intent(out) :: i
  call do_safe_within("number__next_number__0", mod_registers_utils_name_, private_do)
contains
  subroutine private_do
    call next_index__0(i, NUMINDEX_, "NUMINDEX_")
  end subroutine private_do
end subroutine number__next_number__0

