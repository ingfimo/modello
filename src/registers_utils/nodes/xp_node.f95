!> @addtogroup registers_utils_private_
!! @{

!> @brief Allocates nodes' register arrays
!! @param[in] n @int array sizes
!! @param[in] dtyp @xp_ @precision
subroutine xp_node__allocate_nodes (n, dtyp)
  implicit none
  integer, intent(in) :: n
  real(kind=xp_), intent(in) :: dtyp
  call do_safe_within('xp_node__allocate_nodes', mod_registers_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: i, info
    info = 0
    call assert(.not. allocated(xp_NODES_), err_alreadyAlloc_, 'NODES_')
    call assert(.not. allocated(NODEINDEX_), err_alreadyAlloc_, "NODEINDEX_")
    call assert(.not. allocated(NODEGRPHS_), err_alreadyAlloc_, 'NODEGRPHS_')
    if (err_free()) then
       call node__allocate(xp_NODES_, n)
       call assert(info == 0, err_alloc_, 'NODES_')
    end if
    call alloc(NODEINDEX_, [(i, i=1, n)], "NODEINDEX_")
    call alloc(NODEGRPHS_, n, 'NODEGRPHS_')
  end subroutine private_allocate
end subroutine xp_node__allocate_nodes

!> @brief Deallocates nodes' register arrays
!! @param[in] dtyp @xp_ @precision
subroutine xp_node__deallocate_nodes (dtyp)
  implicit none
  real(kind=xp_), intent(in) :: dtyp
  call do_safe_within("xp_node__deallocate_nodes", mod_registers_utils_name_, private_do)
contains
  subroutine private_do
    call assert(allocated(xp_NODES_), err_notAlloc_, 'NODES_')
    call assert(allocated(NODEGRPHS_), err_notAlloc_, 'NODEGRPHS_')
    call err_safe(private_deallocate)
  end subroutine private_do
  subroutine private_deallocate
    integer :: i, info
    info = 0
    do i = 1, size(xp_NODES_)
       if (is_allocated(xp_NODES_(i))) call node__deallocate(xp_NODES_(i))
       if (.not. err_free()) exit
    end do
    if (err_free()) then
       call node__deallocate(xp_NODES_)
       call assert(info == 0, err_dealloc_, 'NODES_')
    end if
    if (err_free()) then
       deallocate(NODEINDEX_, stat=info)
       call assert(info == 0, err_dealloc_, "NODEINDEX_")
    end if
    if (err_free()) then
       call dealloc(NODEGRPHS_, 'NODEGRPHS_')
       call assert(info == 0, err_dealloc_, "NODEGRPAHS_")
    end if
  end subroutine private_deallocate
end subroutine xp_node__deallocate_nodes

!> @brief Gets the next available node id it in the node stack
!! @param[in] dtyp @xp_ @precision
subroutine xp_node__next_node (dtyp)
  implicit none
  real(kind=xp_), intent(in) :: dtyp
  call do_safe_within('node__next', mod_registers_utils_name_, private_do)
contains
  subroutine private_do
    call assert(NODEi_ == 0, err_generic_, 'stack NODEi_ not empty.')
    call assert(allocated(xp_NODES_), err_notAlloc_, 'NODES_')
    call assert(allocated(NODEINDEX_), err_notAlloc_, "NODEINDEX_")
    if (err_free()) call assert(size(NODEINDEX_) > 0, err_generic_, "NUMBERS_ is full")
    call err_safe(private_next)
  end subroutine private_do
  subroutine private_next
    NODEi_ = NODEINDEX_(1)
    if (size(NODEINDEX_) > 1) then
       NODEINDEX_ = NODEINDEX_(2:)
    else
       NODEINDEX_ = [integer::]
    end if
  end subroutine private_next
end subroutine xp_node__next_node

!> @brief Allocates the graphs' register arrays
!! @param[in] n @int array size
!! @param[in] dtyp @xp_ @precision
subroutine xp_graph__allocate_graphs (n, dtyp)
  implicit none
  integer, intent(in) :: n
  real(kind=xp_), intent(in) :: dtyp
  integer :: info
  info = 0
  call do_safe_within('xp_graph__allocate_graphs', mod_registers_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: i
    call assert(.not. allocated(xp_GRAPHS_), err_alreadyAlloc_, 'GRAPHS_')
    call assert(.not. allocated(GRAPHINDEX_), err_alreadyAlloc_, "GRAPHINDEX_")
    if (err_free()) call graph__allocate(xp_GRAPHS_, n)
    call assert(info == 0, err_alloc_, 'GRAPHS_')
    if (err_free()) allocate(GRAPHINDEX_, source=[(i, i=1, n)], stat=info)
    call assert(info == 0, err_alloc_, "GRAPHINDEX_")
  end subroutine private_allocate
end subroutine xp_graph__allocate_graphs

!> @brief Deallocates graphs' register arrays
!! @param[in] dtyp @xp_ @precision
subroutine xp_graph__deallocate_graphs (dtyp)
  implicit none
  real(kind=xp_), intent(in) :: dtyp
  call do_safe_within("xp_graph__deallocate_graphs", mod_registers_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    integer :: i, info
    info = 0
    do i = 1, size(xp_GRAPHS_)
       if (is_allocated(xp_GRAPHS_(i))) call dealloc(xp_GRAPHS_(i)%nodes, 'GRAPHS_(i)%nodes')
       if (.not. err_free()) exit
    end do
    if (err_free()) then
       call graph__deallocate(xp_GRAPHS_)
    end if
    if (err_free()) then
       deallocate(GRAPHINDEX_, stat=info)
       call assert(info == 0, err_dealloc_, "GRAPHINDEX_")
    end if
    if (err_free()) call reset_graphi(dtyp)
  end subroutine private_deallocate
end subroutine xp_graph__deallocate_graphs

!> @brief Gets the next avaialbe graph id stores it into the stack
!! @param[in] dtyp @xp_ @precision
subroutine xp_graph__next_graph (dtyp)
  implicit none
  real(kind=xp_), intent(in) :: dtyp
  call do_safe_within('xp_graph__next_graph', mod_registers_utils_name_, private_do)
contains
  subroutine private_do
    call assert(allocated(xp_GRAPHS_), err_notAlloc_, 'GRAPHS_')
    call assert(allocated(GRAPHINDEX_), err_notAlloc_, "GRAPHINDEX_")
    if (err_free()) call assert(size(GRAPHINDEX_) > 0, err_generic_, "GRAPHS_ if full")
    call err_safe(private_next)
  end subroutine private_do
  subroutine private_next
    GRAPHi_ = GRAPHINDEX_(1)
    if (size(GRAPHINDEX_) > 1) then
       GRAPHINDEX_ = GRAPHINDEX_(2:)
    else
       GRAPHINDEX_ = [integer::]
    end if
  end subroutine private_next
end subroutine xp_graph__next_graph

!> @brief Garbage collector for graphs
!! @param[in] dtyp @xp_ @precision
subroutine xp_graph__graphs_gc (dtyp)
  implicit none
  real(kind=xp_), intent(in) :: dtyp
  call do_safe_within("xp_graph__graph_gc", mod_registers_utils_name_, private_gc)
contains
  subroutine private_gc
    integer :: i
    do i = 1, size(xp_GRAPHS_)
       if (is_empty(xp_GRAPHS_(i))) call dealloc(xp_GRAPHS_(i)%nodes, 'xp_GRAPHS_(i)%nodes')
    end do
  end subroutine private_gc
end subroutine xp_graph__graphs_gc

!> @brief Links the given pointer to the node with the given id 
!! @param[in] i @int node id
!! @param[out] nd @xp_node @pointer
subroutine xp_node__get_node (i, nd)
  implicit none
  integer, intent(in) :: i
  type(xp_node), intent(out), pointer :: nd
  call do_safe_within("xp_graph__get_node", mod_registers_utils_name_, private_get_node)
contains
  subroutine private_get_node
    call assert(allocated(xp_NODES_), err_notAlloc_, 'xp_NODES_')
    if (err_free()) call assert(i > 0 .and. i <= size(xp_NODES_), err_oorng_, 'i')
    if (err_free()) call assert(is_allocated(xp_NODES_(i)), err_notAlloc_, 'xp_GRAPHS_(i)')
    nd => xp_NODES_(i)
  end subroutine private_get_node
end subroutine xp_node__get_node

!> @brief Returns a pointer linked to the node with the given id
!! @param[in] i @int node id
!! @param[in] dtyp @xp_ @precision
function xp_node__nnd (i, dtyp) result(ans)
  implicit none
  integer, intent(in) :: i
  real(kind=xp_), intent(in) :: dtyp
  type(xp_node), pointer :: ans
  call do_safe_within('xp_node__nnd', mod_registers_utils_name_, private_nnd)
contains
  subroutine private_nnd
    call get_node(i, ans)
  end subroutine private_nnd
end function xp_node__nnd

!> @brief Links the given pointer to the graph with the given id
!! @param[in] i @int grpah id
!! @param[g] graph @xp_graph @pointer
subroutine xp_graph__get_graph (i, g)
  implicit none
  integer, intent(in) :: i
  type(xp_graph), intent(out), pointer :: g
  call do_safe_within("xp_graph__get_graph", mod_registers_utils_name_, private_get_graph)
contains
  subroutine private_get_graph
    call assert(allocated(xp_GRAPHS_), err_notAlloc_, 'xp_GRAPHS_')
    if (err_free()) call assert(i > 0 .and. i <= size(xp_GRAPHS_), err_oorng_, 'gi')
    if (err_free()) call assert(is_allocated(xp_GRAPHS_(i)), err_notAlloc_, 'xp_GRAPHS_(gi)')
    g => xp_GRAPHS_(i)
  end subroutine private_get_graph
end subroutine xp_graph__get_graph

!> @brief Returns a pointer linked to the graph with the given id
!! @param[in] i @int graph id
!! @param[in] dtyp @xp_ @precision
function xp_graph__ggg (i, dtyp) result(ans)
  implicit none
  integer, intent(in) :: i
  real(kind=xp_), intent(in) :: dtyp
  type(xp_graph), pointer :: ans
  call do_safe_within('xp_graph__ggg', mod_registers_utils_name_, private_ggg)
contains
  subroutine private_ggg
    call get_graph(i, ans)
  end subroutine private_ggg
end function xp_graph__ggg

!> @}





