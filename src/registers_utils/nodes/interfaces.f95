  !> @addtogroup registers_utils_
  !! @{

  !> @brief Allocates nodes' arrays
  interface allocate_nodes
     !> @prcdoc{registers_utils::sp_node__allocate_nodes}
     module procedure sp_node__allocate_nodes
     !> @prcdoc{registers_utils::dp_node__allocate_nodes}
     module procedure dp_node__allocate_nodes
  end interface allocate_nodes

  !> @brief Deallocates nodes' arrays
  interface deallocate_nodes
     !> @prcdoc{registers_utils::sp_node__deallocate_nodes}
     module procedure sp_node__deallocate_nodes
     !> @prcdoc{registers_utils::dp_node__deallocate_nodes}
     module procedure dp_node__deallocate_nodes
  end interface deallocate_nodes

  !> @brief Gets the next available node id
  interface next_node
     !> @prcdoc{registers_utils::sp_node__next_node}
     module procedure sp_node__next_node
     !> @prcdoc{registers_utils::dp_node__next_node}
     module procedure dp_node__next_node
  end interface next_node

  !> @brief Allocates graph arrays
  interface allocate_graphs
     !> @prcdoc{registers_utils::sp_graph__allocate_graphs}
     module procedure sp_graph__allocate_graphs
     !> @prcdoc{registers_utils::dp_graph__allocate_graphs}
     module procedure dp_graph__allocate_graphs
  end interface allocate_graphs

  !> @brief Deallocates graph arrays
  interface deallocate_graphs
     !> @prcdoc{registers_utils::sp_graph__deallocate_graphs}
     module procedure sp_graph__deallocate_graphs
     !> @prcdoc{registers_utils::dp_graph__deallocate_graphs}
     module procedure dp_graph__deallocate_graphs
  end interface deallocate_graphs

  !> @brief Gets the next available graph id
  interface next_graph
     !> @prcdoc{registers_utils::sp_graph__next_graph}
     module procedure sp_graph__next_graph
     !> @prcdoc{registers_utils::dp_graph__next_graph}
     module procedure dp_graph__next_graph
  end interface next_graph

  !> @brief Garbage collector for graph arrays.
  !! In that case it is deallocated.
  interface graphs_gc
     !> @prcdoc{registers_utils::sp_graph__graphs_gc}
     module procedure sp_graph__graphs_gc
     !> @prcdoc{registers_utils::dp_graph__graphs_gc}
     module procedure dp_graph__graphs_gc
  end interface graphs_gc

  !> @brief Links the given pointer to the node with the
  !! given id, if it exists.
  interface get_node
     !> @prcdoc{registers_utils::sp_node__get_node}
     module procedure sp_node__get_node
     !> @prcdoc{registers_utils::dp_node__get_node}
     module procedure dp_node__get_node
  end interface get_node

  !> @brief Returns a pointer linked to the node
  !! with the given id, if it exists
  interface nnd
     !> @prcdoc{registers_utils::sp_node__nnd}
     module procedure sp_node__nnd
      !> @prcdoc{registers_utils::dp_node__nnd}
     module procedure dp_node__nnd
  end interface nnd

  !> @brief Links the given pointer to the graph
  !! with the given id, if it exists.
  interface get_graph
     !> @prcdoc{registers_utils::sp_graph__get_graph}
     module procedure sp_graph__get_graph
     !> @prcdoc{registers_utils::dp_graph__get_graph}
     module procedure dp_graph__get_graph
  end interface get_graph

  !> @brief Returns a pointer linked to graph with
  !! the given id, if it exists
  interface ggg
     !> @prcdoc{registers_utils::sp_graph__ggg}
     module procedure sp_graph__ggg
     !> @prcdoc{registers_utils::dp_graph__ggg}
     module procedure dp_graph__ggg
  end interface ggg

!> @}
