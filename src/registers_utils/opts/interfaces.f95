  !> @addtogroup registers_utils_public_ 
  !! @{

  !> Allocates OPTS_ arrays
  interface allocate_opts
     !> @prcdoc{registers_utils::sp_opt__allocate_opts}
     module procedure sp_opt__allocate_opts
     !> @prcdoc{registers_utils::dp_opt__allocate_opts}
     module procedure dp_opt__allocate_opts
  end interface allocate_opts

  !> Deallocates OPTS_ arrays
  interface deallocate_opts
     !> @prcdoc{registers_utils::sp_opt__deallocate_opts}
     module procedure sp_opt__deallocate_opts
     !> @prcdoc{registers_utils::dp_opt__deallocate_opts}
     module procedure dp_opt__deallocate_opts
  end interface deallocate_opts

  !> Gets the next optimiser id
  interface next_opt
     !> @prcdoc{registers_utils::sp_opt__next_opt}
     module procedure sp_opt__next_opt
     !> @prcdoc{registers_utils::dp_opt__next_opt}
     module procedure dp_opt__next_opt
  end interface next_opt

  !> Gets a pointer to the optimiser with the given id
  interface get_opt
     !> @prcdoc{registers_utils::sp_opt__get_opt}
     module procedure sp_opt__get_opt
     !> @prcdoc{registers_utils::dp_opt__get_opt}
     module procedure dp_opt__get_opt
  end interface get_opt

  !> Returns a pointer poiting to the optimiser with the given id
  interface ooo
     !> @prcdoc{registers_utils::sp_opt__ooo}
     module procedure sp_opt__ooo
     !> @prcdoc{registers_utils::dp_opt__ooo}
     module procedure dp_opt__ooo 
  end interface ooo

  !> @}
