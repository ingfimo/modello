!> @addtogroup registers_utils_private_
!! @{

!> @brief Allocates the xp_OPTS_ array
!! @param[in] n @int array size
!! @param[in] dtype @xp_ @precision
subroutine xp_opt__allocate_opts (n, dtyp)
  implicit none
  integer, intent(in) :: n
  real(kind=xp_), intent(in) :: dtyp
  call do_within('xp_opt__allocate_gopts', mod_registers_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: info
    info = 0
    call assert(.not. allocated(xp_OPTS_), err_alreadyAlloc_, 'xp_OPTS_')
    if (err_free()) allocate(xp_OPTS_(n), stat=info)
    call assert(info == 0, err_alloc_, 'xp_OPTS_')
  end subroutine private_allocate
end subroutine xp_opt__allocate_opts

!> @brief Deallocates xp_OPTS_
!! @param[in] dtyp @xp_ @precision
subroutine xp_opt__deallocate_opts (dtyp)
  implicit none
  real(kind=xp_), intent(in) :: dtyp
  call do_safe_within('xp_opt__deallocate_gopts', mod_registers_utils_name_, private_do)
contains
  subroutine private_do
    call assert(allocated(xp_OPTS_), err_notAlloc_, 'xp_OPTS_')
    call err_safe(private_deallocate)
  end subroutine private_do
  subroutine private_deallocate
    integer :: i, info
    info = 0
    do i = 1, size(xp_OPTS_)
       if (is_allocated(xp_OPTS_(i))) call opt__deallocate(xp_OPTS_(i))
    end do
    if (err_free()) deallocate(xp_OPTS_, stat=info)
    call assert(info == 0, err_dealloc_, 'OPTS_')
  end subroutine private_deallocate
end subroutine xp_opt__deallocate_opts

!> @brief Gets the next optimiser id for xp_OPTS_
!! @param[in] dtyp @xp_ @precision
!! @todo use index instead of while loop
function xp_opt__next_opt (dtyp) result(i)
  implicit none
  integer :: i
  real(kind=xp_), intent(in) :: dtyp
  i = 0
  call do_safe_within('xp_opt__next_opt', mod_registers_utils_name_, private_do)
contains
  subroutine private_do
    call assert(allocated(xp_OPTS_), err_notAlloc_, 'xp_OPS_')
    call err_safe(private_next)
  end subroutine private_do
  subroutine private_next
    integer :: info
    info = 1
    do while (i <= size(xp_OPTS_))
       i = i + 1
       if (is_deallocated(xp_OPTS_(i))) then
          info = 0
          exit
       end if
    end do
    call assert(info == 0, err_generic_, 'xp_OPTS_ register if full.')
  end subroutine private_next
end function xp_opt__next_opt

!> @brief Gets pointer to the xp_opt with the given id
!! @param[in] i @int optimiser index or identifier
!! @param[out] x @xp_opt @pointer
subroutine xp_opt__get_opt (i, x) 
  implicit none
  integer, intent(in) :: i
  type(xp_opt), intent(out), pointer :: x
  call do_safe_within('xp_opt__get_opt', mod_registers_utils_name_, private_get_opt)
contains
  subroutine private_get_opt
    call assert(allocated(xp_OPTS_), err_notAlloc_, 'xp_OPTS_')
    call assert(i > 0 .and. i <= size(xp_OPTS_), err_oorng_, 'i')
    call assert(is_allocated(xp_OPTS_(i)), err_notAlloc_, 'xp_OPTS_(i)')
    x => xp_OPTS_(i)
  end subroutine private_get_opt
end subroutine xp_opt__get_opt

!> @brief Returns a poiter pointing the xp_opt with the given id
!! @param[in] i @int optimiser identifier
!! @param[in] dtyp @xp_ @precision
!! @returns @xp_opt @pointer
function xp_opt__ooo (i, dtyp) result(ans)
  implicit none
  integer, intent(in) :: i
  real(kind=xp_), intent(in) :: dtyp
  type(xp_opt), pointer :: ans
  call do_safe_within("xp_opt__ooo", mod_registers_utils_name_, private_ooo)
contains
  subroutine private_ooo
    call get_opt(i, ans)
  end subroutine private_ooo
end function xp_opt__ooo


