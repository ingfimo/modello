module hashtabs_utils

  use env
  use errwarn
  use types
  use math, only: unif_rand, rand_int
  use utils
  
  character(len=*), parameter :: mod_hashtabs_utils_name_ = "hashtabs_utils"

  include "./hashing/interfaces.f95"

contains

  function hash_to_index (x, h) result(i)
    implicit none
    type(hashtab), intent(in) :: x
    integer, intent(in) :: h
    integer :: i
    i = modulo(h, size(x%t, 2)) + 1
  end function hash_to_index

  subroutine hashtab__allocate (x, m, n)
    implicit none
    type(hashtab), intent(inout) :: x
    integer, intent(in) :: m, n
    call do_safe_within("hashtab__allocate", mod_hashtabs_utils_name_, private_allocate)
  contains
    subroutine private_allocate
      integer :: info
      call alloc(x%t, m, n, "x%t")
      call alloc(x%r, n, "x%r")
      call alloc(x%m, n, "x%m")
    end subroutine private_allocate
  end subroutine hashtab__allocate

  subroutine hashtab__deallocate (x)
    implicit none
    type(hashtab), intent(inout) :: x
    call do_safe_within("hashtab__deallocate", mod_hashtabs_utils_name_, private_deallocate)
  contains
    subroutine private_deallocate
      call dealloc(x%t, "x%t")
      call dealloc(x%r, "x%r")
      call dealloc(x%m, "x%m")
    end subroutine private_deallocate
  end subroutine hashtab__deallocate

  subroutine hashtab__put (x, h, v)
    implicit none
    type(hashtab), intent(inout) :: x
    integer, intent(in) :: h, v
    integer :: i, j, r
    real(kind=sp_) :: k(1)
    i = hash_to_index(x, h)
    r = size(x%t, 1)
    if (x%r(i) == r) then
       call unif_rand(k)
       j = nint(k(1) * x%m(i)) + 1
       if (j <= r) x%t(j,i) = v
    else
       x%r(i) = x%r(i) + 1
       x%t(x%r(i),i) = v
    end if
    x%m(i) = x%m(i) + 1
  end subroutine hashtab__put

  function hashtab__get (x, h) result(v)
    implicit none
    type(hashtab), intent(in), target :: x
    integer, intent(in) :: h
    integer, pointer :: v(:)
    integer :: i
    i = hash_to_index(x, h)
    v => x%t(1:x%r(i),i)
  end function hashtab__get

  function hashtab__contains (x, h) result(ans)
    implicit none
    type(hashtab), intent(in) :: x
    integer, intent(in) :: h
    logical :: ans
    ans = x%r(hash_to_index(x, h)) > 0
  end function hashtab__contains
    
  subroutine hashtab__clear (x)
    implicit none
    type(hashtab), intent(inout) :: x
    x%t = 0
    x%r = 0
    x%m = 0
  end subroutine hashtab__clear

  include "./hashing/int.f95"
  include "./hashing/sp.f95"
  include "./hashing/dp.f95"
  
end module hashtabs_utils
