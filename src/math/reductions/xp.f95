!> @relates ssq
function xp__SSQ__1 (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x(:)
  real(kind=xp_) :: ans
  !$omp parallel workshare
  ans = sum(x**2)
  !$omp end parallel workshare
end function xp__SSQ__1

!> @relates dx_ssq
elemental function xp__dx_ssq (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = 2 * x
end function xp__dx_ssq

!> @relates ssq
function xp__SSQ__2 (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x(:,:)
  real(kind=xp_) :: ans
  !$omp parallel workshare
  ans = sum(x**2)
  !$omp end parallel workshare
end function xp__SSQ__2

!> relates l2norm
function xp__L2NORM__1 (x) result(ans)
  implicit none
  real(kind=xp_), intent (in) :: x(:)
  real(kind=xp_) :: ans
  ans = sqrt(ssq(x))
end function xp__L2NORM__1

!> relates l2norm
function xp__L2NORM__2 (x) result(ans)
  implicit none
  real(kind=xp_), intent (in) :: x(:,:)
  real(kind=xp_) :: ans
  ans = sqrt(ssq(x))
end function xp__L2NORM__2

!> @relates dx_product
function xp__DX_PRODUCT__1 (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x(:)
  real(kind=xp_) :: ans(size(x))
  !$omp parallel workshare
  ans = product(x) / x
  !$omp end parallel workshare
end function xp__dx_product__1

!> @relates dx_products
function xp__DX_PRODUCT__2 (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x(:,:)
  real(kind=xp_) :: ans(size(X,1),size(X,2))
  !$omp parallel workshare
  ans = product(x) / x
  !$omp end parallel workshare
end function xp__DX_PRODUCT__2



