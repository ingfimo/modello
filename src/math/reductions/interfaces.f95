  !> @addtogroup math__reductions_interfaces_ 
  !! @{

  !> Sum of squares
  !! @author Filippo Monari
  !! @param[in] x double precision array with up to two dimensions
  interface SSQ
     module procedure sp__SSQ__1
     module procedure dp__SSQ__1
     module procedure sp__SSQ__2
     module procedure dp__SSQ__2
  end interface SSQ

  interface dx_ssq
     module procedure sp__dx_ssq
     module procedure dp__dx_ssq
  end interface dx_ssq

  !> L2-Norm
  !! @author Filippo Monari
  !! @param[in] x double precision, rank 1 or 2 array
  interface L2NORM
     module procedure sp__L2NORM__1
     module procedure dp__L2NORM__1
     module procedure sp__L2NORM__2
     module procedure dp__L2NORM__2
  end interface L2NORM

  !> Product derivative
  !! @param[in] x input array (rank 1 or 2)
  interface dx_product
     module procedure sp__dx_product__1
     module procedure dp__dx_product__1
     module procedure sp__dx_product__2
     module procedure dp__dx_product__2
  end interface 

  !> @}
