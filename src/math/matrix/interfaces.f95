  !> @addtogroup math__matrx_interfaces_ 
  !! @{

  !> Identity Matrix
  !! Returns the identity matrix of the given dimension
  !! and of the given data type
  !! @param[in] n integer, matrix dimension
  !! @param[in] dtyp sp_ or dp_, output data type
  interface IM
     module procedure sp__IM
     module procedure dp__IM
  end interface IM

  !> Trace of a matrix
  !! Calculates the trace of a matrix
  !! @param[in] A sp_ or dp_, matrix
  interface TRACE
     module procedure sp__TRACE
     module procedure dp__TRACE
  end interface TRACE

  !> Log Trace
  !! Calculates the log of the trace of a matrix
  !! @param[in] A sp_ or dp_, matrix
  interface LOGTRACE
     module procedure sp__LOGTRACE
     module procedure dp__LOGTRACE
  end interface LOGTRACE

  !> Determinant of GETRF Decomposition
  !! Calcualtes the determinant of a matrix starting from the
  !! outputs of GETRF
  !! @param[in] A sp_ or dp_, matrix, factorization from GETRF
  !! @param[in] ipv integer, vector, permutation indexes from GETRF
  !! @param[out] D sp_ or dp_, determinant
  interface GETRF_DET
     module procedure sp__GETRF_DET
     module procedure dp__GETRF_DET
  end interface GETRF_DET

  !> Inverse Matrix
  !! Calculate the inverse of generic squared matrix.
  !! If the 'D' argument is provided the detrminant is
  !! calculated as well.
  !! @param[inout] A sp_ or dp_, (in) squared matrix, (out) inverse matrix
  !! @param[out] info integer, error flag
  !! @param[out] D sp_or dp_, optional, determinant of the matrix
  interface INVMAT
     module procedure sp__INVMAT
     module procedure dp__INVMAT
  end interface INVMAT

  !> Forward Differentiation of Inverse Matrix
  !! @param[in] dA sp_ or dp_, matrix, derivative of A
  !! @param[in] C sp_ or dp_, matrix, inverse of A
  !! @param[inout] dC sp_ or dp_, matrix, (in) intial value of the derivative of C, (out) updated value of the derivative of C 
  interface fdx_invmat
     module procedure sp__fdx_invmat
     module procedure dp__fdx_invmat
  end interface fdx_invmat

  !> Backward differentiation of Inverse Matrix
  !! @param[in] dC sp_ or dp_, matrix, derivative of the of A
  !! @param[in] C sp_ or dp_, matrix, inverse of A
  !! @param[inout] dA sp_ or dp_, matrix, (in) initial value of the derivative of A, (out) updated value of the derivative of A
  interface bdx_invmat
     module procedure sp__bdx_invmat
     module procedure dp__bdx_invmat
  end interface bdx_invmat

  !> Inverse of Symmetric Matrix
  !! Calculates the inverse of a symmetric marix.
  !! If the argument 'D' is given it calculates the determinant as well.
  !! @param[inout] A sp_ or dp_, (in) symmetric matrix, (out) inverse matrix
  !! @param[out] info integer, error flag
  !! @param[out] D sp_ or dp_, optional, matrix determinant
  interface invsymmat
     module procedure sp__invsymmat
     module procedure dp__invsymmat
  end interface invsymmat

  !> Solves the system of linear equations A.X = B w.r.t. X
  !! If the argument 'D' is given it calculates the determinant
  !! of A as well.
  !! @param[in] A sp_ or dp_, (in) rhs coefficient matrix
  !! @param[inout] B sp_ or dp_, (in) lhs matrix, (out) solution of the system
  !! @param[out] info integer, error flag
  !! @param[out] D sp_ or dp_, optional, determinant of A
  interface SOLVE
     module procedure sp__SOLVE
     module procedure dp__SOLVE
  end interface SOLVE

  !> Backward differentiation of SOLVE w.r.t. B
  !! @param[in] A sp_ or dp_, rhs coefficiet matrix
  !! @param[in] dX sp_ or dp_, derivative of X
  !! @param[inout] dB sp_ or dp_, (in) initial value of the derivative of B,
  !! (out) updated value
  interface bdB_solve
     module procedure sp__bdB_solve
     module procedure dp__bdB_solve
  end interface bdB_solve

  !> Backward differentiation of SOLVE w.r.t. A
  !! @param[in] dB sp_ or dp_, (in) initial value of the derivative of B
  !! @param[in] dX sp_ or dp_, derivative of X
  !! @param[inout] dA sp_ or dp_, (in) initial value of the derivative of A,
  !! (out) updated value
  interface bdA_solve
     module procedure sp__bdA_solve
     module procedure dp__bdA_solve
  end interface bdA_solve

  !> Solves the system of linear equations A.x = B,
  !! where A is a symmetric positive definite matrix
  !! @param[inout] A double precision matrix, (in) rhs coefficient matrix, (out)
  !! LT matrix from Cholesky decomposition
  !! @param[inout] B double precision matrix, (in) lhs matrx,
  !! (out) solution of the system
  !! @param[out] info integer, error flag
  !! @param[out] lnD sp
  interface SOLVESYM
     module procedure sp__SOLVESYM
     module procedure dp__SOLVESYM
  end interface SOLVESYM

  !> Backward differentiation of SOLVESYM w.r.t. B
  !! @param[in] A sp_ or dp_, rhs coefficient matrix
  !! @param[in] dX sp_ or dp_, derivative of X
  !! @param[inout] dB sp_ or dp_, (in) initial value of the derivative of B,
  !! (out) updated value
  !! @param[out] info integer, error flag
  interface bdB_solvesym
     module procedure sp__bdB_solvesym
     module procedure dp__bdB_solvesym
  end interface bdB_solvesym

  !> Backward differentiation of Matrix Determinant
  !! @param[in] IA sp_ or dp_, iverse matrix 
  !! @param[in] D sp_ or dp_, determinant
  !! @param[in] dD sp_ or dp_, determinant derivative
  interface BDX_DET
     module procedure sp__BDX_DET
     module procedure dp__BDX_DET
  end interface BDX_DET

  !> General matrix multiplication C := alpha * op(A).op(B) + beta * C
  !! @param[in] transA integer, 1 indicates op(A) = A**T, 0 indicates op(A) = A
  !! @param[in] transB integer, 0 indicates op(B) = B**T, 0 indicates op(B) = B
  !! @param[in] alpha double precision scalar
  !! @param[in] A double precision matrix
  !! @param[in] B double precision matrix
  !! @param[in] beta double precision scalar
  !! @param[inout] C double precision matrix, (out) result of the matrix multilication operation
  interface gmmmult
     module procedure sp__gmmmult
     module procedure dp__gmmmult
  end interface gmmmult

  !> Bakward differentiation of 'gmmmult' w.r.t. A
  !! Parameters share the name and meaning with 'gmmmult'
  !! @param[in] dC double precision matrix, derivative of C
  !! @param[inout] dA double precision matrix, derivative of A
  interface bdA_gmmmult
     module procedure sp__bdA_gmmmult
     module procedure dp__bdA_gmmmult
  end interface bdA_gmmmult

  !> Bakward differentiation of 'gmmmult' w.r.t. B
  !! Parameters share the name and meaning with 'gmmmult'
  !! @param[in] dC double precision matrix, derivative of C
  !! @param[inout] dB double precision matrix, derivative of B
  interface bdB_gmmmult
     module procedure sp__bdB_gmmmult
     module procedure dp__bdB_gmmmult
  end interface bdB_gmmmult

  !> Bakward differentiation of gmmmult w.r.t. alpha
  !! Parameters share the name and meaning with 'gmmmult'
  !! @param[in] dC double precision matrix, derivative of C
  !! @param[inout] dAlpha double precision matrix, derivative of alpha
  interface bdALPHA_gmmmult
     module procedure sp__bdALPHA_gmmmult
     module procedure dp__bdALPHA_gmmmult
  end interface bdALPHA_gmmmult

  !> General matrix multiplication for symmetric matrices
  !! Performs one of the matrix-matrix operations:
  !! 1. C := alpha*A*B + beta*C,
  !! or
  !! 2. C := alpha*B*A + beta*C,
  !! where alpha and beta are scalars,  A is a symmetric matrix and  B and
  !! C are m by n matrices.
  !! @param[in] side integer, if > 0 preforms (2) else (1)
  !! @param[in] uplo integer, if > 0 reference to UT of A else reference to LT of A
  !! @param[in] alpha double precision
  !! @param[in] A double precision(:,:), symmetric matrix
  !! @param[in] B double precision(:,:)
  !! @param[in] beta double precision
  !! @param[inout] C double precision(:,:)
  interface smmmult
     module procedure sp__smmmult
     module procedure dp__smmmult
  end interface smmmult

  !> General matrix vector multiplication y := alpha * op(A).x + beta * y
  !! @param[in] trans integer, 1 indicated op(A) = T(A), 0 indicates op(A) = A
  !! @param[in] alpha dpuble precision scalar
  !! @param[in] A double precision matrix
  !! @param[in] x double precision vector
  !! @param[in] beta double precision scalar
  !! @param[inout] y double precision vector
  interface gmvmult
     module procedure sp__gmvmult
     module procedure dp__gmvmult
  end interface gmvmult

  !> Bakward differentiation of gmvmult w.r.t. A
  !! Variables share the same meaning as in 'gmvmult'.
  !! @param[in] dy double precision vector, derivative of y
  !! @param[inout] dA double precision vector derivative of A
  interface bdA_gmvmult
     module procedure sp__bdA_gmvmult
     module procedure dp__bdA_gmvmult
  end interface bdA_gmvmult

  !> Bakward differentiation of 'gmvmult' w.r.t. x
  !! Parameters share the same name and meaning as in 'gmvmult'.
  !! @param[in] dy doauble preciaion vector, derivative of y
  !! @param[in] dx douvle precision vector, derivative of x
  interface bdx_gmvmult
     module procedure sp__bdx_gmvmult
     module procedure dp__bdx_gmvmult
  end interface bdx_gmvmult

  !> Bakward differentiation of 'gmvmult' w.r.t. alpha.
  !! Parametrs share the name and meaning with 'gmvmult'.
  !! @param[in] dy double precision, derivative of y
  !! @param[inout] dAlpha derivative of alpha
  interface bdalpha_gmvmult
     module procedure sp__bdalpha_gmvmult
     module procedure dp__bdalpha_gmvmult
  end interface bdalpha_gmvmult

  !> General outer product of two vectors: A := alpha * x.y**T + A
  !! @param[in] alpha double precision scalar
  !! @param[in] x double precision vector
  !! @param[in] y double precision vector
  !! @param[in] A double precision matrix
  interface vvouter
     module procedure sp__vvouter
     module procedure dp__vvouter
  end interface vvouter

  !> Inner product of two vectors: x**T.y
  !! @param[in] x doauble precision vector
  !! @param[in] y double precision vector
  interface vvinner
     module procedure sp__vvinner
     module procedure dp__vvinner
  end interface vvinner

  !> Forward differentiation of the quadratic form:
  !! C = B**T.A.B
  !! @param[in] A double precision(:,:)
  !! @param[in] B double precision(:,:)
  !! @param[in] dA double precision(:,:), derivative of A
  !! @param[in] dB double precision(:,:), derivative of B
  !! @param[in] dC double precision(:,:), derivative of C
  !! @param[inout] dA double precision(:,:), derivative of A
  interface fd_quadratic1
     module procedure sp__fd_quadratic1
     module procedure dp__fd_quadratic1
  end interface fd_quadratic1

  !> Bakward differentiation w.r.t. A and B of the quadratic form:
  !! C = B**T.A.B
  !! @param[in] A double precision(:,:)
  !! @param[in] B double precision(:,:)
  !! @param[in] dC double precision(:,:), derivative of C
  !! @param[inout] dA double precision(:,:), optional, derivative of A
  !! @param[inout] dB double precision(:,:), oprional, derivative of B
  interface bd_quadratic1
     module procedure sp__bd_quadratic1
     module procedure dp__bd_quadratic1
  end interface bd_quadratic1

  !> Bakward differentiation w.r.t. A and B of the quadratic form:
  !! C = B**T.A.B
  !! For the case when A is symmetric, calculation can be speed up
  !! with dp_symm.
  !! @param[in] A double precision(:,:)
  !! @param[in] B double precision(:,:)
  !! @param[in] dC double precision(:,:), derivative of C
  !! @param[inout] dA double precision(:,:), optional, derivative of A
  !! @param[inout] dB double precision(:,:), oprional, derivative of B
  interface bd_quadraticsym1
     module procedure sp__bd_quadraticsym1
     module procedure dp__bd_quadraticsym1
  end interface bd_quadraticsym1

  !> Bakward differentiation w.r.t. A and B of the quadratic form:
  !! C = B**T.A**(-1).B
  !! @param[in] IA double precision(:,:), inverse of A
  !! @param[in] B double precision(:,:)
  !! @param[in] dC double precision(:,:), derivative of C
  !! @param[inout] dA double precision(:,:), optional, derivative of A
  !! @param[inout] dB double precision(:,:), oprional, derivative of B
  interface bd_quadratic2
     module procedure sp__bd_quadratic2
     module procedure dp__bd_quadratic2
  end interface bd_quadratic2
  
  !> Calculates the quadratic form:
  !! Q = B**T . A**(-1) . B
  !! where A is symmetric and positive defined matrix.
  !! This function does not invert A but solve the following system of
  !! linear equations:
  !! A . X = B => X = A**(-1) . B
  !! Therefore, the quadaratic becomee equal to: 
  !! Q = B**T . X
  !! @param[in] A double precision(:,:), symmetric positive defined matrx
  !! @param[in] B double precision(:,:)
  !! @param[inout] Q double precision(:,:)
  !! @param[out] info integer, error flag
  !! @param[inout] lnD double precision, option, log-detrminant of A
  interface quadraticsym2
     module procedure sp__quadraticsym2
     module procedure dp__quadraticsym2
  end interface quadraticsym2

  !> Bakward differentiation w.r.t. A and B of the quadratic form:
  !! C = B**T .A**(-1) . B
  !! where A is symmetrix and positive defined.
  !! @param[in] A double precision(:,:)
  !! @param[in] B double precision(:,:)
  !! @param[in] dC double precision(:,:), derivative of C
  !! @param[inout] dA double precision(:,:), optional, derivative of A
  !! @param[inout] dB double precision(:,:), oprional, derivative of B
  interface bd_quadraticsym2
     module procedure sp__bd_quadraticsym2
     module procedure dp__bd_quadraticsym2
  end interface bd_quadraticsym2

  !> @}
