!> @relates im
function xp__IM (n, dtyp) result(im)
  implicit none
  integer, intent(in) :: n
  real(kind=xp_) :: dtyp
  real(kind=xp_) :: im(n,n)
  integer :: i
  !$omp parallel
  !$omp workshare
  im = 0._xp_
  !$omp end workshare
  !$omp do
  do i = 1, n
     im(i,i) = 1._xp_
  end do
  !$omp end do
  !$omp end parallel
end function xp__IM

!> @relates trace
function xp__TRACE (A) result(ans)
  implicit none
  real(kind=xp_), intent (in), target, contiguous :: A(:,:)
  real(kind=xp_) :: ans
  real(kind=xp_), pointer :: xa(:)
  integer :: n
  n = size(A, 1) + 1
  xa(1:size(A)) => A
  !$omp parallel workshare
  ans = sum(xa(1::n))
  !$omp end parallel workshare
end function xp__TRACE

!> @relates logtrace
function xp__LOGTRACE (A) result(ans)
  implicit none
  real(kind=xp_), intent(in), target, contiguous :: A(:,:)
  real(kind=xp_) :: ans
  real(kind=xp_), pointer :: xa(:)
  integer :: n
  n = size(A, 1) + 1
  xa(1:size(A)) => A
  !$omp parallel workshare
  ans = sum(log(xa(1::n)))
  !$omp end parallel workshare
end function xp__LOGTRACE

!> @relates getrf_det
subroutine xp__GETRF_DET (A, ipv, D)
  real(kind=xp_), intent(in) :: A(:,:)
  integer :: ipv(:)
  real(kind=xp_), intent(out) :: D
  integer :: i
  D = 1
  !$omp parallel do
  do i = 1, size(A, 1)
     D = D * A(i,i) * (-1)**ipv(i)
  end do
  !$omp end parallel do
end subroutine xp__GETRF_DET

!> @relates invmat
subroutine xp__INVMAT (A, info, D)
  implicit none
  real(kind=xp_), intent(inout) :: A(:,:)
  integer, intent(out) :: info
  real(kind=xp_), intent(out), optional :: D
  integer :: m, n, lda
  integer :: ipv(minval(shape(A)))
  real(kind=xp_) :: lwork(1)
  real(kind=xp_), allocatable :: work(:)
  info = 0
  m = size(A, 1)
  n = size(A, 2)
  lda = m
  info = 0
  call getrf(m, n, A, lda, ipv, info)
  call external_error_check(err_dgetrf_start_, info)
  if (info == 0 .and. present(D)) call GETRF_DET(A, ipv, D)
  if (info == 0) then
     call getri(n, A, lda, ipv, lwork, -1, info)
     call external_error_check(err_dgetri_start_, info)
  end if
  if (info == 0) then
     allocate(work(int(lwork(1))), stat=info)
     if (info /= 0) info = err_alloc_
  end if
  if (info == 0) then
     call getri(n, A, lda, ipv, work, int(lwork(1)), info)
     call external_error_check(err_dgetri_start_, info)
  end if
end subroutine xp__INVMAT

!> @relates fdx_invmat
pure subroutine xp__fdx_invmat (dA, C, dC)
  implicit none
  real(kind=xp_), intent(in) :: dA(:,:), C(:,:)
  real(kind=xp_), intent(inout) :: dC(:,:)
  real(kind=xp_) :: B(size(dC, 1),size(dA, 2))
  call gmmmult(0, 0, -1._xp_, C, dA, 0._xp_, B)
  call gmmmult(0, 0,  1._xp_, B, C, 1._xp_, dC) 
end subroutine xp__fdx_invmat

!> @relates bdx_invmat
pure subroutine xp__bdx_invmat (dC, C, dA)
  implicit none
  real(kind=xp_), intent(in) :: dC(:,:), C(:,:)
  real(kind=xp_), intent(inout) :: dA(:,:)
  real(kind=xp_) :: B(size(C, 2),size(dC, 2))
  call gmmmult(1, 0, -1._xp_, C, dC, 0._xp_, B)
  call gmmmult(0, 1, 1._xp_, B, C, 1._xp_, dA)  
end subroutine xp__bdx_invmat

!> @relates invsymmat
subroutine xp__INVSYMMAT (A, info, lnD)
  implicit none
  real(kind=xp_), intent(inout) :: A(:,:)
  integer, intent(out) :: info
  real(kind=xp_), intent(out), optional :: lnD
  integer :: n, lda
  info = 0
  n = size(A, 1)
  lda = max(1, n)
  call potrf('L', n, A, lda, info)
  call external_error_check(err_dpotrf_start_, info)
  if (info == 0 .and. present(lnD)) lnD = 2 * LOGTRACE(A)
  if (info == 0) then
     call potri('L', n, A, lda, info)
     call external_error_check(err_dpotri_start_, info)
  end if
  if (info == 0) call private_fill
contains
  subroutine private_fill
    integer :: i, j
    n = size(A, 1) 
    !$omp parallel do
    do j = 1, n - 1 
       do i = j+1, n
          A(j,i) = A(i,j)
       end do
    end do
    !$omp end parallel do
  end subroutine private_fill
end subroutine xp__INVSYMMAT

!> @relates solve
subroutine xp__SOLVE (A, B, info, D)
  implicit none
  real(kind=xp_), intent(in) :: A(:,:)
  real(kind=xp_), intent(inout) :: B(:,:)
  integer, intent(out) :: info
  real(kind=xp_), intent(out), optional :: D
  real(kind=xp_) :: XA(size(A, 1),size(A, 2))
  integer :: n, lda, nrhs, ldb, ipv(size(A, 2))
  info = 0
  XA = A
  n = size(A, 1)
  lda = max(1, n)
  nrhs = size(B, 2)
  ldb = max(1, n)
  ipv = 0
  call gesv(n, nrhs, XA, lda, ipv, B, ldb, info)
  call external_error_check(err_dgesv_start_, info)
  if (info == 0 .and. present(D)) call GETRF_DET(XA, ipv, D)
end subroutine xp__SOLVE

!> @relates bdb_solve
subroutine xp__bdB_solve (A, dX, dB, info)
  implicit none
  real(kind=xp_), intent(in) :: A(:,:), dX(:,:)
  real(kind=xp_), intent(inout) :: dB(:,:)
  integer, intent(out) :: info
  real(kind=xp_) :: AA(size(A, 1),size(A, 2))
  dB = dX
  AA = transpose(A)
  call SOLVE(AA, dB, info)
end subroutine xp__bdB_solve

!> @relates bda_solve
subroutine xp__bdA_solve (dB, X, dA)
  implicit none
  real(kind=xp_), intent(in) :: dB(:,:), X(:,:)
  real(kind=xp_), intent(out) :: dA(:,:)
  call gmmmult(0, 1, -1._xp_, dB, X, 1._xp_, dA)
end subroutine xp__bdA_solve

!> @relates solvesym
subroutine xp__SOLVESYM (A, B, info, lnD)
  implicit none
  real(kind=xp_), intent(in) :: A(:,:)
  real(kind=xp_), intent(inout) :: B(:,:)
  integer, intent(out) :: info
  real(kind=xp_), intent(out), optional :: lnD
  real(kind=xp_) :: XA(size(A, 1), size(A, 2))
  integer :: n, lda, nrhs, ldb
  info = 0
  XA = A
  n = size(A, 1)
  lda = max(1, n)
  nrhs = size(B, 2)
  ldb = max(1, n)
  call posv('L', n, nrhs, XA, lda, B, ldb, info)
  call external_error_check(err_dposv_start_, info)
  if (info == 0 .and. present(lnD)) lnD = 2 * LOGTRACE(XA)
end subroutine xp__SOLVESYM

!> @relates bdb_solvesym
subroutine xp__bdB_solvesym (A, dX, dB, info)
  implicit none
  real(kind=xp_), intent(in) :: A(:,:), dX(:,:)
  real(kind=xp_), intent(inout) :: dB(:,:)
  integer, intent(out) :: info
  real(kind=xp_) :: AA(size(A, 1),size(A, 2))
  dB = dX
  AA = transpose(A)
  call SOLVESYM(AA, dB, info)
end subroutine xp__bdB_solvesym

!> @relates bdx_det
function xp__BDX_DET (IA, D, dD, sym) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: IA(:,:), D, dD
  real(kind=xp_) :: ans(size(IA,1),size(IA,2))
  logical, intent(in) :: sym
  if (sym) then
     !$omp parallel workshare
     ans = dD * D * IA
     !$omp end parallel workshare
  else
     !$omp parallel workshare
     ans = dD * D * transpose(IA)
     !$omp end parallel workshare
  end if
end function xp__BDX_DET

!> @relates gmmmult
pure subroutine xp__gmmmult (transA, transB, alpha, A, B, beta, C)
  implicit none
  integer, intent(in) :: transA 
  integer, intent(in) :: transB 
  real(kind=xp_), intent(in) :: alpha, A(:,:), B(:,:), beta
  real(kind=xp_), intent(inout) :: C(:,:)
  integer :: LDA, MC, LDB, N, M, K
  character(len=1) :: tA, tB
  M = merge(size(A, 1), size(A, 2), transA < 1)
  K = merge(size(A, 2), size(A, 1), transA < 1)
  LDA = merge(M, K, transA < 1)
  ta = merge('N', 'T', transA < 1)
  LDB = merge( &
       merge(size(B, 1), size(B, 2), transB < 1), &
       merge(size(B, 2), size(B, 1), transB < 1), &
       transB < 1)
  tB = merge('N', 'T', transB < 1)
  MC = size(C, 1)
  N = size(C, 2)
  call gemm(tA, tB, M, N, K, alpha, A, LDA, B, LDB, beta, C, MC)  
end subroutine xp__gmmmult

!> @relates bda_gmmmult
pure subroutine xp__bdA_gmmmult (transA, transB, alpha, B, dC, dA)
  implicit none
  integer, intent(in) :: transA, transB
  real(kind=xp_), intent(in) :: alpha, B(:,:), dC(:,:)
  real(kind=xp_), intent(inout) :: dA(:,:)
  if (transA > 0) then
     !A%dx = B x t(C%dx) => leave B as it was in the original operation
     call gmmmult(transB, 1, alpha, B, dC, 1._xp_, dA)
  else
     !A%dx = C%dx x t(B%dx) => transpose B w.r.t. the original operation
     call gmmmult(0, merge(0, 1, transB > 0), alpha, dC, B, 1._xp_, dA)
  end if
end subroutine xp__bdA_gmmmult

!> @relates bdb_gmmmult
pure subroutine xp__bdB_gmmmult (transA, transB, alpha, A, dC, dB)
  implicit none
  integer, intent(in) :: transA, transB
  real(kind=xp_), intent(in) :: alpha, A(:,:), dC(:,:)
  real(kind=xp_), intent(inout) :: dB(:,:)
  if (transB > 0) then
     !B%dx = t(C%dx) x A => leave A as it was in the original operation
     call gmmmult(1, transA, alpha, dC, A, 1._xp_, dB)
  else
     !B%dx = t(A) x C => transpose A w.r.t. the original operation
     call gmmmult(merge(0, 1, transA > 0), 0, alpha, A, dC, 1._xp_, dB)
  end if
end subroutine xp__bdB_gmmmult

!> @relates bdalpha_gmmmult
subroutine xp__bdALPHA_gmmmult(transa, transB, A, B, dC, dAlpha)
  implicit none
  integer, intent(in) :: transA, transB
  real(kind=xp_), intent(in) :: A(:,:), B(:,:), dC(:,:)
  real(kind=xp_), intent(inout) :: dAlpha
  real(kind=xp_), allocatable :: X(:,:)
  integer :: m, n
  m = merge(size(A, 2), size(A, 1), transA > 0)
  n = merge(size(B, 1), size(B, 2), transB > 0)
  allocate(X(m,n))
  X = 0
  call gmmmult(transA, transB, 1._xp_, A, B, 0._xp_, X)
  !$omp parallel workshare
  dAlpha = dAlpha + sum(X * dC)
  !$omp end parallel workshare
end subroutine xp__bdALPHA_gmmmult

!> @relates smmmult
pure subroutine xp__smmmult (side, uplo, alpha, A, B, beta, C)
  implicit none
  integer, intent(in) :: side
  integer, intent(in) :: uplo
  real(kind=xp_), intent(in) :: alpha, A(:,:), B(:,:), beta
  real(kind=xp_), intent(inout) :: C(:,:)
  integer :: M, N, LDA, LDB, LDC
  character(len=1) :: sd, ul
  M = size(C, 1)
  N = size(C, 2)
  LDA = size(A, 1)
  LDB = size(B, 1)
  LDC = size(C, 1)
  sd = merge('R', 'L', side > 0)
  ul = merge('L', 'U', uplo > 0)
  call symm(sd, ul, M, N, alpha, A, LDA, B, LDB, beta, C, LDC)
end subroutine xp__smmmult

!> @relates gmvmult
pure subroutine xp__gmvmult (trans, alpha, A, x, beta, y)
  implicit none
  integer, intent(in) :: trans
  real(kind=xp_), intent(in) :: alpha, A(:,:), x(:), beta
  real(kind=xp_), intent(inout) :: y(:)
  character :: ta
  integer :: M, N, LDA
  M = size(A, 1)
  N = size(A, 2) 
  LDA = M
  ta = merge('N', 'T', trans < 1)
  call gemv(ta, M, N, alpha, A, LDA, x, 1, beta, y, 1)
end subroutine xp__gmvmult

!> @relates bda_gmvmult
pure subroutine xp__bdA_gmvmult (trans, alpha, x, dy, dA)
  implicit none
  integer, intent(in) :: trans
  real(kind=xp_), intent(in) :: alpha, x(:), dy(:)
  real(kind=xp_), intent(inout) :: dA(:,:)
  if (trans > 0) then
     call vvouter(alpha, x, dy, dA)
  else
     call vvouter(alpha, dy, x, dA)
  end if
end subroutine xp__bdA_gmvmult

!> @relates bdx_gmvmult
pure subroutine xp__bdx_gmvmult (trans, alpha, A, dy, dx)
  implicit none
  integer, intent(in) :: trans
  real(kind=xp_), intent(in) :: alpha, A(:,:), dy(:)
  real(kind=xp_), intent(inout) :: dx(:)
  call gmvmult(merge(0, 1, trans > 0), alpha, A, dy, 1._xp_, dx)
end subroutine xp__bdx_gmvmult

!> @relates bdalpha_gmvmult
subroutine xp__bdalpha_gmvmult (trans, A, x, dy, dAlpha)
  implicit none
  integer, intent(in) :: trans
  real(kind=xp_), intent(in) :: A(:,:), x(:), dy(:)
  real(kind=xp_), intent(inout) :: dAlpha
  real(kind=xp_), allocatable :: yy(:)
  allocate(yy(size(dy)))
  yy = 0
  call gmvmult(trans, 1._xp_, A, x, 0._xp_, yy)
  !$omp parallel workshare
  dAlpha = dAlpha + sum(yy * dy)
  !$omp end parallel workshare
end subroutine xp__bdalpha_gmvmult

!> @relates vvouter
pure subroutine xp__vvouter (alpha, x, y, A)
  implicit none
  real(kind=xp_), intent(in) :: alpha, x(:), y(:)
  real(kind=xp_), intent(inout) :: A(:,:)
  call ger(size(x), size(y), alpha, x, 1, y, 1, A, size(x))
end subroutine xp__vvouter

!> @relates vvinner
pure function xp__vvinner (x, y) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x(:), y(:)
  real(kind=xp_) :: ans
  ans = dot(size(x), x, 1, y, 1) 
end function xp__vvinner

!> @relates fd_quadratic1
subroutine xp__fd_quadratic1 (A, B, dA, dB, dC)
  implicit none
  real(kind=xp_), intent(in), dimension(:,:) :: A, B, dA, dB
  real(kind=xp_), intent(inout) :: dC(:,:)
  real(kind=xp_) :: X(size(dB,2),size(A,2))
  real(kind=xp_) :: Y(size(B,2),size(dA,2))
  real(kind=xp_) :: Z(size(B,2),size(A,2))
  !$omp parallel sections
  !$omp section
  call gmmmult(1, 0, 1._xp_, dB, A, 0._xp_, X)
  !$omp section
  call gmmmult(1, 0, 1._xp_, B, dA, 0._xp_, Y)
  !$omp section
  call gmmmult(1, 0, 1._xp_, B, A, 0._xp_, Z)
  !$omp end parallel sections
  call gmmmult(0, 0, 1._xp_, X, B, 0._xp_, dC)
  call gmmmult(0, 0, 1._xp_, Y, B, 1._xp_, dC)
  call gmmmult(0, 0, 1._xp_, Z, dB, 1._xp_, dC)
end subroutine xp__fd_quadratic1

!> @relates bd_quadratic1
subroutine xp__bd_quadratic1 (A, B, dC, dA, dB)
  implicit none
  real(kind=xp_), intent(in), dimension(:,:) :: A, B, dC
  real(kind=xp_), intent(inout), optional :: dA(:,:), dB(:,:)
  real(kind=xp_) :: BdC(size(B,1),size(dC,2))
  real(kind=xp_) :: AB(size(A,1),size(B,2))
  call gmmmult(0, 0, 1._xp_, B, dC, 0._xp_, BdC)
  !$omp parallel sections
  !$omp section
  if (present(dA)) then
     call gmmmult(0, 1, 1._xp_, BdC, B, 1._xp_, dA)
  end if
  !$omp section
  if (present(dB)) then
     call gmmmult(0, 0, 1._xp_, A, B, 0._xp_, AB)
     call gmmmult(0, 1, 1._xp_, AB, dC, 1._xp_, dB)
     call gmmmult(1, 0, 1._xp_, A, BdC, 1._xp_, dB)
  end if
  !$omp end parallel sections
end subroutine xp__bd_quadratic1

!> @relates bd_quadraticsym1
subroutine xp__bd_quadraticsym1 (A, B, dC, dA, dB)
  implicit none
  real(kind=xp_), intent(in), dimension(:,:) :: A, B, dC
  real(kind=xp_), intent(inout), optional :: dA(:,:), dB(:,:)
  real(kind=xp_) :: BdC(size(B,1),size(dC,2))
  real(kind=xp_) :: AB(size(A,1),size(B,2))
  call gmmmult(0, 0, 1._xp_, B, dC, 0._xp_, BdC)
  !$omp parallel sections
  !$omp section
  if (present(dA)) then
     call gmmmult(0, 1, 1._xp_, BdC, B, 1._xp_, dA)
  end if
  !$omp section
  if (present(dB)) then
     call smmmult(0, 0, 1._xp_, A, B, 0._xp_, AB)
     call gmmmult(0, 1, 1._xp_, AB, dC, 1._xp_, dB)
     call smmmult(0, 0, 1._xp_, A, BdC, 1._xp_, dB)
  end if
  !$omp end parallel sections
end subroutine xp__bd_quadraticsym1

!> @relates bd_quadratic2
subroutine xp__BD_QUADRATIC2 (IA, B, dC, dA, dB)
  implicit none
  real(kind=xp_), intent(in), dimension(:,:) :: IA, B, dC
  real(kind=xp_), intent(inout), optional :: dA(:,:), dB(:,:)
  real(kind=xp_) :: IATB(size(IA,2),size(B,2))
  real(kind=xp_) :: IAB(size(IA,1),size(B,2))
  real(kind=xp_) :: BTIAT(size(B,2),size(IA,1))
  real(kind=xp_) :: IATBdC(size(IA,2),size(dC,2))
  call gmmmult(1, 0, 1._xp_, IA, B, 0._xp_, IATB)
  call gmmmult(0, 0, 1._xp_, IATB, dC, 0._xp_, IATBdC)
  !$omp parallel sections
  !$omp section
  if (present(dA)) then
     call gmmmult(1, 1, 1._xp_, B, IA, 0._xp_, BTIAT)
     call gmmmult(0, 0, -1._xp_, IATBdC, BTIAT, 1._xp_, dA)
  end if
  !$omp section
  if (present(dB)) then
     dB = dB + IATBdC
     call gmmmult(0, 0, 1._xp_, IA, B, 0._xp_, IAB)
     call gmmmult(0, 1, 1._xp_, IAB, dC, 1._xp_, dB)
  end if
  !$omp end parallel sections
end subroutine xp__BD_QUADRATIC2

!> @relates quadraticsym2
subroutine xp__quadraticsym2 (A, B, Q, info, lnD)
  implicit none
  real(kind=xp_), intent(in) :: A(:,:) 
  real(kind=xp_), intent(in) :: B(:,:)
  real(kind=xp_), intent(inout) :: Q(:,:)
  integer, intent(out) :: info
  real(kind=xp_), intent(inout), optional :: lnD
  real(kind=xp_) :: BB(size(B,1),size(B,2))
  BB = B
  if (present(lnD)) then
     call SOLVESYM(A, BB, info, lnD)
  else
     call SOLVESYM(A, BB, info)
  end if
  if (info==0) call gmmmult(1, 0, 1._xp_, BB, B, 0._xp_, Q)
end subroutine xp__quadraticsym2

!> @ bd_quadraticsym2
subroutine xp__BD_QUADRATICSYM2 (A, B, dC, dA, dB, lnD, info)
  implicit none
  real(kind=xp_), intent(in), dimension(:,:) :: A, B
  real(kind=xp_), intent(in) :: dC(:,:)
  real(kind=xp_), intent(inout), optional :: dA(:,:), dB(:,:), lnD
  integer, intent(out) :: info
  real(kind=xp_) :: X(size(A,2),size(dC,2)), BB(size(B,1),size(B,2))
  BB = B
  if (present(lnd)) then
     call SOLVESYM(A, BB, info, lnD)
  else
     call SOLVESYM(A, BB, info)
  end if
  call gmmmult(0, 0, 1._xp_, B, dC, 0._xp_, X)
  !$omp parallel sections
  !$omp section
  if (present(dA)) then
     call gmmmult(0, 1, -1._xp_, X, B, 1._xp_, dA)
  end if
  !$omp section
  if (present(dB)) then
     dB = dB + X
     call gmmmult(0, 1, 1._xp_, B, dC, 1._xp_, dB)
  end if
  !$omp end parallel sections
end subroutine xp__BD_QUADRATICSYM2



