pure function conv (x, k, dmask) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x(:), k(:)
  integer, intent(in), optional :: dmask(:)
  real(kind=xp_) :: ans
  if (present(dmask)) then
     ans = vvinner(x(dmask), k)
  else
     ans = vvinner(x * k)
  end if
end function conv
