  !> @addtogroup math_functions_
  !! @{

  !> @brief Kroneker Delta
  !! @details
  !! @f[
  !! \delta_{i,j} = \{1\ \mbox{if}\ i == j\ \mbox{else}\ 0\}
  !! @f]
  interface deltaij
     !> @ref math::sp__deltaij a
     module procedure sp__deltaij
     !> @ref math::dp__deltaij b
     module procedure dp__deltaij
  end interface deltaij

  !> @brief Digamma Function, derivative of the Gamma function
  !! @todo add formula
  interface psi
     module procedure sp__psi
     module procedure dp__psi
  end interface psi

  !> Log-Beta Function
  !! @todo add formula
  interface logbeta
     module procedure sp__logbeta
     module procedure dp__logbeta
  end interface logbeta

  !> Derivative of th Log-Beta Function w.r.t. a
  interface da_logbeta
     module procedure sp__da_logbeta
     module procedure dp__da_logbeta
  end interface da_logbeta

  !> Derivative of the Log-Beta Function w.r.t. b
  interface db_logbeta
     module procedure sp__db_logbeta
     module procedure dp__db_logbeta
  end interface db_logbeta

  !> Interface - Derivative of @f$ x^a @f$ w.r.t. x
  interface dx_xpow
     module procedure sp__dx_xpow
     module procedure dp__dx_xpow
  end interface dx_xpow

  !> Derivative of @f$ a^x w.r.t. x @f$
  interface dx_powx
     module procedure sp__dx_powx
     module procedure dp__dx_powx
  end interface dx_powx

  !> Derivative of @f$ abs(x) @f$ w.r.t. x
  interface dx_abs
     module procedure sp__dx_abs
     module procedure dp__dx_abs
  end interface dx_abs

  !> Derivative of @f$ exp(x) @f$ w.r.t. x
  interface dx_exp
     module procedure sp__dx_exp
     module procedure dp__dx_exp
  end interface dx_exp

  !> Derivative of @f$ log(x) @f$ w.r.t. x
  interface dx_log
     module procedure sp__dx_log
     module procedure dp__dx_log
  end interface dx_log

  !> Interface - Derivative of sin(x) w.r.t. x
  !!
  !! Elemental functions implementing the derivative of sin(x) w.r.t. x for
  !! different real precisions
  !!
  !! @param[in] x sp_ or dp_
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dx_sin
     !> Elemental function for singular precision inputs
     module procedure sp__dx_sin
     !> Elemental function for double precision inputs
     module procedure dp__dx_sin
  end interface dx_sin

  !> Interface - Derivative of cos(x) w.r.t. x
  !!
  !! Elemental functions implementing the derivative of cos(x) w.r.t. x for
  !! different real precisions
  !!
  !! @param[in] x sp_ od dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dx_cos
     !> Elemental function for singular precision inputs
     module procedure sp__dx_cos
     !> Elemental function for double precision inputs
     module procedure dp__dx_cos
  end interface dx_cos

  !> Interface - Derivative of tan(x) w.r.t. x
  !!
  !! Elemental functions implementing the derivative of tan(x) w.r.t. x for
  !! different real precisions
  !!
  !! @param[in] x sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dx_tan
     !> Elemental function for singular precision inputs
     module procedure sp__dx_tan
     !> Elemental function for double precision inputs
     module procedure dp__dx_tan
  end interface dx_tan

  !> Interface - Derivative of sinh(x) w.r.t. x
  !!
  !! Elemental functions implementing the derivative of sinh(x) w.r.t. x for
  !! different real precisions
  !!
  !! @param[in] x sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dx_sinh
     !> Elemental function for singular precision inputs
     module procedure sp__dx_sinh
     !> Elemental function for double precision inputs
     module procedure dp__dx_sinh
  end interface dx_sinh

  !> Interface - Derivative of cosh(x) w.r.t. x
  !!
  !! Elemental functions implementing the derivative of cosh(x) w.r.t. x for
  !! different real precisions
  !!
  !! @param[in] x sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dx_cosh
     !> Elemental function for singular precision inputs
     module procedure sp__dx_cosh
     !> Elemental function for double precision inputs
     module procedure dp__dx_cosh
  end interface dx_cosh

  !> Interface - Derivative of tanh(x) w.r.t. x
  !!
  !! Elemental functions implementing the derivative of tanh(x) w.r.t. x for
  !! different real precisions
  !!
  !! @param[in] x sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dx_tanh
     !> Elemental function for singular precision inputs
     module procedure sp__dx_tanh
     !> Elemental function for double precision inputs
     module procedure dp__dx_tanh
  end interface dx_tanh

  !> Interface - Squared Exponential Function
  !!
  !! Elemental functions implementing the Squared Exponential function for different real precisions:
  !! @f[
  !! sqexp(x1, x2, a, b) = a \times \mbox{exp}(-b * (x1 - x2)^2)
  !! @f]
  !! @param[in] x1 sp_ or dp_, scalar or array
  !! @param[in] x2 sp_ or dp_, scalar or array
  !! @param[in] b sp_ or dp_, scalar or array, inverse characteristic length or rate
  !! @param[in] a sp_ or dp_, scalar or array, amplitude
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface sqexp
     !> Elemental function for singular precision inputs
     module procedure sp__sqexp
     !> Elemental function for double precision inputs
     module procedure dp__sqexp
  end interface sqexp

  !> Interface - Derivative of the Squared Exponential Function w.r.t. a
  !!
  !! Elemental functions implementing the Squared Exponential's derivative w.r.t. a for different real precisions
  !!
  !! @param[in] x1 sp_ or dp_, scalar or array
  !! @param[in] x2 sp_ or dp_, scalar or array
  !! @apram[in] b sp_ or dp_, , scalar or array, inverse charactersitc length or rate
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface da_sqexp
     !> Elemental function for singular precision inputs
     module procedure sp__da_sqexp
     !> Elemental function for double precision inputs
     module procedure dp__da_sqexp
  end interface da_sqexp

  !> Interface - Derivative of the Squared Exponential Function w.r.t. b
  !!
  !! Elemental functions implementing the Squared Exponential's derivative w.r.t. b for different real precisions
  !!
  !! @param[in] x1 sp_ or dp_, scalar or array
  !! @param[in] x2 sp_ or dp_, scalar or array
  !! @param[in] a sp_ or dp_, scalar or array, amplitude paramter
  !! @apram[in] b sp_ or dp_, scalar or array, inverse charactersitc length or rate
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface db_sqexp
     !> Elemental function for singular precision inputs
     module procedure sp__db_sqexp
     !> Elemental function for double precision inputs
     module procedure dp__db_sqexp
  end interface db_sqexp

  !> Interface - Derivative of the Squared Exponential Function w.r.t. x1
  !!
  !! Elemental functions implementing the Squared Exponential's derivative w.r.t. x1 for different real precisions
  !!
  !! @param[in] x1 sp_ or dp_, scalar or array
  !! @param[in] x2 sp_ or dp_, scalar or array
  !! @param[in] a sp_ or dp_, scalar or array, amplitude paramter
  !! @apram[in] b sp_ or dp_, scalar or array, inverse charactersitc length or rate
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dx1_sqexp
     !> Elemental function for singular precision inputs
     module procedure sp__dx1_sqexp
     !> Elemental function for double precision inputs
     module procedure dp__dx1_sqexp
  end interface dx1_sqexp

  !> Interface - Derivative of the Squared Exponential Function w.r.t. x2
  !!
  !! Elemental functions implementing the Squared Exponential's derivative w.r.t. x2 for different real precisions
  !!
  !! @param[in] x1 sp_ or dp_, scalar or array
  !! @param[in] x2 sp_ or dp_, scalar or array
  !! @param[in] a sp_ or dp_, scalar or array, amplitude paramter
  !! @apram[in] b sp_ or dp_, scalar or array, inverse charactersitc length or rate
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dx2_sqexp
     !> Elemental function for singular precision inputs
     module procedure sp__dx2_sqexp
     !> Elemental function for double precision inputs
     module procedure dp__dx2_sqexp
  end interface dx2_sqexp

  !> @}
