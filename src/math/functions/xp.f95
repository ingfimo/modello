!> @addtogroup math_functions_private_
!! @{

!> @brief Krnoneker delta for xp_ precision
!! @param[in] i first indicator variable
!! @param[in] j second indicator variable
elemental function xp__deltaij(i, j, dtyp) result(k)
  implicit none
  integer, intent(in) :: i, j
  real(kind=xp_), intent(in) :: dtyp
  real(kind=xp_) :: k
  k = merge(1._xp_, 0._xp_, i == j)
end function xp__deltaij

!> @brief Psi function for xp_ precision
!! @param[in] x real scalar or array
elemental function xp__psi (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans, xx, r
  real(kind=xp_), parameter :: c = 8.5_dp_ 
  ! Approximate for smal x
  if (x < tiny_xp_) then
     ans = - euler_mascheroni_xp_ - x**(-1) + ReimannAt2_xp_ * x
     return
  end if
  ! Reduce to psi(X + N)
  xx = x
  ans = 0
  do while (xx < c)
     ans = ans - 1._xp_ / xx
     xx = xx + 1._xp_
  end do
  ! Use Stirling's (actually de Moivre's) expansion.
  r = 1._xp_ / xx
  ans = ans + log(xx) - 0.5_xp_ * r
  r = r**2
  ans = ans &
       - r * (12._xp_**(-1) &
       - r * (120._xp_**(-1) &
       - r * (252._xp_**(-1) &
       - r * (240._xp_**(-1) &
       - r * (132._xp_**(-1))))))
end function xp__psi

!> @brief logarithm of the beta function for xp_ precision
!! @param[in] a lower bound of the integral
!! @param[in] b upper bound of the integral
elemental function xp__logbeta (a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: a, b
  real(kind=xp_) :: ans
  ans = log_gamma(a) + log_gamma(b) - log_gamma(a + b)
end function xp__logbeta

!> @brief
!! derivative of the logarithm of the beta function for xp_ precision
!! with respect to the lower bound of the integral
!! @param[in] a lower bound of the integral
!! @param[in] b upper bound of the integral
elemental function xp__da_logbeta (a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: a, b
  real(kind=xp_) :: ans
  ans = psi(a) - psi(a + b)
end function xp__da_logbeta

!> @brief
!! derivative of the logarithm of the beta function for xp_ precision
!! with respect to the upper bound of the integral
!! @param[in] a lower bound of the integral
!! @param[in] b upper bound of the integral
elemental function xp__db_logbeta (a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: a, b
  real(kind=xp_) :: ans
  ans = psi(b) - psi(a + b)
end function xp__db_logbeta

!> @brief derivative of @f$ x^a @f$ with respect to x
!! @param[in] x real scalar or array
!! @param[in] a real scalar or array
elemental function xp__dx_xpow (x, a) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x, a
  real(kind=xp_) :: ans
  ans = a * x**(a-1)
end function xp__dx_xpow

!> @brief derivative pf @f$ a^x @f$ with respect to x
!! @param[in] a real scalar or array
!! @param[in] x real scalar or array
elemental function xp__dx_powx (a, x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: a, x
  real(kind=xp_) :: ans
  ans = a**x * log(a)
end function xp__dx_powx

!> @brief derivative of @f$ abs(x) @f$
!! @param[in] x scalar or array
elemental function xp__dx_abs (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = sign(1._xp_, x)
end function xp__dx_abs

!> @brief derivative of @f$ exp(x) @f$
!! @param[in] x scalar or array
elemental function xp__dx_exp (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = exp(x)
end function xp__dx_exp

!> @brief derivative of @f$ log(x) @f$
!! @param[in] x scalar or array
elemental function xp__dx_log (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = 1/x
end function xp__dx_log

!> @brief derivative of @f$ sin(x) @f$
!! @param[in] x scalar or array
elemental function xp__dx_sin (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = cos(x)
end function xp__dx_sin

!> @brief derivative of @f$ cos(x) @f$
!! @param[in] x scalar or array
elemental function xp__dx_cos (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = -sin(x)
end function xp__dx_cos

!> @brief derivative of @f$ tan(x) @f$
!! @param[in] x scalar or array
elemental function xp__dx_tan (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = (1/cos(x))**2
end function xp__dx_tan

!> @brief derivative of @f$ sinh(x) @f$
!! @param[in] x scalar or array
elemental function xp__dx_sinh (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = cosh(x)
end function xp__dx_sinh

!> @brief derivative of @f$ cosh(x) @f$
!! @param[in] x scalar or array
elemental function xp__dx_cosh (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = sinh(x)
end function xp__dx_cosh

!> @brief derivative of @f$ tanh(x) @f$
!! @param[in] x scalar or array
elemental function xp__dx_tanh (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = (1/cosh(x))**2
end function xp__dx_tanh

!> @brief squared exponential function
!! @details @f[ a * exp(-b * (x1 - x2)^2) @f]
!! @param[in] x1 scalar or array
!! @param[in] x2 scalar or array
!! @param[in] a scalar or array
!! @param[in] b scalar or array
elemental function xp__sqexp (x1, x2, a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x1, x2, a, b
  real(kind=xp_) :: ans
  ans = a * exp(-(x1 - x2)**2 * b)
end function xp__sqexp

!> @brief derivtive of the square exponential function
!! with respect to a
!! @param[in] x1 scalar or array
!! @param[in] x2 scalar or array
!! @param[in] b scalar or array
elemental function xp__da_sqexp (x1, x2, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x1, x2, b
  real(kind=xp_) :: ans
  ans = exp(-(x1 - x2)**2 * b)
end function xp__da_sqexp

!> @details derivative of the square exponential function
!! with respect to b
!! @param[in] x1 scalar or array
!! @param[in] x2 scalar or array
!! @param[in] a scalar or array
!! @param[in] b scalar or array
elemental function xp__db_sqexp (x1, x2, a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x1, x2, a, b
  real(kind=xp_) :: ans
  ans = -a * (x1 - x2)**2 * exp(-(x1 - x2)**2 * b)
end function xp__db_sqexp

!> @details derivative of the square exponential function
!! with respect to x1
!! @param[in] x1 scalar or array
!! @param[in] x2 scalar or array
!! @param[in] a scalar or array
!! @param[in] b scalar or array
elemental function xp__dx1_sqexp (x1, x2, a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x1, x2, a, b
  real(kind=xp_) :: ans
  ans = -2 * b * (x1 - x2) * a * exp(-(x1 - x2)**2 * b)
end function xp__dx1_sqexp

!> @details derivative of the square exponential function
!! with respect to x2
!! @param[in] x1 scalar or array
!! @param[in] x2 scalar or array
!! @param[in] a scalar or array
!! @param[in] b scalar or array
elemental function xp__dx2_sqexp (x1, x2, a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x1, x2, a, b
  real(kind=xp_) :: ans
  ans = 2 * b * (x1 - x2) * a * exp(-(x1 - x2)**2 * b)
end function xp__dx2_sqexp

!> @}
