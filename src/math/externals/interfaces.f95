  !> @addtogroup math__external_interfaces_
  !! @author Filippo Monari
  !! @{

  !> Interface - GETRF LAPACK subroutine
  !!
  !! GETRF computes an LU factorization of a general M-by-N matrix using partial pivoting with row interchanges.
  !!
  !! The unit diagonal elements of L are not storedin A on exit.
  !!
  !! INFO may have the following values:
  !! * = 0:  successful exit
  !! * < 0:  if INFO = -i, the i-th argument had an illegal value
  !! * > 0:  if INFO = i, U(i,i) is exactly zero. The factorization has been completed, but the factor U is exactly
  !! singular, and division by zero will occur if it is used to solve a system of equations.
  !!
  !! @param[in] M integer, number of rows of matrix A
  !! @param[in] N integer, number of columns of matrix A
  !! @param[inout] A sp_ or dp_ array of dimension (LDA,N). (in) the M-by-N matrix to be factored. (out) the factors L and U, 
  !! @param[in] LDA integer, leading dimension of A: LDA >= max(M, 1)
  !! @param[out] IPIV intger array, dimension (min(M,N)). The pivot indices; for 1 <= i <= min(M,N), row i of A was interchanged with row IPIV(i).
  !! @param[out] INFO integer, error flag.
  interface getrf
     !> Singular precision subroutine.
     !! Documentation: http://www.netlib.org/lapack/explore-html/de/de2/sgetrf_8f.html
     pure subroutine SGETRF (M, N, A, LDA, IPIV, INFO)
       use env, only: sp_
       implicit none
       integer, intent(in) :: M, N, LDA
       real(kind=sp_), intent(inout) :: A(LDA,*)
       integer, intent(out) :: IPIV(*), INFO
     end subroutine SGETRF
     !> Double precision subroutine.
     !! Documentation: http://www.netlib.org/lapack/explore-html/d3/d6a/dgetrf_8f.html
     pure subroutine DGETRF (M, N, A, LDA, IPIV, INFO)
       use env, only: dp_
       implicit none
       integer, intent(in) :: M, N, LDA
       real(kind=dp_), intent(inout) :: A(LDA,*)
       integer, intent(out) :: IPIV(*), INFO
     end subroutine DGETRF
  end interface getrf

  !> Interface - GETRI LAPCK sunroutine GETRI
  !!
  !! GETRI computes the inverse of a matrix using the LU factorization computed by GETRF.
  !!
  !! If LWORK = -1, then a workspace query is assumed; the routine
  !! only calculates the optimal size of the WORK array, returns this value as the first entry of the WORK array.
  !!
  !! INFO may have the following values:
  !! * = 0:  successful exit
  !! * < 0:  if INFO = -i, the i-th argument had an illegal value
  !! * > 0:  if INFO = i, U(i,i) is exactly zero; the matrix is singular and its inverse could not be computed.
  !!
  !! @param[in] N integer, the order of the matrix A. N >= 0.
  !! @param[inout] A sp_ or dp_, dimension (LDA,N). (in) the factors computed by GETRF. (out) if INFO = 0, the inverse of matrix A.
  !! @param[in] LDA integer, the leading dimension of the array A. LDA >= max(1,N).
  !! @param[in] IPIV integer, dimension (N), the pivot indices from GETRF.
  !! @param[out] WORK sp_ or dp_, dimension (MAX(1,LWORK)). If INFO=0, then WORK(1) returns the optimal LWORK.
  !! @param[in] LWORK integer, the dimension of the array WORK. 
  !! @param[out] INFO integer, error flag.
  interface getri
     !> Singular precision subroutine.
     !! Documentation: http://www.netlib.org/lapack/explore-html/d0/d31/sgetri_8f.html
     pure subroutine SGETRI (N, A, LDA, IPIV, WORK, LWORK, INFO)
       use env, only: sp_
       implicit none
       integer, intent(in) :: N, LDA, LWORK, IPIV(*)
       real(kind=sp_), intent(inout) :: A(LDA,*)
       real(kind=sp_), intent(out) :: work(*)
       integer, intent(out) :: INFO
     end subroutine SGETRI
     !> Double precision subroutine.
     !! Documentation: http://www.netlib.org/lapack/explore-html/df/da4/dgetri_8f.html
     pure subroutine DGETRI (N, A, LDA, IPIV, WORK, LWORK, INFO)
       use env, only: dp_
       implicit none
       integer, intent(in) :: N, LDA, LWORK, IPIV(*)
       real(kind=dp_), intent(inout) :: A(LDA,*)
       real(kind=dp_), intent(out) :: work(*)
       integer, intent(out) :: INFO
     end subroutine DGETRI
  end interface getri
  
  interface gesv
     pure subroutine SGESV (N, NRHS, A, LDA, IPIV, B, LDB, INFO)
       use env, only: sp_
       implicit none
       integer, intent(in) :: N, NRHS, LDA, LDB
       real(kind=sp_), intent(inout) :: A(LDA,*), B(LDB,*)
       integer, intent(out) :: IPIV(*), INFO
     end subroutine SGESV
     pure subroutine DGESV (N, NRHS, A, LDA, IPIV, B, LDB, INFO)
       use env, only: dp_
       implicit none
       integer, intent(in) :: N, NRHS, LDA, LDB
       real(kind=dp_), intent(inout) :: A(LDA,*), B(LDB,*)
       integer, intent(out) :: IPIV(*), INFO
     end subroutine DGESV
  end interface gesv
  
  interface potrf
     pure subroutine SPOTRF (UPLO, N, A, LDA, INFO)
       use env, only: sp_
       implicit none
       character(len=1), intent(in) :: UPLO
       integer, intent(in) :: N, LDA
       real(kind=sp_), intent(inout) :: A(LDA,*)
       integer, intent(out) :: INFO
     end subroutine SPOTRF
     pure subroutine DPOTRF (UPLO, N, A, LDA, INFO)
       use env, only: dp_
       implicit none
       character(len=1), intent(in) :: UPLO
       integer, intent(in) :: N, LDA
       real(kind=dp_), intent(inout) :: A(LDA,*)
       integer, intent(out) :: INFO
     end subroutine DPOTRF
  end interface potrf

  interface potri
     pure subroutine SPOTRI (UPLO, N, A, LDA, INFO)
       use env, only: sp_
       implicit none
       character(len=1), intent(in) :: UPLO
       integer, intent(in) :: N, LDA
       real(kind=sp_), intent(inout) :: A(LDA,*)
       integer, intent(out) :: INFO
     end subroutine SPOTRI
     pure subroutine DPOTRI (UPLO, N, A, LDA, INFO)
       use env, only: dp_
       implicit none
       character(len=1), intent(in) :: UPLO
       integer, intent(in) :: N, LDA
       real(kind=dp_), intent(inout) :: A(LDA,*)
       integer, intent(out) :: INFO
     end subroutine DPOTRI
  end interface potri

  interface posv
     pure subroutine SPOSV (UPLO, N, NRHS, A, LDA, B, LDB, INFO)
       use env, only: sp_
       implicit none
       character(len=1), intent(in) :: UPLO
       integer, intent(in) :: N, NRHS, LDA, LDB
       integer, intent(out) :: INFO
       real(kind=sp_), intent(inout) :: A(LDA,N), B(LDB,NRHS)
     end subroutine SPOSV
     pure subroutine DPOSV (UPLO, N, NRHS, A, LDA, B, LDB, INFO)
       use env, only: dp_
       implicit none
       character(len=1), intent(in) :: UPLO
       integer, intent(in) :: N, NRHS, LDA, LDB
       integer, intent(out) :: INFO
       real(kind=dp_), intent(inout) :: A(LDA,N), B(LDB,NRHS)
     end subroutine DPOSV
  end interface posv

  interface gemm
     pure subroutine SGEMM (TRANSA, TRANSB, M, N, K, ALPHA, A, LDA, B, LDB, BETA, C, LDC)
       use env, only: sp_
       implicit none
       character, intent(in) :: TRANSA, TRANSB
       integer, intent(in) :: M, N, K, LDA, LDB, LDC
       real(kind=sp_), intent(in) :: A(LDA,*), B(LDB,*), ALPHA, BETA
       real(kind=sp_), intent(inout) :: C(LDC,*)
     end subroutine SGEMM
     pure subroutine DGEMM (TRANSA, TRANSB, M, N, K, ALPHA, A, LDA, B, LDB, BETA, C, LDC)
       use env, only: dp_
       implicit none
       character, intent(in) :: TRANSA, TRANSB
       integer, intent(in) :: M, N, K, LDA, LDB, LDC
       real(kind=dp_), intent(in) :: A(LDA,*), B(LDB,*), ALPHA, BETA
       real(kind=dp_), intent(inout) :: C(LDC,*)
     end subroutine DGEMM
  end interface gemm

  interface symm
     pure subroutine SSYMM(SIDE, UPLO, M, N, ALPHA, A, LDA, B, LDB, BETA, C, LDC)
       use env, only: sp_
       implicit none
       character, intent(in) :: SIDE, UPLO
       integer, intent(in) :: M, N, LDA, LDB, LDC
       real(kind=sp_), intent(in) :: ALPHA, A(LDA,*), B(LDB,N), BETA, C(LDC,N)
     end subroutine SSYMM
     pure subroutine DSYMM(SIDE, UPLO, M, N, ALPHA, A, LDA, B, LDB, BETA, C, LDC)
       use env, only: dp_
       implicit none
       character, intent(in) :: SIDE, UPLO
       integer, intent(in) :: M, N, LDA, LDB, LDC
       real(kind=dp_), intent(in) :: ALPHA, A(LDA,*), B(LDB,N), BETA, C(LDC,N)
     end subroutine DSYMM
  end interface symm

  interface gemv
     pure subroutine SGEMV (TRANS, M, N, ALPHA, A, LDA, X, INCX, BETA, Y, INCY)
       use env, only: sp_
       implicit none
       character, intent(in) :: TRANS
       integer, intent(in) :: M, N, LDA, INCX, INCY
       real(kind=sp_), intent(in):: ALPHA, A(LDA,N), X(*), BETA
       real(kind=sp_), intent(inout) :: Y(*)
     end subroutine SGEMV
     pure subroutine DGEMV (TRANS, M, N, ALPHA, A, LDA, X, INCX, BETA, Y, INCY)
       use env, only: dp_
       implicit none
       character, intent(in) :: TRANS
       integer, intent(in) :: M, N, LDA, INCX, INCY
       real(kind=dp_), intent(in):: ALPHA, A(LDA,N), X(*), BETA
       real(kind=dp_), intent(inout) :: Y(*)
     end subroutine DGEMV
  end interface gemv

  interface ger
     pure subroutine SGER (M, N, alpha, X, INCX, Y, INCY, A, LDA)
       use env, only: sp_
       implicit none
       integer, intent(in) :: M, N, INCX, INCY, LDA
       real(kind=sp_), intent(in) :: alpha, X(*), Y(*)
       real(kind=sp_), intent(inout) :: A(LDA,*)
     end subroutine SGER
     pure subroutine DGER (M, N, alpha, X, INCX, Y, INCY, A, LDA)
       use env, only: dp_
       implicit none
       integer, intent(in) :: M, N, INCX, INCY, LDA
       real(kind=dp_), intent(in) :: alpha, X(*), Y(*)
       real(kind=dp_), intent(inout) :: A(LDA,*)
     end subroutine DGER
  end interface ger

  interface dot
     pure function SDOT (N, DX, INCX, DY, INCY)
       use env, only: sp_
       integer, intent(in) :: N, INCX, INCY
       real(kind=sp_), intent(in) :: DX(N), DY(N)
       real(kind=sp_) :: DDOT
     end function SDOT
     pure function DDOT (N, DX, INCX, DY, INCY)
       use env, only: dp_
       integer, intent(in) :: N, INCX, INCY
       real(kind=dp_), intent(in) :: DX(N), DY(N)
       real(kind=dp_) :: DDOT
     end function DDOT
  end interface dot
  

  interface r__unif_rand
     real(c_double) function r__unif_rand__dp_ (dtyp) bind(c)
       use iso_c_binding
       real(kind=c_double), intent(out) :: dtyp
     end function r__unif_rand__dp_
     real(c_float) function r__unif_rand__sp_ (dtyp) bind(c)
       use iso_c_binding
       real(kind=c_float), intent(out) :: dtyp
     end function r__unif_rand__sp_
  end interface r__unif_rand

  !> @}
