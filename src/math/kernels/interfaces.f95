  !> @addtogroup math__kernels_interfaces_
  !! @{

  !> Interface - KSQEXP
  !!
  !! Functions implementing the Squared Exponential Kernel for
  !! different real precision.
  !!
  !! @param[in] x1 sp_ or sp_, array of dimension (:)
  !! @param[in] x2 sp_ or sp_, array of dimension (:)
  !! @param[in] a sp_ or sp_, scalar
  !! @param[in] b sp_ or sp_, scalar or array of dimension (:)
  !! (see individual interface members)
  !! @return A scalar of the same type of the given inputs
  interface KSQEXP
     !> Function for singular precision inputs
     !! @param[in] b sp_, scalar
     module procedure sp__KSQEXP__1
     !> Function for double precision inputs
     !! @param[in] b dp_, scalar
     module procedure dp__KSQEXP__1
     !> Function for singular precision inputs
     !! @param[in] b sp_, array of dimension (:)
     module procedure sp__KSQEXP__2
     !> Function for singular precision inputs
     !! @param[in] b dp_, array of dimension (:)
     module procedure dp__KSQEXP__2
  end interface KSQEXP

  !> Interface - Derivative of KSQEXP w.r.t. a
  !!
  !! Functions implementing the Squared Exponential Kernel's
  !! derivative w.r.t. a for different real precision.
  !!
  !! @param[in] x1 sp_ or sp_, array of dimension (:)
  !! @param[in] x2 sp_ or sp_, array of dimension (:)
  !! @param[in] a sp_ or sp_, scalar
  !! @param[in] b sp_ or sp_, scalar or array of dimension (:)
  !! (see individual interface members)
  !! @return A scalar of the same type of the given inputs
  interface DA_KSQEXP
     !> Function for singular precision inputs
     !! @param[in] b sp_, scalar
     module procedure sp__DA_KSQEXP__1
     !> Function for double precision inputs
     !! @param[in] b dp_, scalar
     module procedure dp__DA_KSQEXP__1
     !> Function for singular precision inputs
     !! @param[in] b sp_, array of dimension (:)
     module procedure sp__DA_KSQEXP__2
     !> Function for singular precision inputs
     !! @param[in] b dp_, array of dimension (:)
     module procedure dp__DA_KSQEXP__2
  end interface DA_KSQEXP

  !> Interface - Derivative of KSQEXP w.r.t. b
  !!
  !! Functions implementing the Squared Exponential Kernel's
  !! derivative w.r.t. a for different real precision.
  !!
  !! @param[in] x1 sp_ or sp_, array of dimension (:)
  !! @param[in] x2 sp_ or sp_, array of dimension (:)
  !! @param[in] a sp_ or sp_, scalar
  !! @param[in] b sp_ or sp_, scalar or array of dimension (:)
  !! (see individual interface members)
  !! @return A scalar of the same type of the given inputs
  interface DB_KSQEXP
     !> Function for singular precision inputs
     !! @param[in] b sp_, scalar
     module procedure sp__DB_KSQEXP__1
     !> Function for double precision inputs
     !! @param[in] b dp_, scalar
     module procedure dp__DB_KSQEXP__1
     !> Function for singular precision inputs
     !! @param[in] b sp_, array of dimension (:)
     module procedure sp__DB_KSQEXP__2
     !> Function for singular precision inputs
     !! @param[in] b dp_, array of dimension (:)
     module procedure dp__DB_KSQEXP__2
  end interface DB_KSQEXP

  !> Interface - Derivative of KSQEXP w.r.t. x1
  !!
  !! Functions implementing the Squared Exponential Kernel's
  !! derivative w.r.t. a for different real precision.
  !!
  !! @param[in] x1 sp_ or sp_, array of dimension (:)
  !! @param[in] x2 sp_ or sp_, array of dimension (:)
  !! @param[in] a sp_ or sp_, scalar
  !! @param[in] b sp_ or sp_, scalar or array of dimension (:)
  !! (see individual interface members)
  !! @return A scalar of the same type of the given inputs
  interface DX1_KSQEXP
     !> Function for singular precision inputs
     !! @param[in] b sp_, scalar
     module procedure sp__DX1_KSQEXP__1
     !> Function for double precision inputs
     !! @param[in] b dp_, scalar
     module procedure dp__DX1_KSQEXP__1
     !> Function for singular precision inputs
     !! @param[in] b sp_, array of dimension (:)
     module procedure sp__DX1_KSQEXP__2
     !> Function for singular precision inputs
     !! @param[in] b dp_, array of dimension (:)
     module procedure dp__DX1_KSQEXP__2
  end interface DX1_KSQEXP

  !> Interface - Derivative of KSQEXP w.r.t. x1
  !!
  !! Functions implementing the Squared Exponential Kernel's
  !! derivative w.r.t. a for different real precision.
  !!
  !! @param[in] x1 sp_ or sp_, array of dimension (:)
  !! @param[in] x2 sp_ or sp_, array of dimension (:)
  !! @param[in] a sp_ or sp_, scalar
  !! @param[in] b sp_ or sp_, scalar or array of dimension (:)
  !! (see individual interface members)
  !! @return A scalar of the same type of the given inputs
  interface DX2_KSQEXP
     !> Function for singular precision inputs
     !! @param[in] b sp_, scalar
     module procedure sp__DX2_KSQEXP__1
     !> Function for double precision inputs
     !! @param[in] b dp_, scalar
     module procedure dp__DX2_KSQEXP__1
     !> Function for singular precision inputs
     !! @param[in] b sp_, array of dimension (:)
     module procedure sp__DX2_KSQEXP__2
     !> Function for singular precision inputs
     !! @param[in] b dp_, array of dimension (:)
     module procedure dp__DX2_KSQEXP__2
  end interface DX2_KSQEXP

  !> @}
