!> @relates ksqexp
pure function xp__KSQEXP__1 (x1, x2, a, b) result (ans)
  implicit none
  real(kind=xp_), intent(in) :: x1(:), x2(:), a, b
  real(kind=xp_) :: ans
  !omp parallel workshare
  ans = product(sqexp(x1, x2, a, b))
  !omp end parallel workshare
end function xp__KSQEXP__1

!> @realtes da_ksqexp
function xp__DA_KSQEXP__1 (x1, x2, a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x1(:), x2(:), a, b
  real(kind=xp_) :: ans, s(size(x1))
  !omp parallel workshare
  s = sqexp(x1, x2, a, b)
  ans = sum(product(s)/s * da_sqexp(x1, x2, b))
  !omp end parallel workshare
end function xp__DA_KSQEXP__1

!> @realtes db_ksqexp
function xp__DB_KSQEXP__1 (x1, x2, a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x1(:), x2(:), a, b
  real(kind=xp_) :: ans, s(size(x1))
  !$omp parallel workshare
  s = sqexp(x1, x2, a, b)
  ans = sum(product(s)/s * db_sqexp(x1, x2, a, b))
  !$omp end parallel workshare
end function xp__DB_KSQEXP__1

!> @relates dx1_ksqexp
function xp__DX1_KSQEXP__1 (x1, x2, a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x1(:), x2(:), a, b
  real(kind=xp_) :: ans(size(x1))
  !ans = dx_product(sqexp(x1, x2, a, b)) * dx_sqexp_x1(x1, x2, a, b)
  !$omp parallel workshare
  ans = sqexp(x1, x2, a, b)
  ans = product(ans) / ans * dx1_sqexp(x1, x2, a, b)
  !$omp end parallel workshare
end function xp__DX1_KSQEXP__1

!> @realtes dx2_ksqexp
function xp__DX2_KSQEXP__1 (x1, x2, a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x1(:), x2(:), a, b
  real(kind=xp_) :: ans(size(x2))
  !$omp parallel workshare
  ans = sqexp(x1, x2, a, b)
  ans = product(ans) / ans * dx2_sqexp(x1, x2, a, b)
  !$omp end parallel workshare
end function xp__DX2_KSQEXP__1

!> @relates ksqexp
function xp__KSQEXP__2 (x1, x2, a, b) result (ans)
  implicit none
  real(kind=xp_), intent(in) :: x1(:), x2(:), a, b(:)
  real(kind=xp_) :: ans
  !$omp parallel workshare
  ans = product(sqexp(x1, x2, a, b))
  !$omp end parallel workshare
end function xp__KSQEXP__2

!> @relates da_ksqexp
function xp__DA_KSQEXP__2 (x1, x2, a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x1(:), x2(:), a, b(:)
  real(kind=xp_) :: ans, s(size(x1))
  !$omp parallel workshare
  s = sqexp(x1, x2, a, b)
  ans = sum(product(s) / s * da_sqexp(x1, x2, b))
  !$omp end parallel workshare
end function xp__DA_KSQEXP__2

!> @relates db_ksqexp
function xp__DB_KSQEXP__2 (x1, x2, a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x1(:), x2(:), a, b(:)
  real(kind=xp_) :: ans(size(b))
  !$omp parallel workshare
  ans = sqexp(x1, x2, a, b)
  ans = product(ans) / ans * db_sqexp(x1, x2, a, b)
  !$omp end parallel workshare
end function xp__DB_KSQEXP__2

!> @relates dx1_ksqexp
function xp__DX1_KSQEXP__2 (x1, x2, a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x1(:), x2(:), a, b(:)
  real(kind=xp_) :: ans(size(x1))
  !$omp parallel workshare
  ans = sqexp(x1, x2, a, b)
  ans = product(ans)/ans * dx1_sqexp(x1, x2, a, b)
  !$omp end parallel workshare
end function xp__DX1_KSQEXP__2

!> @relates dx2_ksqexp
function xp__DX2_KSQEXP__2 (x1, x2, a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x1(:), x2(:), a, b(:)
  real(kind=xp_) :: ans(size(x2))
  !$omp parallel workshare
  ans = sqexp(x1, x2, a, b)
  ans = product(ans)/ans * dx2_sqexp(x1, x2, a, b)
  !$omp end parallel workshare
end function xp__DX2_KSQEXP__2

