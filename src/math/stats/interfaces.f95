  !> @addtogroup math__stats_interfaces_
  !! @{

  !> Interface - Uniform log-Density Distribution
  !!
  !! Elemental functions implmenting the logarithm of the uniform distribution
  !! function
  !!
  !! @param[in] a sp_ or dp_, scalar or array, lower bound
  !! @param[in] b sp_ or dp_, scalar or array, upper bound
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface ldunif
     !> Elemental function for singular precision real inputs
     module procedure sp__ldunif 
     !> Elemental function for double precision real inputs
     module procedure dp__ldunif
  end interface ldunif

  !> Interface - Exponential log-Density Distribution
  !!
  !! Elemental function implmenting the logarithm of the exponential distribution
  !! function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] lam sp_ or dp_, scalar or array, rate
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface ldexp
     !> Elemental function for singular precision real inputs
     module procedure sp__ldexp
     !> Elemental function for double precision real inputs
     module procedure dp__ldexp
  end interface ldexp

  !> Interface - Derivative w.r.t. lam of the Exponential log-Density Distribution
  !!
  !! Elemental function implmenting the derivative w.r.t. lam of the
  !! logarithm of the exponential distribution function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] lam sp_ or dp_, scalar or array, rate
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dlam_ldexp
     module procedure sp__dlam_ldexp
     module procedure dp__dlam_ldexp
  end interface dlam_ldexp

  !> Interface - Derivative w.r.t. y of the Exponential log-Density Distribution
  !!
  !! Elemental function implmenting the derivative w.r.t. y of the
  !! logarithm of the exponential distribution function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] lam sp_ or dp_, scalar or array, rate
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dy_ldexp
     !> Elemental function for singular precision real inputs
     module procedure sp__dy_ldexp
     !> Elemental function for double precision real inputs
     module procedure dp__dy_ldexp
  end interface dy_ldexp

  !> Interface - Laplace log-Density Distribution
  !!
  !! Elemental function implmenting the logarithm of the Laplace distribution
  !! function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] mu sp_ or dp_, scalar or array, offset
  !! @param[in] lam sp_ or dp_, scalar or array, rate
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface ldlaplace
     !> Elemental function for singular precision real inputs
     module procedure sp__ldlaplace
     !> Elemental function for double precision real inputs
     module procedure dp__ldlaplace
  end interface ldlaplace

  !> Interface - Derivative w.r.t. mu of the Laplace log-Density Distribution
  !!
  !! Elemental function implmenting the derivative w.r.t. mu of
  !! the logarithm of the Laplace distribution function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] mu sp_ or dp_, scalar or array, offset
  !! @param[in] lam sp_ or dp_, scalar or array, rate
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dmu_ldlaplace
     !> Elemental function for singular precision real inputs
     module procedure sp__dmu_ldlaplace
     !> Elemental function for double precision real inputs
     module procedure dp__dmu_ldlaplace
  end interface dmu_ldlaplace

  !> Interface - Derivative w.r.t. lam of the Laplace log-Density Distribution
  !!
  !! Elemental function implmenting the derivative w.r.t. lam of
  !! the logarithm of the Laplace distribution function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] mu sp_ or dp_, scalar or array, offset
  !! @param[in] lam sp_ or dp_, scalar or array, rate
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dlam_ldlaplace
     !> Elemental function for singular precision real inputs
     module procedure sp__dlam_ldlaplace
     !> Elemental function for double precision real inputs
     module procedure dp__dlam_ldlaplace
  end interface dlam_ldlaplace

  !> Interface - Derivative w.r.t. y of the Laplace log-Density Distribution
  !!
  !! Elemental function implmenting the derivative w.r.t. y of
  !! the logarithm of the Laplace distribution function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] mu sp_ or dp_, scalar or array, offset
  !! @param[in] lam sp_ or dp_, scalar or array, rate
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dy_ldlaplace
     !> Elemental function for singular precision real inputs
     module procedure sp__dy_ldlaplace
     !> Elemental function for double precision real inputs
     module procedure dp__dy_ldlaplace
  end interface dy_ldlaplace

  !> Interface - Beta log-Density Distribution
  !!
  !! Elemental function implmenting the logarithm of the beta distribution
  !! function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] a1 sp_ or dp_, scalar or array, shape
  !! @param[in] a2 sp_ or dp_, scalar or array, shape
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface ldbeta
     !> Elemental function for singular precision real inputs
     module procedure sp__ldbeta
     !> Elemental function for double precision real inputs
     module procedure dp__ldbeta
  end interface ldbeta

  !> Interface - Beta log-Density Distribution's Derivative w.r.t. a1
  !!
  !! Elemental function implmenting the derivative w.r.t. a1 of the
  !! logarithm of the beta distribution function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] a1 sp_ or dp_, scalar or array, shape
  !! @param[in] a2 sp_ or dp_, scalar or array, shape
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface da1_ldbeta
     !> Elemental function for singular precision real inputs
     module procedure sp__da1_ldbeta
     !> Elemental function for double precision real inputs
     module procedure dp__da1_ldbeta
  end interface da1_ldbeta

  !> Interface - Beta log-Density Distribution's Derivative w.r.t. a2
  !!
  !! Elemental function implmenting the derivative w.r.t. a2 of the
  !! logarithm of the beta distribution function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] a1 sp_ or dp_, scalar or array, shape
  !! @param[in] a2 sp_ or dp_, scalar or array, shape
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface da2_ldbeta
     !> Elemental function for singular precision real inputs
     module procedure sp__da2_ldbeta
     !> Elemental function for double precision real inputs
     module procedure dp__da2_ldbeta
  end interface da2_ldbeta

  !> Interface - Beta log-Density Distribution's Derivative w.r.t. y
  !!
  !! Elemental function implmenting the derivative w.r.t. y of the
  !! logarithm of the beta distribution function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] a1 sp_ or dp_, scalar or array, shape
  !! @param[in] a2 sp_ or dp_, scalar or array, shape
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dy_ldbeta
     !> Elemental function for singular precision real inputs
     module procedure sp__dy_ldbeta
     !> Elemental function for double precision real inputs
     module procedure dp__dy_ldbeta
  end interface dy_ldbeta

  !> Interface - Gamma log-Density Distribution
  !!
  !! Elemental function implmenting the logarithm of the beta distribution
  !! function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] a sp_ or dp_, scalar or array, shape
  !! @param[in] b sp_ or dp_, scalar or array, rate
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface ldgamma
     !> Elemental function for singular precision real inputs
     module procedure sp__ldgamma
     !> Elemental function for double precision real inputs
     module procedure dp__ldgamma
  end interface ldgamma

  !> Interface - Gamma log-Density Distribution' Derivative w.r.t. a
  !!
  !! Elemental function implmenting the derivative w.r.t. a of the
  !! logarithm of the beta distribution function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] a sp_ or dp_, scalar or array, shape
  !! @param[in] b sp_ or dp_, scalar or array, rate
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface da_ldgamma
     !> Elemental function for singular precision real inputs
     module procedure sp__da_ldgamma
     !> Elemental function for double precision real inputs
     module procedure dp__da_ldgamma
  end interface da_ldgamma

  !> Interface - Gamma log-Density Distribution' Derivative w.r.t. b
  !!
  !! Elemental function implmenting the derivative w.r.t. b of the
  !! logarithm of the beta distribution function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] a sp_ or dp_, scalar or array, shape
  !! @param[in] b sp_ or dp_, scalar or array, rate
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface db_ldgamma
     !> Elemental function for singular precision real inputs
     module procedure sp__db_ldgamma
     !> Elemental function for double precision real inputs
     module procedure dp__db_ldgamma
  end interface db_ldgamma

  !> Interface - Gamma log-Density Distribution' Derivative w.r.t. y
  !!
  !! Elemental function implmenting the derivative w.r.t. y of the
  !! logarithm of the beta distribution function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] a sp_ or dp_, scalar or array, shape
  !! @param[in] b sp_ or dp_, scalar or array, rate
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dy_ldgamma
     !> Elemental function for singular precision real inputs
     module procedure sp__dy_ldgamma
     !> Elemental function for double precision real inputs
     module procedure dp__dy_ldgamma
  end interface dy_ldgamma

  !> Interface - Normal log-Density Distribution
  !!
  !! Elemental function implmenting the
  !! logarithm of the normal distribution function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] mu sp_ or dp_, scalar or array, mean
  !! @param[in] s sp_ or dp_, scalar or array, variance
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface ldnorm
     !> Elemental function for singular precision real inputs
     module procedure sp__ldnorm
     !> Elemental function for double precision real inputs
     module procedure dp__ldnorm
  end interface ldnorm

  !> Interface - Normal log-Density Distribution' Derivative w.r.t. s
  !!
  !! Elemental function implmenting the derivative w.r.t. s of the 
  !! logarithm of the normal distribution function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] mu sp_ or dp_, scalar or array, mean
  !! @param[in] s sp_ or dp_, scalar or array, variance
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface ds_ldnorm
     !> Elemental function for singular precision real inputs
     module procedure sp__ds_ldnorm
     !> Elemental function for double precision real inputs
     module procedure dp__ds_ldnorm
  end interface ds_ldnorm

  !> Interface - Normal log-Density Distribution' Derivative w.r.t. mu
  !!
  !! Elemental function implmenting the derivative w.r.t. mu of the 
  !! logarithm of the normal distribution function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] mu sp_ or dp_, scalar or array, mean
  !! @param[in] s sp_ or dp_, scalar or array, variance
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dmu_ldnorm
     !> Elemental function for singular precision real inputs
     module procedure sp__dmu_ldnorm
     !> Elemental function for double precision real inputs
     module procedure dp__dmu_ldnorm
  end interface dmu_ldnorm

  !> Interface - Normal log-Density Distribution' Derivative w.r.t. y
  !!
  !! Elemental function implmenting the derivative w.r.t. y of the 
  !! logarithm of the normal distribution function
  !!
  !! @param[in] y sp_ or dp_, scalar or array, variable value
  !! @param[in] mu sp_ or dp_, scalar or array, mean
  !! @param[in] s sp_ or dp_, scalar or array, variance
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dy_ldnorm
     !> Elemental function for singular precision real inputs
     module procedure sp__dy_ldnorm
     !> Elemental function for double precision real inputs
     module procedure dp__dy_ldnorm
  end interface dy_ldnorm

  !> @todo add documentation and rename
  interface BD_LDMVNORM__1
     module procedure sp__BD_LDMVNORM__1
     module procedure dp__BD_LDMVNORM__1
  end interface BD_LDMVNORM__1

  !> @todo add documentation and rename
  interface LDMVNORM__2
     module procedure sp__LDMVNORM__2
     module procedure dp__LDMVNORM__2
  end interface LDMVNORM__2

  !> @}
