!> @relates ldunif
elemental function xp__ldunif (a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: a, b
  real(kind=xp_) :: ans
  ans = 1 / (b - a)
end function xp__ldunif

!> @relates ldexp
elemental function xp__ldexp (y, lam) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, lam
  real(kind=xp_) :: ans
  ans = merge(log(lam) - lam * y, 0._xp_, y > 0)
end function xp__ldexp

!> @relates dlam_ldexp
elemental function xp__dlam_ldexp (y, lam) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, lam
  real(kind=xp_) :: ans
  ans = merge(lam**(-1) - y, 0._xp_, y > 0)
end function xp__dlam_ldexp

!> @relates dy_ldexp
elemental function xp__dy_ldexp (y, lam) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, lam
  real(kind=xp_) :: ans
  ans = merge(lam, 0._xp_, y > 0)
end function xp__dy_ldexp

!> @relates ldlaplace
elemental function xp__ldlaplace (y, mu, lam) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, lam, mu
  real(kind=xp_) :: ans
  ans = log(0.5_xp_) + log(lam) - lam * abs(y - mu)
end function xp__ldlaplace

!> @relates dmu_ldlaplace
elemental function xp__dmu_ldlaplace (y, mu, lam) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, mu, lam
  real(kind=xp_) :: ans
  ans = dx_abs(y - mu) * lam
end function xp__dmu_ldlaplace

!> @relates dlam_ldlaplace
elemental function xp__dlam_ldlaplace (y, mu, lam) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, mu, lam
  real(kind=xp_) :: ans
  ans = lam**(-1._xp_) - abs(y - mu)
end function xp__dlam_ldlaplace

!> @relates dy_ldlaplace
elemental function xp__dy_ldlaplace (y, mu, lam) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, mu, lam
  real(kind=xp_) :: ans
  ans = lam * dx_abs(y - mu)
end function xp__dy_ldlaplace

!> @relates ldbeta
elemental function xp__ldbeta (y, a1, a2) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, a1, a2
  real(kind=xp_) :: ans
  ans = merge(((a1 - 1) * log(y) + (a2 - 1) * log(1 - y)) - logbeta(a1, a2), 0._xp_, &
       y > 0 .and. y < 1) 
end function xp__ldbeta

!> @relates da1_ldbeta
elemental function xp__da1_ldbeta (y, a1, a2) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, a1, a2
  real(kind=xp_) :: ans
  ans = merge(log(y) - da_logbeta(a1, a2), 0._xp_, y > 0 .and. y < 1)
end function xp__da1_ldbeta

!> @relates da2_ldbeta
elemental function xp__da2_ldbeta (y, a1, a2) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, a1, a2
  real(kind=xp_) :: ans
  ans = merge(log(1._xp_ - y) - db_logbeta(a1, a2), 0._xp_, y > 0 .and. y < 1)
end function xp__da2_ldbeta

!> @relates dy_ldbeta
elemental function xp__dy_ldbeta (y, a1, a2) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, a1, a2
  real(kind=xp_) :: ans
  ans = merge((a1 - 1._xp_) * dx_log(y) - (a2 - 1._xp_) * dx_log(1._xp_ - y), 0._xp_, y > 0 .and. y < 1)
end function xp__dy_ldbeta

!> @relates ldgamma
elemental function xp__ldgamma (y, a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, a, b
  real(kind=xp_) :: ans
  ans = merge(a * log(b) - log_gamma(a) + (a - 1._xp_) * log(y) - b * y, 0._xp_, y > 0) 
end function xp__ldgamma

!> @relates da_ldgamma
elemental function xp__da_ldgamma (y, a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, a, b
  real(kind=xp_) :: ans
  ans = merge(log(b) - psi(a) + log(y), 0._xp_, y > 0)
end function xp__da_ldgamma

!> @relates db_ldgamma
elemental function xp__db_ldgamma (y, a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, a, b
  real(kind=xp_) :: ans
  ans = merge(a * b**(-1._xp_) - y, 0._xp_, y > 0)
end function xp__db_ldgamma

!> @relates dy_ldgamma
elemental function xp__dy_ldgamma (y, a, b) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, a, b
  real(kind=xp_) :: ans
  ans = merge((a - 1) * dx_log(y) - b, 0._xp_, y > 0)
end function xp__dy_ldgamma

!> @relates ldnorm
elemental function xp__ldnorm (y, mu, s) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, mu, s
  real(kind=xp_) :: ans
  ans = -log(s) - 0.5 * log(2 * pi_xp_) - 0.5 * ((y - mu) / s)**2 
end function xp__ldnorm

!> @relates ds_ldnorm
elemental function xp__ds_ldnorm (y, mu, s) result (ans)
  implicit none
  real(kind=xp_), intent(in) :: y, mu, s
  real(kind=xp_) :: ans
  ans = -s**(-1) + s**(-3) * (y - mu)**2
end function xp__ds_ldnorm

!> @relates dmu_ldnorm
elemental function xp__dmu_ldnorm (y, mu, s) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, mu, s
  real(kind=xp_) :: ans
  ans = s**(-2) * (y - mu)
end function xp__dmu_ldnorm

!> @relates dy_ldnorm
elemental function xp__dy_ldnorm (y, mu, s) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, mu, s
  real(kind=xp_) :: ans
  ans = (mu - y) / s**2
end function xp__dy_ldnorm

!> @relates bd_ldmvnorm__1
subroutine xp__BD_LDMVNORM__1 (y, mu, E, dld, info, dmu, dE, dy)
  implicit none
  real(kind=xp_), intent(in) :: y(:),  mu(:), E(:,:), dld
  integer, intent(out) :: info
  real(kind=xp_), intent(inout), optional :: dmu(:), dE(:,:), dy(:)
  real(kind=xp_) :: x(size(y),1), dx(size(y),1), dQ(1,1)
  real(kind=xp_) :: IE(size(E,1),size(E,2)), lnD, D, dD
  info = 0
  IE = E
  x(:,1) = y - mu
  dx = 0
  dQ = -0.5_xp_
  call INVSYMMAT(IE, info, lnD)
  if (info == 0) then
     if (present(dmu) .and. present(dE)) then
        dx = 0
        call bd_quadraticsym2(IE, x, dQ, dE, dx, info=info)
        ! w.r.t. mu
        dmu = -dx(:,1) * dld
        ! w.r.t. E
        D = exp(lnD)
        dD = dx_log(D) 
        dE = dE - 0.5_xp_ * BDX_DET(IE, D, dD, .true.)
        dE = dE * dld
        ! w.r.t. y
        if (present(dy)) dy = dy + dx(:,1)
     else if (present(dmu) .and. .not. present(dE)) then
        dx = 0
        call bd_quadraticsym2(IE, x, dQ, dB=dx, info=info)
        dmu = -dx(:,1) * dld
        ! w.r.t. y
        if (present(dy)) dy = dy + dx(:,1)
     else if (present(dE) .and. .not. present(dmu)) then
        call bd_quadraticsym2(IE, x, dQ, dE, info=info)
        D = exp(lnD)
        dD = dx_log(D)
        dE = dE - 0.5_xp_ * BDX_DET(IE, D, dD, .true.)
        dE = dE * dld
        ! w.r.t. y
        if (present(dy)) dy = dy + dx(:,1)
     else
        info = err_missingArg_
     end if
  end if
end subroutine xp__BD_LDMVNORM__1

!> @relates ldmvnorm__2
pure function xp__LDMVNORM__2 (y, mu, A, lnD) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y(:), mu(:), A(:,:), lnD
  real(kind=xp_) :: ans
  real(kind=xp_) :: x(size(y)), Ay(size(y))
  x = y - mu
  call gmvmult(0, 1._xp_, A, x, 0._xp_, Ay)
  ans = -0.5 * (size(y) * log(2 * pi_xp_) - 0.5 * lnD + vvinner(x, Ay))
end function xp__LDMVNORM__2



  
