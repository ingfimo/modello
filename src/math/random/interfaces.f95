  !> @addtogroup math__random_interfaces_ 
  !! @{

  !> Interface - Uniform random numbr generation
  !!
  !! Subroutine generating random numbers according to a uniform
  !! distribution for different real precisions.
  !!
  !! @parameter[out] x sp_ or dp_ array of dimension (:)
  interface unif_rand
     !> Suborutine for single precision numbers
     module procedure sp__unif_rand
     !> Subroutine ofr double precision numbers
     module procedure dp__unif_rand
  end interface unif_rand

  interface rand_int
     module procedure sp__rand_int
     module procedure dp__rand_int
  end interface rand_int

  !> @}
