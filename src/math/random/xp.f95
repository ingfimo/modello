!> @relates unif_rand
subroutine xp__unif_rand (x)
  implicit none
  real(kind=xp_), intent(out) :: x(:)
  real(kind=xp_) :: dtyp
  integer :: i
  do i = 1, size(x)
     x(i) = r__unif_rand(dtyp)
  end do
end subroutine xp__unif_rand

subroutine xp__rand_int (x, a, b, dtyp)
  implicit none
  integer, intent(out) :: x(:)
  integer, intent(in) :: a, b
  real(kind=xp_), intent(in) :: dtyp
  real(kind=xp_) :: z(size(x))
  call unif_rand(z)
  x = nint(z) * (b - a) + a
end subroutine xp__rand_int
  
