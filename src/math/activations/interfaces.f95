  !> @addtogroup math__activations_interfaces_
  !! @{
  
  !> Interface - Sigmoid Function
  !!
  !! Elemental functions implmenting the Sigmoid for different real precisions:
  !! @f[
  !! sigmoid(x) = \frac{1}{1 - e^{-x}}
  !! @f]
  !!
  !! @param[in] x sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface sigmoid
     !> Elemental function for singular precision inputs
     module procedure sp__sigmoid
     !> Elemental function for double precision inputs
     module procedure dp__sigmoid
  end interface sigmoid

  !> Interface - Derivative of the Sigmoid Function w.r.t. x
  !!
  !! Elemental functions implementing the Sigmoid's derivative w.r.t. x:
  !! @f[
  !! \frac{\mbox{d}sigmoid}{\mbox{d}x} = \frac{e^{-x}}{(1 + e^{-x})^2}  
  !! @f]
  !!
  !! @param[in] x sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dx_sigmoid
     !> elemental function for singular precision inputs
     module procedure sp__dx_sigmoid
     !> elemental function for singular precision inputs
     module procedure dp__dx_sigmoid
  end interface dx_sigmoid

  !> Interface - Softplus Function
  !!
  !! Elemental fucntions implementing the Softplus function for different real precisions:
  !! @f[
  !! softplus(x) = log(1 + e^x)
  !! @f]
  !!
  !! @param[in] x sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface softplus
     !> Elemental function for singular precision inputs
     module procedure sp__softplus
     !> Elemental function for double precision inputs
     module procedure dp__softplus
  end interface softplus

  !> Interface - Derivative of the Softplus Function w.r.t. x
  !!
  !! Eelemental fucntions implementing the Softplus' derivative w.r.t. x:
  !! @f[
  !! \frac{\mbox{d}softplus}{\mbox{d}x} = \frac{1}{1 - e^{-x}}
  !! @f]
  !!
  !! @param[in] x sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dx_softplus
     !> Elemental function for singular precision inputs
     module procedure sp__dx_softplus
     !> Elemental function for double precision inputs
     module procedure dp__dx_softplus
  end interface dx_softplus

  !> Interface - Rectified Linear Unit Function (ReLU)
  !!
  !! Elemental fucntions implementing the ReLU function for different real precisions:
  !! @f[
  !! relu(x) = max(0, x)
  !! @f]
  !!
  !! @param[in] x sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface relu
     !> Elemental function for singular precision inputs
     module procedure sp__relu
     !> Elemental function for double precision inputs
     module procedure dp__relu
  end interface relu

  !> Interface - Derivative of the ReLU Function w.r.t. x
  !!
  !! Eelemental fucntions implementing the ReLU's derivative w.r.t. x:
  !! @f[
  !! \frac{\mbox{d}relu}{\mbox{d}x} = \{1\ \mbox{if}\ x > 0\ \mbox{else}\ 0\}
  !! @f]
  !!
  !! @param[in] x sp_ or dp_
  interface dx_relu
     !> Elemental function for singular precision inputs
     module procedure sp__dx_relu
     !> Elemental function for double precision inputs
     module procedure dp__dx_relu
  end interface dx_relu

  !> Interface - Leaky ReLU Function
  !!
  !! Elemental functions implementing the Leaky ReLU function for differnt real precisions:
  !! @f[
  !! leaky\_relu(x, a) = \{x\ \mbox{if}\ x > 0\ \mbox{else}\ a \times x\}
  !! @f]
  !!
  !! @param[in] x sp_ or dp_, scalar or array
  !! @param[in] a sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface leakyrelu
     !> Elemental function for singular precision inputs
     module procedure sp__leakyrelu
     !> Elemental function for double precision inputs
     module procedure dp__leakyrelu
  end interface leakyrelu

  !> Interface - Derivative of the Leaky ReLU Function w.r.t. x
  !!
  !! Elemental functions implemneting the Leaky ReLU's derivative w.r.t. x for differnt real precisions:
  !! @f[
  !! \frac{\mbox{d}leaky\_relu}{\mbox{d}x} = \{1\ \mbox{if}\ x > 0\ \mbox{else}\ a\}
  !! @f]
  !!
  !! @param[in] x sp_ or dp_, scalar or array
  !! @param[in] a sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dx_leakyrelu
     !> Elemental function for singular precision inputs
     module procedure sp__dx_leakyrelu
     !> Elemental function for double precision inputs
     module procedure dp__dx_leakyrelu
  end interface dx_leakyrelu

  !> Interface - Derivative of the Leaky ReLU Function w.r.t. a
  !!
  !! Elemental functions implementing the Leaky ReLU's derivative w.r.t. a for different real precisions:
  !! @f[
  !! \frac{\mbox{d}leaky\_relu}{\mbox{d}a} = \{0\ \mbox{if}\ x > 0\ \mbox{else}\ x\}
  !! @f]
  !!
  !! @param[in] x sp_ or dp_, scalar or array
  !! @param[in] a sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface da_leakyrelu
     !> Elemental function for singular precision inputs
     module procedure sp__da_leakyrelu
     !> Elemental function for double precision inputs
     module procedure dp__da_leakyrelu
  end interface da_leakyrelu

  !> Interface - Exponential Linea Unit (ELU) Function
  !!
  !! Elemental functions implementing the ELU function for different real precisions:
  !! @f[
  !! elu(x, a) = \{x\ \mbox{if}\ x > 0\ \mbox{else}\ a \times (e^x - 1)\}
  !! @f]
  !!
  !! @param[in] x sp_ or dp_, scalar or array
  !! @param[in] a sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface elu
     !> Elemental function for singular precision inputs
     module procedure sp__elu
     !> Elemental function for double precision inputs
     module procedure dp__elu
  end interface elu

  !> Interface - Derivative of the ELU Function w.r.t. x
  !!
  !! Elemental functions implementing the ELU's derivative w.r.t. x for different real precisions:
  !! @f[
  !! \frac{\mbox{d}elu}{\mbox{d}x} = \{1\ \mbox{if}\ x > 0\ \mbox{else}\ a \times e^x\}
  !! @f]
  !!
  !! @param[in] x sp_ or dp_, scalar or array
  !! @param[in] a sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dx_elu
     !> Elemental function for singular precision inputs
     module procedure sp__dx_elu
     !> Elemental function for double precision inputs
     module procedure dp__dx_elu
  end interface dx_elu

  !> Interface - Derivative of the ELU Function w.r.t. a
  !!
  !! Elemental functions implementing the ELU's derivative w.r.t. a for different real precisions:
  !! @f[
  !! \frac{\mbox{d}elu}{\mbox{d}a} = \{0\ \mbox{if}\ x > 0\ \mbox{else}\ e^x - 1\}
  !! @f]
  !!
  !! @param[in] x sp_ or dp_, scalar or array
  !! @param[in] a sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface da_elu
     !> Elemental function for singular precision inputs
     module procedure sp__da_elu
     !> Elemental function for double precision inputs
     module procedure dp__da_elu
  end interface da_elu

  !> Interface - Sigmoid Linea Unit (SiLU) Function
  !!
  !! Elemental functions implementing the ELU function for different real precisions:
  !! @f[
  !! silu(x) = x * sigmoid(x)
  !! @f]
  !! 
  !! @param[in] x sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface silu
     !> Elemental function for singular precision inputs
     module procedure sp__silu
     !> Elemental function for double precision inputs
     module procedure dp__silu
  end interface silu

  !> Interface - Derivative of the SiLU Function w.r.t. x
  !!
  !! Elemental functions implementing the SiLU's derivative w.r.t. x for different real precisions:
  !! @f[
  !! \frac{\mbox{d}silu}{\mbox{d}x} = sigmoid(x) + xe^{-x} \times sigmoid(x)^2
  !! @f]
  !!
  !! @param[in] x sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dx_silu
     !> Elemental function for singular precision inputs
     module procedure sp__dx_silu
     !> Elemental function for double precision inputs
     module procedure dp__dx_silu
  end interface dx_silu

  !> Interface - Swish Function
  !!
  !! Elemental functions implementing the Swish function for different real precisions:
  !! @f[
  !! swish(x, a) = x \times sigmoid(ax)
  !! @f]
  !!
  !! @param[in] x sp_ or dp_, scalar or array
  !! @param[in] a sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface swish
     !> Elemental function for singular precision inputs
     module procedure sp__swish
     !> Elemental function for double precision inputs
     module procedure dp__swish
  end interface swish

  !> Interface - Derivative of the Swish Function w.r.t. x
  !!
  !! Elemental functions implementing the SiLU's derivative w.r.t. x for different real precisions:
  !! @f[
  !! \frac{\mbox{d}swish}{\mbox{x}} = sigmoid(ax) + ax \times e^{-ax} \times sigmoid(ax)^2
  !! @f]
  !! @param[in] x sp_ or dp_, scalar or array
  !! @paarm[in] a sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dx_swish
     !> Elemental function for singular precision inputs
     module procedure sp__dx_swish
     !> Elemental function for double precision inputs
     module procedure dp__dx_swish
  end interface dx_swish

  !> Derivative of the Swish Function w.r.t. a
  !! @param[in] x sp_ or dp_, scalar or array
  !! @param[in] a sp_ or dp_, scalar or array
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface da_swish
     !> Elemental function for singular precision inputs
     module procedure sp__da_swish
     !> Elemental function for double precision inputs
     module procedure dp__da_swish
  end interface da_swish

  !> Interface - Softmax Function
  !!
  !! Functions implementing the Softmax function for different real precisions:
  !! @f[
  !! softmax(\mathbf{x}) = \left[\frac{e^{x_i}}{\sum_j e^{x_j}}\right]
  !! @f]
  !!
  !! @param[in] x sp_ or dp_, array(:)
  !! @return An array of the same type and dimension of x
  interface SOFTMAX
     !> Elemental function for singular precision inputs
     module procedure sp__SOFTMAX
     !> Elemental function for double precision inputs
     module procedure dp__SOFTMAX
  end interface SOFTMAX

  !> Interface - Derivative of the Softmax Function w.r.t. x
  !!
  !! Functions implementing the calculation of the Jacobian of the Sofmax function
  !! for different real rpecisions:
  !! @f[
  !! \mathbf{J}_{i,j} = softmax(\mathbf{x})_i \times \delta_{i,j} \cdot softmax(\mathbf{x})_j
  !! @f]
  !! where @f$\delta_{i,j}@f$ is the Krnoneker Delta.
  !!
  !! @param[in] x sp_ or dp_, array(:)
  !! @param[in] s sp_ or dp_, array(:), softmax of x
  !! @return An array of the same type of x and dimension (size(x),size(x))
  interface DX_SOFTMAX
     !> Function for singular precision inputs
     module procedure sp__DX_SOFTMAX
     !> Function for double precision inputs
     module procedure dp__DX_SOFTMAX
  end interface DX_SOFTMAX

  interface DX_LOG_SOFTMAX
     module procedure sp__DX_LOG_SOFTMAX
     module procedure dp__DX_LOG_SOFTMAX
  end interface DX_LOG_SOFTMAX

  !> @}

 
