!> @relates sigmoid
elemental function xp__sigmoid (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = 1 / (1 + exp(-x))
end function xp__sigmoid

!> @relates dx_sigmoid
elemental function xp__dx_sigmoid (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = exp(-x) / (1 + exp(-x))**2
end function xp__dx_sigmoid

!> @relates softplus
elemental function xp__softplus (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = log(1 + exp(x))
end function xp__softplus

!> @relates dx_softplus
elemental function xp__dx_softplus (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = sigmoid(x)
end function xp__dx_softplus

!> @relates relu
elemental function xp__relu (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = max(0._xp_, x)
end function xp__relu

!> @relates dx_relu
elemental function xp__dx_relu (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = merge(1._xp_, 0._xp_, x > 0)
end function xp__dx_relu

!> @relates leakyrelu
elemental function xp__leakyrelu (x, a) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x, a
  real(kind=xp_) :: ans
  ans = merge(x, a*x, x > 0)
end function xp__leakyrelu

!> @relates dx_leakyrelu
elemental function xp__dx_leakyrelu (x, a) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x, a
  real(kind=xp_) :: ans
  ans = merge(1._xp_, a, x > 0)
end function xp__dx_leakyrelu

!> @relates da_leakyrelu
elemental function xp__da_leakyrelu (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = merge(0._xp_, x, x > 0)
end function xp__da_leakyrelu

!> @relates elu
elemental function xp__elu (x, a) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x, a
  real(kind=xp_) :: ans
  ans = merge(x, a * (exp(x) - 1), x > 0) 
end function xp__elu

!> @relates dx_elu
elemental function xp__dx_elu (x, a) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x, a
  real(kind=xp_) :: ans
  ans = merge(1._xp_, a * exp(x), x > 0)
end function xp__dx_elu

!> @relates da_elu
elemental function xp__da_elu (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = merge(0._xp_, exp(x) - 1, x > 0)
end function xp__da_elu

!> @relates silu
elemental function xp__silu (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = x * sigmoid(x)
end function xp__silu

!> @relates dx_silu
elemental function xp__dx_silu (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x
  real(kind=xp_) :: ans
  ans = 1 / (1+exp(-x)) + x*exp(-x) / (1+exp(-x))**2
end function xp__dx_silu

!> @relates swish
elemental function xp__swish (x, a) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x, a
  real(kind=xp_) :: ans
  ans = x  * sigmoid(a * x)
end function xp__swish

!> @relates dx_swish
elemental function xp__dx_swish (x, a) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x, a
  real(kind=xp_) :: ans
  ans = 1 / (1+exp(-a*x)) + a*x*exp(-a*x) / (1+exp(-a*x))**2
end function xp__dx_swish

!> @relates da_swish
elemental function xp__da_swish (x, a) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x, a
  real(kind=xp_) :: ans
  ans = x**2 * exp(-a*x) / (1 + exp(-a*x))**2
end function xp__da_swish

!> @relates softmax
function xp__SOFTMAX (x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: x(:)
  real(kind=xp_) :: ans(size(x)), s, m
  !$omp parallel workshare
  m = maxval(x)
  ans = exp(x - m + tol_xp_)
  s = sum(ans)
  ans = ans / s
  !$omp end parallel workshare
end function xp__SOFTMAX

!> @relates dx_softmax
function xp__DX_SOFTMAX (x, s) result(ans) 
  implicit none
  real(kind=xp_), intent(in) :: x(:), s(:)
  real(kind=xp_) :: ans(size(x),size(x))
  real(kind=xp_) :: dtyp
  integer :: i, j, n
  n = size(x)
  !omp parallel do
  do j = 1, n
     do i = 1, n
        ans(i, j) = s(i) * (deltaij(i, j, dtyp) - s(j))
     end do
  end do
  !$end omp parallel do
end function xp__DX_SOFTMAX

function xp__DX_LOG_SOFTMAX (s) result(ans) 
  implicit none
  real(kind=xp_), intent(in) :: s(:)
  real(kind=xp_) :: ans(size(s),size(s))
  real(kind=xp_) :: dtyp
  integer :: i, j, n
  n = size(s)
  !omp parallel do
  do j = 1, n
     do i = 1, n
        ans(i, j) = 1 * (deltaij(i, j, dtyp) - s(j))
     end do
  end do
  !$end omp parallel do
end function xp__DX_LOG_SOFTMAX


