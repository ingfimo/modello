!> @relates binentropy
elemental function xp__binentropy (y, x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, x
  real(kind=xp_) :: ans
  ans = - y * log(x) - (1 - y) * log(1 - x)
end function xp__binentropy

!> @relates dx_binentropy
elemental function xp__dx_binentropy (y, x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, x
  real(kind=xp_) :: ans
  ans = -y / x + (1 - y) / (1 - x)
end function xp__dx_binentropy

!> @relates logit_binentropy
elemental function xp__logit_binentropy (y, x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, x
  real(kind=xp_) :: ans
  ans = softplus(x) - y * x
end function xp__logit_binentropy

!> @relates dx_logit_binentropy
elemental function xp__dx_logit_binentropy (y, x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, x
  real(kind=xp_) :: ans
  ans = sigmoid(x) - y
end function xp__dx_logit_binentropy

!> @relates crossentropy
elemental function xp__crossentropy (y, x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, x
  real(kind=xp_) :: ans
  ans = -y * log(x)
end function xp__crossentropy

!> @relates dx_crossentropy
elemental function xp__dx_crossentropy (y, x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y, x
  real(kind=xp_) :: ans
  ans = -y / x
end function xp__dx_crossentropy

!> @relates logit_crossentropy
function xp__LOGIT_CROSSENTROPY (y, x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y(:), x(:)
  real(kind=xp_) :: s, ans(size(y))
  !$omp parallel workshare
  ans = x - maxval(x) + tol_xp_
  s = log(sum(exp(ans)))
  ans = -y * (ans - s)
  !$omp end parallel workshare
end function xp__LOGIT_CROSSENTROPY

!> @relates dx_logit_crossentropy
function xp__DX_LOGIT_CROSSENTROPY (y, x) result(ans)
  implicit none
  real(kind=xp_), intent(in) :: y(:), x(:)
  real(kind=xp_) :: ans(size(y))
  ans = SOFTMAX(x)
  call gmvmult(1, -1._xp_, DX_LOG_SOFTMAX(ans), y, 0._xp_, ans) 
end function xp__DX_LOGIT_CROSSENTROPY


