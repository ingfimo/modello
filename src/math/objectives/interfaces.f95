  !> @addtogroup math__objectives_interfaces_ 
  !! @{

  !> Interface - Binary Entropy
  !!
  !! Elemental functuons implemating the
  !! negative Bernoulli log-lokelihood:
  !! @f[
  !! binaryentropy_i = -y_i \times log(x_i) - (1 - y_i) \times log(1 - x_i)
  !! @f]
  !!
  !! @param[in] y sp_ or dp_, scalar or array, binary variable
  !! @param[in] x sp_ or dp_ scalar or array, probability
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface binentropy
     !> Elemental function for single precision numbers
     module procedure sp__binentropy
     !> Elemental function for double precision numbers
     module procedure dp__binentropy
  end interface binentropy

  !> Interface - Binary Entropy Derivative w.r.t. x
  !!
  !! Elemental functuons implemating the
  !! derivative of the negative Bernoulli log-lokelihood
  !! w.r.t. x:
  !! @f[
  !! \frac{\mbox{d}binentropy_i}{\mbox{d}x_i} = -\frac{y}{x} - \frac{1 - y}{1 - x}
  !! @f]
  !!
  !! @param[in] y sp_ or dp_, scalar or array, binary variable
  !! @param[in] x sp_ or dp_ scalar or array, probability
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dx_binentropy
     !> Elemental function for single precision numbers
     module procedure sp__dx_binentropy
     !> Elemental function for double precision numbers
     module procedure dp__dx_binentropy
  end interface dx_binentropy

  !> Interface - Binary Entropy with Logit Input
  !!
  !! Elemental functuons implemating the calculation of the 
  !! derivative of negative Bernoulli log-lokelihood
  !! starting from probability expressed as logit units
  !! (i.e. the input of the sigmoid function):
  !! @f[
  !! logit\_binentropy_i = softplus(x_i) - y_i \times x_i
  !! @f]
  !!
  !! @param[in] y sp_ or dp_, scalar or array, binary variable
  !! @param[in] x sp_ or dp_ scalar or array, probability
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface logit_binentropy
     !> Elemental function for single precision numbers
     module procedure sp__logit_binentropy
     !> Elemental function for double precision numbers
     module procedure dp__logit_binentropy
  end interface logit_binentropy

  !> Interface - Derivtive w.r.t. x of the Binary Entropy with Logit Unit Input
  !!
  !! Elemental functuons implemating the calcualtion of the
  !! derivative of the negative Bernoulli log-lokelihood
  !! with probability expressed as logit units
  !! @f[
  !! \frac{\mbox{d}logit\_binentropy_i}{\mbox{d}x_i} = sigmoid(x_i) - y_i
  !! @f]
  !!
  !! @param[in] y sp_ or dp_, scalar or array, binary variable
  !! @param[in] x sp_ or dp_ scalar or array, probability
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dx_logit_binentropy
     !> Elemental function for single precision numbers
     module procedure sp__dx_logit_binentropy
     !> Elemental function for double precision numbers
     module procedure dp__dx_logit_binentropy
  end interface dx_logit_binentropy

  !> Interface - Cross-entropy
  !!
  !! Elemental functuons implemating the negative Multinomial
  !! log-lokelihood:
  !! @f[
  !! crossentropy_i = -y_i \times log(x_i)
  !! @f]
  !!
  !! @param[in] y sp_ or dp_, scalar or array, binary variable
  !! @param[in] x sp_ or dp_ scalar or array, probability
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface crossentropy
     !> Elemental function for single precision numbers
     module procedure sp__crossentropy
     !> Elemental function for double precision numbers
     module procedure dp__crossentropy
  end interface crossentropy

  !> Interface - Cross-entropy Derivative w.r.t. x
  !!
  !! Elemental functuons implemating the derivative of the+
  !! negative Multinomial log-lokelihood w.r.t. x:
  !! @f[
  !! \frac{\mbox{d}crossentropy_i}{\mbox{d}x_i} = -y_i \times log(x_i)
  !! @f]
  !!
  !! @param[in] y sp_ or dp_, scalar or array, binary variable
  !! @param[in] x sp_ or dp_ scalar or array, probability
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface dx_crossentropy
     !> Elemental function for single precision numbers
     module procedure sp__dx_crossentropy
     !> Elemental function for double precision numbers
     module procedure dp__dx_crossentropy
  end interface dx_crossentropy

  !> Interface - Cross-entropy with Logit Input
  !!
  !! Elemental functuons implemating the negative Multinomial
  !! log-lokelihood with probability expressed as logit units
  !! (i.e. the input of the sofmax function)
  !!
  !! @param[in] y sp_ or dp_, scalar or array, binary variable
  !! @param[in] x sp_ or dp_ scalar or array, probability
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface LOGIT_CROSSENTROPY
     !> Elemental function for single precision numbers
     module procedure sp__LOGIT_CROSSENTROPY
     !> Elemental function for double precision numbers
     module procedure dp__LOGIT_CROSSENTROPY
  end interface LOGIT_CROSSENTROPY

  !> Interface - Derivative w.r.t.x of the Cross-entropy with Logit Input
  !!
  !! Elemental functuons implemating the derivative w.r.t. x of the
  !! negative Multinomial log-lokelihood with logit input
  !!
  !! @param[in] y sp_ or dp_, scalar or array, binary variable
  !! @param[in] x sp_ or dp_ scalar or array, probability
  !! @return An array or scalar of the same type and dimension of the given inputs
  interface DX_LOGIT_CROSSENTROPY
     !> Elemental function for single precision numbers
     module procedure sp__DX_LOGIT_CROSSENTROPY
     !> Elemental function for double precision numbers
     module procedure dp__DX_LOGIT_CROSSENTROPY
  end interface DX_LOGIT_CROSSENTROPY

  !> @}
