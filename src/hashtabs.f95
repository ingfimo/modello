module hashtabs

  use types
  use errwarn
  use hashtabs_utils
  use registers
  use registers_utils

  character(len=*), parameter :: mod_hashtabs_name_ = "hashtabs"

contains

  subroutine hashtab__append (x, m, n)
    implicit none
    type(hashtab), intent(out), pointer :: x
    integer, intent(in) :: m, n
    call do_safe_within("hashtab__append", mod_hashtabs_name_, private_append)
  contains
    subroutine private_append
      integer :: i
      i = next_hashtab()
      call hashtab__allocate(HTS_(i), m, n)
      if (err_free()) then
         x => HTS_(i)
         x%id = i
      end if
    end subroutine private_append
  end subroutine hashtab__append

  subroutine hashtab__pop (x)
    implicit none
    type(hashtab), intent(inout), pointer :: x
    call do_safe_within("hashtab__pop", mod_hashtabs_name_, private_pop)
  contains
    subroutine private_pop
      integer :: i
      i = x%id
      nullify(x)
      call hashtab__deallocate(HTS_(i))
      if (err_free()) HTSINDEX_ = [HTSINDEX_, i]
    end subroutine private_pop
  end subroutine hashtab__pop

end module hashtabs
  
