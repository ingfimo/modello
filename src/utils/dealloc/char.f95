!> Deallocates character allocatables of rank 0
!! @param[inout] x character, allocatable to deallocate
!! @param[in] xname character, variable name (for error reporting)
subroutine char__dealloc__1 (x, xname)
  implicit none
  character(len=:), intent(inout), allocatable :: x
  character(len=*), intent(in) :: xname
  call do_safe_within('char__dealloc__1', mod_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    integer :: info
    call assert(allocated(x), err_notAlloc_, xname)
    if (err_free()) then
       deallocate(x, stat=info)
       call assert(info == 0, err_dealloc_, xname)
    end if
  end subroutine private_deallocate
end subroutine char__dealloc__1
