!> Deallocates integer scalar pointers
!! @param[inout] x integer, pointer to deallocate
!! @param[in] xname character, variable name (for error reporting)
subroutine int__dealloc__1 (x, xname)
  implicit none
  integer, intent(inout), pointer :: x
  character(len=*), intent(in) :: xname
  call do_safe_within('int__dealloc__1', mod_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    integer :: info
    call assert(associated(x), err_notAlloc_, xname)
    if (err_free()) then
       deallocate(x, stat=info)
       call assert(info == 0, err_dealloc_, xname)
    end if
  end subroutine private_deallocate
end subroutine int__dealloc__1

!> Deallocates integer pointers of rank 1
!! @param[inout] x integer(:), pointer to deallocate
!! @param[in] xname character, variable name (for error reporting)
subroutine int__dealloc__2 (x, xname)
  implicit none
  integer, intent(inout), pointer :: x(:)
  character(len=*), intent(in) :: xname
  call do_safe_within('int__dealloc__2', mod_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    integer :: info
    call assert(associated(x), err_notAlloc_, xname)
    if (err_free()) then
       deallocate(x, stat=info)
       call assert(info == 0, err_dealloc_, xname)
    end if
  end subroutine private_deallocate
end subroutine int__dealloc__2


!> Int__Deallocates integer allocatables of rank 0
!! @param[inout] x integer, allocatable to deallocate
!! @param[in] xname character, variable name (for error reporting)
subroutine int__dealloc__3 (x, xname)
  implicit none
  integer, intent(inout), allocatable :: x
  character(len=*), intent(in) :: xname
  call do_safe_within('int__dealloc__3', mod_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    integer :: info
    call assert(allocated(x), err_notAlloc_, xname)
    if (err_free()) then
       deallocate(x, stat=info)
       call assert(info == 0, err_dealloc_, xname)
    end if
  end subroutine private_deallocate
end subroutine int__dealloc__3

!> Deallocates integer allocatables of rank 1
!! @param[inout] x integer(:,:), allocatable to deallocate
!! @param[in] xname character, variable name (for error reporting)
subroutine int__dealloc__4 (x, xname)
  implicit none
  integer, intent(inout), allocatable :: x(:)
  character(len=*), intent(in) :: xname
  call do_safe_within('int__dealloc__4', mod_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    integer :: info
    call assert(allocated(x), err_notAlloc_, xname)
    if (err_free()) then
       deallocate(x, stat=info)
       call assert(info == 0, err_dealloc_, xname)
    end if
  end subroutine private_deallocate
end subroutine int__dealloc__4

subroutine int__dealloc__5 (x, xname)
  implicit none
  integer, intent(inout), allocatable :: x(:,:)
  character(len=*), intent(in) :: xname
  call do_safe_within('int__dealloc__5', mod_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    integer :: info
    call assert(allocated(x), err_notAlloc_, xname)
    if (err_free()) then
       deallocate(x, stat=info)
       call assert(info == 0, err_dealloc_, xname)
    end if
  end subroutine private_deallocate
end subroutine int__dealloc__5



