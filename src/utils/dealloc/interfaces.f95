  !> Deallocate real and integer allocatable and pointers.
  !! @author Filippo Monari
  !! @param x entity to deallocate
  !! @param xname character identifying x (for error reporting purposes)
  interface dealloc
     ! Integer
     module procedure int__dealloc__1
     module procedure int__dealloc__2
     module procedure int__dealloc__3
     module procedure int__dealloc__4
     module procedure int__dealloc__5
     ! ---
     ! Single precision
     module procedure sp__dealloc__1
     module procedure sp__dealloc__2
     module procedure sp__dealloc__3
     ! ---
     ! Double precision
     module procedure dp__dealloc__1
     module procedure dp__dealloc__2
     module procedure dp__dealloc__3
     ! ---
     ! Char
     module procedure char__dealloc__1
  end interface dealloc

  !> Given the initialisation value of a pointer variable,
  !! takes the correct action to deallocate it.
  !! @author Filippo Monari
  !! @param[inout] x variable to deallocate
  !! @param[in] init, integer, intialosation value
  !! @param[in] xname character, variable name (for error reporting purposes)
  interface dealloc_element
     ! Integer
     module procedure int__dealloc_element__1
     module procedure int__dealloc_element__2
     ! ---
     ! Single precision
     module procedure sp__dealloc_element__1
     module procedure sp__dealloc_element__2
     ! ---
     ! Double precision
     module procedure dp__dealloc_element__1
     module procedure dp__dealloc_element__2
  end interface dealloc_element
  
