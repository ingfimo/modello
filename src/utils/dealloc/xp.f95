!> @defgroup utils__dealloc_ Utility Procedures for Deallocating Arrays and Pointers
!! @author Filippo Monari
!! @{


!> Deallocates double precision pointers of rank 0
!! @param[inout] x double precision, pointer to xp__deallocate
!! @param[in] xname character, variable name (for error reporting)
subroutine xp__dealloc__1 (x, xname)
  implicit none
  real(kind=xp_), intent(inout), pointer :: x
  character(len=*), intent(in) :: xname
  call do_safe_within('xp__dealloc__1', mod_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    integer :: info
    call assert(associated(x), err_notAlloc_, xname)
    if (err_free()) then
       deallocate(x, stat=info)
       call assert(info == 0, err_dealloc_, xname)
    end if
  end subroutine private_deallocate
end subroutine xp__dealloc__1

!> Deallocates double precision pointers of rank 1
!! @param[inout] x double precision(:), pointer to deallocate
!! @param[in] xname character, variable name (for error reporting)
subroutine xp__dealloc__2 (x, xname)
  implicit none
  real(kind=xp_), intent(inout), pointer :: x(:)
  character(len=*), intent(in) :: xname
  call do_safe_within('xp__dealloc__2', mod_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    integer :: info
    call assert(associated(x), err_notAlloc_, xname)
    if (err_free()) then
       deallocate(x, stat=info)
       call assert(info == 0, err_dealloc_, xname)
    end if
  end subroutine private_deallocate
end subroutine xp__dealloc__2


!> Deallocates double precision allocatables of rank 1
!! @param[inout] x double precision(:), allocatable to deallocate
!! @param[in] xname character, variable name (for error reporting)
subroutine xp__dealloc__3 (x, xname)
  implicit none
  real(kind=xp_), intent(inout), allocatable :: x(:)
  character(len=*), intent(in) :: xname
  call do_safe_within('xp__dealloc__3', mod_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    integer :: info
    call assert(allocated(x), err_notAlloc_, xname)
    if (err_free()) then
       deallocate(x, stat=info)
       call assert(info == 0, err_dealloc_, xname)
    end if
  end subroutine private_deallocate
end subroutine xp__dealloc__3


!> @}
