!> Allocates a scalar @int @pointer, and initialises it to the given value
!! @param[inout] x @int, @pointer
!! @param[in] xname @char, name of the variable (for error reporting)
!! @param[in] v @int, @optional if given the pointer is initialises to this values, otherwise to 0
subroutine int__alloc__1 (x, xname, v)
  implicit none
  integer, intent(inout), pointer :: x
  integer, optional :: v
  character(len=*), intent(in) :: xname
  call do_safe_within('int__alloc__1', mod_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: w, info
    call assert(.not. associated(x), err_alreadyAlloc_, xname)
    if (err_free()) then
       w = 0
       if (present(v)) w = v
       allocate(x, source=w, stat=info)
       call assert(info == 0, err_alloc_, xname)
    end if
  end subroutine private_allocate
end subroutine int__alloc__1

!> Allocates an @int @vector @pointer with the given size
!! @param[inout] x @int, @pointer, @vector
!! @param[in] n @int,
!! @param[in] xname @char, name of the variable (for error reporting) 
subroutine  int__alloc__2 (x, n, xname)
  implicit none
  integer, intent(inout), pointer :: x(:)
  integer, intent(in) :: n
  character(len=*), intent(in) :: xname
  call do_safe_within('int__alloc__2', mod_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: info
    call assert(.not. associated(x), err_alreadyAlloc_, xname)
    if (err_free()) then
       allocate(x(n), source=0, stat = info)
       call assert(info == 0, err_alloc_, xname)
    end if
  end subroutine private_allocate
end subroutine int__alloc__2

!> Allocates an @int @vector @pointer, and initialises it ot given source
!! @param[inout] x @int, @pointer, @vector
!! @param[in] xname @char, name of the variable (for error reporting)
!! @param[in] v @int, @vector, source values 
subroutine  int__alloc__3 (x, v, xname) 
  implicit none
  integer, intent(inout), pointer :: x(:)
  integer, intent(in) :: v(:)
  character(len=*), intent(in) :: xname
  call do_safe_within('int__alloc__3', mod_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: info
    call assert(.not. associated(x), err_alreadyAlloc_, xname)
    if (err_free()) then
       allocate(x, source=v, stat = info)
       call assert(info == 0, err_alloc_, xname)
    end if
  end subroutine private_allocate
end subroutine int__alloc__3

!> Allocates an @int @vector @allocatable with the given size
!! @param[inout] x @int, @allocatable, @vector
!! @param[in] n @int, size 
!! @param[in] xname @char, name of the variable (for error reporting)
subroutine  int__alloc__4 (x, n, xname)
  implicit none
  integer, intent(inout), allocatable :: x(:)
  integer, intent(in) :: n
  character(len=*), intent(in) :: xname
  call do_safe_within('int__alloc__4', mod_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: info
    call assert(.not. allocated(x), err_alreadyAlloc_, xname)
    if (err_free()) then
       allocate(x(n), source=0, stat=info)
       call assert(info == 0, err_alloc_, xname)
    end if
  end subroutine private_allocate
end subroutine int__alloc__4

!> Allocates an @int @vector @allocatable, and intialises it to the given source
!! @param[inout] x @int, @allocatable, @vector
!! @param[in] xname @char, name of the variable (for error reporting)
!! @param[in] v @int, @vector, source values 
subroutine int__alloc__5 (x, v, xname) 
  implicit none
  integer, intent(inout), allocatable :: x(:)
  integer, intent(in) :: v(:)
  character(len=*), intent(in) :: xname
  call do_safe_within('int__alloc__5', mod_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: info
    call assert(.not. allocated(x), err_alreadyAlloc_, xname)
    if (err_free()) then
       allocate(x, source=v, stat=info)
       call assert(info == 0, err_alloc_, xname)
    end if
  end subroutine private_allocate
end subroutine int__alloc__5

subroutine int__alloc__6 (x, m, n, xname)
  implicit none
  integer, intent(inout), allocatable :: x(:,:)
  integer, intent(in) :: m, n
  character(len=*), intent(in) :: xname
  call do_safe_within("int_alloc__6", mod_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: info
    call assert(.not. allocated(x), err_alreadyAlloc_, xname)
    if (err_free()) then
       allocate(x(m,n), source=0, stat=info)
       call assert(info == 0, err_alloc_, xname)
    end if
  end subroutine private_allocate
end subroutine int__alloc__6





