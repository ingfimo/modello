!> Allocates a @allocatalbe @char, and initialises it the given source.
!! @param[inout] x @char, @allocatable
!! @param[in] xname @char, name of the variable (for error reporting)
!! @param[in] v @char, value to assing 
subroutine char__alloc__1 (x, v, xname)
  implicit none
  character(len=:), intent(inout), allocatable :: x
  character(len=*), intent(in) :: v
  character(len=*), intent(in) :: xname
  call do_safe_within('char__alloc__1', mod_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: info
    call assert(.not. allocated(x), err_alreadyAlloc_, xname)
    if (err_free()) then
       allocate(x, source=v, stat=info)
       call assert(info == 0, err_alloc_, xname)
    end if
  end subroutine private_allocate
end subroutine char__alloc__1










