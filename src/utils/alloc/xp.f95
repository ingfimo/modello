!> Allocates a @xp_ scalar @pointer
!! @param[inout] x @xp_, @pointer 
!! @param[in] xname @char, name of the variable (for error reporting)
!! @param[in] v @xp_, @optional, if present the pointer is initialised with this values, otherwise to 0 
subroutine xp__alloc__1 (x, xname, v)
  implicit none
  real(kind=xp_), intent(inout), pointer :: x
  real(kind=xp_), optional :: v
  character(len=*), intent(in) :: xname
  call do_safe_within('xp__alloc__1', mod_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: info
    real(kind=xp_) :: w
    call assert(.not. associated(x), err_alreadyAlloc_, xname)
    if (err_free()) then
       w = 0
       if (present(v)) w = v
       allocate(x, source=w, stat = info)
       call assert(info == 0, err_alloc_, xname)
    end if
  end subroutine private_allocate
end subroutine xp__alloc__1

!> Allocates an @xp_ @vector @pointer with the given size
!! @param[inout] x @xp_, @pointer, @vector
!! @param[in] n @int, size
!! @param[in] xname @char, name of the variable (for error reporting) 
subroutine xp__alloc__2 (x, n, xname)
  implicit none
  real(kind=xp_), intent(inout), pointer :: x(:)
  integer, intent(in) :: n
  character(len=*), intent(in) :: xname
  call do_safe_within('xp__alloc__2', mod_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: info
    call assert(.not. associated(x), err_alreadyAlloc_, xname)
    if (err_free()) then
       allocate(x(n), source=0._xp_, stat = info)
       call assert(info == 0, err_alloc_, xname)
    end if
  end subroutine private_allocate
end subroutine xp__alloc__2

!> Allocates an @xp_ @vector @pointer, and initialises it to the given source.
!! @param[inout] x @xp_, @pointer, @vector
!! @param[in] xname @char, name of the variable (for error reporting)
!! @param[in] v @xp_, @vector, source values 
subroutine xp__alloc__3 (x, v, xname) 
  implicit none
  real(kind=xp_), intent(inout), pointer :: x(:)
  real(kind=xp_), intent(in) :: v(:)
  character(len=*), intent(in) :: xname
  call do_safe_within('xp__alloc__3', mod_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: info
    call assert(.not. associated(x), err_alreadyAlloc_, xname)
    if (err_free()) then
       allocate(x, source=v, stat = info)
       call assert(info == 0, err_alloc_, xname)
    end if
  end subroutine private_allocate
end subroutine xp__alloc__3

!> Allocates an @xp_ @vector @allocatalbe with the given size
!! @param[inout] x @xp_ @allocatable, @vector
!! @param[in] n @int, size
!! @param[in] xname @char, name of the variable (for error reporting)
subroutine xp__alloc__5 (x, n, xname)
  implicit none
  real(kind=xp_), intent(inout), allocatable :: x(:)
  integer, intent(in) :: n
  character(len=*), intent(in) :: xname
  call do_safe_within('xp__alloc__5', mod_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: info
    call assert(.not. allocated(x), err_alreadyAlloc_, xname)
    if (err_free()) then
       allocate(x(n), source=0._xp_, stat = info)
       call assert(info == 0, err_alloc_, xname)
    end if
  end subroutine private_allocate
end subroutine xp__alloc__5

!> Allocates an @xp_ @vector @allocatable, and initialises it to the given source
!! @param[inout] x @xp_, @vector, @allocatable
!! @param[in] xname @char, name of the variable (for error reporting)
!! @param[in] v @xp_, @vector, source 
subroutine xp__alloc__6 (x, v, xname) 
  implicit none
  real(kind=xp_), intent(inout), allocatable :: x(:)
  real(kind=xp_), intent(in) :: v(:)
  character(len=*), intent(in) :: xname
  call do_safe_within('xp__alloc__6', mod_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: info
    call assert(.not. allocated(x), err_alreadyAlloc_, xname)
    if (err_free()) then
       allocate(x, source=v, stat = info)
       call assert(info == 0, err_alloc_, xname)
    end if
  end subroutine private_allocate
end subroutine xp__alloc__6

!> Allocates an @xp_ @matrix @allocatalbe with the given shape 
!! @param[inout] x @xp_, @matrix, @allocatable
!! @param[in] m @int, first dimension (rows)
!! @param[in] n @int, second dimension (columns)
!! @param[in] xname @char, name of the variable (for error reporting)
subroutine xp__alloc__7 (x, m, n, xname)
  implicit none
  real(kind=xp_), intent(inout), allocatable :: x(:,:)
  integer, intent(in) :: m, n
  character(len=*), intent(in) :: xname
  call do_safe_within('xp__alloc__7', mod_utils_name_, private_allocate)
contains
  subroutine private_allocate
    integer :: info
    call assert(.not. allocated(x), err_alreadyAlloc_, xname)
    if (err_free()) then
       allocate(x(m,n), source=0._xp_, stat = info)
       call assert(info == 0, err_alloc_, xname)
    end if
  end subroutine private_allocate
end subroutine xp__alloc__7
