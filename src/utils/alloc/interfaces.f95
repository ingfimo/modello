  !> Allocate real and integer allocatable ans pointers.
  !! @author Filippo Monari
  !! @param x entity to allocate
  !! @param v source entity
  !! @param m,n,... dimensions (only for entity with dimensions if v is not given)
  !! @param xname character identifying x (for error reporting purposes)
  interface alloc
     ! Integer
     module procedure int__alloc__1
     module procedure int__alloc__2
     module procedure int__alloc__3
     module procedure int__alloc__4
     module procedure int__alloc__5
     module procedure int__alloc__6
     !---
     ! Single precision
     module procedure sp__alloc__1
     module procedure sp__alloc__2
     module procedure sp__alloc__3
     ! module procedure sp__alloc__4
     module procedure sp__alloc__5
     module procedure sp__alloc__6
     module procedure sp__alloc__7
     !---
     ! Double precision
     module procedure dp__alloc__1
     module procedure dp__alloc__2
     module procedure dp__alloc__3
     ! module procedure dp__alloc__4
     module procedure dp__alloc__5
     module procedure dp__alloc__6
     module procedure dp__alloc__7
     !---
     ! Character
     module procedure char__alloc__1
  end interface alloc
