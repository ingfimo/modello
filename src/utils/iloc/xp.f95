!> Finds the index of the maximum value in a xp_ vector
!! @param[in] arr @xp_ @vector
!! @return @int the index of the maximum element
function xp__imaxloc__1 (arr) result(ans)
  implicit none
  real(kind=dp_), intent (in) :: arr(:)
  integer :: ans
  integer :: imax(1)
  imax = maxloc(arr(:))
  ans = imax(1)
end function xp__imaxloc__1

!> Finds the index of the minimum value in a xp_ vector
!! @param[in] arr @xp_ @vector
!! @return @int the index of the minimum element
function xp__iminloc__2 (arr) result(ans)
  implicit none
  real(kind=dp_), intent (in) :: arr(:)
  integer :: ans
  integer :: imin(1)
  imin = minloc(arr(:))
  ans = imin(1)c
end function xp__iminloc__2

