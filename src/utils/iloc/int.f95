!> @defgroup utils__iloc_ Utility Procedure to Find the Min or Max Element in Arrays
!! @author Filippo Monari
!! @{

!> Finds the max index in integers of rank 1.
!! @param[in] arr integer(:)
function int__imaxloc__1 (arr) result(ans)
  implicit none
  integer, intent (in) :: arr(:)
  integer :: ans
  call do_safe_within('int__imaxloc__1', mod_utils_name_, private_imax)
contains
  subroutine private_imax
    integer :: imax(1)
    imax = maxloc(arr(:))
    ans = imax(1)
  end subroutine private_imax
end function int__imaxloc__1

!> Finds the max index in double precision arrays of rank 1.
!! @param[in] arr double precision(:)
function int__imaxloc__2 (arr) result(ans)
  implicit none
  real(kind=dp_), intent (in) :: arr(:)
  integer :: ans
  call do_safe_within('int__imaxloc__2', mod_utils_name_, private_imax)
contains
  subroutine private_imax
    integer :: imax(1)
    imax = maxloc(arr(:))
    ans = imax(1)
  end subroutine private_imax
end function int__imaxloc__2

!> Finds the min index in integers of rank 1.
!! @param[in] arr integer(:)
function int__iminloc__1 (arr) result(ans)
  implicit none
  integer, intent(in) :: arr(:)
  integer :: ans
  call do_safe_within('int__iminloc__1', mod_utils_name_, private_iminloc)
contains
  subroutine private_iminloc
    integer :: imin(1)
    imin = minloc(arr(:))
    ans = imin(1)
  end subroutine private_iminloc
end function int__iminloc__1

!> Finds the min index in double precision arrays of rank 1.
!! @param[in] arr double precision(:)
function int__iminloc__2 (arr) result(ans)
  implicit none
  real(kind=dp_), intent (in) :: arr(:)
  integer :: ans
  call do_safe_within('int__iminloc__2', mod_utils_name_, private_iminloc)
contains
  subroutine private_iminloc
    integer :: imin(1)
    imin = minloc(arr(:))
    ans = imin(1)
  end subroutine private_iminloc
end function int__iminloc__2

!> @}
