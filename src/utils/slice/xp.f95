! pure function pad_slice (x, s) result(ans)
!   implicit none
!   real(kind=xp_), intent(in) :: x(:)
!   integer, intent(in) :: s(:)
!   real(kind=xp_) :: ans(size(s))
!   integer :: i, j
!   do i = 1 size(s)
!      j = s(i)
!      if (j > 0) then
!         ans(i) = x(j)
!      else
!         ans(i) = 0
!      end if
!   end do
! end function pad_slice

! pure function unpad_slice (xs, s) result(ans)
!   implicit none
!   real(kind=xp_) :: xs(:)
!   integer, intent(in) :: s(:)
!   integer :: ans(sum(int(s > 0)))
!   ans = pack(xs, s > 0)
! end function unpad_slice

! subroutine assign_padded_slice (x, xs, s)
!   implicit none
!   real(kind=xp_), intent(inout) :: x(:)
!   real(kind=xp_), intent(in) :: xs
!   integer, intent(in) :: s(:)
!   x(pack(s, s > 0)) = unpad_slice(xs, s)
! end subroutine assign_padded_slice

