pure function int__shape_to_flat__1 (s, shp) result(ans)
  implicit none
  integer, intent(in) :: s, shp
  integer :: ans
  if (s < 1) then
     ans = -huge(1)
  else if (s > shp) then
     ans = -huge(1)
  else
     ans = s
  end if
end function int__shape_to_flat__1

!> Helper function for slicing 'numbers'.
!! Given a slice identifying a single value in a 'number',
!! and the 'number' shape, return the corresponding index in the flat array.
!! @author Filippo Monari
!! @param[in] s integer(:), slice
!! @param[in] shp integer(:), 'number' shape
pure function int__shape_to_flat__2 (s, shp) result(ans)
  implicit none
  integer, intent(in) :: s(:), shp(:)
  integer :: ans, i
  if (any(s < 1)) then
     ans = -huge(1)
  else if (any(s > shp)) then
     ans = -huge(1)
  else
     ans = s(1) + sum((s(2:size(s)) - 1) * [(product(shp(1:i)), i = 1, size(shp) - 1)])
     !ans = sum((s - 1) * [(product(shp(:i)), i = 1, size(shp) - 1)]) + 1
  end if
end function int__shape_to_flat__2

pure function int__shape_to_flat__3 (s, shp) result(ans)
  implicit none
  integer, intent(in) :: s(:,:), shp(:)
  integer :: ans(size(s, 2)), i
  do i = 1, size(s, 2)
     ans(i) = shape_to_flat(s(:,i), shp)
  end do
end function int__shape_to_flat__3

function int__flat_to_shape (index, shp) result(ans)
  implicit none
  integer, intent(in) :: index, shp(:)
  integer :: i, j, k, ans(size(shp))
  j = index - 1
  ans = 1
  ans(2:) = shp(:(size(shp) - 1))
  do i = size(shp), 1, -1
     k = product(ans(:i))
     ans(i) = j / k
     j = modulo(j, k)
  end do
  ans = ans + 1
end function int__flat_to_shape

!> Helper function for slicing 'numbers'.
!! Given a matrix having as columns slices indentifying individual values in a 'number',
!! returns the corresponding indexes in the flat array.
!! @author Filippo Monari
!! @param[in] s integer(:,:), slice matrix
!! @param[in] shp integer(:), 'number' shape
subroutine int__get_slice_flat_indexes (s, shp, ans)
  implicit none
  integer, intent(in) :: s(:,:), shp(:)
  integer, intent(out) :: ans(size(s, 2))
  integer :: i
  do i = 1, size(s, 2)
     ans(i) = shape_to_flat(s(:,i), shp)
  end do
end subroutine int__get_slice_flat_indexes

!> Given a dimension, calculates that dimension index
!! of each element of an array
!! @param[in] shp integer(:), 'number' shape
!! @param[in] s integer(:), dimension
!! @param[out] ans integer(:), array containing the dimension indexes
subroutine int__get_slice_shape_indexes (shp, ans)
  implicit none
  integer, intent(in) :: shp(:)
  integer, intent(out) :: ans(:,:)
  integer :: i
  do i = 1, size(ans, 2)
     ans(:,i) = flat_to_shape(i, shp)
  end do
end subroutine int__get_slice_shape_indexes

!> Given more than one dimension, calcualtes the relative index
!! for all the elements in a 'number'
!! @param[in] shp integer(:), 'number' shape
!! @param[in] s integer(:), dimension
!! @param[out] iii integer(:), index along the given dimensions
!! @param[out] k integer, max value of iii
subroutine int__get_slices_along_dim (shp, s, iii, k)
  implicit none
  integer, intent(in) :: shp(:), s(:)
  integer, intent(out), target :: iii(product(shp))
  integer, intent(out) :: k
  integer :: i, j, sindexes(size(shp), product(shp))
  integer :: p(product(shp(s)))
  integer, pointer :: jjj(:,:)
  k = product(shp(s))
  call get_slice_shape_indexes(shp, sindexes)
  if (size(s) > 1) then
     call get_slice_flat_indexes(sindexes(s,:), shp(s), sindexes(1,:))
  else
     sindexes(1,:) = sindexes(s(1),:)
  end if
  p = 1
  jjj(1:(product(shp) / k), 1:k) => iii
  do i = 1, size(sindexes, 2)
     j = sindexes(1, i)
     jjj(p(j), j) = i
     p(j) = p(j) + 1
  end do
end subroutine int__get_slices_along_dim

!> Helper function for slicing 'numbers'.
!! Given a matrix having as columns slices indentifying individual
!! values in a 'number' returns the shape of the resulting slice.
!! @author Filippo Monari
!! @param[in] s integer(:,:), slice matrix
function int__slice_shape (s) result(ans)
  implicit none
  integer, intent(in) :: s(:,:)
  integer :: ans(size(s, 1))
  integer :: i
  !$omp parallel do
  do i = 1, size(s, 1)
     ans(i) = size(unique(s(i,:)))
  end do
  !$omp end parallel do
end function int__slice_shape

subroutine find_slices (s, i, m, si)
  implicit none
  integer, intent(in) :: s(:), i, m
  integer, intent(out) :: si(m)
  integer :: j, jj
  jj = 0
  do j=1, size(s)
     if (s(j) == i) then
        jj = jj + 1
        si(jj) = j
     end if
     if (jj >= m) exit
  end do
end subroutine find_slices

elemental function int__dilated_size (s, d) result(ans)
  implicit none
  integer, intent(in) :: s, d
  integer :: ans
  ans = s + d * (s - 1)
end function int__dilated_size

pure function int__dilated_grid_1d (s, d) result(ans)
  implicit none
  integer, intent(in) :: s, d
  integer :: i, ans(dilated_size(s, d))
  if (d < 1) then
     ans = 1
  else
     ans = [(merge(1, 0, modulo(i-1, d + 1) == 0), i = 1, size(ans))]
  end if
end function int__dilated_grid_1d

pure function int__dilated_grid_2d (s, d) result(ans)
  implicit none
  integer, intent(in) :: s(:), d(:)
  integer :: ans(product(dilated_size(s, d)))
  integer :: i, j, shp(size(s))
  integer, allocatable :: g1(:), g2(:)
  shp = dilated_size(s, d)
  allocate(g1, source=dilated_grid_1d(s(1), d(1)))
  allocate(g2, source=dilated_grid_1d(s(2), d(2)))
  do i = 1, shp(1)
     do j = 1, shp(2)
        ans(shape_to_flat([i, j], shp)) = g1(i) * g2(j)
     end do
  end do
end function int__dilated_grid_2d

pure function int__dilated_grid_3d (s, d) result(ans)
  implicit none
  integer, intent(in) :: s(:), d(:)
  integer :: ans(product(dilated_size(s, d)))
  integer :: i, j, k, shp(size(s))
  integer, allocatable :: g1(:), g2(:), g3(:)
  shp = dilated_size(s, d)
  allocate(g1, source=dilated_grid_1d(s(1), d(1)))
  allocate(g2, source=dilated_grid_1d(s(2), d(2)))
  allocate(g3, source=dilated_grid_1d(s(3), d(3)))
  do i = 1, shp(1)
     do j = 1, shp(2)
        do k = 1, shp(3)
           ans(shape_to_flat([i, j, k], shp)) = g1(i) * g2(j) * g3(k)
        end do
     end do
  end do
end function int__dilated_grid_3d

elemental function conv_effective_size (lx, lk, stride, dilation, pad) result(ans)
  implicit none
  integer, intent(in) :: lx, lk, stride, dilation, pad
  integer :: ans
  ans = lx + 2 * pad - dilated_size(lk, dilation) + 1
end function conv_effective_size

elemental function conv1d_lout (l, s, stride, dilation, pad) result(ans)
  implicit none
  integer, intent(in) :: l, s, dilation, stride, pad
  integer :: ans
  ans = (conv_effective_size(l, s, stride, dilation, pad) - 1) / stride + 1
end function conv1d_lout

function offset_slice (s, o, n) result(ans)
  implicit none
  integer, intent(in) :: s(:), o, n
  integer :: i, a, b, ans(size(s) * n)
  do i = 1, n
     a = (i - 1) * size(s) + 1
     b = i * size(s)
     ans(a:b) = (i - 1) * o + s
  end do
end function offset_slice

subroutine kernel_slices_1d (lx, lk, stride, dil, pad, slices)
  integer, intent(in) :: lx, lk, stride, dil, pad
  integer, intent(out), target :: slices(:)
  integer :: i, j, jj, s1, s2, lo, m, es, ds
  integer :: s(dilated_size(lk, dil)), dmask(lk)
  integer, pointer :: xs(:,:)
  es = conv_effective_size(lx, lk, stride, dil, pad)
  lo = conv1d_lout(lx, lk, stride, dil, pad)
  ds = dilated_size(lk, dil)
  dmask = which(dilated_grid_1d(lk, dil) == 1)
  xs(1:lk, 1:lo) => slices
  !!$omp parallel do private(j, jj, s, s1, s2, m)
  do i = 1, es, stride
     j = i - pad
     jj = j + ds - 1
     s2 = 0
     do s1 = j, jj
        s2 = s2 + 1
        s(s2) = shape_to_flat(s1, lx)
     end do
     m = (i - 1) / stride + 1
     xs(:,m) = s(dmask)
  end do
  !!end parallel do
end subroutine kernel_slices_1d

subroutine expand_grid_2d (shp, j1, jj1, j2, jj2, s)
  integer, intent(in) :: shp(2), j1, jj1, j2, jj2
  integer, intent(out) :: s(:)
  integer :: s1, s2, s3
  s3 = 0
  do s2 = j2, jj2
     do s1 = j1, jj1
        s3 = s3 + 1
        s(s3) = shape_to_flat([s1, s2], shp)
     end do
  end do
end subroutine expand_grid_2d

subroutine kernel_slices_2d (lx, lk, stride, dil, pad, slices)
  integer, intent(in) :: lx(2), lk(2), stride(2), dil(2), pad(2)
  integer, intent(out), target :: slices(2)
  integer :: i1, i2, j1, jj1, j2, jj2, lo(2), m(2), es(2), ds(2)
  integer :: s(product(dilated_size(lk, dil))), dmask(product(lk))
  integer, pointer :: xs(:,:,:)
  es = conv_effective_size(lx, lk, stride, dil, pad)
  lo = conv1d_lout(lx, lk, stride, dil, pad)
  ds = dilated_size(lk, dil)
  dmask = which(dilated_grid_2d(lk, dil) == 1)
  xs(1:product(lk), 1:lo(1), 1:lo(2)) => slices
  !$omp parallel do private(j2, jj2, j1, jj1, s, m)
  do i2 = 1, es(2), stride(2)
     j2 = i2 - pad(2)
     jj2 = j2 + ds(2) - 1
     do i1 = 1, es(1), stride(1)
        j1 = i1 - pad(1)
        jj1 = j1 + ds(1) - 1
        call expand_grid_2d(lx, j1, jj1, j2, jj2, s)
        m = ([i1, i2] - 1) / stride + 1
        xs(:,m(1), m(2)) = s(dmask)
     end do
  end do
  !$omp end parallel do
end subroutine kernel_slices_2d

subroutine expand_grid_3d (shp, j1, jj1, j2, jj2, j3, jj3, s)
  integer, intent(in) :: shp(:), j1, jj1, j2, jj2, j3, jj3
  integer, intent(out) :: s(:)
  integer :: s1, s2, s3, s4
  s4 = 0
  do s3 = j3, jj3
     do s2 = j2, jj2
        do s1 = j1, jj1 
           s4 = s4 + 1
           s(s4) = shape_to_flat([s1, s2, s3], shp)
        end do
     end do
  end do
end subroutine expand_grid_3d

subroutine kernel_slices_3d(lx, lk, stride, dil, pad, slices)
  integer, intent(in) :: lx(:), lk(:), stride(:), dil(:), pad(:)
  integer, intent(out), target :: slices(:)
  integer :: i1, i2, i3, j1, jj1, j2, jj2, j3, jj3, lo(3), m(3), es(3), ds(3)
  integer :: s(product(dilated_size(lk, dil))), dmask(product(lk))
  integer, pointer :: xs(:,:,:,:)
  es = conv_effective_size(lx, lk, stride, dil, pad)
  lo = conv1d_lout(lx, lk, stride, dil, pad)
  ds = dilated_size(lk, dil)
  dmask = which(dilated_grid_3d(lk, dil) == 1)
  xs(1:product(lk), 1:lo(1), 1:lo(2), 1:lo(3)) => slices
  !!$omp parallel do private(j3, jj3, j2, jj2, j1, jj1, s, m)
  do i3 = 1, es(3), stride(3)
     j3 = i3 - pad(3)
     jj3 = j3 + ds(3) - 1
     do i2 = 1, es(2), stride(2)
        j2 = i2 - pad(2)
        jj2 = j2 + ds(2) - 1
        do i1 = 1, es(1), stride(1)
           j1 = i1 - pad(3)
           jj1 = j1 + ds(3) - 1
           call expand_grid_3d(lx, j1, jj1, j2, jj2, j3, jj3, s)
           m = ([i1, i2, i3] - 1) / stride + 1
           xs(:,m(1), m(2), m(3)) = s(dmask)
        end do
     end do
  end do
  !!$omp end parallel do
end subroutine kernel_slices_3d


  
  


!> @}
