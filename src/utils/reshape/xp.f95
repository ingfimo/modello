!> Helper function - Reshapes the larger array (h) according to the
!! size of smaller (m) through a pointer (xh).
!! The reshaping is done along the leading dimensions (i.e. columns)
!! Its purpose is to allow broadcast binary operations.
!! @param[in] h double precision(:), array to reshape 
!! @param[in] m integer, carachteristc reshaping length
!! @param[in] xh double precision(:,:), pointer, reshaping pointer
subroutine xp__reshape_bcast (h, m, xh)
  implicit none
  real(kind=xp_), intent(in), target :: h(:)
  integer, intent(in) :: m
  real(kind=xp_), intent(out), pointer :: xh(:,:)
  integer :: n
  n = size(h) / m
  xh(1:m,1:n) => h
end subroutine xp__reshape_bcast
