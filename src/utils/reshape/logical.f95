subroutine logical__reshape_bcast (h, m, xh)
  implicit none
  logical, intent(in), target :: h(:)
  integer, intent(in) :: m
  logical, intent(out), pointer :: xh(:,:)
  integer :: n
  n = size(h) / m
  xh(1:m,1:n) => h
end subroutine logical__reshape_bcast
