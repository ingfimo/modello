!> Helper function to bind 'numbers' together.
!! Fills the binding 'number' or the binded 'numbers' (depending on the value of 'rev')
!! with the correct values taken from source/s.
!! @author Filippo Monari
!! @param[inout] x1 'number' storing the bind
!! @param[inout] x2,x3 'numbers' binded together
!! @param[in] s2 integer, shape of x1
!! @param[in] s3 integer, shape of x3
!! @param[in] k integer, dimension along which to do the binding
!! @param[in] rev logical, if .true. elements flows from the binding to the binded element
subroutine xp__bind_fill (x1, x2, x3, s2, s3, k, rev)
  implicit none
  real(kind=xp_), intent(inout) :: x1(:)
  real(kind=xp_), intent(inout) :: x2(:), x3(:)
  integer, intent(in) :: k, s2(:), s3(:)
  logical, intent(in) :: rev
  integer ::l2, l3
  l2 = bind_lag(s2, k)
  l3 = bind_lag(s3, k)
  if (rev) then
     x2 = x1(bind_indexes(0, l2, l3, size(x1)))
     x3 = x1(bind_indexes(l2, l3, l2, size(x1)))
  else
     x1(bind_indexes(0, l2, l3, size(x1))) = x2
     x1(bind_indexes(l2, l3, l2, size(x1))) = x3
  end if
end subroutine xp__bind_fill

