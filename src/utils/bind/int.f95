!> Helper function to bind 'numbers' together.
!! Calculates the lag (offset) that is necessary to consider
!! in binding two 'numbers'
!! @author Filippo Monari
!! @param[in] shp integer(:), 'number' shape
!! @param[in] k integer,  dimension along which to do the binding
function int__bind_lag (shp, k) result(ans)
  implicit none
  integer, intent(in) :: shp(:), k
  integer :: ans
  ans = product(shp(1:k))
end function int__bind_lag

!> Helper function to bind 'numbers' together.
!! Calculates the index corresponding to the values of a
!! binded 'number'.
!! @author Filippo Monari
!! @param[in] l0 integer, initial offset
!! @param[in] l1 integer, step length of the binded number
!! @param[in] l2 integer, offset due to the second binded 'number'
!! @param[in] n integer, total length of the bind
function int__bind_indexes (l0, l1, l2, n) result(ans)
  implicit none
  integer, intent(in) :: l0, l1, l2, n
  integer, allocatable :: ans(:)
  ans = private_indexes(l0)
contains
  recursive function private_indexes(pl0) result(pans)
    integer, intent(in) :: pl0
    integer, allocatable :: pans(:)
    integer :: i, a, b
    a = pl0 + 1
    b = pl0 + l1
    if (b > n) then
       pans = [integer::]
    else
       pans = [(i, i=a, b), private_indexes(b + l2)]
    end if
  end function private_indexes
end function int__bind_indexes






