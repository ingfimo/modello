!> Appends an @int scalar to a @vector @allocatalbe.
!! @param[inout] x1 @int, @vector, @allocatable
!! @param[in] x2 @int, value to append
!! @param[in] x1name @char, @c x1 name (for error reporting)
subroutine int__append_to_array__1 (x1, x2, x1name)
  implicit none
  integer, intent(inout), allocatable :: x1(:)
  integer, intent(in) :: x2
  character(len=*), intent(in) :: x1name
  call do_safe_within('int__append_to_array__1', mod_utils_name_, private_append)
contains
  subroutine private_append
    integer, allocatable :: tmp(:)
    if (allocated(x1)) then
       call alloc(tmp, x1, 'tmp')
       call dealloc(x1, x1name)
    else
       call alloc(tmp, [integer::], 'tmp')
    end if
    call alloc(x1, [tmp, x2], x1name)
  end subroutine private_append
end subroutine int__append_to_array__1

!> Appends integer of rank 1 to alloctable array.
subroutine int__append_to_array__2 (x1, x2, x1name)
  implicit none
  integer, intent(inout), allocatable :: x1(:)
  character(len=*), intent(in) :: x1name
  integer, intent(in) :: x2(:)
  call do_safe_within('int__append_to_array__2', mod_utils_name_, private_append)
contains
  subroutine private_append
    integer, allocatable :: tmp(:)
    if (allocated(x1)) then
       call alloc(tmp, x1, 'tmp')
       call dealloc(x1, x1name)
    else
       call alloc(tmp, [integer::], 'tmp')
    end if
    call alloc(x1, [tmp, x2], x1name)
  end subroutine private_append
end subroutine int__append_to_array__2

!> Appends integer scalar to pointer array.
subroutine int__append_to_array__3 (x1, x2, x1name)
  implicit none
  integer, intent(inout), pointer :: x1(:)
  integer, intent(in) :: x2
  character(len=*), intent(in) :: x1name
  call do_safe_within('int__append_to_array__3', mod_utils_name_, private_append)
contains
  subroutine private_append
    integer, allocatable :: tmp(:)
    if (associated(x1)) then
       call alloc(tmp, x1, 'tmp')
       call dealloc(x1, x1name)
    else
       call alloc(tmp, [integer::], 'tmp')
    end if
    call alloc(x1, [tmp, x2], x1name)
  end subroutine private_append
end subroutine int__append_to_array__3

!> Appends integer of rank 1 to pointer array.
subroutine int__append_to_array__4 (x1, x2, x1name)
  implicit none
  integer, intent(inout), pointer :: x1(:)
  character(len=*), intent(in) :: x1name
  integer, intent(in) :: x2(:)
  call do_safe_within('int__append_to_array__4', mod_utils_name_, private_append)
contains
  subroutine private_append
    integer, allocatable :: tmp(:)
    if (associated(x1)) then
       call alloc(tmp, x1, 'tmp')
       call dealloc(x1, x1name)
    else
       call alloc(tmp, [integer::], 'tmp')
    end if
    call alloc(x1, [tmp, x2], x1name)
  end subroutine private_append
end subroutine int__append_to_array__4

!> @todo
! subroutine int__randpend__1 (x1, x2)
!   implicit none
!   integer, intent(inout), allocatable :: x1(:)
!   integer, intent(in) :: x2
!   real(kind=dp_) :: r(1)
!   call unif_rand(r)
!   if (r(1) > 0.5) then
!      x1 = [x1, x2]
!   else
!      x1 = [x2, x1]
!   end if
! end subroutine int__randpend__1

! subroutine int__randpend__2 (x1, x2)
!   implicit none
!   integer, intent(inout), allocatable :: x1(:)
!   integer, intent(in) :: x2(:)
!   integer :: i
!   do i = 1, size(x2)
!      call randpend(x1, x2(i))
!   end do
! end subroutine int__randpend__2
