  !> Append to array of rank 1.
  !! @author Filippo Monari
  !! @param x1 appending array
  !! @param x2 array/scalar to be appended
  !! @param x1name character identifying x1
  interface append_to_array
     ! Integer
     module procedure int__append_to_array__1
     module procedure int__append_to_array__2
     module procedure int__append_to_array__3
     module procedure int__append_to_array__4
     ! ---
     ! Single precision
     module procedure sp__append_to_array__1
     module procedure sp__append_to_array__2
     module procedure sp__append_to_array__3
     module procedure sp__append_to_array__4
     ! ---
     ! Double precision
     module procedure dp__append_to_array__1
     module procedure dp__append_to_array__2
     module procedure dp__append_to_array__3
     module procedure dp__append_to_array__4
  end interface append_to_array

  ! interface randpend
  !    module procedure int__randpend__1
  !    module procedure int__randpend__2
  ! end interface randpend
