!> @defgroup utils__append_ Utility Procedures to Append Data to Arrays
!! @author Filippo Monari
!! @{


!> Appends double precision of rank 0 to alloctable array.
subroutine xp__append_to_array__1 (x1, x2, x1name)
  implicit none
  real(kind=xp_), intent(inout), allocatable :: x1(:)
  real(kind=xp_), intent(in) :: x2
  character(len=*), intent(in) :: x1name
  call do_safe_within('xp__append_to_array__1', mod_utils_name_, private_append)
contains
  subroutine private_append
    real(kind=xp_), allocatable :: tmp(:)
    if (allocated(x1)) then
       call alloc(tmp, x1, 'tmp')
       call dealloc(x1, x1name)
    else
       call alloc(tmp, [real(kind=xp_)::], 'tmp')
    end if
    call alloc(x1, [tmp, x2], x1name)
  end subroutine private_append
end subroutine xp__append_to_array__1

!> Appends double precision of rank 1 to alloctable array.
subroutine xp__append_to_array__2 (x1, x2, x1name)
  implicit none
  real(kind=xp_), intent(inout), allocatable :: x1(:)
  character(len=*), intent(in) :: x1name
  real(kind=xp_), intent(in) :: x2(:)
  call do_safe_within('xp__append_to_array__2', mod_utils_name_, private_append)
contains
  subroutine private_append
    real(kind=xp_), allocatable :: tmp(:)
    if (allocated(x1)) then
       call alloc(tmp, x1, 'tmp')
       call dealloc(x1, x1name)
    else
       call alloc(tmp, [real(kind=xp_)::], 'tmp')
    end if
    call alloc(x1, [tmp, x2], x1name)
  end subroutine private_append
end subroutine xp__append_to_array__2

!> Appends double precision of rank 0 to pointer array.
subroutine xp__append_to_array__3 (x1, x2, x1name)
  implicit none
  real(kind=xp_), intent(inout), pointer :: x1(:)
  real(kind=xp_), intent(in) :: x2
  character(len=*), intent(in) :: x1name
  call do_safe_within('xp__append_to_array__3', mod_utils_name_, private_append)
contains
  subroutine private_append
    real(kind=xp_), allocatable :: tmp(:)
    if (associated(x1)) then
       call alloc(tmp, x1, 'tmp')
       call dealloc(x1, x1name)
    else
       call alloc(tmp, [real(kind=xp_)::], 'tmp')
    end if
    call alloc(x1, [tmp, x2], x1name)
  end subroutine private_append
end subroutine xp__append_to_array__3

!> Appends double precision of rank 1 to pointer array.
subroutine xp__append_to_array__4 (x1, x2, x1name)
  implicit none
  real(kind=xp_), intent(inout), pointer :: x1(:)
  character(len=*), intent(in) :: x1name
  real(kind=xp_), intent(in) :: x2(:)
  call do_safe_within('xp__append_to_array__4', mod_utils_name_, private_append)
contains
  subroutine private_append
    real(kind=xp_), allocatable :: tmp(:)
    if (associated(x1)) then
       call alloc(tmp, x1, 'tmp')
       call dealloc(x1, x1name)
    else
       call alloc(tmp, [real(kind=xp_)::], 'tmp')
    end if
    call alloc(x1, [tmp, x2], x1name)
  end subroutine private_append
end subroutine xp__append_to_array__4

!> @}
