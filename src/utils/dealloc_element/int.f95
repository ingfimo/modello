!> Deallocates scalar integer pointer elements
!! @param[inout] x integer, pointer element to deallocate
!! @param[in] init integer, flag indicating the association type (associated or allocate)
!! @param[in] xname character, variable name (for error reporting)
subroutine  int__dealloc_element__1 (x, init, xname) 
  implicit none
  integer, intent(inout), pointer :: x
  integer, intent(in) :: init
  character(len=*), intent(in) :: xname
  call do_safe_within('int__dealloc_element1', mod_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    select case (init)
    case (init_alloc_)
       call dealloc(x, xname)
    case (init_assoc_)
       nullify(x)
    case (init_null_)
       continue
    case default
       call raise_error('init', err_unknwnVal_)
    end select
  end subroutine private_deallocate
end subroutine int__dealloc_element__1

!> Deallocates integer pointer elements of rank 1
!! @param[inout] x integer(:), pointer element to deallocate
!! @param[in] init integer, flag indicating the association type (associated or allocate)
!! @param[in] xname character, variable name (for error reporting)
subroutine int__dealloc_element__2 (x, init, xname)
  implicit none
  integer, intent(inout), pointer :: x(:)
  integer, intent(in) :: init
  character(len=*), intent(in) :: xname
  call do_safe_within('int__deallocate_element__2', mod_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    select case (init)
    case (init_alloc_)
       call dealloc(x, xname)
    case (init_assoc_)
       nullify(x)
    case default
       call raise_error('init', err_unknwnVal_)
    end select
  end subroutine private_deallocate
end subroutine int__dealloc_element__2
