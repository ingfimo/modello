!> @defgroup utils__dealloc_element_ Utility Procedures to Allocate a Number's Elements
!! @author Filippo Monari  
!! @{

!> Deallocates scalar double precision pointer elements
!! @param[inout] x double precision, pointer element to deallocate
!! @param[in] init integer, flag indicating the association type (associated or allocate)
!! @param[in] xname character, variable name (for error reporting)
subroutine xp__dealloc_element__1 (x, init, xname) 
  implicit none
  real(kind=xp_), intent(inout), pointer :: x
  integer, intent(in) :: init
  character(len=*), intent(in) :: xname
  call do_safe_within('xp__dealloc_element__1', mod_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    select case (init)
    case (init_alloc_)
       call dealloc(x, xname)
    case (init_assoc_)
       nullify(x)
    case default
       call raise_error('init', err_unknwnVal_)
    end select
  end subroutine private_deallocate
end subroutine xp__dealloc_element__1

!> Deallocates double precision pointer elements of rank 1
!! @param[inout] x double precision(:), pointer element to deallocate
!! @param[in] init integer, flag indicating the association type (associated or allocate)
!! @param[in] xname character, variable name (for error reporting)
subroutine  xp__dealloc_element__2 (x, init, xname) 
  implicit none
  real(kind=xp_), intent(inout), pointer :: x(:)
  integer, intent(in) :: init
  character(len=*), intent(in) :: xname
  call do_safe_within('xp__dealloc_element__2', mod_utils_name_, private_do)
contains
  subroutine private_do
    call err_safe(private_deallocate)
  end subroutine private_do
  subroutine private_deallocate
    select case (init)
    case (init_alloc_)
       call dealloc(x, xname)
    case (init_assoc_)
       nullify(x)
    case default
       call raise_error('init', err_unknwnVal_)
    end select
  end subroutine private_deallocate
end subroutine xp__dealloc_element__2

!> @}


