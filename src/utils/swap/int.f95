!> Swaps integer scalars.
!! @param[inout] a integer
!! @param[inout] b integer
subroutine int__swap__1 (a, b)
  implicit none
  integer, intent (inout) :: a, b
  call do_safe_within('int__swap__2', mod_utils_name_, private_swap)
contains
  subroutine private_swap
    integer :: dum
    dum = a
    a = b
    b = dum
  end subroutine private_swap
end subroutine int__swap__1

subroutine int__swap_index_order(x, i, j)
  implicit none
  integer, intent(in) :: x(:)
  integer, intent(inout) :: i, j
  integer :: swp
  if (x(j) < x(i)) then
     swp=i
     i=j
     j=swp
  end if
end subroutine int__swap_index_order
  
