!> @defgroup utils__swap_ Utility Procedures to Swap Variable Between Eachother
!! @author Filippo Monari
!! @{


!> Swaps double precision scalars.
!! @param[inout] a double precision
!! @param[inout] b double precision
subroutine xp__swap__1 (a, b)
  implicit none
  real(kind=xp_), intent (inout) :: a, b
  call do_safe_within('xp__swap__1', mod_utils_name_, private_swap)
contains
  subroutine private_swap
    real(kind=xp_) :: dum
    dum = a
    a = b
    b = dum
  end subroutine private_swap
end subroutine xp__swap__1

!> Swaps double precision variables of rank 1
!! with equal size.
!! @param[inout] a double precision(:)
!! @param[inout] b double precision(:)
subroutine xp__swap__2 (a, b)
  implicit none
  real(kind=xp_), intent (inout) :: a(:), b(:)
  call do_safe_within('xp__swap__2', mod_utils_name_, private_swap)
contains
  subroutine private_swap
    real(kind=dp_) :: dum(size(a))
    call assert(size(a) == size(b), err_wrngSz_, 'a or b')
    if (err_free()) dum=a; a=b; b=dum
  end subroutine private_swap
end subroutine xp__swap__2

!> @}
