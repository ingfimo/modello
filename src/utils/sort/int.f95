subroutine int__sort_index(iarr, iii)
  implicit none
  integer, intent(in) :: iarr(:)
  integer, intent(out) :: iii(:)
  integer, parameter :: NN=15, NSTACK=50
  integer :: a
  integer :: n, k, i, j, iiit, jstack, l, r
  integer, allocatable :: istack(:), tstack(:)
  allocate(istack(NSTACK))
  n = size(iii)
  iii=arth_i(1, 1, n)![(i, i=1, n)]
  jstack=0
  l=1
  r=n
  do
     if (r-l < NN) then
        do j=l+1,r
           iiit=iii(j)
           a=iarr(iiit)
           do i=j-1,1,-1
              if (iarr(iii(i)) <= a) exit
              iii(i+1)=iii(i)
           end do
           iii(i+1)=iiit
        end do
        if (jstack == 0) RETURN
        r=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
     else
        k=(l+r)/2
        call swap(iii(k),iii(l+1))
        call swap_index_order(iarr, iii(l), iii(r))
        call swap_index_order(iarr, iii(l+1), iii(r))
        call swap_index_order(iarr, iii(l), iii(l+1))
        i=l+1
        j=r
        iiit=iii(l+1)
        a=iarr(iiit)
        do
           do
              i=i+1
              if (iarr(iii(i)) >= a) exit
           end do
           do
              j=j-1
              if (iarr(iii(j)) <= a) exit
           end do
           if (j < i) exit
           call swap(iii(i),iii(j))
        end do
        iii(l+1)=iii(j)
        iii(j)=iiit
        jstack=jstack+2
        if (jstack > NSTACK) then
           allocate(tstack, source=istack)
           deallocate(istack)
           allocate(istack(size(tstack) + NSTACK))
           istack(1:size(tstack)) = tstack
           deallocate(tstack)
        end if
        if (r-i+1 >= j-l) then
           istack(jstack)=r
           istack(jstack-1)=i
           r=j-1
        else
           istack(jstack)=j-1
           istack(jstack-1)=l
           l=i
        end if
     end if
  end do
end subroutine int__sort_index

FUNCTION arth_i(first,increment,n)
  INTEGER, INTENT(IN) :: first,increment,n
  INTEGER, DIMENSION(n) :: arth_i
  INTEGER :: k,k2,temp
  integer, parameter ::NPAR_ARTH=16, NPAR2_ARTH = 8
  if (n > 0) arth_i(1)=first
  if (n <= NPAR_ARTH) then
     do k=2,n
        arth_i(k)=arth_i(k-1)+increment
     end do
  else
     do k=2,NPAR2_ARTH
        arth_i(k)=arth_i(k-1)+increment
     end do
     temp=increment*NPAR2_ARTH
     k=NPAR2_ARTH
     do
        if (k >= n) exit
        k2=k+k
        arth_i(k+1:min(k2,n))=temp+arth_i(1:min(k,n-k))
        temp=temp+temp
        k=k2
     end do
  end if
END FUNCTION arth_i
