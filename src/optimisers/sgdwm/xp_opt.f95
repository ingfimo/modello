!> Allocate a SGD with momentum optimiser.
!! @param[inout] x @xp_opt optimiser to allocate
!! @param[in] xin @int @vector ids of numbers with optimisation parameters
subroutine xp_opt__allocate_sgdwm (x, xin)
  implicit none
  type(xp_opt), intent(inout) :: x
  integer, intent(in) :: xin(:)
  call do_safe_within('xp_opt__allocate_sgdwm', mod_optimisers_name_, private_allocate)
contains
  subroutine private_allocate
    real(kind=xp_) :: dtyp
    call assert(is_deallocated(x), err_alreadyAlloc_, 'x')
    call assert_xin_has_dx(xin, dtyp)
    call alloc(x%xin, xin, 'x%xin')
    call opt__collect_xab(xin, x%xab, 1._xp_)
    call collect_dxin(xin, x%v1)
    call alloc(x%v2, [real(kind=xp_)::], 'x%v2')
    if (err_free()) x%iter = 0
  end subroutine private_allocate
end subroutine xp_opt__allocate_sgdwm

!> Appends a SGD with momentum optimiser to the relative register
!! @param[out] opt @xp_opt @pointer pointer to the appended optimiser
!! @param[in] xin @int @vector ids of numbers with optimisation parameters
subroutine xp_opt__append_sgdwm (opt, xin)
  implicit none
  type(xp_opt), intent(out), pointer :: opt
  integer, intent(in) :: xin(:)
  call do_safe_within('xp_opt__append_sgdwm', mod_optimisers_name_, private_append)
contains
  subroutine private_append
    real(kind=xp_) :: dtyp
    integer :: i
    i = next_opt(dtyp)
    call opt__allocate_sgdwm(xp_OPTS_(i), xin)
    opt => xp_OPTS_(i)
    opt%id = i
  end subroutine private_append
end subroutine xp_opt__append_sgdwm

!> Stochastig gradient descent with momentum update
!! @param[in] xin integer(:), indexes in the NUMBERS_ array of the input 'numbers'
!! @param[in] lr double precision, learning rate
!! @param[in] alpha double precision, weigting factor the exponential moving average
subroutine xp__sgdwm_update (opt, lr, alpha)
  implicit none
  type(xp_opt), intent(inout), target :: opt
  real(kind=xp_), intent(in) :: lr, alpha
  type(xp_number), pointer :: x
  real(kind=xp_), pointer :: vv0(:)
  integer :: i
  integer, pointer :: xxab(:,:)
  xxab(1:2, 1:size(opt%xin)) => opt%xab
  !$omp parallel do private(x, vv0)
  do i = 1, size(opt%xin)
     x => xp_NUMBERS_(opt%xin(i))
     vv0 => opt%v1(xxab(1, i):xxab(2, i))
     vv0 = vv0 * alpha + x%dv * (1 - alpha)
     !vv0 = vv0 / (1 - alpha**real(iter, dp_))
     x%v = x%v - lr * vv0
  end do
  !$omp end parallel do
end subroutine xp__sgdwm_update

!> SGD with momentum optimisation step
!! @param[in] opt @xp_opt optimiser
!! @param[in] g @xp_graph graph with the computations
!! @param[in] xout @xp_number objective function value
!! @param[in] lr @xp_ learning rate
!! @param[in] alpha @xp_ moement parameter
!! @param[inout] niter @int number of steps
!! @param[out] ftrain @xp_ total objective value of all performed steps
subroutine xp_opt__sgdwm_step (opt, g, xout, lr, alpha, niter, ftrain)
  implicit none
  type(xp_opt), intent(inout) :: opt
  type(xp_graph), intent(in) :: g
  type(xp_number), intent(inout) :: xout
  integer, intent(in) :: niter
  real(kind=xp_), intent(in) :: lr, alpha
  real(kind=xp_), intent(out) :: ftrain
  integer :: i
  ftrain = xout%v(1)
  xout%dv = 1
  do i = 1, niter
     opt%iter = opt%iter + 1
     call graph__bw_zero(g)
     call graph__bw(g)
     call sgdwm_update(opt, lr, alpha)
     call graph__post(g)
     ftrain = ftrain + get_obj(g, xout)
  end do
end subroutine xp_opt__sgdwm_step
