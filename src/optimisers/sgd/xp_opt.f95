!> @brief Allocates a SGD optimiser 
!! @param[inout] x @xp_opt optimiser to allocate
!! @param[in] xin @int @vector ids of numbers containing optimisation parameters
subroutine xp_opt__allocate_sgd (x, xin)
  implicit none
  type(xp_opt), intent(inout) :: x
  integer, intent(in) :: xin(:)
  call do_safe_within('xp_opt__allocate_sgd', mod_optimisers_name_, private_allocate)
contains
  subroutine private_allocate
    real(kind=xp_) :: dtyp
    call assert(is_deallocated(x), err_alreadyAlloc_, 'x')
    call assert_xin_has_dx(xin, dtyp)
    call alloc(x%xin, xin, 'x%xin')
    call alloc(x%xab, [integer::], "x%xab")
    call alloc(x%v1, [real(kind=xp_)::], 'x%v1')
    call alloc(x%v2, [real(kind=xp_)::], 'x%v2')
    if (err_free()) x%iter = 0
  end subroutine private_allocate
end subroutine xp_opt__allocate_sgd

!> @brief Appends a SGD optimiser to the relative register
!! @param[out] opt @xp_opt @pointer pointer to the appended optimiser
!! @param[in] xin @int @vector ids of numbers containing optimisation parameters
subroutine xp_opt__append_sgd (opt, xin)
  implicit none
  type(xp_opt), intent(out), pointer :: opt
  integer, intent(in) :: xin(:)
  call do_safe_within('sgd__append', mod_optimisers_name_, private_append)
contains
  subroutine private_append
    real(kind=xp_) :: dtyp
    integer :: i
    i = next_opt(dtyp)
    call xp_opt__allocate_sgd(xp_OPTS_(i), xin)
    opt => xp_OPTS_(i)
    opt%id = i
  end subroutine private_append
end subroutine xp_opt__append_sgd

!> Stochastic gradient descent update.
!! @param[in] xin integer(:), indexes in the NUMBERS_ array of the input 'numbers'
!! @param[in] lr double precision, learning rate
subroutine xp__sgd_update (xin, lr)
  implicit none
  integer, intent(in) :: xin(:)
  real(kind=xp_), intent(in) :: lr
  type(xp_number), pointer :: x
  integer :: i
  !$omp parallel do private(x)
  do i = 1, size(xin)
     x => xp_NUMBERS_(xin(i))
     x%v = x%v - lr * x%dv
  end do
  !$omp end parallel do
end subroutine xp__sgd_update

!> SGD optimisation step
!! @param[inout] opt @xp_opt sgd optimiser 
!! @param[in] g @xp_graph computation graph
!! @param[in] xout @xp_number containing th value of the objective function
!! @param[in] lr @xp_ learning rate
!! @param[in] niter @int number of iterations
!! @param[out] ftrain @xp_ total objective value of all performed steps
subroutine xp_opt__sgd_step (opt, g, xout, lr, niter, ftrain)
  implicit none
  type(xp_opt), intent(inout) :: opt
  type(xp_graph), intent(in) :: g
  type(xp_number), intent(in) :: xout
  integer, intent(in) :: niter
  real(kind=xp_), intent(in) :: lr
  real(kind=xp_), intent(out) :: ftrain
  integer :: i
  ftrain = xout%v(1)
  xout%dv = 1
  do i = 1, niter
     opt%iter = opt%iter + 1
     call graph__bw_zero(g)
     call graph__bw(g)
     call sgd_update(opt%xin, lr)
     call graph__post(g)
     ftrain = ftrain + get_obj(g, xout)
  end do
  !ftrain = ftrain / niter
end subroutine xp_opt__sgd_step
