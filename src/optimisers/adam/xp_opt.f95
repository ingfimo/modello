!> Allocate an Adam optimiser.
!! @param[inout] x @xp_opt optimiser to allocate
!! @param[in] xin @int @vector ids of numbers with optimisation parameters
subroutine xp_opt__allocate_adam (x, xin)
  implicit none
  type(xp_opt), intent(inout) :: x
  integer, intent(in) :: xin(:)
  call do_safe_within('xp_opt__allocate_adam', mod_optimisers_name_, private_allocate)
contains
  subroutine private_allocate
    real(kind=xp_) :: dtyp
    call assert(is_deallocated(x), err_alreadyAlloc_, 'x')
    call assert_xin_has_dx(xin, dtyp)
    call alloc(x%xin, xin, 'x%xin')
    call opt__collect_xab(xin, x%xab, 1._xp_)
    call collect_dxin(xin, x%v1)
    call alloc(x%v2, x%v1**2, 'x%v2')
    if (err_free()) x%iter = 0
  end subroutine private_allocate
end subroutine xp_opt__allocate_adam

!> Appends an Adam optimiser to the relative register
!! @param[out] opt @xp_opt @pointer pointer referring to the appended optimiser
!! @param[in] xin @int @vector ids of numbers with optimisation parameters
subroutine xp_opt__append_adam (opt, xin)
  implicit none
  type(xp_opt), intent(out), pointer :: opt
  integer, intent(in) :: xin(:)
  call do_safe_within('xp_opt__append_adam', mod_optimisers_name_, private_append)
contains
  subroutine private_append
    real(kind=xp_) :: dtyp
    integer :: i
    i = next_opt(dtyp)
    call opt__allocate_adam(xp_OPTS_(i), xin)
    opt => xp_OPTS_(i)
    opt%id = i
  end subroutine private_append
end subroutine xp_opt__append_adam

!> Adam update.
!! @param[in] xin integer(:), indexes in the NUMBERS_ array of the input 'numbers'
!! @param[in] lr double precision, learning rate
!! @param[in] beta1 double precision, weigting factor the exponential moving average of the 1st moment
!! @param[in] beta2 double precision, weigting factor the exponential moving average of the 2nd moment
!! @param[in] m double precision(:), 1st moment moving average
!! @param[in] v douvle precision(:), 2nd moment moving average
!! @param[in] iter integer, current iteration number
subroutine xp__adam_update (opt, lr, beta1, beta2)
  implicit none
  type(xp_opt), intent(inout), target :: opt
  real(kind=xp_), intent(in) :: lr, beta1, beta2
  type(xp_number), pointer :: x
  real(kind=xp_), pointer :: mm(:), vv(:)
  real(kind=xp_) :: alpha, t
  integer :: i, a, b
  integer, pointer :: xxab(:,:)
  xxab(1:2, 1:size(opt%xin)) => opt%xab
  t = real(opt%iter, xp_)
  !$omp parallel do private(a, b, x, mm, vv)
  do i = 1, size(opt%xin)
     x => xp_NUMBERS_(opt%xin(i))
     a = xxab(1, i)
     b = xxab(2, i)
     mm => opt%v1(a:b)
     vv => opt%v2(a:b)
     mm = mm * beta1 + x%dv * (1 - beta1)
     vv = vv * beta2 + x%dv**2 * (1 - beta2)
     alpha = lr * sqrt(1 - beta2 ** t) / (1 - beta1 ** t)
     x%v = x%v - alpha * mm / (sqrt(vv) + tol_dp_)
  end do
  !$omp end parallel do
end subroutine xp__adam_update

!> Adam optimisation step
!! @param[inout] opt @xp_opt optimiser
!! @param[in] g @xp_graph graph with the computations
!! @param[in] xout @xp_number objctive function value
!! @param[in] lr @xp_ learning rate
!! @param[in] beta1 @xp_ first order moment parameter
!! @param[in] beta2 @xp_ second order moment parameter
!! @param[inout] niter @int number of steps
!! @param[out] ftrain @xp_ total objective value of all performed steps
subroutine xp_opt__adam_step (opt, g, xout, lr, beta1, beta2, niter, ftrain)
  implicit none
  type(xp_opt), intent(inout) :: opt
  type(xp_graph), intent(in) :: g
  type(xp_number), intent(inout) :: xout
  integer, intent(in) :: niter
  real(kind=xp_), intent(in) :: lr, beta1, beta2
  real(kind=xp_), intent(out) :: ftrain
  integer :: i
  ftrain = 0!xout%v(1)
  xout%dv = 1
  do i = 1, niter
     opt%iter = opt%iter + 1
     call graph__bw_zero(g)
     call graph__bw(g)
     if (opt%clip) call xp__clip_gradient(opt%xin, opt%clipval)
     call adam_update(opt, lr, beta1, beta2)
     call graph__post(g)
     ftrain = ftrain + get_obj(g, xout)
  end do
  !ftrain = ftrain / niter
end subroutine xp_opt__adam_step
