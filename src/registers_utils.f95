!> @defgroup registers_utils_ Registers' Utils
!! @brief Procedures to safely interact with registers.
!! @author Filippo Monari
!! @{
!! @defgroup registers_utils_private_ Private
!! @defgroup registers_utils_public_ Public
!! @}

module registers_utils

  use env
  use types
  use registers, only: &
       sp_NUMBERS_, &
       dp_NUMBERS_, &
       NUMINDEX_, &
       INLOCKS_, &
       NUMNDS_, &
       sp_NODES_, &
       dp_NODES_, &
       NODEINDEX_, &
       NODEGRPHS_, &
       NODEi_, &
       sp_GRAPHS_, &
       dp_GRAPHS_, &
       GRAPHi_, &
       GRAPHINDEX_, &
       sp_GRAPH_, &
       dp_GRAPH_, &
       sp_OPTS_, &
       dp_OPTS_, &
       MODE_, &
       DTYP_, &
       INPOSTS_
  use errwarn
  use utils
  use numbers_utils
  use nodes_utils
  use optim_utils

  implicit none
       
  character(len=*), parameter :: mod_registers_utils_name_ = "registers_utils"

  include "./registers_utils/numbers/interfaces.f95"
  include "./registers_utils/nodes/interfaces.f95"
  include "./registers_utils/opts/interfaces.f95"
  
contains

  !> @brief Root procedure to find next available ids
  !! @param[out] i @int next available id
  !! @param[inout] iii @int @vector @allocatable all available indexes
  subroutine next_index__0 (i, iii, iii_name)
    implicit none
    integer, intent(out) :: i
    integer, intent(inout), allocatable :: iii(:)
    character(len=*), intent(in) :: iii_name
    call do_safe_within("next_index__0", mod_registers_utils_name_, private_do)
  contains
    subroutine private_do
      call assert(allocated(iii), err_notAlloc_, iii_name)
      if (err_free()) call assert(size(iii) > 0, err_generic_, iii_name//" is empty")
      call err_Safe(private_next)
    end subroutine private_do
    subroutine private_next
    i = iii(1)
    if (size(iii) > 1) then
       iii = iii(2:)
    else
       iii = [integer::]
    end if
  end subroutine private_next
  end subroutine next_index__0

  !> @brief Sets the calculation mode
  !! @param[in] m @int mode id
  subroutine set_mode (m)
    implicit none
    integer, intent(in) :: m
    call do_safe_within("set_mode", mod_registers_utils_name_, private_set_mode)
  contains
    subroutine private_set_mode
      call assert(any(modes_ == m), err_wrngArg_, "m")
      MODE_ = m
    end subroutine private_set_mode
  end subroutine set_mode

  !> @brief Sets the floating point precision to use in the calculations
  !! @param[in] p @int precision id
  subroutine set_dtyp (p)
    implicit none
    integer :: p
    call do_safe_within("set_dtyp", mod_registers_utils_name_, private_set_dtyp)
  contains
    subroutine private_set_dtyp
      call assert(any(precisions_ == p), err_wrngArg_, "p")
      if (err_free()) DTYP_ = p
    end subroutine private_set_dtyp
  end subroutine set_dtyp

  subroutine set_inposts (x)
    implicit none
    logical, intent(in) :: x
    INPOSTS_ = x
  end subroutine set_inposts

  function within_posts () result(ans)
    implicit none
    logical :: ans
    ans = INPOSTS_
  end function within_posts

  include "./registers_utils/numbers/number.f95"
  include "./registers_utils/numbers/sp_number.f95"
  include "./registers_utils/numbers/dp_number.f95"

  include "./registers_utils/nodes/sp_node.f95"
  include "./registers_utils/nodes/dp_node.f95"

  include "./registers_utils/opts/sp_opt.f95"
  include "./registers_utils/opts/dp_opt.f95"

  include "./registers_utils/hash_tables/hash_tables.f95"

end module registers_utils
