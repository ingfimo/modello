module nodes_utils

  use env
  use types
  use registers, only: &
       sp_NUMBERS_, &
       dp_NUMBERS_, &
       NUMNDS_, &
       NODEGRPHS_, &
       NODEi_, &
       GRAPHi_, &
       sp_NODES_, &
       dp_NODES_, &
       sp_GRAPH_, &
       dp_GRAPH_, &
       sp_GRAPHS_, &
       dp_GRAPHS_, &
       WITHDX_, &
       INPOSTS_
  use errwarn
  use utils, only: alloc, dealloc
  use numbers_utils
  use node_operators, only: node__op, node__fw, node__bw, node__bw_zero
  
  implicit none

  character(len=*), parameter :: mod_nodes_utils_name_ = 'nodes_utils'

  interface nodeattrs__allocate
     module procedure sp_nodeattrs__allocate
     module procedure dp_nodeattrs__allocate
  end interface nodeattrs__allocate

  interface node__allocate
     module procedure sp_node__allocate__1
     module procedure sp_node__allocate__2
     module procedure dp_node__allocate__1
     module procedure dp_node__allocate__2
  end interface node__allocate

  interface graph__allocate
     module procedure sp_graph__allocate
     module procedure dp_graph__allocate
  end interface graph__allocate

  interface nodeattrs__deallocate
     module procedure sp_nodeattrs__deallocate
     module procedure dp_nodeattrs__deallocate
  end interface nodeattrs__deallocate

  interface node__deallocate
     module procedure sp_node__deallocate__1
     module procedure sp_node__deallocate__2
     module procedure dp_node__deallocate__1
     module procedure dp_node__deallocate__2
  end interface node__deallocate

  interface graph__deallocate
     module procedure sp_graph__deallocate
     module procedure dp_graph__deallocate
  end interface graph__deallocate

  interface node__to_vec
     module procedure sp_node__to_vec
     module procedure dp_node__to_vec
  end interface node__to_vec

  interface node__update_inlock
     module procedure sp_node__update_inlock
     module procedure dp_node__update_inlock
  end interface node__update_inlock

  interface node__pack_graph
     module procedure sp_node__pack_graph
     module procedure dp_node__pack_graph
  end interface node__pack_graph

  interface node__reset_number
     module procedure sp_node__reset_number
     module procedure dp_node__reset_number
  end interface node__reset_number

  interface node__get_ipar
     module procedure sp_node__get_ipar
     module procedure dp_node__get_ipar
  end interface node__get_ipar

  interface node__get_rpar
     module procedure sp_node__get_rpar
     module procedure dp_node__get_rpar
  end interface node__get_rpar

  interface node__get_state
     module procedure sp_node__get_state
     module procedure dp_node__get_state
  end interface node__get_state

  interface node__set_state
     module procedure sp_node__set_state
     module procedure dp_node__set_state
  end interface node__set_state

  interface reset_graphi
     module procedure sp_graph__reset_graphi
     module procedure dp_graph__reset_graphi
  end interface reset_graphi

  interface graphi_is_set
     module procedure sp_graph__graphi_is_set
     module procedure dp_graph__graphi_is_set
  end interface graphi_is_set

  interface get_graphi
     module procedure sp_graph__get_graphi
     module procedure dp_graph__get_graphi
  end interface get_graphi

  interface set_graphi
     module procedure sp_graph__set_graphi
     module procedure dp_graph__set_graphi
  end interface set_graphi

  interface graph__op
     module procedure sp_graph__op
     module procedure dp_graph__op
  end interface graph__op

  interface graph__post
     module procedure sp_graph__post
     module procedure dp_graph__post
  end interface graph__post

  interface graph__fw
     module procedure sp_graph__fw
     module procedure dp_graph__fw
  end interface graph__fw

  interface graph__bw_zero
     module procedure sp_graph__bw_zero
     module procedure dp_graph__bw_zero
  end interface graph__bw_zero

  interface graph__bw
     module procedure sp_graph__bw
     module procedure dp_graph__bw
  end interface graph__bw

contains

  include "./nodes_utils/allocate/node.f95"
  include "./nodes_utils/allocate/sp_node.f95"
  include "./nodes_utils/allocate/dp_node.f95"

  include "./nodes_utils/deallocate/node.f95"
  include "./nodes_utils/deallocate/sp_node.f95"
  include "./nodes_utils/deallocate/dp_node.f95"

  include "./nodes_utils/misc/sp_node.f95"
  include "./nodes_utils/misc/dp_node.f95"

  include "./nodes_utils/op/sp_node.f95"
  include "./nodes_utils/op/dp_node.f95"

  include "./nodes_utils/fw/sp_node.f95"
  include "./nodes_utils/fw/dp_node.f95"

  include "./nodes_utils/bw/sp_node.f95"
  include "./nodes_utils/bw/dp_node.f95"

  !> @defgroup node_utils_ Utility Procedures for Nodes and Graphs
  !! @author Filippo Monari
  
 
  
  
  function nodei_is_set () result(ans)
    implicit none
    logical :: ans
    ans = NODEi_ > 0
  end function nodei_is_set

  !> Returns the index of the node currently on the stack.
  !! @author Filippo Monari
  function get_nodei () result (ans)
    implicit none
    integer :: ans
    call do_safe_within('get_nodei', mod_nodes_utils_name_, private_nodei)
  contains
    subroutine private_nodei
      ans = 0
      call assert(nodei_is_set(), err_generic_, 'NODEi_ not set.')
      if (err_free()) ans = NODEi_
    end subroutine private_nodei
  end function get_nodei

  !> Returns the index of the last node on the stack and
  !! flushes it (set the stack to 0)
  !! @author Filippo Monari
  function flush_nodei () result(ans)
    implicit none
    integer :: ans
    call do_safe_within('flush_nodei', mod_nodes_utils_name_, private_flush)
  contains
    subroutine private_flush
      ans = get_nodei()
      if (err_free()) NODEi_ = 0
    end subroutine private_flush
  end function flush_nodei

 


  
   

 


  !> @}

end module nodes_utils
