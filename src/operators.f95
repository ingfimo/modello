!> @defgroup operators_ Operators
!! @{
module operators

  use env
  use types
  use registers
  use math
  use utils
  use hashtabs_utils

  implicit none

  private

  public &
       ! Modifiers
       op_assign, &
       fw_assign, &
       bw_assign, &
       op_slice, &
       fw_slice, &
       bw_slice, &
       op_flatslice, &
       fw_flatslice, &
       bw_flatslice, &
       op_bind, &
       fw_bind, &
       bw_bind, &
       op_embeddings, &
       fw_embeddings, &
       bw_embeddings, &
       op_transpose, &
       fw_transpose, &
       bw_transpose, &
       op_feeding_number, &
       op_feeding_sequence, &
       ! ---
       ! Unary
       op_neg, &
       fw_neg, &
       bw_neg, &
       op_abs, &
       fw_abs, &
       bw_abs, &
       op_exp, &
       fw_exp, &
       bw_exp, &
       op_log, &
       fw_log, &
       bw_log, &
       op_sin, &
       fw_sin, &
       bw_sin, &
       op_cos, &
       fw_cos, &
       bw_cos, &
       op_tan, &
       fw_tan, &
       bw_tan, &
       op_sinh, &
       fw_sinh, &
       bw_sinh, &
       op_cosh, &
       fw_cosh, &
       bw_cosh, &
       op_tanh, &
       fw_tanh, &
       bw_tanh, &
       ! ---
       ! Binary
       op_add, &
       fw_add, &
       bw_add, &
       op_sub, &
       fw_sub, &
       bw_sub, &
       op_mult, &
       fw_mult, &
       bw_mult, &
       op_pow, &
       fw_pow, &
       bw_pow, &
       op_div, &
       fw_div, &
       bw_div, &
       ! ---
       ! Activations
       op_sigmoid, &
       fw_sigmoid, &
       bw_sigmoid, &
       op_softplus, &
       fw_softplus, &
       bw_softplus, &
       op_relu, &
       fw_relu, &
       bw_relu, &
       op_leakyrelu, &
       fw_leakyrelu, &
       bw_leakyrelu, &
       op_elu, &
       fw_elu, &
       bw_elu, &
       op_silu, &
       fw_silu, &
       bw_silu, &
       op_swish, &
       fw_swish, &
       bw_swish, &
       op_softmax, &
       fw_softmax, &
       bw_softmax, &
       ! ---
       ! Matrix
       op_gmmmult, &
       fw_gmmmult, &
       bw_gmmmult, &
       op_gmvmult, &
       fw_gmvmult, &
       bw_gmvmult, &
       op_vvouter, &
       fw_vvouter, &
       bw_vvouter, &
       op_vvinner, &
       fw_vvinner, &
       bw_vvinner, &
       op_invMat, &
       fw_invMat, &
       bw_invMat, &
       op_slidemmm, &
       fw_slidemmm, &
       bw_slidemmm, &
       ! ---
       ! Kernels
       op_ksqexp, &
       fw_ksqexp, &
       bw_ksqexp, &
       ! ---
       ! Reductions
       op_sum, &
       fw_sum, &
       bw_sum, &
       op_product, &
       fw_product, &
       bw_product, &
       op_ssq, &
       fw_ssq, &
       bw_ssq, &
       ! ---
       ! Stats
       op_ldexp, &
       fw_ldexp, &
       bw_ldexp, &
       op_ldlaplace, &
       fw_ldlaplace, &
       bw_ldlaplace, &
       op_ldbeta, &
       fw_ldbeta, &
       bw_ldbeta, &
       op_ldgamma, &
       fw_ldgamma, &
       bw_ldgamma, &
       op_ldnorm, &
       fw_ldnorm, &
       bw_ldnorm, &
       op_ldmvnorm__1, &
       fw_ldmvnorm__1, &
       bw_ldmvnorm__1, &
       op_mvnorm_posterior__1, &
       fw_mvnorm_posterior__1, &
       bw_mvnorm_posterior__1, &
       op_mvnorm_posterior__2, &
       fw_mvnorm_posterior__2, &
       bw_mvnorm_posterior__2, &
       ! ---
       ! Objectives
       op_binentropy, &
       fw_binentropy, &
       bw_binentropy, &
       op_logit_binentropy, &
       fw_logit_binentropy, &
       bw_logit_binentropy, &
       op_crossentropy, &
       fw_crossentropy, &
       bw_crossentropy, &
       op_logit_crossentropy, &
       fw_logit_crossentropy, &
       bw_logit_crossentropy, &
       op_mse, &
       fw_mse, &
       bw_mse, &
       op_mae, &
       fw_mae, &
       bw_mae, &
       op_lkhnorm, &
       fw_lkhnorm, &
       bw_lkhnorm, &
       ! ---
       ! Regularisation
       op_dropout, &
       fw_dropout, &
       bw_dropout, &
       ! ---
       ! Convolution
       op_conv, &
       fw_conv, &
       bw_conv, &
       op_maxpool, &
       fw_maxpool, &
       bw_maxpool

  character(len=*), parameter :: mod_operators_name_ = 'operators'

  !> @defgroup operators__activations_interfaces_ Interfaces for Activation Functions' Operators
  !! @{
  include "./operators/activations/interfaces.f95"
  !> @}

  !> @defgroup operators__binary_interfaces_ Interfaces for Binary Operators
  !! Take two inputs and return an output
  !! (e.g. Addition, subtraction, multiplication, power, and division).
  !! @{
  include "./operators/binary/interfaces.f95"
  !> @}

  !> @defgroup operators__kernels_interfaces_ Interfaces for Kernel Function Operators
  !! @{
  include "./operators/kernels/interfaces.f95"
  !> @}

  !> @defgroup operators__matrix_interfaces_ Interfaces for Matrix Operators
  !! @{
  include "./operators/matrix/interfaces.f95"
  !> @}

  !> @defgroup operators__modifiers_interfaces_ Interface for Modifiers Operators
  !! @{
  include "./operators/modifiers/interfaces.f95"
  !> @}

  !> @defgroup operators__objectives_interfaces_ Interfaces for Objective Functions' Operators
  !! @{
  include "./operators/objectives/interfaces.f95"
  !> @}

  !> @defgroup operators__reductions_interfaces_ Interfaces for Reduction Operators
  !! @{
  include "./operators/reductions/interfaces.f95"
  !> @}

  !> @defgroup operators__regularisations_interfaces_ Interfaces for Regularisation Operators
  !! @{
  include "./operators/regularisations/interfaces.f95"
  !> @}

  !> @defgroup operators__stats_interfaces_ Interfaces for Statistical Functions' Operators
  !! @{
  include "./operators/stats/interfaces.f95"
  !> @}

  !> @defgroup operators__unary_interfaces_ interfaces for Unary Operators
  !! @{
  include "./operators/unary/interfaces.f95"
  !> @}

  include "./operators/convolution/interfaces.f95"
  
contains

  include "./operators/activations/sp_number.f95"
  include "./operators/activations/dp_number.f95"
  include "./operators/binary/sp_number.f95"
  include "./operators/binary/dp_number.f95"
  include "./operators/kernels/sp_number.f95"
  include "./operators/kernels/dp_number.f95"
  include "./operators/matrix/sp_number.f95"
  include "./operators/matrix/dp_number.f95"
  include "./operators/modifiers/sp_number.f95"
  include "./operators/modifiers/dp_number.f95"
  include "./operators/objectives/sp_number.f95"
  include "./operators/objectives/dp_number.f95"
  include "./operators/reductions/sp_number.f95"
  include "./operators/reductions/dp_number.f95"
  include "./operators/regularisations/sp_number.f95"
  include "./operators/regularisations/dp_number.f95"
  include "./operators/stats/sp_number.f95"
  include "./operators/stats/dp_number.f95"
  include "./operators/unary/sp_number.f95"
  include "./operators/unary/dp_number.f95"
  include "./operators/convolution/sp_number.f95"
  include "./operators/convolution/dp_number.f95"
  
end module operators
!> @}





