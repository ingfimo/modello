!> @defgroup node_operators__activations_ Activations
!! @{

!> Sigmoid - operator
!! @param[in] nd 'node'
subroutine xp_node__op_sigmoid (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_sigmoid(x1, x2)
  end associate
end subroutine xp_node__op_sigmoid

!> Sigmoid - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_sigmoid (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_sigmoid(x1, x2)
  end associate
end subroutine xp_node__fw_sigmoid

!> Sigmoid - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_sigmoid (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_sigmoid(x1, x2)
  end associate
end subroutine xp_node__bw_sigmoid

!> @todo document
subroutine xp_node__op_softplus (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_softplus(x1, x2)
  end associate
end subroutine xp_node__op_softplus

!> @todo document
subroutine xp_node__fw_softplus (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_softplus(x1, x2)
  end associate
end subroutine xp_node__fw_softplus

!> @todo document
subroutine xp_node__bw_softplus (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_softplus(x1, x2)
  end associate
end subroutine xp_node__bw_softplus

!> ReLU - operator
!! @param[in] nd 'node'
subroutine xp_node__op_relu (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_relu(x1, x2)
  end associate
end subroutine xp_node__op_relu

!> ReLU - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_relu (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_relu(x1, x2)
  end associate
end subroutine xp_node__fw_relu

!> ReLU - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_relu (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_relu(x1, x2)
  end associate
end subroutine xp_node__bw_relu

!> Leaky ReLU - operator
!! @param[in] nd 'node'
subroutine xp_node__op_leakyrelu (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       a => xp_NUMBERS_(nd%in(2)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_leakyrelu(x1, a, x2)
  end associate
end subroutine xp_node__op_leakyrelu

!> Leaky ReLU - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_leakyrelu (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       a => xp_NUMBERS_(nd%in(2)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_leakyrelu(x1, a, x2)
  end associate
end subroutine xp_node__fw_leakyrelu

!> Leaky ReLU - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_leakyrelu (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       a => xp_NUMBERS_(nd%in(2)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_leakyrelu(x1, a, x2)
  end associate
end subroutine xp_node__bw_leakyrelu

!> ELU - operator
!! @param[in] nd 'node'
subroutine xp_node__op_elu (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       a => xp_NUMBERS_(nd%in(2)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_elu(x1, a, x2)
  end associate
end subroutine xp_node__op_elu

!> ELU - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_elu (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       a => xp_NUMBERS_(nd%in(2)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_elu(x1, a, x2)
  end associate
end subroutine xp_node__fw_elu

!> ELU - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_elu (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       a => xp_NUMBERS_(nd%in(2)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_elu(x1, a, x2)
  end associate
end subroutine xp_node__bw_elu

!> @todo document
subroutine xp_node__op_silu (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_silu(x1, x2)
  end associate
end subroutine xp_node__op_silu

!> @todo document
subroutine xp_node__fw_silu (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_silu(x1, x2)
  end associate
end subroutine xp_node__fw_silu

!> @document
subroutine xp_node__bw_silu (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_silu(x1, x2)
  end associate
end subroutine xp_node__bw_silu

!> Swish - operator
!! @param[in] nd 'node'
subroutine xp_node__op_swish (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       a => xp_NUMBERS_(nd%in(2)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_swish(x1, a, x2)
  end associate
end subroutine xp_node__op_swish

!> Swish - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_swish (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       a => xp_NUMBERS_(nd%in(2)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_swish(x1, a, x2)
  end associate
end subroutine xp_node__fw_swish

!> Swish - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_swish (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       a => xp_NUMBERS_(nd%in(2)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_swish(x1, a, x2)
  end associate
end subroutine xp_node__bw_swish

!> Standard softmax - operator
!! @param[in] nd 'node'
subroutine xp_node__op_softmax__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_softmax(x1, x2)
  end associate
end subroutine xp_node__op_softmax__1

!> Standard softmax - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_softmax__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_softmax(x1, x2)
  end associate
end subroutine xp_node__fw_softmax__1

!> Standard softmax - differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_softmax__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_softmax(x1, x2)
  end associate
end subroutine xp_node__bw_softmax__1

!> Softmax by dimension - operator
!! @param[in] nd 'node'
subroutine xp_node__op_softmax__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_softmax(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__op_softmax__2

!> Softmax by dimension - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_softmax__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_softmax(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__fw_softmax__2

!> Sofmax by dimension - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_softmax__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_softmax(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__bw_softmax__2
