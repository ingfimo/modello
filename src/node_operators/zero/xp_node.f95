!> @relates node__bw_zero
subroutine xp_node__bw_zero (nd)
  implicit none
  type(xp_node), intent(in) :: nd
  integer :: i, ii
  do i = 1, size(nd%attrs%in)
     ii = nd%attrs%in(i)
     xp_NUMBERS_(ii)%dv = 0
  end do
end subroutine xp_node__bw_zero

!> @relates node__fw_zero
subroutine xp_node__fw_zero (nd)
  implicit none
  type(xp_node), intent(in) :: nd
  integer :: i, ii
  do i = 1, size(nd%attrs%out)
     ii = nd%attrs%out(i)
     xp_NUMBERS_(ii)%dv = 0
  end do
end subroutine xp_node__fw_zero
