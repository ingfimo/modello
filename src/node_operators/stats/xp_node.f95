!> @defgroup node_operators__stats_ Statistics
!! @{

!> ldexp - operator
!! @param[in] nd 'nodeattrs'
!! @param[in] ns 'nodestates'
subroutine xp_node__op_ldexp (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       lam => xp_NUMBERS_(nd%in(2)), &
       ld => xp_NUMBERS_(nd%out(1)) &
       )
    call op_ldexp(y, lam, ld)
  end associate
end subroutine xp_node__op_ldexp

!> ldexp - forward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns 'nodestates'
subroutine xp_node__fw_ldexp (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       lam => xp_NUMBERS_(nd%in(2)), &
       ld => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_ldexp(y, lam, ld)
  end associate
end subroutine xp_node__fw_ldexp

!> ldexp - backward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns 'nodestates'
subroutine xp_node__bw_ldexp (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       lam => xp_NUMBERS_(nd%in(2)), &
       ld => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_ldexp(y, lam, ld)
  end associate
end subroutine xp_node__bw_ldexp

!> ldlaplace - operator
!! @param[in] nd `nodeattrs`
!! @param[in] ns 'nodestates'
subroutine xp_node__op_ldlaplace (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       mu => xp_NUMBERS_(nd%in(2)), &
       lam => xp_NUMBERS_(nd%in(3)), &
       ld => xp_NUMBERS_(nd%out(1)) &
       )
    call op_ldlaplace(y, mu, lam, ld)
  end associate
end subroutine xp_node__op_ldlaplace

!> ldlaplace - forward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns 'nodestates'
subroutine xp_node__fw_ldlaplace (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       mu => xp_NUMBERS_(nd%in(2)), &
       lam => xp_NUMBERS_(nd%in(3)), &
       ld => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_ldlaplace(y, mu, lam, ld)
  end associate
end subroutine xp_node__fw_ldlaplace

!> ldlaplace - backward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns 'nodestates'
subroutine xp_node__bw_ldlaplace (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       mu => xp_NUMBERS_(nd%in(2)), &
       lam => xp_NUMBERS_(nd%in(3)), &
       ld => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_ldlaplace(y, mu, lam, ld)
  end associate
end subroutine xp_node__bw_ldlaplace

!> ldbeta - operator
!! @param[in] nd `nodeattrs`
!! @param[in] ns 'nodestates'
subroutine xp_node__op_ldbeta (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       a1 => xp_NUMBERS_(nd%in(2)), &
       a2 => xp_NUMBERS_(nd%in(3)), &
       ld => xp_NUMBERS_(nd%out(1)) &
       )
    call op_ldbeta(y, a1, a2, ld)
  end associate
end subroutine xp_node__op_ldbeta

!> ldbeta - forward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns 'nodestates'
subroutine xp_node__fw_ldbeta (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       a1 => xp_NUMBERS_(nd%in(2)), &
       a2 => xp_NUMBERS_(nd%in(3)), &
       ld => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_ldbeta(y, a1, a2, ld)
  end associate
end subroutine xp_node__fw_ldbeta

!> ldbeta - backward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns 'nodestates'
subroutine xp_node__bw_ldbeta (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       a1 => xp_NUMBERS_(nd%in(2)), &
       a2 => xp_NUMBERS_(nd%in(3)), &
       ld => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_ldbeta(y, a1, a2, ld)
  end associate
end subroutine xp_node__bw_ldbeta

!> ldgamma - operator
!! @param[in] nd `nodeattrs`
!! @param[in] ns 'nodestates'
subroutine xp_node__op_ldgamma (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       a => xp_NUMBERS_(nd%in(2)), &
       b => xp_NUMBERS_(nd%in(3)), &
       ld => xp_NUMBERS_(nd%out(1)) &
       )
    call op_ldgamma(y, a, b, ld)
  end associate
end subroutine xp_node__op_ldgamma

!> ldgamma - forward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns 'nodestates'
subroutine xp_node__fw_ldgamma (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       a => xp_NUMBERS_(nd%in(2)), &
       b => xp_NUMBERS_(nd%in(3)), &
       ld => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_ldgamma(y, a, b, ld)
  end associate
end subroutine xp_node__fw_ldgamma

!> ldgamma - backward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns 'nodestates'
subroutine xp_node__bw_ldgamma (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       a => xp_NUMBERS_(nd%in(2)), &
       b => xp_NUMBERS_(nd%in(3)), &
       ld => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_ldgamma(y, a, b, ld)
  end associate
end subroutine xp_node__bw_ldgamma

!> ldnorm - operator
!! @param[in] nd `nodeattrs`
!! @param[in] ns 'nodestates'
subroutine xp_node__op_ldnorm (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       mu => xp_NUMBERS_(nd%in(2)), &
       s => xp_NUMBERS_(nd%in(3)), &
       ld => xp_NUMBERS_(nd%out(1)) &
       )
    call op_ldnorm(y, mu, s, ld)
  end associate
end subroutine xp_node__op_ldnorm

!> ldnorm - forward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns 'nodestates'
subroutine xp_node__fw_ldnorm (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       mu => xp_NUMBERS_(nd%in(2)), &
       s => xp_NUMBERS_(nd%in(3)), &
       ld => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_ldnorm(y, mu, s, ld)
  end associate
end subroutine xp_node__fw_ldnorm

!> ldnorm - backward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns 'nodestates'
subroutine xp_node__bw_ldnorm (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       mu => xp_NUMBERS_(nd%in(2)), &
       s => xp_NUMBERS_(nd%in(3)), &
       ld => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_ldnorm(y, mu, s, ld)
  end associate
end subroutine xp_node__bw_ldnorm

!> ldmvnorm - operator
!! @param[in] nd `nodeattrs`
!! @param[in] ns 'nodestates'
subroutine xp_node__op_ldmvnorm__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       mu => xp_NUMBERS_(nd%in(2)), &
       E => xp_NUMBERS_(nd%in(3)), &
       ld => xp_NUMBERS_(nd%out(1)) &
       )
    call op_ldmvnorm__1(y, mu, E, ld)
  end associate
end subroutine xp_node__op_ldmvnorm__1

!> ldmvnorm - forward differentiation
!! PLACE HOLDER
!! @param[in] nd `nodeattrs`
!! @param[in] ns 'nodestates'
subroutine xp_node__fw_ldmvnorm__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       mu => xp_NUMBERS_(nd%in(2)), &
       E => xp_NUMBERS_(nd%in(3)), &
       ld => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_ldmvnorm__1(y, mu, E, ld)
  end associate
end subroutine xp_node__fw_ldmvnorm__1

!> ldmvnorm - backward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns 'nodestates'
subroutine xp_node__bw_ldmvnorm__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       mu => xp_NUMBERS_(nd%in(2)), &
       E => xp_NUMBERS_(nd%in(3)), &
       ld => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_ldmvnorm__1(y, mu, E, ld)
  end associate
end subroutine xp_node__bw_ldmvnorm__1

!> mvnorm_posterior__1 - operator
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__op_mvnorm_posterior__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
   associate( &
       a11 => xp_NUMBERS_(nd%in(1)), &
       e21 => xp_NUMBERS_(nd%in(2)), &
       e22 => xp_NUMBERS_(nd%in(3)), &
       y11 => xp_NUMBERS_(nd%in(4)), &
       mu11 => xp_NUMBERS_(nd%in(5)), &
       pmu => xp_NUMBERS_(nd%out(1)), &
       pe => xp_NUMBERS_(nd%out(2)) &
    )
    call op_mvnorm_posterior__1(a11, e21, e22, y11, mu11, pmu, pe)
  end associate
end subroutine xp_node__op_mvnorm_posterior__1

!> mvnorm_posterior__1 - forward difference
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__fw_mvnorm_posterior__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       a11 => xp_NUMBERS_(nd%in(1)), &
       e21 => xp_NUMBERS_(nd%in(2)), &
       e22 => xp_NUMBERS_(nd%in(3)), &
       y11 => xp_NUMBERS_(nd%in(4)), &
       mu11 => xp_NUMBERS_(nd%in(5)), &
       pmu => xp_NUMBERS_(nd%out(1)), &
       pe => xp_NUMBERS_(nd%out(2)) &
    )
    call fw_mvnorm_posterior__1(a11, e21, e22, y11, mu11, pmu, pe)
  end associate
end subroutine xp_node__fw_mvnorm_posterior__1

!> mvnorm_posterior__1 - backward difference
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__bw_mvnorm_posterior__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
   associate( &
       a11 => xp_NUMBERS_(nd%in(1)), &
       e21 => xp_NUMBERS_(nd%in(2)), &
       e22 => xp_NUMBERS_(nd%in(3)), &
       y11 => xp_NUMBERS_(nd%in(4)), &
       mu11 => xp_NUMBERS_(nd%in(5)), &
       pmu => xp_NUMBERS_(nd%out(1)), &
       pe => xp_NUMBERS_(nd%out(2)) &
    )
    call bw_mvnorm_posterior__1(a11, e21, e22, y11, mu11, pmu, pe)
  end associate
end subroutine xp_node__bw_mvnorm_posterior__1

!> mvnorm_posterior__2 - operator
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__op_mvnorm_posterior__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
   associate( &
       e11 => xp_NUMBERS_(nd%in(1)), &
       e21 => xp_NUMBERS_(nd%in(2)), &
       e22 => xp_NUMBERS_(nd%in(3)), &
       y11 => xp_NUMBERS_(nd%in(4)), &
       mu11 => xp_NUMBERS_(nd%in(5)), &
       pmu => xp_NUMBERS_(nd%out(1)), &
       pe => xp_NUMBERS_(nd%out(2)) &
    )
    call op_mvnorm_posterior__2(e11, e21, e22, y11, mu11, pmu, pe)
  end associate
end subroutine xp_node__op_mvnorm_posterior__2

!> mvnorm_posterior__2 - forward difference
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__fw_mvnorm_posterior__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       e11 => xp_NUMBERS_(nd%in(1)), &
       e21 => xp_NUMBERS_(nd%in(2)), &
       e22 => xp_NUMBERS_(nd%in(3)), &
       y11 => xp_NUMBERS_(nd%in(4)), &
       mu11 => xp_NUMBERS_(nd%in(5)), &
       pmu => xp_NUMBERS_(nd%out(1)), &
       pe => xp_NUMBERS_(nd%out(2)) &
    )
    call fw_mvnorm_posterior__2(e11, e21, e22, y11, mu11, pmu, pe)
  end associate
end subroutine xp_node__fw_mvnorm_posterior__2

!> mvnorm_posterior__2 - backward difference
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__bw_mvnorm_posterior__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
   associate( &
       e11 => xp_NUMBERS_(nd%in(1)), &
       e21 => xp_NUMBERS_(nd%in(2)), &
       e22 => xp_NUMBERS_(nd%in(3)), &
       y11 => xp_NUMBERS_(nd%in(4)), &
       mu11 => xp_NUMBERS_(nd%in(5)), &
       pmu => xp_NUMBERS_(nd%out(1)), &
       pe => xp_NUMBERS_(nd%out(2)) &
    )
    call bw_mvnorm_posterior__2(e11, e21, e22, y11, mu11, pmu, pe)
  end associate
end subroutine xp_node__bw_mvnorm_posterior__2

!> @}
