!> @defgroup node_operators_reductions_ Reduction Operators
!! @{

!> Sum - operator
!! @param[in] nd 'node'
subroutine xp_node__op_sum__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_sum(x1, x2)
  end associate
end subroutine xp_node__op_sum__1

!> Sum - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_sum__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_sum(x1, x2)
  end associate
end subroutine xp_node__fw_sum__1

!> Sum - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_sum__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_sum(x1, x2)
  end associate
end subroutine xp_node__bw_sum__1

!> @todo document
subroutine xp_node__op_sum__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_sum(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__op_sum__2

!> @todo document
subroutine xp_node__fw_sum__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_sum(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__fw_sum__2

!> @todo document
subroutine xp_node__bw_sum__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_sum(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__bw_sum__2

!> @todo document
subroutine xp_node__op_product__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_product(x1, x2)
  end associate
end subroutine xp_node__op_product__1

!> @todo document
subroutine xp_node__fw_product__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_product(x1, x2)
  end associate
end subroutine xp_node__fw_product__1

!> @todo document
subroutine xp_node__bw_product__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_product(x1, x2)
  end associate
end subroutine xp_node__bw_product__1

!> @todo document
subroutine xp_node__op_product__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_product(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__op_product__2

!> @todo document
subroutine xp_node__fw_product__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_product(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__fw_product__2

!> @todo document
subroutine xp_node__bw_product__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_product(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__bw_product__2

!> Sum of squares - operator
!! @param[in] nd 'node'
subroutine xp_node__op_ssq__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_ssq(x1, x2)
  end associate
end subroutine xp_node__op_ssq__1

!> Sum of squares - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_ssq__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_ssq(x1, x2)
  end associate
end subroutine xp_node__fw_ssq__1

!> Sum of squares - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_ssq__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_ssq(x1, x2)
  end associate
end subroutine xp_node__bw_ssq__1

!> @todo document
subroutine xp_node__op_ssq__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_ssq(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__op_ssq__2

!> @todo document
subroutine xp_node__fw_ssq__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_ssq(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__fw_ssq__2

!> @todo document
subroutine xp_node__bw_ssq__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_ssq(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__bw_ssq__2

!> @}
