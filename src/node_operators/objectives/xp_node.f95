!> @defgroup node_operators__objectives_ Objective functions
!! @{

!> Binary entropy - operator
!! @param[in] nd 'node'
subroutine xp_node__op_binentropy (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call op_binentropy(y, yh, j)
  end associate
end subroutine xp_node__op_binentropy

!> Binary entripy - forward difference
!! @param[in] nd 'node'
subroutine xp_node__fw_binentropy (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_binentropy(y, yh, j)
  end associate
end subroutine xp_node__fw_binentropy

!> Binary entropy - backward difference
!! @param[in] nd 'node'
subroutine xp_node__bw_binentropy (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_binentropy(y, yh, j)
  end associate
end subroutine xp_node__bw_binentropy

!> @todo document
subroutine xp_node__op_logit_binentropy (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call op_logit_binentropy(y, yh, j)
  end associate
end subroutine xp_node__op_logit_binentropy

!> @todo document
subroutine xp_node__fw_logit_binentropy (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_logit_binentropy(y, yh, j)
  end associate
end subroutine xp_node__fw_logit_binentropy


!> @todo document
subroutine xp_node__bw_logit_binentropy (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_logit_binentropy(y, yh, j)
  end associate
end subroutine xp_node__bw_logit_binentropy

!> Standard cross-entropy - operator
!! @param[in] nd 'node'
subroutine xp_node__op_crossentropy (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call op_crossentropy(y, yh, j)
  end associate
end subroutine xp_node__op_crossentropy

!> Standard cross-entropy - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_crossentropy (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_crossentropy(y, yh, j)
  end associate
end subroutine xp_node__fw_crossentropy

!> Standard cross-entropy - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_crossentropy (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_crossentropy(y, yh, j)
  end associate
end subroutine xp_node__bw_crossentropy

!> @todo document
subroutine xp_node__op_logit_crossentropy__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call op_logit_crossentropy(y, yh, j)
  end associate
end subroutine xp_node__op_logit_crossentropy__1

!> @todo document
subroutine xp_node__fw_logit_crossentropy__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_logit_crossentropy(y, yh, j)
  end associate
end subroutine xp_node__fw_logit_crossentropy__1

!> @todo document
subroutine xp_node__bw_logit_crossentropy__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_logit_crossentropy(y, yh, j)
  end associate
end subroutine xp_node__bw_logit_crossentropy__1

!> @todo document
subroutine xp_node__op_logit_crossentropy__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call op_logit_crossentropy(y, yh, j, nd%ipar)
  end associate
end subroutine xp_node__op_logit_crossentropy__2

!> @todo document
subroutine xp_node__fw_logit_crossentropy__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_logit_crossentropy(y, yh, j, nd%ipar)
  end associate
end subroutine xp_node__fw_logit_crossentropy__2

!> @todo document
subroutine xp_node__bw_logit_crossentropy__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_logit_crossentropy(y, yh, j, nd%ipar)
  end associate
end subroutine xp_node__bw_logit_crossentropy__2

!> MSE - operator
!! @param[in] nd 'node'
subroutine xp_node__op_mse (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call op_mse(y, yh, j)
  end associate
end subroutine xp_node__op_mse

!> MSE - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_mse (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_mse(y, yh, j)
  end associate
end subroutine xp_node__fw_mse

!> MSE - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_mse (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_mse(y, yh, j)
  end associate
end subroutine xp_node__bw_mse

!> MAE - operator
!! @param[in] nd 'node'
subroutine xp_node__op_mae (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call op_mae(y, yh, j)
  end associate
end subroutine xp_node__op_mae

!> MAE - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_mae (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_mae(y, yh, j)
  end associate
end subroutine xp_node__fw_mae

!> MAE - bakward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_mae (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       yh => xp_NUMBERS_(nd%in(2)), &
       j => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_mae(y, yh, j)
  end associate
end subroutine xp_node__bw_mae

!> lkhnorm__1 - operator
!! @param[in] nd 'node'
subroutine xp_node__op_lkhnorm__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       mu => xp_NUMBERS_(nd%in(2)), &
       s => xp_NUMBERS_(nd%in(3)), &
       L => xp_NUMBERS_(nd%out(1)) &
       )
    call op_lkhnorm(y, mu, s, L)
  end associate
end subroutine xp_node__op_lkhnorm__1

!> lkhnorm__1 - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_lkhnorm__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       mu => xp_NUMBERS_(nd%in(2)), &
       s => xp_NUMBERS_(nd%in(3)), &
       L => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_lkhnorm(y, mu, s, L)
  end associate
end subroutine xp_node__fw_lkhnorm__1

!> lkhnorm__1 - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_lkhnorm__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), &
       mu => xp_NUMBERS_(nd%in(2)), &
       s => xp_NUMBERS_(nd%in(3)), &
       L => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_lkhnorm(y, mu, s, L)
  end associate
end subroutine xp_node__bw_lkhnorm__1

!> lkhnorm__2 - operator
!! @param[in] nd 'node'
subroutine xp_node__op_lkhnorm__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), & 
       mu => xp_NUMBERS_(nd%in(2)), &
       s => xp_NUMBERS_(nd%in(3)), &
       w => xp_NUMBERS_(nd%in(4)), &
       L => xp_NUMBERS_(nd%out(1)) &
       )
    call op_lkhnorm(y, mu, s, w, L)
  end associate
end subroutine xp_node__op_lkhnorm__2

!> lkhnorm__2 - forward differentiation
!! @param[in] nd 'node
subroutine xp_node__fw_lkhnorm__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), & 
       mu => xp_NUMBERS_(nd%in(2)), &
       s => xp_NUMBERS_(nd%in(3)), &
       w => xp_NUMBERS_(nd%in(4)), &
       L => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_lkhnorm(y, mu, s, w, L)
  end associate
end subroutine xp_node__fw_lkhnorm__2

!> lkhnorm__2 - backward differentiation
!! @param[in] nd 'node
subroutine xp_node__bw_lkhnorm__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       y => xp_NUMBERS_(nd%in(1)), & 
       mu => xp_NUMBERS_(nd%in(2)), &
       s => xp_NUMBERS_(nd%in(3)), &
       w => xp_NUMBERS_(nd%in(4)), &
       L => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_lkhnorm(y, mu, s, w, L)
  end associate
end subroutine xp_node__bw_lkhnorm__2

!> @}
