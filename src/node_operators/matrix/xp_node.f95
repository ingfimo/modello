!> @defgroup node_operators__matrix_ Matrix
!! @{

!> Gmmmult 1 - operator
!! @param[in] nd 'node'
subroutine xp_node__op_gmmmult__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       alpha => xp_NUMBERS_(nd%in(1)), &
       A => xp_NUMBERS_(nd%in(2)), &
       B => xp_NUMBERS_(nd%in(3)), &
       beta => xp_NUMBERS_(nd%in(4)), &
       C => xp_NUMBERS_(nd%in(5)), &
       CC => xp_NUMBERS_(nd%out(1)) &
       )
    call op_gmmmult(nd%ipar(1), nd%ipar(2), alpha, A, B, beta, C, CC)
  end associate
end subroutine xp_node__op_gmmmult__1

!> Gmmmult 1 - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_gmmmult__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       alpha => xp_NUMBERS_(nd%in(1)), &
       A => xp_NUMBERS_(nd%in(2)), &
       B => xp_NUMBERS_(nd%in(3)), &
       beta => xp_NUMBERS_(nd%in(4)), &
       C => xp_NUMBERS_(nd%in(5)), &
       CC => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_gmmmult(nd%ipar(1), nd%ipar(2), alpha, A, B, beta, C, CC)
  end associate
end subroutine xp_node__fw_gmmmult__1

!> Gmmmult 1 - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_gmmmult__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       alpha => xp_NUMBERS_(nd%in(1)), &
       A => xp_NUMBERS_(nd%in(2)), &
       B => xp_NUMBERS_(nd%in(3)), &
       beta => xp_NUMBERS_(nd%in(4)), &
       C => xp_NUMBERS_(nd%in(5)), &
       CC => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_gmmmult(nd%ipar(1), nd%ipar(2), alpha, A, B, beta, C, CC)
  end associate
end subroutine xp_node__bw_gmmmult__1  

!> Gmmmult 2 - operator
!! @param[in] nd 'node'
subroutine xp_node__op_gmmmult__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       alpha => xp_NUMBERS_(nd%in(1)), &
       A => xp_NUMBERS_(nd%in(2)), &
       B => xp_NUMBERS_(nd%in(3)), &
       C => xp_NUMBERS_(nd%out(1))&
       )
    call op_gmmmult(alpha, nd%ipar(1), nd%ipar(2), A, B, C)
  end associate
end subroutine xp_node__op_gmmmult__2

!> Gmmmult 2 - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_gmmmult__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       alpha => xp_NUMBERS_(nd%in(1)), &
       A => xp_NUMBERS_(nd%in(2)), &
       B => xp_NUMBERS_(nd%in(3)), &
       C => xp_NUMBERS_(nd%out(1))&
       )
    call fw_gmmmult(alpha, nd%ipar(1), nd%ipar(2), A, B, C)
  end associate
end subroutine xp_node__fw_gmmmult__2

!> Gmmmult 2 - backaward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_gmmmult__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       alpha => xp_NUMBERS_(nd%in(1)), &
       A => xp_NUMBERS_(nd%in(2)), &
       B => xp_NUMBERS_(nd%in(3)), &
       C => xp_NUMBERS_(nd%out(1))&
       )
    call bw_gmmmult(alpha, nd%ipar(1), nd%ipar(2), A, B, C)
  end associate
end subroutine xp_node__bw_gmmmult__2

!> Gmmmult 3 - operator
!! @param[in] nd 'node'
subroutine xp_node__op_gmmmult__3 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       A => xp_NUMBERS_(nd%in(1)), &
       B => xp_NUMBERS_(nd%in(2)), &
       C => xp_NUMBERS_(nd%in(3)), &
       CC => xp_NUMBERS_(nd%out(1)) &
       )
    call op_gmmmult(nd%ipar(1), nd%ipar(2), A, B, C, CC)
  end associate
end subroutine xp_node__op_gmmmult__3

!> Gmmmult 3 - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_gmmmult__3 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       A => xp_NUMBERS_(nd%in(1)), &
       B => xp_NUMBERS_(nd%in(2)), &
       C => xp_NUMBERS_(nd%in(3)), &
       CC => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_gmmmult(nd%ipar(1), nd%ipar(2), A, B, C, CC)
  end associate
end subroutine xp_node__fw_gmmmult__3

!> Gmmmult 3 - backaward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_gmmmult__3 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       A => xp_NUMBERS_(nd%in(1)), &
       B => xp_NUMBERS_(nd%in(2)), &
       C => xp_NUMBERS_(nd%in(3)), &
       CC => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_gmmmult(nd%ipar(1), nd%ipar(2), A, B, C, CC)
  end associate
end subroutine xp_node__bw_gmmmult__3

!> Gmmmult 4 - operator
!! @param[in] nd 'node'
subroutine xp_node__op_gmmmult__4 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       A => xp_NUMBERS_(nd%in(1)), &
       B => xp_NUMBERS_(nd%in(2)), &
       C => xp_NUMBERS_(nd%out(1)) &
       )
    call op_gmmmult(nd%ipar(1), nd%ipar(2), A, B, C)
  end associate
end subroutine xp_node__op_gmmmult__4

!> Gmmmult 4 - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_gmmmult__4 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       A => xp_NUMBERS_(nd%in(1)), &
       B => xp_NUMBERS_(nd%in(2)), &
       C => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_gmmmult(nd%ipar(1), nd%ipar(2), A, B, C)
  end associate
end subroutine xp_node__fw_gmmmult__4

!> Gmmmult 4 - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_gmmmult__4 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       A => xp_NUMBERS_(nd%in(1)), &
       B => xp_NUMBERS_(nd%in(2)), &
       C => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_gmmmult(nd%ipar(1), nd%ipar(2), A, B, C)
  end associate
end subroutine xp_node__bw_gmmmult__4

!> Gmvmult 1 - operator
!! @param[in] nd 'node'
subroutine xp_node__op_gmvmult__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       alpha => xp_NUMBERS_(nd%in(1)), &
       x => xp_NUMBERS_(nd%in(2)), &
       A => xp_NUMBERS_(nd%in(3)), &
       beta => xp_NUMBERS_(nd%in(4)), &
       y => xp_NUMBERS_(nd%in(5)), &
       yy => xp_NUMBERS_(nd%out(1)) &
       )
    call op_gmvmult(nd%ipar(1), alpha, x, A, beta, y, yy)
  end associate
end subroutine xp_node__op_gmvmult__1

!> Gmvmult 1 - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_gmvmult__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       alpha => xp_NUMBERS_(nd%in(1)), &
       x => xp_NUMBERS_(nd%in(2)), &
       A => xp_NUMBERS_(nd%in(3)), &
       beta => xp_NUMBERS_(nd%in(4)), &
       y => xp_NUMBERS_(nd%in(5)), &
       yy => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_gmvmult(nd%ipar(1), alpha, x, A, beta, y, yy)
  end associate
end subroutine xp_node__fw_gmvmult__1

!> Gmvmult 1 - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_gmvmult__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       alpha => xp_NUMBERS_(nd%in(1)), &
       x => xp_NUMBERS_(nd%in(2)), &
       A => xp_NUMBERS_(nd%in(3)), &
       beta => xp_NUMBERS_(nd%in(4)), &
       y => xp_NUMBERS_(nd%in(5)), &
       yy => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_gmvmult(nd%ipar(1), alpha, x, A, beta, y, yy)
  end associate
end subroutine xp_node__bw_gmvmult__1

!> Gmvmult 2 - operator
!! @param[in] nd 'node'
subroutine xp_node__op_gmvmult__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       alpha => xp_NUMBERS_(nd%in(1)), &
       x => xp_NUMBERS_(nd%in(2)), &
       A => xp_NUMBERS_(nd%in(3)), &
       y => xp_NUMBERS_(nd%out(1)) &
       )
    call op_gmvmult(alpha, nd%ipar(1), x, A, y)
  end associate
end subroutine xp_node__op_gmvmult__2

!> Gmvmult 2 - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_gmvmult__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       alpha => xp_NUMBERS_(nd%in(1)), &
       x => xp_NUMBERS_(nd%in(2)), &
       A => xp_NUMBERS_(nd%in(3)), &
       y => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_gmvmult(alpha, nd%ipar(1), x, A, y)
  end associate
end subroutine xp_node__fw_gmvmult__2

!> Gmvmult 2 - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_gmvmult__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       alpha => xp_NUMBERS_(nd%in(1)), &
       x => xp_NUMBERS_(nd%in(2)), &
       A => xp_NUMBERS_(nd%in(3)), &
       y => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_gmvmult(alpha, nd%ipar(1), x, A, y)
  end associate
end subroutine xp_node__bw_gmvmult__2

!> Gmvmult 3 - operator
!! @param[in] nd 'node'
subroutine xp_node__op_gmvmult__3 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x => xp_NUMBERS_(nd%in(1)), &
       A => xp_NUMBERS_(nd%in(2)), &
       y => xp_NUMBERS_(nd%in(3)), &
       yy => xp_NUMBERS_(nd%out(1)) &
       )
    call op_gmvmult(nd%ipar(1), x, A, y, yy)
  end associate
end subroutine xp_node__op_gmvmult__3

!> Gmvmult 3 - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_gmvmult__3 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x => xp_NUMBERS_(nd%in(1)), &
       A => xp_NUMBERS_(nd%in(2)), &
       y => xp_NUMBERS_(nd%in(3)), &
       yy => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_gmvmult(nd%ipar(1), x, A, y, yy)
  end associate
end subroutine xp_node__fw_gmvmult__3

!> Gmvmult 3 - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_gmvmult__3 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x => xp_NUMBERS_(nd%in(1)), &
       A => xp_NUMBERS_(nd%in(2)), &
       y => xp_NUMBERS_(nd%in(3)), &
       yy => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_gmvmult(nd%ipar(1), x, A, y, yy)
  end associate
end subroutine xp_node__bw_gmvmult__3

!> Gmvmult 4 - operator
!! @param[in] nd 'node'
subroutine xp_node__op_gmvmult__4 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x => xp_NUMBERS_(nd%in(1)), &
       A => xp_NUMBERS_(nd%in(2)), &
       y => xp_NUMBERS_(nd%out(1)) &
       )
    call op_gmvmult(nd%ipar(1), x, A, y)
  end associate
end subroutine xp_node__op_gmvmult__4

!> Gmvmult 4 - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_gmvmult__4 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x => xp_NUMBERS_(nd%in(1)), &
       A => xp_NUMBERS_(nd%in(2)), &
       y => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_gmvmult(nd%ipar(1), x, A, y)
  end associate
end subroutine xp_node__fw_gmvmult__4

!> Gmvmult 4 - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_gmvmult__4 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x => xp_NUMBERS_(nd%in(1)), &
       A => xp_NUMBERS_(nd%in(2)), &
       y => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_gmvmult(nd%ipar(1), x, A, y)
  end associate
end subroutine xp_node__bw_gmvmult__4

!> Ger 1 - operator
!! @param[in] nd 'node'
subroutine xp_node__op_vvouter__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       alpha => xp_NUMBERS_(nd%in(1)), &
       x => xp_NUMBERS_(nd%in(2)), &
       y => xp_NUMBERS_(nd%in(3)), &
       z => xp_NUMBERS_(nd%in(4)), &
       A => xp_NUMBERS_(nd%out(1)) &
       )
    call op_vvouter(alpha, x, y, z, A)
  end associate
end subroutine xp_node__op_vvouter__1

!> @todo document
subroutine xp_node__fw_vvouter__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       alpha => xp_NUMBERS_(nd%in(1)), &
       x => xp_NUMBERS_(nd%in(2)), &
       y => xp_NUMBERS_(nd%in(3)), &
       z => xp_NUMBERS_(nd%in(4)), &
       A => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_vvouter(alpha, x, y, z, A)
  end associate
end subroutine xp_node__fw_vvouter__1

!> Ger 1 - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_vvouter__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       alpha => xp_NUMBERS_(nd%in(1)), &
       x => xp_NUMBERS_(nd%in(2)), &
       y => xp_NUMBERS_(nd%in(3)), &
       z => xp_NUMBERS_(nd%in(4)), &
       A => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_vvouter(alpha, x, y, z, A)
  end associate
end subroutine xp_node__bw_vvouter__1

!> Ger 2 - operator
!! @param[in] nd 'node'
subroutine xp_node__op_vvouter__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x => xp_NUMBERS_(nd%in(1)), &
       y => xp_NUMBERS_(nd%in(2)), &
       z => xp_NUMBERS_(nd%in(3)), &
       A => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_vvouter(x, y, z, A)
  end associate
end subroutine xp_node__op_vvouter__2

!> @todo document
subroutine xp_node__fw_vvouter__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x => xp_NUMBERS_(nd%in(1)), &
       y => xp_NUMBERS_(nd%in(2)), &
       z => xp_NUMBERS_(nd%in(3)), &
       A => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_vvouter(x, y, z, A)
  end associate
end subroutine xp_node__fw_vvouter__2

!> Ger 2 - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_vvouter__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x => xp_NUMBERS_(nd%in(1)), &
       y => xp_NUMBERS_(nd%in(2)), &
       z => xp_NUMBERS_(nd%in(3)), &
       A => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_vvouter(x, y, z, A)
  end associate
end subroutine xp_node__bw_vvouter__2

!> Ger 3 - operator
!! @param[in] nd 'node'
subroutine xp_node__op_vvouter__3 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x => xp_NUMBERS_(nd%in(1)), &
       y => xp_NUMBERS_(nd%in(2)), &
       A => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_vvouter(x, y, A)
  end associate
end subroutine xp_node__op_vvouter__3

!> @todo document
subroutine xp_node__fw_vvouter__3 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x => xp_NUMBERS_(nd%in(1)), &
       y => xp_NUMBERS_(nd%in(2)), &
       A => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_vvouter(x, y, A)
  end associate
end subroutine xp_node__fw_vvouter__3

!> Ger 3 - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_vvouter__3 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x => xp_NUMBERS_(nd%in(1)), &
       y => xp_NUMBERS_(nd%in(2)), &
       A => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_vvouter(x, y, A)
  end associate
end subroutine xp_node__bw_vvouter__3

!> Dot - operator
!! @param[in] nd 'node'
subroutine xp_node__op_vvinner (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x => xp_NUMBERS_(nd%in(1)), &
       y => xp_NUMBERS_(nd%in(2)), &
       z => xp_NUMBERS_(nd%out(1)) &
       )
    call op_vvinner(x, y, z)
  end associate
end subroutine xp_node__op_vvinner

!> Dot - forward differentiation
!! PLACE HOLDER
!! @param[in] nd 'node'
subroutine xp_node__fw_vvinner (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x => xp_NUMBERS_(nd%in(1)), &
       y => xp_NUMBERS_(nd%in(2)), &
       z => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_vvinner(x, y, z)
  end associate
end subroutine xp_node__fw_vvinner

!> Dot - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_vvinner (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x => xp_NUMBERS_(nd%in(1)), &
       y => xp_NUMBERS_(nd%in(2)), &
       z => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_vvinner(x, y, z)
  end associate
end subroutine xp_node__bw_vvinner

!> General matrix inversion - operator
!! @param[in] nd 'node'
subroutine xp_node__op_invMat (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  call do_within('xp_node__op_invMat', mod_node_operators_name_, private_op)
contains
  subroutine private_op
    associate( &
         A => xp_NUMBERS_(nd%in(1)), &
         B => xp_NUMBERS_(nd%out(1)) &
         )
      call op_invMat(A, B)
    end associate
  end subroutine private_op
end subroutine xp_node__op_invMat

!> General matrix inversion - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_invMat (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  call do_within('xp_node__op_invMat', mod_node_operators_name_, private_fw)
contains
  subroutine private_fw
    associate( &
         A => xp_NUMBERS_(nd%in(1)), &
         B => xp_NUMBERS_(nd%out(1)) &
         )
      call fw_invMat(A, B)
    end associate
  end subroutine private_fw
end subroutine xp_node__fw_invMat

!> General matrix inversion - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_invMat (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  call do_within('xp_node__op_degmmmult0', mod_node_operators_name_, private_bw)
contains
  subroutine private_bw
    associate( &
         A => xp_NUMBERS_(nd%in(1)), &
         B => xp_NUMBERS_(nd%out(1)) &
         )
      call bw_invMat(A, B)
    end associate
  end subroutine private_bw
end subroutine xp_node__bw_invMat

subroutine xp_node__op_slidemmm (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate ( &
       A => xp_NUMBERS_(nd%in(1)), &
       B => xp_NUMBERS_(nd%in(2)), &
       C => xp_NUMBERS_(nd%out(1)) &
    )
    call op_slidemmm(A, B, C, nd%ipar)
  end associate
end subroutine xp_node__op_slidemmm

subroutine xp_node__fw_slidemmm (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate ( &
       A => xp_NUMBERS_(nd%in(1)), &
       B => xp_NUMBERS_(nd%in(2)), &
       C => xp_NUMBERS_(nd%out(1)) &
    )
    call fw_slidemmm(A, B, C, nd%ipar)
  end associate
end subroutine xp_node__fw_slidemmm

subroutine xp_node__bw_slidemmm (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate ( &
       A => xp_NUMBERS_(nd%in(1)), &
       B => xp_NUMBERS_(nd%in(2)), &
       C => xp_NUMBERS_(nd%out(1)) &
    )
    call bw_slidemmm(A, B, C, nd%ipar)
  end associate
end subroutine xp_node__bw_slidemmm
!> @}  
