!> @relates node__bw
subroutine xp_node__bw (nd)
  implicit none
  type(xp_node), intent(inout) :: nd
  select case (nd%attrs%opid)
     ! Modifiers
  case (op_assign_id_)
     nd%bw => xp_node__bw_assign
  case (op_slice_id_)
     nd%bw => xp_node__bw_slice
  case (op_flatslice_id_)
     nd%bw => xp_node__bw_flatslice
  case (op_contiguous_slice_id_)
     nd%bw => xp_node__op_null
  case (op_reshape_id_)
     nd%bw => xp_node__op_null
  case (op_drop_shape_id_)
     nd%bw => xp_node__op_null
  case (op_bind_id_)
     nd%bw => xp_node__bw_bind
  case (op_embeddings_id_)
     nd%bw => xp_node__bw_embeddings
  case (op_transpose_id_)
     nd%bw => xp_node__bw_transpose
  case (op_feeding_number_id_)
     nd%bw => xp_node__op_null
  case (op_feeding_sequence_id_)
     nd%bw => xp_node__op_null
  case (op_feed_id_)
     nd%bw => xp_node__op_null
     ! Unary
  case (op_neg_id_)
     nd%bw => xp_node__bw_neg
  case (op_abs_id_)
     nd%bw => xp_node__bw_abs
  case (op_expon_id_)
     nd%bw => xp_node__bw_exp
  case (op_log_id_)
     nd%bw => xp_node__bw_log
  case (op_sin_id_)
     nd%bw => xp_node__bw_sin
  case (op_cos_id_)
     nd%bw => xp_node__bw_cos
  case (op_tan_id_)
     nd%bw => xp_node__bw_tan
  case (op_sinh_id_)
     nd%bw => xp_node__bw_sinh
  case (op_cosh_id_)
     nd%bw => xp_node__bw_cosh
  case (op_tanh_id_)
     nd%bw => xp_node__bw_tanh
     ! ---
     ! Binary
  case (op_add1_id_)
     nd%bw => xp_node__bw_add__1
  case (op_add2_id_)
     nd%bw => xp_node__bw_add__2
  case (op_sub1_id_)
     nd%bw => xp_node__bw_sub__1
  case (op_sub2_id_)
     nd%bw => xp_node__bw_sub__2
  case (op_mult1_id_)
     nd%bw => xp_node__bw_mult__1
  case (op_mult2_id_)
     nd%bw => xp_node__bw_mult__2
  case (op_pow1_id_)
     nd%bw => xp_node__bw_pow__1
  case (op_pow2_id_)
     nd%bw => xp_node__bw_pow__2
  case (op_div1_id_)
     nd%bw => xp_node__bw_div__1
  case (op_div2_id_)
     nd%bw => xp_node__bw_div__2
     ! ---
     ! Activations
  case (op_sigmoid_id_)
     nd%bw => xp_node__bw_sigmoid
  case (op_softplus_id_)
     nd%bw => xp_node__bw_softplus
  case (op_relu_id_)
     nd%bw => xp_node__bw_relu
  case (op_leakyrelu_id_)
     nd%bw => xp_node__bw_leakyrelu
  case (op_elu_id_)
     nd%bw => xp_node__bw_elu
  case (op_silu_id_)
     nd%bw => xp_node__bw_silu
  case (op_swish_id_)
     nd%bw => xp_node__bw_swish
  case (op_softmax1_id_)
     nd%bw => xp_node__bw_softmax__1
  case (op_softmax2_id_)
     nd%bw => xp_node__bw_softmax__2
     ! ---
     ! Matrix
  case (op_gmmmult1_id_)
     nd%bw => xp_node__bw_gmmmult__1
  case (op_gmmmult2_id_)
     nd%bw => xp_node__bw_gmmmult__2
  case (op_gmmmult3_id_)
     nd%bw => xp_node__bw_gmmmult__3
  case (op_gmmmult4_id_)
     nd%bw => xp_node__bw_gmmmult__4
  case (op_gmvmult1_id_)
     nd%bw => xp_node__bw_gmvmult__1
  case (op_gmvmult2_id_)
     nd%bw => xp_node__bw_gmvmult__2
  case (op_gmvmult3_id_)
     nd%bw => xp_node__bw_gmvmult__3
  case (op_gmvmult4_id_)
     nd%bw => xp_node__bw_gmvmult__4
  case (op_vvouter1_id_)
     nd%bw => xp_node__bw_vvouter__1
  case (op_vvouter2_id_)
     nd%bw => xp_node__bw_vvouter__2
  case (op_vvouter3_id_)
     nd%bw => xp_node__bw_vvouter__3
  case (op_vvinner_id_)
     nd%bw => xp_node__bw_vvinner 
  case (op_invMat_id_)
     nd%bw => xp_node__bw_invMat
  case (op_slidemmm_id_)
     nd%bw => xp_node__bw_slidemmm
     ! ---
     ! Kernels
  case (op_ksqexpon_id_)
     nd%bw => xp_node__bw_ksqexp
     ! ---
     ! Reductions
  case (op_sum1_id_)
     nd%bw => xp_node__bw_sum__1
  case (op_sum2_id_)
     nd%bw => xp_node__bw_sum__2
  case (op_product1_id_)
     nd%bw => xp_node__bw_product__1
  case (op_product2_id_)
     nd%bw => xp_node__bw_product__2
  case (op_ssq1_id_)
     nd%bw => xp_node__bw_ssq__1
  case (op_ssq2_id_)
     nd%bw => xp_node__bw_ssq__2
     ! ---
     ! Stats
  case (op_ldexpon_id_)
     nd%bw => xp_node__bw_ldexp
  case (op_ldlaplace_id_)
     nd%bw => xp_node__bw_ldlaplace
  case (op_ldbeta_id_)
     nd%bw => xp_node__bw_ldbeta
  case (op_ldgamma_id_)
     nd%bw => xp_node__bw_ldgamma
  case (op_ldnorm_id_)
     nd%bw => xp_node__bw_ldnorm
  case (op_ldmvnorm1_id_)
     nd%bw => xp_node__bw_ldmvnorm__1
  case (op_mvnormpost1_id_)
     nd%bw => xp_node__bw_mvnorm_posterior__1
  case (op_mvnormpost2_id_)
     nd%bw => xp_node__bw_mvnorm_posterior__2
     ! ---
     ! Objectives
  case (op_binentropy_id_)
     nd%bw => xp_node__bw_binentropy
  case (op_logit_binentropy_id_)
     nd%bw => xp_node__bw_logit_binentropy
  case (op_crossentropy_id_)
     nd%bw => xp_node__bw_crossentropy
  case (op_logit_crossentropy1_id_)
     nd%bw => xp_node__bw_logit_crossentropy__1
  case (op_logit_crossentropy2_id_)
     nd%op => xp_node__bw_logit_crossentropy__2
  case (op_mse_id_)
     nd%bw => xp_node__bw_mse
  case (op_mae_id_)
     nd%bw => xp_node__bw_mae
  case (op_lkhnorm1_id_)
     nd%bw => xp_node__bw_lkhnorm__1
  case (op_lkhnorm2_id_)
     nd%bw => xp_node__bw_lkhnorm__2
     ! ---
     ! Regularisation
  case (op_dropout_id_)
     nd%bw => xp_node__bw_dropout
     ! ---
     ! Convolution
  case (op_conv_id_)
     nd%bw => xp_node__bw_conv
  case (op_maxpool_id_)
     nd%bw => xp_node__bw_maxpool
  case default
     call raise_error("opid", err_unknwnVal_)
     !call raise_critical_error(mod_node_operators_name_, "xp_node__bw", "unknown node operator")
  end select
end subroutine xp_node__bw
