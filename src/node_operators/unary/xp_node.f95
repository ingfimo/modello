!> @defgroup node_operators__unary_ Unary Operators
!! @{

!> @todo document
subroutine xp_node__op_neg (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_neg(x1, x2)
  end associate
end subroutine xp_node__op_neg

!> @todo document
subroutine xp_node__fw_neg (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_neg(x1, x2)
  end associate
end subroutine xp_node__fw_neg

!> @todo document
subroutine xp_node__bw_neg (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_neg(x1, x2)
  end associate
end subroutine xp_node__bw_neg

!> Abs - operator
!! @param[in] nd 'node'
subroutine xp_node__op_abs (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_abs(x1, x2)
  end associate
end subroutine xp_node__op_abs

!> Abs - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_abs (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_abs(x1, x2)
  end associate
end subroutine xp_node__fw_abs

!> Abs - backward differenatiation
!! @param[in] nd 'node'
subroutine xp_node__bw_abs (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_abs(x1, x2)
  end associate
end subroutine xp_node__bw_abs

!> Exponential - operator
!! @param[in] nd 'node'
subroutine xp_node__op_exp (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_exp(x1, x2)
  end associate
end subroutine xp_node__op_exp

!> Exponential - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_exp (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_exp(x1, x2)
  end associate
end subroutine xp_node__fw_exp

!> Exponential - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_exp (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_exp(x1, x2)
  end associate
end subroutine xp_node__bw_exp

!> Logarithm - operator
!! @param[in] nd 'node'
subroutine xp_node__op_log (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_log(x1, x2)
  end associate
end subroutine xp_node__op_log

!> Logarithm - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_log (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_log(x1, x2)
  end associate
end subroutine xp_node__fw_log

!> Logarithm - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_log (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_log(x1, x2)
  end associate
end subroutine xp_node__bw_log

!> Sine - operator
!! @param[in] nd 'node'
subroutine xp_node__op_sin (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_sin(x1, x2)
  end associate
end subroutine xp_node__op_sin

!> Sine - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_sin (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_sin(x1, x2)
  end associate
end subroutine xp_node__fw_sin

!> Sine - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_sin (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_sin(x1, x2)
  end associate
end subroutine xp_node__bw_sin

!> Cosine - operator
!! @param[in] nd 'node'
subroutine xp_node__op_cos (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_cos(x1, x2)
  end associate
end subroutine xp_node__op_cos

!> Cosine - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_cos (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_cos(x1, x2)
  end associate
end subroutine xp_node__fw_cos

!> Cosine backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_cos (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_cos(x1, x2)
  end associate
end subroutine xp_node__bw_cos

!> Tangent - operator
!! @param[in] nd 'node'
subroutine xp_node__op_tan (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_tan(x1, x2)
  end associate
end subroutine xp_node__op_tan

!> Tangent - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_tan (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_tan(x1, x2)
  end associate
end subroutine xp_node__fw_tan

!> Tangent - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_tan (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_tan(x1, x2)
  end associate
end subroutine xp_node__bw_tan

!> Hyperbolic Sine -  operator
!! @param[in] nd 'node'
subroutine xp_node__op_sinh (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_sinh(x1, x2)
  end associate
end subroutine xp_node__op_sinh

!> Hyperbolic Sine - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_sinh (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_sinh(x1, x2)
  end associate
end subroutine xp_node__fw_sinh

!> Hyperbolic Sine - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_sinh (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_sinh(x1, x2)
  end associate
end subroutine xp_node__bw_sinh

!> Hyperbolic Cosine - operator
!! @param[in] nd 'node'
subroutine xp_node__op_cosh (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1))&
       )
    call op_cosh(x1, x2)
  end associate
end subroutine xp_node__op_cosh

!> Hyperbolic Cosine - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_cosh (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_cosh(x1, x2)
  end associate
end subroutine xp_node__fw_cosh

!> Hyperbolic Cosine - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_cosh (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_cosh(x1, x2)
  end associate
end subroutine xp_node__bw_cosh

!> Hyperbolic Tangent - operator
!! @param[in] nd 'node'
subroutine xp_node__op_tanh (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_tanh(x1, x2)
  end associate
end subroutine xp_node__op_tanh

!> Hyperbolic Tangent - forward differentiation
!! @param[in] nd 'node'
subroutine xp_node__fw_tanh (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_tanh(x1, x2)
  end associate
end subroutine xp_node__fw_tanh

!> Hyperbolic Tangent - backward differentiation
!! @param[in] nd 'node'
subroutine xp_node__bw_tanh (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_tanh(x1, x2)
  end associate
end subroutine xp_node__bw_tanh

!> @}
