subroutine xp_node__op_conv (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       k => xp_NUMBERS_(nd%in(2)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_conv(x1, k, x2, nd%ipar)
  end associate
end subroutine xp_node__op_conv

subroutine xp_node__fw_conv (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       k => xp_NUMBERS_(nd%in(2)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_conv(x1, k, x2, nd%ipar)
  end associate
end subroutine xp_node__fw_conv

subroutine xp_node__bw_conv (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       k => xp_NUMBERS_(nd%in(2)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_conv(x1, k, x2, nd%ipar)
  end associate
end subroutine xp_node__bw_conv

subroutine xp_node__op_maxpool (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_maxpool(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__op_maxpool

subroutine xp_node__fw_maxpool (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_maxpool(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__fw_maxpool

subroutine xp_node__bw_maxpool (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_maxpool(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__bw_maxpool
    

    

