!> @defgroup nod_operators__binary_ Binary Operators
!! @{


!> Addition - operator
!! @param[in] nd `nodeattrs`
subroutine xp_node__op_add__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_add(x1, x2, x3)
  end associate
end subroutine xp_node__op_add__1

!> Addition - forward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__fw_add__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_add(x1, x2, x3)
  end associate
end subroutine xp_node__fw_add__1

!> Addition - backward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__bw_add__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_add(x1, x2, x3)
  end associate
end subroutine xp_node__bw_add__1

!> @todo document
subroutine xp_node__op_add__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_add(x1, x2, x3, nd%ipar)
  end associate
end subroutine xp_node__op_add__2

!> Addition - forward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__fw_add__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_add(x1, x2, x3, nd%ipar)
  end associate
end subroutine xp_node__fw_add__2

!> Addition - backward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__bw_add__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_add(x1, x2, x3, nd%ipar)
  end associate
end subroutine xp_node__bw_add__2

!> Subtraction - operator
!! @param[in] nd `nodeattrs`
subroutine xp_node__op_sub__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_sub(x1, x2, x3)
  end associate
end subroutine xp_node__op_sub__1

!> Subtraction - forward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__fw_sub__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_sub(x1, x2, x3)
  end associate
end subroutine xp_node__fw_sub__1

!> Subtraction - backward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__bw_sub__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_sub(x1, x2, x3)
  end associate
end subroutine xp_node__bw_sub__1

!> Subtraction - operator
!! @param[in] nd `nodeattrs`
subroutine xp_node__op_sub__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_sub(x1, x2, x3, nd%ipar)
  end associate
end subroutine xp_node__op_sub__2

!> Subtraction - forward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__fw_sub__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_sub(x1, x2, x3, nd%ipar)
  end associate
end subroutine xp_node__fw_sub__2

!> Subtraction - backward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__bw_sub__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_sub(x1, x2, x3, nd%ipar)
  end associate
end subroutine xp_node__bw_sub__2

!> Multipliction - operator
!! @param[in] nd `nodeattrs`
subroutine xp_node__op_mult__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_mult(x1, x2, x3)
  end associate
end subroutine xp_node__op_mult__1

!> Multiplication - forward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__fw_mult__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_mult(x1, x2, x3)
  end associate
end subroutine xp_node__fw_mult__1

!> Multiplication - backward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__bw_mult__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_mult(x1, x2, x3)
  end associate
end subroutine xp_node__bw_mult__1

!> @todo document
subroutine xp_node__op_mult__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_mult(x1, x2, x3, nd%ipar)
  end associate
end subroutine xp_node__op_mult__2

!> Multiplication - forward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__fw_mult__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_mult(x1, x2, x3, nd%ipar)
  end associate
end subroutine xp_node__fw_mult__2

!> Multiplication - backward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__bw_mult__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_mult(x1, x2, x3, nd%ipar)
  end associate
end subroutine xp_node__bw_mult__2

!> Power - operator
!! @param[in] nd `nodeattrs`
subroutine xp_node__op_pow__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_pow(x1, x2, x3)
  end associate
end subroutine xp_node__op_pow__1

!> Power - forward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__fw_pow__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_pow(x1, x2, x3)
  end associate
end subroutine xp_node__fw_pow__1

!> Power - backward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__bw_pow__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_pow(x1, x2, x3)
  end associate
end subroutine xp_node__bw_pow__1

!> @todo document
subroutine xp_node__op_pow__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_pow(x1, x2, x3, nd%ipar)
  end associate
end subroutine xp_node__op_pow__2

!> Power - forward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__fw_pow__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_pow(x1, x2, x3, nd%ipar)
  end associate
end subroutine xp_node__fw_pow__2

!> Power - backward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__bw_pow__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_pow(x1, x2, x3, nd%ipar)
  end associate
end subroutine xp_node__bw_pow__2

!> Division - operator
!! @param[in] nd `nodeattrs`
subroutine xp_node__op_div__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_div(x1, x2, x3)
  end associate
end subroutine xp_node__op_div__1

!> Division - forward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__fw_div__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_div(x1, x2, x3)
  end associate
end subroutine xp_node__fw_div__1

!> Division - backward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__bw_div__1 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_div(x1, x2, x3)
  end associate
end subroutine xp_node__bw_div__1

!> @todo document
subroutine xp_node__op_div__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_div(x1, x2, x3, nd%ipar)
  end associate
end subroutine xp_node__op_div__2

!> Division - forward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__fw_div__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_div(x1, x2, x3, nd%ipar)
  end associate
end subroutine xp_node__fw_div__2

!> Division - backward differentiation
!! @param[in] nd `nodeattrs`
subroutine xp_node__bw_div__2 (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_div(x1, x2, x3, nd%ipar)
  end associate
end subroutine xp_node__bw_div__2

!> @}
