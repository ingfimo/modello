!> @defgroup node_operators__regularisations_ Regularisations
!! @{

!> @todo document, fix
subroutine xp_node__op_dropout (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    !FIX 
    !call op_dropout(x1, x2, x3, nd%rpar(1))
  end associate
end subroutine xp_node__op_dropout

!> @todo document
subroutine xp_node__fw_dropout (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_dropout(x1, x2, x3)
  end associate
end subroutine xp_node__fw_dropout

!> @todo document
subroutine xp_node__bw_dropout (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_dropout(x1, x2, x3)
  end associate
end subroutine xp_node__bw_dropout

!> @}
