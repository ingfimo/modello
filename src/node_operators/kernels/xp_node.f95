!> @defgroup node_operators__kernels_ Kernels
!! @{

!> ksqexp - operator
!! @param[in] nd 'node
subroutine xp_node__op_ksqexp (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       a => xp_NUMBERS_(nd%in(3)), &
       b => xp_NUMBERS_(nd%in(4)), &
       k => xp_NUMBERS_(nd%out(1)) &
       )
    call op_ksqexp(x1, x2, a, b, k)
  end associate
end subroutine xp_node__op_ksqexp

!> @todo document
subroutine xp_node__fw_ksqexp (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       a => xp_NUMBERS_(nd%in(3)), &
       b => xp_NUMBERS_(nd%in(4)), &
       k => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_ksqexp(x1, x2, a, b, k)
  end associate
end subroutine xp_node__fw_ksqexp

!> ksqexp - backward differentiation
!! @param[in] nd 'node
subroutine xp_node__bw_ksqexp (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       a => xp_NUMBERS_(nd%in(3)), &
       b => xp_NUMBERS_(nd%in(4)), &
       k => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_ksqexp(x1, x2, a, b, k)
  end associate
end subroutine xp_node__bw_ksqexp

!> @}
