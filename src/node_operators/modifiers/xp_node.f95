!> @defgroup node_operators__modifiers_ Modifiers
!! @{

subroutine xp_node__op_assign (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate(x1 => xp_NUMBERS_(nd%in(1)), x2 => xp_NUMBERS_(nd%in(2)))
    call op_assign(x1, x2)
  end associate
end subroutine xp_node__op_assign

subroutine xp_node__fw_assign (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate(x1 => xp_NUMBERS_(nd%in(1)), x2 => xp_NUMBERS_(nd%in(2)))
    call bw_assign(x1, x2)
  end associate
end subroutine xp_node__fw_assign

subroutine xp_node__bw_assign (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate(x1 => xp_NUMBERS_(nd%in(1)), x2 => xp_NUMBERS_(nd%in(2)))
    call bw_assign(x1, x2)
  end associate
end subroutine xp_node__bw_assign

!> Slice - operator
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__op_slice (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate(x1 => xp_NUMBERS_(nd%in(1)), x2 => xp_NUMBERS_(nd%out(1)))
    call op_slice(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__op_slice

!> Slice - forward differentiation 
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__fw_slice (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate(x1 => xp_NUMBERS_(nd%in(1)), x2 => xp_NUMBERS_(nd%out(1)))
    call fw_slice(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__fw_slice

!> Slice - backward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__bw_slice (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate(x1 => xp_NUMBERS_(nd%in(1)), x2 => xp_NUMBERS_(nd%out(1)))
    call bw_slice(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__bw_slice

!> Flat Slice - operator
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__op_flatslice (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate(x1 => xp_NUMBERS_(nd%in(1)), x2 => xp_NUMBERS_(nd%out(1)))
    call op_flatslice(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__op_flatslice

!> Flat Slice - forward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__fw_flatslice (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate(x1 => xp_NUMBERS_(nd%in(1)), x2 => xp_NUMBERS_(nd%out(1)))
    call fw_flatslice(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__fw_flatslice

!> Flat Slice - backward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__bw_flatslice (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate(x1 => xp_NUMBERS_(nd%in(1)), x2 => xp_NUMBERS_(nd%out(1)))
    call bw_flatslice(x1, x2, nd%ipar)
  end associate
end subroutine xp_node__bw_flatslice

!> Bind - operator
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__op_bind (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%in(2)), &
       x3 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_bind(x1, x2, x3, nd%ipar(1))
  end associate
end subroutine xp_node__op_bind

!> Bind - forward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__fw_bind (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => nd%in(1), &
       x2 => nd%in(2), &
       x3 => nd%out(1) &
       )
    call fw_bind(xp_NUMBERS_(x1), xp_NUMBERS_(x2), xp_NUMBERS_(x3), nd%ipar(1))
  end associate
end subroutine xp_node__fw_bind

!> Bind - backward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__bw_bind (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => nd%in(1), &
       x2 => nd%in(2), &
       x3 => nd%out(1) &
       )
    call bw_bind(xp_NUMBERS_(x1), xp_NUMBERS_(x2), xp_NUMBERS_(x3), nd%ipar(1))
  end associate
end subroutine xp_node__bw_bind

!> Embeddings - operator
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__op_embeddings (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       f => xp_NUMBERS_(nd%in(1)), &
       x => xp_NUMBERS_(nd%in(2)), &
       e => xp_NUMBERS_(nd%out(1)) &
       )
    call op_embeddings(f, x, e)
  end associate
end subroutine xp_node__op_embeddings

!> Embeddings - forward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__fw_embeddings (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       f => xp_NUMBERS_(nd%in(1)), &
       x => xp_NUMBERS_(nd%in(2)), &
       e => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_embeddings(f, x, e)
  end associate
end subroutine xp_node__fw_embeddings

!> Embeddings - backward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__bw_embeddings (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       f => xp_NUMBERS_(nd%in(1)), &
       x => xp_NUMBERS_(nd%in(2)), &
       e => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_embeddings(f, x, e)
  end associate
end subroutine xp_node__bw_embeddings

!> Transpose - operator
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__op_transpose (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_transpose(x1, x2)
  end associate
end subroutine xp_node__op_transpose

!> Transpose - forward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`ent
subroutine xp_node__fw_transpose (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call fw_transpose(x1, x2)
  end associate
end subroutine xp_node__fw_transpose

!> Transpose - backward differentiation
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__bw_transpose (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call bw_transpose(x1, x2)
  end associate
end subroutine xp_node__bw_transpose

!> Feeding Number - operator
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__op_feeding_number (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_feeding_number(x1, x2, ns%s1)
  end associate
end subroutine xp_node__op_feeding_number

!> Feeding Sequence - operator
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__op_feeding_sequence (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  associate( &
       x1 => xp_NUMBERS_(nd%in(1)), &
       x2 => xp_NUMBERS_(nd%out(1)) &
       )
    call op_feeding_sequence(x1, x2, ns%s1, nd%ipar(3), nd%ipar(4))
  end associate    
end subroutine xp_node__op_feeding_sequence

!> Feed - operator
!! @param[in] nd `nodeattrs`
!! @param[in] ns `nodestates`
subroutine xp_node__op_feed (nd, ns)
  implicit none
  type(xp_nodeattrs), intent(in) :: nd
  type(nodestates), intent(inout) :: ns
  integer :: batch(nd%ipar(1)), i, n
  n = size(ns%s1)
  !$omp parallel do
  do i = 0, nd%ipar(1) - 1
     batch(i + 1) = ns%s1(modulo(ns%s2(1) + i, n) + 1)
  end do
  !$omp end parallel do
  ns%s2(1) = ns%s2(1) + nd%ipar(1)
  do i = 2, size(nd%ipar)
     call private_feed(xp_NODES_(nd%ipar(i)))
  end do
contains
  subroutine private_feed (x)
    type(xp_node), intent(inout) :: x
    deallocate(x%states%s1)
    allocate(x%states%s1, source=x%attrs%ipar(batch + 4))
    call x%op(x%attrs, x%states)
  end subroutine private_feed
end subroutine xp_node__op_feed

!> @}
