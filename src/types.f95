!  This file is part of Modello.
!
!  Modello is free software; you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation; either version 2 of the License, or
!  (at your option) any later version.
!  
!  Modello is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with this program; if not, write to the Free Software
!  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
!  MA 02110-1301, USA.

module types

  use env
  
  implicit none

  private
  public &
       prc_call, &
       number, &
       sp_number, &
       dp_number, &
       nodeattrs, &
       nodestates, &
       sp_nodeattrs, &
       dp_nodeattrs, &
       dp_node, &
       sp_node, &
       graph, &
       sp_graph, &
       dp_graph, &
       sp_opt, &
       dp_opt, &
       hashtab, &
       is_deallocated, &
       is_empty, &
       is_allocated, &
       has_dx, &
       get_size, &
       get_rank, &
       get_net_rank, &
       with_shape

  character(len=*), parameter :: mod_types_name_ = 'types'
  
  !> Type 'procedure_call'
  !! Represents a call to a procedure.
  type prc_call
     character(len=:), allocatable :: prcname !< procedure name
     character(len=:), allocatable :: modname !< name of the module containin the procedure
     type(prc_call), pointer :: next => null() !< pointer to the next procedure call
     type(prc_call), pointer :: prev => null() !< pointer to previous call
  end type prc_call
  
  !> Type 'number'
  !! Represents the arrays involved in the calculation.
  type number
     integer :: id = 0 !< identifier index (position in the NUMBERS_ array)
     integer, allocatable :: shp(:) !< shape of the number
     integer :: init(number_init_sz_) = init_null_ !< initialisation register
     logical, pointer :: sparse(:) 
  end type number

  type, extends(number) :: sp_number
     real(kind=sp_), pointer :: v(:) => null() !< value if the 'number'
     real(kind=sp_), pointer :: dv(:) => null() !< value of the 'number' derivative
  end type sp_number

  type, extends(number) :: dp_number
     real(kind=dp_), pointer :: v(:) => null() !< value if the 'number'
     real(kind=dp_), pointer :: dv(:) => null() !< value of the 'number' derivative
  end type dp_number

  type :: nodeattrs
     integer :: id = 0  !< identifier index (position in the NODES_ array)
     integer :: opid = 0  !< operator ideitifier
     integer, allocatable :: out(:) !< identifiers of the output 'numbers'
     integer, allocatable :: in(:) !< indentifiers of the input 'numbers'
     integer, allocatable :: ipar(:) !< operator flags (can be empty)
  end type nodeattrs

  type :: nodestates
     integer, allocatable :: s1(:)
     integer, allocatable :: s2(:)
  end type nodestates

  type, extends(nodeattrs) :: sp_nodeattrs
     real(kind=sp_), allocatable :: rpar(:)
  end type sp_nodeattrs

  type, extends(nodeattrs) :: dp_nodeattrs
     real(kind=dp_), allocatable :: rpar(:)
  end type dp_nodeattrs

  abstract interface
     subroutine sp_node_method (nd, ns)
       import :: sp_nodeattrs
       import :: nodestates
       implicit none
       type(sp_nodeattrs), intent(in) :: nd
       type(nodestates), intent(inout) :: ns
     end subroutine sp_node_method
     subroutine dp_node_method (nd, ns)
       import :: dp_nodeattrs
       import :: nodestates
       implicit none
       type(dp_nodeattrs), intent(in) :: nd
       type(nodestates), intent(inout) :: ns
     end subroutine dp_node_method
  end interface

  !> Type 'node'
  !! A node stores the infrmations necessary to reproduce a certain calculation step.
  type sp_node
     type(sp_nodeattrs), pointer :: attrs => null()
     type(nodestates), pointer :: states => null()
     procedure(sp_node_method), pointer, nopass :: op => null()
     procedure(sp_node_method), pointer, nopass :: fw => null()
     procedure(sp_node_method), pointer, nopass :: bw => null()
  end type sp_node

  type dp_node
     type(dp_nodeattrs), pointer :: attrs => null()
     type(nodestates), pointer :: states => null()
     procedure(dp_node_method), pointer, nopass :: op => null()
     procedure(dp_node_method), pointer, nopass :: fw => null()
     procedure(dp_node_method), pointer, nopass :: bw => null()
  end type dp_node

  !> Type 'graph'
  !! Is a collcetion of nodes and represent a certain calculation flow.
  type :: graph
     integer, allocatable :: nodes(:) !< nodes making the graph
     integer, allocatable :: posts(:) !< nodes storing operation to run post-update
     integer :: id = 0
     logical :: withdx = .true.
  end type graph

  type, extends(graph) :: sp_graph
  end type sp_graph

  type, extends(graph) :: dp_graph
  end type dp_graph

  type :: opt
     integer, allocatable :: xin(:)
     integer, allocatable :: xab(:)
     integer :: iter = -1
     logical :: clip = .false.
     integer :: id = 0   
  end type opt

  !> sp_opt
  type, extends(opt) :: sp_opt
     real(kind=sp_), allocatable :: v1(:) !< first order moment logging
     real(kind=sp_), allocatable :: v2(:) !< second order moment logging
     real(kind=sp_) :: clipval = 0 
  end type sp_opt

  !> dp_opt
  type, extends(opt) :: dp_opt
     real(kind=dp_), allocatable :: v1(:) !< first order moment logging
     real(kind=dp_), allocatable :: v2(:) !< second order moment logging
     real(kind=dp_) :: clipval = 0
  end type dp_opt

  type :: hashtab
     integer, allocatable :: t(:,:)
     integer, allocatable :: r(:)
     integer, allocatable :: m(:)
     integer :: id = 0
  end type hashtab
     
  
  !> Checks if a 'type' is correctly deallocated (returns .true.) or not (returns .false.).
  !! @author Filippo Monari
  !! @param[in] x a defined 'type'
  interface is_deallocated
     module procedure sp_number__is_deallocated
     module procedure dp_number__is_deallocated
     module procedure sp_nodeattrs__is_deallocated
     module procedure dp_nodeattrs__is_deallocated
     module procedure sp_node__is_deallocated
     module procedure dp_node__is_deallocated
     module procedure graph__is_deallocated
     module procedure sp_opt__is_deallocated
     module procedure dp_opt__is_deallocated
  end interface is_deallocated

  ! !> Checks if a 'number' is correctly initialised (returne .true.) or not (returns .false.).
  ! !! @author Filippo Monari
  ! !! @param[in] x a 'number'
  ! interface is_init
  !    module procedure number__is_init
  ! end interface is_init

  !> Cehcks if a 'number' or 'graph' is empty (returns .true.) or not (returns .false.).
  !! @author Filippo Monari
  !! @param[in] x a 'number' or 'graph'
  interface is_empty
     module procedure number__is_empty
     module procedure graph__is_empty
  end interface is_empty

  !> Checks if a 'type' is correctly allocated (returns .true.) or not (returns .false.).
  !! @author Filippo Monari
  !! @param[in] x a defined 'type'
  interface is_allocated
     module procedure prc_call__is_allocated
     module procedure sp_number__is_allocated
     module procedure dp_number__is_allocated
     module procedure sp_nodeattrs__is_allocated
     module procedure dp_nodeattrs__is_allocated
     module procedure sp_node__is_allocated
     module procedure dp_node__is_allocated
     module procedure graph__is_allocated
     module procedure sp_opt__is_allocated
     module procedure dp_opt__is_allocated
  end interface is_allocated
  
  !> Checks if a 'number' has a derivative element (returns .true.) or not (returns .false.).
  !! @author Filippo Monari
  !! @param[in] x a 'number'
  interface has_dx
     module procedure sp_number__has_dx
     module procedure dp_number__has_dx
  end interface has_dx

  !> Returns the size of a 'number' or 'graph'.
  !! @author Filippo Monari
  !! @param[in] x a 'number' or 'graph'
  interface get_size
     module procedure number__size__1
     module procedure number__size__2
     module procedure graph__size
  end interface get_size

  !> Returns the rank of a 'number'.
  !! @author Filippo Monari
  !! @param[in] x a 'number'
  interface get_rank
     module procedure number__rank
  end interface get_rank

  interface get_net_rank
     module procedure number__net_rank
  end interface get_net_rank

  !> Gives shape to a 'number' by associating to it a pointer
  !! of the correct rank and shape
  !! @author Filippo Monari
  !! @param[in] x 'number'
  !! @param[out] xs pointer of correct type and shape
  !! @param[in] which character, 'v' for shaping 'x%v', 'dv' for shaping 'x%dv'
  !! @details
  !! The correctness of the provided shape of the porvided pointer is not
  !! checked.
  interface with_shape
     module procedure sp_number__with_shape__1
     module procedure dp_number__with_shape__1
     module procedure sp_number__with_shape__2
     module procedure dp_number__with_shape__2
  end interface with_shape
  
contains

  include "./types/prc_call/prc_call.f95"
  include "./types/number/number.f95"
  include "./types/number/sp.f95"
  include "./types/number/dp.f95"
  include "./types/node/node.f95"
  include "./types/node/sp.f95"
  include "./types/node/dp.f95"
  include "./types/graph/graph.f95"
  include "./types/opt/opt.f95"
  include "./types/opt/sp_opt.f95"
  include "./types/opt/dp_opt.f95"
 
end module types



	
