!  This file is part of Modello.
!
!  Modello is free software; you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation; either version 2 of the License, or
!  (at your option) any later version.
!  
!  Modello is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with this program; if not, write to the Free Software
!  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
!  MA 02110-1301, USA.

module numbers
  !_DOC_
  !This module defines the data structures to be used in the calculations.
  !New required data structures must be defined in this file as well.

  !* DEPENDENCIES

  use env
  use types
  use registers, only: &
       sp_NUMBERS_, &
       dp_NUMBERS_, &
       NUMINDEX_, &
       INLOCKS_, &
       NUMNDS_, & 
       sp_NODES_, &
       dp_NODES_
  use registers_utils, only: next_number
  use errwarn
  use utils
  use numbers_utils
  use node_operators
  use nodes

  implicit none

  private

  public &
       number__append, &
       ! number__append_slice, &
       ! number__append_flat_slice, &
       number__append_contiguous_slice, &
       number__append_reshape, &
       number__append_drop_shape, &
       number__append_bind, &
       number__op, &
       number__fw, &
       number__bw_zero, &
       number__bw, &
       number__pop


  character(len=*), parameter :: mod_numbers_name_ = 'numbers'

  !> Startng from a source entity or shape vector, appends a new number to the relative register
  !! and returns its 'symbol'
  !! @author Filippo Monari
  !! @param[inout] x 'symbol' to associate to the appended 'number'
  !! @param[in] shp integer(:), shape
  !! @param[in] dx logical, if .true. the derivative is allocated
  !! @param[in] v double precision optional, source
  interface number__append
     module procedure sp_number__append__1
     module procedure sp_number__append__2
     module procedure sp_number__append__3
     module procedure dp_number__append__1
     module procedure dp_number__append__2
     module procedure dp_number__append__3
  end interface number__append

  ! interface number__append_slice
  !    module procedure sp_number__append_slice
  !    module procedure dp_number__append_slice
  ! end interface number__append_slice

  ! interface number__append_flat_slice
  !    module procedure sp_number__append_flat_slice
  !    module procedure dp_number__append_flat_slice
  ! end interface number__append_flat_slice

  interface number__append_contiguous_slice
     module procedure sp_number__append_contiguous_slice
     module procedure dp_number__append_contiguous_slice
  end interface number__append_contiguous_slice

  interface number__append_reshape
     module procedure sp_number__append_reshape
     module procedure dp_number__append_reshape
  end interface number__append_reshape

  interface number__append_drop_shape
     module procedure sp_number__append_drop_shape
     module procedure dp_number__append_drop_shape
  end interface number__append_drop_shape

  interface number__append_bind
     module procedure sp_number__append_bind
     module procedure dp_number__append_bind
  end interface number__append_bind

  interface number__op
     module procedure sp_number__op
     module procedure dp_number__op
  end interface number__op

  interface number__fw
     module procedure sp_number__fw
     module procedure dp_number__fw
  end interface number__fw

  interface number__bw
     module procedure sp_number__bw
     module procedure dp_number__bw
  end interface number__bw

  interface number__bw_zero
     module procedure sp_number__bw_zero
     module procedure dp_number__bw_zero
  end interface number__bw_zero

  interface number__pop
     module procedure sp_number__pop
     module procedure dp_number__pop
  end interface number__pop
  
contains

  include "./numbers/append/sp_number.f95"
  include "./numbers/append/dp_number.f95"

  include "./numbers/misc/sp_number.f95"
  include "./numbers/misc/dp_number.f95"
 
end module numbers
