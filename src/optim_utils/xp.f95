!> @brief Gets the state (if any) of an @optimiser
!! @param[inout] opt @xp_opt
!! @param[in] i @int, 1 for the first state, 2 for the second one
!! @param[out] state @int, @allocatable, @vector
subroutine xp_opt__get_state (opt, i, state)
  implicit none
  type(xp_opt), intent(in) :: opt
  integer, intent(in) :: i
  real(kind=xp_), intent(out), allocatable :: state(:)
  call do_safe_within("xp_opt__get_state", mod_optim_utils_name_, private_get)
contains
  subroutine private_get
    call assert(is_allocated(opt), err_notAlloc_, "opt")
    if (err_free()) then
       select case (i)
       case (1)
          call alloc(state, opt%v1, "state")
       case (2)
          call alloc(state, opt%v2, "state")
       case default
          call raise_error("i", err_unknwnVal_)
       end select
    end if
  end subroutine private_get
end subroutine xp_opt__get_state

!> @brief Sets the value of the state of an @optimiser
!! @param[inout] opt @xp_opt
!! @param[in] i @int, 1 for the first state, 2 for the second state
!! @param[in] state @int @vector state value
subroutine xp_opt__set_state (opt, i, state)
  implicit none
  type(xp_opt), intent(inout) :: opt
  integer, intent(in) :: i
  real(kind=xp_), intent(in) :: state(:)
  call do_safe_within("xp_opt__set_state", mod_optim_utils_name_, private_set)
contains
  subroutine private_set
    call assert(is_allocated(opt), err_notAlloc_, "opt")
    if (err_free()) then
       select case (i)
       case (1)
          call assert(size(opt%v1) == size(state), err_wrngSz_, "state")
          if (err_free()) then
             call dealloc(opt%v1, "v1")
             call alloc(opt%v1, state, "v1")
          end if
       case (2)
          call assert(size(opt%v2) == size(state), err_wrngSz_, "state")
          if (err_free()) then
             call dealloc(opt%v2, "v2")
             call alloc(opt%v2, state, "v2")
          end if
       case default
          call raise_error("i", err_unknwnVal_)
       end select
    end if
  end subroutine private_set
end subroutine xp_opt__set_state

!> @brief Sets the parameters for gradient clipping in an @optimiser
!! @param[inout] opt @xp_opt
!! @param[in] clip @bool, if @true gradient clipping is performed
!! @param[in] clipval @xp_ maximum allowed norm for the gradient vectors
subroutine xp_opt__set_clipping (opt, clip, clipval)
  implicit none
  type(xp_opt), intent(inout) :: opt
  logical, intent(in) :: clip
  real(kind=xp_) :: clipval
  call do_safe_within("xp_opt__set_clipping", mod_optim_utils_name_, private_set)
contains
  subroutine private_set
    call assert(is_allocated(opt), err_notAlloc_, "opt")
    if (err_free()) then
       opt%clip = clip
       opt%clipval = clipval
    end if
  end subroutine private_set
end subroutine xp_opt__set_clipping

!> @brief Gets the current paramters for gradient clipping
!! @param[in] opt @xp_opt
!! @param[out] clipping @xp_, @vector
subroutine xp_opt__get_clipping (opt, clipping)
  implicit none
  type(xp_opt), intent(in) :: opt
  real(kind=xp_), intent(out) :: clipping(:)
  call do_safe_within("xp_opt__get_clipping", mod_optim_utils_name_, private_get)
contains
  subroutine private_get
    call assert(is_allocated(opt), err_notAlloc_, "opt")
    call assert(size(clipping) == 2, err_wrngSz_, "clipping")
    if (err_free()) then
       if (opt%clip) then
          clipping = [1._xp_, opt%clipval]
       else
          clipping = [-1._xp_, opt%clipval]
       end if
    end if
  end subroutine private_get
end subroutine xp_opt__get_clipping

!> @brief Checks that all optimization parameters have gradients
!! @param[in] xin @int @vector ids of numbers containing optimisation parameters
!! @param[in] dtyp @xp_ @precision
subroutine xp_opt__assert_xin_has_dx (xin, dtyp)
  implicit none
  integer, intent(in) :: xin(:)
  real(kind=xp_), intent(in) :: dtyp
  integer :: i
  call do_safe_within("xp_opt__assert_xin_has_dx", mod_optim_utils_name_, private_assert)
contains
  subroutine private_assert
    do i = 1, size(xin)
       call assert(has_dx(xp_NUMBERS_(xin(i))), err_generic_, "Gradient descent input has no gradient.")
       if (err_free()) exit
    end do
  end subroutine private_assert
end subroutine xp_opt__assert_xin_has_dx

subroutine xp_opt__collect_xab (xin, xab, dtyp)
  implicit none
  integer, intent(in) :: xin(:)
  integer, intent(out), allocatable, target :: xab(:)
  real(kind=xp_), intent(in) :: dtyp
  integer, pointer :: xxab(:,:)
  call do_safe_within("xp_opt__collect_xab", mod_optim_utils_name_, private_collect)
contains
  subroutine private_collect
    integer :: i
    call alloc(xab, 2 * size(xin), "xab")
    xxab(1:2,1:size(xin)) => xab
    xxab(1, 1) = 1
    xxab(2, 1) = get_size(xp_NUMBERS_(xin(1)))
    do i = 2, size(xin)
       xxab(1, i) = xxab(2, i-1) + 1
       xxab(2, i) = xxab(2, i) + get_size(xp_NUMBERS_(xin(i)))
    end do
  end subroutine private_collect
end subroutine xp_opt__collect_xab
  
!> Collects all the input values in the given allocatable ('x').
!! @param[in] xin integer(:), indexes in the NUMBERS_ array of the input 'numbers'
!! @param[out] x double precision allocatable, variable collecting the values
subroutine xp__collect_xin (xin, x)
  implicit none
  integer, intent(in) :: xin(:)
  real(kind=xp_), intent(out), allocatable :: x(:)
  integer :: i
  call do_safe_within("xp__collect_xin", mod_optim_utils_name_, private_collect)
contains
  subroutine private_collect
    do i = 1, size(xin)
       call append_to_array(x, xp_NUMBERS_(xin(i))%v, 'x')
       if (.not. err_free()) exit
    end do
  end subroutine private_collect
end subroutine xp__collect_xin

!> Collects the input derivative values in the given allocatable ('dx')
!! @param[in] xin integer(:), indexes in the NUMBERS_ array of the input 'numbers'
!! @param[out] dx double precision allocatable, variable collecting the values
subroutine xp__collect_dxin (xin, dx)
  implicit none
  integer, intent(in) :: xin(:)
  real(kind=xp_), intent(out), allocatable :: dx(:)
  integer :: i
  do i = 1, size(xin)
     call append_to_array(dx, xp_NUMBERS_(xin(i))%dv, 'dx')
     if (.not. err_free()) exit
  end do
end subroutine xp__collect_dxin

! !> Initialises to one the values of the output derivative
! !! @param[in] xout integer, index in the NUMBERS_ array of the output 'number'
! !! @todo delete
! subroutine xp__init_bw (xout)
!   implicit none
!   type(xp_number), intent(in) :: xout
!   xout%dv = 1
! end subroutine xp__init_bw

!> Simple input update. Just overwrites the input values with the updates ones.
!! @param[in] xin integer(:), indexes in the NUMBERS_ array of the input numbers
!! @param[in] p double precision(:), updated values
subroutine xp__simple_update (xin, p)
  implicit none
  integer, intent(in) :: xin(:)
  real(kind=xp_), intent(in) :: p(:)
  type(xp_number), pointer :: x
  integer :: i, a, b
  b = 0
  do i = 1, size(xin)
     x => xp_NUMBERS_(xin(i))
     a = b + 1
     b = b + get_size(x)
     x%v = p(a:b)
  end do
end subroutine xp__simple_update

!> Given a graph calculates and return the objective function value.
!! @param[in] g integer, graph index in the GRAPHS_ array
!! @param[in] xout integer, index in the NUMBERS_ array of the output 'number'
function xp__get_obj (g, xout) result(ans)
  implicit none
  type(xp_graph), intent(in) :: g
  type(xp_number), intent(in) :: xout
  real(kind=xp_) :: ans
  call graph__op(g)
  ans = xout%v(1)
end function xp__get_obj

!> Deallocates an 'opt' object
!! @param[in] x 'opt'
subroutine xp_opt__deallocate (x)
  implicit none
  type(xp_opt), intent(inout) :: x
  call do_safe_within('xp_opt__deallocate', mod_optim_utils_name_, private_deallocate)
contains
  subroutine private_deallocate
    call assert(is_allocated(x), err_notAlloc_, 'x')
    call dealloc(x%xin, 'x%in')
    call dealloc(x%v1, 'x%v1')
    call dealloc(x%v2, 'x%v2')
    x%clip = .false.
    x%clipval = 0
    if (err_free()) then
       x%iter = 0
    end if
  end subroutine private_deallocate
end subroutine xp_opt__deallocate

!> Pops (removes) an 'opt' object from the GOPTS_ register.
!! @param[in] i integer, 'opt' index
subroutine xp_opt__pop (opt)
  implicit none
  type(xp_opt), intent(inout), pointer :: opt
  call do_safe_within('gopt__pop', mod_optim_utils_name_, private_pop)
contains
  subroutine private_pop
    integer :: opti
    call assert(allocated(xp_OPTS_), err_notAlloc_, 'GOPTS_')
    opti = opt%id
    nullify(opt)
    call opt__deallocate(xp_OPTS_(opti))
  end subroutine private_pop
end subroutine xp_opt__pop

subroutine xp__clip_gradient (xin, v)
  implicit none
  integer, intent(in) :: xin(:)
  real(kind=xp_), intent(in) :: v
  type(xp_number), pointer :: x
  integer :: i
  real(kind=xp_) :: m
  do i=1, size(xin)
     x => xp_NUMBERS_(xin(i))
     m = L2NORM(x%dv)
     if (m > v) x%dv = x%dv * v  / m
  end do
end subroutine xp__clip_gradient


