!  This file is part of Modello.
!
!  Modello is free software; you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation; either version 2 of the License, or
!  (at your option) any later version.
!  
!  Modello is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with this program; if not, write to the Free Software
!  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
!  MA 02110-1301, USA.

module env

  !> @defgroup env_ Environmental Constants and Parameters
  !! @author Filippo Monari <ingfimo@gmail.com>
  !! @{

  implicit none

  !> Abstract interface used to pass subroutines with no arguments
  !! to other functions or subroutines
  abstract interface 
     subroutine sbr0_
     end subroutine sbr0_
  end interface
  
  !> @defgroup env__precision_kinds_ Precision Kinds
  !! Constants defining the different kind of numerical precision.
  !! Currently only double precision numbers can be used
  !! @{
  integer, parameter :: sp_ = kind(1.0)  !< single precision
  integer, parameter :: dp_ = kind(1.d0) !< double precision
  integer, parameter :: precisions_(2) = [sp_, dp_]
  !> @}
  
  !> @defgroup env__numerical_constants_ Numerical Constants
  !! Useful numerical constants.
  !! @{
  real(kind=dp_), parameter :: eps_dp_ = epsilon(1._dp_) !< machine double precision
  real(kind=sp_), parameter :: eps_sp_ = epsilon(1._sp_) !< macine single precision
  real(kind=dp_), parameter :: pi_dp_ = acos(-1._dp_)    !< pi greek double precision
  real(kind=sp_), parameter :: pi_sp_ = acos(-1._sp_)    !< pi greek single precision
  real(kind=dp_), parameter :: OO_dp_ = huge(1._dp_)     !< double precision infinite (max dp number)
  real(kind=sp_), parameter :: OO_sp_ = huge(1._sp_)     !< single precision infinire (max sp number)
  real(kind=dp_), parameter :: tol_dp_ = sqrt(eps_dp_) !< double precision tollerance for calculations (about 1.49e-8)
  real(kind=sp_), parameter :: tol_sp_ = sqrt(eps_sp_) !< single precision tollerance for calculations (about 1.49e-8)
  real(kind=dp_), parameter :: tiny_dp_ = 1e-6_dp_     !< double precision threshold for tiny numbers
  real(kind=sp_), parameter :: tiny_sp_ = 1e-6_sp_     !< single precision threshold for tiny numbers
  real(kind=dp_), parameter :: euler_mascheroni_dp_ = 0.57721566490153286060_dp_ !< double precision Euler-Mascheroni constant
  real(kind=dp_), parameter :: euler_mascheroni_sp_ = 0.57721566490153286060_sp_ !< single precision Euler-Mascheroni constant
  real(kind=dp_), parameter :: ReimannAt2_dp_ = pi_dp_**2 / 6 !< double precision value of Reimann Z function at 2
  real(kind=dp_), parameter :: ReimannAt2_sp_ = pi_sp_**2 / 6 !< double precision value of Reimann Z function at 2
  !> @}
  
  !> @defgroup  env__error_codes_ Error Codes
  !! Integers defining different available error types
  !! @{
  integer, parameter :: err_alloc_ = 1        !< error on allocation 
  integer, parameter :: err_dealloc_ = 2      !< error on deallocation 
  integer, parameter :: err_unknwnVal_ = 3    !< unknwon value 
  integer, parameter :: err_wrngSz_ = 4       !< wrong size or shape 
  integer, parameter :: err_oorng_ = 5        !< out of range 
  integer, parameter :: err_alreadyAlloc_ = 6 !< alredy allocated 
  integer, parameter :: err_notAlloc_ = 7     !< not allocated 
  integer, parameter :: err_alreadyAssoc_ = 8 !< already associated 
  integer, parameter :: err_notAssoc_ = 9     !< not associated 
  integer, parameter :: err_alreadyInit_ = 10 !< already initialised 
  integer, parameter :: err_notInit_ = 11     !< not initialised
  integer, parameter :: err_missingArg_ = 12  !< missing argument
  integer, parameter :: err_wrngArg_ = 13     !< wrong argument
  integer, parameter :: err_wrngTyp_ = 14     !< wrong type
  integer, parameter :: err_generic_ = 15     !< generic error

  integer, parameter :: err_external_wrngArg_end_ = 50
  integer, parameter :: err_external_singular_ = err_external_wrngArg_end_ + 1
  integer, parameter :: err_external_end_ = 99
  
  integer, parameter :: err_dgetrf_start_ = 100
  integer, parameter :: err_dgetri_start_ = 200
  integer, parameter :: err_dpotrf_start_ = 300
  integer, parameter :: err_dpotri_start_ = 400
  integer, parameter :: err_dgesv_start_ = 500
  integer, parameter :: err_dposv_start_ = 600
  !> @}

  !> @defgroup env__warning_cdes_ Warning Codes
  !! Integers defining different available warning types
  !! @{
  integer, parameter :: warn_generic_ = 1 !< generic warning
  integer, parameter :: warn_hasdx_ = 2   !< number has gradient
  !> @}
  
  !> @defgroup env__number_parameters_ Number Parameters
  !! Parameters relative to 'number' types
  !! @{
  
  !> @defgroup env__number_parameters__init_ Init Register Parameters
  !! Parameters defining the size and the slots of the 'init' register of a 'number'.
  !! @{
  integer, parameter :: number_init_sz_ = 2 !< size of the init register
  ! integer, parameter :: number_init_idi_ = 1 !< index of the init register for x%id
  ! integer, parameter :: number_init_shpi_ = 2 !< index of the init register for x%shp
  integer, parameter :: number_init_vi_ = 1   !< index of the init register for x%v
  integer, parameter :: number_init_dvi_ = 2  !< index of the init register for x%dv
  !> @}
  
  !> @defgroup env__number_parameters_init_falgs_ Initialisation Flags
  !! Falgs signaling the initialitisation status for 'number' elements.
  !! @{
  integer, parameter :: init_null_ = 0  !< null, not allocated
  integer, parameter :: init_alloc_ = 1 !< allocated
  integer, parameter :: init_assoc_ = 2 !< associated
  integer, parameter :: init_vals_(3) = [init_null_, init_alloc_, init_assoc_] !< set of defined initialisation values
  !> @}
  !! @}
  
  !> @defgroup env__operator_ids_ Operator IDs
  !! Integer parameters defininig the operators to be applied in the calculations.
  !! @{

  !> @defgroup env_operatorIds_modifiers_ Modifiers Operators
  !! Slincing, binding and reshaping.
  !! @{
  integer, parameter :: op_modif_start_ = 1 !< start value for modifier operator indexs
  integer, parameter :: op_modif_end_ = 100  !< end value for modifier operator indexs
  integer, parameter :: op_slice_id_ = 0 + op_modif_start_ !< generic slice id
  integer, parameter :: op_flatslice_id_ = 1 + op_modif_start_ !< flat slice id
  integer, parameter :: op_contiguous_slice_id_ = 2 + op_modif_start_ !< contiguous (with pointer) slice id
  integer, parameter :: op_reshape_id_ = 3 + op_modif_start_ !< reshape id
  integer, parameter :: op_drop_shape_id_ = 4 + op_modif_start_ !< drop id
  integer, parameter :: op_bind_id_ = 5 + op_modif_start_       !< bind id
  integer, parameter :: op_embeddings_id_ = 6 + op_modif_start_ !< embeddings id
  integer, parameter :: op_transpose_id_ = 7 + op_modif_start_  !< traspose operator id
  integer, parameter :: op_assign_id_ = 8 + op_modif_start_
  !> @}
  
  !> @defgroup env__operator_ids__unary_operators_ Unary Operators
  !! Take a number as input, and returs a number of the same rank and shappe.
  !! @{
  integer, parameter :: op_unary_start_ = 101 !< start value for unary operator indexes
  integer, parameter :: op_unary_end_ = 200   !< end value for unary operator indexes
  integer, parameter :: op_neg_id_ = 0 + op_unary_start_ !< negative id
  integer, parameter :: op_abs_id_ = 1 + op_unary_start_ !< abs id
  integer, parameter :: op_expon_id_ = 2 + op_unary_start_ !< exponential id
  integer, parameter :: op_log_id_ = 3 + op_unary_start_ !< log id
  integer, parameter :: op_sin_id_ = 4 + op_unary_start_ !< sin id
  integer, parameter :: op_cos_id_ = 5 + op_unary_start_ !< cos id
  integer, parameter :: op_tan_id_ = 6 + op_unary_start_ !< tan id
  integer, parameter :: op_sinh_id_ = 7 + op_unary_start_ !< sinh id
  integer, parameter :: op_cosh_id_ = 8 + op_unary_start_ !< cosh id
  integer, parameter :: op_tanh_id_ = 9 + op_unary_start_ !< tanh id
  !> @}
  
  !> @defgroup env__operator_ids__binary_opertors_ Binary Operators
  !! Take two 'numbers' as inputs, and return a number with
  !! rank the higer rank and with shape the shape of the higher rank number.
  !! With simple broadcasting it is mean the spreading of the shorter number over the
  !! longer one.
  !! With broadcasting it is meant a more complex mechanism wherein depending on the number shapes
  !! the shorter number is spread only along certain dimensions.
  !! @{
  integer, parameter :: op_binary_start_ = 201 !< starting value for binary operator indexes
  integer, parameter :: op_binary_end_ = 300   !< end value for binary operator indexes
  integer, parameter :: op_add1_id_ = 0 + op_binary_start_ !< addition with simple broadcasting
  integer, parameter :: op_add2_id_ = 1 + op_binary_start_ !< addition with broadcasting
  integer, parameter :: op_sub1_id_ = 2 + op_binary_start_ !< subtraction with simple broadcasting
  integer, parameter :: op_sub2_id_ = 3 + op_binary_start_ !< subtraction with broadcasting
  integer, parameter :: op_mult1_id_ = 4 + op_binary_start_ !< multiplication with simple broadcasting
  integer, parameter :: op_mult2_id_ = 5 + op_binary_start_ !< multiplication with broadcasting
  integer, parameter :: op_pow1_id_ = 6 + op_binary_start_  !< power with simple broadcasting
  integer, parameter :: op_pow2_id_ = 7 + op_binary_start_  !< power with broadcasting 
  integer, parameter :: op_div1_id_ = 8 + op_binary_start_  !< division with simple broadcasting
  integer, parameter :: op_div2_id_ = 9 + op_binary_start_  !< division with broadcasting
  !> @}

  !> @defgroup env__operator_ids__activations_ Activation Functions
  !! Operators indicating fucntions commonly used as activations in Neural Networks.
  !! The Hyperbolic Tangent, being a common operation, is considered as unary operator.
  !! @{
  integer, parameter :: op_activation_start_ = 301 !< starting value for activation function operators
  integer, parameter :: op_activation_end_ = 400   !< end value for activation function operators
  integer, parameter :: op_sigmoid_id_ = 0 + op_activation_start_ !< sigmoid function 
  integer, parameter :: op_relu_id_ = 1 + op_activation_start_ !< relu function
  integer, parameter :: op_leakyrelu_id_ = 2 + op_activation_start_ !< leaky relu function
  integer, parameter :: op_elu_id_ = 3 + op_activation_start_ !< elu function
  integer, parameter :: op_silu_id_ = 4 + op_activation_start_ !< silu function
  integer, parameter :: op_swish_id_ = 5 + op_activation_start_ !< swish function
  integer, parameter :: op_softmax1_id_ = 6 + op_activation_start_ !< softmax function
  integer, parameter :: op_softmax2_id_ = 7 + op_activation_start_ !< softmax function along an axis
  integer, parameter :: op_softplus_id_ = 8 + op_activation_start_ !< softplus function
  !> @}

  !> @defgroup env__operator_ids__matrix_ Matrix Operators
  !! Operators for matris-matrix multiplication, vector-matrix multiplication,
  !! vector-vector multiplication, and matrix inversion 
  !! @{
  integer, parameter :: op_matrix_start_ = 401 !< start value for matrix operators
  integer, parameter :: op_matrix_end_ = 500   !< end value for matrix operators
  integer, parameter :: op_gmmmult1_id_ = 0 + op_matrix_start_ !< gmmmult (alpha * op(A).op(B) + beta * C) 
  integer, parameter :: op_gmmmult2_id_ = 1 + op_matrix_start_ !< gmmmult (alpha * op(A).op(B) + C) 
  integer, parameter :: op_gmmmult3_id_ = 2 + op_matrix_start_ !< gmmmult (op(A).op(B) + beta * C) 
  integer, parameter :: op_gmmmult4_id_ = 3 + op_matrix_start_ !< gmmmult (op(A).op(B)) id
  integer, parameter :: op_gmvmult1_id_ = 4 + op_matrix_start_ !< gmvmult (alpha op(A).x + beta * y) 
  integer, parameter :: op_gmvmult2_id_ = 5 + op_matrix_start_ !< gmvmult (op(A).x + y) id
  integer, parameter :: op_gmvmult3_id_ = 6 + op_matrix_start_ !< gmvmult (alpha op(A).x) id
  integer, parameter :: op_gmvmult4_id_ = 7 + op_matrix_start_ !< gmvmult (op(A).x) id
  integer, parameter :: op_vvouter1_id_ = 8 + op_matrix_start_ ! <ger (alpha * x.y**T + A) id
  integer, parameter :: op_vvouter2_id_ = 9 + op_matrix_start_ !< ger (x.y**T + A) id
  integer, parameter :: op_vvouter3_id_ = 10 + op_matrix_start_ !< ger (x.y**T) id
  integer, parameter :: op_vvinner_id_ = 11 + op_matrix_start_ !< dot (x**T.y) id
  integer, parameter :: op_invMat_id_ = 12 + op_matrix_start_ !< matrix inverstion id
  integer, parameter :: op_slidemmm_id_ = 13 + op_matrix_start_ !< slide style matrix matrix multiplication
  !> @}

  !> @defgroup env__operators_ids__kernels Kernel Operators
  !! Operators implementing kernel functions (or covariance functions)
  !! @{
  integer, parameter :: op_kernel_start_ = 501 !< start value for kernel operators
  integer, parameter :: op_kernel_end_ = 600   !< end value for kernel operators
  integer, parameter :: op_ksqexpon_id_ = 0 + op_kernel_start_ !< square exponential kernel
  !! @}
  
  !> @defgroup env__operators_ids__reductions Reduction Operators
  !! Take a 'number' with rank > 0 and return a 'number' with lower rank
  !! @{
  integer, parameter :: op_reduction_start_ = 601 !< starting value for reduction operators
  integer, parameter :: op_reduction_end_ = 700 !< end value for reduction operators
  integer, parameter :: op_sum1_id_ = 0 + op_reduction_start_ !< sum along all dimensions 
  integer, parameter :: op_sum2_id_ = 1 + op_reduction_start_ !< sum along one dimension
  integer, parameter :: op_product1_id_ = 2 + op_reduction_start_ !< product along all dimensions
  integer, parameter :: op_product2_id_ = 3 + op_reduction_start_ !< product along one dimension
  integer, parameter :: op_ssq1_id_ = 4 + op_reduction_start_ !< sum of squares along all dimensions
  integer, parameter :: op_ssq2_id_ = 5 + op_reduction_start_ !< sum of squares along one dimension
  !> @}

  !> @defgroup env__operator_ids__ldprob_ log-probability Density Distribution Functions
  !! Opertrors implementing the natural logarithm of probability density distributions
  integer, parameter :: op_ldprob_start_ = 701 !< start value for log-probability density ditributions
  integer, parameter :: op_ldprob_end_ = 800   !< end value for log-probability density distributions
  integer, parameter :: op_ldexpon_id_= 0 + op_ldprob_start_ !< exponential
  integer, parameter :: op_ldlaplace_id_ = 1 + op_ldprob_start_ !< laplace 
  integer, parameter :: op_ldbeta_id_ = 2 + op_ldprob_start_    !< beta
  integer, parameter :: op_ldgamma_id_ = 3 + op_ldprob_start_   !< gamma
  integer, parameter :: op_ldnorm_id_ = 4 + op_ldprob_start_    !< normal
  integer, parameter :: op_ldmvnorm1_id_ = 5 + op_ldprob_start_ !< multivariate normal
  integer, parameter :: op_mvnormpost1_id_ = 6 + op_ldprob_start_ !< multivariate normal posterior, with prior precision matrix
  integer, parameter :: op_mvnormpost2_id_ = 7 + op_ldprob_start_ !< multivariate normal posterior, with covariance matrix
  !> @}

  !> @defgroup env__operator_ids__objectives_ Objective IDs
  !! Operators implementing objective functions
  !! @{
  integer, parameter :: op_obj_start_ = 801                    !< start value for objective function operators
  integer, parameter :: op_obj_end_ = 900                      !< end values for objective fucntion operators
  integer, parameter :: op_binentropy_id_ = 0 + op_obj_start_ !< binary entropy id
  integer, parameter :: op_crossentropy_id_ = 1 + op_obj_start_ !< cross-entropy id
  integer, parameter :: op_mse_id_ = 2 + op_obj_start_ !< mean squared error id
  integer, parameter :: op_mae_id_ = 3 + op_obj_start_ !< mean absolute error id
  integer, parameter :: op_lkhnorm1_id_ = 4 + op_obj_start_ !< normal likelihood for i.i.d. variables 
  integer, parameter :: op_lkhnorm2_id_ = 5 + op_obj_start_ !< weighted normal likelihood for i.i.d. variables
  integer, parameter :: op_logit_binentropy_id_ = 6 + op_obj_start_ !< logit binary-entropy (takes logit units as input predictons)
  integer, parameter :: op_logit_crossentropy1_id_ = 7 + op_obj_start_ !< logit cross-entropy (takes logit units as input predictions)
  integer, parameter :: op_logit_crossentropy2_id_ = 8 + op_obj_start_ !< logit cross-entropy along dimension (takes logit units as input predictions)
  !> @}

  !> @defgroup env__operator_ids__regularisations_ Regularisations IDs
  !! Operators implemeting regularisation techniques
  !! @{
  integer, parameter :: op_regular_start_ = 901 !< start value for regularisation operators ids
  integer, parameter :: op_regular_end_ = 1000 !< end value for regularization operators ids
  integer, parameter :: op_dropout_id_ = 0 + op_regular_start_ !< dropout 
  !!> @}

  integer, parameter :: op_conv_start_ = 1001
  integer, parameter :: op_conv_end_ = 1100
  integer, parameter :: op_conv_id_ = op_conv_start_
  integer, parameter :: op_maxpool_id_ = 1 + op_conv_start_

  !> @defgroup env__operators_ids__feed_ Feed Operators IDs
  !! Operators for feeding @a numbers with data
  !! @{
  integer, parameter :: op_feed_start_ = 2001
  integer, parameter :: op_feed_end_ = 2100
  integer, parameter :: op_feeding_number_id_ = op_feed_start_ !< feeding number operator id
  integer, parameter :: op_feeding_sequence_id_ = 1 + op_feed_start_ !< feeding sequence operator id
  integer, parameter :: op_feed_id_ = op_feed_end_ !< feed operator id
  !> @}

  !> @defgrouo env__operators_ids__modules_ Module Operators IDs
  !! @deprecated
  !! @{
  integer, parameter :: op_modules_start_ = 1101
  integer, parameter :: op_module_end_ = 1200
  integer, parameter :: op_lstm_id_ = op_modules_start_
  !> @}
  
  !> @}

  !> @defgroup env__calculation_modes_ Calculation Mode IDs
  !! Ids identifying different calculation modes. Calculation modes
  !! determine different beahviours for certain operators.
  !! @{
  integer, parameter :: training_mode_ = 0 !< mode indicating model training
  integer, parameter :: evaluation_mode_ = 1 !< mode indicating model evaluation
  integer, parameter :: modes_(2) = [training_mode_, evaluation_mode_] !< All avaialble modes
  !> @}

  !> @}

contains

  !> Checks if an operator id referes to a feeding operator
  !! @param[in] x @c integer, operator id
  !! @return @c logical, @c .true. if the operators refers to a feeding operator, @c .false. otherwise 
  pure function is_feed (x) result(ans)
    implicit none
    integer, intent(in) :: x
    logical :: ans
    ans = x >= op_feed_start_ .and. x < op_feed_end_
  end function is_feed
  
end module env



	
