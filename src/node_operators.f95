module node_operators

  use omp_lib
  use env
  use types
  use registers, only: sp_NUMBERS_, dp_NUMBERS_, sp_NODES_, dp_NODES_
  use errwarn
  use utils, only: append_to_array
  use operators

  implicit none

  private

  public &
       node__op, &
       node__fw, &
       node__bw, &
       node__fw_zero, &
       node__bw_zero

  character(len=*), parameter :: mod_node_operators_name_ = 'node_operators'

  !> Applies the node operators to the its inputs and store the
  !! result in the output.
  !! @param[in] nd sp_node or dp_node, array of dimension (:)
  interface node__op
     module procedure sp_node__op
     module procedure dp_node__op
  end interface node__op

  !> Applies forward differentiation to a node depeding on its operators.
  !! @param[in] nd sp_node or dp_node, array of dimension (:)
  interface node__fw
     module procedure sp_node__fw
     module procedure dp_node__fw
  end interface node__fw

  !> Applies bakward differentiation to a node depending on its operator.
  !! @param[in] nd sp_node or dp_node, array of dimension (:)
  interface node__bw
     module procedure sp_node__bw
     module procedure dp_node__bw
  end interface node__bw

  !> Resets the gradient as needed to perform
  !! forward differentiation
  interface node__fw_zero
     module procedure sp_node__bw_zero
     module procedure dp_node__bw_zero
  end interface node__fw_zero

  !> Resets the gradients as needed to perform
  !! backward differentiation
  interface node__bw_zero
     module procedure sp_node__bw_zero
     module procedure dp_node__bw_zero
  end interface node__bw_zero
     
contains

  include "./node_operators/activations/sp_node.f95"
  include "./node_operators/activations/dp_node.f95"

  include "./node_operators/binary/sp_node.f95"
  include "./node_operators/binary/dp_node.f95"

  include "./node_operators/kernels/sp_node.f95"
  include "./node_operators/kernels/dp_node.f95"

  include "./node_operators/matrix/sp_node.f95"
  include "./node_operators/matrix/dp_node.f95"

  include "./node_operators/modifiers/sp_node.f95"
  include "./node_operators/modifiers/dp_node.f95"

  include "./node_operators/objectives/sp_node.f95"
  include "./node_operators/objectives/dp_node.f95"

  include "./node_operators/reductions/sp_node.f95"
  include "./node_operators/reductions/dp_node.f95"

  include "./node_operators/regularisations/sp_node.f95"
  include "./node_operators/regularisations/dp_node.f95"

  include "./node_operators/stats/sp_node.f95"
  include "./node_operators/stats/dp_node.f95"

  include "./node_operators/unary/sp_node.f95"
  include "./node_operators/unary/dp_node.f95"

  include "./node_operators/null/sp_node.f95"
  include "./node_operators/null/dp_node.f95"

  include "./node_operators/op/sp_node.f95"
  include "./node_operators/op/dp_node.f95"
  
  include "./node_operators/fw/sp_node.f95"
  include "./node_operators/fw/dp_node.f95"

  include "./node_operators/bw/sp_node.f95"
  include "./node_operators/bw/dp_node.f95"

  include "./node_operators/zero/sp_node.f95"
  include "./node_operators/zero/dp_node.f95"

  include "./node_operators/convolution/sp_node.f95"
  include "./node_operators/convolution/dp_node.f95"
  
end module node_operators
