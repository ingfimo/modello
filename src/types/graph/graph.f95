!> @defgroup types__primitives__graphs_ Type Primitives for Graphs
!! @author Filippo Monari
!! @{

!> Returns .true. if the 'graph' has been
!!deallocated correctly.
!! @param[in] x 'graph'
pure function graph__is_deallocated (x) result(ans)
  implicit none
  class(graph), intent(in) :: x
  logical :: ans
  ans = &
       .not. allocated(x%nodes) &
       .and. &
       .not. allocated(x%posts) &
       .and. &
       x%id == 0
end function graph__is_deallocated

!> Returns .true. if the 'graph' has been
!! correctly allocated.
!! @param[in] x 'graph'
pure function graph__is_allocated (x) result(ans)
  implicit none
  class(graph), intent(in) :: x
  logical :: ans
  ans = &
       allocated(x%nodes) &
       .and. &
       allocated(x%posts) &
       .and. &
       x%id > 0
end function graph__is_allocated

!> Returns the size (number of nodes) of
!! the 'graph'.
!! @param[in] x
pure function graph__size (x) result(sz)
  implicit none
  class(graph), intent(in) :: x
  integer :: sz
  sz = -1
  if (is_allocated(x)) sz = size(x%nodes)
end function graph__size

!> Returns .true. if the 'graph' is empty
!! (has no nodes).
!! @param[in] x 'graph'
pure function graph__is_empty (x) result(ans)
  implicit none
  class(graph), intent(in) :: x
  logical :: ans
  ans = .false.
  if (is_allocated(x)) ans = get_size(x) == 0
end function graph__is_empty

!> @}
