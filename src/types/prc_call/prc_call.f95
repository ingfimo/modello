!> @defgroup types__primitives__prc_calls_ Type Primitives for Procedure Calls
!! @author Filippo Monari
!! @{

!> Returns .true. if the procedure call object (prc_call)
!! is allocated correctly.
!! @param[in] x prc_call object
pure function prc_call__is_allocated (x) result(ans)
  implicit none
  type(prc_call), intent(in) :: x
  logical :: ans
  ans = allocated(x%prcname) .and. allocated(x%modname) .and. associated(x%next)
end function prc_call__is_allocated

!> @}
