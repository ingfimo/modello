!> Returns .true. if the 'optim' has been
!! allocated correctly.
!! @param[in] x 'optim'
pure function xp_opt__is_allocated (x) result(ans)
  implicit none
  type(xp_opt), intent(in) :: x
  logical :: ans
  ans = &
       allocated(x%xin) &
       .and. &
       allocated(x%xab) &
       .and. &
       x%iter >= 0 &
       .and. &
       x%id > 0 &
       .and. &
       allocated(x%v1) &
       .and. &
       allocated(x%v2)
end function xp_opt__is_allocated

!> Returns .true. if the 'optim' has been
!! deallocated correctly.
!! @param[in] x 'optim'
pure function xp_opt__is_deallocated (x) result(ans)
  implicit none
  type(xp_opt), intent(in) :: x
  logical :: ans
  ans = &
       .not. allocated(x%xin) &
       .and. &
       .not. allocated(x%xab) &
       .and. &
       x%iter < 0 &
       .and. &
       x%id == 0 &
       .and. &
       .not. allocated(x%v1) &
       .and. &
       .not. allocated(x%v2)
end function xp_opt__is_deallocated
