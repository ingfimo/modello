!> @defgroup types__primitives__optims_ Type Primitives for Optimisers
!! @author Filippo Monari
!! @{

!> Returns .true. if the 'optim' has been
!! allocated correctly.
!! @param[in] x 'optim'
! pure function opt__is_allocated__0 (x) result(ans)
!   implicit none
!   class(opt), intent(in) :: x
!   logical :: ans
!   ans = &
!        allocated(x%xin) &
!        .and. &
!        x%iter >= 0 
! end function opt__is_allocated__0

! !> Returns .true. if the 'optim' has been
! !! deallocated correctly.
! !! @param[in] x 'optim'
! pure function opt__is_deallocated__0 (x) result(ans)
!   implicit none
!   class(opt), intent(in) :: x
!   logical :: ans
!   ans = &
!        .not.allocated(x%xin) &
!        .and. &
!        x%iter < 0 
! end function opt__is_deallocated__0

!> @}
