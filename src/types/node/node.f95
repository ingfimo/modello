!> Returns .true. if the 'node' has been
!!deallocated correctly.
!! @param[in] x 'node'
pure function nodeattrs__is_deallocated__0 (x) result(ans)
  implicit none
  class(nodeattrs), intent(in) :: x
  logical :: ans
  ans = &
       x%id == 0 &
       .and. &
       x%opid == 0 &
       .and. &
       .not. allocated(x%out) &
       .and. &
       .not. allocated(x%in) &
       .and. &
       .not. allocated(x%ipar) 
end function nodeattrs__is_deallocated__0

!> Returns .true. if the 'node' has been
!! allocated correctly.
!! @param[in] x 'node'
pure function nodeattrs__is_allocated__0 (x) result(ans)
  implicit none
  class(nodeattrs), intent(in) :: x
  logical :: ans
  ans = &
       x%opid > 0 &
       .and. &
       x%id > 0 &
       .and. &
       allocated(x%out) &
       .and. &
       allocated(x%in) &
       .and. &
       allocated(x%ipar) 
end function nodeattrs__is_allocated__0

