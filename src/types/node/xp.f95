!> Returns .true. if the 'node' has been
!!deallocated correctly.
!! @param[in] x 'node'
pure function xp_nodeattrs__is_deallocated (x) result(ans)
  implicit none
  type(xp_nodeattrs), intent(in) :: x
  logical :: ans
  ans = &
       nodeattrs__is_deallocated__0(x) &
       .and. &
       .not. allocated(x%rpar)
end function xp_nodeattrs__is_deallocated

!> Returns .true. if the 'node' has been
!! allocated correctly.
!! @param[in] x 'node'
pure function xp_nodeattrs__is_allocated (x) result(ans)
  implicit none
  type(xp_nodeattrs), intent(in) :: x
  logical :: ans
  ans = &
       nodeattrs__is_allocated__0(x) &
       .and. &
       allocated(x%rpar)
end function xp_nodeattrs__is_allocated

pure function xp_node__is_deallocated (x) result(ans)
  implicit none
  type(xp_node), intent(in) :: x
  logical :: ans
  ans = &
       is_deallocated(x%attrs) &
       .and. &
       .not. associated(x%op) &
       .and. &
       .not. associated(x%fw) &
       .and. &
       .not. associated(x%bw)
end function xp_node__is_deallocated

pure function xp_node__is_allocated (x) result(ans)
  implicit none
  type(xp_node), intent(in) :: x
  logical :: ans
  ans = .false.
  if (associated(x%attrs)) then
     ans = &
          is_allocated(x%attrs) &
          .and. &
          associated(x%op) &
          .and. &
          associated(x%fw) &
          .and. &
          associated(x%bw)
  end if
end function xp_node__is_allocated


