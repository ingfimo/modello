!> Returns .true. if the 'number' has been
!! correctly deallocated.
!! @param[in] x prc_call object
pure function xp_number__is_deallocated (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  logical :: ans
  ans = &
       number__is_deallocated__0(x) &
       .and. &
       .not. associated(x%v) &
       .and. &
       .not. associated(x%dv)
end function xp_number__is_deallocated

!> Returns .true. if the 'number' has been
!! allocated correctly.
!! @param[in] x 'number'
pure function xp_number__is_allocated (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  logical :: ans
  ans = number__is_allocated__0(x)
  if (ans) ans = &
       associated(x%v) &
       .and. &
       associated(x%dv) 
end function xp_number__is_allocated

!> Returns .true. if the number as a gradient.
!! @param[in] x 'number'
pure function xp_number__has_dx (x) result(ans)
  implicit none
  type(xp_number), intent(in) :: x
  logical :: ans
  ans = .false.
  if (is_allocated(x)) ans = size(x%dv) > 0
end function xp_number__has_dx

!> Gives shape to rank 2 'number'.
!! @param[in] x 'number'
!! @param[out] xs double precision(:,:), pointer
subroutine xp_number__with_shape__1 (x, xs, dx)
  implicit none
  type(xp_number), intent(in), target :: x
  real(kind=xp_), intent(out), pointer :: xs(:,:)
  logical, intent(in) ::dx
  if (dx) then
     xs(1:x%shp(1),1:x%shp(2)) => x%dv
  else
     xs(1:x%shp(1),1:x%shp(2)) => x%v
  end if
end subroutine xp_number__with_shape__1

!> Gives shape to rank 3 'number'.
!! @param[in] x 'number'
!! @param[out] xs double precision(:,:,:), pointer
subroutine xp_number__with_shape__2 (x, xs, dx)
  implicit none
  type(xp_number), intent(in), target :: x
  real(kind=xp_), intent(out), pointer :: xs(:,:,:)
  logical, intent(in) :: dx
  if (dx) then
     xs(1:x%shp(1),1:x%shp(2),1:x%shp(3)) => x%dv
  else
     xs(1:x%shp(1),1:x%shp(2),1:x%shp(3)) => x%v
  end if
end subroutine xp_number__with_shape__2
