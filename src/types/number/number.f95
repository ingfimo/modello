!> Returns .true. if the 'number' has been
!! correctly deallocated.
!! @param[in] x prc_call object
pure function number__is_deallocated__0 (x) result(ans)
  implicit none
  class(number), intent(in) :: x
  logical :: ans
  ans = &
       x%id == 0 &
       .and. &
       all(x%init == init_null_) &
       .and. &
       .not. allocated(x%shp)  
end function number__is_deallocated__0

!> Returns .true. if the 'number' has been
!! initialised correctly but does not contains
!! any values yet.
!! @param[in] x 'number'
pure function number__is_empty (x) result(ans)
  implicit none
  class(number), intent(in) :: x
  logical :: ans
  ans = get_size(x) == 0
end function number__is_empty

!> Returns .true. if the 'number' has been
!! allocated correctly.
!! @param[in] x 'number'
pure function number__is_allocated__0 (x) result(ans)
  implicit none
  class(number), intent(in) :: x
  logical :: ans
  ans = &
       all(x%init /= init_null_) &
       .and. &
       allocated(x%shp) &
       .and. &
       x%id > 0
end function number__is_allocated__0

!> Returns the size of the 'number'.
!! @param[in] x 'number'
pure function number__size__1 (x) result(sz)
  implicit none
  class(number), intent(in) :: x
  integer :: sz
  sz = -1
  if (number__is_allocated__0(x)) sz = product(x%shp)
end function number__size__1

!> Returns the size of the 'number'
!! along the i-th dimension
pure function number__size__2 (x, i) result(sz)
  implicit none
  class(number), intent(in) :: x
  integer, intent(in) :: i
  integer :: sz
  sz = -1
  if (number__is_allocated__0(x) .and. i <= get_rank(x)) sz = x%shp(i)
end function number__size__2

!> Returns the rank (number of dimensions) of a 'number'.
!! @param[in] x 'number'
pure function number__rank (x) result(rnk)
  implicit none
  class(number), intent(in) :: x
  integer :: rnk
  rnk = -1
  if (number__is_allocated__0(x)) rnk = size(x%shp)
end function number__rank

!> Returns the rank of a 'number' after having discarded
!! the degenerate dimensions (thos with size 1)
!! @param[in] x 'number'
pure function number__net_rank (x) result(rnk)
  implicit none
  class(number), intent(in) :: x
  integer :: rnk
  rnk = -1
  if (number__is_allocated__0(x)) rnk = size(pack(x%shp, x%shp > 1))
end function number__net_rank
