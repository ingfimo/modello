void F77_NAME(intrf_f__number__assign)(int *id1, int *id2);
extern SEXP intrf_c__number__assign (SEXP id1, SEXP id2) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__number__assign)(INTEGER(id1), INTEGER(id2));
  UNPROTECT(1);
  return(ans);
}

void F77_NAME(intrf_f__number__slice)(int *id, int *n, int *s, int *idout);
extern SEXP intrf_c__number__slice (SEXP id, SEXP s) {
  SEXP ans;
  int n = length(s);
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__slice)(INTEGER(id), &n, INTEGER(s), INTEGER(ans));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__flat_slice)(int *id, int *n, int *s, int *idout);
extern SEXP intrf_c__number__flat_slice (SEXP id, SEXP s) {
  SEXP ans;
  int n = length(s);
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__flat_slice)(INTEGER(id), &n, INTEGER(s), INTEGER(ans));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__contiguous_slice)(int *id, int *s1, int *s2, int *idout);
extern SEXP intrf_c__number__contiguous_slice (SEXP id, SEXP s1, SEXP s2) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__contiguous_slice)(INTEGER(id), INTEGER(s1), INTEGER(s2),
					      INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__reshape)(int *id, int *n, int *shp, int *idout);
extern SEXP intrf_c__number__reshape (SEXP id, SEXP shp) {
  SEXP idout;
  int n = length(shp);
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__reshape)(INTEGER(id), &n, INTEGER(shp), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__drop_shape)(int *id, int *idout);
extern SEXP intrf_c__number__drop_shape (SEXP id) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__drop_shape)(INTEGER(id), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__bind)(int *id1, int *id2, int *k, int *idout);
extern SEXP intrf_c__number__bind (SEXP id1, SEXP id2, SEXP k) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__bind)(INTEGER(id1), INTEGER(id2), INTEGER(k), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__embeddings)(int *idf, int *idx, int *shp, int *n, int*idout);
extern SEXP intrf_c__number__embeddings (SEXP idf, SEXP idx, SEXP shp) {
  SEXP idout;
  int n = length(shp);
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__embeddings)(INTEGER(idf), INTEGER(idx), INTEGER(shp), &n, INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__transpose)(int *idx, int *idout);
extern SEXP intrf_c__number__transpose (SEXP idx) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__transpose)(INTEGER(idx), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__feeding_number)(int *id, int *idout, int *indexes, int *shp, int *bsz, int *m, int *n);
extern SEXP intrf_c__number__feeding_number (SEXP id, SEXP indexes, SEXP shp, SEXP bsz) {
  SEXP ans;
  int m = length(shp);
  int n = length(indexes);
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__feeding_number)(INTEGER(id), INTEGER(ans), INTEGER(indexes), INTEGER(shp),
					    INTEGER(bsz), &m, &n);
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__feeding_sequence)(int *id, int *idout, int *indexes, int *shp, int *l, int *bsz, int *b, int *m, int *n);
extern SEXP intrf_c__number__feeding_sequence (SEXP id, SEXP indexes, SEXP shp, SEXP l, SEXP bsz, SEXP b) {
  SEXP ans;
  int m = length(shp);
  int n = length(indexes);
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__feeding_sequence)(INTEGER(id), INTEGER(ans), INTEGER(indexes), INTEGER(shp),
					      INTEGER(l), INTEGER(bsz), INTEGER(b), &m, &n);
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__feed)(int *id, int *n, int *idout);
extern SEXP intrf_c__number__feed (SEXP id) {
  SEXP ans;
  int n = length(id);
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__feed)(INTEGER(id), &n, INTEGER(ans));
  UNPROTECT(1);
  return(ans);
}
