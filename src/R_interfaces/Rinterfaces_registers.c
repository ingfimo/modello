void F77_NAME(intrf_f__set_mode)(int *m);
extern SEXP intrf_c__set_mode (SEXP m) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__set_mode)(INTEGER(m));
  UNPROTECT(1);
  return(ans);
}

void F77_NAME(intrf_f__set_dtyp)(int *dtyp);
extern SEXP intrf_c__set_dtyp (SEXP dtyp) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__set_dtyp)(INTEGER(dtyp));
  UNPROTECT(1);
  return(ans);
}

void F77_NAME(intrf_f__set_inposts)(int *x);
extern SEXP intrf_c__set_inposts (SEXP x) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__set_inposts)(INTEGER(x));
  UNPROTECT(1);
  return(ans);
}

void F77_NAME(intrf_f__get_dp)(int *x);
extern SEXP intrf_c__get_dp () {
  SEXP ans;
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__get_dp)(INTEGER(ans));
  UNPROTECT(1);
  return(ans);
}

void F77_NAME(intrf_f__get_sp)(int *x);
extern SEXP intrf_c__get_sp () {
  SEXP ans;
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__get_sp)(INTEGER(ans));
  UNPROTECT(1);
  return(ans);
}

void F77_NAME(intrf_f__get_training_mode)(int *x);
extern SEXP intrf_c__get_training_mode () {
  SEXP ans;
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__get_training_mode)(INTEGER(ans));
  UNPROTECT(1);
  return(ans);
}

void F77_NAME(intrf_f__get_evaluation_mode)(int *x);
extern SEXP intrf_c__get_evaluation_mode () {
  SEXP ans;
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__get_evaluation_mode)(INTEGER(ans));
  UNPROTECT(1);
  return(ans);
}

void F77_NAME(intrf_f__allocate_hash_tables)(int *n);
extern SEXP intrf_c__allocate_hash_tables (SEXP n) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__allocate_hash_tables)(INTEGER(n));
  UNPROTECT(1);
  return(ans);
}
