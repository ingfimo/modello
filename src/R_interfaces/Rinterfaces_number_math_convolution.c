void F77_NAME(intrf_f__number__conv)(int *id1, int *id2, int *idout, int *stride, int *n1,
				       int *dilation, int *n2, int *pad, int *n3);
extern SEXP intrf_c__number__conv (SEXP id1, SEXP id2, SEXP stride, SEXP dilation, SEXP pad) {
  SEXP ans;
  int n1 = length(stride);
  int n2 = length(dilation);
  int n3 = length(pad);
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__conv)(INTEGER(id1), INTEGER(id2), INTEGER(ans),
				    INTEGER(stride), &n1, INTEGER(dilation), &n2,
				    INTEGER(pad), &n3);
  UNPROTECT(1);
  return(ans);
}

void F77_NAME(intrf_f__number__maxpool)(int *id1, int *idout, int *kshp, int *n1,
					int *stride, int *n2, int *dilation, int *n3, int *pad, int *n4);
extern SEXP intrf_c__number__maxpool (SEXP id1, SEXP kshp, SEXP stride, SEXP dilation, SEXP pad) {
  SEXP ans;
  int n1 = length(kshp);
  int n2 = length(stride);
  int n3 = length(dilation);
  int n4 = length(pad);
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__maxpool)(INTEGER(id1), INTEGER(ans), INTEGER(kshp), &n1, INTEGER(stride), &n2,
				     INTEGER(dilation), &n3, INTEGER(pad), &n4);
  UNPROTECT(1);
  return(ans);
}
