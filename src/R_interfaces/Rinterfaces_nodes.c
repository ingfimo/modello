void F77_NAME(intrf_f__allocate_nodes)(int *n);
extern SEXP intrf_c__allocate_nodes (SEXP n) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__allocate_nodes)(INTEGER(n));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__deallocate_nodes)();
extern SEXP intrf_c__deallocate_nodes () {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__deallocate_nodes)();
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__node__get_ipar)(int *id, int *sz, int *n, int *p);
extern SEXP intrf_c__node__get_ipar (SEXP id) {
  SEXP ans;
  int p = 0;
  int sz = 1;
  int n = 1;
  F77_CALL(intrf_f__node__get_ipar)(INTEGER(id), &sz, &n, &p);
  PROTECT(ans=allocVector(INTSXP, p));
  sz = 0;
  F77_CALL(intrf_f__node__get_ipar)(INTEGER(id), &sz, &p, INTEGER(ans));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__node__get_rpar)(int *id, int *sz, int *n, double *p);
extern SEXP intrf_c__node__get_rpar (SEXP id) {
  SEXP ans;
  double p = 0;
  int n = 1;
  int sz = 1;
  F77_CALL(intrf_f__node__get_rpar)(INTEGER(id), &sz, &n, &p);
  PROTECT(ans=allocVector(REALSXP, n));
  sz = 0;
  F77_CALL(intrf_f__node__get_rpar)(INTEGER(id), &sz, &n, REAL(ans));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__node__get_state)(int *id, int *i, int *sz, int *n, int *s);
extern SEXP intrf_c__node__get_state (SEXP id, SEXP i) {
  SEXP ans;
  int s = 0;
  int sz = 1;
  int n = 1;
  F77_CALL(intrf_f__node__get_state)(INTEGER(id), INTEGER(i), &sz, &n, &s);
  PROTECT(ans=allocVector(INTSXP, s));
  sz = 0;
  F77_CALL(intrf_f__node__get_state)(INTEGER(id), INTEGER(i), &sz, &s, INTEGER(ans));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__node__set_state)(int *id, int *i, int *n, int *s);
extern SEXP intrf_c__node__set_state (SEXP id, SEXP i, SEXP s) {
  SEXP ans;
  int n=length(s);
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__node__set_state)(INTEGER(id), INTEGER(i), &n, INTEGER(s));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__allocate_graphs)(int *n);
extern SEXP intrf_c__allocate_graphs (SEXP n) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__allocate_graphs)(INTEGER(n));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__graph__is_allocated)(int *id, int *ans);
extern SEXP intrf_c__graph__is_allocated (SEXP id) {
  SEXP ans;
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__graph__is_allocated)(INTEGER(id), INTEGER(ans));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__deallocate_graphs)();
extern SEXP intrf_c__deallocate_graphs () {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__deallocate_graphs)();
  UNPROTECT(1);
  return(ans);
} 
void F77_NAME(intrf_f__graph__open)(int *i, int* wdx);
extern SEXP intrf_c__graph__open (SEXP i, SEXP wdx) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__graph__open)(INTEGER(i), INTEGER(wdx));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__graph__close)();
extern SEXP intrf_c__graph__close () {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__graph__close)();
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__get_graphi)(int *i);
extern SEXP intrf_c__get_graphi () {
  SEXP ans;
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__get_graphi)(INTEGER(ans));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__graph__pop)(int *gi);
extern SEXP intrf_c__graph__pop (SEXP gi) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__graph__pop)(INTEGER(gi));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__graph__gc)();
extern SEXP intrf_c__graph__gc () {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__graph__gc)();
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__op)(int *ndi);
extern SEXP intrf_c__number__op (SEXP ndi) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__number__op)(INTEGER(ndi));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__fw)(int *ndi);
extern SEXP intrf_c__number__fw (SEXP ndi) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__number__fw)(INTEGER(ndi));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__bw_zero)(int *ndi);
extern SEXP intrf_c__number__bw_zero (SEXP ndi) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__number__bw_zero)(INTEGER(ndi));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__bw)(int *ndi);
extern SEXP intrf_c__number__bw (SEXP ndi) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__number__bw)(INTEGER(ndi));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__graph__op)(int *ndi);
extern SEXP intrf_c__graph__op (SEXP ndi) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__graph__op)(INTEGER(ndi));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__graph__fw)(int *ndi);
extern SEXP intrf_c__graph__fw (SEXP ndi) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__graph__fw)(INTEGER(ndi));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__graph__bw_zero)(int *ndi);
extern SEXP intrf_c__graph__bw_zero (SEXP ndi) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__graph__bw_zero)(INTEGER(ndi));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__graph__bw)(int *ndi);
extern SEXP intrf_c__graph__bw (SEXP ndi) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__graph__bw)(INTEGER(ndi));
  UNPROTECT(1);
  return(ans);
}
