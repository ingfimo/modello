void F77_NAME(intrf_f__number__dropout)(int *id1, double *r, int *idout);
extern SEXP intrf_c__number__dropout (SEXP id1, SEXP r) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__dropout)(INTEGER(id1), REAL(r), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
