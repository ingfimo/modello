void F77_NAME(intrf_f__number__rank)(int *id, int *r);
extern SEXP intrf_c__number__rank (SEXP id) {
  SEXP r;
  PROTECT(r=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__rank)(INTEGER(id), INTEGER(r));
  UNPROTECT(1);
  return(r);
}
void F77_NAME(intrf_f__allocate_numbers)(int *n);
extern SEXP intrf_c__allocate_numbers (SEXP n) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__allocate_numbers)(INTEGER(n));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__is_allocated)(int *id, int *ans);
extern SEXP intrf_c__number__is_allocated (SEXP id) {
  SEXP ans;
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__is_allocated)(INTEGER(id), INTEGER(ans));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__has_dx)(int *id, int *ans);
extern SEXP intrf_c__number__has_dx (SEXP id) {
  SEXP ans;
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__has_dx)(INTEGER(id), INTEGER(ans));
  unprotect(1);
  return(ans);
}
void F77_NAME(intrf_f__deallocate_numbers)();
extern SEXP intrf_c__deallocate_numbers () {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__deallocate_numbers)();
  UNPROTECT(1);
  return(ans);
} 
void F77_NAME(intrf_f__number__append)(int *id, int *r, int *shp, int *dx);
extern SEXP intrf_c__number__append (SEXP shp, SEXP dx) {
  SEXP ans;
  int r = length(shp);
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__append)(INTEGER(ans), &r, INTEGER(shp), INTEGER(dx));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__size)(int *i, int *sz);
extern SEXP intrf_c__number__size (SEXP id) {
  SEXP ans;
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__size)(INTEGER(id), INTEGER(ans));
  UNPROTECT(1);
  return(ans);
} 
void F77_NAME(intrf_f__number__shape)(int* id, int *r, int *shp);
extern SEXP intrf_c__number__shape (SEXP id) {
  SEXP r, ans;
  r = intrf_c__number__rank(id);
  PROTECT(ans=allocVector(INTSXP, INTEGER(r)[0]));
  F77_CALL(intrf_f__number__shape)(INTEGER(id), INTEGER(r), INTEGER(ans));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__set_v)(int *id, int *n, double *x);
extern SEXP intrf_c__number__set_v (SEXP id, SEXP x) {
  SEXP ans;
  int n = length(x);
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__number__set_v)(INTEGER(id), &n, REAL(x));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__set_v_sequence)(int *id, int *m, int *n, double *x);
extern SEXP intrf_c__number__set_v_sequence (SEXP id, SEXP x) {
  SEXP ans;
  int m = length(id);
  int n = nrows(x);
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__number__set_v_sequence)(INTEGER(id), &m, &n, REAL(x));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__set_dv)(int *i, int *n, double *x);
extern SEXP intrf_c__number__set_dv (SEXP i, SEXP x) {
  SEXP ans;
  int n = length(x);
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__number__set_dv)(INTEGER(i), &n, REAL(x));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__set_slice_v)(int *id, double *v, int *s, int *m, int *n, int *l);
extern SEXP intrf_c__number__set_slice_v (SEXP id, SEXP v, SEXP s) {
  SEXP ans;
  int l = length(v);
  int m = nrows(s);
  int n = ncols(s);
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__number__set_slice_v)(INTEGER(id), REAL(v), INTEGER(s), &m, &n, &l);
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__set_slice_dv)(int *id, double *dv, int *s, int *m, int *n, int *l);
extern SEXP intrf_c__number__set_slice_dv (SEXP id, SEXP dv, SEXP s) {
  SEXP ans;
  int l = length(dv);
  int m = nrows(s);
  int n = ncols(s);
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__number__set_slice_dv)(INTEGER(id), REAL(dv), INTEGER(s), &m, &n, &l);
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__set_flat_slice_v)(int *id, double *v, int *s, int *m, int *l);
extern SEXP intrf_c__number__set_flat_slice_v (SEXP id, SEXP v, SEXP s) {
  SEXP ans;
  int l = length(v);
  int m = length(s);
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__number__set_flat_slice_v)(INTEGER(id), REAL(v), INTEGER(s), &m, &l);
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__set_flat_slice_dv)(int *id, double *dv, int *s, int *m, int *l);
extern SEXP intrf_c__number__set_flat_slice_dv (SEXP id, SEXP dv, SEXP s) {
  SEXP ans;
  int l = length(dv);
  int m = length(s);
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__number__set_flat_slice_dv)(INTEGER(id), REAL(dv), INTEGER(s), &m, &l);
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__get_v)(int *id, int *n, double *x);
extern SEXP intrf_c__number__get_v (SEXP id) {
  SEXP r, sz, shp, ans;
  r = intrf_c__number__rank(id);
  sz = intrf_c__number__size(id);
  shp = intrf_c__number__shape(id);
  PROTECT(ans=allocVector(REALSXP, INTEGER(sz)[0]));
  F77_CALL(intrf_f__number__get_v)(INTEGER(id), INTEGER(sz), REAL(ans));
  if (INTEGER(r)[0] > 0) setAttrib(ans, R_DimSymbol, shp);
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__get_dv)(int *id, int *n, double *x);
extern SEXP intrf_c__number__get_dv (SEXP id) {
  SEXP r, sz, shp, ans;
  r = intrf_c__number__rank(id);
  sz = intrf_c__number__size(id);
  shp = intrf_c__number__shape(id);
  PROTECT(ans=allocVector(REALSXP, INTEGER(sz)[0]));
  F77_CALL(intrf_f__number__get_dv)(INTEGER(id), INTEGER(sz), REAL(ans));
  if (INTEGER(r)[0] > 0) setAttrib(ans, R_DimSymbol, shp);
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__inlock_free)(int * id, int *ans);
extern SEXP intrf_c__number__inlock_free (SEXP id) {
  SEXP ans;
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__inlock_free)(INTEGER(id), INTEGER(ans));
  UNPROTECT(1);
  return(ans);
}

void F77_NAME(intrf_f__number__pop)(int *id);
extern SEXP intrf_c__number__pop (SEXP id) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__number__pop)(INTEGER(id));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__gc)();
extern SEXP intrf_c__number__gc () {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__number__gc)();
  UNPROTECT(1);
  return(ans);
}
