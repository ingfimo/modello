void F77_NAME(intrf_f__number__add)(int *id1, int *id2, int *idout);
extern SEXP intrf_c__number__add (SEXP id1, SEXP id2) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__add)(INTEGER(id1), INTEGER(id2), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__sub)(int *id1, int *id2, int *idout);
extern SEXP intrf_c__number__sub (SEXP id1, SEXP id2) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__sub)(INTEGER(id1), INTEGER(id2), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__mult)(int *id1, int *id2, int *idout);
extern SEXP intrf_c__number__mult (SEXP id1, SEXP id2) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__mult)(INTEGER(id1), INTEGER(id2), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__pow)(int *id1, int *id2, int *idout);
extern SEXP intrf_c__number__pow (SEXP id1, SEXP id2, SEXP typ1, SEXP typ2) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__pow)(INTEGER(id1), INTEGER(id2), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__div)(int *id1, int *id2, int *idout);
extern SEXP intrf_c__number__div (SEXP id1, SEXP id2) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__div)(INTEGER(id1), INTEGER(id2), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
