void F77_NAME(intrf_f__number__sum)(int *idx, int *k, int *idout);
extern SEXP intrf_c__number__sum (SEXP idx, SEXP k) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__sum)(INTEGER(idx), INTEGER(k), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__product)(int *idx, int *k, int *idout);
extern SEXP intrf_c__number__product (SEXP idx, SEXP k) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__product)(INTEGER(idx), INTEGER(k), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__ssq)(int *idx, int *k, int *idout);
extern SEXP intrf_c__number__ssq (SEXP idx, SEXP k) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__ssq)(INTEGER(idx), INTEGER(k), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
