void F77_NAME(intrf_f__number__ksqexp)(int *idx1, int *idx2, int *ida, int *idb, int *idout);
extern SEXP intrf_c__number__ksqexp (SEXP idx1, SEXP idx2, SEXP ida, SEXP idb) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__ksqexp)(INTEGER(idx1), INTEGER(idx2),
				    INTEGER(ida), INTEGER(idb), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
