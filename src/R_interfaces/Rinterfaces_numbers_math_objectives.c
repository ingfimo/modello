void F77_NAME(intrf_f__number__binentropy)(int *id1, int *id2, int *idout);
extern SEXP intrf_c__number__binentropy (SEXP id1, SEXP id2, SEXP k) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__binentropy)(INTEGER(id1), INTEGER(id2), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__logit_binentropy)(int *id1, int *id2, int *idout);
extern SEXP intrf_c__number__logit_binentropy (SEXP id1, SEXP id2) {
  SEXP ans;
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__logit_binentropy)(INTEGER(id1), INTEGER(id2), INTEGER(ans));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__crossentropy)(int *id1, int *id2, int *idout);
extern SEXP intrf_c__number__crossentropy (SEXP id1, SEXP id2) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__crossentropy)(INTEGER(id1), INTEGER(id2), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__logit_crossentropy)(int *id1, int *id2, int *k, int *idout);
extern SEXP intrf_c__number__logit_crossentropy (SEXP id1, SEXP id2, SEXP k) {
  SEXP ans;
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__logit_crossentropy)(INTEGER(id1), INTEGER(id2), INTEGER(k),
						INTEGER(ans));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__number__mse)(int *idy, int *idyh, int *idout);
extern SEXP intrf_c__number__mse (SEXP idy, SEXP idyh) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__mse)(INTEGER(idy), INTEGER(idyh), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__mae)(int *idy, int *idyh, int *idout);
extern SEXP intrf_c__number__mae (SEXP idy, SEXP idyh) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__mae)(INTEGER(idy), INTEGER(idyh), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}  
void F77_NAME(intrf_f__number__lkhnorm)(int *idy, int *idmu, int *ids, int *idw, int *idout);
extern SEXP intrf_c__number__lkhnorm (SEXP idy, SEXP idmu, SEXP ids, SEXP idw) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__lkhnorm)(INTEGER(idy), INTEGER(idmu), INTEGER(ids),
				      INTEGER(idw), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
