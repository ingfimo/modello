void F77_NAME(intrf_f__number__gmmmult__1)(int *transA, int *transB, int *idAlpha, int *idA,
					int *idB, int *idBeta, int *idC, int *idout);
extern SEXP intrf_c__number__gmmmult__1 (SEXP transA, SEXP transB,
				      SEXP idAlpha, SEXP idA, SEXP idB, SEXP idBeta, SEXP idC) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__gmmmult__1)(INTEGER(transA), INTEGER(transB), INTEGER(idAlpha), INTEGER(idA),
				     INTEGER(idB), INTEGER(idBeta), INTEGER(idC), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__gmmmult__2)(int *transA, int *transB, int *idAlpha, int *idA,
					int *idB, int *idout);
extern SEXP intrf_c__number__gmmmult__2 (SEXP transA, SEXP transB, SEXP idAlpha, SEXP idA, SEXP idB) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__gmmmult__2)(INTEGER(transA), INTEGER(transB), INTEGER(idAlpha),
				     INTEGER(idA), INTEGER(idB), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__gmmmult__3)(int *transA, int *transB, int *idA, int *idB, int *idC, int *idout);
extern SEXP intrf_c__number__gmmmult__3 (SEXP transA, SEXP transB, SEXP idA, SEXP idB, SEXP idC) {
  SEXP idout;;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__gmmmult__3)(INTEGER(transA), INTEGER(transB), INTEGER(idA),
				     INTEGER(idB), INTEGER(idC), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__gmmmult__4)(int *transA, int *transB, int *idA, int *idB, int *idout);
extern SEXP intrf_c__number__gmmmult__4 (SEXP transA, SEXP transB, SEXP idA, SEXP idB) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__gmmmult__4)(INTEGER(transA), INTEGER(transB), INTEGER(idA), INTEGER(idB),
				     INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__gmvmult__1)(int *trans, int *idAlpha, int *idA, int *idx,
					   int *idBeta, int *idy, int *idout);
extern SEXP intrf_c__number__gmvmult__1 (SEXP trans, SEXP idAlpha, SEXP idA, SEXP idx,
					 SEXP idBeta, SEXP idy) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__gmvmult__1)(INTEGER(trans), INTEGER(idAlpha), INTEGER(idA),
					INTEGER(idx), INTEGER(idBeta), INTEGER(idy), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__gmvmult__2)(int *trans, int *idAlpha, int *idA, int *idx, int *idout);
extern SEXP intrf_c__number__gmvmult__2 (SEXP trans, SEXP idAlpha, SEXP idA, SEXP idx) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__gmvmult__2)(INTEGER(trans), INTEGER(idAlpha), INTEGER(idA),
					INTEGER(idx), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__gmvmult__3)(int *trans, int *idA, int *idx, int *idy, int *idout);
extern SEXP intrf_c__number__gmvmult__3 (SEXP trans, SEXP idA, SEXP idx, SEXP idy) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__gmvmult__3)(INTEGER(trans), INTEGER(idA), INTEGER(idx),
					INTEGER(idy), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__gmvmult__4)(int *trans, int *idA, int *idx, int *idout);
extern SEXP intrf_c__number__gmvmult__4 (SEXP trans, SEXP idA, SEXP idx) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__gmvmult__4)(INTEGER(trans), INTEGER(idA), INTEGER(idx), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__vvouter__1)(int *idAlpha, int *idx, int *idy, int *idz, int *idout);
extern SEXP intrf_c__number__vvouter__1 (SEXP idAlpha, SEXP idx, SEXP idy, SEXP idz) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__vvouter__1)(INTEGER(idAlpha), INTEGER(idx), INTEGER(idy), INTEGER(idz),
				       INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__vvouter__2)(int *idx, int *idy, int *idz, int *idout);
extern SEXP intrf_c__number__vvouter__2 (SEXP idx, SEXP idy, SEXP idz) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__vvouter__2)(INTEGER(idx), INTEGER(idy), INTEGER(idz), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__vvouter__3)(int *idx, int *idy, int *idout);
extern SEXP intrf_c__number__vvouter__3 (SEXP idx, SEXP idy) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__vvouter__3)(INTEGER(idx), INTEGER(idy), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__vvinner)(int *idx, int *idy, int *idout);
extern SEXP intrf_c__number__vvinner (SEXP idx, SEXP idy) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__vvinner)(INTEGER(idx), INTEGER(idy), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__invMat)(int *idx, int *idout);
extern SEXP intrf_c__number__invMat (SEXP idx) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__invMat)(INTEGER(idx), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__slidemmm)(int *transA, int *transB, int *hashB, int *ida, int *idb,
					 int *idc, int *bm, int *bn, int *hm, int *l, int *b);
extern SEXP intrf_c__number__slidemmm (SEXP transA, SEXP transB, SEXP hashB, SEXP ida, SEXP idb,
				       SEXP bm, SEXP bn, SEXP hm, SEXP l, SEXP b) {
  SEXP idc;
  PROTECT(idc=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__slidemmm)(INTEGER(transA), INTEGER(transB), INTEGER(hashB), INTEGER(ida), INTEGER(idb),
				      INTEGER(idc), INTEGER(bm), INTEGER(bn), INTEGER(hm), INTEGER(l), INTEGER(b));
  UNPROTECT(1);
  return(idc);
}
