void F77_NAME(intrf_f__allocate_gopts)(int *n);
extern SEXP intrf_c__allocate_gopts (SEXP n) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__allocate_gopts)(INTEGER(n));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__deallocate_gopts)();
extern SEXP intrf_c__deallocate_gopts () {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__deallocate_gopts)();
  UNPROTECT(1);
  return(ans);
} 
void F77_NAME(intrf_f__opt__pop)(int *i);
extern SEXP intrf_c__gopt__pop (SEXP i) {
  SEXP ans;
  PROTECT(ans = allocVector(NILSXP, 1));
  F77_CALL(intrf_f__opt__pop)(INTEGER(i));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__opt__append_sgd)(int *i, int *nin, int *xin);
extern SEXP intrf_c__sgd__append (SEXP xin) {
  SEXP ans;
  int nin = length(xin);
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__opt__append_sgd)(INTEGER(ans), &nin, INTEGER(xin));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__opt__sgd_step)(int *xoi, int *gi, int *xout, double *lr, int *niter, double *ftrain);
extern SEXP intrf_c__sgd__step (SEXP xoi, SEXP gi, SEXP xout, SEXP lr, SEXP niter) {
  SEXP ans;
  PROTECT(ans=allocVector(REALSXP, 1));
  F77_CALL(intrf_f__opt__sgd_step)(INTEGER(xoi), INTEGER(gi), INTEGER(xout), REAL(lr),
				   INTEGER(niter), REAL(ans));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__opt__append_sgdwm)(int *i, int *nin, int *xin);
extern SEXP intrf_c__sgdwm__append (SEXP xin) {
  SEXP ans;
  int nin = length(xin);
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__opt__append_sgdwm)(INTEGER(ans), &nin, INTEGER(xin));
  UNPROTECT(1);
  return(ans);
} 
void F77_NAME(intrf_f__opt__sgdwm_step)(int *xoi, int *gi, int *xout, double *lr, double *alpha, int *niter, double *ftrain);
extern SEXP intrf_c__sgdwm__step (SEXP xoi, SEXP gi, SEXP xout, SEXP lr, SEXP alpha, SEXP niter) {
  SEXP ans;
  PROTECT(ans=allocVector(REALSXP, 1));
  F77_CALL(intrf_f__opt__sgdwm_step)(INTEGER(xoi), INTEGER(gi), INTEGER(xout), REAL(lr),
				     REAL(alpha), INTEGER(niter), REAL(ans));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__opt__append_adam)(int *i, int *nin, int *xin);
extern SEXP intrf_c__adam__append (SEXP xin) {
  SEXP ans;
  int nin = length(xin);
  PROTECT(ans=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__opt__append_adam)(INTEGER(ans), &nin, INTEGER(xin));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__opt__adam_step)(int *xoi, int *gi, int *xout, double *lr,
				       double *beta, double *beta2, int *niter, double *ftrain);
extern SEXP intrf_c__adam__step (SEXP xoi, SEXP gi, SEXP xout, SEXP lr, SEXP beta1, SEXP beta2, SEXP niter) {
  SEXP ans;
  PROTECT(ans=allocVector(REALSXP, 1));
  F77_CALL(intrf_f__opt__adam_step)(INTEGER(xoi), INTEGER(gi), INTEGER(xout), REAL(lr),
				    REAL(beta1), REAL(beta2), INTEGER(niter), REAL(ans));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__opt__get_state)(int *id, int *i, int *n, double *state, int *sz);
extern SEXP intrf_c__opt__get_state (SEXP id, SEXP i) {
  SEXP ans;
  SEXP dummy;
  int sz = 1;
  int n = 1;
  PROTECT(dummy=allocVector(REALSXP, n));
  F77_CALL(intrf_f__opt__get_state)(INTEGER(id), INTEGER(i), &n, REAL(dummy), &sz);
  UNPROTECT(1);
  sz = 0;
  PROTECT(ans=allocVector(REALSXP, n));
  F77_CALL(intrf_f__opt__get_state)(INTEGER(id), INTEGER(i), &n, REAL(ans), &sz);
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__opt__set_state)(int *id, int *i, int *n, double *state);
extern SEXP intrf_c__opt__set_state (SEXP id, SEXP i, SEXP state) {
  SEXP ans;
  int n = length(state);
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__opt__set_state)(INTEGER(id), INTEGER(i), &n, REAL(state));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__opt__set_clipping)(int *id, int *clip, double *clipval);
extern SEXP intrf_c__opt__set_clipping (SEXP id, SEXP clip, SEXP clipval) {
  SEXP ans;
  PROTECT(ans=allocVector(NILSXP, 1));
  F77_CALL(intrf_f__opt__set_clipping)(INTEGER(id), INTEGER(clip), REAL(clipval));
  UNPROTECT(1);
  return(ans);
}
void F77_NAME(intrf_f__opt__get_clipping)(int *id, double *clipping);
extern SEXP intrf_c__opt__get_clipping (SEXP id) {
  SEXP ans;
  PROTECT(ans=allocVector(REALSXP, 2));
  F77_CALL(intrf_f__opt__get_clipping)(INTEGER(id), REAL(ans));
  UNPROTECT(1);
  return(ans);
}
