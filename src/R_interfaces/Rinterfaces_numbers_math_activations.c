void F77_NAME(intrf_f__number__sigmoid)(int *id, int *idout);
extern SEXP intrf_c__number__sigmoid (SEXP id) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__sigmoid)(INTEGER(id), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__softplus)(int *id, int *idout);
extern SEXP intrf_c__number__softplus (SEXP id) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__softplus)(INTEGER(id), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__relu)(int *id, int *idout);
extern SEXP intrf_c__number__relu (SEXP id) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__relu)(INTEGER(id), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__leaky_relu)(int *idx, int *ida, int *idout);
extern SEXP intrf_c__number__leaky_relu (SEXP idx, SEXP ida) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__leaky_relu)(INTEGER(idx), INTEGER(ida), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__elu)(int *id, int *ida, int *idout);
extern SEXP intrf_c__number__elu (SEXP id, SEXP ida) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__elu)(INTEGER(id), INTEGER(ida), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__silu)(int *id, int *idout);
extern SEXP intrf_c__number__silu (SEXP id) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__silu)(INTEGER(id), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__swish)(int *idx, int *ida, int *idout);
extern SEXP intrf_c__number__swish (SEXP idx, SEXP ida) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__swish)(INTEGER(idx), INTEGER(ida), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__softmax)(int *id, int *k, int *idout);
extern SEXP intrf_c__number__softmax (SEXP id, SEXP k) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__softmax)(INTEGER(id), INTEGER(k), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
