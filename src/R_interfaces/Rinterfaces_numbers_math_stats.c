void F77_NAME(intrf_f__number__ldexp)(int *idy, int *idlam, int *idout);
extern SEXP intrf_c__number__ldexp (SEXP idy, SEXP idlam) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__ldexp)(INTEGER(idy), INTEGER(idlam), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__ldlaplace)(int *idy, int *idmu, int *idlam, int *idout);
extern SEXP intrf_c__number__ldlaplace (SEXP idy, SEXP idmu, SEXP idlam) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__ldlaplace)(INTEGER(idy), INTEGER(idmu),
				       INTEGER(idlam), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__ldbeta)(int *idy, int *a1, int *a2, int *idout);
extern SEXP intrf_c__number__ldbeta (SEXP idy, SEXP ida1, SEXP ida2) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__ldbeta)(INTEGER(idy), INTEGER(ida1),
				    INTEGER(ida2), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__ldgamma)(int *idy, int *ida, int *idb, int *idout);
extern SEXP intrf_c__number__ldgamma (SEXP idy, SEXP ida, SEXP idb) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__ldgamma)(INTEGER(idy), INTEGER(ida),
				     INTEGER(idb), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__ldnorm)(int *idy, int *idmu, int *ids, int *idout);
extern SEXP intrf_c__number__ldnorm (SEXP idy, SEXP idmu, SEXP ids) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__ldnorm)(INTEGER(idy), INTEGER(idmu),
				    INTEGER(ids), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__ldmvnorm__1)(int *idy, int *idmu, int *idE, int *idout);
extern SEXP intrf_c__number__ldmvnorm__1 (SEXP idy, SEXP idmu, SEXP idE) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__ldmvnorm__1)(INTEGER(idy), INTEGER(idmu),
					 INTEGER(idE), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__mvnorm_posterior)(int *inv, int *ida11, int *ide21, int *ide22, int *idy11, 
						 int *idmu11, int *idout);
extern SEXP intrf_c__number__mvnorm_posterior (SEXP ida11, SEXP ide21, SEXP ide22,
					       SEXP idy11, SEXP idmu11, SEXP inv) {
  SEXP ans;
  PROTECT(ans=allocVector(INTSXP, 2));
  F77_CALL(intrf_f__number__mvnorm_posterior)(INTEGER(inv), INTEGER(ida11), INTEGER(ide21), INTEGER(ide22),
					      INTEGER(idy11), INTEGER(idmu11), INTEGER(ans));
  UNPROTECT(1);
  return(ans);
}
  
