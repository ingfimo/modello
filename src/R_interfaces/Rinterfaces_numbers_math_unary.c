void F77_NAME(intrf_f__number__abs)(int *id, int *idout);
extern SEXP intrf_c__number__abs (SEXP id) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__abs)(INTEGER(id), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__exp)(int *id, int *idout);
extern SEXP intrf_c__number__exp (SEXP id) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__exp)(INTEGER(id), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__log)(int *id, int *idout);
extern SEXP intrf_c__number__log (SEXP id) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__log)(INTEGER(id), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__sin)(int *id, int *idout);
extern SEXP intrf_c__number__sin (SEXP id) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__sin)(INTEGER(id), INTEGER(idout));
  //SET_VECTOR_ELT(ans, 0, idout);
  //SET_VECTOR_ELT(ans, 1, typout);
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__cos)(int *id, int *idout);
extern SEXP intrf_c__number__cos (SEXP id) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__cos)(INTEGER(id), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__tan)(int *id, int *idout);
extern SEXP intrf_c__number__tan (SEXP id) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__tan)(INTEGER(id), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__sinh)(int *id, int *idout);
extern SEXP intrf_c__number__sinh (SEXP id) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__sinh)(INTEGER(id), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__cosh)(int *id, int *idout);
extern SEXP intrf_c__number__cosh (SEXP id) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__cosh)(INTEGER(id), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
void F77_NAME(intrf_f__number__tanh)(int *id, int *idout);
extern SEXP intrf_c__number__tanh (SEXP id) {
  SEXP idout;
  PROTECT(idout=allocVector(INTSXP, 1));
  F77_CALL(intrf_f__number__tanh)(INTEGER(id), INTEGER(idout));
  UNPROTECT(1);
  return(idout);
}
