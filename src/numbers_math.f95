!> @defgroup numbers_math_ Numbers' Math
!! @author Filippo Monari
!! @{

module numbers_math

  use env
  use types
  use registers
  use errwarn
  use numbers_utils
  use numbers
  use math
  use nodes_utils
  use nodes
  use operators
  use utils
  use hashtabs

  implicit none

  private
  public &
       ! Modifiers
       number__assign, &
       number__slice, &
       number__flat_slice, &
       number__contiguous_slice, &
       number__reshape, &
       number__drop_shape, &
       number__bind, &
       number__embeddings, &
       number__transpose, &
       number__feeding_number, &
       number__feeding_sequence, &
       number__feed, &
       ! ---
       ! Unary
       abs, &
       exp, &
       log, &
       sin, &
       cos, &
       tan, &
       sinh, &
       cosh, &
       tanh, &
       ! ---
       ! Binary
       operator(+), &
       operator(-), &
       operator(*), &
       operator(**), &
       operator(/), &
       ! ---
       ! Activations
       number__sigmoid, &
       number__softplus, &
       number__relu, &
       number__leaky_relu, &
       number__elu, &
       number__silu, &
       number__swish, &
       number__softmax, &
       ! ---
       ! Matrix
       number__gmmmult, &
       number__gmvmult, &
       number__vvouter, &
       number__vvinner, &
       number__invMat, &
       number__slidemmm, &
       ! ---
       ! Kernels
       number__ksqexp, &
       ! Reductions
       number__sum, &
       number__product, &
       number__ssq, &
       ! ---
       ! Stats
       number__ldexp, &
       number__ldlaplace, &
       number__ldbeta, &
       number__ldgamma, &
       number__ldnorm, &
       number__ldmvnorm__1, &
       number__mvnorm_posterior, &
       ! ---
       ! Objectives
       number__binentropy, &
       number__logit_binentropy, &
       number__crossentropy, &
       number__logit_crossentropy, &
       number__mse, &
       number__mae, &
       number__lkhnorm, &
       ! ---
       ! Regularisation
       number__dropout, &
       ! ---
       ! Convolution
       number__conv1d, &
       number__conv2d, &
       number__conv3d, &
       number__maxpool1d, &
       number__maxpool2d, &
       number__maxpool3d
  

  character(len=*), parameter :: mod_numbers_math_name_ = 'numbers_math'

  !> @defgroup numbers_math__activations_ Activation Functions' Interfaces
  !! @{
  include "./number_math/activations/interfaces.f95"
  !> @}

  !> @defgroup numbers_math__binary_ Binary Functions' Interfaces
  !! @{
  include "./number_math/binary/interfaces.f95"
  !> @}

  !> @defgroup numbers_math__kernels_ Kernel Functions' Interfaces
  !! @{
  include "./number_math/kernels/interfaces.f95"
  !> @}

  !> @defgroup numbers_math__matrix_ Matrix Functions' Interfaces
  !! @{
  include "./number_math/matrix/interfaces.f95"
  !> @}

  !> @defgroup numbers_math__modifiers_ Modifiers' Interfaces
  !! @{
  include "./number_math/modifiers/interfaces.f95"
  !> @}

  !> @defgroup numbers_math__objectives_ Objective Functions Interfaces
  !! @{
  include "./number_math/objectives/interfaces.f95"
  !> @}

  !> @defgroup numbers_math__reductions_ Reduction Functions' Interfaces
  !! @{
  include "./number_math/reductions/interfaces.f95"
  !> @}

  !> @defgroup numbers_math__regularisations_ Regularisation Functions' Interfaces
  !! @{
  include "./number_math/regularisations/interfaces.f95"
  !> @}

  !> @defgroup numbers_math__stats_ Statistical Functions' Interfaces
  !! @{
  include "./number_math/stats/interfaces.f95"
  !> @}

  !> @defgroup numbers_math__unary_ Unary Functions' Interfaces
  !! @{
  include "./number_math/unary/interfaces.f95"
  !> @}

  include "./number_math/convolution/interfaces.f95"
  
contains

  include "./number_math/activations/sp_number.f95"
  include "./number_math/activations/dp_number.f95"
  include "./number_math/binary/sp_number.f95"
  include "./number_math/binary/dp_number.f95"
  include "./number_math/kernels/sp_number.f95"
  include "./number_math/kernels/dp_number.f95"
  include "./number_math/matrix/sp_number.f95"
  include "./number_math/matrix/dp_number.f95"
  include "./number_math/modifiers/sp_number.f95"
  include "./number_math/modifiers/dp_number.f95"
  include "./number_math/objectives/sp_number.f95"
  include "./number_math/objectives/dp_number.f95"
  include "./number_math/reductions/sp_number.f95"
  include "./number_math/reductions/dp_number.f95"
  include "./number_math/regularisations/sp_number.f95"
  include "./number_math/regularisations/dp_number.f95"
  include "./number_math/stats/sp_number.f95"
  include "./number_math/stats/dp_number.f95"
  include "./number_math/unary/sp_number.f95"
  include "./number_math/unary/dp_number.f95"
  include "./number_math/convolution/sp_number.f95"
  include "./number_math/convolution/dp_number.f95"
 
end module numbers_math
